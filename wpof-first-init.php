<?php
/*
 * wpof-first-init.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Initialisation du plugin lors de l'installation
 */

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

function first_init()
{
    global $wpdb;

    $reinit = isset($_POST['action']);
    
    if ($reinit) echo "<p>Init sql OPAGA tables</p>";
    init_sql_opaga_tables();
    
    if ($reinit) echo "<p>Import OPAGA options</p>";
    add_option("wpof_version", WPOF_VERSION);
    import_json("options");
    update_option("wpof_annee1", date('Y'));
    
    if ($reinit) echo "<p>Roles et Ultimate options";
    // Rôles ultimate-member
    import_sql("roles");
    init_um_options();
    
    if ($reinit) echo "<p>Créer les pages prédéfinies</p>";
    // Pages prédéfinies (Formations, équipe péda, calendrier, accueil)
    import_sql("pages");
    
    if ($reinit) echo "<p>Modification des permaliens</p>";
    update_option("permalink_structure", '/%postname%/');
    
    flush_rewrite_rules();
}

/*
 * Finalisation de l'initialisation
 */
function opaga_finish_init()
{
    // Désactiver (mettre à la corbeille) la page et le formulaire register
    global $wpdb;
    $register_posts = $wpdb->get_col("SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_um_core' AND meta_value = 'register';");
    if (is_array($register_posts) && count($register_posts) >= 2)
    {
        foreach($register_posts as $id)
            wp_trash_post($id);
        add_option("opaga_maj_init", 1);
    }
    
    flush_rewrite_rules();
}

add_action('wp_ajax_init_sql_opaga_tables', 'init_sql_opaga_tables');
function init_sql_opaga_tables()
{
    global $wpdb;
    $reponse = array('log' => '');
    
    $sql_tables = file_get_contents(wpof_path . "init/opaga_tables.sql");
    $sql_tables = str_replace('{prefix}', $wpdb->prefix, $sql_tables);
    $sql_tables = str_replace('{charset_collate}', $wpdb->get_charset_collate(), $sql_tables);
    $sql_tables = explode(';', $sql_tables);
    foreach($sql_tables as $t)
    {
        $reponse['log'] .= "\n<pre>".$t.'</pre>';
        $res = dbDelta($t);
        $reponse['log'] .= "<p>".$t." → <strong>".var_export($res, true)."</strong></p>";
    }
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        echo json_encode($reponse);
        die();
    }
    else
        return $reponse;
}

add_action('wp_ajax_init_um_options', 'init_um_options');
function init_um_options()
{
    // modèles de mails
    $mail_options = array
    (
        "welcome_email_on" => "1",
        "checkmail_email_on" => "1",
        "pending_email_on" => "1",
        "approved_email_on" => "1",
        "rejected_email_on" => "1",
        "inactive_email_on" => "1",
        "deletion_email_on" => "1",
        "resetpw_email_on" => "1",
        "changedpw_email_on" => "1",
        "changedaccount_email_on" => "1",
        "notification_new_user_on" => "1",
        "notification_review_on" => "1",
        "notification_deletion_on" => "1",
        "validation_docs_email_on" => "1",

        "welcome_email_sub" => "Bienvenue sur {site_name}",
        "checkmail_email_sub" => "{site_name} – Confirmez votre inscription",
        "pending_email_sub" => "{site_name} – En attente de validation",
        "approved_email_sub" => "{site_name} Accès confirmé",
        "rejected_email_sub" => "{site_name} – Accès refusé",
        "inactive_email_sub" => "{site_name} – Votre accès a été désactivé",
        "deletion_email_sub" => "{site_name} – Compte supprimé",
        "resetpw_email_sub" => "{site_name} – Demande de réinitialisation de votre mot de passe",
        "changedpw_email_sub" => "{site_name} – Nouveau mot de passe",
        "changedaccount_email_sub" => "Votre compte sur {site_name} a été mis à jour",
        "notification_new_user_sub" => "{site_name} Nouveau compte créé pour {display_name}",
        "notification_review_sub" => "{site_name} – Validation requise pour {display_name}",
        "notification_deletion_sub" => "{site_name} – Suppression du compte de {display_name}",
        "validation_docs_email_sub" => "{site_name} - Vous avez des documents à valider",
    );
    $profile_options = array
    (
        "profile_area_max_width" => "1000px",
        "profile_cover_enabled" => "0",
        "profile_show_bio" => "0",
        "profile_empty_text" => "0",
        "profile_tab_main" => "0",
        "profile_tab_posts" => "0",
        "profile_tab_comments" => "0",
        "disable_profile_photo_upload" => "1",
        "login_secondary_btn" => "0",
        "members_page" => "",
        "permalink_base" => "name",
        "author_redirect" => "0",
        "members_page" => "0",
    );
    
    $um_options = get_option("um_options");
    update_option("um_options", array_merge($um_options, $mail_options, $profile_options));
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        ob_start();
        ?>
        <dl>
        <?php foreach(get_option("um_options") as $k => $v) : ?>
            <dt><?php echo $k; ?></dt>
            <dd><?php var_dump($v); ?></dd>
        <?php endforeach; ?>
        </dl>
        <?php
        echo json_encode(array('log' => ob_get_clean()));
        die();
    }
}
//init_um_options();

?>
