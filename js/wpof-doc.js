jQuery(document).ready(function($)
{
    var datepicker_trois_mois =
    {
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 3,
    };
    
    function doc_row_append_events(tr_object)
    {
        tr_object.find('.doc-creer').on('click', '', doc_creer);
        tr_object.find('.doc-check-ckw').on('click', '', doc_check_ckw);
        tr_object.find('.infos-manquantes').on('click', '', doc_infos_manquantes);
        tr_object.find('.info-parent').on('click', '', highlight_parent);
        tr_object.find('.doc-demande-valid').on('click', '', doc_demande_valid);
        tr_object.find('.doc-diffuser').on('click', '', doc_diffuser);
        tr_object.find('.doc-scan').on('click', '', doc_scan);
        tr_object.find('.doc-supprimer').on('click', '', doc_supprimer);
        tr_object.find('.doc-valider').on('click', '', doc_valider);
        tr_object.find('.doc-rejeter').on('click', '', doc_rejeter);
        tr_object.find('.input_jpost input').on('change', '', change_jpost_value);
        tr_object.find('.input_jpost select').on('change', '', change_jpost_value);
        tr_object.find('.input_jpost_value.datepicker').on('focus', '', function() { $(this).datepicker(datepicker_trois_mois); });
        
        return tr_object;
    }
    
    // Bouton de vérification des custom keywords
    $(".doc-check-ckw").click(doc_check_ckw);
    function doc_check_ckw(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        
        jQuery.post
        ({
            url: ajaxurl,
            async: false,
            data: {
                'action': 'doc_check_custom_keywords',
                'contexte': contexte,
                'contexte_id': contexte_id,
                'session_id': session,
                'type_doc': type_doc,
            },
            success: function(response)
            {
                param = JSON.parse(response);
                dialog_box = $(param.dialog);
                dialog_box.on('click', '.openButton', toggle_bloc_hidden);
                dialog_box.dialog
                ({
                    autoOpen: false,
                    minWidth: 400,
                    width: 'auto',
                    modal: true,
                    title: "Informations à renseigner pour un document complet",
                    buttons:
                    {
                        "Valider": function()
                        {
                            dialog_box = $(this);
                            form = dialog_box.find("form")[0];
                            formData = new FormData(form);
                            dialog_box.find("textarea.wp-editor-area").each(function()
                            {
                                name = $(this).attr('id');
                                formData.set(name, tinyMCE.get(name).getContent().replace("'", "’"));
                                tinyMCE.execCommand("mceRemoveEditor", false, $(this).attr('id'));
                            });
                            jQuery.post
                            ({
                                url: ajaxurl,
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (response)
                                {
                                    param = JSON.parse(response);
                                    newline = doc_row_append_events($($(param.lignedoc).html()));
                                    ligne.removeClass("empty");
                                    ligne.empty().append(newline);
                                    ligne.find('.selectable input[type="checkbox"]').prop('checked', checked);
                                },
                            });
                            dialog_box.dialog("close");
                        },
                    },
                    close:function()
                    {
                        dialog_box.dialog("destroy");
                    },
                });
                if (dialog_box.find('textarea.wp-editor-area') != undefined)
                {
                    tinyMCE.init(tinymce_options);
                    dialog_box.ready(function()
                    {
                        dialog_box.find('textarea.wp-editor-area').each(function()
                        {
                            tinyMCE.execCommand("mceAddEditor", false, $(this).attr('id'));
                        });
                        dialog_box.on('click', '.add_keyword_defaut', add_keyword_defaut);
                        dialog_box.find('.insert-date').on('click', '', insert_date);
                        dialog_box.find('.del-content').on('click', '', del_editor_content);
                    });
                }
                dialog_box.dialog("open");
            }
        });
    }
    
    // Copier la valeur par défaut dans le champ input correspondant
    $(".add_keyword_defaut").click(add_keyword_defaut);
    function add_keyword_defaut(e)
    {
        e.preventDefault();
        
        input_id = $(this).attr('data-id');
        input_type = $(this).attr('data-type');
        content = $(this).attr('data-content');
        
        switch(input_type)
        {
            case "bool":
                suffix = (content == 1) ? "oui" : "non";
                $("#" + input_id + suffix).prop('checked', true);
                break;
            case "text":
                tinyMCE.get(input_id.replace("ckw_", "")).setContent(content);
                break;
            case "number":
                $("#" + input_id).val(content);
                break;
            default:
                break;
        }
    }
    
    // Bouton créer ou signer le document
    $(".doc-creer").click(doc_creer);
    function doc_creer(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        doc_uid = ligne.attr("data-docuid");
        attention = $(this).hasClass("attention");
        action_doc = "brouillon";
        var ckw = 0;
        if ($(this).attr("data-final"))
            action_doc = "final";
        else
            ckw = $(this).attr('data-ckw');
        cols = ligne.attr("data-cols");
        if ($(this).attr("data-signataire"))
            signataire = 1;
        else
            signataire = 0;
        
        checked = false;
        if (ligne.find('.selectable input[type="checkbox"]').prop('checked') == true)
            checked = true;
        
        upload_form_name = "upload-" + doc_uid;
        
        if (!attention)
        {
            bouton_text = $("#nom-" + doc_uid).html();
            $("#nom-" + doc_uid).html("Patientez");
        
            ligne.addClass("empty");
            jQuery.post
            ({
                url: ajaxurl,
                asinc: false,
                data: {
                    'action': 'traitement_doc',
                    'contexte_id': contexte_id,
                    'contexte': contexte,
                    'session_id': session,
                    'type_doc': type_doc,
                    'cols': cols,
                    'signataire': signataire,
                    'action_doc': action_doc,
                },
                success: function(response)
                {
                    param = JSON.parse(response);
                    newline = doc_row_append_events($($(param.lignedoc).html()));
                    tr_id = $(param.lignedoc).attr('id');
                    if (ligne.closest("table.datatable").length > 0)
                    {
                        table = $("table.datatable").DataTable();
                        new_data = Array();
                        $(param.lignedoc).children().each(function ()
                        {
                            new_data.push($(this).html());
                        });
                        table.row("#" + tr_id).data(new_data);
                        $('#' + tr_id).removeClass("empty");
                        $('#' + tr_id).find('.selectable input[type="checkbox"]').prop('checked', checked);
                    }
                    else
                    {
                        ligne.removeClass("empty");
                        ligne.empty().append(newline);
                    }
                    //ligne.find('.selectable input[type="checkbox"]').prop('checked', checked);
                    if (param.message !== undefined)
                        show_message(param.message);
                }
            });
        }
    }
    
    // Afficher les infos manquantes d'un document dans la message box
    $(".docrow .infos-manquantes").click(doc_infos_manquantes);
    function doc_infos_manquantes(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'doc_get_infos_manquantes',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
            },
            function(response)
            {
                $("#opaga-message .message").html("");
                show_message(response);
            },
        );
    }


    // Bouton demander la validation (signature par le responsable de formation)
    $(".doc-demande-valid").click(doc_demande_valid);
    function doc_demande_valid(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        doc_uid = ligne.attr("data-docuid");
        cols = ligne.attr("data-cols");
        
        if (session == undefined)
        {
            console.log($(this));
            ligne.addClass("bg-erreur").css("height: 30px;");
        }
        
        if (ligne.closest(".datatable") !== undefined)
            datatable_context = true;
        else
            datatable_context = false;
        if (datatable_context)
            ligne.addClass("empty");
        
        checked = false;
        if (ligne.find('.selectable input[type="checkbox"]').prop('checked') == true)
            checked = true;
        
        jQuery.post
        ({
            url: ajaxurl,
            data: ({
                'action': 'traitement_doc',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
                'cols': cols,
                'action_doc': 'demande_valid',
            }),
            async: false,
            success: function(response)
            {
                param = JSON.parse(response);
                if (datatable_context)
                {
                    newline = doc_row_append_events($($(param.lignedoc).html()));
                    ligne.removeClass("empty");
                    ligne.empty().append(newline);
                    ligne.find('.selectable input[type="checkbox"]').prop('checked', checked);
                }
                if (e.isTrigger === undefined)
                    notification_update('ou envoyez-la plus tard en passant par l’icône de notifications <span class="icone big important"><a href="#notifications"><i class="fa fa-bell"></i></a></span>');
            }
        });
        
        // TODO plutôt que toggle, vérifier le véritable état !
        if (datatable_context === false)
            $(this).toggleClass("en-cours");
        
    }

    // Bouton pour diffuser le document au stagiaire
    $(".doc-diffuser").click(doc_diffuser);
    function doc_diffuser(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        doc_uid = ligne.attr("data-docuid");
        cols = ligne.attr("data-cols");
        
        if (ligne.closest(".datatable") !== undefined)
            datatable_context = true;
        else
            datatable_context = false;
        if (datatable_context)
            ligne.addClass("empty");
        
        checked = false;
        if (ligne.find('.selectable input[type="checkbox"]').prop('checked') == true)
            checked = true;
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'traitement_doc',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
                'cols': cols,
                'action_doc': 'diffuser',
            },
            function(response)
            {
                param = JSON.parse(response);
                if (datatable_context)
                {
                    newline = doc_row_append_events($($(param.lignedoc).html()));
                    ligne.removeClass("empty");
                    ligne.empty().append(newline);
                    ligne.find('.selectable input[type="checkbox"]').prop('checked', checked);
                }
                show_message(param.message);
            }
        );
        
        // plutôt que toggle, vérifier le véritable état !
        $(this).toggleClass("en-cours");
    }

    // Bouton pour supprimer le document
    $(".doc-supprimer").click(doc_supprimer);
    function doc_supprimer(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        doc_uid = ligne.attr("data-docuid");
        cols = ligne.attr("data-cols");
        
        var supprim = true;
        yesno = $("<div>La suppression est irréversible !</div>");
        yesno.dialog(
        {
            autoOpen: true,
            title: "Êtes-vous sûr ?",
            classes: { 'ui-dialog': 'dialog-remove' },
            modal: true,
            buttons:
            {
                "Oui": function()
                {
                    jQuery.post
                    (
                        ajaxurl,
                        {
                            'action': 'traitement_doc',
                            'contexte_id': contexte_id,
                            'contexte': contexte,
                            'session_id': session,
                            'type_doc': type_doc,
                            'cols': cols,
                            'action_doc': 'supprimer',
                        },
                        function(response)
                        {
                            param = JSON.parse(response);
                            newline = doc_row_append_events($($(param.lignedoc).html()));
                            ligne.removeClass("empty");
                            ligne.empty().append(newline);
                            ligne.find('.selectable input[type="checkbox"]').prop('checked', checked);
                        }
                    );
                    yesno.dialog("destroy");
                },
                "Non": function() { supprim = false; yesno.dialog("destroy");},
            },
            close: function()
            {
                supprim = false; yesno.dialog("destroy");
            }
        });
        yesno.dialog("open");
    }
    
    $('.doc-valider').click(doc_valider);
    function doc_valider(e)
    {
        e.preventDefault();
        
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        cols = ligne.attr("data-cols");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'traitement_doc',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
                'cols': cols,
                'action_doc': 'valider',
            },
            function(response)
            {
                param = JSON.parse(response);
                ligne.remove();
                show_message(param.message);
            }
        );
    }
    
    $('.doc-rejeter').click(doc_rejeter);
    function doc_rejeter(e)
    {
        e.preventDefault();
        
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        cols = ligne.attr("data-cols");
        
        reject_doc = $('<div><form><textarea name="message-rejet" cols="80" rows="10"></textarea></form></div>');
        reject_doc.dialog(
        {
            autoOpen: true,
            title: "Raison du rejet",
            classes: { 'ui-dialog': 'dialog-remove' },
            modal: true,
            buttons:
            {
                "Valider": function()
                {
                    message_rejet = reject_doc.find("textarea[name='message-rejet']").val();
                    jQuery.post
                    (
                        ajaxurl,
                        {
                            'action': 'traitement_doc',
                            'contexte_id': contexte_id,
                            'contexte': contexte,
                            'session_id': session,
                            'type_doc': type_doc,
                            'cols': cols,
                            'action_doc': 'rejeter',
                            'message_rejet': message_rejet,
                        },
                        function(response)
                        {
                            param = JSON.parse(response);
                            ligne.remove();
                            show_message(param.message);
                        }
                    );
                    reject_doc.dialog("destroy");
                },
                "Annuler": function() { supprim = false; reject_doc.dialog("destroy");},
            },
            close: function()
            {
                supprim = false; reject_doc.dialog("destroy");
            }
        });
        reject_doc.dialog("open");
    }
    

    // Modification du document en HTML
    $(".doc-edit-html").click(function(e)
    {
        e.preventDefault();
        
        info_doc = $(this).closest(".docrow");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'get_html_doc_content',
                'session_id': info_doc.attr('data-sessionid'),
                'contexte': info_doc.attr('data-contexte'),
                'contexte_id': info_doc.attr('data-contexteid'),
                'typedoc': info_doc.attr('data-typedoc'),
            },
            function(response)
            {
                param = JSON.parse(response);
                dialog_box = $(param.html);
                dialog_box.dialog
                ({
                    autoOpen: true,
                    minWidth: 700,
                    width: 'auto',
                    modal: true,
                    buttons:
                    {
                        "Valider": function()
                        {
                            dialog_box.find("textarea.wp-editor-area").each(function()
                            {
                                name = $(this).attr('id');
                                content = tinyMCE.get(name).getContent().replace("'", "’");
                                console.log(content);
                                jQuery.post
                                (
                                    ajaxurl,
                                    {
                                        'action': 'update_html_doc_content',
                                        'session_id': info_doc.attr('data-sessionid'),
                                        'contexte': info_doc.attr('data-contexte'),
                                        'contexte_id': info_doc.attr('data-contexteid'),
                                        'typedoc': info_doc.attr('data-typedoc'),
                                        'content': content,
                                    },
                                    function(response)
                                    {
                                        
                                    }
                                )
                            });
                            $(this).dialog("close");
                        },
                        "Fermer": function()
                        {
                            $(this).dialog("close");
                        },
                    },
                    close:function()
                    {
                        tinyMCE.execCommand("mceRemoveEditor", false, $(this).attr('id'));
                        $(this).dialog("destroy");
                    }
                });
                dialog_box.ready(function()
                {
                    dialog_box.find('textarea.wp-editor-area').each(function()
                    {
                        tinyMCE.execCommand("mceAddEditor", false, $(this).attr('id'));
                    });
                });
            }
        );
    });
    
    // bouton pour envoyer un document basé sur un modèle prédéfini
    $(".doc-scan").click(doc_scan);
    function doc_scan(e)
    {
        e.preventDefault();
        $(this).parent().find(".message").html("");
        dialog = $(this).parent().children(".dialog");
        dialog.dialog(
        {
            autoOpen: false,
            height: 400,
            width: 550,
            modal: true,
    //        create: function() { $(this).on('click', '.ajax-save-file', save_file); },
            buttons:
            {
                "Déposer": function() { save_file(dialog.find("form")); },
                "Fermer": function() { dialog.dialog("destroy");}
            },
            close: function()
            {
                //form[ 0 ].reset();
                allFields.removeClass( "ui-state-error" );
            }
        });
        dialog.dialog("open");
    }

    // Supression d'un fichier téléversé
    $(".del-scan").click(del_scan);
    function del_scan(e)
    {
        e.preventDefault();
        id_span_message = "#" + $(this).attr("data-message");
        id_conteneur = "#" + $(this).attr("data-conteneur");
        session_id = $(this).attr("data-sessionid");
        md5sum = $(this).attr("data-md5sum");
        
        jQuery.post
        ({
            url: ajaxurl,
            data:
            {
                'action': 'delete_scan_file',
                'session_id': session_id,
                'md5sum': md5sum,
            },
            success: function(response)
            {
                param = JSON.parse(response);
                $(id_span_message).append("<p>" + param.message + "</p>");
                $(id_conteneur).remove();
            },
            error: function()
            {
                $(id_span_message).html("<span class='erreur'>Erreur jQuery post</span>");
            }
        });
    }

    $(".info-parent").click(highlight_parent);
    function highlight_parent(e)
    {
        e.preventDefault();
        lines = $("tr." + $(this).attr('data-parent'));
        lines.children('td').addClass("bg-succes").delay(1000).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
    }
    
    
        // envoyer un fichier libre en ajax post
    $(".ajax-save-file").click(function(e)
    {
        form = $(this).closest('form');
        save_file(form);
    });

    // fonction qui gère le dépot de fichier, soit prédéfini (input[name='document'] est renseigné), soit libre
    function save_file(form)
    {
        var id_span_message = "#" + form.find("input[name='id_span_message']").val();
        var id_span_filename = "#" + form.find("input[name='id_span_filename']").val();
        var ligne_id = "#" + form.find("input[name='ligne_id']").val();
        var ligne = $(ligne_id);
        
        filelist = form.children("input[type='file']")[0].files;
        if (filelist.length == 0)
        {
            form.children('input[type="file"]').addClass('erreur').delay(1000).queue(function(){ $(this).removeClass("erreur").dequeue(); });
            return;
        }
        
        json_filelist = Array();
        
        var formData = new FormData();
    //    formData.set('file', $(this).parent().children("input[type='file']").prop('files'));
        for (var i = 0; i < filelist.length; i++)
        {
            formData.append("files[]", (filelist[i]));
        }
        
        formData.set('action', form.find("input[name='action']").val());
        if (form.find("input[name='session_id']").val() !== undefined)
            formData.set('session_id', form.find("input[name='session_id']").val());
        else if (form.find("input[name='user_id']").val() !== undefined)
        {
            formData.set('user_id', form.find("input[name='user_id']").val());
            formData.set('meta_key', form.find("input[name='meta_key']").val());
        }
        formData.set('nb_files', filelist.length);
                        
        if (ligne.attr('data-typedoc') != undefined)
        {
            formData.set('contexte_id', ligne.attr("data-contexteid"));
            formData.set('contexte', ligne.attr("data-contexte"));
            formData.set('signataire', form.find("input[name='signataire']").val());
            formData.set('signature_responsable', form.find("input[name='signature_responsable']").prop('checked'));
            formData.set('signature_client', form.find("input[name='signature_client']").prop('checked'));
            formData.set('signature_stagiaire', form.find("input[name='signature_stagiaire']").prop('checked'));
            formData.set('callback', form.find("input[name='callback']").val());
            formData.set('cols', form.find("input[name='cols']").val());
            formData.set('type_doc', ligne.attr("data-typedoc"));
            formData.set('doc_uid', ligne.attr("data-docuid"));
        }
        
        $(id_span_message).html('Patientez');
        
        jQuery.post
        ({
            url: ajaxurl,
            data: formData,
            processData: false,
            contentType: false,
            success: function(response)
            {
                param = JSON.parse(response);
                $(id_span_message).html("<p>" + param.message + "</p>");
                if (param.lignedoc != "")
                {
                    newline = doc_row_append_events($($(param.lignedoc).html()));
                    $(ligne_id).removeClass("empty");
                    $(ligne_id).empty().append(newline);
                    $(ligne_id).find('.selectable input[type="checkbox"]').prop('checked', checked);
                }
                else if (param.img !== undefined)
                {
                    img_id = "#" + formData.get('meta_key') + '_img';
                    if ($(img_id) !== undefined)
                        $(img_id).replaceWith(param.img);
                    else
                        $("#" + formData.get('meta_key') + '_link').replaceWith(param.img);
                }
                else if (param.link !== undefined)
                {
                    $("#" + formData.get('meta_key') + '_link').replaceWith(param.link);
                }
                else
                {
                    $("#liste-scan tr:nth-child(n+2)").remove();
                    $("#liste-scan").append(param.filename);
                    $("#liste-scan").on('click', ".del-scan", del_scan);
                }
                if (param.remove !== undefined)
                    $(param.remove).remove();
            },
            error: function()
            {
                $(id_span_message).html("<span class='erreur'>Erreur jQuery post</span>");
            }
        });
    }
    
    $('#doc_select select[name="documents"]').change(function(e)
    {
        e.preventDefault();
        type_doc = $(this).val();
        
        dest = $($(this).attr('data-dest')).DataTable();
        if (dest !== undefined)
        {
            dest.cells(".selectable").nodes().each(function()
            {
                $(this).find("input[type='checkbox']").prop('checked', false);
            });
            dest.cells('.' + type_doc + " .selectable").nodes().each(function()
            {
                $(this).find("input[type='checkbox']").prop('checked', true);
            });
        }
    });
    
    $("#doc_global_command span").click(function(e)
    {
        dest = $($(this).closest("#doc_global_command").attr('data-dest')).DataTable();
        target_bouton = $(this).attr('data-target');
        fonction = $(this).attr('data-function');
        
        possible_fonction = [ 'doc_view_checked' ];
        
        if (dest !== undefined && target_bouton !== undefined)
        {
            show_message('Patientez !');
            founded_docs = 0;
            dest.rows('.docrow').every(function()
            {
                row = $(this.node(0));
                if (row.find(".selectable input[type='checkbox']").prop('checked') == true && row.find(target_bouton).length > 0)
                {
                    row.find(target_bouton).trigger('click');
                    founded_docs++;
                }
            });
            clear_message();
            if (founded_docs == 0)
                show_message('Veuillez sélectionner des documents concernés par l’action <strong>' + $(this).attr('title') + '</strong>');
            else
            {
                plural = (founded_docs > 1) ? "s" : "";
                show_message(founded_docs + ' document' + plural + ' traité' + plural);
            }
            
            if (target_bouton == '.doc-demande-valid')
            {
                notification_update('ou envoyez-la plus tard en passant par l’icône de notifications <span class="icone big important"><a href="#notifications"><i class="fa fa-bell"></i></a></span>');
            }
        }
        else if (possible_fonction.includes(fonction))
        {
            doc_functions_array[fonction](dest);
        }
    });
    
    $('.selectable input[type="checkbox"]').change(checkbox_change);
    function checkbox_change(e)
    {
        e.preventDefault();
        $('#doc_select select[name="documents"]').val(-1);
    }
    
    doc_functions_array =
    {
        'doc_view_checked': function(dest)
            {
                dest.rows('.docrow').every(function()
                {
                    row = $(this.node(0));
                    if (row.find(".selectable input[type='checkbox']").prop('checked') == true)
                        row.show();
                    else
                        row.hide();
                });
            }
    }

    /*
    $(".doc-compile").click(function()
    {
        session = $(this).attr("data-sessionid");
        user = $(this).attr("data-userid");
        doc = $(this).attr("data-docid");
        resultat = "#" + $(this).attr("data-resultat");
        
        $(resultat).html("Patientez...");
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'export_pdf',
                'user_id': user,
                'session_id': session,
                'doc_id': doc,
            },
            function(response)
            {
                $(resultat).html(response);
            }
        );
        
    });
    */

});
