jQuery(document).ready(function($)
{
$('body').on("keydown", null, do_shortcuts);
$('.openButton').click(toggle_bloc_hidden);
$('.notif-icon').click(toggle_bloc_hidden);
$('.notif-action').click(notification_action);

// Fonction pour ouvrir ou fermer un bloc depuis un bouton radio ou checkbox
// Le bouton doit avoir pour data-id l'id du bloc à gérer
// Pas forcément utile de perdre du temps là-dessus...
$('.openCheck').change(function(e)
{
    id = "#"+$(this).attr("data-id");
    $(id).toggleClass('blocHidden');
});

$('.toggle-button').click(toggle_button);

$('#show-log').click(function(e)
{
    $('#opaga-log').show(200);
});
$('#opaga-message .close').click(close_opaga_message);
function close_opaga_message()
{
    message_box = $('#opaga-message');
    message_box.hide(400);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'show_message_box',
        },
        function (response)
        {
            contenu = $(response);
            contenu.on('click', '.close', close_opaga_message);
            $(document).off('keydown');
            message_box.replaceWith(contenu);
        }
    );
}
$('#opaga-log .close').click(function() {$(this).parent().hide(400); });
$('#notifications .close').click(function() {$(this).parent().addClass('blocHidden'); });

$('.choix-interactif').click(function()
{
    $(this).children("li").each(function()
    {
        if ($(this).children("label").children("input").is(':checked'))
            $(this).children("label").children("span").show();
        else
            $(this).children("label").children("span").hide();
    });
});

// Gestion du menu du tableau de bord de session
$('.wpof-menu ul .onglet').click(switch_onglet);
function switch_onglet(e)
{
    id = "#" + $(this).attr("data-id");
    famille = $(this).parent().parent().attr("data-famille");
    $('.famille-' + famille + ' .tableau').addClass('blocHidden');
    $('.famille-' + famille + ' ul .onglet').removeClass('highlightButton');
    if (false === $(this).hasClass("fermer-tableau"))
    {
        $(id).removeClass('blocHidden');
        $(this).addClass('highlightButton');
    }
}

$("#main-tabs").tabs({active: $("input[name='default_main_tab']").val()});
$("#options-tabs").tabs({active: $("input[name='default_options_tab']").val()});
$("#user-tabs").tabs({active: $("input[name='default_user_tab']").val()});
var tabs_client = $("#tabs-clients").tabs();
tabs_client.addClass( "ui-tabs-vertical ui-helper-clearfix" );
$("#tabs-clients li").removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
tabs_client.find(".ui-tabs-nav").sortable
({
    axis: "y",
    stop: function()
    {
        list = $(this).children();
        if (list.first().hasClass("tab-stagiaire"))
            $(this).sortable("cancel");
        else
        {
            const list_id = [];
            last_client = null;
            last_client_id = 0;
            cpt = 0;
            list.each(function()
            {
                if ($(this).hasClass("tab-client"))
                    list_id.push({type: "client", id: $(this).attr('data-id')});
                else
                    list_id.push({type: "stagiaire", id: $(this).attr('data-id')});
            });
            jQuery.post
            (
                ajaxurl,
                {
                    'action': 'sort_clients_stagiaires',
                    'session_id': $(".id.session").attr('data-id'),
                    'list_id': list_id,
                },
                function(response)
                {
                    console.log(response);
                }
            )
        }
        tabs_client.tabs( "refresh" );
        console.log($(this));
    },
    cancel: "li.tab-client",
    cursor: "move",
    dropOnEmpty: false,
    connectWith: ".tab-client",
});
$("#profile-box-menu").menu();


$("#main-tabs > ul > li").click(switch_default_tab);
$("#options-tabs > ul > li").click(switch_default_tab);
$("#user-tabs > ul > li").click(switch_default_tab);
function switch_default_tab(e)
{
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'set_default_tab',
            'tab_id': $(this).attr('aria-labelledby').replace('ui-id-', ''),
            'tab_name': $(this).closest('.ui-tabs').attr('id'),
        },
    );

    if ($(this).parent().attr('data-reload') != undefined)
        document.location.reload();
}


/*
$('.wpof-menu ul li.fermer-tableau').click(function(e)
{
    id = $(this).attr("data-id");
    famille = $(this).parent().parent().attr("data-famille");
    $('.tableau' + id).addClass('blocHidden');
    $('.wpof-menu ul li.bouton').removeClass('highlightButton');
});
*/


// Champs relatifs à la TVA dans les options générales
$("input.tva").change(function(e)
{
    e.preventDefault();
    if ($("input[name='wpof_of_hastva']").prop("checked"))
    {
        $("input[name='wpof_of_exotva']").prop("disabled", false);
        $("input[name='wpof_of_tauxtva']").prop("disabled", false);
    }
    else
    {
        $("input[name='wpof_of_tauxtva']").prop("disabled", true);
        $("input[name='wpof_of_exotva']").prop("disabled", true);
    }
});

// 
$('input:checkbox.has-employeur').change(function (e)
{
    id = "#" + $(this).attr("data-id");
    $('table' + id + ' .employeur-oui').toggleClass('blocHidden');
    $('table' + id + ' .employeur-non').toggleClass('blocHidden');
});


$('textarea.autoselect').click(function (e)
{
    $(this).select();
});

/*
$('input.tarif_total_chiffre').click(function (e)
{
    id = $(this).attr("data-id");
    if ($(this).val() == "")
        $(this).val($('#nb-jour' + id).val() * $('#tarif-jour' + id).val());
});
*/

$(".sql_select").click(function(e)
{
    e.preventDefault();
    session_id = $(this).attr("data-sessionid");
    stagiaire_id = $(this).attr("data-stagiaireid");
    
    sql_info = $("#pilote_dialog");
    
    if (sql_info.dialog( "instance" ))
        sql_info.dialog("destroy");

    sql_info.dialog
    ({
        autoOpen: true,
        title: "Infos sur la session " + session_id,
        height: 600,
        width: 1200,
        modal: false,
        close: function()
        {
            sql_info.dialog("destroy");
        }
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action' : 'sql_session_formation',
            'session_id' : session_id,
            'stagiaire_id' : stagiaire_id,
        },
        function (response)
        {
            sql_info.html(response);
        }
    );
});

$(".button-add-media").click(function(e)
{
    e.preventDefault();
    
    bouton_enregistrer = $(this).closest('.metadata').find('.bouton');
    value_id = $(this).attr("data-valueid");
    link_media = $(this).attr("data-linkmedia");
    img_media = $(this).attr("data-image");
    
    if (!$(this).hasClass("inactif"))
    {
        var uploader = wp.media
        ({
            title: "Envoyer un fichier",
            button: { text: "Téléverser" },
            multiple: false,
        })
        .on('select', function()
        {
            var selection = uploader.state().get('selection');
            var attachment = selection.first().toJSON();
            //console.log(attachment);
            $("#" + link_media).attr("href", attachment.link);
            $("#" + link_media).text(attachment.title);
            $("#" + value_id).val(attachment.id);
            $("#" + value_id).trigger("change");
            if (img_media != undefined)
                $("#" + img_media).attr("src", attachment.url);
        })
        .open();
    }
});



// Permuter d'utilisateur
$(".switch-user").click(function(e)
{
    e.preventDefault();
    user = $(this).attr("data-userid");
    url = $(this).attr("data-url");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'switchuser',
            'user_id': user,
        },
        function(response)
        {
            document.location.assign(url);
        }
    );
});

$('.user_func').click(function(e)
{
    stop_propagation(e);
    user_id = $(this).attr('data-userid');
    fonction = $(this).attr('data-func');
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': fonction,
            'user_id': user_id,
        },
        function(response)
        {
            param = JSON.parse(response);
            show_message(param.message);
        }
    );
});

// Bouton pour ajouter une ligne d'ajout de nouveau compte
var compte_num = 1;
$(".new-compte span#plus").click(function(e)
{
    var markup = "<tr id='" + compte_num +"_new-compte' data-num='" + compte_num +"' class='new-compte new-compte-plus'>";
    if ($(this).attr('data-genre') == 1)
        markup += "<td><select name='" + compte_num +"_genre'><option value='Madame'>Madame</option><option value='Monsieur'>Monsieur</option></select></td>";
    markup += "<td><input type='text' name='" + compte_num +"_firstname' placeholder='Prénom' /></td>";
    markup += "<td><input type='text' name='" + compte_num +"_lastname' placeholder='Nom' /></td>";
    markup += "<td><input type='text' name='" + compte_num +"_email' placeholder='Courriel' /></td>";
    if ($("table.add-compte tbody").find('input[name="0_code"]').length > 0)
        markup += "<td><input type='text' name='" + compte_num +"_code' placeholder='Code formateur⋅ice' /></td>";
    select_role = $(this).attr("data-selectrole");
    if (select_role !== undefined)
    {
        markup += "<td><select name='" + compte_num + "_role'>";
        role_list = JSON.parse(select_role);
        $.each(role_list, function(index, value)
        {
            markup += "<option value='" + index + "'>" + value + "</option>";
        });
        markup += "</select></td>";
    }
    markup += "</tr>";
    $("table.add-compte tbody").append(markup);
    compte_num ++;
});

// ajouter des stagiaires (création de compte + éventuellement inscription à la session session_id)
$("#add-compte-bouton").click(function(e)
{
    bouton = $(this);
    parent_id = "#" + $(this).parent().attr("id");
    var allusers = new Array();
    $(parent_id + " .new-compte").each(function()
    {
        id = "#" + $(this).attr("id");
        num = $(this).attr("data-num");
        genre = $(id + " :input[name$='_genre']").val();
        firstname = $(id + " :input[name$='_firstname']").val();
        lastname = $(id + " :input[name$='_lastname']").val();
        email = $(id + " :input[name$='_email']").val();
        code = $(id + " :input[name$='_code']").val();
        role = $(id + " :input[name$='_role']").val();
        allusers[num] = { "genre" : genre, "firstname" : firstname, "lastname" : lastname, "email" : email, "code": code, "role" : role };
    });
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'add_compte',
            dataType: 'json',
            contentType: 'application/json',
            'data': JSON.stringify({ 'allusers' : allusers }),
        },
        function(response)
        {
            param = JSON.parse(response);
            show_message(param);
            bouton.removeClass('enregistrement-requis');
            $(parent_id).find('input').val('');
            $(parent_id).find('.new-compte-plus').remove();
        }
    );
});


$('.stagiaire-submit').click(function(e)
{
    e.preventDefault();
    
    bouton = $(this);
    message_bloc = $(this).closest('.ui-tabs-panel').children('.message');
    formData = new FormData($(this).closest("form")[0]);
    formData.set('action', 'enregistrer_stagiaire_form');
    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response)
        {
            param = JSON.parse(response);
            bouton.removeClass('enregistrement-requis');
            message_bloc.html(param.message);
        }
    });
    
});

$(".notif-modif input").change(notification_modif);
$(".notif-modif textarea").change(notification_modif);
$(".notif-modif select").change(notification_modif);
function notification_modif(e)
{
    e.preventDefault();
    $(this).closest(".notif-modif").find(".submit").addClass("enregistrement-requis");
}

$(".enregistrement-requis").mouseup(function(e) { $(this).removeClass("enregistrement-requis"); });

// Créneaux

// Activer ou désactiver un créneau pour un client ou un stagiaire
$("div.creneau").click(switch_creneau);
function switch_creneau(e)
{
    creno_id = $(this).attr("data-id");
    session_id = $(this).closest(".tableau-creneau").attr("data-sessionid");
    objet_id = $(this).closest(".tableau-creneau").attr("data-objetid");
    objet = $(this).closest(".tableau-creneau").attr("data-objet");
    
    if (undefined === objet_id) return;
    
    var creno = $(this);
    
    // actif vaut 0 si this a la classe actif (ça veut dire que ce clic le désactive) et 1 dans le cas inverse
    actif = ($(this).hasClass("actif")) ? 0 : 1;
    
    jQuery.post
    (
        ajaxurl,
        {
            'action' : 'active_creneau',
            'creno_id' : creno_id,
            'session_id' : session_id,
            'objet_id' : objet_id,
            'objet' : objet,
            'actif' : actif,
        },
        function(response)
        {
            param = JSON.parse(response);
            
            if (param.nb_heure_estime_decimal !== undefined)
            {
                //console.log(response);
                creno.toggleClass("actif");
                
                // Mise à jour de pour_infos_box
                params = { 'object_class': objet, 'session_id': session_id, 'client_id': objet_id, 'user_id': objet_id };
                postprocess_func['update_pour_infos_client'](params);
                jQuery(".input_jpost.nb_heure_estime_decimal").each(function()
                {
                    if ($(this).find('input[name="object_class"]').val() == objet)
                    {
                        if ((objet == "Client" && $(this).find('input[name="client_id"]').val() == objet_id)
                            || (objet == "SessionStagiaire" && $(this).find('input[name="stagiaire_id"]').val() == objet_id))
                            $(this).find('input[name="nb_heure_estime_decimal"]').val(param.nb_heure_estime_decimal);
                    }
                });
            }
            else
                console.log(param.erreur);
        }
    );
}

    var datepicker_sql_options =
    {
        dateFormat: 'yy-mm-dd',
    };
    var datepicker_options =
    {
        dateFormat: 'dd/mm/yy',
        onClose: datepicker_close,
        numberOfMonths: 3,
    };
    var datepicker_base =
    {
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 1,
    };
    var datepicker_trois_mois =
    {
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 3,
    };
    var datepicker_signature_options = { dateFormat: 'd MM yy' };
    
    $('.liste-creneau .datepicker').datepicker(datepicker_options);
    $('input.input_jpost_value.datepicker').datepicker(datepicker_base);
    $('input[name="decale_date"]').datepicker(datepicker_trois_mois);
    $('.datepicker.input_jpost_value').datepicker(datepicker_signature_options);
    $('.datepicker_sql').datepicker(datepicker_sql_options);
    
    /* Plage de date */
    $('.choix_plage_date .datepicker').datepicker(datepicker_trois_mois);
    $('.choix_plage_date input.debut').change(function() { $(this).closest('.choix_plage_date').find('.fin').datepicker("option", "minDate", $(this).val()); });
    $('.choix_plage_date input.fin').change(function() { $(this).closest('.choix_plage_date').find('.debut').datepicker("option", "maxDate", $(this).val()); });
    
    $('input[name="decale_date"]').change(function(e)
    {
        date = $(this).val();
        
        if (date.match(/\d\d\/\d\d\/\d\d\d\d/))
            $(this).parent().children(".valider_decale_date").show();
        else
            $(this).parent().children(".valider_decale_date").hide();
    });
    
    $('.valider_decale_date').click(function(e)
    {
        session_id = $(".id.session").attr('data-id');
        new_date = $(this).parent().children('input[name="decale_date"]').val();
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'session_decale_date',
                'session_id': session_id,
                'new_date': new_date,
            },
            function(response)
            {
                param = JSON.parse(response);
                if (param.result == true && param.url !== undefined)
                    document.location.assign(param.url);
            }
        );
    });
    
    $(".add-date").click(add_new_date);
    function add_new_date(e)
    {
        e.preventDefault();
        parent = $(this).parent(); // TODO : à virer
        tableau = $(this).closest(".tableau-creneau");
        session_id = tableau.attr("data-sessionid");
        
        var fields = new Object();
        decaljour = $(this).attr("data-decaljour");
        if (undefined != decaljour)
        {
            // calcul du décalage en millisecondes
            decaljourMS = 1000 * 60 * 60 * 24 * decaljour;
            
            // récupération du bloc date + liste créneaux sous forme d'un objet jquery'
            dateBaseLine = $(this).closest(".liste-creneau");

            // récupération date actuelle
            dateBaseText = dateBaseLine.find("input.datepicker").val();
            // calcul de la nouvelle date
            dateArray = dateBaseText.split('/');
            newDateObj = new Date(dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0]);
            newDateObj.setTime(newDateObj.getTime() + decaljourMS);
            newDateText = newDateObj.toLocaleDateString('fr-FR');
            
            // vérification si il existe déjà une ligne avec cette date-là
            double_found = false;
            tableau.find(".datepicker").each(function()
            {
                if ($(this).val() == newDateText)
                {
                    double_found = true;
                    $(this).closest(".liste-creneau").addClass("bg-alerte").delay(700).queue(function(){ $(this).removeClass("bg-alerte").dequeue(); });
                }
            });
            
            // si la nouvelle date n'existe pas encore, on peut la créer'
            if (!double_found)
            {
                crenos = Array();
                dateBaseLine.find('.creneau').each(function() { crenos.push($(this).attr('data-id')); });
                params = 
                {
                    'session_id': $(".id.session").attr('data-id'),
                    'action': 'update_date',
                    'date': newDateText,
                    'creneaux': crenos,
                };
                jQuery.post
                ({
                    url: ajaxurl,
                    data: params,
                    success: function(response)
                    {
                        param = JSON.parse(response);
                        
                        if (param.html != undefined)
                        {
                            newDateLine = $(param.html);
                            postprocess_func['edit_date_add_events'](newDateLine);
                            postprocess_func['update_dates_subtitle']({ 'session_id': $(".id.session").attr('data-id')});
                            // si decaljour vaut 1 on ajoute la nouvelle ligne sous la ligne copiée
                            // sinon, on l'ajoute à la fin. TODO : faire un vrai tri des dates'
                            if (decaljour == 1)
                                dateBaseLine.after(newDateLine);
                            else
                                $(".empty-date").before(newDateLine);
                            
                            // on fait clignoter en couleur succes
                            newDateLine.addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                        }
                        //$(document).trigger('cpt.ready');
                    },
                });
            }
            
        }
        else
        {
            bouton = $(".empty-date");
            jQuery.post
            ({
                url: ajaxurl,
                data: { 'action': 'add_new_date_line', 'session_id': session_id, 'fields': fields },
                success: function(response)
                {
                    line = $(response);
                    postprocess_func['edit_date_add_events'](line);
                    postprocess_func['update_dates_subtitle']({ 'session_id': $(".id.session").attr('data-id')});
                    bouton.before(line);
                    datepick = line.find(".datepicker");
                    datepick.trigger('focus');
                },
            });
        }
    }
    
    // Enregistrement effectif d'une date
    function datepicker_close(date, datepicker)
    {
        date_line = $(this).closest('.liste-creneau');
        old_date = date_line.attr('data-date');
//        old_date = date_line.find("input[name$='_date']").val();
        
        if (date.match(/\d\d\/\d\d\/\d\d\d\d/))
        {
            date_line.children('.bouton').show();
            if (date != old_date)
            {
                params = 
                {
                    'session_id': $(".id.session").attr('data-id'),
                    'action': 'update_date',
                    'date': date,
                    'old_date': old_date,
                };
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        date_line.attr("data-date", date);
                        date_line.children('.icone-bouton').show();
                        
                        args = Object();
                        args.session_id = $(".id.session").attr('data-id');
                        postprocess_func['update_dates_subtitle'](args);
                        jQuery.post
                        (
                            ajaxurl,
                            {
                                'action': 'get_week_day',
                                'date': date,
                            },
                            function(response)
                            {
                                date_line.find("span.week_day").replaceWith($(response));
                            }
                        )
                    },
                );
            }
        }
        else
        {
            if (date != old_date)
            {
                date_line.find('.del-date').trigger('click');
            }
            date_line.remove();
        }
    }
    
    // Suppression d'une date
    $(".del-date").click(del_date);
    function del_date()
    {
        date_line = $(this).closest('.liste-creneau');
        params =
        {
            'session_id': $(".id.session").attr('data-id'),
            'action': 'del_date_creneau',
            'date': date_line.find('.datepicker').val(),
        };
        
        jQuery.post
        (
            ajaxurl,
            params,
            function (response)
            {
                param = JSON.parse(response);
                //console.log(param.log);
                date_line.remove();
                postprocess_func['update_dates_subtitle']({ 'session_id': $(".id.session").attr('data-id')});
                
                // mise à jour pour infos session
                /*
                arg = Object();
                arg.session_id = $(".id.session").attr('data-id');
                postprocess_func['update_pour_infos_session'](arg);
                */
            },
        );
    }
    
    // Suppression effective d'un créneau
    $(".del-creneau").click(del_creneau);
    function del_creneau()
    {
        creno_div = $(this).closest(".creneau");
        date_line = $(this).closest('.liste-creneau');
        params =
        {
            'session_id': $(".id.session").attr('data-id'),
            'action': 'del_date_creneau',
            'date': date_line.find('.datepicker').val(),
            'creneau': creno_div.attr('data-id'),
        };
        
        jQuery.post
        (
            ajaxurl,
            params,
            function (response)
            {
                param = JSON.parse(response);
                //console.log(param.log);
                creno_div.remove();
                postprocess_func['tabs_reload'](Array());
                // mise à jour pour infos session
                /*
                arg = Object();
                arg.session_id = $(".id.session").attr('data-id');
                postprocess_func['update_pour_infos_session'](arg);
                */
            },
        );
    }
    

$("input.input_jpost_value").focus(function(e)
{
    $(this).removeClass('bord-succes');
});

$(".editable").click(toggle_edit_data);
$(".editable a").click(stop_propagation);
$(".editable .icone").click(stop_propagation);
$(".editable .icone-bouton").click(stop_propagation);

$(".input_jpost input[type!='button']").click(stop_propagation);
$(".input_jpost select").click(stop_propagation);
$(".select_jpost select").click(stop_propagation);
$(".input_jpost textarea").click(stop_propagation);

$(".input_jpost input[type!='button']").change(change_jpost_value);
//$(".input_jpost input[type='hidden'].media").change(change_jpost_value);
$(".input_jpost select").change(change_jpost_value);
$(".select_jpost select").change(change_jpost_value);
$(".input_jpost textarea").change(change_jpost_value);

function toggle_edit_data(e)
{
    toggle_on = true;
    if ($(this).hasClass('edit-data'))
        toggle_on = false;
    $(".editable.edit-data").removeClass("edit-data");
    if (toggle_on)
        $(this).addClass('edit-data');
}

$("textarea").on("keydown", null, stop_propagation);

function propagate_editable_click(e)
{
    e.preventDefault();
    $(this).closest(".editable").trigger("click");
}

$('.lieu.details input[name="lieu_ville"]').change(function(e)
{
    args = Object();
    args.session_id = $(".id.session").attr('data-id');
    postprocess_func['update_session_titre'](args);
});

$('.calcul-cumul').click(function(e)
{
    calcul_cumul_heures_stagiaires($(this).attr('data-parent'));
});

$('.icone-bouton.set-acompte').click(function(e)
{
    e.preventDefault();
    
    tarif_total = $(this).closest('.financement-block').find('input[name="tarif_total_chiffre"]').val();
    acompte_input = $(this).closest('.input_jpost').find('input[name="acompte"]')
    acompte_input.val((tarif_total * $(this).attr('data-acompte') / 100));
    acompte_input.trigger('change');
});

$('.switch-detail-tarif-peda .bouton').click(function(e)
{
    e.preventDefault();

    $(this).closest(".financement-block").find('.detail-peda').show();
    $(this).closest(".switch-detail-tarif-peda").hide();
});

$('.icone-bouton.move-client').click(function(e)
{
    e.preventDefault();
    data =
    {
        'client_id': $(this).attr('data-clientid'),
        'session_source_id': $(this).attr('data-sessionid'),
        'action': 'copie_ou_deplace_client',
    };
    
    jQuery.post
    ({
        url: ajaxurl,
        data: data,
        success: function(response)
        {
            console.log(response);
        }
    });
});

$('.icone-bouton.refresh-datatable').click(function(e)
{
    e.preventDefault();
    $("table.dataTable").DataTable().draw();
});

$('.icone-bouton.statut_stagiaire_client').click(function(e)
{
    e.preventDefault();
    e.stopPropagation();
    client_id = $(this).closest('tr').attr('data-clientid');
    statut = $(this).closest('td').find('select[name="statut_stagiaire"]').val();
    
    console.log($(this));
    console.log(statut);
    $('tr.client' + client_id).each(function(e)
    {
        console.log($(this).attr('id'));
        $(this).find('select[name="statut_stagiaire"]').val(statut);
        $(this).find('select[name="statut_stagiaire"]').trigger('change');
    });
});

$(".attention.completion").click(function(e)
{
    e.preventDefault();
    parent = $(this).closest('tr');
    contexte = parent.attr('data-context');
    if (undefined == contexte)
    {
        parent = $(this).parent();
        contexte = parent.attr('data-context');
    }
    id = parent.attr('id').replace(contexte, '');
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'show_missing_data',
            'id': id,
            'contexte': contexte,
        },
        function (response)
        {
            show_message(response);
        }
    );
});

$(".dynamic-dialog").click(dynamic_dialog);

$('.aide_toggle_edit').click(aide_toggle_edit);
$('.aide_reset').click(aide_reset);

// Supprimer un texte d'aide dans le JSON de référence
$(".delete.aide").click(function(e)
{
    slug = $(this).attr('data-slug');
    parent = $(this).closest('div.aide.objet');
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'delete_aide',
            'slug': slug,
        },
        function(response)
        {
            if (response != undefined)
            {
                parent.remove();
                console.log(response);
            }
            else
                console.log("Rien ne s'est passé");
        }
    );
});

dialog_function = 
{
};

$('.add_as_stagiaire').click(function(e)
{
    e.preventDefault();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'add_stagiaire',
            'source': $(this).attr('data-source'),
            'session_id': $(".id.session").attr('data-id'),
            'client_id': $(this).closest(".board-client").attr('data-id'),
        },
        function(response)
        {
            param = JSON.parse(response);
            show_message(param.message);
            click_to_reload();
        },
    );
});

$('.programme-session').click(function(e)
{
    e.preventDefault();
    bouton = $(this);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'add_new_session',
            'formation': bouton.attr('data-id'),
            'session_unique_titre': '',
        },
        function(response)
        {
            param = JSON.parse(response);
            
            if (param.url != undefined)
                document.location.assign(param.url);
        }
    );
});

$(".delete-entity").click(delete_entity);
function delete_entity(e)
{
    e.preventDefault();
    
    id = $(this).attr('data-id');
    if ($(this).attr('data-sessionid') != undefined)
        session_id = $(this).attr('data-sessionid');
    else
        session_id = $(".id.session").attr('data-id');
    object_class = $(this).attr('data-objectclass')
    reload = $(this).attr('data-reload');
    parent_to_kill = $(this).attr('data-parent');
    
    yesno = $("<div>La suppression est irréversible !</div>");
    yesno.dialog(
    {
        autoOpen: true,
        title: "Êtes-vous sûr ?",
        modal: true,
        classes: { 'ui-dialog': 'dialog-remove' },
        buttons:
        {
            "Oui": function()
            {
                jQuery.post
                ({
                    url: ajaxurl,
                    data: { 'action': 'delete_entity', 'object_class': object_class, 'id': id, 'session_id': session_id },
                    success: function (response)
                    {
                        param = JSON.parse(response);
                        
                        if (param.succes != undefined)
                        {
                            $(parent_to_kill).remove();
                            if (param.url !== undefined)
                                document.location.assign(param.url);
                            if (reload === true || reload == 1)
                                document.location.reload();
                            else if (reload !== false && reload !== undefined)
                                document.location.assign(reload);
                        }
                    },
                });
                yesno.dialog("destroy");
            },
            "Non": function() { supprim = false; yesno.dialog("destroy");},
        },
        close: function()
        {
            supprim = false; yesno.dialog("destroy");
        }
    });
    yesno.dialog("open");
}

/* Réponse à questionnaire */
$('.questionnaire .reponse-submit').click(function(e)
{
    e.preventDefault();
    form = $('.questionnaire form.formq');
    data = new FormData(form[0]);
    data.set('action', 'questionnaire_enregistrer_reponse');
    checkbox = Object();
    form.find('input[type="checkbox"]:checked').each(function()
    {
        inputname = $(this).attr('name');
        if (inputname !== undefined)
        {
            if (checkbox[inputname] === undefined)
                checkbox[inputname] = [];
            checkbox[inputname].push($(this).val());
        }
    });

    for (const [key, value] of Object.entries(checkbox)) {
        data.set(key, JSON.stringify(value));
    }

    jQuery.post({
        url: ajaxurl,
        data: data,
        processData: false,
        contentType: false,
        success: function (response)
        {
            param = JSON.parse(response);
            if (param.message_fin !== undefined)
            {
                dialog_box = $('<div>' + param.message_fin + '</div>');
                dialog_box.dialog({
                    title: 'Confirmation',
                    buttons: { "Fermer": function() { dialog_box.dialog("destroy"); } },
                });
            }
        },
        error: function(xhr, textStatus, error)
        {
            console.log(xhr);
            console.log(textStatus);
            console.log(error);
        }
    });
});

$('.questionnaire_block .questionnaire.action').click(function(e)
{
    e.preventDefault();
    bouton = $(this);
    session_id = $(".id.session").attr('data-id');
    questionnaire_id = bouton.closest('.questionnaire_block').attr('data-id');
    switch($(this).attr('data-function'))
    {
        case "sendmail":
            if ($(this).hasClass('bg-alerte'))
            {
                jQuery.post
                (
                    ajaxurl,
                    {
                        'session_id': session_id,
                        'action': 'compose_missing_mail_stagiaires',
                    },
                    function(response)
                    {
                        confirm = $('<div>' + response + '</div>');
                        confirm.dialog(
                        {
                            autoOpen: true,
                            title: "Alerte !",
                            width: 600,
                            modal: true,
                            buttons:
                            {
                                "Oui": function()
                                {
                                    bouton.removeClass('bg-alerte');
                                    bouton.click();
                                    confirm.dialog("destroy");
                                },
                                "Non": function() { confirm.dialog("destroy");},
                            },
                            close: function() { confirm.dialog("destroy"); }
                        });
                        confirm.dialog("open");
                    }
                );
            }
            else
            {
                jQuery.post
                (
                    ajaxurl,
                    {
                        'session_id': session_id,
                        'questionnaire_id': questionnaire_id,
                        'subject': bouton.attr('data-subject'),
                        'content': bouton.attr('data-content'),
                        'action': 'sendmail_to_stagiaires'
                    },
                    function(response)
                    {
                        param = JSON.parse(response);
                        show_message(param.message);
                    }
                );
            }
            break;
    }
});

/* Quiz fonctions */
$(".quiz-sortable").sortable({ stop: change_quiz_order, });
$(".quiz-sortable").disableSelection();

$(".quiz-sortable li input[type='text']").change(quiz_change_value);
function quiz_change_value(e)
{
    li = $(this).closest("li");
    if (li.attr('data-new') != undefined)
        li.removeAttr('data-new');
    $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
}

function change_quiz_order(e, ui)
{
    $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
}

$(".quiz-enregistrer").click(function (e)
{
    bouton = $(this);
    parent_id = $(".id").attr('data-id');
    quiz_id = $(this).closest('.quiz').attr('data-id');
    sujet = $(this).closest('.quiz').attr('data-sujet');
    questions = Array();
    sortable = $(this).closest('.quiz').find("ul.quiz-sortable");
    sortable.children().each(function()
    {
        q = Object();
        q.type = $(this).attr('data-type');
        q.text = $(this).find("input[type='text']").val();
        questions.push(q);
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'quiz_enregistrer',
            'quiz_id': quiz_id,
            'sujet': sujet,
            'parent_id': parent_id,
            'questions': questions,
        },
        function (response)
        {
            param = JSON.parse(response);
            console.log(param.log);
            bouton.removeClass("enregistrement-requis");
            bouton.closest('.quiz').attr('data-id', param.quiz_id);
        }
    );
});

// Ajouter une ligne
$(".quiz-ajouter").click(function (e)
{
    e.preventDefault();
    
    type = $(this).attr('data-type');
    list = $(this).closest('.quiz').find("ul.quiz-sortable");
    quiz_id = $(this).closest('.quiz').attr('data-id');
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'quiz_add_line',
            'type': type,
            'quiz_id': quiz_id,
        },
        function(response)
        {
            param = JSON.parse(response);
            if (param.log != undefined)
                console.log(param.log);
            if (param.html != undefined)
            {
                list.append(param.html);
                list.on('click', '.quiz-supprimer', quiz_supprimer);
                list.on('change', 'input[type="text"]', quiz_change_value);
            }
        },
    );
});

// Supprimer un ligne
$(".quiz-supprimer").click(quiz_supprimer);
function quiz_supprimer(e)
{
    e.preventDefault();
    
    li = $(this).closest("li");
    if (li.attr('data-new') == undefined)
        $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
    li.remove();
}

$("div.exe_comptable input").change(change_exe_comptable);
function change_exe_comptable(e)
{
    e.preventDefault();
    infosession = $(this).closest(".infosession");
    div_exe_comptable = $(this).parent();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_exe_comptable',
            'session_id': infosession.attr("data-sessionid"),
            'stagiaire_id': infosession.attr("data-stagiaireid"),
            'annee': $(this).attr('name'),
            'tarif': $(this).val(),
        },
        function(response)
        {
            param = JSON.parse(response);
            infosession.find(".message").html(param.message);
            new_div = $(param.inputs);
            new_div.children('input').on('change', '', change_exe_comptable);
            div_exe_comptable.replaceWith(new_div);
        }
    );
}

// (Dés)Active le mode fullscreen d'un bloc
$(".fullscreen-mode").click(fullscreen_mode);
function fullscreen_mode(e)
{
    bloc = $($(this).attr('data-id'));
    if (bloc.hasClass("fullscreen"))
    {
        bloc.removeClass("fullscreen");
        $(this).removeClass("fait");
        $("#wpadminbar").removeClass("blocHidden");
        $("footer").removeClass("blocHidden");
        bloc.appendTo($("div.entry-content"));
        tmp.remove();
    }
    else
    {
        bloc.addClass("fullscreen");
        $(this).addClass("fait");
        $("#wpadminbar").addClass("blocHidden");
        $("footer").addClass("blocHidden");
        tmp = $("<div id='tmp'></div>");
        bloc.appendTo(tmp);
        tmp.prependTo($('body.pilote'));
        //bloc.html("");
    }
}

// (Dés)Active le mode édition d'un bloc
$(".edition-mode").click(edition_mode);
function edition_mode(e)
{
    bloc = $($(this).attr('data-id'));
    if (bloc.hasClass("edit-data"))
    {
        bloc.removeClass("edit-data");
        $(this).removeClass("fait");
    }
    else
    {
        bloc.addClass("edit-data");
        $(this).addClass("fait");
    }
}

// (Dés)Active le mode édition d'un bloc
$(".filtre .toggle").click(toggle_filtre);
function toggle_filtre(e)
{
    e.preventDefault();
    
    target = $($(this).attr('data-target'));
    toggle_element = target.find($($(this).attr('data-toggle')));
    if ($(this).hasClass('fait'))
    {
        toggle_element.addClass('blocHidden');
        $(this).removeClass('fait');
    }
    else
    {
        toggle_element.removeClass('blocHidden');
        $(this).addClass('fait');
    }
}

$('#profile-box').mouseover(function()
{
    $(this).css('z-index', 20);
});

$('#profile-box').mouseout(function()
{
    $(this).css('z-index', '');
});

/* Fonctions pour le pilote de sessions */
$('.toggle-stagiaires').click(function(e)
{
    $('#' + $(this).attr('data-id')).toggle();
});

$('.affiche_verif_dialog').click(function(e)
{
    e.preventDefault();
    
    verif_dailog = $('.verif_dialog');
    verif_dailog.find('textarea[name="message"]').text(verif_dailog.find('textarea[name="message"]').text().replace(/(<([^>]+)>)/gi, ""));
    verif_dailog.dialog
    ({
        autoOpen: true,
        title: "Rapports d'erreurs",
        height: 600,
        width: 1200,
        modal: false,
        buttons:
        {
            "Fermer": function()
            {
                verif_dailog.dialog("destroy");
            }
        },
        close: function()
        {
            verif_dailog.dialog("destroy");
        }
    });
});

$(".voir_rapport").click(function(e)
{
    e.preventDefault();
    
    if ($(this).attr('data-id') == "all")
        $(".rapport_csv").toggle();
    else
        $("#" + $(this).attr('data-id')).toggle();
});

$(".get_rapport").click(function(e)
{
    e.preventDefault();
    
    force_download($("#csv_global").text(), "rapport_global.csv", "text/csv");
});

$("span.copy-clipboard").click(copy_content);

$(".envoi_rapport").click(function(e)
{
    e.preventDefault();
    
    parent = $(this).closest('.verif_dialog');
    bouton = $(this);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'envoi_rapport',
            'sujet': parent.find('input[name="sujet"]').val(),
            'expediteur': parent.find('select[name="expediteur"]').val(),
            'message': parent.find('textarea[name="message"]').val(),
            'csv': parent.find(".rapport_csv").html(),
            'destinataire': bouton.attr('data-dest'),
        },
        function(response)
        {
            $("#message" + bouton.attr('data-dest')).html(response);
        }
    );
});

$(".highlight-element").click(highlight_element);

$(".select_by_date select[name='annee_comptable']").change(switch_reference_year);
function switch_reference_year(e)
{
    e.preventDefault();
    form = $(this).closest('.select_by_date');
    year = $(this).val();
    if (year > 2000)
    {
        form.find('input[name="date_debut"]').val('01/01/' + year);
        form.find('input[name="date_fin"]').val('31/12/' + year);
    }
    else
    {
        first_year = form.find('input[name="annee1"]').val();
        last_year = new Date().getFullYear() + 1;
        form.find('input[name="date_debut"]').val('01/01/' + first_year);
        form.find('input[name="date_fin"]').val('31/12/' + last_year);
    }
}


// Plage de dates
$(".select_by_date .appliquer-plage").click(function(e)
{
    e.preventDefault();
    
    parent = $(this).closest(".select_by_date");
    filtered_class = parent.attr('data-class');
    ref_date = parent.find("input[name='ref_date_filter']:checked").val();
    no_date = (parent.find("input[name='no_date_filter']").is(":checked")) ? 1 : 0;
    plage = {'date_debut': parent.find("input[name='date_debut']").val(), 'date_fin': parent.find("input[name='date_fin']").val() };
    //callback = $(this).attr('data-action'); 
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'set_date_range_filter',
            'plage': plage,
            'ref_date': ref_date,
            'no_date_filter': no_date,
        },
        function(response)
        {
            document.location.reload();
        }
    );
    
    /*
    if (callback !== undefined)
        jQuery.post
        (
            ajaxurl,
            {
                'action': callback,
                'plage': plage,
            },
            function(response)
            {
                param = JSON.parse(response);
                $("#" + param.div_id).replaceWith(param.html);
            }
        );
    */

});

$('.qrcode.action').click(function(e)
{
    e.preventDefault();

    switch($(this).attr('data-function'))
    {
        case "show":
            jQuery.post
            (
                ajaxurl,
                {
                    'action': 'qrcode_show',
                    'text': $(this).attr('data-text'),
                },
                function(response)
                {
                    div = $('<div class="fullscreen">' + response + '</div>');
                    $('body').append(div);
                    div.on('click', '', function (e) { $(this).remove(); })
                }
            );
    }
});

$("form .edit-opaga-content").click(function(e)
{
    e.preventDefault();

    form = $(this).closest("form")[0];
    formData = new FormData(form);
    formData.append('action', 'edit_opaga_content');
    $(this).closest("form").find("textarea.wp-editor-area").each(function()
    {
        formData.set('text', tinyMCE.get('text').getContent().replace("'", "’"));
    });
    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response)
        {
            param = JSON.parse(response);
            if (param.message !== undefined)
                show_message(param.message);
        },
    });
});

$(".close-info").click(info_action);
function info_action()
{
    content_id = $(this).attr("data-id");
    jQuery.post
    (
        ajaxurl,
        {
            action: 'update_info_state',
            content_id: content_id,
        },
        function(response)
        {
            param = JSON.parse(response);
            if (param.erreur !== undefined)
                show_message(param);
            else {
                console.log(param.res);
                $("#info-"+param.id).remove();
            }
        }
    );
}
$("div[role='info'] .unfold").hover(unfold_info, unfold_info);
$("div[role='info'] .unfold").click(unfold_info);
function unfold_info() {
    const to_unfold = $(this).parent().children().children("div[role='description']")
    if (to_unfold.hasClass("line-clamp-2")) {
        to_unfold.removeClass("line-clamp-2")
    }
    else {
        to_unfold.addClass("line-clamp-2")
    }
}

postprocess_func =
{
    'toggle_class': function(params)
        {
            if (params.ppselector == undefined || params.pptoggle == undefined)
                return;
            $(params.ppselector).toggleClass(params.pptoggle);
        },
    'edit_creneau_add_events': function(o)
        {
            o.off('click', '.dynamic-dialog', dynamic_dialog).on('click', '.dynamic-dialog', dynamic_dialog);
            o.off('click', '.del-creneau', del_creneau).on('click', '.del-creneau', del_creneau);
            postprocess_func['tabs_reload'](Array());
            //console.log(o);
            jQuery.post
            (
                ajaxurl,
                {
                    'action': 'check_duree_max',
                    'session_id': $(".id.session").attr('data-id'),
                },
                function(response)
                {
                    param = JSON.parse(response);
                    if (param.erreur !== undefined)
                        show_message(param);
                }
            );
        },
    'edit_date_add_events': function(o)
        {
            o.on('click', '.del-date', del_date);
            o.on('focus', '.datepicker', function() { jQuery(this).datepicker(datepicker_options); });
            o.on('click', '.add-date', add_new_date);
            //o.find(".add-date").hide();
            postprocess_func['edit_creneau_add_events'](o);
        },
    'update_client_nom': function(params)
        {
            if (params.client_id == undefined) return;
            jQuery('a[href="#tab-c' + params.client_id + '"]').html(params.value);
        },
    'update_pour_infos_session': function(params)
        {
            params.object_class = 'SessionFormation';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos_client': function(params)
        {
            params.object_class = 'Client';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos_stagiaire': function(params)
        {
            params.object_class = 'SessionStagiaire';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos': function(params)
        {
            if (params == null) return;
            params.action = 'update_pour_infos';
            
            jQuery.post
            (
                ajaxurl,
                params,
                function (response)
                {
                    param = JSON.parse(response);
                    if (param.html != undefined)
                        jQuery(param.parent).find('.infos-content').html(param.html);
                    else
                        console.log(param.erreur);
                },
            );
        },
    'update_session_titre': function(params)
        {
            if (params.session_id != undefined)
            {
                params.action = 'update_session_titre';
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.titre != undefined)
                            jQuery("article h1:first-child").text(param.titre);
                        if (param.log != undefined)
                            console.log(param.log);
                    }
                );
            }
        },
    'update_dates_subtitle': function(params)
        {
            if (params.session_id != undefined)
            {
                params.action = 'session_get_dates_texte';
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.dates != undefined)
                            jQuery(".opaga_head .dates").html(param.dates);
                        if (param.log != undefined)
                            console.log(param.log);
                    }
                );
            }
        },
    'toggle_lieu_details': function(params)
        {
            if (params.meta_value != undefined)
            {
                params.action = 'update_lieu_details';
                params.session_id = jQuery(".id.session").attr('data-id');
                
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.html != undefined)
                            jQuery("." + params.meta_key + ".details").replaceWith(param.html);
                        if (param.log != undefined)
                            console.log(param.log);
                    },
                );
            }
        },
    'toggle_opco': function(params)
        {
            if (params.meta_key == "financement")
            {
                div = jQuery(".input_jpost.toggle-opco");
                if (["mutu8"].indexOf(params.meta_value) != -1)
                    div.show();
                else
                    div.hide();
            }
        },
    'toggle_bloc': function(params)
        {
            console.log(params);
            jQuery("." + params.ppclass + ".show-when").hide();
            jQuery("." + params.ppclass + ".show-when." + params.value).show();
        },
    'tabs_reload': function(params)
        {
            jQuery(".ui-tabs ul").attr('data-reload', 1);
        },
    'manage_random_stagiaire': function(params)
        {
            console.log(params);
            if (params.client_id != undefined)
            {
                params.action = 'manage_random_stagiaire';
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.log != undefined)
                            console.log(param.log);
                        if (param.html != undefined)
                        {
                            liste_stagiaires = jQuery(param.html);
                            liste_stagiaires.on('change', '.input_jpost input', change_jpost_value);
                            liste_stagiaires.on('change', '.input_jpost select', change_jpost_value);
                            jQuery('tr#c' + params.client_id + ' td').html(liste_stagiaires);
                        }
                    }
                );
            }
        },
    'page-reload': function(params)
        {
            document.location.reload();
        },
    'update_data': function(params)
        {
            console.log(params);
            if (params.object_class != undefined)
            {
                params.action = 'update_data';
                params.meta_key = params.ppkey;
                params.meta_value = params.ppvalue;
                
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.log != undefined)
                            console.log(param.log);
                        $(params.ppdest).html(params.ppvalue);
                    }
                )
            }
        },
    'update_tarif': function(params)
        {
            if (params.ppmodif != undefined)
            {
                params.action = 'formation_update_tarif',
                
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        to_update = JSON.parse(param.update_form);
                        for (field in to_update)
                        {
                            $("input[name='" + field + "']").val(to_update[field]);
                            $("input[name='" + field + "']").closest('td').addClass("bg-succes").delay(1400).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                        }
                    },
                );
            }
        },
    'update_client_tarif': function(params)
        {
            console.log(params);
            input = $('input[name="' + params.meta_key + '"]');

            parent = input.closest('.financement-block');
            console.log(parent);
            console.log(parent.find('input.input_jpost_value'));
            data = new FormData();
            parent.find('input.input_jpost_value').each(function(e)
            {
                data.append($(this).attr('name'), $(this).val());
            });
            data.append('action', 'check_client_tarif');
            jQuery.post({
                url: ajaxurl,
                data: data,
                processData: false,
                contentType: false,
                success: function (response)
                {
                    parent.find('div.input_jpost').removeClass('bg-alerte');
                    parent.find('.icone-info').removeClass('bg-alerte');
                    console.log(response);
                    param = JSON.parse(response);
                    if (param.alerte.length > 0)
                        param.alerte.forEach(function(val, key)
                        {
                            parent.find('div.input_jpost.' + val).addClass('bg-alerte');
                            parent.find('.icone-info').addClass('bg-alerte');
                        });
                }
            });
        },
    'update_keyword_default_text': function(params)
        {
            if (params.has('key') && params.has('contenu'))
            {
                input = $('input[name="' + params.get('key') + '"]');
                input.val(params.get('contenu'));
                td_parent = input.closest('td.defaut');
                td_parent.find('.dynamic-dialog').attr('data-contenu', params.get('contenu'));
                td_parent.find('.previsu').html(params.get('contenu').substring(0, 150) + " …");
                td_parent.find('.previsu').html(td_parent.find('.previsu').text());
            }
        },
    'check_my_formation': function(params)
        {
            params.action = 'check_my_formation';
            jQuery.post
            (
                ajaxurl,
                params,
                function(response)
                {
                    param = JSON.parse(response);
                    if (param.my_formation == 0)
                        show_message(param.message);
                }
            );
        },
};

    select2_settings = {
        placeholder: 'Cliquez pour choisir',
        width: '100%',
        language: 'fr',
    };
    $('select[multiple]').select2(select2_settings);
    $('select.single-select2').select2(select2_settings);
    $('.single-select2 select').select2(select2_settings);
});

// Fin du jQuery(document).ready

var month = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];

// Fonction pour forcer un téléchargement
function force_download(data, filename, mimetype)
{
    let link = document.createElement('a');
    link.download = filename;
    let blob = new Blob([data], {type: mimetype});
    link.href = URL.createObjectURL(blob);
    link.click();
    URL.revokeObjectURL(link.href);
    link.remove();
}

function append_message(html)
{
    jQuery("#opaga-message").append(html);
}

function clear_message()
{
    jQuery("#opaga-message .message").html("");
}

function show_message(response)
{
    param = new Object();
    try
    {  
        param = JSON.parse(response);
    }
    catch (e)
    {  
        if (response instanceof Object)
            param = response;
        else
            jQuery("#opaga-message").append("<div class='message'>" + response + "</div>");
    }
    if (param.erreur !== undefined && param.erreur != "")
        jQuery("#opaga-message").append("<div class='erreur message'>" + param.erreur + "</div>");
    if (param.succes !== undefined && param.succes != "")
        jQuery("#opaga-message").append("<div class='succes message'>" + param.succes + "</div>");
    
    if (jQuery("#opaga-message .message").text().length > 0)
    {
        jQuery("#opaga-message").show(400);
    }
}

function change_jpost_value(e)
{
    e.preventDefault();
    
    
    input = jQuery(this);
    if (e.target.editorContainer !== undefined)
    {
        meta_key = input.attr('id');
        local_parent = jQuery(".editor." + meta_key);
    }
    else
    {
        meta_key = input.attr('name');
        local_parent = input.closest(".input_jpost");
        if (input.attr('type') == 'number' && input.val() == "")
            input.val(0);
    }
    
    if (!local_parent.hasClass('input_jpost'))
        return;
    
    global_parent = local_parent.closest("tr");
    
    nodeName = local_parent.attr("data-type").toLowerCase();
    object_class = local_parent.find("input[name='object_class']").val();
    session_id = local_parent.find("input[name='session_id']").val();
    formation_id = local_parent.find("input[name='formation_id']").val();
    user_id = local_parent.find("input[name='user_id']").val();
    type = input.attr('type');
    contexte = local_parent.find("input[name='contexte']").val();
    contexte_id = local_parent.find("input[name='contexte_id']").val();
    client_id = local_parent.find("input[name='client_id']").val();
    stagiaire_id = local_parent.find("input[name='stagiaire_id']").val();
    stat_id = local_parent.find("input[name='stat_id']").val();
    type_doc = local_parent.find("input[name='type_doc']").val();
    valid_icon = local_parent.find(".valid");
    display_valeur = local_parent.find(".valeur");
    postprocess = local_parent.find("input[name='postprocess']").val();
    postprocess_args = Object();
    local_parent.find("input.pparg").each(function()
    {
        postprocess_args[jQuery(this).attr('name')] = jQuery(this).val();
    });
    
    if (nodeName === "editor")
    {
        value = tinyMCE.get(meta_key).getContent().replaceAll(/"”*/g, '"').replaceAll(/”*"/g, '"');
    }
    else
    {
        if (input.attr('type') == 'number')
        {
            if (input.attr('max') !== undefined && input.val() > input.attr('max'))
                input.val(input.attr('max'));
            if (input.attr('min') !== undefined && input.val() < input.attr('min'))
                input.val(input.attr('min'));
        }
        value = input.val();
    }
    
    // special checkbox
    if (type == "checkbox")
    {
        if (input.is(":checked"))
            value = 1;
        else
            value = 0;
    }
    
    jQuery.post
    ({
        url: ajaxurl,
        data: {
            'action': 'update_jpost_value',
            'object_class': object_class,
            'user_id': user_id,
            'session_id': session_id,
            'formation_id': formation_id,
            'client_id': client_id,
            'stagiaire_id': stagiaire_id,
            'stat_id': stat_id,
            'type': type,
            'contexte': contexte,
            'contexte_id': contexte_id,
            'type_doc': type_doc,
            'meta_key': meta_key,
            'value': value,
            'postprocess': postprocess,
            'nodeName': nodeName,
        },
        success: function(response)
        {
            param = JSON.parse(response);
            valid_icon.addClass("visible fa-beat").delay(10000).queue( function() { jQuery(this).removeClass("visible fa-beat").dequeue() });
            display_valeur.text(param.valeur);
            if (nodeName == "select" || (nodeName != "editor" && input.val() != param.key))
            {
                input.val(param.key);
                value = param.key;
            }
            
            if (postprocess != undefined)
            {
                if (param.postprocess_args != undefined)
                    jQuery.extend(postprocess_args, param.postprocess_args);
                
                postprocess_args.meta_value = value;
                postprocess_args.meta_key = meta_key;
                
                // parcours de tableau sauce Javascript plutôt que jQuery
                postprocess.split('+').forEach(function(item)
                {
                    postprocess_func[item](postprocess_args);
                });
            }
            
            if (global_parent)
            {
                if (stagiaire_id > 0)
                {
                    session_tr = jQuery("#session" + session_id);
                    jQuery.each(param.session_formation, function(key, value)
                    {
                        session_tr.find("." + key).html(value);
                        session_tr.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                        //session_tr.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        session_tr.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                    });
                    jQuery.each(param.session_stagiaire, function(key, value)
                    {
                        global_parent.find("." + key).html(value);
                        global_parent.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                        //global_parent.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        global_parent.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                    });
                }
                else
                {
                    jQuery.each(param.session_formation, function(key, value)
                    {
                        global_parent.find("." + key).html(value);
                        // global_parent.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        global_parent.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                        global_parent.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                    });
                }
            }
            
        },
        error: function()
        {
            jQuery.post
            (
                ajaxurl,
                {
                    'action': 'check_connected',
                },
                function(response)
                {
                    if (response == 0)
                    {
                        jQuery.post
                        (
                            ajaxurl,
                            {
                                'action': 'get_reconnect_dialog',
                            },
                            function(response)
                            {
                                param = JSON.parse(response);
                                infodialog = jQuery(param.content);
                                infodialog.dialog({
                                    autoOpen: true,
                                    title: param.title,
                                    height: 'auto',
                                    width: 'auto',
                                    modal: false,
                                    close: function()
                                    {
                                        infodialog.dialog("destroy");
                                    }
                                });
                            }
                        );
                    }
                }
            );
        },
    });
}


function dynamic_dialog(e)
{
    e.preventDefault();
    e.stopPropagation();
    bouton = jQuery(this);
    
    session_id = bouton.attr('data-sessionid');
    if (session_id == undefined)
        session_id = jQuery(".id.session").attr("data-id");

    fonction = bouton.attr('data-function');
    aide_slug = bouton.attr('data-help');
    
    if (bouton.attr('data-submit-txt') != undefined)
        btn_valider_txt = bouton.attr('data-submit-txt');
    else
        btn_valider_txt = "Valider";

    if (bouton.attr('data-close-txt') != undefined)
        btn_fermer_txt = bouton.attr('data-close-txt');
    else
        btn_fermer_txt = "Fermer";

    const [node] = bouton;
    const attrs = {};
    jQuery.each(node.attributes, function(index, attribute)
    {
        if (attribute.name.substring(0, 4) == "data")
            attrs[attribute.name.substring(5)] = attribute.value;
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'the_dynamic_dialog',
            'function': fonction,
            'session_id': session_id,
            'formation_id': bouton.attr('data-formationid'),
            'client_id': bouton.attr('data-clientid'),
            'stagiaire_id': bouton.attr('data-stagiaireid'),
            'creno_id': bouton.attr('data-crenoid'),
            'aide_slug': aide_slug,
            'date': bouton.closest('.liste-creneau').attr('data-date'),
            'data': attrs,
        },
        function(response)
        {
            let dialog_box = jQuery(response);
            dialog_box.dialog
            ({
                autoOpen: true,
                minWidth: 400,
                width: 'auto',
                modal: true,
                classes: {
                    "ui-dialog": "ui-dialog-" + fonction
                },
                create: function( event, ui ) {
                    jQuery('select[multiple]').select2({
                        placeholder: 'Choisir des formateur.rices',
                        width: '100%'
                    });
                },
                buttons:
                {
                    btn_valider_txt : {
                        text: btn_valider_txt,
                        class: "valider",
                        disabled: (jQuery(response).find('input[name="no_valid"').length > 0),
                        click: function()
                        {
                            let dialog_form = jQuery(this);
                            form = dialog_form.find("form")[0];
                            formData = new FormData(form);
                            dialog_form.find('select').each(function()
                            {
                                select_multiple = jQuery(this);
                                name = select_multiple.attr('name');
                                values = Array();
                                select_multiple.find("option:selected").each(function()
                                {
                                    values.push(select_multiple.val());
                                });
                                formData.set(name, values);
                            });
                            dialog_form.find("textarea.wp-editor-area").each(function()
                            {
                                name = jQuery(this).attr('id');
                                formData.set(name, tinyMCE.get(name).getContent().replace("'", "’"));
                            });
                            
                            if (formData.has('action'))
                            {
                                jQuery.post
                                ({
                                    url: ajaxurl,
                                    data: formData,
                                    processData: false,
                                    contentType: false,
                                    success: function (response)
                                    {
                                        param = JSON.parse(response);
                                        
                                        if (param.url !== undefined)
                                        {
                                            if (param.newtab !== undefined)
                                            {
                                                tab_opened = window.open(param.url, "_blank");
                                                if (tab_opened === null || tab_opened === undefined)
                                                {
                                                    link = jQuery('<a href="' + param.url + '" target="_blank">Votre navigateur bloque l\'ouverture de la nouvelle fenêtre, cliquez ici pour y accéder</a>');
                                                    dialog_form.append(link);
                                                    link.click();
                                                }
                                            }
                                            else
                                                document.location.assign(param.url);
                                        }
                                        
                                        if (param.erreur != undefined)
                                            dialog_form.children(".message").html("<span class='erreur'>" + param.erreur + "</span>");
                                        else if (param.succes != undefined)
                                            dialog_form.children(".message").html("<span class='succes'>" + param.succes + "</span>");
                                        
                                        if (formData.get('valid_once') !== null && param.erreur === undefined)
                                        {
                                            var buttons = dialog_form.dialog("option", "buttons");
                                            delete buttons.btn_valider_txt;
                                            dialog_form.dialog("option", "buttons", buttons);
                                        }
                                        
                                        if (param.html != undefined && param.html != "")
                                        {
                                            bloc_html = jQuery(param.html);
                                            if (param.eventsfunc != undefined)
                                                postprocess_func[param.eventsfunc](bloc_html);
                                                
                                            if (param.prependto != undefined)
                                            {
                                                prependto = bouton.closest(param.prependto);
                                                bloc_html.prependTo(jQuery(prependto));
                                            }

                                            if (param.appendto != undefined)
                                            {
                                                appendto = bouton.closest(param.appendto);
                                                bloc_html.appendTo(jQuery(appendto));
                                            }
                                            
                                            if (param.replace != undefined)
                                            {
                                                replace = bouton.closest(param.replace);
                                                jQuery(replace).html(bloc_html);
                                            }
                                            
                                            if (param.replacewith != undefined)
                                            {
                                                replace = bouton.closest(param.replacewith);
                                                jQuery(replace).replaceWith(bloc_html);
                                            }
                                        }
                                    },
                                    error: function (response)
                                    {
                                        console.log(response);
                                        console.log("Erreur sur action " + formData.get('action'));
                                    },
                                });
                            }
                                
                            if (formData.has('eventsfunc'))
                            {
                                postprocess_func[formData.get('eventsfunc')](formData);
                            }
                            if (formData.has('close_on_valid'))
                            {
                                dialog_form.dialog("close");
                                if (formData.has('reload_on_valid'))
                                    if (formData.get('reload_on_valid') == true)
                                        document.location.reload(true);
                                    else
                                        document.location.assign(formData.get('reload_on_valid'));
                            }
                            else if (formData.has('reset_on_success'))
                            {
                                dialog_form.find("form input[type!='hidden']").val('');
                                dialog_form.find("form textarea").val('');
                                dialog_form.find("form select[multiple]").val('').trigger('change');
                                if (formData.has('focus_to'))
                                    jQuery("#" + formData.get('focus_to')).focus();
                            }
                        },
                    },
                    btn_fermer_txt : {
                        text: btn_fermer_txt,
                        class: "close",
                        click: function()
                        {
                            jQuery(this).dialog("close");
                        }
                    },
                },
                close:function()
                {
                    let close_box = jQuery(this);
                    formData = new FormData(close_box.find("form")[0]);
                    close_box.find("textarea.wp-editor-area").each(function()
                    {
                        tinyMCE.execCommand("mceRemoveEditor", false, jQuery(this).attr('id'));
                    });
                    close_box.dialog("destroy");
                    if (formData != undefined && formData.has('reload_on_close'))
                        if (formData.get('reload_on_close') == true)
                            document.location.reload(true);
                        else
                            document.location.assign(formData.get('reload_on_close'));
                },
            });
            
            // pour permettre d'ouvrir une autre fenêtre de dialogue depuis celle-ci
            dialog_box.on('click', '.dynamic-dialog', dynamic_dialog);
            dialog_box.on('click', '.openButton', toggle_bloc_hidden);

            dialog_box.find('.close').on('click', '', function(e) {
                dialog_box.dialog("close");
            });
            
            jQuery("textarea").on("keydown", null, stop_propagation);

            // spécial pour les dialogues d'aide en ligne
            if (dialog_box.find('textarea.wp-editor-area') != undefined)
            {
                tinyMCE.init(tinymce_options);
                dialog_box.on('click', '.aide_toggle_edit', aide_toggle_edit);
                dialog_box.on('click', '.aide_reset', aide_reset);
                dialog_box.find('.insert-date').on('click', '', insert_date);
                dialog_box.find('.del-content').on('click', '', del_editor_content);
                dialog_box.ready(function()
                {
                    dialog_box.find('textarea.wp-editor-area').each(function()
                    {
                        tinyMCE.execCommand("mceAddEditor", false, jQuery(this).attr('id'));
                    });
                });
            }
            
            // action spéciales, événements à rajouter
            dialog_box.find("input[name='callback']").each(function()
            {
                //console.log(jQuery(this).val());
                dialog_box.on(jQuery(this).attr('data-event'), jQuery(this).attr('data-support'), dialog_function[jQuery(this).val()]);
            });
        }
    );
}

function activate_select2()
{
    jQuery(this).select2({
        placeholder: 'Cliquez pour choisir',
        width: '100%',
    });
}

function aide_toggle_edit(e)
{
    e.preventDefault();
    
    parent = jQuery(this).closest("form");
    
    parent.find(".aide_edit").toggle();
    parent.find(".aide_show").toggle();
    
    parent.find(".aide_show h2").text(parent.find("input[name='titre']").val());
    parent.find(".aide_show div").html(tinyMCE.get("texte").getContent());
}

function aide_reset(e)
{
    e.preventDefault();
    
    parent = jQuery(this).closest("form");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'get_aide_defaut',
            'slug': parent.find("input[name='slug']").val(),
        },
        function (response)
        {
            param = JSON.parse(response);
            parent.find(".aide_show h2").text(param.titre);
            tinyMCE.get("texte").setContent(param.texte);
        }
    );
}

function insert_date(e)
{
    e.preventDefault();
    editor_id = jQuery(this).attr("data-content");
    
    editor = tinyMCE.get(editor_id);
    text_date = new Intl.DateTimeFormat('fr-FR', { dateStyle: 'medium', timeStyle: 'short' }).format(new Date());
    editor.execCommand('mceInsertContent', false, '<strong>' + text_date + '</strong> : \uFEFF');
}

function notification_update(message_add_text = "")
{
    jQuery.post
    ({
        url: ajaxurl,
        data: {
            'action': 'update_notification_icone',
        },
        async: false,
        success: function(response)
        {
            param = JSON.parse(response);
            console.log(param);
            icon = jQuery(param.notif);
            text = icon.find('#notifications_text');
            if (text.text().length > 1)
            {
                console.log(text.html());
                text.find('.close').remove();
                clear_message();
                show_message(text.html() + '<p>' + message_add_text + '</p>');
            }
            icon.find('.notif-icon').on('click', null, toggle_bloc_hidden);
            icon.find('#notifications_text .notif-action').on('click', null, notification_action);
            jQuery('#opaga-message .notif-action').on('click', '', notification_action);
            jQuery('#notifications').replaceWith(icon);
            jQuery('#notifications .close').on('click', '', function() { jQuery(this).parent().addClass('blocHidden'); });
        }
    });
}

function notification_action(e)
{
    e.preventDefault();
    command = jQuery(this).attr('data-action');
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'notification_action',
            'command': command,
        },
        function(response)
        {
            param = JSON.parse(response);
            if (param.message !== undefined)
                show_message(param.message);
            notification_update();
        }
    );
}

// Fonction pour ouvrir ou fermer un bloc
// Le bouton doit avoir pour data-id l'id du bloc à gérer
function toggle_bloc_hidden(e)
{
    e.preventDefault();
    id = "#"+jQuery(this).attr("data-id");
    jQuery(id).toggleClass('blocHidden');
    jQuery(this).toggleClass('open');
}

function toggle_button(e)
{
    button = jQuery(this);
    if (button.attr('data-keep') !== undefined)
        jQuery(button.attr('data-keep')).show();
    if (button.hasClass('on'))
    {
        button.removeClass('on');
        button.addClass('off');
        jQuery(button.attr('data-parent') + " " + button.attr('data-hide')).hide();
    }
    else
    {
        button.removeClass('off');
        button.addClass('on');
        jQuery(button.attr('data-parent') + " " + button.attr('data-hide')).show();
    }
}

function calcul_cumul_heures_stagiaires(parent)
{
    if (parent === undefined)
        parent = 'body';
    
    parent = jQuery(parent);
    
    input_cumul = parent.find("input[name='nb_heures_stagiaires']");
    input_cumul.val(parent.find("input[name='nb_stagiaires']").val() * parent.find("input[name='nb_heure_estime_decimal']").val());
    input_cumul.trigger('change');
}

function click_to_reload()
{
    body = jQuery('body');
    body.append('<div class="click_to_reload"></div>');
    body.on('click', '.click_to_reload', do_reload);
}

function do_reload()
{
    document.location.reload();
}

function do_shortcuts(e)
{
    if (e.keyCode == jQuery.ui.keyCode.ESCAPE)
        jQuery(this).find(".close").trigger("click");
    if (e.keyCode == jQuery.ui.keyCode.ENTER)
        jQuery(this).find(".valider").trigger("click");
}

function stop_propagation(e)
{
    e.stopPropagation();
}

function highlight_element(e)
{
    e.preventDefault();
    e.stopPropagation();
    element = jQuery(jQuery(this).attr('data-id'));
    element.addClass("highlight-bg").delay(1000).queue(function(){ jQuery(this).removeClass("highlight-bg").dequeue(); });
}

async function copy_content(e)
{
    e.preventDefault();
    e.stopPropagation();
    src = jQuery(this);
    if (jQuery(this).attr('data-src') !== undefined)
        value = jQuery(this).attr('data-src');
    else if (jQuery(this).attr('data-html') !== undefined)
        value = src.innerHTML();
    else
        value = src.text();
    
    try
    {
        await navigator.clipboard.writeText(value);
        jQuery(this).find('.info').addClass('fa-bounce succes').delay(2000).queue(function(){ jQuery(this).removeClass("fa-bounce succes").dequeue(); })
    }
    catch (err)
    {
        alert(err + "\n\nNe fonctionne qu'en HTTPS !");
    }
}

function confirm_delete(callback_func)
{
    yesno = jQuery("<div>La suppression est irréversible !</div>");
    yesno.dialog(
    {
        autoOpen: true,
        title: "Êtes-vous sûr ?",
        modal: true,
        classes: { 'ui-dialog': 'dialog-remove' },
        buttons:
        {
            "Oui": function()
            {
                yesno.dialog("destroy");
                callback_func(true);
            },
            "Non": function()
            {
                yesno.dialog("destroy")
                callback_func(false);
            },
        },
        close: function()
        {
            yesno.dialog("destroy");
            callback_func(false);
        }
    });
    yesno.dialog("open");
}

function del_editor_content(e)
{
    e.preventDefault();
    editor_id = jQuery(this).attr("data-content");
    
    if (jQuery('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id))
    {
        tinyMCE.get(editor_id).setContent("");
        if (jQuery(this).closest(".notif-modif") != undefined)
            jQuery(this).closest(".notif-modif").find(".input_jpost").addClass("modif");
    }
    else
    {
        jQuery('#'+textarea_id).val("");
    }
}

var tinymce_options =
{
    selector: "textarea.opaga_editor",
    language: "fr",
    spellchecker_languages: "French=fr",
    paste_as_text: true,
    menubar: false,
    plugins: 'charmap compat3x fullscreen image lists paste textcolor wpautoresize wpeditimage wpgallery wptextpattern colorpicker directionality hr link media tabfocus wordpress wpdialogs wpemoji wplink wpview',
    toolbar: "formatselect styleselect table | bold italic | bullist numlist | blockquote alignleft aligncenter alignright | link unlink | pastetext removeformat charmap | outdent indent | undo redo spellchecker searchreplace",
    quickbars_insert_toolbar: false,
};
