function edit_locked(editor, node)
{
    var $ = jQuery;
    node = $(node);

    if (node.hasClass('custom_keyword'))
        edit_custom_keyword(editor, node);
    else if (node.hasClass('keyword'))
        edit_keyword(editor, node);
    else if (node.hasClass('test'))
        edit_test_condition(editor, node);
}

function edit_custom_keyword(editor, node)
{
    var $ = jQuery;
    node = $(node);

    editor.windowManager.open({
        title: "Définir un mot-clé personnalisé",
        width : 400,
        height : 250,
        classes: "keyword",
        html :
            '<div class="edit-keyword" style="padding: 5px 15px;">' +
            '<p>Donnez un nom à ce nouveau mot-clé puis enregistrer le modèle pour paraméter le mot-clé</p>' +
            'Nom <input type="text" name="kwparam" value="" />' +
            '</div>',
        buttons: [
            {
                text: 'Valider',
                onclick: function()
                {
                    //some code here that modifies the selected node in TinyMCE
                    if ($('input[name="kwparam"]').val() != "")
                        node.html("document:" + $('input[name="kwparam"]').val());
                    editor.windowManager.close();
                }
            },
            {
                text: 'Annuler',
                onclick: 'close'
            }
        ]
    });
}

function edit_keyword(editor, node)
{
    var $ = jQuery;
    node = $(node);
    
    params = node.html().split(':');
    tag = params[0] + ":" + params[1];
    keyword_data = null;
    jQuery.post
    ({
        url: ajaxurl,
        data: {
            'tag' : tag,
            'action' : 'get_keyword_data',
        },
        success: function(response)
        {
            keyword_data = JSON.parse(response);
            if (keyword_data !== null)
            {
                kwparam = (params[2] !== undefined) ? params[2] : "";
                editor.windowManager.open({
                    title: "Modifier le mot-clé " + params[0] + ":" + params[1],
                    width : 400,
                    height : 250,
                    classes: "keyword",
                    html :
                        '<div class="edit-keyword" style="padding: 5px 15px;">' +
                        ((keyword_data['desc'] !== undefined) ? '<p>' + keyword_data['desc'] + '</p>' : "") +
                        'Paramètre <input type="text" name="kwparam" value="' + kwparam + '" />' +
                        '</div>',
                    buttons: [
                        {
                            text: 'Valider',
                            onclick: function()
                            {
                                //some code here that modifies the selected node in TinyMCE
                                suffix = "";
                                if ($('input[name="kwparam"]').val() != "")
                                    suffix = ":" + $('input[name="kwparam"]').val();
                                node.html(tag + suffix);
                                editor.windowManager.close();
                            }
                        },
                        {
                            text: 'Annuler',
                            onclick: 'close'
                        }
                    ]
                });
            }
        }
    });

}

function check_test_structure(node)
{
    var $ = jQuery;
    tags_order = [ 'if', 'sign', 'then', 'else', 'fi' ];
    tags_real = [];
    node.children('span.test').each(function()
    {
        tags_real.push($(this).attr('data-kw'));
    });

    missing = tags_order.filter((element) => !tags_real.includes(element));
    valid = (JSON.stringify(tags_order) == JSON.stringify(tags_real));

    message = '';
    if (missing.length > 0)
        message = 'Il manque les mots-clés suivants : ' + missing.toString();
    if (missing.length == 0 && valid == false)
        message = 'L\'ordre des mots-clés est erroné';

    return { 'valid': valid, 'missing': missing, 'message': message };
}

function repair_test_structure(node)
{
    var $ = jQuery;
    tags_order = [ 'if', 'sign', 'then', 'else', 'fi' ];
    missing_tags = check_test_structure(node).missing;
    if (missing_tags.includes('fi'))
    {
        node.prepend($('<span class="test locked" data-kw="if" contenteditable="false"> si </span>'));
        console.log("Ajout tag if");
    }
    if (missing_tags.includes('sign'))
    {
        node.children('span[data-kw="if"]').after($('<span class="test locked" data-kw="sign" data-sign="egal" contenteditable="false"> égal </span>'));
        console.log("Ajout tag sign egal");
    }
    if (missing_tags.includes('then'))
    {
        node.children('span[data-kw="sign"]').after($('<span class="test locked" data-kw="then" contenteditable="false"> alors </span>'));
        console.log("Ajout tag alors");
    }
    if (missing_tags.includes('fi'))
    {
        node.append($('<span class="test locked" data-kw="fi" contenteditable="false"> fin test </span>'));
        console.log("Ajout tag fi");
    }
    if (missing_tags.includes('else'))
    {
        node.children('span[data-kw="fi"]').before($('<span class="test locked" data-kw="else" contenteditable="false"> sinon </span>'));
        console.log("Ajout tag sinon");
    }
    clean_test_expression(node);
}

function clean_test_expression(node)
{
    html_content = node.html();
    if (!html_content.startsWith('<span'))
    {
        regex = /^[^<]*/;
        to_move = html_content.match(regex)[0];
        node.html(html_content.replace(to_move, ""));
        if (to_move.trim() != "")
            node.before(to_move);
    }
    if (!html_content.endsWith('</span>'))
    {
        regex = /[^>]*$/;
        to_move = html_content.match(regex)[0];
        node.html(html_content.replace(to_move, ""));
        if (to_move.trim() != "")
            node.after(to_move);
    }
    check = check_test_structure(node);
    if (check.valid === false)
    {
        node.addClass('erreur');
        node.attr('title', check.message);
    }
    else
    {
        node.removeClass('erreur');
        node.attr('title', '');
    }
}

function edit_test_condition(editor, node)
{
    var $ = jQuery;
    node = $(node);
    sign_node = node.parent().children('span[data-kw="sign"]');
    test_nodes = node.parent().children('span.test');
    
    test_sign_data = null;
    jQuery.post
    ({
        url: ajaxurl,
        data: {
            'action' : 'get_test_sign_list',
        },
        success: function(response)
        {
            test_sign_data = JSON.parse(response);
            if (test_sign_data !== null)
            {
                test_sign_options = "";
                check_structure = check_test_structure(node.parent());
                $.each(test_sign_data, function(idx, val) { test_sign_options += '<option ' + ((idx == sign_node.attr('data-sign')) ? 'selected="selected"' : '')  + 'value="' + idx + '">' + val.text + '</option>'; });
            
                editor.windowManager.open({
                    title: "Modifier l'opérateur de test",
                    width : 400,
                    height : 250,
                    classes: "test",
                    html :
                        '<div class="edit-test-sign" style="padding: 5px 15px;">' +
                        '<select name="test-sign" style="border: 1px solid #080;">' +
                        test_sign_options +
                        '</select>' +
                        '<p>' + check_structure.message + '</p>' +
                        '</div>',
                    buttons: [
                        {
                            text: 'Valider',
                            onclick: function()
                            {
                                repair_test_structure(node.parent());

                                selected = $('select[name="test-sign"] option:selected');
                                sign_node.html(selected.html());
                                sign_node.attr('data-sign', selected.val());

                                editor.windowManager.close();
                            }
                        },
                        {
                            text: 'Annuler',
                            onclick: 'close'
                        }
                    ]
                });
            }
        }
    });
}

// Fonction utilisée pour les wp_editor. Liée par callback dans les paramètres de wp_editor
function init_opaga_editor(editor)
{
    var $ = jQuery;
    editor.setContent(editor.getContent().replaceAll(/"”*/g, '"').replaceAll(/”*"/g, '"'));
    arrow_keys = ['ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown'];
    editor.on("change", function()
    {
        this.on("blur", change_jpost_value);
    });
    editor.on('click', function (e)
    {
        node = editor.selection.getNode();
        if ($(node).hasClass('locked'))
        {
            edit_locked(editor, node);
        }
    });
    editor.on('mouseup', function (e)
    {
    });
    editor.on('keyup', function (e)
    {
    });
    editor.on('keydown', function (e)
    {
        node = editor.selection.getNode();
        if ($(node).hasClass('locked'))
        {
            if (e.key == ' ')
            {
                e.preventDefault();
                edit_locked(editor, node);
            }

            if (!arrow_keys.includes(e.code))
                e.preventDefault();
        }

        // empêcher d'écrire du texte dans le span.test-expression hors des balises if et fi
        if ($(node).hasClass('test-expression') && !arrow_keys.includes(e.code))
        {
            clean_test_expression($(node));
        }
    });
    editor.on('mouseup', function (e)
    {
        node = editor.selection.getNode();
        if ($(node).hasClass('test-expression'))
        {
            clean_test_expression($(node));
        }
    });
    editor.on("ExecCommand", function(e)
    {
        if ([ 'bold', 'italic'].includes(e.value))
        {
            switch(e.value)
            {
                case 'bold':
                    tag = 'strong';
                    break;
                case 'italic':
                    tag = 'em';
                    break;
            }
            selection = editor.selection.getContent();
            if ($(selection).find(tag).length == 0)
                editor.selection.setContent('<' + tag + '>' + selection + '</' + tag + '>');
            else
            {
                selection = selection.replace('</' + tag + '>', '');
                selection = selection.replace('</span><' + tag + '>', '</span>');
                editor.selection.setContent(selection);
            }
        }
    });
    editor.on("SetContent", change_jpost_value);
}
function setup_opaga_editor(editor)
{
    const editor_models = ['content', 'wpof_pdf_header', 'wpof_pdf_footer'];
    if (editor_models.includes(editor.id))
    {
        editor.addButton('opagamenu',
        {
            type: 'menubutton',
            text: 'OPAGA',
            icon: false,
            menu:
            [
                { 
                    text: '2 Signatures',
                    onclick: function() { editor.insertContent('<table class="signature"><tr><td>' + editor.selection.getContent() + '</td><td>signature 2</td></table>'); } 
                },
                {
                    text: '3 Signatures',
                    onclick: function() { editor.insertContent('<table class="signature"><tr><td>' + editor.selection.getContent() + '</td><td>signature 2</td><td>signature 3</td></table>'); }
                },
                { text: 'Note de bas de page', onclick: function() { editor.insertContent('<div class="footnote">' + editor.selection.getContent() + '</div>'); } }
            ]
        });
    }
}
