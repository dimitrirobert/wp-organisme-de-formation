jQuery(document).ready(function($)
{
    $(".maj-wpof").click(function(e)
    {
        e.preventDefault();
        log = $("#log");
        bouton = $(this);
        
        jQuery.post
        (
            ajaxurl,
            {
                'action' : bouton.attr('data-action'),
                'param' : bouton.attr('data-param'),
            },
            function(response)
            {
                p = JSON.parse(response);
                log.html(p.log);
            },
        );
    });
    
    $(".insert_in_editor").click(function(e)
    {
        e.preventDefault();
        
        classes = [];
        supclass = $(this).attr("data-class");
        if (supclass !== undefined)
            classes.push(supclass);
        
        classes.push('locked');

        editor = tinyMCE.get($(this).attr('data-editor'));
        if ($(this).attr('data-testsign') === undefined)
        {
            classes.push('keyword');
            editor.execCommand('mceInsertContent', false, ' <span class="' + classes.join(' ') + '">' + $(this).text() + '</span> ');
        }
        else
        {
            classes.push('test');
            editor.execCommand('mceInsertContent', false,
                '<span class="test-expression">' +
                '<span class="' + classes.join(' ') + '" data-kw="if"> si </span>' +
                ' operande1 ' +
                '<span class="' + classes.join(' ') + '" data-kw="sign" data-sign="' + $(this).attr('data-sign') + '">' + $(this).attr('title') + '</span>' +
                (($(this).attr('data-testsign') != "") ? ' operande2 ' : "") +
                '<span class="' + classes.join(' ') + '" data-kw="then"> alors </span>' +
                ' texte_si_condition_vraie ' +
                '<span class="' + classes.join(' ') + '" data-kw="else"> sinon </span>' +
                ' texte_si_condition_fausse ' +
                '<span class="' + classes.join(' ') + '" data-kw="fi"> fin test </span>' +
                '</span>');
        }
    });
    
    $(".clean-modele").click(function(e)
    {
        e.preventDefault();
        
        editor = tinyMCE.get($(this).attr('data-editor'));
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'clean_modele',
                'content': editor.getContent().replace("'", "’"),
            },
            function(response)
            {
                if (response !== undefined)
                    editor.setContent(response);
            }
        );
    });
    
    $(".ckw_switch_type").change(function(e)
    {
        e.preventDefault();
        
        key = $(this).attr('data-key');
        parent = $("#"+key);
        type = $(this).val();
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'modele_custom_keywords_defaut',
                'key': key,
                'type': type,
                'defaut': '',
            },
            function(response)
            {
                new_line = $(response);
                parent.find('.defaut').html(new_line);
                parent.find('.dynamic-dialog').on('click', dynamic_dialog);
            }
        )
    });
    
    $(".ckw.delete").click(function(e)
    {
        tr = $(this).closest('tr');
        editor = tinyMCE.get('content');
        content = editor.getContent();
        keyword = tr.attr('id');
        keyword_complet = "document:" + keyword;
        
        keyword_in_text = editor.dom.select(".keyword:contains(" + keyword_complet + ")");
        if (keyword_in_text.length > 0)
        {
            tinywindow = editor.getWin();
            tinywindow.alert('Le mot-clé ' + keyword_complet + ' est présent ' + keyword_in_text.length + ' fois dans le texte.\nSupprimez toute occurence pour supprimer le mot-clé');
            tinywindow.getSelection().selectAllChildren(keyword_in_text[0]);
            keyword_in_text[0].scrollIntoView("center");
            editor.focus();
        }
        else
        {
            jQuery.post
            (
                ajaxurl,
                {
                    'action': "ckw_supprime",
                    'ckw': keyword,
                    'modele_id': $('#post_ID').val(),
                },
                function(response)
                {
                    param = JSON.parse(response);
                    if (param.erreur === undefined)
                        tr.remove();
                    else
                        alert('Problème de suppression du mot-clé ' + keyword_complet + ' : ' + param.erreur);
                }
            );
        }
    });
    
    $("table.predefini select").change(function(e)
    {
        dest_td = $(this).closest('tr').find('td.editor');
        if ($(this).val() == 'inactif')
            dest_td.hide();
        else
            dest_td.show();
    });
    
    selectplugin = "select[name='wpof_plugin']";
    $(selectplugin).ready(function(e)
    {
        showhideoptions()
    });
    $(selectplugin).change(function(e)
    {
        showhideoptions()
    });

    function showhideoptions() 
    {
        selected_ext = $(selectplugin).val();
        ext_id = '#' + selected_ext + '-options';
        $(ext_id).show();
        $('.wpof-plugin-options').not(ext_id).hide();
    }

    $(".color-picker").wpColorPicker({ palettes: false, defaultColor: $(this).val() });
    
});
