jQuery(document).ready(function($)
{
    $(".icone-bouton.qmanager, .action.qmanager").click(qmanager_dispatch);
    function qmanager_dispatch(e)
    {
        e.preventDefault();
        parent = $(this).closest('.questionnaire_row');
        switch($(this).attr('data-function'))
        {
            case 'add_questionnaire':
                jQuery.post
                (
                    ajaxurl,
                    {
                        'action': 'add_questionnaire',
                    },
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.update != false)
                            document.location.assign(location.href + '?questionnaire_id=' + param.id);
                    }
                );
                break;
            case 'copy_questionnaire':
                if (parent !== undefined)
                {
                    jQuery.post
                    (
                        ajaxurl,
                        {
                            'action': 'copy_questionnaire',
                            'id': parent.attr('data-id'),
                        },
                        function (response)
                        {
                            param = JSON.parse(response);
                            if (param.html !== undefined)
                            {
                                html = $(param.html);
                                parent.after(html);
                                html.on('click', '.action.qmanager', qmanager_dispatch);
                            }
                        }
                    );
                }
                break;
            case 'del_questionnaire':
                if (parent !== undefined)
                {
                    confirm_delete(function(ok)
                    {
                        if (ok)
                            jQuery.post
                            (
                                ajaxurl,
                                {
                                    'action': 'del_questionnaire',
                                    'id': parent.attr('data-id'),
                                },
                                function (response)
                                {
                                    param = JSON.parse(response);
                                    if (param.erreur !== undefined)
                                        show_message(param.erreur);
                                    else
                                        parent.remove();
                                }
                            );
                    });
                }
                break;
        }
    }

    function enable_enregistrement_requis()
    {
        $(".submit").addClass("enregistrement-requis");
    }

    $(".questionnaire-sortable").sortable({ stop: change_questionnaire_order, });
    $(".questionnaire-sortable").disableSelection();

    $(".questionnaire input, .questionnaire select").change(questionnaire_change_value);
    function questionnaire_change_value(e)
    {
        enable_enregistrement_requis();
    }

    function change_questionnaire_order(e, ui)
    {
        enable_enregistrement_requis();
    }


    $(".action.question").click(question_dispatch);

    function question_dispatch(e)
    {
        e.preventDefault();
        e.stopPropagation();

        element = $(this);

        switch(element.attr('data-function'))
        {
            case 'add_question':
                dest = $(this).closest('.add_question_manager').attr('data-dest');
                add_question($(this).attr('data-type'), dest);
                break;
            case 'toggle_options':
                toggle_options($(this));
                break;
            case 'toggle_needed':
                toggle_needed($(this));
                break;
            case 'question-retirer':
                question_retirer($(this));
                break;
            case 'question-supprimer':
                question_supprimer($(this));
                break;
            case 'question-dupliquer':
                question_dupliquer($(this).closest('.liq'));
                break;
            case 'add_choice':
                add_choice(element);
                break;
            case 'del_choice':
                element.closest('.choice').remove();
                break;
        }
    }
    // Ajouter une ligne
    function add_question(type, dest)
    {
        questionnaire_id = $("#tab-questionnaire-edit").attr('data-id');
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'questionnaire_add_question',
                'type': type,
                'questionnaire_id': questionnaire_id,
            },
            function(response)
            {
                param = JSON.parse(response);
                if (param.log != undefined)
                    console.log(param.log);
                if (param.html != undefined)
                {
                    list = $("#" + dest);
                    question = $(param.html);
                    list.append(question);
                    question.on('click', '.action', question_dispatch);
                    question.on('change', 'input[type="text"]', questionnaire_change_value);
                }
            },
        );
    }

    function add_choice(element)
    {
        jQuery.post
        (
            ajaxurl,
            { 'action': 'add_choice' },
            function(response)
            {
                param = JSON.parse(response);
                if (param.html !== undefined)
                {
                    new_choice = $(param.html);
                    element.before(new_choice);
                    new_choice.on('click', '.action.question', question_dispatch);
                    new_choice.on('change', 'input[type="text"]', questionnaire_change_value);
                }
            }
        );
    }

    // dupliquer la question
    function question_dupliquer(question_row)
    {
        new_question = question_row.clone();
        new_question.attr('data-new', Date.now());
        new_question.attr('data-id', -1);
        new_question.find('span[data-function="show_linked_questionnaire"]').remove();
        new_question.find('span[data-function="toggle_needed"]').removeClass("important").addClass("disabled");
        question_row.after(new_question);
        new_question.on('click', '.action', question_dispatch);
        new_question.on('change', 'input[type="text"]', questionnaire_change_value);
    }
    // Retirer une question
    function question_retirer(obj)
    {
        li = obj.closest("li");
        if (li.attr('data-new') == undefined)
            enable_enregistrement_requis();
        li.remove();
    }

    function question_supprimer(obj)
    {
        question_row = obj.closest('tr.questionrow');
        confirm_delete(function(ok)
        {
            if (ok)
                jQuery.post
                (
                    ajaxurl,
                    {
                        'action': 'delete_question',
                        'question_id': question_row.attr('data-id'),
                        'force': obj.attr('data-force'),
                    },
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.result == true)
                            question_row.remove();
                    }
                );
        });
    }

    $(".action.reponse").click(reponse_dispatch);
    function reponse_dispatch(e)
    {
        e.preventDefault();
        e.stopPropagation();

        element = $(this);
        parent = element.closest('.repondant');

        switch(element.attr('data-function'))
        {
            case 'delete-reponse':
                confirm_delete(function(ok)
                {
                    if (ok)
                        jQuery.post
                        (
                            ajaxurl,
                            {
                                'action': 'delete_reponse',
                                'repondeur_id': parent.attr('data-id'),
                                'repondeur_type': parent.attr('data-type'),
                                'session_id': parent.attr('data-sessionid'),
                                'questionnaire_id': parent.attr('data-questionnaireid'),
                            },
                            function(response)
                            {
                                param = JSON.parse(response);
                                console.log(param);
                                if (param.result == true)
                                    parent.remove();
                            }
                        );
                });
        };
    }
    // Afficher/cacher les options
    function toggle_options(obj)
    {
        option_bloc = obj.closest(".liq").find(".options");
        option_bloc.toggleClass('blocHidden');
        obj.toggleClass('open');
    }

    // Rendre obligatoire ou facultative une question dans un questionnaire donné
    function toggle_needed(obj)
    {
        input = obj.parent().find('input[name="needed"]');
        if (input.val() == true)
        {
            input.val(0);
            obj.removeClass('important').addClass('disabled');
        }
        else
        {
            input.val(1);
            obj.addClass('important').removeClass('disabled');
        }
        enable_enregistrement_requis();
    }

    $(".questionnaire-enregistrer").click(function (e)
    {
        bouton = $(this);
        questionnaire = $(this).closest('#tab-questionnaire-edit');
        questionnaire_id = questionnaire.attr('data-id');
        questions = Array();
        questions_needed = Object();
        sortable = questionnaire.find("ul.questionnaire-sortable");
        sortable.children().each(function() 
        {
            options = Object();
            $(this).find('.options input').each(function()
            {
                val = $(this).val().trim();
                if ($(this).attr('name').match(/\[\]/))
                {
                    if (val != "")
                    {
                        multi_name = $(this).attr('name').replace(/\[\]/, '');
                        if (options[multi_name] === undefined)
                            options[multi_name] = [];
                        options[multi_name].push(val);
                    }
                }
                else
                    options[$(this).attr('name')] = val;
            });
            questions.push
            ({
                id: $(this).attr('data-id'),
                tmp_id: $(this).attr('data-new'),
                qtype: $(this).attr('data-qtype'),
                title: $(this).find("input[name='title']").val(),
                needed: $(this).find('input[name="needed"]').val(),
                options: options,
            });
        });

        jQuery.post
        (
            ajaxurl,
            {
                'action': 'questionnaire_enregistrer',
                'questionnaire_id': questionnaire_id,
                'sujet': questionnaire.find('select[name="questionnaire_type"]').val(),
                'title': questionnaire.find('input[name="title"').val(),
                'nature_formation': questionnaire.find('select[name="nature_formation"]').val(),
                'modalite_session': questionnaire.find('select[name="modalite_session"]').val(),
                'questions': questions,
                'level': questionnaire.find('input[name="level"').val(),
            },
            function (response)
            {
                param = JSON.parse(response);
                console.log(param.log);
                if (param.new_id !== undefined)
                {
                    console.log(param.new_id);
                    for (const [tmp_id, new_id] of Object.entries(param.new_id))
                    {
                        question = $('li[data-new="' + tmp_id + '"]');
                        question.attr('data-id', new_id);
                        question.removeAttr('data-new');
                    }
                }
                bouton.removeClass("enregistrement-requis");
                bouton.closest('.questionnaire').attr('data-id', param.questionnaire_id);
            }
        );
    });

    $('.submit[data-function="save_stat"]').click(function(e)
    {
        form = $(this).closest('form.add_stat');
        formdata = new FormData($(this).closest('form.add_stat')[0]);
        erreur = false;
        if (formdata.get('title') == "")
        {
            window.alert('Vous devez définir un titre');
            erreur = true;
        }
        if (formdata.get('question_id') == undefined || formdata.get('question_id') == "")
        {
            window.alert('Vous devez choisir au moins une question');
            erreur = true;
        }

        if (!erreur)
        {
            formdata.append('action', 'save_stat');
            form.find('select').each(function()
            {
                formdata.set($(this).attr('name'), JSON.stringify(Object.assign({}, $(this).val())));
                console.log($(this).val());
            });
            jQuery.post
            ({
                url: ajaxurl,
                data: formdata,
                processData: false,
                contentType: false,
                success: function (response)
                {
                    document.location.reload();
                }
            });
        }
        console.log(formdata);
    });
});