<?php
/**
 * Plugin Name: OPAGA
 * Plugin URI: https://opaga.fr/
 * Description: Plugin permettant de gérer l'administration de la formation professionnelle continue selon la loi française. Gestion des inscriptions, création automatique des documents obligatoires, suivi des stagiaires, bilan pédagogique et financier.
 * Version: 0.1
 * Author: Dimitri Robert – CAE Coopaname
 * Author URI:  https://formation-logiciel-libre.com/
 * License: AGPL3 license
 * Depends: Ultimate Member
 *
 * Copyright 2018 Dimitri Robert <contact@opaga.fr>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

define( 'debug', get_option("wpof_debug") == 1 );
if (debug)
{
    error_reporting(E_ALL);
    ini_set('display_errors','On');
}
else
    ini_set('display_errors','Off');

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'WPOF_VERSION', '0.1' );
define( 'WPOF_WP_VERSION', get_bloginfo( 'version' ) );
define( 'wpof_url', plugin_dir_url(__FILE__ ));
define( 'wpof_path', plugin_dir_path(__FILE__ ));
define( 'opaga_custom_path', get_stylesheet_directory(). '/opaga');
define( 'dompdf_path', wpof_path . "/vendor/dompdf/dompdf" );

setlocale(LC_TIME, 'fr_FR.utf8','fra');

// Définition d'une timezone par défaut si pas définie dans la config de PHP
$timezone = ini_get('date.timezone');
if (empty($timezone))
    date_default_timezone_set("Europe/Paris");
else
    date_default_timezone_set($timezone);

require_once(wpof_path . "/wpof-first-init.php");

function activate_wpof()
{
    update_option('Activated_Plugin', 'OPAGA');
}
register_activation_hook(__FILE__, 'activate_wpof');

function load_plugin()
{
    if (is_admin() && get_option('Activated_Plugin') == 'OPAGA')
    {
        if (is_plugin_active('ultimate-member/ultimate-member.php'))
        {
            delete_option('Activated_Plugin');

            if (get_option("wpof_version") == "")
                first_init();
        }
    }
    if (get_option("opaga_maj_init") != 1)
        opaga_finish_init();
}
add_action( 'admin_init', 'load_plugin' );

function check_um_active_notice()
{
    if (is_admin() && !is_plugin_active('ultimate-member/ultimate-member.php'))
    {
         echo '<div class="notice notice-error">
             <p>Vous devez installer et activer le plugin <a href="'.get_admin_url().'plugin-install.php?s=ultimate+member&tab=search&type=term">Ultimate Member</a></p>
         </div>';
    }
}
add_action('admin_notices', 'check_um_active_notice');

define( 'WPOF_TABLE_SUFFIX_DOCUMENTS', 'wpof_documents');

setlocale(LC_ALL, 'fr_FR@utf8');

function wpof_load_scripts()
{
    global $post;
    global $wpof;

    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('jquery-ui-dialog');
    wp_enqueue_script('jquery-ui-tabs');
    wp_enqueue_script('jquery-ui-menu');
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('jquery-ui-slider');
    wp_enqueue_script('jquery-ui-checkboxradio');
    wp_enqueue_style('jquery-ui', wpof_url."css/jquery-ui.css");
    
    wp_enqueue_style( 'wpof', wpof_url."css/wpof.css" );
    wp_enqueue_style( 'wpof_public', wpof_url."css/public-pages.css" );
    
    wp_enqueue_style( 'tailwind', wpof_url."css/tailwind.css" );

    wp_enqueue_script( 'wpof', wpof_url."js/wpof.js", array('jquery') );
    wp_enqueue_script( 'wpof-token', wpof_url."js/wpof-token.js", array('jquery') );
    wp_enqueue_script( 'wpof-doc', wpof_url."js/wpof-doc.js", array('jquery') );
    if (is_admin())
    {
        wp_enqueue_style( 'wpof-admin', wpof_url."css/style-admin.css" );
        wp_enqueue_script( 'wpof-admin', wpof_url."js/wpof-admin.js", array('jquery') );
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'iris', admin_url( 'js/iris.min.js' ), array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ), false, 1 );
        wp_enqueue_script( 'wp-color-picker', admin_url('js/color-picker.min.js'), array('iris'), false, 1 );
    }
//    else if (get_option('wpof_theme') && $post && ($post->post_name == $wpof->url_acces || (is_user_logged_in() && ($post->post_type == "session" || ($post->post_type == "formation") && isset($_GET[$wpof->formation_edit_link_suffix]) || in_array($post->post_name, $wpof->no_theme)))))
    else if (get_option('wpof_theme') && $post && (is_user_logged_in() && in_array($post->post_name, $wpof->pages_responsable)))
    {
        global $wp_styles;
        
        foreach($wp_styles->registered as $style)
        {
            if (preg_match('@wp-content/themes@', $style->src))
                wp_dequeue_style($style->handle);
        }
            
        if (file_exists(get_stylesheet_directory()."/opaga-no-theme.css"))
            $no_theme_css = get_stylesheet_directory_uri()."/opaga-no-theme.css";
        else
            $no_theme_css = wpof_url."css/opaga-no-theme.css";
        wp_enqueue_style('opaga_no_theme', $no_theme_css);
    }
    
    wp_enqueue_script( 'wpof_cpt', wpof_url."cpt/cpt.js", array('jquery') );
    wp_enqueue_editor();
    wp_enqueue_script( 'wpof_editor', wpof_url."js/wp-editor.js", array('jquery') );
    
    if ( ! wp_style_is('fontawesome-css-6') )
    {
        wp_register_style('fontawesome-css-6', wpof_url . 'css/fontawesome-all.min.css');
        wp_enqueue_style('fontawesome-css-6');      
    }
    wp_enqueue_style('opaga_virtual_pages', wpof_url."css/virtual-pages.css");
    wp_enqueue_style('select2', wpof_url.'vendor/select2/select2/dist/css/select2.min.css' );
	wp_enqueue_script('select2', wpof_url.'vendor/select2/select2/dist/js/select2.min.js', array('jquery') );
	wp_enqueue_script('select2-fr', wpof_url.'vendor/select2/select2/dist/js/i18n/fr.js', array('select2') );
}
add_action( 'wp_enqueue_scripts', 'wpof_load_scripts', 21 );
add_action( 'admin_enqueue_scripts', 'wpof_load_scripts', 21 );

function opaga_load_datatables()
{
    wp_enqueue_style('datatables', "https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css");
    wp_enqueue_style('datatables-buttons', "https://cdn.datatables.net/buttons/2.3.6/css/buttons.dataTables.min.css");
    wp_enqueue_style('datatables-fixedColumns', "https://cdn.datatables.net/fixedcolumns/4.2.2/css/fixedColumns.dataTables.min.css");
    wp_enqueue_style('datatables-fixedHeader', "https://cdn.datatables.net/fixedheader/3.3.2/css/fixedHeader.dataTables.min.css");
//    wp_enqueue_style('datatables-select', "https://cdn.datatables.net/select/1.6.2/css/select.dataTables.min.css");
    wp_enqueue_style('datatables-searchBuilder', "https://cdn.datatables.net/searchbuilder/1.4.2/css/searchBuilder.dataTables.min.css");
    wp_enqueue_style('datatables-dateTime', "https://cdn.datatables.net/datetime/1.4.1/css/dataTables.dateTime.min.css");
//    wp_enqueue_style('datatables-searchPanes', "https://cdn.datatables.net/searchpanes/2.1.2/css/searchPanes.dataTables.min.css");
    wp_enqueue_style('datatables-stateRestore', "https://cdn.datatables.net/staterestore/1.2.2/css/stateRestore.dataTables.min.css");
 
    wp_enqueue_script('datatables', "https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js", array('jquery'));
    wp_enqueue_script('datatables-load', wpof_url.'js/datatables.js', array('datatables')); // localisation en français
    wp_enqueue_script('datatables-buttons', "https://cdn.datatables.net/buttons/2.3.6/js/dataTables.buttons.min.js", array('datatables'));
    wp_enqueue_script('datatables-buttons-colVis', "https://cdn.datatables.net/buttons/2.3.6/js/buttons.colVis.min.js", array('datatables-buttons'));
    wp_enqueue_script('datatables-buttons-html5', "https://cdn.datatables.net/buttons/2.3.6/js/buttons.html5.min.js", array('datatables-buttons'));
    wp_enqueue_script('datatables-buttons-print', "https://cdn.datatables.net/buttons/2.3.6/js/buttons.print.min.js", array('datatables-buttons'));
    wp_enqueue_script('datatables-fixedColumns', "https://cdn.datatables.net/fixedcolumns/4.2.2/js/dataTables.fixedColumns.min.js", array('datatables'));
    wp_enqueue_script('datatables-fixedHeader', "https://cdn.datatables.net/fixedheader/3.3.2/js/dataTables.fixedHeader.min.js", array('datatables'));
//    wp_enqueue_script('datatables-select', "https://cdn.datatables.net/select/1.6.2/js/dataTables.select.min.js", array('datatables'));
    wp_enqueue_script('datatables-searchBuilder', "https://cdn.datatables.net/searchbuilder/1.4.2/js/dataTables.searchBuilder.min.js", array('datatables'));
    wp_enqueue_script('datatables-dateTime', "https://cdn.datatables.net/datetime/1.4.1/js/dataTables.dateTime.min.js", array('datatables'));
//    wp_enqueue_script('datatables-searchPanes', "https://cdn.datatables.net/searchpanes/2.1.2/js/dataTables.searchPanes.min.js", array('datatables'));
    wp_enqueue_script('datatables-stateRestore', "https://cdn.datatables.net/staterestore/1.2.2/js/dataTables.stateRestore.min.js", array( 'datatables' ));
}

/**
 * Déclaration de la variable JS `ajaxurl` en frontend.
 */
function wpof_register_ajaxurl_var() {
    $ajaxurl = admin_url( 'admin-ajax.php' );
    $script = sprintf( 'var ajaxurl = %s;', json_encode( $ajaxurl ) );
    wp_add_inline_script( 'wpof', $script, 'before' );
}
add_action( 'wp_enqueue_scripts', 'wpof_register_ajaxurl_var', 22 );

function init_opaga_page()
{
    global $wpof, $wp_query;
    require_once(wpof_path . "/class/class-page-virtuelle.php");
    require_once(wpof_path . "/class/class-board-select-by-date.php");
    require_once(wpof_path . "/class/class-board-toggle-button.php");
    
    $wpof->init();    

    $wpof->uri_params = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
    
    // lorsque l'on arrive ici depuis un appel ajax.post, il faut charger le HTTP_REFERER dans wpof->uri_params pour recréer les conditions de la page virtuelle
    if (substr_count($wpof->uri_params, 'admin-ajax.php') > 0 && !empty($_SERVER['HTTP_REFERER']))
        $wpof->uri_params = trim(str_replace(home_url(), '', $_SERVER['HTTP_REFERER']), '/');
    
    $wpof->uri_params = preg_split('/[\/?]+/', $wpof->uri_params);
    $slug = array_shift($wpof->uri_params);
    $wpof->slug = $slug;
    
    add_action('wp_enqueue_scripts', 'opaga_load_datatables', 7);
    
    if (is_user_logged_in())
        update_user_meta(get_current_user_id(), "last_online", time());
    
    if (get_option("wpof_version") != "" && (is_user_logged_in() || in_array($slug, $wpof->pages_acces_token)))
    {
        if (in_array($slug, $wpof->pages_acces_token)) // suppression du nav_menu et du champ de recherche
        {
            add_filter('wp_nav_menu', function() { return ""; });
            add_filter('get_search_form', function() { return ""; });
        }

        if (in_array($slug, $wpof->no_theme))
        {
            global $virtual_pagename;
            require_once(wpof_path . "/class/class-page-virtuelle.php");
            require_once(wpof_path . "/wpof-responsable-fonctions.php");
            
            $virtual_pagename = array_search($slug, $wpof->no_theme);
            
            $page_file = wpof_path . '/pages/page-'.$virtual_pagename.'.php';
            if (!file_exists($page_file))
                throw new Exception("$page_file introuvable");
            
            require_once $page_file;
            $page_virtuelle = new PageVirtuelle(array('pagename' => $virtual_pagename, 'params' => $wpof->uri_params));
            $wp_query = new WP_Query(array('post_type' => 'virtual'));
        }
            
        if (get_option('wpof_theme')
            && (in_array($slug, $wpof->pages_responsable)))
        {
            add_filter('single_template', 'get_opaga_page_template');
            add_filter("page_template", 'get_opaga_page_template');
        }
    }
    else
    {
        if ($slug == $wpof->url_user && isset($wpof->uri_params[0]))
        {
            $user = get_user_by('login', $wpof->uri_params[0]);
            if ($user)
            {
                $formateur = new Formateur($user->ID);
                if ($formateur->profil_public == 1)
                {
                    add_filter('the_content', array($formateur, 'vue_publique'));
                    add_filter('archive_template', 'get_default_page_template');
                    require_once wpof_path . '/pages/page-user.php';
                    $page_virtuelle = new PageVirtuelle(array('pagename' => 'user', 'params' => $wpof->uri_params, 'title' => $formateur->get_displayname()));
                    $wp_query = new WP_Query(array('post_type' => 'virtual'));
                }
            }
        }
    }
    add_image_size( 'formateur_photo', 250, 250, true );
}
add_action('init', 'init_opaga_page', 2);

function get_opaga_page_template($template)
{
    global $wpof, $post;
    if (is_user_logged_in() || $post->post_name == $wpof->url_acces)
    {
        $template = wpof_path . '/template/no-theme/page.php';
    }
    
    return $template;
}

function get_default_page_template($template)
{
    return get_stylesheet_directory() . '/page.php';
}

function rewrite_opaga_url()
{
    global $wpof;
    // pages spéciales
    foreach($wpof->no_theme as $slug)
        add_rewrite_rule($slug.'/([^/]+)/?$', 'index.php?post_type=page&$matches[1]', 'top');
    add_rewrite_rule('session/([^/]+)(/[^/]+)*/?$', 'index.php?session=$matches[1]&$matches[2]', 'top');
}
add_action('init', 'rewrite_opaga_url');

function wpof_session_start()
{
    if (!session_id())
        @session_start();
    // le préfixe @ permet d'ignorer les erreurs envoyés par la fonction, par exemple, si la configuration PHP ne supporte pas les sessions
}
add_action( 'init', 'wpof_session_start', 1 );


function wpof_query_post_type( $query )
{
    global $wpof;
    
    if ( is_archive() && $query->is_main_query() && isset($query->query['post_type'])) {
        $post_type = $query->query['post_type'];
        if ($post_type == 'session') 
        {
            $query->query_vars['meta_query'] = array(
                'relation' => 'AND',
                'acces_clause' => array(
                    'key' => 'acces_session', 
                    'value' => 'public', 
                    'compare' => '='
                ),
                'fdate_clause' => array(
                    'key' => 'first_date_timestamp',
                    'compare' => 'EXISTS',
                    //'value' => date_create()->modify('-2 days')->format('U'),
                    //'compare' => '>',
                ),
            );
            $query->query_vars['orderby'] = array( 'fdate_clause' => 'DESC' );
        }
        // On ne modifie pas la query si shortcode
        if ($post_type == 'formation' && empty($query->query_vars['opaga_shortcode'])) 
        {
            $formations_id = $wpof->get_actions_id("formation");
            $formations_id = $wpof->filter_id_by_value($formations_id, "formation", "acces_public", 1, "=");
            $formations_id = $wpof->filter_id_by_value($formations_id, "formation", "brouillon", 1, "=", true);
            
            /*
            $query->query_vars['meta_query'] = array(
                'relation' => 'AND',
                'acces_clause' => array(
                    'key' => 'acces_public', 
                    'value' => 1, 
                    'compare' => '='
                ),
            );*/
            $query->query_vars['orderby'] = 'rand';
            $query->query_vars['post__in'] = $formations_id;
        }
    }
    return $query;
}
add_action( 'pre_get_posts', 'wpof_query_post_type' );

add_filter( 'the_author_posts_link', function ( $link )
{
    global $post, $wpof;
    if ($post->post_type === 'session' || $post->post_type === 'formation')
    {
        $formateurs = get_post_meta($post->ID, "formateur", true);
        $link = [];
        foreach ($formateurs as $form_id)
        {
            $formateur = get_formateur_by_id($form_id);
            $display_name = $formateur->get_displayname();
            if ($formateur->profil_public == 1)
            {
                $link[] = sprintf(
                    '<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
                    esc_url($formateur->permalink),
                    esc_attr( sprintf( __( 'Voir le profil de %s', 'coopaga' ), $display_name ) ),
                    $display_name
                );
            }
            elseif ($formateur->profil_public == 1)
                $link[] = $display_name;
        }
        if (!empty($link)) {
            return implode(', ',  $link);
        }
        else
            return "";
    }
    return $link;
}, 10, 1 );

require_once(wpof_path . "/wpof-config.php");
require_once(wpof_path . "/wpof-fonctions.php");
require_once(wpof_path . "/wpof-custom-post-types.php");
require_once(wpof_path . "/wpof-session-formation.php");
require_once(wpof_path . "/wpof-utilisateur.php");
require_once(wpof_path . "/wpof-formation.php");
require_once(wpof_path . "/wpof-messages.php");
require_once(wpof_path . "/wpof-dialog.php");
require_once(wpof_path . "/class/class-stat.php");

// classes
require_once(wpof_path . "/class/class-formation.php");
require_once(wpof_path . "/class/class-aide.php");
require_once(wpof_path . "/class/class-formateur.php");
require_once(wpof_path . "/class/class-message.php");

// TODO : vérifier si on ne pourrait se contenter d'appeler ces deux ressources que dans la page-export.php
require_once(wpof_path . "/class/class-schema-export.php");
require_once wpof_path . '/pages/page-export.php';

// Expéditeur des mails
function new_mail_from_name($old)
{
    return get_bloginfo('name');
}
add_filter('wp_mail_from_name', 'new_mail_from_name');
function new_mail_from_email($old)
{
    return get_bloginfo('admin_email');
}
add_filter('wp_mail_from', 'new_mail_from_email');

function opaga_add_to_menu($array, $args)
{
    global $wpof;
    if ($args->theme_location == $wpof->main_menu)
    {
        if (is_user_logged_in())
        {
            $array = '<li class="opaga-board menu-item"><a href="'.home_url().'/'.$wpof->url_user.'/">'.__("Tableau de bord").'</a></li>'.$array;
            $array .= '<li class="opaga-board menu-item connect"><a href="'.home_url().'/logout/">'.__("Déconnexion").'</a></li>';
            if ("admin" == wpof_get_role(get_current_user_id()))
                $array .= '<li class="opaga-board menu-item" id="show-log">Log</li>';
        }
        else
            $array .= '<li class="opaga-board menu-item connect"><a href="'.home_url().'/login/">'.__("Connexion").'</a></li>';
    }
    
    return $array; 
}; 
add_filter('wp_nav_menu_items', 'opaga_add_to_menu', 10, 2);

// globales
$Formation = array();
$SessionFormation = array();
$SessionStagiaire = array();
$Client = array();
$Documents = array();

function add_extra_plugin($plugins)
{
    $plugins['table'] = wpof_url.'js/tinymce-plugins/table/plugin.min.js';
    $plugins['searchreplace'] = wpof_url.'js/tinymce-plugins/searchreplace/plugin.min.js';
    $plugins['noneditable'] = wpof_url.'js/tinymce-plugins/noneditable/plugin.min.js';
    return $plugins;
}
add_filter('mce_external_plugins', 'add_extra_plugin');

if (is_admin())
{
    require_once(wpof_path . "/wpof-admin.php");
    //if (get_option("wpof_version") < WPOF_VERSION)
        require_once(wpof_path . "/wpof-mise-a-jour.php");
}

?>
