<?php
/*
 * wpof-bpf.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-wpof.php");
require_once(wpof_path . "/wpof-formation.php");
require_once(wpof_path . "/wpof-fonctions.php");
require_once(wpof_path . "/class/class-termlist.php");
require_once(wpof_path . "/class/class-modele.php");
require_once(wpof_path . "/class/class-plugins-manager.php");

$wpof = new WPOF();

// patch, TODO à reporter dans une mise à jour de la base !!!
$responsable_meta = get_option("um_role_responsable_meta");
if (!isset($responsable_meta["_um_can_delete_everyone"]) || $responsable_meta["_um_can_delete_everyone"] != 1)
{
    $responsable_meta["_um_can_delete_everyone"] = 1;
    $responsable_meta["_um_can_delete_roles"] = array("um_stagiaire", "um_formateur-trice", "um_responsable");
    update_option("um_role_responsable_meta", $responsable_meta);
}
$um_options = get_option("um_options");
$um_options['disable_profile_photo_upload'] = 1;
$um_options["profile_tab_oftab_privacy"] = 0;
update_option("um_options", $um_options);
// Fin patch

$type_class = array
(
    "modele" => "Modele",
    "session" => "SessionFormation",
    "formation" => "Formation",
    "client" => "Client",
    "stagiaire" => "SessionStagiaire",
    "formateur" => "Formateur",
    "document" => "Document",
    "of" => "WPOF"
);

// Phrase à employer concernant l'exonération de TVA sur la formation
$wpof->terms_exo_tva = __("Cette prestation de formation est exonérée de TVA conformément à l'article 261 du CGI");

$wpof->super_roles = array("admin", "um_responsable");

// Tâches de responsable
$wpof->taches_resp = new TermList("taches_resp");
$wpof->taches_resp->add_term('signature_off', "Signature officiellement reconnue", array('formateur_possible' => false));
$wpof->taches_resp->add_term('signature_po', "Signature par ordre", array('formateur_possible' => false));
$wpof->taches_resp->add_term('premier_contact', "Destinataire du premier contact depuis le formulaire (inscription, renseignements)", array('formateur_possible' => true));

// Type de session de formation (TODO : à virer)
$wpof->type_session = array
(
    'inter' => array('value' => __('Inter-entreprises')),
    'intra' => array('value' => __('Intra-entreprise')),
    'sous_traitance' => array('value' => __('En sous-traitance pour un autre OF'))
);
// Visibilité de la session (ancienne méthode TODO : à virer)
$wpof->visibilite_session = array
(
    'public' => array('value' => __('Publique')),
//    'connecte' => array('value' => __("Nécessite d'être connecté")),
    'invite' => array('value' => __('Sur invitation')),
);

// Onglets de la page session
$wpof->onglet_session = array
(
    'client' => __("Clients/stagiaires"),
    'session' => __("Session"),
    'formation' => __("Formation"),
    'dates' => __("Dates"),
    'lieu' => __("Lieu"),
    'documents' => __("Documents"),
    'questionnaires' => __("Questionnaires"),
    'token' => __("Accès privés"),
);

// Onglets de la page formateur/responsable
$wpof->onglet_user = array
(
    'agenda' => __("Agenda"),
    'catalogue' => __("Catalogue"),
    'clients' => __("Clients"),
    'contrats' => __("Contrats"),
    'profil' => __("Profil de formateur⋅trice"),
    'veille' => __("Veille"),
    'infos' => __("Infos"),
);

// Accès à la session (ex-Visibilité de la session) via un TermList
$wpof->acces_session = new TermList("acces_session");
$wpof->acces_session->add_term('public', __('Publique'));
$wpof->acces_session->add_term('invite', __('Sur invitation'));
//$wpof->acces_session->add_term('connecte',__("Nécessite d'être connecté"));

// Modalité de session
$wpof->modalite_session = new TermList("modalite_session");
$wpof->modalite_session->add_term('pres', __("Présentiel"));
$wpof->modalite_session->add_term('dist', __("À distance"));
$wpof->modalite_session->add_term('mixte', __("Mixte (présentiel et distanciel)"));

/*
 * Types de créneau
 * correspondent aux différentes aspects d'un parcours de formation
 */
$wpof->type_creneau = array
(
    'presentiel' => 'Présentiel',
    'foad_sync' => 'À distance, synchrone',
    'foad_async' => 'À distance, asynchrone',
    'afest' => 'AFEST',
    'recherche' => 'Recherches personnelles',
    'technique' => 'Informel, hors formation',
);

/*
 * Types de tarifs de formation
 */
$wpof->declinaison_tarif = array
(
    'inter' => __('par stagiaire'),
    'intra' => __('pour un groupe d’une même structure'),
    'heure' => __('par heure'),
    'total' => __('total'),
);
$wpof->declinaison_tarif['inter_heure'] = $wpof->declinaison_tarif['inter']." ".$wpof->declinaison_tarif['heure'];
$wpof->declinaison_tarif['inter_total'] = $wpof->declinaison_tarif['inter']." ".$wpof->declinaison_tarif['total'];
$wpof->declinaison_tarif['intra_heure'] = $wpof->declinaison_tarif['intra']." ".$wpof->declinaison_tarif['heure'];
$wpof->declinaison_tarif['intra_total'] = $wpof->declinaison_tarif['intra']." ".$wpof->declinaison_tarif['total'];
    
$wpof->type_tarif_formation = new TermList("type_tarif_formation");
$wpof->type_tarif_formation->add_term("base", "Tarif de base");
$wpof->type_tarif_formation->add_term("reduit", "Tarif réduit");

$wpof->desc_formateur = new TermList("desc_formateur");
$wpof->desc_formateur->add_term("photo", "Photo", array('needed' => false));
$wpof->desc_formateur->add_term("cv", "Nom du CV local", array('needed' => false));
$wpof->desc_formateur->add_term("cv_url", "URL du CV", array('needed' => true));
$wpof->desc_formateur->add_term("activite", "Activité, métier", array('needed' => true));
$wpof->desc_formateur->add_term("url", "Site Web", array('needed' => false));
if (champ_additionnel("formateur_marque"))
{
    $wpof->desc_formateur->add_term("marque", "Marque du/de la formateur⋅trice", array('needed' => false));
    $wpof->desc_formateur->add_term("logo_marque", "Logo de la marque", array('needed' => false));
}
if (champ_additionnel('formateur_statut'))
    $wpof->desc_formateur->add_term("statut", "Statut du/de la formateur⋅rice", array('needed' => true));
if (champ_additionnel('formateur_code'))
    $wpof->desc_formateur->add_term("code", "Code ou identifiant du/de la formateur⋅rice", array('needed' => true));
$wpof->desc_formateur->add_term("presentation", "Présentation", array('needed' => true));
$wpof->desc_formateur->add_term("realisations", "Réalisations", array('needed' => false));
$wpof->desc_formateur->add_term("veille_bloc_notes", "Bloc-note de veille", array('needed' => false));
$wpof->desc_formateur->add_term("date_modif_veille", "Dernière mise à jour de la veille", array('needed' => false));
$wpof->desc_formateur->add_term("annee_comptable", "Année comptable", array('needed' => false));
$wpof->desc_formateur->add_term("genre", "Genre", array('needed' => false));
$wpof->desc_formateur->add_term("tags", "Mots-clés", array('needed' => false));
$wpof->desc_formateur->add_term("sous_traitant", "Sous-traitant", array('needed' => false));
$wpof->desc_formateur->add_term("archive", "Archivé", array('needed' => false));
$wpof->desc_formateur->add_term("completion", "Complétion", array('needed' => false));


/*
 * Liste des champs servant à décrire précisement une action de formation (programme de formation)
 */
$wpof->desc_formation = new TermList("desc_formation");
$wpof->desc_formation->add_group("proposition", "Infos nécessaires pour l'action de formation");
$wpof->desc_formation->add_term("presentation", "Présentation générale", array('group' => 'proposition', 'formation_needed' => true, 'session_needed' => true));
$wpof->desc_formation->add_term("prerequis", "Pré-requis", array('group' => 'proposition', 'formation_needed' => true, 'session_needed' => true));
$wpof->desc_formation->add_term("objectifs_pro", "Objectifs généraux", array('group' => 'proposition', 'formation_needed' => true, 'session_needed' => true));
$wpof->desc_formation->add_term("objectifs", "Objectifs opérationnels, pédagogiques, évaluables", array('group' => 'proposition', 'formation_needed' => true, 'session_needed' => true));
$wpof->desc_formation->add_term("public_cible", "Public concerné", array('group' => 'proposition', 'formation_needed' => false, 'session_needed' => false));
$wpof->desc_formation->add_term("inscription_delai", "Modalités d'inscription et délai d'accès", array('group' => 'proposition', 'formation_needed' => true, 'session_needed' => true));
$wpof->desc_formation->add_term("modalites_pedagogiques", "Méthodes mobilisées", array('group' => 'proposition', 'formation_needed' => true, 'session_needed' => true));
$wpof->desc_formation->add_term("ressources", "Ressources pédagogiques", array('group' => 'proposition', 'formation_needed' => false, 'session_needed' => true));
$wpof->desc_formation->add_term("organisation", "Modalités d'organisation", array('group' => 'proposition', 'formation_needed' => false, 'session_needed' => false));
$wpof->desc_formation->add_term("materiel_pedagogique", "Matériel pédagogique", array('group' => 'proposition', 'formation_needed' => false, 'session_needed' => true));
$wpof->desc_formation->add_term("modalites_evaluation", "Modalités d'évaluation", array('group' => 'proposition', 'formation_needed' => true, 'session_needed' => true));
$wpof->desc_formation->add_term("accessibilite", "Accessibilité des personnes en situation de handicap", array('group' => 'proposition', 'formation_needed' => true, 'session_needed' => true));
$wpof->desc_formation->add_term("programme", "Contenus", array('group' => 'proposition', 'formation_needed' => false, 'session_needed' => false));

$wpof->desc_formation->add_group("convention", "Infos utiles pour la convention de formation");
//$wpof->desc_formation->add_term("engagements_parties", "Engagements réciproques des parties et obligation de discrétion", array('group' => 'convention'));
$wpof->desc_formation->add_term("propriete_intellectuelle", "Propriété intellectuelle", array('group' => 'convention', 'formation_needed' => false, 'session_needed' => true));

$wpof->completion_formation = array
(
    "duree",
    "tarif_base_inter_total",
);
$wpof->completion_formation = array_merge($wpof->completion_formation, array_keys($wpof->desc_formation->get_group("proposition", "formation_needed")->term));
$wpof->completion_session = array
(
    'invite' => array
    (
        "dates_texte",
        "nb_heure",
    ),
    'public' => array
    (
    ),
);
$wpof->completion_session['invite'] = array_merge($wpof->completion_session['invite'], array_keys($wpof->desc_formation->get_group("proposition", "session_needed")->term));
$wpof->completion_session['public'] = array_merge($wpof->completion_session['public'], $wpof->completion_session['invite']);

$wpof->desc_formation_mode = array
(
    'inactif' => 'Inactif',
    'force' => 'Forcé',
    'propose' => 'Proposé',
);

if (!isset($wpof->formation_presentation_mode))
    foreach(array_keys($wpof->desc_formation->term) as $k)
        $wpof->{"formation_".$k."_mode"} = 'inactif';

// Lien d'édition de contenu
$wpof->formation_edit_link_suffix = "edit";

$wpof->plugin_manager = new PluginsManager();
$wpof->plugins = $wpof->plugin_manager->getOpagaPlugins();

/*
 * Types de client : entité
 */ 
$wpof->entite_client = new TermList("entite_client");
$wpof->entite_client->add_term("morale", "Morale");
$wpof->entite_client->add_term("physique", "Physique");

/*
 * Liste des champs servant à décrire précisement un lieu
 */
$wpof->desc_lieu = new TermList("desc_lieu");
$wpof->desc_lieu->add_term("nom", "Nom", array('type' => 'text'));
$wpof->desc_lieu->add_term("adresse", "Adresse", array('type' => 'textarea'));
$wpof->desc_lieu->add_term("code_postal", "Code postal", array('type' => 'text'));
$wpof->desc_lieu->add_term("ville", "Ville", array('type' => 'text'));
$wpof->desc_lieu->add_term("localisation", "Localisation (champ libre)", array('type' => 'editor'));
//$wpof->desc_lieu->add_term("secu_erp", "PV de sécurité pour ERP", array('type' => 'image'));

/*
 * Type de questions pour les questionnaires
 */
$wpof->questionnaire_type_question = new TermList("questionnaire_type_question");
$wpof->questionnaire_type_question->add_term("title", "Titre", array('noquestion' => 1));
$wpof->questionnaire_type_question->add_term("subtitle", "Sous-titre", array('noquestion' => 1));
$wpof->questionnaire_type_question->add_term("desc", "Description", array('noquestion' => 1));
$wpof->questionnaire_type_question->add_term("likert", "Échelle de Likert", array('options' => ['legend_min' => __("Légende valeur minimale"), 'legend_max' => __("Légende valeur maximale"), 'size' => __("Nombre de choix")]));
$wpof->questionnaire_type_question->add_term("radio", "Choix unique", array('options' => ['choice' => []]));
$wpof->questionnaire_type_question->add_term("checkbox", "Choix multiple", array('options' => ['choice' => []]));
$wpof->questionnaire_type_question->add_term("text", "Texte court");
$wpof->questionnaire_type_question->add_term("textarea", "Texte long");
$wpof->questionnaire_type_question->add_term("number", "Nombre", array('options' => ['min' => __("Minimum"), 'max' => __("Maximum"), 'step' => __("Pas")]));

/*
 * Types de questionnaires
 */
$wpof->questionnaire_type = new TermList("questionnaire_type");
//$wpof->questionnaire_type->add_term("prerequis", __("Évaluation des pré-requis"));
//$wpof->questionnaire_type->add_term("objectifs", __("Évaluation des objectifs pédagogiques"));
//$wpof->questionnaire_type->add_term("besoins", __("Recueil des besoins"));
$wpof->questionnaire_type->add_term("satisfaction", __("Satisfaction"));
//$wpof->questionnaire_type->add_term("bilan_formateur", __("Bilan de formateur⋅ice"));
//$wpof->questionnaire_type->add_term("libre", __("Questionnaire libre"));

$wpof->questionnaire_meta = new TermList("questionnaire_meta");
$wpof->questionnaire_meta->add_term("nature_formation", __("Nature de la formation"), array('multi' => true));
$wpof->questionnaire_meta->add_term("modalite_session", __("Modalité de la session"), array('multi' => true));

/** 
 * NIveau de questionnaire
 */
$wpof->questionnaire_level = new TermList("questionnaire_level");
$wpof->questionnaire_level->add_term("of", __("Organisme de formation"));
$wpof->questionnaire_level->add_term("group", __("Groupe de formateur⋅ices"));
$wpof->questionnaire_level->add_term("user", __("Personnel"));

/*
 * Documents administratifs
 */
// Le contexte définit dans quelle(s) partie(s) de l'interface apparaît le document
$wpof->doc_context = new stdClass();
$wpof->doc_context->morale = 0x1;
$wpof->doc_context->physique = 0x80;
$wpof->doc_context->sous_traitance = 0x2;
$wpof->doc_context->direct = $wpof->doc_context->morale | $wpof->doc_context->physique;
$wpof->doc_context->contrat = $wpof->doc_context->direct | $wpof->doc_context->sous_traitance;
$wpof->doc_context->session = 0x4;
$wpof->doc_context->client = 0x8;
$wpof->doc_context->stagiaire = 0x10;
$wpof->doc_context->formateur = 0x20;
$wpof->doc_context->formation = 0x40;
$wpof->doc_context->entite = $wpof->doc_context->session | $wpof->doc_context->client | $wpof->doc_context->stagiaire | $wpof->doc_context->formateur | $wpof->doc_context->formation;
$wpof->doc_context->questionnaire = 0x80;

$wpof->doc_context_terms = array
(
    $wpof->doc_context->morale => __('Personne morale'),
    $wpof->doc_context->physique => __('Personne physique'),
    $wpof->doc_context->sous_traitance => __('En sous-traitance'),
    $wpof->doc_context->direct => __('En direct'),
    $wpof->doc_context->session => __('Session'),
    $wpof->doc_context->client => __('Client'),
    $wpof->doc_context->stagiaire => __('Stagiaire'),
    $wpof->doc_context->formateur => __('Formateur⋅trice'),
    $wpof->doc_context->formation => __('Formation'),
);

$wpof->doc_signature = new stdClass();
$wpof->doc_signature->stagiaire = Document::VALID_STAGIAIRE_NEED;
$wpof->doc_signature->client = Document::VALID_CLIENT_NEED;
$wpof->doc_signature->responsable = Document::VALID_RESPONSABLE_NEED;
$wpof->doc_signature->formateur = Document::VALID_FORMATEUR_NEED;

$wpof->documents = new TermList("documents");
$wpof->documents->add_term('proposition', __("Programme de formation"),
    array('contexte' => $wpof->doc_context->formation | $wpof->doc_context->session, 'signature' => 0, 'accessopen' => 1)
    );
$wpof->documents->add_term('questionnaire', __("Questionnaire"),
    array('contexte' => $wpof->doc_context->questionnaire, 'signature' => 0, 'nodraft' => 1, 'onthefly' => 1)
    );
/*
$wpof->documents->add_term('eval_formation', __("Évaluation de la formation"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->stagiaire, 'signature' => 0)
    );
$wpof->documents->add_term('quiz_connaissances', __("Évaluation des compétences sur les objectifs"),
    array('contexte' => $wpof->doc_context->direct | $wpof->doc_context->stagiaire, 'signature' => 0)
    );
*/    
get_all_modeles();
if (!empty($Modele))
    foreach($Modele as $modele)
        $wpof->documents->add_term($modele->slug, $modele->desc, array('contexte' => $modele->contexte, 'signature' => $modele->signature));

/*
 * Pages virtuelles
 */
// URL et titre de la page gestion (par défaut)
if (!isset($wpof->url_gestion)) $wpof->url_gestion = "gestion";
if (!isset($wpof->title_gestion)) $wpof->title_gestion = __("Gestion générale");
// URL et titre de la page aide (par défaut)
if (!isset($wpof->url_aide)) $wpof->url_aide = "aide";
if (!isset($wpof->title_aide)) $wpof->title_aide = __("Aide en ligne");
// URL et titre de la page export (par défaut)
if (!isset($wpof->url_export)) $wpof->url_export = "export";
if (!isset($wpof->title_export)) $wpof->title_export = __("Gestion des exports");
// URL et titre de la page informations des clients et stagiaires (par défaut)
if (!isset($wpof->url_acces)) $wpof->url_acces = "mes-infos";
if (!isset($wpof->title_acces)) $wpof->title_acces = __("Mes informations");
// URL et titre de la page tableau de bord utilisateur (par défaut)
if (!isset($wpof->url_user)) $wpof->url_user = "membre";
if (!isset($wpof->title_user)) $wpof->title_user = __("Tableau de bord");
// URL et titre de la page tableau de bord utilisateur (par défaut)
if (!isset($wpof->url_tuto)) $wpof->url_tuto = "tuto";
if (!isset($wpof->title_tuto)) $wpof->title_tuto = __("Tutoriel");
// URL et titre de la page questionnaire (par défaut)
if (!isset($wpof->url_questionnaire)) $wpof->url_questionnaire = "questionnaire";
if (!isset($wpof->title_questionnaire)) $wpof->title_questionnaire = __("Questionnaire");
// URL et titre de la page info (par défaut)
if (!isset($wpof->url_info)) $wpof->url_info = "info";
if (!isset($wpof->title_info)) $wpof->title_info = __("Information");

// Pages spéciales où le thème de WordPress ne doit pas s'appliquer
$wpof->pages_responsable = array
(
    'gestion' => $wpof->url_gestion, 
    'aide' => $wpof->url_aide, 
    'export' => $wpof->url_export, 
);
$wpof->pages_acces_token = array
(
    'acces' => $wpof->url_acces, 
    'questionnaire' => $wpof->url_questionnaire,
);
$wpof->no_theme = array_merge
(
    $wpof->pages_responsable,
    $wpof->pages_acces_token,
    array
    (
        'user' => $wpof->url_user,
        'tuto' => $wpof->url_tuto,
        'info' => $wpof->url_info,
    )
);

// Pages de gestion
$wpof->pages_gestion = array
(
    "formation" => __("Catalogue de formations"),
    "formateur" => __("Équipe pédagogique"),
    "veille" => __("Veille"),
    "session" => __("Agenda des sessions"),
    "client" => __("Base clients"),
    "stagiaire" => __("Base stagiaires"),
    "document" => __("Documents"),
    "questionnaires" => __("Questionnaires"),
    "info" => __("Infos"),
    "bpf" => __("BPF"),
    "system" => __("Système"),
);

$wpof->genre = new TermList('genre');
$wpof->genre->add_term("f", "Femme");
$wpof->genre->add_term("h", "Homme");
$wpof->genre->add_term("a", "Autre");

// tables supplémentaires de la base de données
$wpof->suffix_session_stagiaire = "wpof_session_stagiaire";
$wpof->suffix_stagiaire = "wpof_session_stagiaire";
$wpof->suffix_client = "wpof_client";
$wpof->suffix_documents = "wpof_documents";
$wpof->suffix_quiz = "wpof_quiz";
$wpof->suffix_creneaux = "wpof_creneaux";
$wpof->suffix_log = "wpof_log";
$wpof->suffix_questionnaire = "wpof_questionnaire";
$wpof->suffix_question = "wpof_question";
$wpof->suffix_qreponse = "wpof_qreponse";
$wpof->suffix_question_relationships = "wpof_question_relationships";
$wpof->suffix_statmeta = "wpof_statmeta";
$wpof->suffix_content = "wpof_content";
$wpof->suffix_content_cat = "wpof_content_cat";
$wpof->suffix_content_rel = "wpof_content_rel";
// pour compatibilité
$suffix_session_stagiaire = "wpof_session_stagiaire";
$suffix_stagiaire = "wpof_session_stagiaire";
$suffix_client = "wpof_client";
$suffix_documents = "wpof_documents";
$suffix_quiz = "wpof_quiz";
$suffix_eval = "wpof_eval";
$suffix_creneaux = "wpof_creneaux";

$wpof->content_types = array
(
    'info' => 'Information',
);

$tinymce_wpof_settings = array
(
    'tinymce' => array
    (
        'language' => 'fr',
        'spellchecker_languages' => 'French=fr',
        'paste_as_text' => true,
        'toolbar1' => 'opagamenu,formatselect,table,|,bold,italic,|,bullist,numlist,|,blockquote,alignleft,aligncenter,alignright,|,link,unlink,|,pastetext,removeformat,charmap,|,outdent,indent,|,undo,redo,spellchecker,searchreplace',
        'toolbar2' => '',
        'init_instance_callback' => 'init_opaga_editor',
        'setup' => 'setup_opaga_editor',
        'content_css' => wpof_url . "/css/wp-editor.css",
        'noneditable_noneditable_class' => "locked"
    ),
    'drag_drop_upload' => true,
    'editor_height' => 180,
    'editor_class' => 'opaga_editor',
    'quicktags' => false,
);

/*
 * Gestion des tests dans les modèles de document
 */
$wpof->test_choix = new stdClass();
$wpof->test_choix->oui = 1;
$wpof->test_choix->non = 2;
$wpof->test_choix->les_deux = $wpof->test_choix->oui | $wpof->test_choix->non;
$wpof->test_modele = new TermList("test_modele");
$wpof->test_modele->add_term("egal", "égal à", array('sign' => '==', 'choix' => $wpof->test_choix->les_deux));
$wpof->test_modele->add_term("infegal", "inférieur ou égal à", array('sign' => '<=', 'choix' => $wpof->test_choix->les_deux));
$wpof->test_modele->add_term("supegal", "supérieur ou égal à", array('sign' => '>=', 'choix' => $wpof->test_choix->les_deux));
$wpof->test_modele->add_term("inf", "inférieur à", array('sign' => '<', 'choix' => $wpof->test_choix->les_deux));
$wpof->test_modele->add_term("sup", "supérieur à", array('sign' => '>', 'choix' => $wpof->test_choix->les_deux));
$wpof->test_modele->add_term("diff", "différent de", array('sign' => '!=', 'choix' => $wpof->test_choix->les_deux));
$wpof->test_modele->add_term("vrai", "est vrai", array('sign' => '', 'choix' => $wpof->test_choix->oui));
$wpof->test_modele->add_term("faux", "est faux", array('sign' => '', 'choix' => $wpof->test_choix->non));

/*
 * Éditeur spécifique pour rédiger les questionnaires
 */
$tinymce_wpof_quiz_settings = array
(
    'tinymce' => array
    (
        'language' => 'fr',
        'spellchecker_languages' => 'French=fr',
        'paste_as_text' => true,
        'media_buttons' => false,
        'quicktags' => false,
        'toolbar1' => 'formatselect,bold,italic',
        'toolbar2' => '',
        'dfw' => false,
        //'content_css' => wpof_url . "/css/wp-editor.css",
    ),
    'editor_height' => 180,
    'quicktags' => false,
);

// Champs additionnels
$wpof->champs_additionnels_liste = array();
$wpof->champs_additionnels_liste['numero_contrat'] = __("Numéro de contrat pour les clients");
$wpof->champs_additionnels_liste['periode_validite'] = __("Lors de la création d'un document à signer, possibilité de préciser une période de validité du document");
$wpof->champs_additionnels_liste['formateur_marque'] = __("Les formateurs peuvent avoir et mettre en avant leur propre marque");
$wpof->champs_additionnels_liste['formateur_statut'] = __("Précisez le statut de vos formateurs");
$wpof->champs_additionnels_liste['formateur_code'] = __("Précisez un code d'activité ou identifiant du⋅de la formateur⋅ice");
$wpof->champs_additionnels_liste['genre_stagiaire'] = __("Enregistrer le genre des stagiaires");
$wpof->champs_additionnels_liste['duree_jour'] = __("Permettre de donner une indication de durée du genre « soit deux demi-journées »");
$wpof->champs_additionnels_liste['image_signature'] = __("Permettre d'intégrer une image de la signature du⋅de la responsable signataire (attention, pas valide juridiquement !)");
$wpof->champs_additionnels_liste['responsable_usurpe'] = __("Les responsables peuvent se connecter à la place d'une autre personne (responsable ou formateur⋅ice)");
$wpof->champs_additionnels_liste['comptes_locaux'] = __("L'identification est gérée localement");
$wpof->champs_additionnels_liste['all_can_remove_formation'] = __("Les formateur⋅ices peuvent également supprimer des formations et des sessions");
$wpof->champs_additionnels_liste['detail_tarif_peda'] = __("Les formateur⋅ices peuvent détailler le tarif pédagogique pour chaque client (animation, préparation)");

// Mots-clés personnalisables pour les modèles de document
$wpof->custom_keyword = new stdClass();
$wpof->custom_keyword->types = array('bool' => 'Oui/Non', 'text' => 'Texte', 'number' => 'Nombre');
$wpof->custom_keyword->template = json_encode(array('keyword' => 'KEYWORD', 'type' => '', 'needed' => false, 'defaut' => '', 'desc' => __('À quoi sert ce mot-clé ?')), JSON_UNESCAPED_UNICODE);

/*
 * Options légales
 * Pas de localisation, ce sont les termes officiels
 */

$wpof->acompte_pourcent_max_particulier = 0.3;
$wpof->max_duree_bc = 24;

// Types de financement
$wpof->financement = new TermList("financement");
$wpof->financement->add_group("g1", "Organismes paritaires collecteurs ou gestionnaires des fonds de la formation");
$wpof->financement->add_group("g2", "Pouvoirs publics pour la formation de publics spécifiques");
$wpof->financement->add_term("prive", "Entreprise pour la formation de ses salariés");
$wpof->financement->add_term("mutu1", "contrat d'apprentissage", array('group' => 'g1'));
$wpof->financement->add_term("mutu2", "contrat de professionnalisation", array('group' => 'g1'));
$wpof->financement->add_term("mutu3", "promotion ou reconversion en alternance", array('group' => 'g1'));
$wpof->financement->add_term("mutu4", "congé individuel de formation ou projet de transition professionnelle", array('group' => 'g1'));
$wpof->financement->add_term("mutu5", "compte personnel de formation", array('group' => 'g1'));
$wpof->financement->add_term("mutu6", "dispositif spécifique pour les personnes en recherche d'emploi", array('group' => 'g1'));
$wpof->financement->add_term("mutu7", "dispositif spécifique pour les travailleurs non-salariés", array('group' => 'g1'));
$wpof->financement->add_term("mutu8", "plan de développement des compétences ou autres dispositifs", array('group' => 'g1'));
//$wpof->financement->add_term("assur", "Fonds d'assurance");
$wpof->financement->add_term("public", "Pouvoirs publics pour la formation de leurs agents (État, collectivités territoriales, établissements publics à caractère administratif)");
$wpof->financement->add_term("pubspec1", "Instances européennes", array('group' => 'g2'));
$wpof->financement->add_term("pubspec2", "État", array('group' => 'g2'));
$wpof->financement->add_term("pubspec3", "Conseils régionaux", array('group' => 'g2'));
$wpof->financement->add_term("pubspec4", "Pôle emploi", array('group' => 'g2'));
$wpof->financement->add_term("pubspec5", "Autres ressources publiques", array('group' => 'g2'));
$wpof->financement->add_term("part", "Contrats conclus avec des personnes à titre individuel et à leurs frais");
$wpof->financement->add_term("opac", "Contrats conclus avec d’autres organismes de formation");
$wpof->financement->add_term("autres", "Autres produits au titre de la formation professionnelle continue");

// Nature de la formation (ou objectif de la prestation)
$wpof->nature_formation = new TermList("nature_formation");
$wpof->nature_formation->add_group("g1", "diplôme ou titre à finalité professionnelle (hors CQP) inscrit au RNCP");
$wpof->nature_formation->add_term("form", "Autre formation professionnelle continue");
$wpof->nature_formation->add_term("bilan", "Bilan de compétences");
$wpof->nature_formation->add_term("vae", "Actions d'accompagnement à la validation des acquis d'expérience");
$wpof->nature_formation->add_term("rs", "certification (dont CQP) ou habilitation enregistrée au répertoire spécifique (RS)");
$wpof->nature_formation->add_term("nors", "CQP non enregistré au RNC ou au RS");
$wpof->nature_formation->add_term("dip1", "Niveau 6 à 8 (licence, master, diplôme d’ingénieur, doctorat)", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip2", "Niveau 5 (BTS, DUT, écoles de formation sanitaire et sociale…)", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip3", "Niveau 4 (BAC professionnel, BT, BP, BM…)", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip4", "Niveau 3 (BEP, CAP,…)", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip5", "Niveau 2", array('group' => 'g1'));
$wpof->nature_formation->add_term("dip6", "certificat de qualification professionnelle (CQP) sans niveau de qualification", array('group' => 'g1'));

// Types de stagiaires
$wpof->statut_stagiaire = new TermList("statut_stagiaire");
$wpof->statut_stagiaire->add_term("sal_prive", "Salarié d'employeur privé hors apprentis");
$wpof->statut_stagiaire->add_term("apprenti", "Apprenti");
$wpof->statut_stagiaire->add_term("rech_emploi", "Personne en recherche d'emploi formée par votre organisme de formation");
$wpof->statut_stagiaire->add_term("particulier", "Particulier à ses propres frais formé par votre organisme de formation");
$wpof->statut_stagiaire->add_term("autre", "Autres stagiaires");

// OPCO
$wpof->opco = new TermList("opco");
$wpof->opco->add_term("afdas", "AFDAS");
$wpof->opco->add_term("atlas", "ATLAS");
$wpof->opco->add_term("ocapiat", "OCAPIAT");
$wpof->opco->add_term("2i", "OPCO 2i");
$wpof->opco->add_term("construction", "OPCO de la Construction");
$wpof->opco->add_term("ep", "OPCO des entreprises de proximité (EP)");
$wpof->opco->add_term("akto", "AKTO / ESSFIMO");
$wpof->opco->add_term("mobilites", "OPCO Mobilités");
$wpof->opco->add_term("commerce", "OPCOmmerce");
$wpof->opco->add_term("sante", "OPCO Santé");
$wpof->opco->add_term("uniformation", "Uniformation – OPCO de la Cohésion sociale");

// FAF
$wpof->faf = new TermList("faf");
$wpof->faf->add_term("fifpl", "FIF-PL (professions libérales)");
$wpof->faf->add_term("fafpm", "FAF-PM (profession médicales)");
$wpof->faf->add_term("agefice", "Agefice (chefs d'entreprise)");
$wpof->faf->add_term("afdas", "AFDAS (artistes auteurs)");
$wpof->faf->add_term("fafcea", "FAFCEA (artisanat)");
$wpof->faf->add_term("vivea", "Vivéa (agriculture, forêt)");
$wpof->faf->add_term("ocapiat", "Ocapiat (pêche, cultures marines)");
$wpof->faf->add_term("crma", "Chambre régionale des métiers");

// Spécialités de formation
$wpof->specialite = new TermList("specialite");
$wpof->specialite->add_group("100", "Formations générales");
$wpof->specialite->add_term("100", "100 – Formations générales", array('group' => "100"));
$wpof->specialite->add_group("110", "Spécialités pluriscientifiques");
$wpof->specialite->add_term("110", "110 – Spécialités pluriscientifiques", array('group' => "110"));
$wpof->specialite->add_term("111", "111 – Physique-chimie", array('group' => "110"));
$wpof->specialite->add_term("112", "112 – Chimie-biologie, biochimie", array('group' => "110"));
$wpof->specialite->add_term("113", "113 – Sciences naturelles (biologie-géologie)", array('group' => "110"));
$wpof->specialite->add_term("114", "114 – Mathématiques", array('group' => "110"));
$wpof->specialite->add_term("115", "115 – Physique", array('group' => "110"));
$wpof->specialite->add_term("116", "116 – Chimie", array('group' => "110"));
$wpof->specialite->add_term("117", "117 – Sciences de la terre", array('group' => "110"));
$wpof->specialite->add_term("118", "118 – Sciences de la vie", array('group' => "110"));
$wpof->specialite->add_group("120", "Spécialités pluridisciplinaires, sciences humaines et droit");
$wpof->specialite->add_term("120", "120 – Spécialités pluridisciplinaires, sciences humaines et droit", array('group' => "120"));
$wpof->specialite->add_term("121", "121 – Géographie", array('group' => "120"));
$wpof->specialite->add_term("122", "122 – Economie", array('group' => "120"));
$wpof->specialite->add_term("123", "123 – Sciences sociales (y compris démographie, anthropologie)", array('group' => "120"));
$wpof->specialite->add_term("124", "124 – Psychologie", array('group' => "120"));
$wpof->specialite->add_term("125", "125 – Linguistique", array('group' => "120"));
$wpof->specialite->add_term("126", "126 – Histoire", array('group' => "120"));
$wpof->specialite->add_term("127", "127 – Philosophie, éthique et théologie", array('group' => "120"));
$wpof->specialite->add_term("128", "128 – Droit, sciences politiques", array('group' => "120"));
$wpof->specialite->add_group("130", "Spécialités littéraires et artistiques plurivalentes");
$wpof->specialite->add_term("130", "130 – Spécialités littéraires et artistiques plurivalentes", array('group' => "130"));
$wpof->specialite->add_term("131", "131 – Français, littérature et civilisation française", array('group' => "130"));
$wpof->specialite->add_term("132", "132 – Arts plastiques", array('group' => "130"));
$wpof->specialite->add_term("133", "133 – Musique, arts du spectacle", array('group' => "130"));
$wpof->specialite->add_term("134", "134 – Autres disciplines artistiques et spécialités artistiques plurivalentes", array('group' => "130"));
$wpof->specialite->add_term("135", "135 – Langues et civilisations anciennes", array('group' => "130"));
$wpof->specialite->add_term("136", "136 – Langues vivantes, civilisations étrangères et régionales", array('group' => "130"));
$wpof->specialite->add_group("200", "Technologies industrielles fondamentales (génie industriel, procédés de transformation, spécialités à dominante fonctionnelle)");
$wpof->specialite->add_term("200", "200 – Technologies industrielles fondamentales (génie industriel, procédés de transformation, spécialités à dominante fonctionnelle)", array('group' => "200"));
$wpof->specialite->add_term("201", "201 – Technologies de commandes des transformations industriels (automatismes et robotique industriels, informatique industrielle)", array('group' => "200"));
$wpof->specialite->add_group("210", "Spécialités plurivalentes de l'agronomie et de l'agriculture");
$wpof->specialite->add_term("210", "210 – Spécialités plurivalentes de l'agronomie et de l'agriculture", array('group' => "210"));
$wpof->specialite->add_term("211", "211 – Productions végétales, cultures spécialisées (horticulture, viticulture, arboriculture fruitière...)", array('group' => "210"));
$wpof->specialite->add_term("212", "212 – Productions animales, élevage spécialisé, aquaculture, soins aux animaux, y compris vétérinaire", array('group' => "210"));
$wpof->specialite->add_term("213", "213 – Forêts, espaces naturels, faune sauvage, pêche", array('group' => "210"));
$wpof->specialite->add_term("214", "214 – Aménagement paysager (parcs, jardins, espaces verts ...)", array('group' => "210"));
$wpof->specialite->add_group("220", "Spécialités pluritechnologiques des transformations");
$wpof->specialite->add_term("220", "220 – Spécialités pluritechnologiques des transformations", array('group' => "220"));
$wpof->specialite->add_term("221", "221 – Agro-alimentaire, alimentation, cuisine", array('group' => "220"));
$wpof->specialite->add_term("222", "222 – Transformations chimiques et apparentées (y compris industrie pharmaceutique)", array('group' => "220"));
$wpof->specialite->add_term("223", "223 – Métallurgie (y compris sidérurgie, fonderie, non ferreux...)", array('group' => "220"));
$wpof->specialite->add_term("224", "224 – Matériaux de construction, verre, céramique", array('group' => "220"));
$wpof->specialite->add_term("225", "225 – Plasturgie, matériaux composites", array('group' => "220"));
$wpof->specialite->add_term("226", "226 – Papier, carton", array('group' => "220"));
$wpof->specialite->add_term("227", "227 – Energie, génie climatique (y compris énergie nucléaire, thermique, hydraulique ; utilités : froid, climatisation, chauffage)", array('group' => "220"));
$wpof->specialite->add_group("230", "Spécialités pluritechnologiques, génie civil, construction, bois");
$wpof->specialite->add_term("230", "230 – Spécialités pluritechnologiques, génie civil, construction, bois", array('group' => "230"));
$wpof->specialite->add_term("231", "231 – Mines et carrières, génie civil, topographie", array('group' => "230"));
$wpof->specialite->add_term("232", "232 – Bâtiment : construction et couverture", array('group' => "230"));
$wpof->specialite->add_term("233", "233 – Bâtiment : finitions", array('group' => "230"));
$wpof->specialite->add_term("234", "234 – Travail du bois et de l'ameublement", array('group' => "230"));
$wpof->specialite->add_group("240", "Spécialités pluritechnologiques matériaux souples");
$wpof->specialite->add_term("240", "240 – Spécialités pluritechnologiques matériaux souples", array('group' => "240"));
$wpof->specialite->add_term("241", "241 – Textile", array('group' => "240"));
$wpof->specialite->add_term("242", "242 – Habillement (y compris mode, couture)", array('group' => "240"));
$wpof->specialite->add_term("243", "243 – Cuirs et peaux", array('group' => "240"));
$wpof->specialite->add_group("250", "Spécialités pluritechnologiques mécanique-électricité (y compris maintenance mécano-électrique)");
$wpof->specialite->add_term("250", "250 – Spécialités pluritechnologiques mécanique-électricité (y compris maintenance mécano-électrique)", array('group' => "250"));
$wpof->specialite->add_term("251", "251 – Mécanique générale et de précision, usinage", array('group' => "250"));
$wpof->specialite->add_term("252", "252 – Moteurs et mécanique auto", array('group' => "250"));
$wpof->specialite->add_term("253", "253 – Mécanique aéronautique et spatiale", array('group' => "250"));
$wpof->specialite->add_term("254", "254 – Structures métalliques (y compris soudure, carrosserie, coque bateau, cellule avion)", array('group' => "250"));
$wpof->specialite->add_term("255", "255 – Electricité, électronique (non compris automatismes, productique)", array('group' => "250"));
$wpof->specialite->add_group("300", "Spécialités plurivalentes des services");
$wpof->specialite->add_term("300", "300 – Spécialités plurivalentes des services", array('group' => "300"));
$wpof->specialite->add_group("310", "Spécialités plurivalentes des échanges et de la gestion (y compris administration générale des entreprises et des collectivités)");
$wpof->specialite->add_term("310", "310 – Spécialités plurivalentes des échanges et de la gestion (y compris administration générale des entreprises et des collectivités)" ,array('group' => "310"));
$wpof->specialite->add_term("311", "311 – Transports, manutention, magasinage", array('group' => "310"));
$wpof->specialite->add_term("312", "312 – Commerce, vente", array('group' => "310"));
$wpof->specialite->add_term("313", "313 – Finances, banque, assurances", array('group' => "310"));
$wpof->specialite->add_term("314", "314 – Comptabilité, gestion", array('group' => "310"));
$wpof->specialite->add_term("315", "315 – Ressources humaines, gestion du personnel, gestion de l'emploi", array('group' => "310"));
$wpof->specialite->add_group("320", "Spécialités plurivalentes de la communication");
$wpof->specialite->add_term("320", "320 – Spécialités plurivalentes de la communication", array('group' => "320"));
$wpof->specialite->add_term("321", "321 – Journalisme, communication (y compris communication graphique et publicité)", array('group' => "320"));
$wpof->specialite->add_term("322", "322 – Techniques de l'imprimerie et de l'édition", array('group' => "320"));
$wpof->specialite->add_term("323", "323 – Techniques de l'image et du son, métiers connexes du spectacle", array('group' => "320"));
$wpof->specialite->add_term("324", "324 – Secrétariat, bureautique", array('group' => "320"));
$wpof->specialite->add_term("325", "325 – Documentation, bibliothèques, administration des données", array('group' => "320"));
$wpof->specialite->add_term("326", "326 – Informatique, traitement de l'information, réseaux de transmission des données", array('group' => "320"));
$wpof->specialite->add_group("330", "Spécialités plurivalentes sanitaires et sociales");
$wpof->specialite->add_term("330", "330 – Spécialités plurivalentes sanitaires et sociales", array('group' => "330"));
$wpof->specialite->add_term("331", "331 – Santé", array('group' => "330"));
$wpof->specialite->add_term("332", "332 – Travail social", array('group' => "330"));
$wpof->specialite->add_term("333", "333 – Enseignement, formation", array('group' => "330"));
$wpof->specialite->add_term("334", "334 – Accueil, hôtellerie, tourisme", array('group' => "330"));
$wpof->specialite->add_term("335", "335 – Animation culturelle, sportive et de loisirs", array('group' => "330"));
$wpof->specialite->add_term("336", "336 – Coiffure, esthétique et autres spécialités des services aux personnes", array('group' => "330"));
$wpof->specialite->add_group("340", "Spécialités plurivalentes des services à la collectivité");
$wpof->specialite->add_term("340", "340 - Spécialités plurivalentes des services à la collectivité", array('group' => "340"));
$wpof->specialite->add_term("341", "341 – Aménagement du territoire, développement, urbanisme", array('group' => "340"));
$wpof->specialite->add_term("342", "342 – Protection et développement du patrimoine", array('group' => "340"));
$wpof->specialite->add_term("343", "343 – Nettoyage, assainissement, protection de l'environnement", array('group' => "340"));
$wpof->specialite->add_term("344", "344 – Sécurité des biens et des personnes, police, surveillance (y compris hygiène et sécurité)", array('group' => "340"));
$wpof->specialite->add_term("345", "345 – Application des droits et statut des personnes", array('group' => "340"));
$wpof->specialite->add_term("346", "346 – Spécialités militaires", array('group' => "340"));
$wpof->specialite->add_group("410", "Spécialités concernant plusieurs capacités");
$wpof->specialite->add_term("410", "410 – Spécialités concernant plusieurs capacités", array('group' => "410"));
$wpof->specialite->add_term("411", "411 – Pratiques sportives (y compris : arts martiaux)", array('group' => "410"));
$wpof->specialite->add_term("412", "412 – Développement des capacités mentales et apprentissages de base", array('group' => "410"));
$wpof->specialite->add_term("413", "413 – Développement des capacités comportementales et relationnelles", array('group' => "410"));
$wpof->specialite->add_term("414", "414 – Développement des capacités individuelles d'organisation", array('group' => "410"));
$wpof->specialite->add_term("415", "415 – Développement des capacités d'orientation, d'insertion ou de réinsertion sociales et professionnelles", array('group' => "410"));
$wpof->specialite->add_term("421", "421 – Jeux et activités spécifiques de loisirs", array('group' => "410"));
$wpof->specialite->add_term("422", "422 – Economie et activités domestiques", array('group' => "410"));
$wpof->specialite->add_term("423", "423 – Vie familiale, vie sociale et autres formations au développement personnel", array('group' => "410"));

$wpof->region = new TermList('region');
$wpof->region->add_term('idf', "Île-de-France");
$wpof->region->add_term('cvdl', "Centre-Val de Loire");
$wpof->region->add_term('hdf', 'Hauts-de-France');
$wpof->region->add_term('ge', 'Grand Est');
$wpof->region->add_term('pdl', 'Pays de la Loire');
$wpof->region->add_term('bretagne', 'Bretagne');
$wpof->region->add_term('normandie', 'Normandie');
$wpof->region->add_term('paca', "Provence-Alpes-Côte d'Azur");
$wpof->region->add_term('corse', 'Corse');
$wpof->region->add_term('na', 'Nouvelle-Aquitaine');
$wpof->region->add_term('occitanie', 'Occitanie');
$wpof->region->add_term('ara', 'Auvergne-Rhône-Alpes');
$wpof->region->add_term('bfc', "Bourgogne-Franche-Comté");
$wpof->region->add_term('gp', "Guadeloupe");
$wpof->region->add_term('gf', "Guyane");
$wpof->region->add_term('mq', "Martinique");
$wpof->region->add_term('re', "La Réunion");
$wpof->region->add_term('mt', "Mayotte");

// options qui doivent être initialisées à la valeur venat du prototype et créées dans la base lors de l'installation
$wpof_options_preinit = array
(
    'wpof_monnaie',
    'wpof_monnaie_symbole',
    'wpof_of_tauxtva',
    'wpof_champs_additionnels',
    'wpof_tarif_inter',
    'wpof_url_gestion',
    'wpof_title_gestion',
    'wpof_url_aide',
    'wpof_title_aide',
    'wpof_url_export',
    'wpof_title_export',
    'wpof_url_user',
    'wpof_title_user',
    'wpof_url_acces',
    'wpof_title_acces',
    'wpof_hidden_menus',
    'wpof_allways_menu',
    'wpof_max_upload_size',
    'wpof_pdf_marge_haut',
    'wpof_pdf_marge_bas',
    'wpof_pdf_marge_gauche',
    'wpof_pdf_marge_droite',
    'wpof_pdf_hauteur_header',
    'wpof_pdf_hauteur_footer',
    'wpof_pdf_titre_font',
    'wpof_pdf_texte_font',
    'wpof_pdf_couleur_titre_doc',
    'wpof_pdf_couleur_titre_autres',
    'wpof_pdf_header',
    'wpof_pdf_footer',
    'wpof_token_validity',
    'wpof_tarif_heure_alerte',
    'wpof_acompte_pourcent',
    'wpof_acompte_systematique',
    'wpof_log_load_rows',
    'wpof_nb_ligne_emargement',
    'wpof_use_category',
    'wpof_use_tag',
    'wpof_theme',
    'wpof_wp_menu',
    'wpof_content_revisions_max',
    'wpof_delai_alerte_veille',
    'wpof_dompdf_https_noverif'
);
foreach (array_keys($wpof->desc_formation->term) as $t)
{
    $wpof_options_preinit[] = "wpof_formation_{$t}_text";
    $wpof_options_preinit[] = "wpof_formation_{$t}_mode";
}

// options qui doivent être initialisées à une valeur vide mais créées dans la base lors de l'installation
$wpof_options_empty_init = array
(
    'wpof_of_nom',
    'wpof_of_adresse',
    'wpof_of_code_postal',
    'wpof_of_ville',
    'wpof_of_telephone',
    'wpof_of_email',
    'wpof_of_description',
    'wpof_of_url',
    'wpof_of_logo',
    'wpof_of_siret',
    'wpof_of_ape',
    'wpof_of_rcs',
    'wpof_of_tva_intracom',
    'wpof_of_noof',
    'wpof_of_datadock',
    'wpof_of_qualiopi',
    'wpof_of_hastva',
    'wpof_of_exotva',
    'wpof_annee1',
    'wpof_debug',
    'wpof_plugin',
    'wpof_eval_form',
    'wpof_pdf_css',
    'wpof_tache_fonction',
    "wpof_bc_page",
);

foreach(array_keys($wpof->taches_resp->term) as $t)
    $wpof_options_empty_init[] = "wpof_tache_$t";

/*
 * Options de WPOF gérées dans l'interface d'admin (wp-admin/admin.php?page=wpof)
 */
$wpof_options = array_merge($wpof_options_preinit, $wpof_options_empty_init, $wpof->plugin_manager->getPluginsOptions());

$sql_files = array
(
    "roles" => (object) array("cols" => "option_name, option_value", "table" => "options", "where" => "option_name = 'um_roles' OR option_name = 'um_role_responsable_meta' OR option_name = 'um_role_formateur-trice_meta'"),
);

add_action('wp_ajax_export_db_struct', 'export_db_struct');
function export_db_struct()
{
    global $wpdb;
    $reponse = array('log' => '');
    $sql = "";
    
    $tables = $wpdb->get_col("SHOW TABLES LIKE '%wpof%';");
    $reponse['log'] .= '<pre>'.var_export($tables, true).'</pre>';
    foreach($tables as $t)
    {
        $definition = $wpdb->get_col("SHOW CREATE TABLE ".$t.";", 1);
        $definition = str_replace($wpdb->prefix, '{prefix}', $definition);
        $definition = preg_replace('/\)[^\)]*$/', ') {charset_collate}', $definition);
        $sql .= $definition[0].";\n";
        $reponse['log'] .= '<pre>'.var_export($definition, true).'</pre>';
    }

    if (!empty($sql))
    {
        $filename = wpof_path . "init/opaga_tables.sql";
        $sql_file = fopen($filename, "w");
        fwrite($sql_file, $sql);
        fclose($sql_file);
        $reponse['log'] .= '<p class="succes">'.$filename." ".__("créé").'</p>';
    }
    else
        $reponse['log'] .= '<p class="erreur">'.__("Erreur d'export de la structure des tables OPAGA").'</p>';
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_export_sql', 'export_sql');
function export_sql()
{
    global $sql_files, $wpdb;
    $sql_id = null;
    
    $reponse = array();
    
    if (isset($_POST['param']))
        $sql_id = $_POST['param'];
    
    if (!$sql_id)
        die();
    
    $sql_obj = $sql_files[$sql_id];
    $sql_insert = "";
    
    $query = "SELECT {$sql_obj->cols} FROM {$wpdb->prefix}{$sql_obj->table} WHERE {$sql_obj->where};";
    $reponse['log'] = $query;
    $sql_insert .= "REPLACE INTO {table_prefix}{$sql_obj->table} ({$sql_obj->cols}) VALUES\n";
    $rows = array();
    foreach($wpdb->get_results($query, ARRAY_A) as $row)
    {
        $this_row = array();
        foreach($row as $col)
            $this_row[] = "'".addslashes($col)."'";
        $rows[] = "(".join(", ", $this_row).")";
    }
    $sql_insert .= join(",\n", $rows)."\n";

/*    if (isset($sql_obj->update))
    {
        $update_values = array();
        foreach(explode(', ', $sql_obj->update) as $v)
            $update_values[] = "$v = new.$v";
            
        //$sql_insert .= " AS new ON DUPLICATE KEY UPDATE ".join(", ", $update_values);
    }*/
    
    $sql_insert .= ";";
    
    $reponse['log'] .= "\n → ". wpof_path . "init/".$sql_id.".sql";
    $sql_file_h = fopen(wpof_path . "init/".$sql_id.".sql", "w");
    fwrite($sql_file_h, $sql_insert);
    fclose($sql_file_h);
    
    echo json_encode($reponse);
    
    //if (isset($_POST['param']))
        die();
}


add_action('wp_ajax_import_sql', 'import_sql');
function import_sql($param = null)
{
    global $sql_files, $wpdb;
    
    if (isset($_POST['param']))
        $param = $_POST['param'];
    
    if ($param !== null)
    {
        $reponse = array();
        $sql_id = $param;
        
        $wpof_sql = file_get_contents(wpof_path . "init/".$sql_id.".sql");
        $wpof_sql = str_replace("{table_prefix}", $wpdb->prefix, $wpof_sql);
        if ($wpdb->query($wpof_sql))
            $reponse['log'] = __("Import réalisé avec succès");
        else
            $reponse['log'] = "<p class='erreur'>".__("Échec !")."</p>".$wpof_sql;
    }
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        echo json_encode($reponse);
        die();
    }
}    

add_action('wp_ajax_import_json', 'import_json');
function import_json($param = null)
{
    global $wpof, $type_class;
    
    $reponse = array('log' => '');
    if (isset($_POST['param']))
        $param = $_POST['param'];
    
    if ($param !== null)
    {
        if (post_type_exists($param))
        {
            $post_type = $param;
            
            $filename = wpof_path . "init/".$post_type.".json";
            $class = $type_class[$post_type];
            
            foreach(json_decode(file_get_contents($filename)) as $object)
            {
                $new_object = new $class();
                foreach(get_object_vars($object) as $key => $val)
                    $new_object->$key = $val;
                ob_start();
                $new_object->wp_insert();
                $reponse['log'] .= '<pre>'.ob_get_clean().'</pre>';
                $reponse['log'] .= "<p>".$new_object->titre." / ".$new_object->desc." / ".$new_object->id."</p>";
            }
        }
        elseif ($param == 'options')
        {
            $filename = wpof_path . "init/options.json";
            //$reponse['log'] = '<pre>'.var_export(json_decode(file_get_contents($filename), true), true).'</pre>';
            foreach(json_decode(file_get_contents($filename), true) as $name => $value)
            {
                $res = add_option($name, $value);
                if ($res === false)
                    $res = update_option($name, $value);
                $class = ($res) ? 'succes' : 'erreur';
                $reponse['log'] .= '<p class="'.$class.'">'.$name.' / '.var_export($value, true).'</p>';
            }
        }
    }
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        echo json_encode($reponse);
        die();
    }
}
?>
