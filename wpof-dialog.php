<?php
/*
 * wpof-dialog.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

add_action('wp_ajax_the_dynamic_dialog', 'the_dynamic_dialog');
add_action('wp_ajax_nopriv_the_dynamic_dialog', 'the_dynamic_dialog');
function the_dynamic_dialog()
{
    $function = null;
    if (isset($_POST['function']) && function_exists($_POST['function']))
        $function = $_POST['function'];

    $form_class = esc_attr($function);
    $dialog_class = $form_class ? $form_class . '-dialog' : '';

    $param = array();
    switch ($function) 
    {
        case 'new_session':
            $titre = __("Créez une nouvelle session");
            $param = $_POST;
            break;
        case 'new_formation':
            $titre = __("Créez une nouvelle formation");
            break;
        case 'select_client':
            if (isset($_POST['client_id']))
            {
                $titre = __("Connecter un client à la base distante");
                $param['client_id'] = $_POST['client_id'];
            }
            else
                $titre = __("Ajouter un client à la session");
            $param['session_id'] = $_POST['session_id'];
            break;
        case 'new_client':
            $titre = __("Créez un nouveau client");
            $param['session_id'] = $_POST['session_id'];
            break;
        case 'facturer_client':
            $titre = __("Facturer le client");
            $param['session_id'] = $_POST['session_id'];
            $param['client_id'] = $_POST['client_id'];
            break;
        case 'copy_move_client':
            $titre = __("Copiez ou déplacez nouveau client vers une autre session");
            $param['session_id'] = $_POST['session_id'];
            $param['client_id'] = $_POST['client_id'];
            break;
        case 'new_stagiaire':
            $titre = __("Inscrivez un⋅e stagiaire");
            $param['session_id'] = $_POST['session_id'];
            $param['client_id'] = $_POST['client_id'];
            break;
        case "add_or_edit_creneau":
            $titre = __("Modifiez un créneau de formation");
            $param = $_POST;
            break;
        case "suivi_creneau":
            $titre = __("Suivi stagiaire sur ce créneau");
            $param = $_POST;
            break;
        case 'premier_contact':
            $titre = __("Contactez-nous");
            $param = $_POST;
            break;
        case 'new_token_form':
            $titre = __("Obtenir un nouveau lien d'accès");
            $param['session_id'] = $_POST['session_id'];
            break;
        case "debug_SESSION":
            $titre = __("Variables de \$_SESSION");
            break;
        case "sql_session_formation":
            $titre = __("Requêtes SQL SELECT sur la session");
            $param['session_id'] = $_POST['session_id'];
            if (isset($_POST['client_id'])) $param['client_id'] = $_POST['client_id'];
            if (isset($_POST['stagiaire_id'])) $param['stagiaire_id'] = $_POST['stagiaire_id'];
            break;
        case "help_dialog":
            $titre = __("Aide en ligne");
            $param['slug'] = $_POST['aide_slug'];
            break;
        case "simple_editor_dialog":
            $titre = $_POST['data']['title'];
            $param = $_POST;
            break;
        case "remove_formation_form":
            $titre = __("Supprimer la formation");
            //$param['formation_id'] = $_POST['formation_id'];
            $param = $_POST;
            break;
        case "relink":
            $titre = __("Rattacher les entités");
            $param = $_POST;
            break;
        case "edit_content_cat":
            $titre = __("Créer ou modifier une catégorie de contenu");
            $param = $_POST;
            break;
        default:
            $titre = __("Fonction inconnue…");
            break;
    }
    
    
    ?>
    <div class="dialog edit-data <?php echo $dialog_class; ?>" style="" title="<?php echo $titre; ?>">
    <?php if ($function) : ?>
        <form class="<?php echo $form_class; ?>">
        <?php
        if (count($param) > 0)
            echo $function($param);
        else
            echo $function();
        ?>
        </form>
        <p class='message'></p>
    <?php else : ?>
        <p class="erreur"><?php _e("Aucune fonction pour afficher le contenu de cette boîte de dialogue."); ?></p>
        <input type='hidden' name='no_valid' value='1' />
    <?php endif; ?>
    </div>
    
    <?php
    die();
}

/*
 * Formulaire pour créer une nouvelle fiche de formation
 */
function new_formation()
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    $formation = new Formation();
    
    echo get_aide("new_formation");
    if (in_array($role, array('um-responsable', 'admin')))
        echo get_icone_aide("new_formation", "Conseil rapide pour créer une nouvelle formation");
    echo get_input_jpost($formation, "titre", array('input' => 'text', 'label' => __("Titre de la formation"), 'size' => '80'));
    
    if (init_term_list("formateur"))
    {
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        if ($role == "um_formateur-trice")
            $formation->formateur[] = $current_user_id;
        echo get_input_jpost($formation, "formateur", array('select' => 'multiple', 'label' => __("Équipe pédagogique")));
    }
    
    // Nom de l'action a effectuer ensuite
    echo "<input type='hidden' name='action' value='add_new_formation' />";
    //echo "<input type='hidden' name='close_on_valid' value='1' />";
}

/*
 * Formulaire pour créer une nouvelle session de formation
 */
function new_session($param)
{
    global $wpof;
    $session = new SessionFormation();
    $param = $param['data'];
    $current_user_id = get_current_user_id();
    $role = wpof_get_role($current_user_id);

    $soustraitance = false;
    if (isset($param['type']) && $param['type'] == 'opac')
        $soustraitance = true;
    
    if ($wpof->role == "um_formateur-trice") {
        $args = array('formateur' => $current_user_id);
        $session->formateur[] = $current_user_id;
    }
    else
        $args = array();
    ?>

    <?php if ($soustraitance) : ?>
    <div class="threecols">
    <?php endif; ?>
    <div>
    <label class="top"><?php _e("Intitulé de session unique"); ?></label>
    <input type="text" id="session_unique_titre" name="session_unique_titre" size="80" />
    </div>
    <?php
    if (init_term_list("formateur") && (in_array($role, $wpof->super_roles) || !$soustraitance))
    {
        echo get_input_jpost($session, "formateur", array('select' => 'multiple', 'label' => __("Équipe pédagogique")));
    }
    else
    { ?>
        <input type='hidden' name='formateur' value='<?php echo $current_user_id; ?>' />
        <?php
    }
    
    
    if ($soustraitance) : ?>
    <?php
    $plugin = $wpof->plugin_manager->getPlugin();
    
    // Constitution de la liste (ou des listes) de clients à afficher
    $clients = array();
    if ($plugin->canGetClients() !== false)
    {
        try {
            foreach ($session->formateur as $fid)
                $clients = array_merge_recursive($clients, $plugin->getClients(get_formateur_by_id($fid)));
        } catch (\Throwable $th) {
            ?>
            <p class="erreur"><?php _e("Récupération des clients impossible !"); ?></p>
            <span class="openButton" data-id="error-message"><?php _e("Voir le message d'erreur"); ?></span>
            <input type='hidden' name='no_valid' value='1' />
            <div class="blocHidden" id="error-message"><?php echo $th->getMessage(); ?></div>
            <?php error_log($th->getMessage());
            die;
        }
    }

    ?>
    <div>
    <label class="top"><?php _e("Nom de l'organisme qui vous sous-traite la prestation"); ?></label>
    <?php if (!empty($clients)) : 
        $ec = Client::ENTITE_PERS_MORALE; ?>
        <select name="client_id[<?php echo $ec; ?>]">
        <?php foreach (array_unique($clients[$ec]) as $id => $name) : ?>
            <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
        <?php endforeach; ?>
        </select>
        <p><?php _e("Le client n'est pas dans la liste ?"); ?> <?php echo $plugin->getCreateClientUrl($session); ?></p>
    <?php else: ?>
    <input name="nom" type="text" />
    <?php endif; ?>
    </div>
    </div> <!-- threecols -->
    <?php if (!in_array($role, $wpof->super_roles)) : ?>
    <div class="openButton icone-bouton" data-id="soustraitance_more"><?php _e("Si la session a déjà eu lieu, ajoutez les informations complémentaires"); ?></div>
    <?php endif; ?>
    <div class="<?php if (!in_array($role, $wpof->super_roles)) : ?>blocHidden<?php endif; ?>" id="soustraitance_more">
        <p><?php _e("Ne remplir les champs ci-dessous qu'en cas de saisie à postériori"); ?></p>
        <?php if (!in_array($role, $wpof->super_roles)) : ?>
        <p class="important"><?php _e("Pensez à déposer le contrat de sous-traitance dans la fiche client !"); ?></p>
        <?php endif; ?>
        <div class="threecols">
        <textarea name="dates" rows="10" placeholder="<?php _e("Dates, au format JJ/MM/AAAA, une par ligne"); ?>"></textarea>
        <?php if ($plugin->canGetClients() === false) : ?>
        <input name="numero_facture" type="text" placeholder="<?php _e("Numéro de facture"); ?>" />
        <?php endif; ?>
        <input name="num_of" type="text" placeholder="<?php _e("Numéro de déclaration d'activité"); ?>" />
        <input name="tarif_total_chiffre" type="number" step="0.01" placeholder="<?php _e("Tarif total facturé HT"); ?>" />
        <input name="tarif_total_autres_chiffre" type="number" step="0.01" placeholder="<?php _e("Si mentionné sur facture, tarif des frais non pédagogiques (HT)"); ?>" />
        <input name="nb_stagiaires" type="number" step="1" min="1" placeholder="<?php _e("Nombre de stagiaires"); ?>" />
        <input step="0.25" min="0" name="nb_heure_estime_decimal" type="number" placeholder="<?php _e("Durée de la session de formation (en heures)"); ?>" />
        <span class="icone-bouton" id="calcul-cumul"><?php _e("Calcul"); ?></span>
        <input name="nb_heures_stagiaires" type="number" step="0.25" min="0" placeholder="<?php _e("Cumul heures×stagiaires"); ?>" style="width: 75%;" />
        <div><strong><?php _e("À la validation :") ?></strong></div>
        <div><input type="checkbox" name="reload_url" value="1" <?php if (!in_array($role, $wpof->super_roles)) : ?> checked="checked"<?php endif; ?>/> <label><?php _e("Recharger la page sur la session nouvellement créée"); ?></label></div>
        <div><input type="checkbox" value="1" name="reset_on_success" <?php if (in_array($role, $wpof->super_roles)) : ?> checked="checked"<?php endif; ?> /> <label><?php _e("Effacer ce formulaire pour saisir une nouvelle session"); ?></label></div>
        <input type='hidden' name='focus_to' value='session_unique_titre' />
        </div>
    </div>
    <script>
    jQuery(document).ready(function($)
    {
        $("#calcul-cumul").click(function() { calcul_cumul_heures_stagiaires("#soustraitance_more"); });
    });
    </script>
    <input type='hidden' name='type_client' value='opac' />
    <?php endif; ?>
    <input type='hidden' name='admin' value='<?php echo (in_array($role, $wpof->super_roles)) ? 1 : 0; ?>' />
    <input type='hidden' name='action' value='add_new_session' />
    <?php 
}

/*
 * Formulaire pour créer un nouveau client
 */
function new_client($param)
{
    global $wpof;
    $session_id = $param['session_id'];
    $session = get_session_by_id($session_id);
    $particulier_only = (in_array($session->nature_formation, array('bilan', 'vae'))) ? true : false;
    
    if (!$particulier_only) :
    ?>
    <p><?php _e("Le client est :"); ?></p>
    <p><input type="radio" name="type_client" id="<?php echo Client::FINANCEMENT_OPAC; ?>" value="<?php echo Client::FINANCEMENT_OPAC; ?>" class="type_client" /><label for="<?php echo Client::FINANCEMENT_OPAC; ?>"><?php _e("Un organisme de formation qui vous sous-traite la prestation"); ?></label><br />
    <input type="radio" name="type_client" id="<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>" value="<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>" class="type_client" /><label for="<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>"><?php _e("Un particulier (personne physique)"); ?></label><br />
    <input type="radio" name="type_client" id="<?php echo Client::ENTITE_PERS_MORALE; ?>" checked="checked" value="<?php echo Client::ENTITE_PERS_MORALE; ?>" class="type_client" /><label for="<?php echo Client::ENTITE_PERS_MORALE; ?>"><?php _e("Toute autre personne morale"); ?></label></p>
    <?php else: ?>
    <p><?php printf(__("Cette session est de type <strong>%s</strong>, votre client est forcément un⋅e particulier⋅e"), $wpof->nature_formation->get_term($session->nature_formation)); ?></p>
    <input type='hidden' name='type_client' value='<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>' />
    <?php endif; ?>
    
    <div class="input_jpost prenom text" <?php if (!$particulier_only) : ?> style="display: none" <?php endif; ?>>
    <label class="top input_jpost_label"><?php _e("Prénom"); ?></label>
    <input type="text" class="input_jpost_value" name="prenom" value="" />
    </div>
    
    <div class="input_jpost nom text">
    <label class="top input_jpost_label"><?php echo __("Nom"); ?><?php if (!$particulier_only) : ?><span class='raison_sociale'> <?php _e("ou raison sociale"); ?></span><?php endif; ?></label>
    <input type="text" class="input_jpost_value" name="nom" value="" />
    </div>
    <?php
    //echo get_input_jpost($client, "contact", array('input' => 'text', 'label' => __('Nom du contact')));
    echo hidden_input("session_id", $session_id);
    
    // Nom de l'action a effectuer ensuite
    ?>
    <input type='hidden' name='action' value='add_new_client' />
    <input type='hidden' name='close_on_valid' value='1' />
    <script>
        var $ = jQuery;
        $('input[name="type_client"').change(function(e)
        {
            console.log($(".type_client:checked").val());
            if ($(".type_client:checked").val() == '<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>')
            {
                $(".dialog div.prenom").show();
                $(".dialog span.raison_sociale").hide();
            }
            else
            {
                $(".dialog div.prenom").hide();
                $(".dialog span.raison_sociale").show();
            }
        });
    </script>
    <?php
}

/*
 * Formulaire pour ajouter un client dans la session
 */
function select_client($param)
{
    global $wpof;
    $plugin = $wpof->plugin_manager->getPlugin();
    if ($plugin->canGetClients() === false)
    {
        new_client($param);
    }
    else
    {
        $session_id = $param['session_id'];
        $session = get_session_by_id($session_id);
        $particulier_only = (in_array($session->nature_formation, array('bilan', 'vae'))) ? true : false;
        
        // Si je suis responsable de formation, j'ai accès à tous les clients des formateur⋅ices de cette session
        if (in_array($wpof->role, $wpof->super_roles))
            $formateur_id = $session->formateur;
        // sinon, je n'ai accès qu'à mes clients
        else
            $formateur_id = array(get_current_user_id());
        
        // Constitution de la liste (ou des listes) de clients à afficher
        $clients = array();
        try {
            foreach ($formateur_id as $fid)
                $clients = array_merge_recursive($clients, $plugin->getClients(get_formateur_by_id($fid)));
        } catch (\Throwable $th) {
            ?>
            <p class="erreur"><?php _e("Récupération des clients impossible !"); ?></p>
            <span class="openButton" data-id="error-message"><?php _e("Voir le message d'erreur"); ?></span>
            <input type='hidden' name='no_valid' value='1' />
            <div class="blocHidden" id="error-message"><?php echo $th->getMessage(); ?></div>
            <?php error_log($th->getMessage());
            die;
        }
        
        if (!$particulier_only) :
        ?>
            <p><?php _e("Le client est :"); ?></p>
            <p><input type="radio" name="type_client" id="opac" value="<?php echo Client::FINANCEMENT_OPAC; ?>" class="type_client" /><label for="opac"><?php _e("Un organisme de formation qui vous sous-traite la prestation"); ?></label><br />
            <input type="radio" name="type_client" id="part" value="<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>" class="type_client" /><label for="part"><?php _e("Un particulier (personne physique)"); ?></label><br />
            <input type="radio" name="type_client" id="autre" checked="checked" value="<?php echo Client::ENTITE_PERS_MORALE; ?>" class="type_client" /><label for="autre"><?php _e("Toute autre personne morale"); ?></label></p>
        <?php else: ?>
            <p><?php printf(__("Cette session est de type <strong>%s</strong>, votre client est forcément un⋅e particulier⋅e"), $wpof->nature_formation->get_term($session->nature_formation)); ?></p>
            <input type='hidden' name='type_client' value='<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>' />
        <?php endif; ?>

        <?php
            $session_clients_ext_ids = [];
            foreach ($session->clients as $client_id) {
                $session_client = get_client_by_id($session_id, $client_id);
                $session_clients_ext_ids[$session_client->external_id] = $session_client->external_id;
            }
        ?>
        <?php foreach(array(Client::ENTITE_PERS_PHYSIQUE, Client::ENTITE_PERS_MORALE) as $ec) : ?>
            <div class="input_jpost nom select" id="select_<?php echo $ec; ?>" style="display: <?php echo ($ec == Client::ENTITE_PERS_PHYSIQUE) ? "none" : "block"; ?>">
                <?php if (!empty(array_diff_key($clients[$ec],$session_clients_ext_ids))) :
                    asort($clients[$ec], SORT_NATURAL | SORT_FLAG_CASE); ?>
                    <label class="top input_jpost_label"><?php printf(__("Sélectionnez la personne %s"), $ec); ?></label>
                    <select name="client_id[<?php echo $ec; ?>]">
                        <?php foreach (array_unique($clients[$ec]) as $id => $name) : ?>
                            <?php if (! in_array($id, $session_clients_ext_ids)): ?>
                            <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                <?php else: ?>
                    <div class="bg-alerte faux-bouton"><?php the_wpof_fa('warning'); ?> Vous n'avez pas de client personne <?php echo $ec; ?> ajoutable pour cette session, <?php echo $plugin->getCreateClientUrl($session, 'créez-en un !'); ?></div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        <p><?php _e("Le client n'est pas dans la liste ?"); ?> <?php echo $plugin->getCreateClientUrl($session); ?></p>
        <?php
        echo hidden_input("session_id", $session_id);
        
        // Nom de l'action a effectuer ensuite
        if (isset($param['client_id'])) :?>
        <input type='hidden' name='action' value='connect_client_externalid' />
        <input type='hidden' name='opaga_client_id' value='<?php echo $param['client_id']; ?>' />
        <input type='hidden' name='reload_on_valid' value='1' />
        <?php else: ?>
        <input type='hidden' name='action' value='add_client_session' />
        <?php endif; ?>
        <input type='hidden' name='close_on_valid' value='1' />

        <script>
        jQuery(document).ready(function($)
        {
            $(".type_client").change(function(e)
            {
                if ($(this).val() == "<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>")
                {
                    $("#select_<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>").show();
                    $("#select_<?php echo Client::ENTITE_PERS_MORALE; ?>").hide();
                }
                else
                {
                    $("#select_<?php echo Client::ENTITE_PERS_PHYSIQUE; ?>").hide();
                    $("#select_<?php echo Client::ENTITE_PERS_MORALE; ?>").show();
                }
            });
        });
        </script>
        <?php
    }
}

/**
 * Dialogue qui propose le prix à facturer et gérer si on fait un acompte (toutes les conditions sont par remplie pour facturer) ou une facture finale.
 * @param array $params : les paramères session_id et client_id
 * 
 * @see go_facturation() (dans wpof-session-formation.php)
 */
function facturer_client($params)
{
    global $wpof;
    $session = get_session_by_id($params['session_id']);
    $client = get_client_by_id($params['session_id'], $params['client_id']);
    $plugin = $wpof->plugin_manager->getPlugin();
    $factures_passees = $plugin->getFacturesClient($client, $session);

    ob_start(); ?>
    <input type='hidden' name='session_id' value='<?php echo $params['session_id']; ?>' />
    <input type='hidden' name='client_id' value='<?php echo $params['client_id']; ?>' />
    <input type='hidden' name='close_on_valid' value='1' />
    <?php $hidden_fields = ob_get_clean(); ?>

    <?php if (!$session->is_session_ended()): ?>
            <?php $manque = $client->get_missing_legal(); ?>
            
            <?php if (empty($manque)): ?>
                <?php $is_really_acompte = ($client->acompte > 0 && $factures_passees['montant_total_ht'] == 0); ?>

                <?php if ($is_really_acompte) : ?>
                    <h2><?php _e("Facture d'acompte"); ?></h2>
                    <p><?php printf(__("La facture est pré-remplie avec l'acompte de <strong>%s</strong>, ne modifiez pas cette valeur pour rester conforme au contrat !"), get_tarif_formation($client->acompte)); ?></p>
                    <input type='hidden' name='is_acompte' value='1' />
                <?php else: ?>
                    <h2><?php _e("Facture intermédiaire"); ?></h2>
                    <p><?php _e("Vous allez éditer une <strong>facture intermédiaire</strong>. Saisissez vous-même le montant convenu avec votre client."); ?></p>
                    <?php if ($factures_passees['montant_total_ht'] == 0) : ?>
                    <p><?php printf(__("Pour information vous n'avez rien facturé pour l'instant et le total contractualisé est de %s"), get_tarif_formation($client->tarif_total_chiffre)); ?></p>
                    <?php else : ?>
                    <p><?php _e("Pour information"); ?></p>
                    <ul>
                        <li><?php printf(__("Déjà facturé %s"), get_tarif_formation($factures_passees['montant_total_ht'])); ?></li>
                        <li><?php printf(__("Total contractualisé %s"), get_tarif_formation($client->tarif_total_chiffre)); ?></li>
                        <li><?php printf(__("Reste à facturer <strong>%s</strong>"), get_tarif_formation($client->tarif_total_chiffre - $factures_passees['montant_total_ht'])); ?></li>
                    </ul>
                    <?php endif; ?>
                    <input type='hidden' name='is_acompte' value='0' />
                <?php endif; ?>
                <p><?php _e("Note : <strong>vous ne pouvez pas facturer le solde</strong> tant que la session n'est pas achevée !"); ?></p>
                <p class="important"><?php _e("Relisez bien votre facture, complétez les manques avant de la sauvegarder !"); ?></p>
                <p class="alerte"><?php _e("En validant votre navigateur devrait ouvrir un nouvel onglet. Si ce n'est pas le cas, vérifiez les réglages ou alertes du navigateur."); ?></p>

                <?php echo $hidden_fields; ?>
                <input type='hidden' name='action' value='go_facturation' />
                <input type='hidden' name='is_solde' value='0' />
            <?php else: ?>
                <p class="erreur"><?php _e("Vous ne pouvez pas facturer pour les raisons suivantes :"); ?></p>
                <input type='hidden' name='no_valid' value='1' />
                <ul>
                    <?php foreach($manque as $raison): ?>
                        <li><?php echo $raison; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
    <?php elseif($factures_passees['montant_total_ht'] >= $client->tarif_total_chiffre): ?>
            <p><?php _e('Le montant total a déjà été facturé pour ce client !'); ?> <a class="close" target="_blank" href="<?php echo $plugin->getFacturesClientUrl($client, $session, false); ?>"><?php _e('Voir la ou les factures'); ?></a></p>
            <input type='hidden' name='no_valid' value='1' />
    <?php else: ?>
            <?php $manque = $client->get_missing_legal(); ?>

            <?php if (empty($manque)): ?>
                <h2><?php _e('Facture de solde'); ?></h2>
                <p><?php printf(__("En cliquant sur <strong>Valider</strong>, vous allez facturer le solde, soit un montant de <strong>%s</strong>"), get_tarif_formation($client->tarif_total_chiffre)); ?></p>
                <p><?php printf(__("Ce montant est défini dans la partie %s."), '<a href="#financement" class="close" >Financement</a>'); ?></p>
                <p><?php _e("Seront déduits de ce montant les éventuelles factures intermédiaires."); ?></p>
                <p class="important"><?php _e("Relisez bien votre facture, complétez les manques avant de la sauvegarder !"); ?></p>
                <p class="alerte"><?php _e("En validant votre navigateur devrait ouvrir un nouvel onglet. Si ce n'est pas le cas, vérifiez les réglages ou alertes du navigateur."); ?></p>

                <?php echo $hidden_fields; ?>
                <input type='hidden' name='is_solde' value='1' />
                <input type='hidden' name='is_acompte' value='0' />
                <input type='hidden' name='action' value='go_facturation' />
            <?php else: ?>
                <p class="erreur"><?php _e("Vous ne pouvez pas facturer pour les raisons suivantes :"); ?></p>
                <input type='hidden' name='no_valid' value='1' />
                <ul>
                    <?php foreach($manque as $raison): ?>
                        <li><?php echo $raison; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

    <?php endif; ?>
    <?php
}

function copy_move_client($param)
{
    global $wpof;
    
    $session_source = get_session_by_id($param['session_id']);
    if (in_array($wpof->role, $wpof->super_roles))
        $formateur_id = $session_source->formateur[0];
    else
        $formateur_id = get_current_user_id();
    
    $unsorted_sessions = $wpof->get_sessions($formateur_id);
    $sessions_futur = array();
    $sessions_passe = array();
    $current_timestamp = time();
    $nodate = 1;
    
    foreach($unsorted_sessions as $s)
    {
        if (empty($s->first_date_timestamp))
        {
            $s->first_date_timestamp = $current_timestamp + $nodate;
            $nodate++;
        }
        if ($s->first_date_timestamp < $current_timestamp)
            $sessions_passe[$s->first_date_timestamp] = $s;
        else
            $sessions_futur[$s->first_date_timestamp] = $s;
    }
    
    krsort($sessions_passe);
    krsort($sessions_futur);
    
    ?>
    <p><?php _e("Souhaitez-vous "); ?> 
    <label><input type="radio" name="choix_cp_mv" value="copy" /><?php _e("copier"); ?> </label>
    <label><input type="radio" name="choix_cp_mv" value="move" /><?php _e("déplacer"); ?> </label>
    </p>
    <label for="session_dest_id"><?php _e("Vers la session"); ?></label>
    <select id="session_dest_id" name="session_dest_id">
    <optgroup label="<?php _e('Sessions futures'); ?>">
    <?php foreach($sessions_futur as $s) : ?>
        <?php if ($s->id != $session_source->id) : ?>
            <option value="<?php echo $s->id; ?>"><?php echo $s->titre_session." – ".$s->dates_texte; ?></option>
        <?php endif; ?>
    <?php endforeach; ?>
    </optgroup>
    <option value="<?php echo $session_source->id; ?>">! <?php echo $session_source->titre_session." – ".__("session actuelle !"); ?></option>
    <optgroup label="<?php _e('Sessions passées'); ?>">
    <?php foreach($sessions_passe as $s) : ?>
        <?php if ($s->id != $session_source->id) : ?>
            <option value="<?php echo $s->id; ?>"><?php echo $s->titre_session." – ".$s->dates_texte; ?></option>
        <?php endif; ?>
    <?php endforeach; ?>
    </optgroup>
    </select>
    <input type='hidden' name='action' value='copie_ou_deplace_client' />
    <input type="hidden" name="session_source_id" value="<?php echo $param['session_id']; ?>" />
    <input type="hidden" name="client_id" value="<?php echo $param['client_id']; ?>" />
    <input type='hidden' name='reload_on_valid' value='1' />
    <input type='hidden' name='valid_once' value='1' />
    
    <p id="session_dest" class="message"></p>
    <?php
}

/*
 * Formulaire pour créer de nouveaux stagiaires
 */
function new_stagiaire($param)
{
    ?>
    <input type="hidden" name="session_id" value="<?php echo $param['session_id']; ?>" />
    <input type="hidden" name="client_id" value="<?php echo $param['client_id']; ?>" />
    <input type='hidden' name='action' value='add_stagiaire' />
    
    <?php if (champ_additionnel("genre_stagiaire")) : ?>
    <label class="top" for="genre"><?php _e("Genre"); ?></label> <?php echo wpof_needed_icon(); ?><select name="genre" id="genre"><option value="Madame">Madame</option><option value="Monsieur">Monsieur</option></select>
    <?php endif; ?>
    <label class="top" for="prenom"><?php _e("Prénom"); ?></label> <?php echo wpof_needed_icon(); ?><input type="text" name="prenom" id="prenom" />
    <label class="top" for="nom"><?php _e("Nom"); ?></label> <?php echo wpof_needed_icon(); ?><input type="text" name="nom" id="nom" />
    <label class="top" for="email"><?php _e("Courriel, optionnel"); ?></label><input type="text" name="email" id="email" />

    <p><?php _e("<strong>Validez</strong> pour ajouter une personne. Une fois que vous avez ajouté toutes les personnes souhaitées, cliquez sur <strong>Fermer</strong>."); ?></p>
    <input type='hidden' name='reset_on_success' value='1' />
    <input type='hidden' name='focus_to' value='prenom' />
    <?php
    $client = get_client_by_id($param['session_id'], $param['client_id']);
    $reload_url = $client->permalien."#stagiaires";
    ?>
    <input type='hidden' name='reload_on_close' value='<?php echo $reload_url; ?>' />
    <?php
}

/*
 * Formulaire de modification d'un créneau existant ou vide
 */
function add_or_edit_creneau($param)
{
    $creno = new Creneau((integer) $param['creno_id']);
    $creno->init_from_form($param);
    
    echo $creno->get_creneau_form();
    ?>
    <input type='hidden' name='action' value='edit_creneau' />
    <input type='hidden' name='close_on_valid' value='1' />
    <?php
}

function suivi_creneau()
{
    global $wpof, $tinymce_wpof_settings;
    $creno = new Creneau($_POST['data']['creneau']);
    $session = new SessionFormation($_POST['session_id']);
    $stagiaire = new SessionStagiaire($session->id, $_POST['data']['stagiaire']);
    ?>
    <h2><?php _e('Suivi et commentaires'); ?></h2>
    <p><?php echo __('Stagiaire')." <strong>".$stagiaire->get_displayname()."</strong>"; ?></p>
    <p><?php echo __('Créneau')." <strong>".$creno->date." ".$creno->titre."</strong> (".$wpof->type_creneau[$creno->type].")"; ?></p>
    <?php
    $content = (isset($stagiaire->creneaux_suivi[$creno->id])) ? $stagiaire->creneaux_suivi[$creno->id] : "";
    wp_editor($content, 'suivi_creneau', array_merge($tinymce_wpof_settings));
    ?>
    <input type='hidden' name='action' value='edit_suivi_creneau' />
    <input type='hidden' name='close_on_valid' value='1' />
    <input type='hidden' name='session_id' value='<?php echo $session->id; ?>' />
    <input type='hidden' name='stagiaire_id' value='<?php echo $stagiaire->id; ?>' />
    <input type='hidden' name='creno_id' value='<?php echo $creno->id; ?>' />
    <?php
}

/*
 * Créer un formulaire pour l'inscription/demande d'infos/prise de contact des stagiaires
 * La sécurité repose sur des champs cachés et une question
 */
function premier_contact($param)
{
    if (isset($param['session_id']))
    {
        $entite = get_session_by_id($param['session_id']);
        $titre = $entite->titre_session;
        $class = "session";
    }
    elseif (isset($param['formation_id']))
    {
        $entite = get_formation_by_id($param['formation_id']);
        $titre = $entite->titre;
        $class = "formation";
    }
    elseif (isset($param['data']['formateur_id']))
    {
        $entite = get_formateur_by_id($param['data']['formateur_id']);
        $titre = 'Contacter ' . $entite->get_displayname();
        $class = "formateur";
    }
    else
    {
        $entite = $class = null;
        $titre = __("Des renseignements sur notre offre de formation");
    }
    
    ?>
    <h3><?php echo $titre; ?></h3>
    
    <label class="top" id="id" for="identifiant"><?php _e("Choisissez votre identifiant"); ?></label><input id="identifiant" type="text" name="identifiant" />

    <div class="dialog-grid">
    <div class="grid-3"><label class="top" id="pn" for="nom"><?php _e("Nom et prénom"); ?></label><input id="nom" type="text" name="nom" /></div>
    <div class="grid-3"><label class="top" id="s" for="structure"><?php _e("Structure qui m'emploie ou que je gère"); ?></label><input id="structure" type="text" name="structure" /></div>
    <div class="grid-3"><label class="top" id="em" for="email"><?php _e("Email"); ?></label><input id="email" type="email" name="email" /></div>
    <div class="grid-3"><label class="top" id="tel" for="telephone"><?php _e("N° de téléphone (pour être rappelé⋅e)"); ?></label><input id="telephone" type="text" name="telephone" /></div>
    <div class="grid-full"><label class="top" id="c" for="verif"><?php _e("Quel mois de l'année sommes-nous ?"); ?></label><input id="verif" type="text" name="verif" /></div>

    <?php if ($class == "session" || $class == "formation") : ?>
        <div class="grid-full dialog-grid">
        <label class="grid-full"><?php _e("Je souhaite"); ?></label>

        <?php if ($class == "session") : ?>
        <div class="grid-2 radio"><input id="inscription" type="radio" name="choix" value="inscription" /> <label for="inscription"><?php _e("M'inscrire à cette session"); ?></label></div>
        <div class="grid-2 radio"><input id="intra" type="radio" name="choix" value="intra" /> <label for="intra"><?php _e("Savoir comment bénéficier d'une formation de groupe (intra) pour ma structure"); ?></label></div>
        <div class="grid-2 radio"><input id="informations" type="radio" name="choix" value="informations" checked="checked" /> <label for="informations"><?php _e("Simplement prendre contact pour plus d'informations"); ?></label></div>
        <?php endif; ?>
        
        <?php if ($class == "formation") : ?>
            <div class="grid-3 radio"><input id="dates" type="radio" name="choix" value="dates" /> <label for="dates"><?php _e("Connaître les prochaines dates de cette formation"); ?></label></div>
            <div class="grid-3 radio"><input id="informations" type="radio" name="choix" value="informations" checked="checked" /> <label for="informations"><?php _e("Simplement prendre contact pour plus d'informations"); ?></label></div>
        <?php endif; ?>    
        </div>
    <?php endif; ?>

    <div class="grid-full">
        <label class="top" id="txt" for="message"><?php _e("Message"); ?></label>
        <textarea name="message" id="message" cols="80" rows="5"></textarea>
    </div>

    <input type='hidden' name='action' value='first_contact' />
    <input type='hidden' name='valid_once' value='1' />
    <?php if ($class) : ?>
    <input type='hidden' name='<?php echo $class; ?>_id' value='<?php echo $entite->id; ?>' />
    <?php endif; ?>
    </div>

    <?php
}

/*
 * Créer un formulaire pour demander un nouveau lien d'accès pour les stagiaires et les clients
 * La sécurité repose sur des champs cachés et une question
 */
function new_token_form($param)
{
    $session = get_session_by_id($param['session_id']);
    
    ?>
    <h3><?php echo $session->titre_session; ?></h3>
    <label class="top" id="id" for="identifiant"><?php _e("Choisissez votre identifiant"); ?></label><input id="identifiant" type="text" name="identifiant" />
    <label class="top" id="em" for="email"><?php _e("Saisissez votre email"); ?></label><input id="email" type="email" name="email" />
    <label class="top" id="c" for="verif"><?php _e("Quel mois de l'année sommes-nous ?"); ?></label><input id="verif" type="text" name="verif" />
    <input type='hidden' name='action' value='get_new_token' />
    <input type='hidden' name='session_id' value='<?php echo $session->id; ?>' />
    <?php
}

/*
 *
 */
function help_dialog($param)
{
    $slug = $param['slug'];
    
    $aide = new Aide($slug);
    echo $aide->get_aide();
    
    $role = wpof_get_role(get_current_user_id());
    if (in_array($role, array("um_responsable", "admin"))) : ?>
    <input type='hidden' name='action' value='update_aide' />
    <?php endif; ?>

    <input type='hidden' name='close_on_valid' value='1' />
    <?php
}

function simple_editor_dialog($param)
{
    global $tinymce_wpof_settings;
    $eventsfunc_choices = ['update_keyword_default_text'];
    if (in_array($param['data']['eventfunc'], $eventsfunc_choices)) :
    wp_editor($param['data']['contenu'], 'contenu', array_merge($tinymce_wpof_settings, array('textarea_rows' => 5, 'textarea_name' => 'contenu')));
    ?>
    <input type='hidden' name='key' value='<?php echo $param['data']['key']; ?>' />
    <input type='hidden' name='eventsfunc' value='<?php echo $param['data']['eventfunc']; ?>' />
    <input type='hidden' name='close_on_valid' value='1' />
    <?php
    else : ?>
    <p class="erreur"><?php printf(__("Fonction d'événement non reconnue %s"), $param['data']['eventfunc']); ?></p>
    <input type='hidden' name='no_valid' value='1' />
    <?php
    endif;
}

function remove_formation_form($param)
{
    global $Formation, $wpof;
    
    if ($param['data']['objectclass'] == "Formation")
    {
        $old_formation_id = $param['data']['id'];
        $formation = new Formation($old_formation_id);
        echo get_icone_aide("formation_supprimer", __("Supprimer une formation")); ?>
        <h3><?php printf(__("Vous souhaitez supprimer la formation %s"), $formation->titre); ?></h3>
        <?php 
        $session_id = $formation->get_session_links();
        if (!empty($session_id)) :
            $session = array();
            ?>
            <p><?php _e("Elle est liée aux sessions suivantes"); ?></p>
            <ul>
            <?php
            foreach($session_id as $sid) :
                $session[$sid] = new SessionFormation($sid);
                ?>
                <li><?php echo $session[$sid]->get_a_link(); ?> (<?php echo $session[$sid]->dates_texte; ?>)</li>
                <input type="hidden" name="session[]" value="<?php echo $sid; ?>" />
                <?php
            endforeach;
            ?>
            </ul>
            <?php
            $all_formations = $wpof->get_actions_id("formation");
            $all_formations = array_diff($all_formations, array($old_formation_id));
            $formateur_formations = array();
            foreach($formation->formateur as $user_id)
                $formateur_formations = array_merge($formateur_formations, $wpof->get_actions_id("formation", $user_id));
            $formateur_formations = array_unique($formateur_formations);
            $formateur_formations = array_diff($formateur_formations, array($old_formation_id));
    
            foreach($all_formations as $fid)
                $Formation[$fid] = get_formation_by_id($fid);
            ?>
            <h3><?php _e("Que souhaitez-vous faire de ces sessions ?"); ?></h3>
            <p>
            <input type="radio" name="session_action" value="remove_all" id="remove_all" /> <label for="remove_all"><?php _e("Supprimer également les sessions au lieu de les rattacher !"); ?></label>
            </p>
            <p>
            <input type="radio" name="session_action" value="detach" id="detach" /> <label for="detach"><?php _e("Détacher les sessions (la formation est supprimée, mais les sessions ne sont plus attachées à une formation)"); ?></label>
            </p>
            <p>
            <input type="radio" name="session_action" value="attach" id="attach" /> <label for="attach"><?php _e("Rattacher les sessions à une autre formation à choisir ci-dessous"); ?></label>
            </p>
            <p><?php _e("Formations de la même équipe pédagogique"); ?></p>
            <select class="simple-select2" multiple="multiple" name="new_formation_id">
            <?php
            foreach($formateur_formations as $fid) : ?>
                <option value="<?php echo $fid; ?>"><?php echo $Formation[$fid]->titre; ?></option>
            <?php endforeach; ?>
            </select>
            <?php if (in_array($wpof->role, $wpof->super_roles)) : ?>
            <p><?php _e("ou toutes les formations"); ?></p>
            <select class="simple-select2" multiple="multiple" name="new_formation_id_all">
            <?php
            foreach($all_formations as $fid) : ?>
                <option value="<?php echo $fid; ?>"><?php echo $Formation[$fid]->titre; ?> / <?php echo $Formation[$fid]->get_formateurs_noms(); ?></option>
            <?php endforeach; ?>
            </select>
            <?php endif; ?>
            <script>
            jQuery(document).ready(function($)
            {
                $('select.simple-select2').select2({
                    placeholder: 'Cliquez pour choisir',
                    width: '100%',
                    maximumSelectionLength: 1,
                });
                $('button.valider').prop('disabled', true);
                $('input[name="session_action"]').on('change', '', function(e) {$('button.valider').prop('disabled', false);});
            });
            </script>
        <?php endif; ?>
            <input type='hidden' name='formation_id' value='<?php echo $param['data']['id']; ?>' />
            <input type='hidden' name='action' value='remove_formation' />
            <input type='hidden' name='close_on_valid' value='1' />
            <input type='hidden' name='reload_on_valid' value='1' />
        <?php
    }
}

function relink($param)
{
    $data = $param['data'];?>
    <?php
    if (isset($data['stagiaireid']))
    {
        
    }
    else
    {
        $session_from_client = new SessionFormation($data['sessionid']);
        $client = new Client($session_from_client->id, $data['clientid']);
        
        echo '<p>Le client indique être lié à la session ['.$session_from_client->get_id_link().'] '.$session_from_client->titre_session.'</p>';
        $session_clients = get_raw_data("session", "clients");
        $session_match = array();
        foreach($session_clients as $sid => $clients)
        {
            if (is_array($clients) && in_array($client->id, $clients))
                $session_match[] = $sid;
        }
        foreach($session_match as $sid)
        {
            $session = get_session_by_id($sid);
            echo '<p>Le client est référencé dans la session ['.$session->get_id_link().'] '.$session->titre_session.'</p>';
        }
        
    }
}

/**
 * Affiche le dialog de création ou modification de catégorie
 * @param array $param
 * @return void
 */
function edit_content_cat(Array $param)
{
    if (isset($param['data']['slug'])) :
        $cat = OpagaContent::get_category($param['data']['slug']);
        if ($cat === null) : ?>
        <p class="erreur"><?php printf(__("La catégorie [%s] n'existe pas"), $param['data']['slug']); ?></p>
        <input type='hidden' name='no_valid' value='1' />
        <?php 
        return; // si le slug n'existe pas, on n'affiche rien et on désactive le bouton valider
        endif; ?>
    <?php else : ?>
    <input type='hidden' name='new_category' value='1' />
    <?php endif;

    if (isset($param['data']['type']))
    {
        $cat = new stdClass();
        $cat->slug = $cat->name = $cat->style = $cat->icon = "";
        $cat->type = $param['data']['type'];
    }

    if (!empty($cat->type)) : ?>
    <input type="hidden" name="type" value="<?php echo $cat->type; ?>" />
    <label class="top" for="slug"><?php _e("Identifiant") ?> <?php echo get_icone_aide('content_categorie_slug'); ?></label>
    <?php if (empty($cat->slug)) : ?>
    <input type="text" name="slug" id="slug" value="<?php echo $cat->slug; ?>" />
    <?php else : ?>
    <div class="succes"><?php echo $cat->slug; ?></div>
    <input type="hidden" name="slug" value="<?php echo $cat->slug; ?>" />
    <?php endif; ?>
    
    <label class="top" for="name"><?php _e("Nom") ?></label>
    <input type="text" name="name" id="name" value="<?php echo $cat->name; ?>" />

    <label class="top" for="icon"><?php _e("Icône") ?> <?php echo get_icone_aide('content_categorie_icone'); ?></label>
    <input type="text" name="icon" id="icon" value="<?php echo $cat->icon; ?>" />
    
    <input type='hidden' name='action' value='edit_content_category' />
    <input type='hidden' name='close_on_valid' value='1' />
    <input type='hidden' name='reload_on_valid' value='1' />
    <?php else : ?>
        <p class="alerte"><?php _e("Aucun type défini !"); ?></p>
    <?php endif;
}

function debug_SESSION()
{
    ?>
    <pre>
    <?php var_dump($_SESSION); ?>
    </pre>
    <?php
}
