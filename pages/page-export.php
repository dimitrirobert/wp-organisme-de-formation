<?php
/*
 * page-export.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


function wpof_load_export_scripts()
{
    wp_enqueue_script('wpof-export', wpof_url."js/wpof-export.js", array('jquery'));
    wp_enqueue_style('wpof-export', wpof_url."css/wpof-export.css");
}
add_action( 'wp_enqueue_scripts', 'wpof_load_export_scripts', 21 );

$WP_users = array();

function get_export_content()
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    
    $html = "";
    
    if (!in_array($role, array("um_formateur-trice", "um_responsable", "admin")))
        return "";
    
    $html .= "<div class='flexrow flextop margin white-board'>";
    // onglet à ouvrir
    $default_main_tab = 0;
    if (isset($_SESSION['main-tabs']))
        $default_main_tab = $_SESSION['main-tabs'];
    echo hidden_input("default_main_tab", $default_main_tab);

    // choix de la plage de dates
    $date_select = new BoardSelectByDate("SessionFormation");
    $html .= $date_select->get_form();
    $date_select->select_by_plage();
    
    // actions pour exporter
    $html .= get_action_export();
    
    $html .= "</div>";
    
    // onglets
    ob_start();
    ?>
    <div id="main-tabs">
    <ul>
            <li><a href="#tab-select"><?php _e("Sélection"); ?></a></li>
            <li><a href="#tab-cols"><?php _e("Colonnes"); ?></a></li>
            <?php if ($role == "admin") : ?>
            <li><a href="#tab-formations"><?php _e("Formations"); ?></a></li>
            <li><a href="#tab-formateurs"><?php _e("Formateur⋅trices"); ?></a></li>
            <?php endif; ?>
    </ul>
        
        <div id="tab-select">
        <?php echo get_selection_export(); ?>
        </div>
        
        <div id="tab-cols">
        <?php echo get_colonnes_export(); ?>
        </div>
        
        <?php if ($role == "admin") : ?>
            <div id="tab-formations">
            <?php echo get_formations_export(); ?>
            </div>
            
            <div id="tab-formateurs">
            <?php echo get_formateurs_export(); ?>
            </div>
        <?php endif; ?>
    
    <?php
    $html .= ob_get_clean();
    
    return $html;
}

function get_selection_export()
{
    $html = "";
    $html .= "<h2>".__("Sélection des sessions")."</h2>";
    $html .= get_tableau_sessions();
    
    return $html;
}

add_action('wp_ajax_get_tableau_sessions', 'get_tableau_sessions');
function get_tableau_sessions()
{
    global $wpof, $SessionFormation;
    $div_id = 'select_session';
    $reponse = array();
    
    $plage = array
    (
        'date_debut' => '01/01/'.$wpof->annee1,
        'date_fin' => date("d/m/Y", time() + (365 * 24 * 60 * 60)),
    );
    
    if (isset($_POST['plage']))
        $plage = $_POST['plage'];
    
    ob_start();
    ?>
    <div id='<?php echo $div_id; ?>'>
    
    <table class="opaga opaga2 selection_export export">
    <thead>
    <tr class="head">
    <th class="thin"><input type="checkbox" name="all" checked="checked" data-list="<?php echo $div_id; ?>"/></th>
    <th><?php _e("Session"); ?></th>
    <th><?php _e("Dates"); ?></th>
    <th><?php _e("Client(s)"); ?></th>
    </tr>
    </thead>
    
    <?php foreach($SessionFormation as $session) : ?>
        <tr class="global_check colonne">
        <td class="center"><input type="checkbox" name="<?php echo $session->id; ?>" checked="checked" /></td>
        <td><a href="<?php echo $session->permalien; ?>"><?php echo $session->titre_session; ?></a></td>
        <td class="center"><?php echo join("<br />", array_keys($session->creneaux)); ?></td>
        <td><?php echo $session->get_clients("nom", "b"); ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
    
    </div>
    <?php
    $html = ob_get_clean();
    
    if (isset($_POST['action']))
    {
        $reponse['log'] = json_encode($_POST);
        $reponse['div_id'] = $div_id;
        $reponse['html'] = $html;
        echo json_encode($reponse);
        die;
    }
    else
        return $html;
}


function get_colonnes_export()
{
    ob_start();
    ?>
    <h2><?php _e("Choix des colonnes"); ?></h2>
    <div class="flexrow">
    <fieldset class="parent">
    <legend><?php _e("Schémas"); ?></legend>
    
    <?php
    echo get_schemas();
    echo get_save_new_schema();
    ?>
    </fieldset>
    
    <fieldset class="parent">
    <legend><?php _e("Actions"); ?></legend>
    <?php echo get_cols_actions(); ?>
    </fieldset>
    
    <fieldset class="parent">
    <legend><?php _e("Filtres"); ?></legend>
    <?php
    echo get_filtres();
    ?>
    </fieldset>
    </div>
    
    <?php
    $user_schemas = get_user_meta(get_current_user_id(), "schema_export", true);
    if (is_array($user_schemas) && count($user_schemas) > 0)
        $schema = reset($user_schemas);
    else
        $schema = new SchemaExport();
        
    echo $schema->get_choix_cols();
    ?>
    
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

function get_action_export()
{
    ob_start();
    ?>
    <fieldset id="action_export">
    <h2><?php _e("Exporter en"); ?><span class="icone-bouton action_export" data-format="csv"><?php the_wpof_fa('file-csv'); ?><?php _e("CSV"); ?></span></h2>
    </fieldset>
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

function get_filtres()
{
    ob_start();
    ?>
    <div id="filtres_export">
        <div class="flewrow">
        <div><?php _e("Afficher/cacher les colonnes non cochées"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_uncheck" data-list="cols-sortable"><?php _e("toutes"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_entite" data-entite="session" data-list="cols-sortable"><?php _e("session"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_entite" data-entite="client" data-list="cols-sortable"><?php _e("client"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_entite" data-entite="stagiaire" data-list="cols-sortable"><?php _e("stagiaire"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_entite" data-entite="perso" data-list="cols-sortable"><?php _e("personnalisées"); ?></div>
        </div>
    </div>
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

function get_cols_actions()
{
    ob_start();
    ?>
    <div id="cols_actions_export">
    <div class="icone-bouton cols_action" data-action="add_col" data-list="cols-sortable"><?php _e("Ajouter une colonne"); ?></div>
    </div>
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

function get_formations_export()
{
    ob_start();
    $formations = get_formations();
    $div_id = "formations";
    global $WP_users;
    
    ?>
    <div class="data inter_opaga" id='<?php echo $div_id; ?>'>
    
    <?php if (is_array($formations)) : ?>
    <h2><?php _e("Exporter des formations"); ?></h2>
    <table class="opaga opaga2 export">
    <thead>
    <tr class="head">
    <th class="thin"><input type="checkbox" name="all" checked="checked" data-list="<?php echo $div_id; ?>"/></th>
    <th><?php _e("Intitulé"); ?></th>
    <th><?php _e("Email Formateur⋅trices"); ?></th>
    <th><?php _e("Éval pré-requis"); ?></th>
    <th><?php _e("Éval objectifs"); ?></th>
    </tr>
    </thead>
    
    <?php foreach ($formations as $f) : ?>
        <tr class="global_check">
        <td class="center"><input type="checkbox" name="<?php echo $f->id; ?>" checked="checked" /></td>
        <td><a href="<?php echo $f->permalien; ?>"><?php echo $f->titre; ?></a></td>
        <td>
            <?php
            $email = array();
            foreach($f->formateur as $user_id)
            {
                if (!isset($WP_users[$user_id]))
                    $WP_users[$user_id] = get_user_by("ID", $user_id);
                $email[] = $WP_users[$user_id]->user_email;
            }
            echo join(";", $email);
            ?>
        </td>
        <td><?php echo $f->quizpr_id; ?></td>
        <td><?php echo $f->quizobj_id; ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
    <div class="icone-bouton inter_opaga_export" data-type="formations" data-format="json"><?php _e("Exporter en JSON pour import dans OPAGA"); ?></div>
    <?php endif; ?>
    
    </div>
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

function get_formateurs_export()
{
    ob_start();
    ?>
    
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}


add_action('wp_ajax_get_schemas', 'get_schemas');
function get_schemas($user_id = -1)
{
    if ($user_id <= 0)
        $user_id = get_current_user_id();
    
    $base_schemas = array();
    $base_schemas[] = new SchemaExport();
    
    ob_start();
    ?>
    <div class="schemas_dispo">
    <?php
    $user_schemas = get_user_meta($user_id, "schema_export", true);
    if (is_array($user_schemas)) :
        foreach($user_schemas as $sch) : ?>
            <div class="icone-bouton" data-schema="<?php echo $sch->id; ?>" data-userid="<?php echo $user_id; ?>" data-destination="#cols-sortable"><span class="select-schema"><?php echo $sch->nom; ?></span> <?php the_wpof_fa_del('delete-schema'); ?></div>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php foreach($base_schemas as $sch) : ?>
        <div class="icone-bouton" data-schema="<?php echo $sch->id; ?>" data-userid="<?php echo $user_id; ?>" data-destination="#cols-sortable"><span class="select-schema"><?php echo $sch->nom; ?></span></div>
    <?php endforeach; ?>
    </div>
    <?php
    $html = preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
    if (isset($_POST['action']))
    {
        echo $html;
        die();
    }
    else
        return $html;
}

add_action('wp_ajax_active_schema', 'active_schema');
function active_schema()
{
    $schema = new SchemaExport($_POST['schema']);
    $schema->the_choix_cols_list();
    
    die();
}

function get_save_new_schema($user_id = -1)
{
    if ($user_id <= 0)
        $user_id = get_current_user_id();
    
    ob_start();
    ?>
    <label for="nom_schema"><?php _e("Nom du nouveau schéma"); ?></label> <input type="text" name="nom_schema" value="" /> <span class="icone-bouton enregistrer-schema" data-userid="<?php echo $user_id; ?>"><?php _e("Enregistrer"); ?></span>
    
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

/*
 * retourne le plus bas niveau de données dans le schéma sélectionné :
 * session
 * client
 * stagiaire
 */
function get_low_level_data($data)
{
    $entite = array();
    foreach($data as $c)
        $entite[$c['entite']] = 1;
    
    if (isset($entite['stagiaire']))
        return 'stagiaire';
    elseif (isset($entite['client']))
        return 'client';
    else
        return 'session';
}

/*
 * Renvoie une ligne d'export
 */
function get_export_row($data, $entities)
{
    $row = array();
    
    foreach($data as $c)
    {
        $entite = $c['entite'];
        if (isset($entities[$entite]))
        {
            $object = $entities[$entite];
            switch($entite)
            {
                case "session":
                case "client":
                case "stagiaire":
                    $key = $c['db_text'];
                    if (isset($object->$key))
                    {
                        if (is_scalar($object->$key))
                            $row[] = $object->$key;
                        else
                            $row[] = serialize($object->$key);
                    }
                    else
                        $row[] = "";
                    break;
                case "perso":
                    $row[] = $c['valeur'];
                    break;
            }
        }
    }
    return $row;
}

add_action('wp_ajax_opaga_export_data', 'opaga_export_data');
function opaga_export_data()
{
    $reponse = array();
    
    $export_path = WP_CONTENT_DIR . "/uploads/";
    
    $format = (isset($_POST['format'])) ? $_POST['format'] : "csv";
    
    $data = array();
    
    $low_level = get_low_level_data($_POST['cols']);
    
    foreach($_POST['sessions'] as $sid)
    {
        $entities = array();
        $row = array();
        $entities['session'] = get_session_by_id($sid);
        
        foreach($entities['session']->clients as $cid)
        {
            $entities['client'] = get_client_by_id($sid, $cid);
            foreach($entities['client']->stagiaires as $stag_id)
            {
                $entities['stagiaire'] = get_stagiaire_by_id($sid, $stag_id);
                
                $row[] = get_export_row($_POST['cols'], $entities);
            }
            unset($entities['stagiaire']);
            if (empty($row))
                $row[] = get_export_row($_POST['cols'], $entities);
        }
        unset($entities['client']);
        if (empty($row))
            $row[] = get_export_row($_POST['cols'], $entities);
        $data = array_merge($row, $data);
    }
    
    $head_row = array();
    foreach($_POST['cols'] as $c)
        $head_row[] = $c['new_text'];
    
    switch($format)
    {
        case 'csv':
            $csv_data = array();
            $csv_data[] = join(';', $head_row);
            foreach($data as $line)
                $csv_data[] = join(';', $line);
            $reponse['data'] = join("\r", $csv_data);
            $reponse['filename'] = "export_".date("Ymd-His").".$format";
            $reponse['mimetype'] = "text/csv";
            break;
    }
    
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_inter_opaga_export_data', 'inter_opaga_export_data');
function inter_opaga_export_data()
{
    global $wpdb, $wpof, $suffix_quiz;
    
    $reponse = array();
    $reponse['log'] = "";

    $format = (isset($_POST['format'])) ? $_POST['format'] : "json";
    $data = array();
    
    global $WP_users;
    
    $type = null;
    
    if (isset($_POST['type']))
        $type = $_POST['type'];
    else
        $reponse['log'] = __("Type non défini");
        
    switch ($type)
    {
        case "formations":
            foreach($_POST['donnees'] as $formation_id)
            {
                $row_post = array();
                $row_meta = array();
                
                $post = get_post($formation_id);
                foreach(array('post_title', 'post_name', 'post_modified', 'post_type') as $field)
                    $row_post[$field] = $post->$field;
                
                $query = $wpdb->prepare("SELECT meta_key, meta_value FROM {$wpdb->prefix}postmeta WHERE post_id = '%d';", $formation_id);
                foreach($wpdb->get_results($query, ARRAY_A) as $m)
                {
                    if ($m['meta_key'] == "formateur")
                    {
                        $email = array();
                        foreach(unserialize($m['meta_value']) as $user_id)
                        {
                            if (!isset($WP_users[$user_id]))
                                $WP_users[$user_id] = get_user_by("ID", $user_id);
                            $email[] = $WP_users[$user_id]->user_email;
                        }
                        $m['meta_value'] = serialize($email);
                    }
                    if (substr($m['meta_key'], 0, 1) != "_" && substr($m['meta_key'], 0, 3) != "um_" && $m['meta_value'] != "")
                        $row_meta[$m['meta_key']] = stripslashes($m['meta_value']);
                }
                
                $quiz = array();
                foreach(array('quizpr_id', 'quizobj_id') as $qid)
                {
                    if (isset($row_meta[$qid]))
                    {
                        $query = $wpdb->prepare("SELECT quiz_id, parent_id, type, subject, title, meta_key, meta_value from {$wpdb->prefix}$suffix_quiz WHERE quiz_id = '%d' AND type = 'Q';", $row_meta[$qid]);
                        $quiz[$qid] = $wpdb->get_results($query);
                    }
                }
                
                $data[] = array('data' => $row_post, 'meta' => $row_meta, 'quiz' => $quiz);
            }
            break;
        case "modeles":
            $posts = get_posts(array('post_type' => 'modele', 'numberposts' => -1));
            foreach($posts as $p)
                $data[] = new Modele($p->ID);
            break;
        case "options":
            global $wpof_options_preinit, $wpof_options_empty_init;
            foreach($wpof_options_preinit as $key)
            {
                $prop = str_replace("wpof_", "", $key);
                $data[$key] = $wpof->$prop;
            }
            foreach($wpof_options_empty_init as $key)
                $data[$key] = "";
            break;
        default:
            break;
    }

    if (!empty($data))
    {
        $filename = "export_".$type."_".date("Ymd-His").".".$format;
        switch($format)
        {
            case 'csv':
                $csv_data = array();
                $csv_data[] = join(';', $head_row);
                foreach($data as $line)
                    $csv_data[] = join(';', $line);
                $reponse['data'] = join("\r", $csv_data);
                $reponse['filename'] = $filename;
                $reponse['mimetype'] = "text/csv";
                break;
            case 'json':
                $reponse['data'] = json_encode($data);
                $reponse['filename'] = $filename;
                $reponse['mimetype'] = "text/json";
                break;
            default:
                break;
        }
    }
    echo json_encode($reponse);
    
    die();

}

add_action('wp_ajax_inter_opaga_import_formation', 'inter_opaga_import_formation');
function inter_opaga_import_formation()
{
    global $wpdb;
    
    $reponse = array('log' => '', 'message' => '');
    
    if ($_FILES["files"]["error"][0] > 0)
    {
        $reponse['message'] .= "<span class='erreur'>".__("Erreur de téléversement code ")." ".$_FILES["files"]["error"][0]." ".__("pour le fichier")." ".$_FILES["files"]["name"][0]."</span>";
    }
    else
    {
        foreach(json_decode(file_get_contents($_FILES['files']['tmp_name'][0])) as $f)
        {
            // on cherche si les adresses email des formateurs existent dans le nouvel OPAGA et on remplace par les ID
            $formateur = unserialize($f->meta->formateur);
            $f->meta->formateur = array();
            foreach($formateur as $email)
            {
                $formateur_id = get_user_by("email", $email);
                if ($formateur_id)
                    $f->meta->formateur[] = $formateur_id->ID;
            }
            
            // auteur ID
            if (!empty($f->meta->formateur))
                $author_id = $f->meta->formateur[0];
            else
                $author_id = get_current_user_id();
            
            // data
            $f->data->post_author = $author_id;
            $f->data->post_status = "publish";
            $formation_id = wp_insert_post((array) $f->data, true);
            if (is_wp_error($formation_id))
            {
                $reponse['log'] .= __("Erreur création formation")." ".$f->post_title." ".$formation_id->get_error_message();
                continue;
            }
            
            // quiz
            /*
            foreach($f->quiz as $qid => $qtab)
            {
                $quiz = new Quiz();
                $quiz->set_identite($qid, $formation_id);
                $quiz_id = $quiz->last_quiz_id() + 1;
                foreach($qtab as $question)
                {
                    $query = $wpdb->prepare("INSERT INTO $table_quiz (quiz_id, parent_id, subject, type, title, meta_key, meta_value)
                        VALUES ('%d', '%d', '%s', 'Q', '%s', '%d', '%s');",
                        $quiz_id, $formation_id, $question->subject, $question->title, $question->meta_key, $question->meta_value);
                    $wpdb->query($query);
                }
                
                $f->meta->$qid = $quiz_id;
            }
            */
            
            // meta
            $f->meta->formateur = serialize($f->meta->formateur);
            foreach($f->meta as $key => $value)
            {
                $query = $wpdb->prepare("INSERT INTO {$wpdb->prefix}postmeta (post_id, meta_key, meta_value) 
                    VALUES ('%d', '%s', '%s');",
                    $formation_id, $key, $value);
                $wpdb->query($query);
            }
            
            //$reponse['log'] .= var_export($f, true);
            $reponse['message'] .= "<p>".sprintf(__("Formation <span class='succes'>%s</span> ajoutée au catalogue %s"), $f->data->post_title, (empty($f->meta->acces_public)) ? __("privé") : __("public"))."</p>";
        }
    }

    echo json_encode($reponse);
    die();
}

?>
