<?php
/*
 * page-acces.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_acces_content()
{
    global $wpof;
    $html = "";
    
    $acces_interdit = __("Vous n'avez pas accès à cette ressource");
    if (!isset($_GET['t']))
        return $acces_interdit;
    
    $token = new Token(array('token' => $_GET['t']));
    $data = $token->get_acces();
    
    if (isset($data['erreur']))
    {
        $html .= "<p class='erreur'>".$data['erreur']."</p>";
        if (isset($data['session_id']))
            $html .= get_reset_token_form($data['session_id']);
        return $html;
    }
    
    $session = get_session_by_id($data['session_id']);
    $session->init_clients();
    $session->init_stagiaires();
    
    $user = $data['user'];
    
    $html .= $user->get_the_interface($token);
    
    return $html;
}

function get_reset_token_form($session_id)
{
    ob_start();
    
    ?>
    <div id="risette">
    <p><?php _e("Saisissez votre adresse email pour recevoir un nouveau lien"); ?></p>
    <input name="email" type="email" />
    <input type="hidden" name="session_id" value="<?php echo $session_id; ?>" />
    <div class="icone-bouton token-reset"><?php _e("Envoyer"); ?></div>
    <p class="message"></p>
    </div>
    <?php
    
    return ob_get_clean();
}
