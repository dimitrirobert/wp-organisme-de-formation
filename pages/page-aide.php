<?php
/*
 * page-aide.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_aide_content()
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    
/*    if (!in_array($role, array("um_responsable", "admin")))
        return "";*/
    
    $aide_array = (array) $wpof->aide;
    ksort($aide_array);
    
    $html = "<p>".__("Compilation de toutes les bulles d'aide")." (".count($aide_array).")</p>";
    foreach($aide_array as $a)
    {
        $html .= "<div class='objet aide' id='{$a->slug}'>";
        $aide_objet = new Aide($a->slug);
        $html .= "<div>".str_replace('_', ' ', $a->slug)."<div class='delete aide icone-bouton' data-slug='{$a->slug}'>Supprimer</div></div>";
        $html .= $aide_objet->get_aide(false);
        $html .= "</div>";
        //$html .= "</div>";
    }
    
    return $html;
}
