<?php
/*
 * page-questionnaire.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_questionnaire_content()
{
    global $wpof;

    $wpof->uri_params = array_pad($wpof->uri_params, 3, false);
    list($session_id, $qid, $token) = $wpof->uri_params;

    ob_start();

    $erreur = "";
    $session = get_session_by_id($session_id);
    if (empty($session->titre_session))
        $erreur = __("Ce questionnaire n'est rattaché à aucune session !");

    $questionnaire = new Questionnaire($qid);
    if (empty($questionnaire->subject))
        $erreur = __("Ce questionnaire n'existe pas !");

    if (!empty($erreur)) : ?>
    <p class="erreur"><?php echo $erreur; ?></p>
    <?php else :
    if ($token === false)
    {
        $tokenO = new Token([ 'size' => 8 ]);
        $token = $tokenO->token;
        $wpof->log->log_action('questionnaire_load', $questionnaire, 'token', '', $token);
    }

    $questionnaire_url = home_url()."/".$wpof->url_questionnaire."/".$session_id."/".$qid."/".$token;
    
    ?>
    <div id="intro" class="center">
    <h2><?php echo $questionnaire->title; ?></h2>
    <p><?php printf(__("Pour la session <strong>%s</strong> ayant eu lieu le%s %s"), $session->get_displayname(false), (count($session->dates_array) > 1) ? "s" : "", $session->dates_texte); ?></p>
    <p><?php printf(__("Animée par <strong>%s</strong>"), $session->get_formateurs_noms()); ?></p>
    <?php if (!empty($lieu_infos = $session->get_lieu_infos(true))) : ?>
    <p><?php echo __("À")." ".$lieu_infos; ?></p>
    <?php endif; ?>
    <p><?php _e("<strong>Important</strong> votre lien permanent vers ce questionnaire, à conserver si vous souhaitez répondre en plusieurs fois"); ?></p>
    <p><?php echo get_copy_span($questionnaire_url); ?></p>
    </div>
    <?php echo $questionnaire->get_display([ 'session_id' => $session_id, 'repondeur_id' => $token, 'repondeur_type' => 'stagiaire', 'questionnaire_id' => $questionnaire->id ]); ?>
    <div class="questionnaire-footer">
    <?php if (is_user_logged_in()) : ?>
        <div class="center"><a class="icone-bouton questionnaire-reset" href="<?php echo home_url()."/".$wpof->url_questionnaire."/".$session_id."/".$qid."/"; ?>"><?php _e("Saisir les réponses pour une autre personne"); ?></a></div>
    </div>
    <?php else : ?>
    <p><?php _e("<strong>Important</strong> votre lien permanent vers ce questionnaire, à conserver si vous souhaitez répondre en plusieurs fois"); ?></p>
    <p><?php echo get_copy_span($questionnaire_url); ?></p>
    <?php endif; ?>
    <?php endif;
    return ob_get_clean();
}
