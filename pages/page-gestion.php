<?php
/*
 * page-gestion.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_gestion_content($params = array())
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    $page_gestion = "menu";
    
    if (!in_array($role, array("um_responsable", "admin")))
        return "";
    
    $formateur = get_formateur_by_id(get_current_user_id());
    if (empty($formateur->annee_comptable))
        $formateur->update_meta('annee_comptable', date("Y") - 1);
    
    $html = "";
    $sub_sub_page = null;
    
    if (!empty($params['subpage']))
    {
        $page_gestion = $params['subpage'];
        $fonction = "get_page_gestion_$page_gestion";
        
        if (isset($wpof->uri_params[1]) && isset($wpof->{$page_gestion."_sub_pages"}) && in_array($wpof->uri_params[1], array_keys($wpof->{$page_gestion."_sub_pages"})))
            $sub_sub_page = $wpof->uri_params[1];
        
        $html .= $fonction($sub_sub_page);
    }
    
    return get_page_gestion_menu($page_gestion, $sub_sub_page).$html;
}

function get_page_gestion_menu($selected, $sub_selected = "")
{
    global $wpof;
    
    ob_start();
    
    $formateur = get_formateur_by_id(get_current_user_id());
    
    if (is_signataire())
    {
        $docs_a_valider = get_docs_to_validated(Document::VALID_RESPONSABLE_REQUEST);
        $nb_docs = count($docs_a_valider);
        if ($nb_docs > 0)
            echo '<a class="icone-bouton float right bg-alerte" href="'.home_url().'/'.$wpof->url_gestion.'/document/signer/">'.wpof_fa('warning').' '.sprintf("Il y a %d à valider et signer", $nb_docs).'</a>';
    }
    ?>

    <ul id="gestion_menu">
    <?php foreach($wpof->pages_gestion as $slug => $titre) : ?>
        <li><a <?php echo ($slug == $selected) ? 'class="selected"' : ''; ?> href="<?php echo home_url()."/".$wpof->url_gestion."/".$slug; ?>"><?php echo $titre; ?></a></li>
    <?php endforeach; ?>
    </ul>
    
    <?php if (!empty($wpof->{$selected."_sub_pages"})) : ?>
    <?php if (empty($sub_selected)) $sub_selected = array_key_first($wpof->{$selected."_sub_pages"}); ?>
    <ul id="gestion_sub_menu">
    <?php foreach($wpof->{$selected."_sub_pages"} as $slug => $titre) : ?>
        <li><a <?php echo ($slug == $sub_selected) ? 'class="selected"' : ''; ?> href="<?php echo home_url()."/".$wpof->url_gestion."/".$selected."/".$slug; ?>"><?php echo $titre; ?></a></li>
    <?php endforeach; ?>
    </ul>
    <?php endif; ?>
    
    <?php return ob_get_clean();
}
