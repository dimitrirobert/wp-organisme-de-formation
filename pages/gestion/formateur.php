<?php
/*
 * pages/gestion/formateur.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

$wpof->formateur_sub_pages = array
(
    "liste" => __("Formateur⋅ices"),
    "add" => __("Ajouter des membres"),
);

function get_page_gestion_formateur($sub_page = null)
{
    global $wpof;
    if ($sub_page === null)
        $sub_page = array_key_first($wpof->formateur_sub_pages);
    
    $html = "";
    $fonction = "get_tableau_gestion_formateur_".$sub_page;
    $html .= $fonction();
    
    return $html;
}

function get_tableau_gestion_formateur_liste()
{
    $role = wpof_get_role(get_current_user_id());
    $formateurs = get_opaga_users(array('um_responsable', 'um_formateur-trice'));
    
    if (empty($formateurs))
        return '<p class="bg-alerte">'.__("Aucun⋅e formateur⋅ice ni responsable. Créez de nouveaux comptes.").'</p>'.get_tableau_gestion_formateur_add();
    
    init_anime_formation();
    init_anime_session();
    
    ob_start();
    if (!empty($formateurs)) :
    ?>
    <h3><?php _e('Gérez les utilisateurs actuels'); ?></h3>
    <table class="gestion_formateurs opaga opaga2 datatable">
    <thead>
    <?php echo Formateur::get_formateur_control_head(); ?>
    </thead>
    
    <?php
    foreach($formateurs as $f)
        echo $f->get_formateur_control();
    ?>
    </table>
    <?php endif;
    
    return ob_get_clean();
}

function get_tableau_gestion_formateur_add()
{
    ob_start();
    ?>
    <h3><?php _e('Ajoutez de nouveaux membres'); ?></h3>
    <?php echo get_add_compte_form(); ?>
    <?php
    return ob_get_clean();
}


?>
