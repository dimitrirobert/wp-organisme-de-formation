<?php
/*
 * pages/gestion/session.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_page_gestion_session()
{
    global $wpof;
    $html = '<h2 class="gestion">'.$wpof->pages_gestion['session'].'</h2>';
    
    $annee_defaut = get_user_meta(get_current_user_id(), "annee_comptable", true);
    if (empty($annee_defaut))
        $annee_defaut = -1;
    
    $html .= '<div class="icone-bouton dynamic-dialog float right" data-function="new_session" data-type="opac">'.wpof_fa_add().__("Nouvelle session en sous-traitance").'</div>';
    // choix de la plage de dates
    $date_select = new BoardSelectByDate("SessionFormation");
    $html .= $date_select->get_form();
    $date_select->select_by_plage();
    
    $html .= get_tableau_gestion_sessions();
    $html .= '<div class="icone-bouton dynamic-dialog" data-function="new_session" data-type="opac">'.wpof_fa_add().__("Nouvelle session en sous-traitance").'</div>';
    
    return $html;
}

function get_tableau_gestion_sessions()
{
    global $SessionFormation;
    $sessions = $SessionFormation; //get_sessions_from_db();
    if (empty($sessions))
        return __("Pas de session");
    
    ob_start(); ?>
    <table class="opaga opaga2 datatable">
    <thead>
    <?php echo SessionFormation::get_session_control_head(); ?>
    </thead>
    <?php
        foreach($sessions as $s)
            echo $s->get_session_control();
    ?>
    </table>
    <?php
    return ob_get_clean();
}

// retourne un tableau des sessions
function get_sessions_from_db()
{
    global $wpdb, $wpof, $suffix_session_formation;
    
    $query = "SELECT ID as session_id FROM ".$wpdb->prefix."posts WHERE post_type = 'session';";
    $session_tab_id = $wpdb->get_results($query);
    $sessions = array();
    
    foreach($session_tab_id as $o)
        $sessions[] = get_session_by_id($o->session_id, $o->session_id);
    
    return $sessions;
}

?>
