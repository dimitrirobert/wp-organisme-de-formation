<?php
/*
 * pages/gestion/formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_page_gestion_formation()
{
    global $wpof;
    $html = '<h2 class="gestion">'.$wpof->pages_gestion['formation'].'</h2>';
    
    $html .= '<p class="icone-bouton dynamic-dialog" data-function="new_formation">'.wpof_fa_add().__("Nouvelle formation").'</p>';
    $html .= get_tableau_gestion_formations();
    $html .= '<p class="icone-bouton dynamic-dialog" data-function="new_formation">'.wpof_fa_add().__("Nouvelle formation").'</p>';
    
    if ($wpof->role == "admin")
        $html .= get_import_formation_form();
    
    return $html;
}

function get_tableau_gestion_formations()
{
    global $wpof;
    $formations = $wpof->get_formations();
    
    if (empty($formations))
        return '<p class="erreur">'.__("Votre catalogue de formations est vide").'</p>';
    
    ob_start(); ?>
    <table class="opaga opaga2 datatable">
    <thead>
    <?php echo Formation::get_formation_control_head(); ?>
    </thead>
    <?php
    foreach($formations as $f)
        echo $f->get_formation_control();
    ?>
    </table>
    <?php
    return ob_get_clean();
}

function get_import_formation_form()
{
    ob_start();
    ?>
    <h2><?php _e("Importer des formations"); ?></h2>
    <div>
    <form method="POST" name="upload" enctype="multipart/form-data">
        <input name="scan" type="file" accept="application/json" />
        <?php
            echo hidden_input('id_span_message', 'import-message');
            echo hidden_input('action', 'inter_opaga_import_formation');
        ?>
        <input class="ajax-save-file icone-bouton" type="button" value="<?php _e("Importer des formations en JSON") ?>" />
    </form>
    <p id="import-message" class="message"></p>
    </div>
    <?php
    return ob_get_clean();
}

?>
