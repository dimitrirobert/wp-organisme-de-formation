<?php
/*
 * pages/gestion/info.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-info.php");

$wpof->info_sub_pages = array
(
    "edit" => __("Publier une info"),
    "liste" => __("Infos déjà publiées"),
    "cat" => __("Gérer les catégories"),
);

function get_page_gestion_info($sub_page = null)
{
    global $wpof;
    if ($sub_page === null)
        $sub_page = array_key_first($wpof->info_sub_pages);
    
    $html = '<div id="tab-info" class="white-board">';
    $fonction = "get_tableau_gestion_info_".$sub_page;
    $html .= $fonction().'</div> <!-- tab-info -->';
    
    return $html;
}

function get_tableau_gestion_info_edit()
{
    global $wpof;
    
    if (isset($_GET['info_id']))
        $info = new Info($_GET['info_id']);
    else
        $info = new Info();
    ob_start();
    echo $info->get_edit();
    ?>
    
    <?php
    return ob_get_clean();
}

function get_tableau_gestion_info_cat()
{
    global $wpdb, $wpof;
    $table = $wpdb->prefix.$wpof->suffix_content_cat;

    $query = $wpdb->prepare("SELECT * FROM ".$table." WHERE type = %s;", 'info');
    $categories = $wpdb->get_results($query);
    
    ob_start();
    ?>
    <?php if (is_array($categories) && !empty($categories)) : ?>
        <div class="icone-bouton dynamic-dialog" data-function="edit_content_cat" data-type="info"><?php _e("Nouvelle catégorie") ?></div>
        <table class="opaga opaga2 datatable">
            <thead>
            <tr>
                <th class="thin"><?php _e('Identifiant'); ?> <?php echo get_icone_aide('content_categorie_slug'); ?></th>
                <th><?php _e('Nom'); ?></th>
                <th class="thin"><?php _e('Nombre d’infos associées'); ?> <?php echo get_icone_aide('content_categorie_nb_associes'); ?></th>
                <th class="thin"><?php _e('Icône'); ?> <?php echo get_icone_aide('content_categorie_icone'); ?></th>
                <th class="thin"><?php _e('Modifier'); ?></th>
            </tr>
            </thead>
            <?php foreach($categories as $cat) : ?>
                <tr id="<?php echo $cat->slug; ?>">
                    <td><?php echo $cat->slug; ?></td>
                    <td><?php echo $cat->name; ?></td>
                    <td class="center">
                        <?php
                        $query = $wpdb->prepare("SELECT COUNT(*) FROM ".$wpdb->prefix.$wpof->suffix_content." WHERE type = %s AND category = %s;", 'info', $cat->slug);
                        echo $wpdb->get_var($query);
                        ?>
                    </td>
                    <td class="center"><?php if (!empty($cat->icon)) echo wpof_fa($cat->icon)." (".$cat->icon.")"; ?></td>
                    <td class="center"><span class="icone-bouton dynamic-dialog" data-function="edit_content_cat" data-slug="<?php echo $cat->slug; ?>"><?php the_wpof_fa_edit(); ?></></td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    <div class="icone-bouton dynamic-dialog" data-function="edit_content_cat" data-type="info"><?php _e("Nouvelle catégorie") ?></div>
    <?php
    return ob_get_clean();
}

function get_tableau_gestion_info_liste()
{
    global $wpof;
    
    ob_start();
    ?>
    <table class="datatable opaga opaga2">
        <?php
            echo OpagaContent::get_control_head();
            foreach(Info::get_contents('info') as $info)
                echo $info->get_control();
        ?>
    </table>
    <?php
    return ob_get_clean();
}

?>
