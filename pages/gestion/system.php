<?php
/*
 * pages/gestion/system.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

$wpof->system_sub_pages = array
(
    "log" => __("Log"),
    "connexion_log" => __("Connexions"),
    "debug" => __("Debug"),
);

function get_page_gestion_system($sub_page = null)
{
    global $wpof;
    
    $html = "";
    if (isset($wpof->uri_params[0]) && $wpof->uri_params[0] == "system")
        array_shift($wpof->uri_params);
        
    if (isset($wpof->uri_params[0]) && array_key_exists($wpof->uri_params[0], $wpof->system_sub_pages))
        array_shift($wpof->uri_params);
    
    if ($sub_page === null)
        $sub_page = array_key_first($wpof->system_sub_pages);
    
    $fonction = "get_tableau_gestion_system_".$sub_page;
    $html .= $fonction();
    
    return $html;
}

function get_tableau_gestion_system_log()
{
    global $wpof;
    
    $log_load_rows = (empty($wpof->log_load_rows)) ? 10000 : $wpof->log_load_rows;
    $log_rows = null;
    $filter = "";
    
    if (isset($wpof->uri_params[0]))
    {
        switch($wpof->uri_params[0])
        {
            case 'formateur':
                if (isset($wpof->uri_params[1]))
                {
                    $log_rows = $wpof->log->get_formateur_log($wpof->uri_params[1], $log_load_rows);
                    $filter = "formateur/".$wpof->uri_params[1];
                }
                break;
            default:
                if (isset($wpof->uri_params[1]))
                {
                    $log_rows = $wpof->log->get_entite_log($wpof->uri_params[0], $wpof->uri_params[1], $log_load_rows);
                    $filter = $wpof->uri_params[0]."/".$wpof->uri_params[1];
                }
                break;
        }
    }
    
    if ($log_rows == null)
        $log_rows = $wpof->log->get_log($log_load_rows);
    
    ob_start();
    ?>
    <table class="opaga opaga2 datatable" data-order-desc="1" <?php if (!empty($filter)) : ?>data-filter="<?php echo $filter; ?>"<?php endif; ?>>
    <thead>
    <tr>
    <?php foreach($wpof->log->get_table_cols() as $key => $text) : ?>
    <th id="col_<?php echo $key; ?>"><?php echo $text; ?></th>
    <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach($log_rows as $row) : ?>
    <tr>
        <?php foreach((array) $row as $key => $cell) : ?>
        <td class="cell_<?php echo $key; ?>"><?php show_string_cut($cell, 80); ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
    <?php
    return ob_get_clean();
}

function get_tableau_gestion_system_connexion_log()
{
    global $wpof;
    
    ob_start();
    ?>
    <table class="opaga opaga2 datatable" data-order-desc="0">
    <thead>
    <tr>
    <th id="datetime"><?php _e("Date et heure"); ?></th>
    <th id="name"><?php _e("Prénom et nom"); ?></th>
    <th id="name"><?php _e("Rôle"); ?></th>
    <th id="email"><?php _e("Email"); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach(get_users() as $user) : ?>
    <?php $ts = (integer) get_user_meta($user->ID, "last_online", true); ?>
    <tr <?php if ($ts < 946740000) : ?> class="tr-empty"<?php endif; ?>>
        <td><span class="hidden"><?php echo $ts; ?></span>
        <?php if ($ts > 946740000) echo date("j/m/Y à H:i", $ts); ?>
        </td>
        <td><?php echo $user->display_name; ?></td>
        <td><?php echo wpof_get_role($user->ID); ?></td>
        <td><?php echo $user->user_email; ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
    <?php
    return ob_get_clean();
}

function get_tableau_gestion_system_debug()
{
    global $wpof, $Documents, $SessionFormation;
    
    $wpof->role = wpof_get_role(get_current_user_id());
    
    return get_yaml();
}

function get_yaml()
{
    global $wpof;

    $config_dir = wpof_path. '/config/keywords';
    $kw_config = yaml_parse_file($config_dir.'/stagiaire.yaml');
    ob_start(); ?>
    <pre>
    <?php var_dump($kw_config); ?>
    </pre>
    <?php 
    return ob_get_clean();
}

function get_unlink_creneaux()
{
    global $wpof, $wpdb, $suffix_creneaux;
    
    $query = "SELECT session_id, id FROM ".$wpdb->prefix.$suffix_creneaux." ORDER BY session_id;";
    $all_creneaux = $wpdb->get_results($query);
    $registered_creneaux = array();
    $unlink_creneaux = array();

    ob_start();
    foreach($all_creneaux as $obj)
    {
        if (empty($registered_creneaux[$obj->session_id]))
        {
            $registered_creneaux[$obj->session_id] = array();
            $creneaux = get_post_meta($obj->session_id, "creneaux", true);
            if (is_array($creneaux))
                foreach ($creneaux as $date_tab)
                    $registered_creneaux[$obj->session_id] = array_merge($registered_creneaux[$obj->session_id], $date_tab);
        }
        if (!in_array($obj->id, $registered_creneaux[$obj->session_id]))
            $unlink_creneaux[$obj->id] = new Creneau($obj->id);
    }
    
    
    $session_id = 0;
    foreach($unlink_creneaux as $cid => $creno) : 
        if ($session_id != $creno->session_id) : ?>
        <?php if ($session_id != 0) : ?></div></div><?php endif; ?>
        <?php
            $session_id = $creno->session_id;
            $session = get_session_by_id($session_id);
        ?>
        <div class="id session" data-id="<?php $session_id; ?>">
        <p><?php echo $session->get_a_link()." ".$session->dates_texte; ?> <strong>[<?php echo implode(',', $registered_creneaux[$session_id]); ?>]</strong></p>
        <div class="liste-creneau" style="flex-wrap: wrap">
        <?php endif; ?>
        
        <?php echo $cid.$creno->get_html(null, true)." "; ?>
    <?php endforeach; ?>
    </div></div>
    <p>
    <?php
    if (empty($unlink_creneaux))
        _e("Aucun créneau fantôme");
    else
        echo "DELETE FROM ".$wpdb->prefix.$suffix_creneaux." WHERE id IN (".implode(',', array_keys($unlink_creneaux)).");";
    ?>
    </p>
    <?php
    return ob_get_clean();
}

?>
