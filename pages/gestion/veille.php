<?php
/*
 * pages/gestion/veille.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_page_gestion_veille()
{
    global $wpof;
    $html = '<h2 class="gestion">'.$wpof->pages_gestion['veille'].'</h2>';
    
    $html .= get_tableau_gestion_veille();
    
    return $html;
}

function get_tableau_gestion_veille()
{
    global $wpof;
    
    $wpof->role = wpof_get_role(get_current_user_id());
    
    $formateurs = $wpof->get_formateurs_by_role();
    
    ob_start();
    ?>
    <div id="tab-veille" class="white-board">
    <?php if (!empty($formateurs)) : ?>
    <table class="opaga opaga2 datatable gestion-docs-admin">
    <thead>
    <tr>
    <th class="thin"><?php _e('Formateur⋅ice'); ?></th>
    <th id="order" data-order="desc" class="thin"><?php _e('Dernière mise à jour'); ?></th>
    <th><?php _e('Bloc-notes'); ?></th>
    </tr>
    </thead>
    <?php foreach($formateurs as $f) : ?>
    <tr>
    <td><?php echo $f->get_displayname(); ?></td>
    <td><span class="hidden"><?php $date_sort = explode("/", $f->date_modif_veille); echo $date_sort[2].$date_sort[1].$date_sort[0]; ?></span><?php echo $f->date_modif_veille; ?></td>
    <td>
    <?php
    if (!empty($f->veille_bloc_notes)) :
        $richtext_veille = $f->veille_bloc_notes;
        $bruttext_veille = strip_tags($richtext_veille);
        $excerpt = '<strong>('.strlen($bruttext_veille).' '.__('caractères').')</strong> '.string_cut($bruttext_veille, 100).'…';
        ?>
    <span class="openButton" data-id="<?php echo $f->id; ?>veille"><?php echo $excerpt; ?></span>
    <div class="blocHidden" id="<?php echo $f->id; ?>veille">
    <?php echo $richtext_veille; ?>
    </div>
    <?php endif; ?>
    </td>
    </tr>
    <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div> <!-- tab-veille -->
    <?php
    return ob_get_clean();
}

?>
