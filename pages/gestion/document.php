<?php
/*
 * pages/gestion/document.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

$wpof->document_sub_pages = array
(
    "liste" => __("Tous les documents"),
    "preuves" => __("Preuves"),
    "signer" => __("À signer"),
);

function get_page_gestion_document($sub_page = null)
{
    global $wpof;
    if ($sub_page === null)
        $sub_page = array_key_first($wpof->document_sub_pages);
    
    $html = "";
    $fonction = "get_page_gestion_document_".$sub_page;
    $html .= $fonction();
    
    return $html;
}

function get_document_date_selector()
{
    global $SessionFormation, $Documents;
    $html = "";
    
    // choix de la plage de dates
    $date_select = new BoardSelectByDate("SessionFormation");
    $html .= $date_select->get_form();
    $date_select->select_by_plage();
    
    foreach($SessionFormation as $s)
        $s->init_all_docs();
    
    ksort($Documents);
    
    return $html;
}

function get_page_gestion_document_preuves()
{
    global $wpof, $Documents, $SessionFormation;
    
    $wpof->role = wpof_get_role(get_current_user_id());
    
    ob_start();
    echo get_document_date_selector();
    ?>
    <div id="tab-docs" class="white-board">
    <?php if (!empty($Documents)) : ?>
    <table class="opaga opaga2 datatable gestion-docs-admin">
    <thead>
    <th>
        Formateurice
    </th>
    <th>
        Date de la session
    </th>
    <th>
        Titre de la session
    </th>
    <th>
        Type de document
    </th>
    <th>
        Entitée concernée
    </th>
    <th>
        Etat du document
    </th>
    <th>
        Modifié le
    </th>
    <th>
        Document
    </th>
    </thead>
    <tbody>
    <?php
    foreach($Documents as $doc)
        echo $doc->get_elements_preuve();
    ?>
    </tbody>
    </table>
    <?php endif; ?>
    </div> <!-- tab-documents -->
    <?php
    return ob_get_clean();
}

function get_page_gestion_document_liste()
{
    global $wpof, $Documents, $SessionFormation;
    
    $wpof->role = wpof_get_role(get_current_user_id());
    
    ob_start();
    echo get_document_date_selector();
    ?>
    <div id="tab-docs" class="white-board">
    <?php if (!empty($Documents)) : ?>
    <table class="opaga opaga2 datatable gestion-docs-admin">
    <thead>
    <?php echo Document::get_document_control_head(); ?>
    </thead>
    <?php
    foreach($Documents as $doc)
        echo $doc->get_document_control();
    ?>
    </table>
    <?php endif; ?>
    </div> <!-- tab-documents -->
    <?php
    return ob_get_clean();
}

function get_page_gestion_document_signer()
{
    if (is_signataire())
    {
        $responsable = new Formateur(get_current_user_id());
        
        $nb_docs = 0;
        $docs_a_valider = get_docs_to_validated(Document::VALID_RESPONSABLE_REQUEST);
        $nb_docs = count($docs_a_valider);
        ob_start();
        ?>
        <?php if ($nb_docs > 0): 
        $signature_pouvoir = __("Vous êtes habilité⋅e à signer.");
        if ($responsable->signataire == "signature_po")
        {
            if (signataire_pour_id() === null)
                $signature_pouvoir = '<span class="erreur">'.__("Aucun⋅e responsable n'a la signature officielle, veuillez corriger cela au plus vite !").'</span>';
            else
                $signature_pouvoir = sprintf(__("Vous signez sur ordre de %s."), signataire_pour_nom());
        }
        ?>
        <div id="documents" class="white-board">
            <p><?php echo __("Voici la liste des documents à vérifier et, éventuellement, signer.")." ".$signature_pouvoir; ?></p>
            
            <table class='gestion-docs-admin edit-data'><tbody>
            <tr class="tr-titre">
                <th><?php _e("Document"); ?></th>
                <th><?php _e("Finaliser"); ?></th>
                <th><?php _e("Déposer le doc signé"); ?></th>
                <?php if (champ_additionnel("image_signature")) : ?>
                <th><?php _e("Valider le document"); ?></th>
                <?php endif; ?>
                <th><?php _e("Rejeter le document"); ?></th>
            </tr>
            
            <?php
            $last_session = 0;
            
            $columns = Document::COL_DOCUMENT | Document::COL_FINAL | Document::COL_SCAN | Document::COL_NOM_ENTITE | Document::COL_REJETER;
            $colspan = 4;
            if (champ_additionnel("image_signature"))
            {
                $columns |= Document::COL_VALIDER;
                $colspan = 5;
            }
            
            foreach ($docs_a_valider as $doc_line)
            {
                if ($last_session != $doc_line->session_id)
                {
                    $formateurs_nom = array();
                    $last_session = $doc_line->session_id;
                    $formateurs_id = get_post_meta($last_session, 'formateur', true);
                    if (!is_array($formateurs_id))
                        $formateurs_nom[] = '<strong class="erreur">'.__("Personne").'</strong>';
                    else
                        foreach($formateurs_id as $f_id)
                            $formateurs_nom[] = get_displayname($f_id, false);
                    
                    echo "<tr><td colspan='".$colspan."' class='td-intertitre'><a href='".get_the_permalink($doc_line->session_id)."'>".get_the_title($doc_line->session_id)."</a> ".__("animée par")." ".join(", ", $formateurs_nom)."</td></tr>";
                }
                $doc = new Document($doc_line->document, $doc_line->session_id, $doc_line->contexte, $doc_line->contexte_id);
                $doc->signataire = true;
                echo $doc->get_html_ligne($columns);
            }
            ?>
            </tbody></table>
        </div> <!-- .white-board -->
        <?php else: ?>
        <div id="documents" class="white-board">
            <?php _e("Aucun document à signer. Passez une bonne journée."); ?>
        </div>
        <?php
        endif; // if ($nb_docs > 0):
        return ob_get_clean();
    }
    else
        return __("Vous ne pouvez pas signer les documents.");
}

?>
