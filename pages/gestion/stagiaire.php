<?php
/*
 * pages/gestion/stagiaire.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_page_gestion_stagiaire()
{
    global $wpof;
    $html = '<h2 class="gestion">'.$wpof->pages_gestion['stagiaire'].'</h2>';
    
    $annee_defaut = get_user_meta(get_current_user_id(), "annee_comptable", true);
    if (empty($annee_defaut))
        $annee_defaut = -1;
    
    // choix de la plage de dates
    $date_select = new BoardSelectByDate("SessionStagiaire");
    $html .= $date_select->get_form();
    $date_select->select_by_plage();
    
    $html .= get_tableau_gestion_stagiaires();
    
    return $html;
}

function get_tableau_gestion_stagiaires()
{
    global $SessionStagiaire;
    if (empty($SessionStagiaire))
        return __("Pas de stagiaire");
    
    ob_start(); ?>
    <table class="opaga opaga2 datatable">
    <thead>
    <?php echo SessionStagiaire::get_stagiaire_control_head(); ?>
    </thead>
    <?php
        foreach($SessionStagiaire as $s)
            echo $s->get_stagiaire_control();
    ?>
    </table>
    <?php
    return ob_get_clean();
}

?>
