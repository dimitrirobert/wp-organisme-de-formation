<?php
/*
 * pages/gestion/client.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// Inclure dompdf, HtmlDomParser installés via composer
require_once wpof_path . '/vendor/autoload.php';
use voku\helper\HtmlDomParser;

require_once(wpof_path . "/class/class-export-spreadsheet.php");


function get_page_gestion_client()
{
    global $wpof;
    $html = '<h2 class="gestion">'.$wpof->pages_gestion['client'].'</h2>';
    $html .= '<div class="gestion-board">';
    
    $annee_defaut = get_user_meta(get_current_user_id(), "annee_comptable", true);
    if (empty($annee_defaut))
        $annee_defaut = -1;
    
    // choix de la plage de dates
    $date_select = new BoardSelectByDate("Client");
    $html .= $date_select->get_form();
    $date_select->select_by_plage();
    
    /*
    $toggle_button_sous_traitance = new BoardToggleButton(
        array(
            'text_off' => __('Afficher la sous-traitance'),
            'text_on' => __('Masquer la sous-traitance'),
            'hide' => '.sous-traitance',
            'keep' => '.morale, .physique',
            'parent' => '.datatable',
        ));
    */
    $html .= '<div>';
    //$html .= $toggle_button_sous_traitance->get_html_button('on');
    $html .= '<div class="icone-bouton export" data-func="export_client_table">'.__("Exporter tout en tableur").'</div>';
    $html .= '</div>';
    
    $html .= '</div>'; // .gestion-board
    
    $html .= get_tableau_gestion_clients();
    
    return $html;
}

function get_tableau_gestion_clients()
{
    global $Client;
    if (empty($Client))
        return __("Pas de client");
    
    ob_start(); ?>
    <table class="opaga opaga2 datatable">
    <thead>
    <?php echo Client::get_client_control_head(); ?>
    </thead>
    <?php
        foreach($Client as $c)
            echo $c->get_client_control();
    ?>
    </table>
    <?php
    return ob_get_clean();
}

add_action('wp_ajax_export_client_table', 'export_client_table');
function export_client_table()
{
    global $wpof;
    $reponse = array();
    
    $role = wpof_get_role(get_current_user_id());
    if (!in_array($role, $wpof->super_roles))
        die();
    
    $export = new ExportSpreadsheet();
    $sheet = $export->get_sheet();
    $data = array();
    $document_html = HtmlDomParser::str_get_html(get_tableau_gestion_clients());
    
    $row_num = 1;
    $col_num = 'A';
    $first_row = $document_html->findOne('tr');
    
    foreach($first_row->find('th,td') as $th)
    {
        $sheet->setCellValue($col_num.$row_num, strip_tags($th->innerHtml()));
        if ($col_num != 'Z')
            $col_num++;
        else
            $col_num = 'AA';
    }
    $sheet->setAutoFilter('A1:'.$col_num.'1');
    $sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);
    
    $row_num++;

    foreach($document_html->find('tr:not(:first-child)') as $tr)
    {
        $col_num = 'A';
        foreach($tr->find('th,td') as $td)
        {
            $cell = "";
            $class = "";
            if ($td->hasAttribute('data-value'))
                $cell = trim($td->getAttribute('data-value'));
            
            if ($td->hasAttribute('class') && substr_count($td->getAttribute("class"), "date") > 0)
            {
                // MySQL-like timestamp '2008-12-31' or date string
                \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder( new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder() );

                $sheet->getStyle($col_num.$row_num)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
            }
            $sheet->setCellValue($col_num.$row_num, strip_tags($cell));
                    
            if ($col_num != 'Z')
                $col_num++;
            else
                $col_num = 'AA';
        }
        $row_num++;
    }
    
    $sheet->getPageSetup()->setPrintArea('A1:'.$col_num.$row_num);
    $sheet->getStyle('A1:'.$col_num.$row_num)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    $sheet->getDefaultColumnDimension()->setWidth(12);
    $sheet->freezePane('A2');
    
    ob_start();
    $export->export();
    $reponse = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,'.base64_encode(ob_get_clean());
    
    /*
    $reponse['mimetype'] = "";
    $reponse['filename'] = 'clients.xlsx';
    */
    
    //echo json_encode($reponse);
    die($reponse);
}

?>
