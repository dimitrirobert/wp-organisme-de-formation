<?php
/*
 * pages/gestion/questionnaires.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-qmanager.php");
function wpof_load_questionnaire_scripts()
{
    wp_enqueue_script('wpof-questionnaire', wpof_url."js/wpof-questionnaire.js", array('jquery'));
}
add_action( 'wp_enqueue_scripts', 'wpof_load_questionnaire_scripts', 21 );


$wpof->questionnaires_sub_pages = array
(
    "manager" => __("Tous les questionnaires"),
    "questions" => __("Questions"),
    "reponses" => __("Réponses"),
    "stat_manager" => __("Statistiques"),
);

$qmanager = null;
$questionnaire = null;

/**
 * Affichage de la page de gestion des questionnaires
 */
function get_page_gestion_questionnaires(String|int $sub_page = null): string
{
    global $wpof, $qmanager, $questionnaire;

    $qmanager = new QManager();

    $html = "";
    if (isset($_GET['questionnaire_id']) && is_numeric($_GET['questionnaire_id']))
    {
        $questionnaire = new Questionnaire($_GET['questionnaire_id']);
        if (get_class($questionnaire) === "Questionnaire")
            $sub_page = "edit";
    }
    if ($sub_page === null)
        $sub_page = array_key_first($wpof->questionnaires_sub_pages);
    
    $fonction = "get_page_gestion_questionnaires_".$sub_page;
    $html .= $fonction();
    
    return $html;
}

/**
 * Affichage du manager de questionnaires
 */
function get_page_gestion_questionnaires_manager(): String
{
    global $wpof, $qmanager;
    
    ob_start();
    ?>
    <div id="tab-questionnaires" class="white-board">
    <?php echo $qmanager->get_button("add_questionnaire", "Ajouter un questionnaire", wpof_fa_add()); ?>
    <table class="opaga opaga2 datatable">
        <thead>
        <?php echo $qmanager->get_questionnaire_control_head(); ?>
        </thead>
        <tbody>
            <?php
                foreach($qmanager->get_questionnaires() as $q)
                    echo $q->get_questionnaire_control();
            ?>
        </tbody>
    </table>
    </div> <!-- tab-questionnaires -->
    <?php
    return ob_get_clean();
}

/**
 * Affichage de l'éditeur d'un questionnaire
 */
function get_page_gestion_questionnaires_edit(): String
{
    global $wpof, $qmanager, $questionnaire;
    
    ob_start();
    ?>
    <div id="tab-questionnaire-edit" class="white-board questionnaire" data-id="<?php echo $questionnaire->id; ?>">
    <div class="header">
        <h2><?php _e("Modifier le questionnaire"); ?></h2>
        <input type="hidden" name="level" value="of" />
        <?php echo $questionnaire->get_params_form(); ?>
    </div>
    <div class="body flexrow">
        <?php echo $questionnaire->get_add_question_buttons(); ?>
        <ul id="questions<?php echo $questionnaire->id; ?>" class="questionnaire-sortable">
        <?php
            foreach($questionnaire->question as $qid => $question)
                echo $question->get_edit($questionnaire->id);
        ?>
        </ul>
    </div>
    </div> <!-- tab-questionnaire-edit -->
    <?php
    return ob_get_clean();
}

/**
 * Affichage du manager de questions
 */
function get_page_gestion_questionnaires_questions(): String
{
    global $wpof, $qmanager;

    $qmanager->get_questionnaires();
    
    $question = [];
    foreach($qmanager->get_questions_id() as $qid)
    {
        $question[$qid] = new Question($qid);
    }

    ob_start();
    ?>
    <pre>
        <?php //var_dump($qmanager->get_questions_id()); ?>
    </pre>
    <div id="tab-questions-manager" class="white-board">
    <table class="opaga opaga2 datatable">
        <thead>
            <tr>
                <th class="thin"><?php _e("ID"); ?></th>
                <th><?php _e("Titre"); ?></th>
                <th><?php _e("Type"); ?></th>
                <th><?php _e("Utilisée dans"); ?></th>
                <th><?php _e("Nb réponses"); ?></th>
                <th class="thin not_orderable"><?php _e("Supprimer"); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($question as $qid => $q) : ?>
            <tr class="questionrow" data-id="<?php echo $qid; ?>">
                <td><?php echo $q->id; ?></td>
                <td><?php echo $q->title; ?></td>
                <td><?php echo $wpof->questionnaire_type_question->get_term($q->qtype); ?></td>
                <td>
                    <?php $questionnaires = $q->get_linked_questionnaire();
                    if (empty($questionnaires))
                        echo __('aucun');
                    else
                    {
                        ?>
                        <ul>
                        <?php foreach($questionnaires as $id) : ?>
                            <li>
                                <?php echo $qmanager->questionnaire[$id]->get_displayname(true); ?>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                        <?php
                    } ?>
                </td>
                <td class="center"><?php echo count($q->get_reponses_repondeur_id()); ?></td>
                <td class="center">
                    <?php if (count($questionnaires) == 0) : ?>
                    <span class='pointer action question delete' data-function='question-supprimer'><?php the_wpof_fa_del(); ?></span>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div> <!-- tab-questions-manager -->
    <?php
    return ob_get_clean();
}


/**
 * Affichage du manager de réponses
 */
function get_page_gestion_questionnaires_reponses(): String
{
    global $wpof, $wpdb, $qmanager;

    $query = "SELECT DISTINCT r.repondeur_id, r.repondeur_type, r.session_id, p.post_title as session_title, pm.meta_value as formateur, pm.meta_value, r.questionnaire_id, r.date ".
        "FROM ".$wpdb->prefix.$wpof->suffix_qreponse." as r, ".$wpdb->prefix."posts as p, ".$wpdb->prefix."postmeta as pm ".
        "WHERE r.session_id = p.ID AND p.ID = pm.post_id AND pm.meta_key = 'formateur' ".
        "ORDER BY r.session_id;";
    $reponses = $wpdb->get_results($query);

    ob_start();
    ?>
    <pre>
        <?php //var_dump($reponses); ?>
    </pre>
    <div id="tab-reponses" class="white-board">
    <table class="opaga opaga2 datatable">
        <thead>
        <tr>
            <th class="thin"><?php _e("ID répondant"); ?></th>
            <th class="thin"><?php _e("Type"); ?></th>
            <th class="thin"><?php _e("Date réponse"); ?></th>
            <th class="thin"><?php _e("Nb réponses"); ?></th>
            <th><?php _e("Session"); ?></th>
            <th id="order" data-order="desc"><?php _e("Achevée le"); ?></th>
            <th><?php _e("Animée par"); ?></th>
            <th><?php _e("Questionnaire"); ?></th>
            <th><?php _e("Nature formation"); ?></th>
            <th><?php _e("Modalité session"); ?></th>
            <th class="thin not_orderable"><?php _e("Voir"); ?></th>
            <th class="thin not_orderable"><?php _e("Modifier"); the_wpof_fa('triangle-exclamation', 'alerte', __("Attention, ne modifiez les valeurs que pour corriger une erreur !")); ?></th>
            <?php if ($wpof->role == "admin") : ?>
            <th class="thin center not_orderable"><?php _e("Supprimer"); ?></th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
            <?php foreach($reponses as $r) : ?>
            <tr class="repondant" data-id="<?php echo $r->repondeur_id; ?>" data-sessionid="<?php echo $r->session_id; ?>" data-questionnaireid="<?php echo $r->questionnaire_id; ?>" data-type="<?php echo $r->repondeur_type; ?>">
                <td class="center"><a href="<?php echo get_the_permalink($r->session_id).'questionnaires/reponses/'.$r->questionnaire_id.'/'.$r->repondeur_id; ?>"><?php echo $r->repondeur_id; ?></a></td>
                <td><?php echo $r->repondeur_type; ?></td>
                <td><?php echo $r->date; ?></td>
                <td class="center">
                    <?php
                    $query = $wpdb->prepare("SELECT count(repondeur_id) FROM ".$wpdb->prefix.$wpof->suffix_qreponse." WHERE repondeur_id = %s;", $r->repondeur_id);
                    echo $wpdb->get_var($query);
                    ?>
                </td>
                <td><a href="<?php echo get_the_permalink($r->session_id).'questionnaires/reponses/'.$r->questionnaire_id.'/'; ?>"><?php echo $r->session_title; ?></a></td>
                <td><?php echo date("Y-m-d", get_post_meta($r->session_id, "last_date_timestamp", true)); ?></td>
                <td><?php $formateur = unserialize($r->formateur); echo implode(', ', array_map("get_displayname", $formateur)); ?></td>
                <td><?php $questionnaire = new Questionnaire($r->questionnaire_id); echo "[".$questionnaire->id."] ".$questionnaire->get_displayname(true, false); ?></td>
                <td>
                    <?php $nature = [];
                    foreach($questionnaire->meta['nature_formation'] as $nf)
                        $nature[] = $wpof->nature_formation->get_term($nf);
                    echo implode('<br />', $nature);
                    ?>
                </td>
                <td>
                    <?php $modalite = [];
                    foreach($questionnaire->meta['modalite_session'] as $nf)
                        $modalite[] = $wpof->modalite_session->get_term($nf);
                    echo implode('<br />', $modalite);
                    ?>
                </td>
                <td class="center"><a href="<?php echo get_the_permalink($r->session_id).'questionnaires/reponses/'.$r->questionnaire_id.'/'.$r->repondeur_id; ?>"><?php the_wpof_fa('eye'); ?></a>
                </td>
                <td class="center">
                <a title="<?php _e("Ne modifiez les valeurs que pour corriger une erreur !"); ?>" href="<?php echo home_url().'/questionnaire/'.$r->session_id.'/'.$r->questionnaire_id.'/'.$r->repondeur_id; ?>"><?php the_wpof_fa_edit('alerte'); ?></a>
                </td>
                <?php if ($wpof->role == "admin") : ?>
                <td class="center"><span class="pointer action reponse delete" data-function="delete-reponse"><?php the_wpof_fa_del(); ?></span></td>
                <?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?php
    return ob_get_clean();
}

/**
 * Affichage du manager de statistiques
 */
function get_page_gestion_questionnaires_stat_manager(): String
{
    require_once(wpof_path . "/class/class-stat.php");

    global $wpof, $qmanager;

    $stat = new Stat();
    $stat_id = Stat::get_all_stats_id();
    if (!empty($stat_id))
        init_term_list("question_id", array('type' => 'likert'));
    
    ob_start();
    ?>
    <div id="tab-stat-edit" class="white-board">
    <?php echo $stat->get_edit(); ?>
    </div> <!-- tab-stat-edit -->
    <div id="tab-stat-manager" class="white-board">
    <table class="opaga opaga2 datatable edit-data">
        <thead>
        <tr>
            <th class="thin"><?php _e("ID"); ?></th>
            <th><?php _e("Titre"); ?></th>
            <th><?php _e("Description"); ?></th>
            <th class="not_orderable"><?php _e("Code court"); echo get_icone_aide('stat_shortcode'); ?></th>
            <th><?php _e("Questions compilées"); echo get_icone_aide('stat_questions');?></th>
            <th class="thin center"><?php _e("Valeur globale"); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($stat_id as $id) : ?>
            <tr>
                <?php $stat = new Stat($id); ?>
                <td class="center"><?php echo $stat->id; ?></td>
                <td><?php echo get_input_jpost($stat, 'title', array('input' => 'text', 'aide' => false)); ?></td>
                <td><?php echo get_input_jpost($stat, 'description', array('input' => 'text', 'aide' => false)); ?></td>
                <td>
                    <?php echo __('Valeur et titre').' '.get_copy_span('[[opagastat '.$stat->id.']]'); ?><br />
                    <?php echo __('Titre seul').' '.get_copy_span('[[opagastat_titre '.$stat->id.']]'); ?>
                </td>
                <td><?php echo get_input_jpost($stat, 'question_id', array('select' => 'multiple', 'aide' => false)); ?></td>
                <td><?php $stat->the_value(); ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    </div> <!-- tab-stat-manager -->

    <?php if ($wpof->role == 'admin') : ?>
    <div class="white-board">
        <?php $stat = new Stat(1); ?>
        <p>Bilan <?php echo $stat->title; ?> : <?php $stat->the_value("nature", "bilan"); ?></p>
        <p>Action de form <?php echo $stat->title; ?> : <?php $stat->the_value("nature", "form"); ?></p>
        <p>Formateur <?php echo $stat->title; ?> : <?php $stat->the_value("formateur", 2); ?></p>
        <p>Formation <?php echo $stat->title; ?> : <?php $stat->the_value("formation", 30); ?></p>
        <?php $stat = new Stat(2); ?>
        <p>Formateur <?php echo $stat->title; ?> : <?php $stat->the_value("formateur", 2); ?></p>
        <p>Formation <?php echo $stat->title; ?> : <?php $stat->the_value("formation", 30); ?></p>
        <?php $stat = new Stat(3); ?>
        <p>Formateur <?php echo $stat->title; ?> : <?php $stat->the_value("formateur", 2); ?></p>
        <p>Formation <?php echo $stat->title; ?> : <?php $stat->the_value("formation", 30); ?></p>
    </div>
    <?php endif; ?>
    <?php
    return ob_get_clean();
}


?>
