<?php
/*
 * wpof-bpf.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-board-toc.php");

function get_page_gestion_bpf()
{
    $annee_defaut = get_user_meta(get_current_user_id(), "annee_comptable", true);
    if (empty($annee_defaut))
        $annee_defaut = date('Y') - 1;
    
    $html = '<div class="gestion-board">';
    // choix de la plage de dates
    $date_select = new BoardSelectByDate("SessionFormation");
    $html .= $date_select->get_form();
    $date_select->select_by_plage();
    
    $html .= "<div class='white-board' id='bpf' data-annee='$annee_defaut'>";
    $html .= get_bpf();
    $html .= "</div>";
    $html .= "</div>";
    return $html;
}

function get_bpf()
{
    global $SessionFormation;
    global $Client;
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    $wpof->toc = new BoardToc();
    
    $html = "";
    
    $session_id_1 = array_keys($SessionFormation);
    sort($session_id_1);
    $client_id_1 = array();
    
    $session_ctl = array();
    $session_ctl[0] = array();
    
    $nb_stagiaires = $nb_stagiaires_st = $nb_clients_st = $nb_clients = 0;
    
    select_clients_from_db();
    /*
    if ($role == 'admin')
        $html .= '<p>clients ID '.count($Client).': '.implode(', ', array_keys($Client)).'</p>';
    */
    
    foreach($Client as $c)
    {
        if ($c->financement == 'opac')
        {
            $nb_stagiaires_st += $c->nb_stagiaires;
            $nb_clients_st++;
        }
        else
        {
            $nb_stagiaires += $c->nb_confirmes;
            $nb_clients++;
        }
        $client_id_1[$c->id] = 1;
    }
    
    select_stagiaires_from_db();
    /*
    if ($role == 'admin')
        $html .= '<p>clients ID '.count($Client).': '.implode(', ', array_keys($Client)).'</p>';
    */
    
/*    foreach($SessionFormation as $s)
    {
        $s->init_clients();
        //$s->init_stagiaires();
        
        foreach($s->clients as $cid)
        {
            if ($Client[$cid]->financement == "opac")
            {
                $nb_stagiaires_st += (int)$Client[$cid]->nb_stagiaires;
                $nb_clients_st ++;
            }
            else
            {
                $nb_stagiaires += count($Client[$cid]->stagiaires);
                $nb_clients ++;
            }
            $client_id_1[$cid] = 1;
        }
        
        if (!in_array($s->type_index, array_keys($wpof->type_session)))
            $session_ctl[0][] = get_session_numero($s);
        else
        {
            if (empty($session_ctl[$s->type_index]))
                $session_ctl[$s->type_index] = array();
            $session_ctl[$s->type_index][] = get_session_numero($s);
        }
    }
*/
    $nb_avant = count($Client);
    $html .= "<ul>";
    $html .= "<li>".__("Nombre de sessions")." : ".count($SessionFormation)."</li>";
    $html .= "<li>Nombre de clients en direct : $nb_clients</li>";
    $html .= "<li>Nombre de clients en sous-traitance : $nb_clients_st</li>";
    $html .= "<li>Total clients : ". ($nb_clients_st + $nb_clients)." / $nb_avant</li>";
    $html .= "<li>Nombre de stagiaires en direct : $nb_stagiaires</li>";
    $html .= "<li>Nombre de stagiaires en sous-traitance : $nb_stagiaires_st</li>";
    $html .= "</ul>";
    
    /*
    if ($role == 'admin')
    {
        $session_id_2 = array_keys($SessionFormation);
        sort($session_id_2);

        $html .= '<p>Sessions excédentaires '.implode(', ', array_map(function($valeur) {global $SessionFormation; return $SessionFormation[$valeur]->get_id_link();}, array_diff($session_id_2, $session_id_1))).'</p>';
        $html .= '<p>Clients excédentaires '.implode(', ', array_diff(array_keys($Client), array_keys($client_id_1))).'</p>';
        $html .= '<p>clients ID : '.implode(', ', array_keys($Client)).'</p>';
    }
    */

    $html .= get_bpf_c();
    $html .= get_bpf_d();
    $html .= get_bpf_e();
    $html .= get_bpf_f1();
    $html .= get_bpf_f2();
    $html .= get_bpf_f3();
    $html .= get_bpf_f4();
    $html .= get_bpf_g();
    $html .= $wpof->toc->get_html_toc("bpf");
    
    /*
    if ($role == 'admin')
    {
        $html .= "<pre>";
        $html .= "Nb clients ".$nb_avant."/".count($Client)."\n";
        $html .= "Nb sessions ".count($SessionFormation)."\n";
        foreach($SessionFormation as $s)
        {
            if (date("Y", $s->first_date_timestamp) != 2021)
                $html .= $s->id." ".$s->titre_session."\n".var_export(array_keys($s->creneaux), true)."\n";
        }
        $html .= "</pre>";
    }
    */
    return $html;
}

// Bilan financier HT : origine des produits de l'OF
function get_bpf_c()
{
    global $wpof;
    global $SessionFormation;
    global $Client;
    
    $total_produits = 0;
    $financement = array();
    foreach(array_keys($wpof->financement->term) as $k)
        $financement[$k] = 0;
    $fin_erreur = array();
    $fin_ctl = array();
    $fin_total = 0;
    
    foreach($Client as $client)
    {
        if ($wpof->financement->is_term($client->financement) && (int)$client->tarif_total_chiffre != 0)
        {
            $financement[$client->financement] += ((int)$client->tarif_total_chiffre - (int)$client->tarif_total_autres_chiffre);
            $fin_ctl[$client->session_formation_id][$client->id] = $client->financement;
        }
        else
            $fin_erreur[$client->session_formation_id][$client->id] = $client->financement;
        
        $financement['autres'] += (int)$client->tarif_total_autres_chiffre;
        $fin_total += (int)$client->tarif_total_chiffre;
    }
    
    ob_start();
    echo $wpof->toc->set_title("C. Bilan financier hors taxes : origine des produits de l'organisme", 2); ?>
    
    <?php
        $sous_total = $financement["prive"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("prive"); ?><span class="bpf_right">1. </span><span class="bpf_value"><?php echo get_copy_span(numf($sous_total)); ?></span></p>
    
    <div class="bpf_group">
        <h3 class="groupe"><?php echo $wpof->financement->group["g1"]; ?></h3>
        
        <?php
            $lettre = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
            $total_mutu = 0;
        ?>
        <?php for($i = 1; $i <= 8; $i++) : ?>
            <?php
                $sous_total_mutu = $financement["mutu".$i];
                $total_mutu += $sous_total_mutu;
            ?>
            <p class="sous_value"><?php echo $wpof->financement->get_term("mutu".$i); ?><span class="bpf_right"><?php echo $lettre[$i]; ?>. </span><span class="bpf_value"><?php echo get_copy_span(numf($sous_total_mutu)); ?></span></p>
        <?php endfor; ?>
        
        <?php
            $total_produits += $total_mutu;
        ?>
        <p class="value total"><?php echo __("Total")." ".$wpof->financement->group["g1"]." ".__("(total des lignes a à h)"); ?> <span class="bpf_right">2. </span><span class="bpf_value"><?php echo get_copy_span(numf($total_mutu)); ?></span></p>
    </div>
    
    <?php
        $sous_total = $financement["public"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("public"); ?><span class="bpf_right">3. </span><span class="bpf_value"><?php echo get_copy_span(numf($sous_total)); ?></span></p>
    
    <div class="bpf_group">
        <h3 class="groupe"><?php echo $wpof->financement->group["g2"]; ?></h3>
        
        <?php
            $total_pubspec = 0;
        ?>
        <?php for($i = 1; $i <= 5; $i++) : ?>
            <?php
                $sous_total_pubspec = $financement["pubspec".$i];
                $total_pubspec += $sous_total_pubspec;
            ?>
            <p class="sous_value"><?php echo $wpof->financement->get_term("pubspec".$i); ?><span class="bpf_right"><?php echo $i+3; ?>. </span><span class="bpf_value"><?php echo get_copy_span(numf($sous_total_pubspec)); ?></span></p>
        <?php endfor; ?>
        
        <?php
            $total_produits += $total_pubspec;
        ?>
    </div>
    
    <?php
        $sous_total = $financement["part"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("part"); ?><span class="bpf_right">9. </span><span class="bpf_value"><?php echo get_copy_span(numf($sous_total)); ?></span></p>
    
    <?php
        $sous_total = $financement["opac"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("opac"); ?><span class="bpf_right">10. </span><span class="bpf_value"><?php echo get_copy_span(numf($sous_total)); ?></span></p>
    
    <?php
        $sous_total = $financement["autres"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("autres"); ?><span class="bpf_right">11. </span><span class="bpf_value"><?php echo get_copy_span(numf($sous_total)); ?></span></p>
    
    <?php $erreur_total = (abs($fin_total - $total_produits) > 0.5); ?>
    <p class="total"><?php _e("Total des produits réalisés au titre de la formation professionnelle (total des lignes 1 à 11)"); ?>
    <span class="bpf_value"><?php 
        echo get_copy_span(numf($total_produits));
        if ($erreur_total === true)
            echo "<br /><span class='erreur'>(+ ".($fin_total - $total_produits)." ".__("à réaffecter").")</span>";
    ?></span>
    </p>
    <?php
    
    // contrôle : liste par type de financement réellement indiqués dans les sessions (permet de vérifier les infos erronées)
    ?>
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-financement"><?php _e("Voir en détails"); ?></span><?php if ($erreur_total) the_wpof_fa("triangle-exclamation", "fa-lg fa-bounce important"); ?></p>
    <div class="blocHidden" id="liste-financement">
    <?php if ($erreur_total) : ?>
    <p><?php _e("Erreurs, manques à corriger"); ?></p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Mode de financement actuel"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Facture"); ?></td><td><?php _e("Montant total HT"); ?></td></tr>
    <?php
        foreach($fin_erreur as $session => $fin) :
            foreach($fin as $client_id => $f)
            {
                if (!$wpof->financement->is_term($f)) : ?>
                    <tr class="editable">
                    <td><?php echo get_input_jpost($Client[$client_id], "financement", array('select' => 1, 'aide' => false)); ?></td>
                    <td><?php echo $SessionFormation[$session]->get_id_link()." ".$SessionFormation[$session]->get_a_link(); ?></td>
                    <td><?php echo (!empty($Client[$client_id]->numero_contrat)) ? $Client[$client_id]->numero_contrat." " : " "; echo $Client[$client_id]->get_displayname(true); ?></td>
                    <td><?php echo get_input_jpost($Client[$client_id], "numero_facture", array('input' => 'text', 'aide' => false)); ?></td>
                    <td class="fullwidth<?php if ($Client[$client_id]->tarif_total_chiffre == 0) : ?> bg-alerte<?php endif; ?>"><?php echo get_input_jpost($Client[$client_id], "tarif_total_chiffre", array('input' => 'number', 'step' => '0.01', 'min' => 0, 'aide' => false)); ?></td>
                    </tr>
                <?php endif;
            }
        endforeach; ?>
    <tr class="total"><td colspan="4"><?php _e("Total mauvaise affectation"); ?></td><td><?php echo $fin_total - array_sum($financement); ?></td></tr>
    <tr class="total"><td colspan="4"><?php _e("Total tout"); ?></td><td><?php echo $fin_total; ?></td></tr>
    </table>
    <?php endif; // erreur_total ?>
    <p><?php _e("Modes de financement à priori corrects, pour vérification"); ?></p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Mode de financement actuel"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Facture"); ?></td><td><?php _e("Montant total HT"); ?></td></tr>
    <?php
        foreach($fin_ctl as $session => $fin) :
            foreach($fin as $client_id => $f)
            {
                ?>
                <tr class="editable">
                <td><?php echo get_input_jpost($Client[$client_id], "financement", array('select' => 1, 'aide' => false)); ?></td>
                <td><?php echo $SessionFormation[$session]->get_id_link()." ".$SessionFormation[$session]->get_a_link(); ?></td>
                <td><?php echo (!empty($Client[$client_id]->numero_contrat)) ? $Client[$client_id]->numero_contrat." " : " "; echo $Client[$client_id]->get_displayname(true); ?></td>
                <td><?php echo get_input_jpost($Client[$client_id], "numero_facture", array('input' => 'text', 'aide' => false)); ?></td>
                <td class="fullwidth" <?php if ($Client[$client_id]->tarif_total_chiffre == 0) : ?> class="bg-alerte" <?php endif; ?>><?php echo get_input_jpost($Client[$client_id], "tarif_total_chiffre", array('input' => 'number', 'step' => '0.01', 'min' => 0, 'aide' => false)); ?></td>
                </tr>
                <?php
            }
        endforeach; ?>
    </table>
    
    </div>
    
    </div>
    <?php
    return ob_get_clean();
}

// Bilan financier HT : charges de l'OF
function get_bpf_d()
{
    ob_start();
    return ob_get_clean();
}

// Personnes dispensant des heures de formation
function get_bpf_e()
{
    global $wpof;
    global $SessionFormation;
    
    $formateur_interne = array();
    $formateur_interne_st_only = array();
    $formateur_st = array();
    
    $session_ctl = array();
    
    foreach($SessionFormation as $session)
    {
        $only_soustraitance = $session->has_soustraitance(true);
        
        foreach($session->formateur as $f)
        {
            if (empty(get_user_meta($f, "sous_traitant", true)))
            {
                if (isset($formateur_interne[$f]))
                    $formateur_interne[$f] += $session->nb_heure_decimal;
                else
                    $formateur_interne[$f] = $session->nb_heure_decimal;
                if ($only_soustraitance)
                {
                    if (isset($formateur_interne_st_only[$f]))
                        $formateur_interne_st_only[$f] += $session->nb_heure_decimal;
                    else
                        $formateur_interne_st_only[$f] = $session->nb_heure_decimal;
                }
            }
            else
            {
                if (!isset($formateur_st[$f]))
                    $formateur_st[$f] = 0;
                    
                $formateur_st[$f] += $session->nb_heure_decimal;
            }
                
            if (empty($session_ctl[$f]))
                $session_ctl[$f] = array();
            $session_ctl[$f][$session->id] = array('duree' => $session->nb_heure_decimal, 'st' => $only_soustraitance);
        }
    }
    
    ob_start();
    echo $wpof->toc->set_title("E. Personnes dispensant des heures de formation", 2); ?>
    <p><strong>Attention</strong> lorsque qu'une session est animée par plus d'un formateur, la durée de la formation est attribuée à chacun, pouvant entraîner des comptes multiples de temps ! À corriger à la main pour l'instant</p>
    <table class="opaga opaga2">
    <tr><td></td><td><?php _e("Nombre de formateurs"); ?></td><td><?php _e("Nombre d'heures de formation"); ?></td></tr>
    <tr>
    <td><?php _e("Personnes de votre organisme dispensant des heures de formation"); ?></td>
    <td><?php echo get_copy_span(numf(count($formateur_interne))); ?></td>
    <td><?php echo get_copy_span(numf(array_sum($formateur_interne))); ?></td>
    </tr>
    <tr>
    <td><?php printf(__("Personnes dispensant exclusivement en sous-traitance pour un autre OF que %s"), $wpof->of_nom); ?></td>
    <td><?php echo numf(count($formateur_interne_st_only)); ?></td>
    <td><?php echo numf(array_sum($formateur_interne_st_only)); ?></td>
    </tr>
    <tr>
    <td><?php _e("Personnes extérieures à votre organisme dispensant des heures de formation dans le cadre de contrats de sous-traitance"); ?></td>
    <td><?php echo get_copy_span(numf(count($formateur_st))); ?></td>
    <td><?php echo get_copy_span(numf(array_sum($formateur_st))); ?></td>
    </tr>
    </table>
    
    <?php
    
    // contrôle : liste des formateurs interne et externe
    ?>
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-formateur"><?php _e("Voir en détails"); ?></span></p>
    <div class="blocHidden" id="liste-formateur">
    <p><?php _e("Définissez les formateurs sous-traitants depuis la page"); ?> <a href="<?php echo home_url()."/".$wpof->url_gestion."/formateur"; ?>"><?php _e("Équipe pédagogique"); ?></a></p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Formateur⋅trice"); ?></td><td><?php _e("Session (nb h)"); ?></td><td><?php _e("Nombre d'heures"); ?></td></tr>
    <?php foreach($formateur_interne as $user_id => $nb_heures) : ?>
    <tr><td><?php echo get_displayname($user_id, false); ?> <strong>(<?php _e("interne"); ?>)</strong></td>
    <td>
        <?php
        $liste_session_heure = array();
        foreach ($session_ctl[$user_id] as $sid => $data) : ?>
            <div <?php if ($data['duree'] < 1) : ?> class="bg-alerte" <?php endif; ?>>
            <a href="<?php echo $SessionFormation[$sid]->permalien; ?>"><?php echo ($SessionFormation[$sid]->numero != "") ? $SessionFormation[$sid]->numero : $sid; ?></a>
            <?php echo '('.$data['duree'].' h)'; echo ($data['st']) ? ' <strong>'.__("en sous-traitance").'</strong>' : ''; ?>
            </div>
        <?php endforeach; ?>
    </td>
    <td><?php echo numf($nb_heures); ?></td></tr>
    <?php endforeach; ?>
    <?php foreach($formateur_st as $user_id => $nb_heures) : ?>
    <tr><td><?php echo get_displayname($user_id, false); ?> <em>(<?php _e("externe"); ?>)</em></td>
    <td>
        <?php
        $liste_session_heure = array();
        foreach ($session_ctl[$user_id] as $sid => $data) : ?>
            <div <?php if ($data['duree'] < 1) : ?> class="bg-alerte" <?php endif; ?>>
            <a href="<?php echo $SessionFormation[$sid]->permalien; ?>"><?php echo ($SessionFormation[$sid]->numero != "") ? $SessionFormation[$sid]->numero : $sid; ?></a>
            <?php echo '('.$data['duree'].' h)'; echo ($data['st']) ? ' <strong>'.__("en sous-traitance").'</strong>' : ''; ?>
            </div>
        <?php endforeach; ?>
    </td>
    <td><?php echo numf($nb_heures); ?></td></tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Type de stagiaire de l'OF
function get_bpf_f1()
{
    global $wpof;
    global $SessionFormation, $Client, $SessionStagiaire;
    
    // tableau pour remonter les durées nulles
    $erreur_duree = array();
    // tableau pour les erreurs de statut
    $erreur_statut = array();
    $ctl_statut = array(); // pour lister précisément les statuts à priori bien renseignés
    $total_heure_erreur = 0;
    $total_stagiaire_erreur = 0;
    
    $statut_stagiaire = array();
    foreach(array_keys($wpof->statut_stagiaire->term) as $k)
        $statut_stagiaire[$k] = array();
    $total_stagiaires = $total_heures = 0;
    $total_stagiaires_a_distance = 0;
    
    foreach($Client as $client)
    {
        if ($client->financement != "opac")
        {
            if (!isset($SessionFormation[$client->session_formation_id]))
            {
                log_add("Client ".$client->id." sans session !");
                continue;
            }
            $session = $SessionFormation[$client->session_formation_id];
            $session->init_stagiaires();
            $session_a_distance = $session->is_a_distance();
            
            foreach($client->stagiaires as $user_id)
            {
                $stagiaire = new SessionStagiaire($session->id, $user_id);
                if (!empty($stagiaire->statut_stagiaire) && $wpof->statut_stagiaire->is_term($stagiaire->statut_stagiaire) && !empty($stagiaire->nb_heure_estime_decimal))
                {
                    $statut_stagiaire[$stagiaire->statut_stagiaire][] = (float) $stagiaire->nb_heure_estime_decimal;
                    $ctl_statut[] = array
                        (
                            'statut' => get_input_jpost($stagiaire, "statut_stagiaire", array('select' => 1, 'class' => 'flexrow', 'aide' => false)),
                            'session_id' => $session->get_a_link(),
                            'client' => $client->get_nom(),
                            'stagiaire' => $stagiaire->get_displayname(true),
                            'duree' => get_input_jpost($stagiaire, "nb_heure_estime_decimal", array('input' => 'number', 'step' => '0.25', 'aide' => false))
                        );
                }
                else
                {
                    $erreur_statut[] = array
                        (
                            'statut' => get_input_jpost($stagiaire, "statut_stagiaire", array('select' => 1, 'class' => 'flexrow', 'aide' => false)),
                            'session_id' => $session->get_a_link(),
                            'client' => $client->get_nom(),
                            'stagiaire' => $stagiaire->get_displayname(true),
                            'duree' => get_input_jpost($stagiaire, "nb_heure_estime_decimal", array('input' => 'number', 'step' => '0.25', 'aide' => false, 'class' => (empty($stagiaire->nb_heure_estime_decimal)) ? "bg-alerte" : ""))
                        );
                    $total_heure_erreur += (float) $stagiaire->nb_heure_estime_decimal;
                    $total_stagiaire_erreur ++;
                }
                $total_stagiaires++;
                if ($session_a_distance)
                    $total_stagiaires_a_distance++;
                $total_heures += $stagiaire->nb_heure_estime_decimal;
                
                if ($stagiaire->nb_heure_estime_decimal == 0)
                {
                    if (empty($erreur_duree[$session->id]))
                        $erreur_duree[$session->id] = array();
                    $erreur_duree[$session->id][] = $stagiaire->get_displayname(true);
                }
            }
        }
    }
    
    $total_stagiaires_affiche = $total_heures_affiche = 0;
    ob_start();
    echo $wpof->toc->set_title("F1. Type de stagiaires de l'organisme", 2); ?>
    
    <table class="opaga opaga2">
    <tr><td></td><td><?php _e("Nombre de stagiaires ou apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys($wpof->statut_stagiaire->term) as $k) : ?>
    <tr>
    <td><?php echo $wpof->statut_stagiaire->get_term($k); ?></td>
    <td><?php $total_stagiaires_affiche += count($statut_stagiaire[$k]); echo get_copy_span(numf(count($statut_stagiaire[$k]))); ?></td>
    <td><?php $total_heures_affiche += array_sum($statut_stagiaire[$k]); echo get_copy_span(numf(array_sum($statut_stagiaire[$k]))); ?></td>
    </tr>
    <?php endforeach; ?>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo get_copy_span(numf($total_stagiaires)); ?></td><td><?php echo get_copy_span(numf($total_heures)); ?></td></tr>
    <td><?php _e("dont stagiaires et apprentis ayant suivi une action en tout ou partie à distance"); ?></td><td><?php echo get_copy_span(numf($total_stagiaires_a_distance)); ?></td><td></td></tr>
    <?php if ($total_heures != $total_heures_affiche) : ?>
    <tr class="erreur"><td><?php _e("Dont total des mauvaises affectations (à corriger)"); ?></td><td><?php echo numf($total_stagiaires - $total_stagiaires_affiche); ?></td><td><?php echo $total_heures - $total_heures_affiche; ?></td></tr>
    <?php endif; ?>
    </table>
    
    <div class="bpf_ctl">
    <?php if (!empty($erreur_duree)) : ?>
    <p><span class="openButton" data-id="erreur-duree-stagiaire"><?php _e("Attention ! certains stagiaires ont un nombre d'heures égal à 0"); ?></span>
    <?php if (!empty($erreur_duree)) the_wpof_fa("triangle-exclamation", "fa-lg fa-bounce important"); ?></p>
    <div class="blocHidden" id="erreur-duree-stagiaire">
    <table class="opaga opaga2">
    <?php foreach ($erreur_duree as $sid => $stag_zero) : ?>
    <tr><td><?php echo implode(", ", $stag_zero); ?></td>
    <td><?php echo $SessionFormation[$sid]->get_a_link(); ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    <?php endif; ?>
    <p><span class="openButton" data-id="liste-type-stagiaire"><?php _e("Voir en détails"); ?></span>
    <?php if (!empty($erreur_duree)) the_wpof_fa("triangle-exclamation", "fa-lg fa-bounce important"); ?></p>
    <div class="blocHidden" id="liste-type-stagiaire">
    <?php if (count($erreur_statut) > 0) : ?>
    <p><?php _e("Définissez les types de stagiaire ici"); ?>.</p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Statut invalide, ancien ou non défini"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Stagiaire"); ?></td><td><?php _e("Durée de formation"); ?></td></tr>
    <?php foreach($erreur_statut as $row) : ?>
    <tr class="editable">
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    <?php endif; ?>
    <p><?php _e("Statuts des stagiaires déjà affectés (pour vérification)"); ?></p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Statut"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Stagiaire"); ?></td><td><?php _e("Durée de formation"); ?></td></tr>
    <?php foreach($ctl_statut as $row) : ?>
    <tr class="editable">
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// dont activité est sous-traitée de l'OF (formateur sous-traitant)
function get_bpf_f2()
{
    global $wpof;
    global $SessionFormation, $Client;
    
    $session_st = array();
    $total_stagiaires = $total_heures = 0;
    
    $session_st_ctl = array();
    
    foreach($SessionFormation as $session)
    {
        foreach($session->formateur as $f)
        {
            if (!empty(get_user_meta($f, "sous_traitant", true)))
            {
                // on fait un classement par session pour une vérif ultérieure
                if (!isset($session_st[$session->id]))
                    $session_st[$session->id] = array("stagiaire" => 0, "heure" => 0);
                foreach($session->clients as $cid)
                {
                    if ($Client[$cid]->financement == "opac") continue;
                    
                    foreach($Client[$cid]->stagiaires as $sid)
                    {
                        $stagiaire = new SessionStagiaire($session->id, $sid);
                        $session_st[$session->id]["heure"] += $stagiaire->nb_heure_estime_decimal;
                        $session_st[$session->id]["stagiaire"] ++;
                        
                        $session_st_ctl[] = array('session_id' => $session->get_id_link(), 'formateur' => get_displayname($f, false), 'client' => $Client[$cid]->get_nom(), 'stagiaire' => $stagiaire->get_displayname(), 'duree' => $stagiaire->nb_heure_estime_decimal);
                    }
                }
                $total_stagiaires += $session_st[$session->id]["stagiaire"];
                $total_heures += $session_st[$session->id]["heure"];
            }
        }
    }

    ob_start();
    echo $wpof->toc->set_title("F2. Dont activité sous-traitée", 2); ?>
    <table class="opaga opaga2">
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <tr>
    <td><?php _e("Stagiaire ou apprentis dont l'action a été confiée par votre organisme à un autre organisme"); ?></td>
    <td><?php echo get_copy_span(numf($total_stagiaires)); ?></td>
    <td><?php echo get_copy_span(numf($total_heures)); ?></td>
    </tr>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-sous-traitant"><?php _e("Voir en détails"); ?></span></p>
    <div class="blocHidden" id="liste-sous-traitant">
    <table class="opaga opaga2">
    <tr><td><?php _e("Session ID"); ?></td><td><?php _e("Formateur⋅trice"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Stagiaire"); ?></td><td><?php _e("Durée"); ?></td></tr>
    <?php foreach($session_st_ctl as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Objectif général des prestations dispensées
function get_bpf_f3()
{
    global $wpof;
    global $SessionFormation, $Client;
    
    $total_stagiaires = $total_heures = 0;
    $nature_formation = array();
    $ctl_nature = array();
    $erreur_nature = array();
    foreach(array_keys($wpof->nature_formation->term) as $k)
        $nature_formation[$k] = array('duree' => 0, 'stagiaires' => 0);
    
    foreach($Client as $client)
    {
        if ($client->financement != "opac")
        {
		if (!isset($SessionFormation[$client->session_formation_id]))
		{
			log_add("Client ".$client->id." sans session !");
			continue;
		}
            $session = $SessionFormation[$client->session_formation_id];
            $client_nb_stagiaires = count($client->stagiaires);
            $client_nb_heure_stagiaire = $client->get_nb_heure_stagiaire();
                
            $total_stagiaires += $client_nb_stagiaires;
            $total_heures += $client_nb_heure_stagiaire;
            
            if ($session->formation_id > 0)
            {
                $entite = get_formation_by_id($session->formation_id);
                $entite_type = "Formation";
            }
            else
            {
                $entite = $session;
                $entite_type = "Session unique";
            }
            if (!empty($entite->nature_formation) && $wpof->nature_formation->is_term($entite->nature_formation))
            {
                $ctl_nature[] = array
                    (
                        'nature' => get_input_jpost($entite, "nature_formation", array('select' => 1, 'class' => 'flexrow', 'aide' => false)),
                        'session_id' => $entite->get_a_link()." (".$entite_type.")",
                        'client' => $client->get_nom(),
                        'stagiaires' => $client_nb_stagiaires,
                        'duree' => $client_nb_heure_stagiaire
                    );
                $nature_formation[$entite->nature_formation]["stagiaires"] += $client_nb_stagiaires;
                $nature_formation[$entite->nature_formation]["duree"] += $client_nb_heure_stagiaire;
            }
            else
            {
                $erreur_nature[] = array
                    (
                        'nature' => get_input_jpost($entite, "nature_formation", array('select' => 1, 'class' => 'flexrow', 'aide' => false)),
                        'session_id' => $entite->get_id_link(),
                        'client' => $client->get_nom(),
                        'stagiaires' => count($client->stagiaires),
                        'duree' => $client->get_nb_heure_stagiaire()
                    );
            }
        }
    }
  
    $total_stagiaires_affiche = $total_heures_affiche = 0;
    ob_start();
    echo $wpof->toc->set_title("F3. Objectif général des prestations dispensées", 2); ?>
    <table class="opaga opaga2">
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys($wpof->nature_formation->term) as $k) : ?>
    <tr>
    <td><?php echo $wpof->nature_formation->get_term($k); ?></td>
    <td><?php $total_stagiaires_affiche += $nature_formation[$k]["stagiaires"]; echo get_copy_span(numf($nature_formation[$k]["stagiaires"])); ?></td>
    <td><?php $total_heures_affiche += $nature_formation[$k]["duree"]; echo get_copy_span(numf($nature_formation[$k]["duree"])); ?></td>
    </tr>
    <?php endforeach; ?>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo get_copy_span(numf($total_stagiaires)); ?></td><td><?php echo get_copy_span(numf($total_heures)); ?></td></tr>
    <?php if (!empty($erreur_nature)) : ?>
    <tr class="erreur">
    <td><?php _e("Dont total des mauvaises affectations (à corriger)"); ?></td><td><?php echo numf($total_stagiaires - $total_stagiaires_affiche); ?></td><td><?php echo numf($total_heures - $total_heures_affiche); ?></td></tr>
    <?php endif; ?>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-nature"><?php _e("Voir en détails"); ?></span><?php if (!empty($erreur_nature)) the_wpof_fa("triangle-exclamation", "fa-lg fa-bounce important"); ?></p>
    <div class="blocHidden edit-data" id="liste-nature">
    <?php if (!empty($erreur_nature)) : ?>
    <p><?php _e("Corrigez les objectifs de prestation (nature de formation) ici"); ?>.</p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Objectif de prestation invalide, ancien ou non défini"); ?></td><td><?php _e("Formation ou session"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($erreur_nature as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    <?php endif; ?>
    <p><?php _e("Pour information les objectifs de prestation (nature de formation) <strong>à priori</strong> correctement renseignés"); ?>.</p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Objectif de prestation"); ?></td><td><?php _e("Formation ou session"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($ctl_nature as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Spécialités de formation
function get_bpf_f4($tri = "heures")
{
    global $wpof;
    global $SessionFormation, $Client;
    
    $total_stagiaires = $total_heures = 0;
    $specialites = array();
    
    // pour vérification
    $ctl_spec = array();
    $erreur_spec = array();
    
    foreach($Client as $client)
    {
        if ($client->financement != "opac")
        {
            if (!isset($SessionFormation[$client->session_formation_id]))
            {
                log_add("Client ".$client->id." sans session !");
                continue;
            }
            $session = $SessionFormation[$client->session_formation_id];
            $client_nb_stagiaires = count($client->stagiaires);
            $client_nb_heure_stagiaire = $client->get_nb_heure_stagiaire();
                
            $total_stagiaires += $client_nb_stagiaires;
            $total_heures += $client_nb_heure_stagiaire;
            
            if ($session->formation_id > 0)
            {
                $entite = get_formation_by_id($session->formation_id);
                $entite_type = "Formation";
            }
            else
            {
                $entite = $session;
                $entite_type = "Session unique";
            }
            if (!empty($session->specialite) && $wpof->specialite->is_term($session->specialite))
            {
                $ctl_spec[] = array
                    (
                        'specialite' => get_input_jpost($entite, "specialite", array('select' => 1, 'class' => 'flexrow', 'aide' => false)),
                        'session_id' => $entite->get_a_link()." (".$entite_type.")",
                        'client' => $client->get_nom(),
                        'stagiaires' => $client_nb_stagiaires,
                        'duree' => $client_nb_heure_stagiaire
                    );
                if (!isset($specialites[$session->specialite]))
                    $specialites[$session->specialite] = array('duree' => 0, 'stagiaires' => 0);
                $specialites[$session->specialite]["stagiaires"] += $client_nb_stagiaires;
                $specialites[$session->specialite]["duree"] += $client_nb_heure_stagiaire;
            }
            else
            {
                $erreur_spec[] = array
                    (
                        'specialite' => get_input_jpost($entite, "specialite", array('select' => 1, 'class' => 'flexrow', 'aide' => false)),
                        'session_id' => $entite->get_a_link()." (".$entite_type.")",
                        'client' => $client->get_nom(),
                        'stagiaires' => count($client->stagiaires),
                        'duree' => $client->get_nb_heure_stagiaire()
                    );
            }
        }
    }
    
    // on trie ensuite le tableau $specialites par stagiaires et par heures
    $spec_stagiaires = array();
    $spec_heures = array();
    
    $nb_spec = 5;
    
    foreach($specialites as $k => $val)
    {
        $spec_stagiaires[$k] = $val['stagiaires'];
        $spec_heures[$k] = $val['duree'];
    }
    arsort($spec_stagiaires);
    arsort($spec_heures);
    //ksort($spec_ctl);
    
    $total_stagiaires_affiche = array_sum($spec_stagiaires);
    $total_heures_affiche = array_sum($spec_heures);
    
    $spec_ref = ($tri == "stagiaires") ? $spec_stagiaires : $spec_heures;
    
    ob_start();
    echo $wpof->toc->set_title("F4. Spécialités de formation", 2); ?>
    <p class="value"><?php _e("Cinq principales spécialités de formation"); ?></p>
    <table class="opaga opaga2">
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys(array_slice($spec_ref, 0, $nb_spec, true)) as $k) : ?>
    <tr>
    <td><?php echo $wpof->specialite->get_term($k); ?></td>
    <td><?php echo get_copy_span(numf($spec_stagiaires[$k])); ?></td>
    <td><?php echo get_copy_span(numf($spec_heures[$k])); ?></td>
    </tr>
    <?php endforeach; ?>
    <tr>
    <td><?php _e("Autres spécialités"); ?></td>
    <td><?php echo get_copy_span(numf(array_sum(array_intersect_key($spec_stagiaires, array_slice($spec_ref, $nb_spec, NULL, true))))); ?></td>
    <td><?php echo get_copy_span(numf(array_sum(array_intersect_key($spec_heures, array_slice($spec_ref, $nb_spec, NULL, true))))); ?></td>
    </tr>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo get_copy_span(numf($total_stagiaires)); ?></td><td><?php echo get_copy_span(numf($total_heures)); ?></td></tr>
    <?php if (!empty($erreur_spec)) : ?>
    <tr class="erreur">
    <td><?php _e("Dont total des mauvaises affectations (à corriger)"); ?></td><td><?php echo numf($total_stagiaires - $total_stagiaires_affiche); ?></td><td><?php echo numf($total_heures - $total_heures_affiche); ?></td></tr>
    <?php endif; ?>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-specialite"><?php _e("Voir en détails"); ?></span>
    <?php if (!empty($erreur_spec)) the_wpof_fa("triangle-exclamation", "fa-lg fa-bounce important"); ?></p>
    </p>
    <div class="blocHidden edit-data" id="liste-specialite">
    <h3><?php _e("Détails des autres spécialités"); ?></h3>
    <table class="opaga opaga2">
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys(array_slice($spec_ref, $nb_spec, NULL, true)) as $k) : ?>
    <tr>
    <td><?php echo $wpof->specialite->get_term($k); ?></td>
    <td><?php echo get_copy_span(numf($spec_stagiaires[$k])); ?></td>
    <td><?php echo get_copy_span(numf($spec_heures[$k])); ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    
    <h3><?php _e("Détail par session"); ?></h3>
    <?php if (!empty($erreur_spec)) : ?>
    <p><?php _e("Corrigez les spécialités inconnues ou manquantes ici."); ?>.</p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Spécialité"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($erreur_spec as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    <?php endif; ?>
    <p><?php _e("Pour information les spécialités à priori correctement définies."); ?>.</p>
    <table class="opaga opaga2">
    <tr><td><?php _e("Spécialité"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($ctl_spec as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Stagiaires dont la formation a été confiée à votre OF par un autre OF (votre OF est sous-traitant d'un autre OF)
function get_bpf_g()
{
    global $SessionFormation, $Client;
    global $wpof;
    
    $total_stagiaires = $total_heures = 0;
    $ctl_sous_traitance = array();
    $erreur_detectee = false;
    
    foreach($Client as $client)
    {
        if (!isset($SessionFormation[$client->session_formation_id]))
        {
            log_add("Client ".$client->id." sans session !");
            continue;
        }
        $session = $SessionFormation[$client->session_formation_id];
        if ($client->financement == "opac")
        {
            $total_stagiaires += (int)$client->nb_stagiaires;
            $total_heures += (int)$client->nb_heures_stagiaires;
            
            $class_stagiaires = ((int)$client->nb_stagiaires < 1) ? "bg-alerte" : "";
            $class_duree = ((int)$client->nb_heures_stagiaires <= (int)$client->nb_stagiaires) ? "bg-alerte" : "";
            if (!empty($class_duree.$class_stagiaires))
                $erreur_detectee = true;
            $ctl_sous_traitance[] = array
                (
                    'session_id' => $session->get_a_link(),
                    'formateur' => $session->get_formateurs_noms(),
                    'client' => $client->get_displayname(true),
                    'stagiaires' => get_input_jpost($client, 'nb_stagiaires', array('input' => 'number', 'aide' => false, 'class' => $class_stagiaires)),
                    'duree' => get_input_jpost($client, 'nb_heures_stagiaires', array('input' => 'number', 'step' => '0.25', 'aide' => false, 'class' => $class_duree))
                );
        }
    }
    
    ob_start();
    echo $wpof->toc->set_title("G. Stagiaires dont la formation a été confiée à votre organisme par un autre OF", 2); ?>
    <table class="opaga opaga2">
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <tr>
    <td><?php _e("Formations confiées à votre organisme par un autre organisme de formation"); ?></td>
    <td><?php echo get_copy_span(numf($total_stagiaires)); ?></td>
    <td><?php echo get_copy_span(numf($total_heures)); ?></td>
    </tr>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-sous-traite"><?php _e("Voir en détails"); ?></span><?php if ($erreur_detectee) the_wpof_fa("triangle-exclamation", "fa-lg fa-bounce important"); ?></p>
    <div class="blocHidden" id="liste-sous-traite">
    <table class="opaga opaga2">
    <tr><td><?php _e("Session ID"); ?></td><td><?php _e('Formateur⋅ices'); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($ctl_sous_traitance as $row) : ?>
    <tr class="editable">
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

?>
