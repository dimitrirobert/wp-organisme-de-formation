<?php
/*
 * page-user.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_user_content($params = array())
{
    global $wpof;
    $connected_user_id = get_current_user_id();
    
    $html = "";
    if (!empty($params['username']))
        $user = get_user_by('login', $params['username']);
    else
        $user = wp_get_current_user();
    
    if ($user)
    {
        $formateur = new Formateur($user->ID);
        ob_start();
        if ($user->ID == $connected_user_id || in_array(wpof_get_role($connected_user_id), $wpof->super_roles))
            $formateur->board_profil();
        elseif ($formateur->profil_public == 1) // à priori on ne passe jamais ici : le cas vue_publique est géré dans init_opaga_page()
            $formateur->vue_publique();
        $html .= ob_get_clean();
    }
    
    return $html;
}
