<?php
/*
 * odoo.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

require_once(wpof_path . "/class/class-plugin.php");
require_once(wpof_path . '/class/class-erp.php');

class LocalErp extends ERP implements OpagaPlugin
{
    public static function getPluginName()
    {
        return 'ERP basique';
    }

    public static function getPluginDescription()
    {
        return "Connecteur ERP local (si pas d'ERP externe par exemple) pour les clients, factures, devis, etc.";
    }

    public static function getOptions()
    {
        return [];
    }

    public static function loadAdmin()
    {
        return "";
    }

    function __construct()
    {

    }

    public function canGetClients(): bool
    {
        return false;
    }

    public function getClients(Formateur $formateurice, string $entite_client = null): array
    {
        global $wpof;
        $clients = [Client::ENTITE_PERS_MORALE => [], Client::ENTITE_PERS_PHYSIQUE => []];

        foreach ($formateurice->get_my_clients($entite_client) as $entite => $client_by_entite) {
            foreach ($client_by_entite as $cid => $client) {
                $clients[$entite][$cid] = $client->get_displayname(false);
            }
        }

        return $clients;
    }

    public function getCreateClientUrl(SessionFormation $session, string $msg = 'Créez le !'): string
    {
        return ' <a class="dynamic-dialog close" data-function="new_client" data-sessionid="' . $session->id . '">' . $msg . '</a>';
    }


    public function haveFactureLink(Client $client): bool
    {
        return false;
    }

    public function getConnectionButton(int $session_id, int $client_id): String
    {
        return "";
    }

    public function getCreateFactureUrl(Client $client, SessionFormation $session, float $total, bool $is_acompte, bool $is_solde = false, array $http_params = []): bool
    {
        return $this->haveFactureLink($client);
    }

    public function getFacturesClient(Client $client, SessionFormation $session, bool $is_acompte = true): bool
    {
        return $this->haveFactureLink($client);
    }

    public function getFacturesClientUrl(Client $client, SessionFormation $session, bool $is_acompte = true): bool
    {
        return $this->haveFactureLink($client);
    }

    public function getClientInfos(string $client_id): array
    {
        $client_infos = [
            'contact'     => [
                'adresse'     => get_client_meta($client_id, 'adresse'),
                'code_postal' => get_client_meta($client_id, 'code_postal'),
                'ville'       => get_client_meta($client_id, 'ville'),
                'pays'        => get_client_meta($client_id, 'pays'),
                'telephone'   => get_client_meta($client_id, 'telephone'),
            ],
            'type_client' => get_client_meta($client_id, 'entite_client'),
        ];
        if ($client_infos['type_client'] == Client::ENTITE_PERS_PHYSIQUE) {
            $stagiaires   = unserialize(get_client_meta($client_id, 'stagiaires'));
            $stagiaire_id = $stagiaires[0];
            if (get_stagiaire_meta($stagiaire_id, 'prenom')) {
                $client_infos['prenom'] = get_stagiaire_meta($stagiaire_id, 'prenom');
                $client_infos['nom']    = get_stagiaire_meta($stagiaire_id, 'nom');
            } else {
                $identite               = explode(' ', $client_infos['nom'], 2);
                $client_infos['prenom'] = $client_id . " " . $stagiaire_id . " lala " . $identite[0];
                $client_infos['nom']    = $identite[1];
            }
            $client_infos['email'] = get_client_meta($client_id, 'email');
        } else {
            $client_infos['nom']   = get_client_meta($client_id, 'nom');
            $client_infos['siret'] = get_client_meta($client_id, 'siret');
        }

        return $client_infos;
    }


    public function createContrat(Contrat $contrat)
    {

    }
}