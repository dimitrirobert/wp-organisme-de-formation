<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Définition d'un nouveau logo pour la page de login de WordPress
 */ 
function my_login_logo()
{
    ?>
<style type="text/css">
body.login
{
    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
    background-position: center top;
    background-repeat: no-repeat;
}
</style>
<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// CodeMirror pour la CSS personnalisée
function codemirror_enqueue_scripts($hook)
{
    $cm_settings['codeEditor'] = wp_enqueue_code_editor(array('type' => 'text/css'));
    wp_localize_script('jquery', 'cm_settings', $cm_settings);
    wp_enqueue_script('wp-theme-plugin-editor');
    wp_enqueue_style('wp-codemirror');
}
add_action('admin_enqueue_scripts', 'codemirror_enqueue_scripts');

add_action( 'admin_enqueue_scripts', 'wpof_load_export_scripts', 21 );

/*
 * Page(s) d'options
 */


// suppression de menus et sous-menus
function remove_submenu()
{
    global $submenu;
    //supprimer le sous menu "themes"
    //unset($submenu['themes.php'][10]);
}

function remove_menu()
{
    global $menu;
    global $wpof;

    $hidden_menus = get_option("wpof_hidden_menus");
    if ($hidden_menus != "")
        foreach($hidden_menus as $num => $value)
            if ($value == 1) unset($menu[$num]);
}
add_action('admin_head', 'remove_menu');
//add_action('admin_head', 'remove_submenu');

///////////////
// Création d'une page d'options
///////////////
add_action('admin_menu', 'wpof_admin_add_page');
function wpof_admin_add_page()
{
    add_menu_page(__('OPAGA'), __('OPAGA'), 'manage_options', 'wpof', 'wpof_options_page', 'dashicons-admin-site', 1);
    add_submenu_page('wpof', __('Modèles'), __('Modèles'), 'manage_options', 'edit.php?post_type=modele');
}

//// Page principale

add_action('admin_init', 'wpof_admin_init');
function wpof_admin_init()
{
    wp_enqueue_media();
    global $wpof_options;
    foreach ($wpof_options as $o)
        register_setting('wpof', $o);
}

/*
 * Export des options du plugin au format JSON
 * Le fichier est enregistré dans le dossier du plugin
 */
function wpof_export_options()
{
    global $wpof;
    return json_encode($wpof, JSON_HEX_APOS|JSON_UNESCAPED_UNICODE);
}

function wpof_options_page()
{
    global $wpof, $tinymce_wpof_settings;
    $role = wpof_get_role(get_current_user_id());
    
    $tinymce_wpof_settings['quicktags'] = true;
    
    ?>
    <div class='wrap'>
    <h2><?php _e('Options — OPAGA, gestion de votre organisme de formation'); ?></h2>

    <?php
        // mise à jour ?
        if ($wpof->version < WPOF_VERSION)
            require_once(wpof_path . "/wpof-mise-a-jour.php");
    ?>
    <form method="post" action="options.php">
    
    <?php
    settings_fields('wpof');
    ?>
        
    <?php echo hidden_input("default_options_tab", (isset($_SESSION['options-tabs'])) ? $_SESSION['options-tabs'] : 0); ?>
    
    <div class="wpof-menu" id="options-tabs">
    <ul>
    <li><a href="#organisme-options"><?php _e("Votre organisme"); ?></a></li>
    <li><a href="#taches-options"><?php _e("Tâches"); ?></a></li>
    <li><a href="#opaga-options"><?php _e("Options d'OPAGA"); ?></a></li>
    <li><a href="#predefini-options"><?php _e("Champs prédéfinis"); ?></a></li>
    <li><a href="#pdf-options"><?php _e("Options PDF"); ?></a></li>
    <li><a href="#plugins-options"><?php _e("Extensions Opaga"); ?></a></li>
    
    <?php if (current_user_can('manage_options')): ?>
    <li><a href="#wpof-admin"><?php _e("Configuration avancée"); ?></a></li>
    <?php endif; ?>
    </ul>
    
    <div id="organisme-options">
    <?php show_organisme_options(); ?>
    </div>
    
    <div id="taches-options">
    <?php show_taches_options(); ?>
    </div>
    
    <div id="opaga-options">
    <?php show_opaga_options(); ?>
    </div>
    
    <div id="predefini-options">
    <?php show_predefini_options(); ?>
    </div>
    
    <div id="pdf-options">
        <?php show_pdf_options(); ?>
    </div>
    
    <div id="plugins-options">
    <?php show_plugins_options(); ?>
    </div>
    
    <?php if (current_user_can('manage_options')): ?>
    <div id="wpof-admin">
    <?php show_manage_options(); ?>
    </div>
    <?php endif; ?>

    </div> <!-- #options-tabs -->
    
    <?php submit_button(); ?>
    </form>
<?php
}

function show_organisme_options()
{
    global $wpof, $tinymce_wpof_settings;
    ?>
    
    <table class="form-table">
    <tr valign="top">
        <th scope="row"><?php _e('Organisme de formation gérant ce site'); ?></th>
        <td><input type="text" name="wpof_of_nom" value="<?php echo $wpof->of_nom; ?>" /></td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e('Coordonnées'); ?></th>
        <td>
        <label for="of_adresse"><?php _e("Adresse"); ?></label>
        <textarea id="of_adresse" name="wpof_of_adresse" placeholder="<?php _e('Adresse'); ?>" cols="60" rows="3"><?php echo $wpof->of_adresse; ?></textarea>
        <p>
        <label class="inline" for="ofcp"><?php _e('Code postal'); ?></label><input type="text" id="ofcp" name="wpof_of_code_postal" placeholder="<?php _e("Code postal");?>" value="<?php echo $wpof->of_code_postal; ?>" />
        <label class="inline" for="ofville"><?php _e('Ville'); ?></label><input type="text" id='ofville' name="wpof_of_ville" placeholder="<?php _e("Ville");?>" value="<?php echo $wpof->of_ville; ?>" />
        </p>
        <p>
        <label class="inline" for="ofemail"><?php _e('Email'); ?></label><input type="text" id='ofemail' name="wpof_of_email" placeholder="<?php _e("Email");?>" value="<?php echo $wpof->of_email; ?>" />
        <label class="inline" for="oftel"><?php _e('Téléphone'); ?></label><input type="text" id='oftel' name="wpof_of_telephone" placeholder="<?php _e("Numéro de téléphone"); ?>" value="<?php echo $wpof->of_telephone; ?>" />
        <label class="inline" for="ofurl"><?php _e('Lien Web'); ?></label><input type="text" id='ofurl' name="wpof_of_url" placeholder="<?php _e("Lien Web"); ?>" value="<?php echo $wpof->of_url; ?>" />
        </p>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e('Description libre'); ?></th>
        <td>
        <?php
            wp_editor($wpof->of_description, "wpof_of_description", $tinymce_wpof_settings);
        ?>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><p><?php _e('Logo'); ?></p>
        <p><a href="#" class="clickButton button-add-media" data-valueid="wpof_of_logo" data-linkmedia="wpof_of_logo_link" data-image="wpof_of_logo_img"><?php _e("Téléversez une image"); ?></a></p>
        </th>
        <td>
        <?php $of_logo = ($wpof->of_logo) ? $wpof->of_logo : ""; ?>
        <input type="hidden" id="wpof_of_logo" name="wpof_of_logo" value="<?php echo $of_logo; ?>" />
        <img id="wpof_of_logo_img" style="max-width: 300px; heigth: auto;" src="<?php echo wp_get_attachment_url($of_logo); ?>" />
        <a id="wpof_of_logo_link" target="_blank" href="<?php if ($of_logo != "") echo get_attachment_link($of_logo); ?>"><?php if ($of_logo != "") echo get_the_title($of_logo); ?></a>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("RCS"); ?></th>
        <td><input type="text" name="wpof_of_rcs" value="<?php echo $wpof->of_rcs; ?>" /></td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Numéro de SIRET"); ?></th>
        <td><input type="text" name="wpof_of_siret" value="<?php echo $wpof->of_siret; ?>" /></td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Numéro APE"); ?></th>
        <td><input type="text" name="wpof_of_ape" value="<?php echo $wpof->of_ape; ?>" /></td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Numéro TVA intra-communautaire"); ?></th>
        <td><input type="text" name="wpof_of_tva_intracom" value="<?php echo $wpof->of_tva_intracom; ?>" /></td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Numéro d'organisme de formation (déclaration d'activité auprès de la Dreets (ex-Dirrecte))"); ?></th>
        <td><input type="text" name="wpof_of_noof" value="<?php echo $wpof->of_noof; ?>" /></td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Numéro Datadock"); ?></th>
        <td><input type="text" name="wpof_of_datadock" value="<?php echo $wpof->of_datadock; ?>" /></td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e("Numéro Qualiopi"); ?></th>
        <td><input type="text" name="wpof_of_qualiopi" value="<?php echo $wpof->of_qualiopi; ?>" /></td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e("TVA"); ?></th>
        <td>
        <p>
        <label for="wpof_of_hastva">
        <input class="tva" type="checkbox" id="wpof_of_hastva" name="wpof_of_hastva" value="1" <?php checked(1, $wpof->of_hastva, true); ?> />
        <?php _e("Assujettis à la TVA ?"); ?></label>
        </p>
        <p>
        <?php
        $disabled_exotva ="";
        if (!$wpof->of_hastva)
            $disabled_exotva = "disabled='disabled'";
        ?>
        <label for="wpof_of_exotva">
        <input type="checkbox" id="wpof_of_exotva" name="wpof_of_exotva" <?php echo $disabled_exotva; ?> value="1" <?php checked(1, $wpof->of_exotva, true); ?> />
        <?php _e("Formation exonérée de TVA (article 261 du CGI) ?"); ?></label>
        </p>
        <p>
        <label><?php _e("Taux de TVA"); ?></label>
        <?php
        $disabled_tva ="";
        if (!$wpof->of_hastva)
            $disabled_tva = "disabled='disabled'";
        ?>
        <input type="number" min="0" max="100" step="0.01" name="wpof_of_tauxtva" <?php echo $disabled_tva; ?> value="<?php echo $wpof->of_tauxtva; ?>" /> %
        <?php echo "(".__("utilisé notamment pour les frais hors formation").")"; ?>
        </p>
        </td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php echo __("Monnaie"); ?></th>
        <td>
        <input type="text" name="wpof_monnaie" placeholder="<?php _e("Unité en lettres"); ?>" value="<?php echo $wpof->monnaie; ?>" />
        <input type="text" name="wpof_monnaie_symbole" placeholder="<?php _e("Symbole"); ?>" value="<?php echo $wpof->monnaie_symbole; ?>" /></td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php echo __("Tarif inter /heure/stagiaire"); ?></th>
        <td><input type="text" name="wpof_tarif_inter" value="<?php echo $wpof->tarif_inter; ?>" /></td>
    </tr>
    
    </table>
    <?php
}

function show_taches_options()
{
    global $wpof;
    $responsables = get_opaga_users('um_responsable');
    ?>
    <table class="opaga">
    <thead>
    <tr valign="top">
    <th><?php _e('Tâches'); ?> →<br />↓ <?php _e("Personnes"); ?></th>
    <th><?php _e('Fonction à afficher dans les documents ou les correspondances'); ?></th>
    <?php foreach(array_keys($wpof->taches_resp->term) as $tache) : ?>
        <th><?php echo $wpof->taches_resp->get_term($tache); ?></th>
        <?php
            if (!is_array($wpof->{"tache_".$tache}))
                $wpof->{"tache_".$tache} = array();
        ?>
    <?php endforeach; ?>
    </tr>
    </thead>
    
    <tbody>
    <?php foreach($responsables as $rid => $responsable) : ?>
        <tr valign="top">
        <th><?php echo $responsable->get_displayname(); ?></th>
        <td class="center"><input type="text" name="wpof_tache_fonction[<?php echo $rid; ?>]" value="<?php echo get_fonction_responsable($rid); ?>" /></td>
        <?php foreach(array_keys($wpof->taches_resp->term) as $tache) : ?>
            <td class="center" data-respid="<?php echo $rid; ?>"><input type="checkbox" name="wpof_tache_<?php echo $tache; ?>[<?php echo $rid; ?>]" <?php checked(in_array($rid, array_keys($wpof->{"tache_".$tache}))); ?> value="1" /></td>
        <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    
    <tr>
    <th><?php _e("Équipe pédagogique"); ?></th>
    <td></td>
    <?php foreach(array_keys($wpof->taches_resp->term) as $tache) : ?>
        <?php if ($wpof->taches_resp->term[$tache]->formateur_possible) : ?>
        <td class="center" data-respid="*"><input type="checkbox" name="wpof_tache_<?php echo $tache; ?>[0]" <?php checked(in_array(0, array_keys($wpof->{"tache_".$tache}))); ?> value="1" /></td>
        <?php else : ?>
        <td></td>
        <?php endif; ?>
    <?php endforeach; ?>
    </tr>
    </tbody>
    </table>
    <?php
}

function show_opaga_options()
{
    global $wpof;
    ?>
    <table class="form-table">
    <tr valign="top">
        <th scope="row"><?php echo __("Première année d'activité gérée avec ce logiciel"); ?> <?php echo get_icone_aide("option_annee1"); ?></th>
        <td><input type="text" name="wpof_annee1" value="<?php echo $wpof->annee1; ?>" /></td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php _e("Page de gestion"); ?> <?php echo get_icone_aide("option_gestion"); ?></th>
        <td>
        <div class="label">
        <label for="wpof_url_gestion"><?php _e("URL d'accès à la gestion"); ?></label>
        <?php echo home_url(); ?>/<input type="text" name="wpof_url_gestion" placeholder="<?php _e("URL d'accès à la gestion"); ?>" value="<?php echo $wpof->url_gestion; ?>" />
        </div>
        <div class="label">
        <label for="wpof_title_gestion"><?php _e("Titre de la page"); ?></label>
        <input type="text" name="wpof_title_gestion" placeholder="<?php _e("Titre de la page"); ?>" value="<?php echo $wpof->title_gestion; ?>" />
        </div>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Page personnelle des utilisateur⋅ices (profil, tableau de bord)"); ?> <?php echo get_icone_aide("option_user"); ?></th>
        <td>
        <div class="label">
        <label for="wpof_url_user"><?php _e("URL d'accès"); ?></label>
        <?php echo home_url(); ?>/<input type="text" name="wpof_url_user" placeholder="<?php _e("URL d'accès"); ?>" value="<?php echo $wpof->url_user; ?>" />
        </div>
        <div class="label">
        <label for="wpof_title_user"><?php _e("Titre de la page"); ?></label>
        <input type="text" name="wpof_title_user" placeholder="<?php _e("Titre de la page"); ?>" value="<?php echo $wpof->title_user; ?>" />
        </div>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Page privée pour les clients et les stagiaires"); ?> <?php echo get_icone_aide("option_acces"); ?></th>
        <td>
        <div class="label">
        <label for="wpof_url_acces"><?php _e("URL d'accès"); ?></label>
        <?php echo home_url(); ?>/<input type="text" name="wpof_url_acces" placeholder="<?php _e("URL d'accès"); ?>" value="<?php echo $wpof->url_acces; ?>" />
        </div>
        <div class="label">
        <label for="wpof_title_acces"><?php _e("Titre de la page"); ?></label>
        <input type="text" name="wpof_title_acces" placeholder="<?php _e("Titre de la page"); ?>" value="<?php echo $wpof->title_acces; ?>" />
        </div>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Page d'aide, recensant toutes les bulles d'aide en ligne"); ?> <?php echo get_icone_aide("option_aide"); ?></th>
        <td>
        <div class="label">
        <label for="wpof_url_aide"><?php _e("URL d'accès à cette page d'aide"); ?></label>
        <?php echo home_url(); ?>/<input type="text" name="wpof_url_aide" placeholder="<?php _e("URL d'accès l'aide"); ?>" value="<?php echo $wpof->url_aide; ?>" />
        </div>
        <div class="label">
        <label for="wpof_title_aide"><?php _e("Titre de la page"); ?></label>
        <input type="text" name="wpof_title_aide" placeholder="<?php _e("Titre de la page"); ?>" value="<?php echo $wpof->title_aide; ?>" />
        </div>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Page de gestion des exports de données"); ?> <?php echo get_icone_aide("option_export"); ?></th>
        <td>
        <div class="label">
        <label for="wpof_url_export"><?php _e("URL d'accès"); ?></label>
        <?php echo home_url(); ?>/<input type="text" name="wpof_url_export" placeholder="<?php _e("URL d'accès l'export"); ?>" value="<?php echo $wpof->url_export; ?>" />
        </div>
        <div class="label">
        <label for="wpof_title_export"><?php _e("Titre de la page"); ?></label>
        <input type="text" name="wpof_title_export" placeholder="<?php _e("Titre de la page"); ?>" value="<?php echo $wpof->title_export; ?>" />
        </div>
        </td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php _e("Champs additionnels pour des cas particuliers"); ?></th>
        <td>
        <?php
        $champs_additionnels = get_option("wpof_champs_additionnels");
        foreach ($wpof->champs_additionnels_liste as $k => $name) : ?>
            <label>
            <input type="checkbox" name="wpof_champs_additionnels[<?php echo $k; ?>]" value="1" <?php checked(1, (isset($champs_additionnels["$k"])) ? $champs_additionnels["$k"] : 0, true); ?> />
            <?php echo $name; ?>  <?php echo get_icone_aide("option_champ_add_$k"); ?></label>
        <?php endforeach; ?>
        </td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php _e("Délai maximum avant alerte sur mise à jour de la veille personnelle (en jours)"); ?> <?php echo get_icone_aide("wpof_delai_alerte_veille"); ?></th>
        <td><input type="number" name="wpof_delai_alerte_veille" min="0" value="<?php echo $wpof->delai_alerte_veille; ?>" /></td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php echo __("Durée de validité du jeton de connexion en heures (0 pour illimité)"); ?> <?php echo get_icone_aide("token_validity"); ?></th>
        <td><input type="number" name="wpof_token_validity" min="0" value="<?php echo $wpof->token_validity; ?>" /></td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php echo __("Tarif horaire plancher déclenchant l'alerte pour tarif trop bas"); ?> <?php echo get_icone_aide("wpof_tarif_heure_alerte"); ?></th>
        <td><input type="number" name="wpof_tarif_heure_alerte" min="0" max="200" value="<?php echo $wpof->tarif_heure_alerte; ?>" /> <?php echo $wpof->monnaie_symbole; ?></td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php echo __("Pourcentage par défaut pour calculer l'acompte"); ?> <?php echo get_icone_aide("wpof_acompte_pourcent"); ?></th>
        <td><input type="number" name="wpof_acompte_pourcent" min="0" max="100" value="<?php echo $wpof->acompte_pourcent; ?>" /> %</td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php _e("Nombre maximum de lignes de log chargées"); ?> <?php echo get_icone_aide("wpof_log_load_rows"); ?></th>
        <td><input type="number" name="wpof_log_load_rows" min="100" value="<?php echo $wpof->log_load_rows; ?>" /></td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php _e("Nombre de lignes sur une feuille d'émargement"); ?> <?php echo get_icone_aide("wpof_nb_ligne_emargement"); ?></th>
        <td><input type="number" name="wpof_nb_ligne_emargement" min="1" max="40" value="<?php echo $wpof->nb_ligne_emargement; ?>" /></td>
    </tr>

    <tr valign="top">
        <th scope="row"><?php _e("Taxonomies pour les formations et sessions"); ?> <?php echo get_icone_aide("wpof_taxonomies"); ?></th>
        <td>
            <input type="checkbox" name="wpof_use_category" value="1" <?php checked(1, $wpof->use_category, true); ?> /> <?php _e("Catégories (classification en arborescence)"); ?><br />
            <input type="checkbox" name="wpof_use_tag" value="1" <?php checked(1, $wpof->use_tag, true); ?> /> <?php _e("Étiquettes (classification transversale)"); ?>
        </td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e('Page de présentation du bilan de compétences'); ?></th>
        <td>
        <?php
            if (empty($wpof->bc_page))
                $wpof->bc_page = -1;
            $liste = select_post_by_type("page", "wpof_bc_page", $wpof->bc_page, __('Veuillez choisir une page'));
            if ($liste == NULL)
                echo '<a href="/wp-admin/edit.php?post_type=page">'.__("Aucune page définie dans votre WordPress !").'</a>';
            else
                echo $liste;
        ?>
        </td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e("Nombre maximum de révisions conservées pour les contenus OPAGA"); ?> <?php echo get_icone_aide("wpof_content_revisions_max"); ?></th>
        <td><input type="number" name="wpof_content_revisions_max" min="5" value="<?php echo $wpof->content_revisions_max; ?>" /></td>
    </tr>
    
    </table>
    <?php
}

function show_predefini_options()
{
    global $wpof, $tinymce_wpof_settings;
    ?>
    <h2><?php _e("Textes prédéfinis pour les formations"); ?></h2>
    <p><?php _e("Pour chaque paramètre choisissez si vous souhaitez l'imposer (forcer) à toutes les formations, le proposer ou ne rien faire (inactif)"); ?></p>
    <p><?php echo get_icone_aide("predefini_options", __("Plus d’infos")); ?></p>
    
    <table class="opaga opaga2 predefini">
    <?php
        foreach (array_keys($wpof->desc_formation->term) as $t)
        {
            $text = get_option("wpof_formation_{$t}_text");
            $mode = get_option("wpof_formation_{$t}_mode");
            if (empty($mode))
                $mode = 'inactif';
            
            $editor_display = ($mode == 'inactif') ? "style='display: none;'" : "";
            ?>
            <tr>
            <td><?php echo select_by_list($wpof->desc_formation_mode, "wpof_formation_{$t}_mode", $mode); ?> <strong><?php echo $wpof->desc_formation->get_term($t); ?></strong>
            </td>
            <td class='editor' <?php echo $editor_display; ?>><?php wp_editor($text, "wpof_formation_{$t}_text", $tinymce_wpof_settings); ?></td>
            </tr>
            <?php
        }
    ?>
    </table>
    <?php
}

function show_plugins_options()
{
    global $wpof;
    ?>
    <h2><?php echo __("Choisir une extension Opaga à activer"); ?></h2>
    <div>
        <?php
            $selected_plugin = get_option('wpof_plugin');
            echo select_by_list($wpof->plugins, "wpof_plugin", $selected_plugin);

            foreach ($wpof->plugins as $name => $value) {
                ?>
                <div class="wpof-plugin-options" id="<?php echo $name ?>-options" <?php if ($selected_plugin != $name) { echo 'style="display:none;"'; } ?> >
                <?php $name::loadAdmin();?>
                </div>
                <?php
            }
        ?>

    </div>
    <?php
}

function show_pdf_options()
{
    global $wpof, $tinymce_wpof_settings;
    ?>
    <fieldset>
    <legend><?php _e('En-tête et pied de page'); ?></legend>
    <?php echo get_icone_aide('modeles_redaction', "Rédiger les modèles de document"); ?>
        <div>
            <h3><?php _e("En-tête"); ?></h3>
            <div class="flex-container height-max-300">
            <?php wp_editor($wpof->pdf_header, "wpof_pdf_header", $tinymce_wpof_settings); ?>
            <?php echo get_keywords_list_to_editor('wpof_pdf_header') ?>
            </div>
        <div>
        <div>
            <h3><?php _e("Pied de page"); ?></h3>
            <div class="flex-container height-max-300">
            <?php wp_editor($wpof->pdf_footer, "wpof_pdf_footer", $tinymce_wpof_settings); ?>
            <?php echo get_keywords_list_to_editor('wpof_pdf_footer') ?>
            </div>
        </div>
    </fieldset>
        
    <div class="flex-container">
        <fieldset>
        <legend><?php _e('Dimensions'); ?></legend>
        <p><?php _e("Note : les dimensions sont en millimètres."); ?></p>
        <div class="margin label-fixe">
            <div>
                <h3><?php echo __("Marges"); ?></h3>
                <p><label for="pdf-marge-haut"><?php _e("haut"); ?></label><input type="number" id="pdf-marge-haut" name="wpof_pdf_marge_haut" value="<?php echo $wpof->pdf_marge_haut; ?>" /></p>
                <p><label for="pdf-marge-bas"><?php _e("bas"); ?></label><input type="number" id="pdf-marge-bas" name="wpof_pdf_marge_bas" value="<?php echo $wpof->pdf_marge_bas; ?>" /></p>
                <p><label for="pdf-marge-gauche"><?php _e("gauche"); ?></label><input type="number" id="pdf-marge-gauche" name="wpof_pdf_marge_gauche" value="<?php echo $wpof->pdf_marge_gauche; ?>" /></p>
                <p><label for="pdf-marge-droite"><?php _e("droite"); ?></label><input type="number" id="pdf-marge-droite" name="wpof_pdf_marge_droite" value="<?php echo $wpof->pdf_marge_droite; ?>" /></p>
            </div>
            <div>
                <h3><?php echo __("Hauteurs"); ?></h3>
                <p><label for="pdf-hauteur-header"><?php _e("En-tête"); ?></label><input type="number" id="pdf-hauteur-header" name="wpof_pdf_hauteur_header" value="<?php echo $wpof->pdf_hauteur_header; ?>" /></p>
                <p><label for="pdf-hauteur-footer"><?php _e("Pied de page"); ?></label><input type="number" id="pdf-hauteur-footer" name="wpof_pdf_hauteur_footer" value="<?php echo $wpof->pdf_hauteur_footer; ?>" /></p>
            </div>
        </div>
        <p><?php
            $body_height = 297 - ($wpof->pdf_marge_haut + $wpof->pdf_marge_bas + $wpof->pdf_hauteur_header + $wpof->pdf_hauteur_footer);
            echo __("Hauteur du corps : ").$body_height."mm (".__("Hauteur du papier - (marge haute + marge basse + hauteur en-tête + hauteur pied)").")";
            ?>
        </p>
        </fieldset>
        
        <fieldset>
        <legend><?php _e("Textes"); ?></legend>
        <div>
            <?php 
                $font_list = array_keys((array) json_decode(file_get_contents(dompdf_path . '/lib/fonts/installed-fonts.dist.json')));
                $font_list = array_combine($font_list, $font_list);
            ?>
            <label for="pdf-titre-font" class="top"><?php _e("Police des titres"); ?></label>
            <?php echo select_by_list($font_list, 'wpof_pdf_titre_font', $wpof->pdf_titre_font); ?>
            
            <label for="pdf-texte-font" class="top"><?php _e("Police du texte courant"); ?></label>
            <?php echo select_by_list($font_list, 'wpof_pdf_texte_font', $wpof->pdf_texte_font); ?>
            
            <label for="pdf-titre-color" class="top"><?php _e("Couleur des titres de document"); ?></label>
            <input type="text" class="color-picker" id='pdf-titre-color' name="wpof_pdf_couleur_titre_doc" value="<?php echo $wpof->pdf_couleur_titre_doc ;?>" />
            
            <label for="pdf-titre-color" class="top"><?php _e("Couleur des autres titres"); ?></label>
            <input type="text" class="color-picker" id='pdf-titre-color' name="wpof_pdf_couleur_titre_autres" value="<?php echo $wpof->pdf_couleur_titre_autres ;?>" />
        </div>
        </fieldset>
    </div>

    <fieldset>
    <legend><?php _e("Feuille de style personnalisée (CSS)"); ?></legend>
        <p><?php _e("La feuille de style s'applique après les réglages ci-dessus. En cas de conflit, la feuille de style ci-dessous s'applique."); ?></p>
        <textarea id="pdf-css" name="wpof_pdf_css" cols="100" rows="20"><?php echo $wpof->pdf_css; ?></textarea>
            <script>
            jQuery(document).ready(function($)
            {
                wp.codeEditor.initialize($('#pdf-css'), cm_settings);
            });
            </script>
    </fieldset>
    <?php 
}

function show_manage_options()
{
    global $menu, $wpof;
    ?>
    <table class="form-table">
    <tr valign="top">
        <th scope="row"><?php echo __("Menus cachés"); ?>
        <?php
        $hiddenable_menu = array
        (
            "2" => __("Dashboard"),
            "5" => __("Posts"),
            "10" => __("Media"),
            "20" => __("Pages"),
            "25" => __("Comments"),
            "42.78578" => "Ultimate Member",
            "75" => __("Tools"),
        );
        ?>
        </th>
        <td>
        <?php
        $hidden_menus = get_option("wpof_hidden_menus");
        foreach ($hiddenable_menu as $hm => $menu_name)
        {
        ?>
            <p><label>
            <input type="checkbox" name="wpof_hidden_menus[<?php echo $hm; ?>]" value="1" <?php checked(1, (isset($hidden_menus["$hm"])) ? $hidden_menus["$hm"] : 0, true); ?> />
            <?php echo $menu_name; ?></label></p>
        <?php
        }
        ?>
        </td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e("Mode debug"); ?></th>
        <td>
        <input type="checkbox" name="wpof_debug" value="1" <?php checked(get_option("wpof_debug"), 1); ?> />
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Accepter un certificat SSL auto-signé pour HTTPS (sinon blocage dans DomPDF)"); ?></th>
        <td>
        <input type="checkbox" name="wpof_dompdf_https_noverif" value="1" <?php checked(get_option("wpof_dompdf_https_noverif"), 1); ?> />
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Utiliser le thème opaga pour les pages de gestion :"); ?><br /><?php echo implode(", ", $wpof->pages_responsable); ?></th>
        <td>
        <input type="checkbox" name="wpof_theme" value="1" <?php checked(get_option("wpof_theme"), 1); ?> />
        </td>
    </tr>
    <?php
    $nav_menu_locations = get_nav_menu_locations();
    $main_menu = get_option("wpof_main_menu");
    if (empty($main_menu))
    {
        $main_menu = array_key_first($nav_menu_locations);
        update_option("wpof_main_menu", $main_menu);
    }
    ?>
    <tr valign="top">
        <th scope="row"><?php _e("Menu principal auquel attacher le menu OPAGA"); ?></th>
        <td>
        <select name="wpof_main_menu">
        <?php $selected = $main_menu; ?>
        <?php foreach(array_keys($nav_menu_locations) as $location) : ?>
        <option value="<?php echo $location; ?>" <?php selected($selected, $location, true); ?>><?php echo wp_get_nav_menu_name($location); ?></option>
        <?php endforeach; ?>
        </select>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Menu WordPress pour les pages de gestion"); ?></th>
        <td>
        <select name="wpof_wp_menu">
        <?php $selected = get_option("wpof_wp_menu"); ?>
        <option value="-1"><?php _e("Aucun"); ?></option>
        <?php foreach(array_keys($nav_menu_locations) as $location) : ?>
        <option value="<?php echo $location; ?>" <?php selected($selected, $location, true); ?>><?php echo wp_get_nav_menu_name($location); ?></option>
        <?php endforeach; ?>
        </select>
        </td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e("Afficher le menu OPAGA même lorsque l'utilisateur n'est pas connecté (avec seulement le lien de connexion)"); ?></th>
        <td>
        <input type="checkbox" name="wpof_allways_menu" value="1" <?php checked(get_option("wpof_allways_menu"), 1); ?> />
        </td>
    </tr>
    <tr valign="top">
        <th scope="row"><?php _e("Poids maximum en téléversement pour les formateur⋅trices"); ?></th>
        <td>
        <?php
            $max_upload = min((int)ini_get('post_max_size'), (int)ini_get('upload_max_filesize'));
            $min_upload = min(5, $max_upload);
        ?>
        <input type="number" name="wpof_max_upload_size" min="<?php echo $min_upload; ?>" max="<?php echo $max_upload; ?>" value="<?php echo get_option("wpof_max_upload_size"); ?>" /> Mo (maximum <?php echo $max_upload; ?> Mo)
        </td>
    </tr>
    </table>
    <?php if (function_exists("shell_exec")): ?>
    <pre><?php echo shell_exec("cd ".wpof_path."; git log --pretty=medium -n1 HEAD"); ?></pre>
    <?php endif; ?>
    <?php if (strstr(home_url(), "make-formations.fr")) : ?>
    <p>
    <span class="bouton maj-wpof" data-log="log" data-action="export_aide" data-param="json"><?php _e("Export de l'aide en JSON"); ?></span>
    <span class="bouton maj-wpof" data-log="log" data-action="reset_aide" data-param="json"><?php _e("Réinitialiser tous les textes d'aide"); ?></span>
    </p>
    <p>
    <span class="bouton maj-wpof" data-log="log" data-action="export_db_struct"><?php _e("Export de la structure SQL"); ?></span>
    <span class="bouton maj-wpof" data-log="log" data-action="export_sql" data-param="roles"><?php _e("Export des rôles en SQL"); ?></span>
    <span class="bouton inter_opaga_export" data-log="log" data-type="options" data-format="json"><?php _e("Export des options d'OPAGA en JSON"); ?></span>
    <span class="bouton inter_opaga_export" data-type="modeles" data-format="json">Exporter les modèles de document en JSON</span>
    </p>
    <?php endif; ?>
    <p>
    <?php if (strstr(home_url(), "make-formations.fr")) : ?>
    <?php endif; ?>
    <span class="bouton maj-wpof" data-log="log" data-action="import_json" data-param="modele"><?php _e("Import des modèles de document en JSON"); ?></span>
    </p>
    <?php if (debug) : ?>
    <p><?php _e("Attention, n'utilisez les fonctions ci-dessous que sur une installation existante depuis plusieurs mois"); ?></p>
    <p>
    <span class="bouton maj-wpof" data-log="log" data-action="init_sql_opaga_tables"><?php _e("Import des tables SQL OPAGA"); ?></span>
    <span class="bouton maj-wpof" data-log="log" data-action="reinit_plugin"><?php _e("Relancer la procédure d'initialisation"); ?></span>
    <span class="bouton maj-wpof" data-log="log" data-action="init_sql_opaga_tables"><?php _e("Reimport des tables SQL OPAGA"); ?></span>
    <span class="bouton maj-wpof" data-log="log" data-action="import_sql" data-param="roles"><?php _e("Reimport des rôles en SQL"); ?></span>
    <span class="bouton maj-wpof" data-log="log" data-action="import_json" data-param="options"><?php _e("Reimport des options d'OPAGA en JSON"); ?></span>
    <span class="bouton maj-wpof" data-log="log" data-action="maj_completion"><?php _e("Complétion formations et sessions"); ?></span>
    <span class="bouton maj-wpof" data-log="log" data-action="maj_nature_formation"><?php _e("Nature formation par défaut"); ?></span>
    </p>
    <?php endif; ?>
    <div id="log">
    </div>
    <?php
}
