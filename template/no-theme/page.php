<?php
/**
 * Template simplifiée pour les pages spéciales d'OPAGA
 */
remove_filter( 'the_content', 'wpautop' );
global $wpof;
?>

<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>><!-- OPAGA -->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class("opaga"); ?>>
        <?php wp_body_open(); ?>
        <header id="site-header" role="banner">
            <?php if (!isset($wpof->wp_menu) || $wpof->wp_menu == -1) : ?>
            <span class="opaga-board-link icone-bouton"><a href="<?php echo home_url().'/'.$wpof->url_user.'/'; ?>"><?php _e("Tableau de bord"); ?></a></span>
            <?php endif; ?>
            <div id="site-title">
                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_html(get_bloginfo('name')); ?>" rel="home"><?php echo esc_html(get_bloginfo('name')); ?></a>
            </div>
            <div id="site-description"><?php bloginfo( 'description' ); ?>
            </div>
        <?php if (isset($wpof->wp_menu) && has_nav_menu($wpof->wp_menu)) : ?>
            <nav id="site-navigation" class="opaga-main-menu" aria-label="<?php esc_attr_e( 'Top Menu', 'opaga' ); ?>">
                <?php
                wp_nav_menu
                (
                    array
                    (
                        'theme_location' => $wpof->wp_menu,
                        'menu_class'     => '',
                        'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    )
                );
                ?>
               </nav><!-- #site-navigation -->
        <?php endif; ?>

        </header>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    
    if (have_posts())
        while (have_posts())
        {
            the_post();
            ?>
            <div class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
            </div>
            
            <div class="entry-content">
            <?php the_content(); ?>
            </div>
            <?php
        }
    ?>
    </article>

    <footer>
    <?php wp_footer(); ?>
    </footer>
</body>
</html>
