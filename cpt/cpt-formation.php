<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-quiz.php");
require_once(wpof_path . "/class/class-aide.php");

/**
 * Add post type formation
 */
function register_cpt_formation() {

    /**
     * Post Type: Formations.
     */

    $labels = array(
        "name" => __( "Formations", "generic" ),
        "singular_name" => __( "Formation", "generic" ),
        "all_items" => __( "Toutes les formations", "generic" ),
        "add_new" => __( "Ajouter une nouvelle", "generic" ),
        "add_new_item" => __("Ajouter une nouvelle formation"),
        "view_item" => __("Voir la formation"),
        "edit_item" => __("Modifier la formation"),
        "update_item" => __("Mettre à jour la formation"),
    );

    $args = array(
        "label" => __( "Formations", "generic" ),
        "labels" => $labels,
        "description" => "Fiche-programme de formation",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => false,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => 'formations',
        "show_in_menu" => false,
        "show_in_nav_menus" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "formation", "with_front" => true ),
        "query_var" => true,
        "menu_position" => 4,
        "menu_icon" => "dashicons-welcome-learn-more",
        "supports" => array( "title", "thumbnail" ),
        "taxonomies" => array( "category", "post_tag" ),
    );

    register_post_type( "formation", $args );
}
add_action('init', 'register_cpt_formation', 1);
