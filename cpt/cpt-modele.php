<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-wpof.php");
require_once(wpof_path . "/class/class-document.php");
require_once(wpof_path . "/wpof-config.php");
 
function modele_add_editor_styles()
{
    if (isset($_GET['post_type']) && $_GET['post_type'] == "modele")
        add_editor_style(wpof_path . '/css/wp-editor.css');
}
add_action('admin_init', 'modele_add_editor_styles');

/**
 * Add post type modele
 */
function register_cpt_modele()
{
    /**
     * Post Type: Modèles.
     */

    $labels = array(
        "name" => __("Modèles"),
        "singular_name" => __("Modèle"),
        "all_items" => __("Modèles"),
        "add_new" => __("Ajouter un nouveau"),
        "add_new_item" => __("Ajouter un nouveau modèle"),
        "view_item" => __("Voir le modèle"),
        "edit_item" => __("Modifier le modèle"),
        "update_item" => __("Mettre à jour le modèle"),
    );

    $args = array(
        "label" => __("Modèles"),
        "labels" => $labels,
        "description" => "Modèle de document",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => false,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => 'admin.php?page=wpof',
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "modele", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => "dashicons-media-code",
        "supports" => array("title", "revisions"),
        "taxonomies" => array(),
    );

    register_post_type( "modele", $args );
}

add_action('init', 'register_cpt_modele', 1);

add_filter('wp_modele_revisions_to_keep', 'filter_modele_revisions_max', 10, 2);
function filter_modele_revisions_max($num, $post)
{
    global $wpof;
    return $wpof->content_revisions_max;
}


function move_modele_data()
{
    # Get the globals:
    global $post, $wp_meta_boxes;

    # Output the "advanced" meta boxes:
    do_meta_boxes(get_current_screen(), 'prev', $post );

    # Remove the initial "advanced" meta boxes:
    unset($wp_meta_boxes['post']['test']);
}
//add_action('edit_form_after_title', 'move_modele_data');

// add meta box
add_action('add_meta_boxes','initialisation_modele_metaboxes');
function initialisation_modele_metaboxes()
{
//    add_meta_box('modele-keywords', __("Mots-clés utilisables"), 'modele_keywords_meta_box', 'modele', 'side');
    add_meta_box('modele-data', __("Conditions d'utilisation du document"), 'modele_data_meta_box', 'modele', 'normal', 'high');
    add_meta_box('modele-content', __('Contenu du modèle'), 'modele_content_meta_box', 'modele', 'normal', 'high');
    add_meta_box('modele-aide', __('Aide : comment utiliser ce document'), 'modele_aide_meta_box', 'modele', 'normal');
}

function modele_data_meta_box($post)
{
    global $wpof;
    global $tinymce_wpof_settings;
    
    $modele = new Modele($post->ID);
    $contexte = $modele->contexte;
    $signature = $modele->signature;
    $orientation = $modele->orientation;
    
    ?>
    <div class="flexrow">
        <div>
        <h3><?php _e("Titre du document tel qu'il apparaît dans le PDF"); ?></h3>
        <input type="text" size="80" name="titre" value="<?php echo $modele->titre; ?>" />
        <?php echo get_icone_aide("modele_titre"); ?>
        </div>
        <div>
        
        </div>
    </div>
    <h3><?php _e("Contexte des documents produits à partir de ce modèle"); ?></h3>
    <div class="flex-container">
        <fieldset><legend><?php _e("pour les contrats"); ?></legend>
        <?php echo get_icone_aide("modele_contrat"); ?>
        <label><input type="checkbox" name="contrat[]" value="morale" <?php checked(($contexte & $wpof->doc_context->morale) > 0); ?>/><?php _e("en direct, personne morale"); ?></label>
        <label><input type="checkbox" name="contrat[]" value="physique" <?php checked(($contexte & $wpof->doc_context->physique) > 0); ?>/><?php _e("en direct, personne physique"); ?></label>
        <label><input type="checkbox" name="contrat[]" value="sous_traitance" <?php checked(($contexte & $wpof->doc_context->sous_traitance) > 0); ?>/><?php _e("gérés par un autre OF que le votre (vous êtes alors sous-traitant)"); ?></label>
        <hr />
        <label><input type="checkbox" name="is_contrat" value="1" <?php checked($modele->is_contrat); ?>/><?php _e("Ce document est le contrat pour le client coché ci-dessus"); ?></label>
        </fieldset>
        <fieldset><legend><?php _e("pour la ou les entités"); ?></legend>
        <?php echo get_icone_aide("modele_entite"); ?>
        <label><input type="checkbox" name="entite[]" value="session" <?php checked(($contexte & $wpof->doc_context->session) > 0); ?>/><?php _e("session de formation"); ?></label>
        <label><input type="checkbox" name="entite[]" value="client" <?php checked(($contexte & $wpof->doc_context->client) > 0); ?>/><?php _e("client"); ?></label>
        <label><input type="checkbox" name="entite[]" value="stagiaire" <?php checked(($contexte & $wpof->doc_context->stagiaire) > 0); ?>/><?php _e("stagiaire"); ?></label>
        <label><input type="checkbox" name="entite[]" value="formateur" <?php checked(($contexte & $wpof->doc_context->formateur) > 0); ?>/><?php _e("formateur⋅trice"); ?></label>
        <label><input type="checkbox" name="entite[]" value="formation" <?php checked(($contexte & $wpof->doc_context->formation) > 0); ?>/><?php _e("fiche de formation"); ?></label>
        </fieldset>
        <fieldset><legend><?php _e("devant être signés par"); ?></legend>
        <?php echo get_icone_aide("modele_signature"); ?>
        <label><input type="checkbox" name="signature[]" value="responsable" <?php checked(($signature & $wpof->doc_signature->responsable) > 0); ?>/><?php _e("responsable de formation"); ?></label>
        <label><input type="checkbox" name="signature[]" value="client" <?php checked(($signature & $wpof->doc_signature->client) > 0); ?>/><?php _e("client"); ?></label>
        <label><input type="checkbox" name="signature[]" value="stagiaire" <?php checked(($signature & $wpof->doc_signature->stagiaire) > 0); ?>/><?php _e("stagiaire"); ?></label>
        <label><input type="checkbox" name="signature[]" value="formateur" <?php checked(($signature & $wpof->doc_signature->formateur) > 0); ?>/><?php _e("formateur"); ?></label>
        </fieldset>
        <fieldset><legend><?php _e("paramètres"); ?></legend>
        <label for="orientation"><?php _e("Orientation"); ?></label>
        <?php echo select_by_list(array("portrait" => __("Portrait"), "landscape" => __("Paysage")), "orientation", $orientation, "id='orientation'"); ?>
        </fieldset>
    </div>
    <?php modele_custom_keywords_list($post);?>
    <?php
}

function modele_aide_meta_box($post)
{
    global $tinymce_wpof_settings;
    ?>
    <p><?php _e("Rédigez ici une aide à l'utilisation de ce document : dans quel contexte, pour quoi, pour qui, etc."); ?></p>
    <?php
    if (isset($post->post_name))
    {
        $aide_slug = "doc_".$post->post_name;
        $aide = new Aide($aide_slug);
        $texte = $aide->texte;
    }
    else
    {
        $aide_slug = "aide_new";
        $texte = "";
    }
    wp_editor($texte, "aide_texte", array_merge($tinymce_wpof_settings));
}

function modele_content_meta_box($post)
{
    global $tinymce_wpof_settings;
    
    echo get_icone_aide('modeles_redaction', "Rédiger les modèles de document");
    ?>
    <div class="flex-container height-max-700">
    <?php wp_editor($post->post_content, 'content', array_merge($tinymce_wpof_settings, array('editor_height' => 550))); ?>
    <?php echo get_keywords_list_to_editor('content'); ?>
    </div>
    <?php
}

function modele_custom_keywords_list($post)
{
    global $wpof;
    $modele = new Modele($post->ID);
    
    if (!empty($modele->ckw)) :
    ?>
    
    <fieldset id="ckw">
        <legend><?php _e("Mots-clés personnalisés"); ?></legend>
        <table class="opaga">
        <thead>
        <tr>
        <th class="thin"><?php _e("Mot-clé"); echo get_icone_aide("custom_keyword_mot_cle"); ?></th>
        <th><?php _e("Description"); echo get_icone_aide("custom_keyword_description"); ?></th>
        <th class="thin"><?php _e("Type"); echo get_icone_aide("custom_keyword_type"); ?></th>
        <th class="thin"><?php _e("Obligatoire"); echo get_icone_aide("custom_keyword_needed"); ?></th>
        <th><?php _e("Valeur par défaut facultative"); echo get_icone_aide("custom_keyword_defaut"); ?></th>
        <th class="thin"><?php _e("Supprimer"); echo get_icone_aide("custom_keyword_remove"); ?></th>
        </tr>
        </thead>
        <?php foreach($modele->ckw as $key => $val) : ?>
            <tr id="<?php echo $key; ?>">
            <td>document:<?php echo $key; ?> <input type="hidden" name="ckw[]" value="<?php echo $key; ?>" /></td>
            <td class="desc"><input type="text" class="long-text" name="<?php echo $key."_desc"; ?>" value="<?php echo $val->desc; ?>" /></td>
            <td class="type"><?php echo select_by_list($wpof->custom_keyword->types, $key."_type", $val->type, 'class="ckw_switch_type" data-key="'.$key.'"'); ?></td>
            <td class="needed center"><input type="checkbox" name="<?php echo $key."_needed"; ?>" value="1" <?php checked(1, $val->needed); ?>" /></td>
            <td class="defaut"> <?php modele_custom_keywords_defaut($key, $val->type, $val->defaut); ?> </td>
            <td class="remove center"> <p class="ckw delete icone-bouton"><?php the_wpof_fa_del(); ?></p> </td>
            </tr>
        <?php endforeach; ?>
        </table>
    </fieldset>
    
    <?php
    endif;
}

add_action('wp_ajax_modele_custom_keywords_defaut', 'modele_custom_keywords_defaut');
function modele_custom_keywords_defaut($key = null, $type = null, $defaut = null)
{
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        $key = $_POST['key'];
        $type = $_POST['type'];
        $defaut = $_POST['defaut'];
    }
    switch($type)
    {
        case "text":
            ?>
            <span class="dynamic-dialog bouton float right" data-title="<?php _e("Modifier le texte par défaut"); ?>" data-function="simple_editor_dialog" data-eventfunc="update_keyword_default_text" data-key="<?php echo $key; ?>_defaut" data-contenu="<?php echo $defaut; ?>" title="<?php _e("Modifier"); ?>"><?php the_wpof_fa_edit(); ?></span>
            <p class="previsu"><?php echo string_cut(strip_tags($defaut), 150)." …"; ?></p>
            <input type="hidden" name="<?php echo $key; ?>_defaut" value="<?php echo $defaut; ?>" />
            <?php
            break;
        case "number":
            ?>
            <input type="number" step="0.001" class="long-text" value="<?php echo $defaut; ?>" name="<?php echo $key; ?>_defaut" />
            <?php
            break;
        case "bool":
        default:
            ?>
            <input type="radio" name="<?php echo $key; ?>_defaut" id="oui" value="1" <?php checked(1, $defaut, true); ?> /><label class="inline" for="oui">Oui</label> | 
            <input type="radio" name="<?php echo $key; ?>_defaut" id="non" value="0" <?php checked(0, $defaut, true); ?> /><label class="inline" for="non">Non</label> | 
            <input type="radio" name="<?php echo $key; ?>_defaut" id="aucune" value="" <?php checked("", $defaut, true); ?> /><label class="inline" for="aucune">Aucune</label>
            <?php
            break;
    }
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
        die();
}

// save meta box with update
add_action('save_post','save_modele_metaboxes');
function save_modele_metaboxes($post_ID)
{
    global $wpof;
    
    if (get_post_type($post_ID) != "modele") return;
    if (empty($_POST['post_title'])) return;
    
    if (!empty($_POST['titre']))
        update_post_meta($post_ID, "titre", $_POST['titre']);
    else
        update_post_meta($post_ID, "titre", $_POST['post_title']);
    
    $contexte = 0;
    if (isset($_POST['contrat']))
        foreach($_POST['contrat'] as $c)
            $contexte |= $wpof->doc_context->$c;
            
    if (isset($_POST['entite']))
        foreach($_POST['entite'] as $e)
            $contexte |= $wpof->doc_context->$e;
    
    $signature = 0;
    if (isset($_POST['signature']))
        foreach($_POST['signature'] as $s)
            $signature |= $wpof->doc_signature->$s;
    
    if (isset($_POST['ckw']))
    {
        foreach($_POST['ckw'] as $key)
        {
            $needed = (isset($_POST[$key.'_needed'])) ? $_POST[$key.'_needed'] : 0;
            $valeurs = array('key' => $key, 'desc' => $_POST[$key.'_desc'], 'type' => $_POST[$key.'_type'], 'needed' => $needed, 'defaut' => preg_replace("/\r|\n/", "", $_POST[$key.'_defaut']));
            update_post_meta($post_ID, 'ckw_'.$key, json_encode($valeurs, JSON_UNESCAPED_UNICODE));
        }
    }
    
    update_post_meta($post_ID, "contexte", $contexte);
    update_post_meta($post_ID, "signature", $signature);
    update_post_meta($post_ID, "is_contrat", isset($_POST['is_contrat']));

    update_post_meta($post_ID, "orientation", $_POST['orientation']);
    
    $slug = "";
    if (isset($_POST['post_name']))
        $slug = str_replace("-", "_", sanitize_title($_POST['post_name']));
    elseif (isset($_POST['post_title']))
        $slug = str_replace("-", "_", sanitize_title($_POST['post_title']));
    else
        $slug = "inconnu";
    
    if (isset($_POST['aide_texte']))
    {
        $aide = new Aide("doc_".$slug);
        $aide->update($_POST['post_title'], stripslashes($_POST['aide_texte']));
    }
    
    $content = stripslashes($_POST['content']);
    $content = check_custom_keywords($content);
    $content = clean_modele($content);
    
    // mise à jour du slug pour remplacer les - par des _
    if (!wp_is_post_revision($post_ID))
    {
        // unhook this function so it doesn't loop infinitely
        remove_action('save_post', 'save_modele_metaboxes');
     
        $args = array
        (
            'ID' => $post_ID,
            'post_name' => $slug,
            'post_content' => $content,
        );
        // update the post, which calls save_post again
        wp_update_post( $args );
 
        // re-hook this function
        add_action('save_post', 'save_modele_metaboxes');
    }
}
