jQuery(document).ready(function($)
{

    $("#lieu-existant select").on("change", function()
    {
        titre = $("#title").val();
        var parties = titre.split(" - ");
        
        if ($(this).val() == -1)
        {
            $("#nouveau-lieu").show();
            $("#add-nouveau-lieu").show();
            $("#lieu-data input").removeAttr("disabled");
            $("#lieu-data textarea").removeAttr("readonly");
            $("#adresse").val("");
            $("#code_postal").val("");
            $("#ville").val("");
            $("#localisation").val("");
            $("#secu_erp").val("");
            $("#erp-nom-image").attr("href", "#");
            $("#erp-nom-image").text("");
            $("#erp-add-image").removeClass("inactif");
            parties[2] = $("#ville").val();
            $("#title").val(parties.join(" - "));
        }
        else
        {
            $("#nouveau-lieu").hide();
            $("#add-nouveau-lieu").hide();
            $("#lieu-data input").attr("disabled", "disabled");
            $("#lieu-data textarea").attr("readonly", "readonly");
            
            jQuery.post
            (
                ajaxurl,
                {
                    'action': 'get_lieu',
                    'lieu_id': $(this).val(),
                },
                function(response)
                {
                    lieu = JSON.parse(response);
                    $("#adresse").val(lieu.adresse);
                    $("#code_postal").val(lieu.code_postal);
                    $("#ville").val(lieu.ville);
                    $("#localisation").val(lieu.localisation);
                    $("#secu_erp").val(lieu.secu_erp);
                    $("#erp-nom-image").attr("href", lieu.secu_erp_link);
                    $("#erp-nom-image").text(lieu.secu_erp_name);
                    $("#erp-add-image").addClass("inactif");
                    parties[2] = lieu.ville;
                    $("#title").val(parties.join(" - "));
                },
            );
            
        }
    });
    
    $("#ville").blur(function()
    {
        titre = $("#title").val();
        var parties = titre.split(" - ");
        parties[2] = $("#ville").val();
        $("#title").val(parties.join(" - "));
    });
    /*
    $("#erp-add-image").click(function(e)
    {
        e.preventDefault();
        
        if (!$(this).hasClass("inactif"))
        {
            var uploader = wp.media
            ({
                title: "Envoyer un fichier",
                button: { text: "Téléverser" },
                multiple: false,
            })
            .on('select', function()
            {
                var selection = uploader.state().get('selection');
                var attachment = selection.first().toJSON();
                console.log(attachment);
                $("#erp-nom-image").attr("href", attachment.link);
                $("#erp-nom-image").text(attachment.title);
                $("#secu_erp").val(attachment.id);
            })
            .open();
        }
    });
 */
    
    $("#add-nouveau-lieu").click(function(e)
    {
        e.preventDefault();
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'save_nouveau_lieu',
                'nouveau-lieu-nom': $("#nouveau-lieu input").val(),
                'adresse': $("#adresse").val(),
                'code_postal': $("#code_postal").val(),
                'ville': $("#ville").val(),
                'localisation': $("#localisation").val(),
                'secu_erp': $("#secu_erp").val(),
            },
            function(response)
            {
                lieu = JSON.parse(response);
                $("#lieu-existant select").append(new Option(lieu.nom, lieu.id, true, true));
                $("#lieu-data input").attr("disabled", "disabled");
                $("#lieu-data textarea").attr("readonly", "readonly");
                $("#nouveau-lieu").hide();
                $("#add-nouveau-lieu").hide();
                
                // réceptionner l'id, le rajouter dans la liste, le sélectionner et griser tous les champs
            }
        );
    });
    
    // Ajoute le contenu du champ depuis la fiche formation
    $("button.add-content-formation").click(function(e)
    {
        e.preventDefault();
        editor_id = $(this).attr("data-content");
        formation_id = $(this).attr("data-formationid");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'add_content_formation',
                'formation_id': formation_id,
                'content': editor_id,
            },
            function(response)
            {
                if ($('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id))
                {
                    editor = tinyMCE.get(editor_id);
                    content = editor.getContent() + response;
                    editor.setContent(content);
                }
                else
                {
                    content = $('#'+textarea_id).val() + response;
                    $('#'+textarea_id).val(content);
                }
            }
        );
    });
    
    $("button.new-line").click(function(e)
    {
        e.preventDefault();
        editor_id = $(this).attr("data-content");
        
        if ($('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id))
        {
            tinyMCE.get(editor_id).setContent(tinyMCE.get(editor_id).getContent() + "<div class='flex'><p>1</p><p>2</p></div>");
            //tinyMCE.get(editor_id).setContent(tinyMCE.get(editor_id).getContent() + "<table><tr><td>1</td><td>2</td></tr></table>");
        }
        else
        {
            $('#'+textarea_id).val($('#'+textarea_id).val() + "<div class='flex'></div>");
        }
    });
    
    // Ajoute le contenu du champ depuis la proposition faite par l'organisme de formation
    $("button.add-content-of").click(function(e)
    {
        e.preventDefault();
        editor_id = $(this).attr("data-content");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'add_content_of',
                'content': editor_id,
            },
            function(response)
            {
                if ($('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id))
                {
                    content = tinyMCE.get(editor_id).getContent() + response;
                    tinyMCE.get(editor_id).setContent(content);
                    if ($(this).closest(".notif-modif") != undefined)
                        $(this).closest(".notif-modif").find(".input_jpost").addClass("modif");
                }
                else
                {
                    content = $('#'+textarea_id).val() + response;
                    $('#'+textarea_id).val(content);
                }
            }
        );
    });
    
    // Efface le champ
    $("button.del-content").click(del_editor_content);
    
    // Insère la date et l'heure
    $("button.insert-date").click(insert_date);
});
