<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

 
/**
 * Add post type session de formation
 */
function register_cpt_session_formation() {

    /**
     * Post Type: Formations.
     */

    $labels = array(
        "name" => __( "Sessions de formation", "generic" ),
        "singular_name" => __( "Session de formation", "generic" ),
        "all_items" => __( "Toutes les sessions", "generic" ),
        "add_new" => __( "Ajouter une nouvelle", "generic" ),
        "add_new_item" => __("Ajouter une nouvelle session"),
        "view_item" => __("Voir la session"),
        "edit_item" => __("Modifier la session"),
        "update_item" => __("Mettre à jour la session"),
    );

    $args = array(
        "label" => __( "Sessions de formation", "generic" ),
        "labels" => $labels,
        "description" => "Session de formation programmée avec dates et lieu",
        "public" => true,
        "publicly_queryable" => true,
        "delete_with_user" => false,
        "show_in_rest" => false,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => 'sessions',
        'show_ui' => false, // Should the primary admin menu be displayed?
        'show_in_nav_menus' => false, // Should it show up in Appearance > Menus?
        'show_in_menu' => false, // This inherits from show_ui, and determines *where* it should be displayed in the admin
        'show_in_admin_bar' => false, // Should it show up in the toolbar when a user
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "session", "with_front" => true ),
        "query_var" => true,
        "menu_position" => 3.14,
        "menu_icon" => "dashicons-calendar-alt",
        "supports" => array( "title", "thumbnail" ),
        "taxonomies" => array(),
    );

    register_post_type( "session", $args );
}

add_action('init', 'register_cpt_session_formation', 1);
