CREATE TABLE `{prefix}wpof_client` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned DEFAULT NULL,
  `meta_key` varchar(128) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`,`client_id`,`meta_key`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_content` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  `category` text NOT NULL,
  `type` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_content_cat` (
  `slug` text NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `style` longtext DEFAULT NULL,
  `icon` longtext DEFAULT 'fa-question' COMMENT 'Font Awesome icon',
  PRIMARY KEY (`slug`(128))
) {charset_collate};
CREATE TABLE `{prefix}wpof_content_rel` (
  `id_content` bigint(20) unsigned NOT NULL,
  `id_user` bigint(20) unsigned NOT NULL,
  `viewed` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `pinned` tinyint(1) NOT NULL DEFAULT 0,
  UNIQUE KEY `id_content_id_user` (`id_content`,`id_user`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_creneaux` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint(20) unsigned NOT NULL,
  `activite_id` bigint(20) DEFAULT NULL,
  `module_id` bigint(20) DEFAULT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `lieu_id` bigint(20) DEFAULT NULL,
  `salle_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_debut` (`date_debut`,`session_id`,`lieu_id`,`salle_id`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_documents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint(20) unsigned DEFAULT NULL,
  `contexte` smallint(5) unsigned DEFAULT NULL,
  `contexte_id` bigint(20) unsigned DEFAULT NULL,
  `document` varchar(128) DEFAULT NULL,
  `meta_key` varchar(128) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`,`contexte`,`contexte_id`,`document`,`meta_key`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Date et heure',
  `user_name` varchar(512) NOT NULL COMMENT 'Formateur⋅ice',
  `action` varchar(128) NOT NULL COMMENT 'Action',
  `entite_type` varchar(32) NOT NULL COMMENT 'Type entité',
  `entite_id` int(11) NOT NULL COMMENT 'Identifiant entité',
  `entite_name` varchar(2048) NOT NULL COMMENT 'Nom entité',
  `meta_key` varchar(128) DEFAULT NULL COMMENT 'Clé',
  `last_value` text DEFAULT NULL COMMENT 'Ancienne valeur',
  `new_value` text DEFAULT NULL COMMENT 'Nouvelle valeur',
  PRIMARY KEY (`id`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_qreponse` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `questionnaire_id` bigint(20) unsigned DEFAULT NULL,
  `question_id` bigint(20) unsigned DEFAULT NULL,
  `session_id` bigint(20) unsigned DEFAULT NULL,
  `repondeur_id` varchar(32) DEFAULT NULL,
  `repondeur_type` varchar(64) DEFAULT NULL,
  `reponse` longtext DEFAULT NULL,
  `date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_id` (`questionnaire_id`,`question_id`,`session_id`,`repondeur_id`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_question` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(16) DEFAULT NULL,
  `author_id` bigint(20) unsigned DEFAULT NULL,
  `qtype` varchar(64) DEFAULT NULL,
  `title` longtext DEFAULT NULL,
  `options` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_question_relationships` (
  `questionnaire_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `question_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `ordre` smallint(3) unsigned NOT NULL,
  `needed` tinyint(3) unsigned DEFAULT NULL,
  `condition` longtext DEFAULT NULL,
  PRIMARY KEY (`questionnaire_id`,`question_id`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_questionnaire` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(16) DEFAULT NULL,
  `author_id` bigint(20) unsigned DEFAULT NULL,
  `subject` varchar(64) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_questionnairemeta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `qid` int(10) unsigned NOT NULL,
  `meta_key` varchar(128) NOT NULL,
  `meta_value` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_quiz` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quiz_id` bigint(20) unsigned DEFAULT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `type` varchar(2) DEFAULT NULL,
  `subject` varchar(64) DEFAULT NULL,
  `occurrence` tinyint(4) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `meta_key` smallint(5) unsigned DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `quiz_id` (`quiz_id`,`parent_id`,`subject`,`occurrence`,`meta_key`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_session_stagiaire` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` bigint(20) unsigned DEFAULT NULL,
  `stagiaire_id` bigint(20) unsigned DEFAULT NULL,
  `meta_key` varchar(128) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`,`stagiaire_id`,`meta_key`)
) {charset_collate};
CREATE TABLE `{prefix}wpof_statmeta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stat_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) {charset_collate};
