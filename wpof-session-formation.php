<?php
/*
 * wpof-session-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-session-formation.php");
require_once(wpof_path . "/class/class-session-stagiaire.php");

/*
 * État d'avancement de l'inscription d'un stagiaire
 * C'est le formateur qui fait avancer cet état
 */
$etat_session = array
(
    'initial' => __("Demande d'inscription"),
    'inscrit' => __("Inscrit (prêt pour convention)"),
    'confirme' => __("Confirmé (convention reçue signée)"),
    'pendant' => __("En formation"),
    'apres' => __("Formation suivie"),
    'annulok' => __("Annulation dans les délais"),
    'annulhd' => __("Annulation hors délais"),
);

add_action('wp_ajax_first_contact', 'first_contact');
add_action('wp_ajax_nopriv_first_contact', 'first_contact');
function first_contact()
{
    $reponse = array();
    
    // vérification captcha
    if (!empty($_POST['identifiant']))
        return;
    
    if (strtolower($_POST['verif']) != strtolower(date_i18n("F")) && $_POST['verif'] != date("m") && $_POST['verif'] != date("n"))
        $reponse['erreur'] = __("Vous n'avez pas répondu correctement à la question de vérification.");
    else
    {
        global $wpof;
        if (isset($_POST['session_id']))
        {
            $session = get_session_by_id($_POST['session_id']);
            $sujet_demande = sprintf(__("au sujet de la session %s"), $session->titre_session);
            $formateur = $session->formateur;
            $permalien = $session->permalien;
        }
        elseif (isset($_POST['formation_id']))
        {
            $formation = get_formation_by_id($_POST['formation_id']);
            $sujet_demande = sprintf(__("au sujet de la formation %s"), $formation->titre);
            $formateur = $formation->formateur;
            $permalien = $formation->permalien;
        }
        elseif (isset($_POST['formateur_id']))
        {
            $sujet_demande = sprintf(__("Contact depuis le site %s"), get_bloginfo('name'));
            $formateur_class = get_formateur_by_id($_POST['formateur_id']);
            $formateur = array($formateur_class->id);
            $permalien = $formateur_class->permalink;
        }
        if (!empty($_POST['choix']))
        {
            if (in_array(substr($_POST['choix'], 0, 1), array('a', 'e', 'i', 'o', 'u', 'y')))
                $choix = sprintf(__("d'%s"), $_POST['choix']);
            else
                $choix = sprintf(__("de %s"), $_POST['choix']);
        }
        else
            $choix = "";
        
        $destinataires = array();
        foreach(array_keys($wpof->tache_premier_contact) as $id)
        {
            if ($id == "0") // 0 = équipe pédagogique
            {
                foreach($formateur as $user_id)
                {
                    $user = get_userdata($user_id);
                    if ($user != false)
                        $destinataires[$user_id] = $user->user_email;
                }
            }
            else
            {
                $user = get_userdata($id);
                if ($user != false)
                    $destinataires[$id] = $user->user_email;
            }
        }
        
        $corps_email = sprintf("Bonjour,\n\n%s vient de faire une demande %s %s.\n→ %s\n\nstructure : %s\nemail : %s\ntéléphone : %s\n%s\n\n-- \nDemande effectuée depuis le site %s.",
                               $_POST['nom'],
                               $choix,
                               $sujet_demande,
                               $permalien,
                               $_POST['structure'],
                               $_POST['email'],
                               $_POST['telephone'],
                               $_POST['message'],
                               get_site_url());
        
        $sujet_email = sprintf("[Formation] Demande %s de %s", $choix, $_POST['nom']);
        
        $result = wp_mail($destinataires, $sujet_email, $corps_email);
        if ($result === true)
            $reponse['succes'] = __("Votre demande a été transmise, nous vous contactons très rapidement.");
        else
            $reponse['erreur'] = __("Il y a eu un problème lors de l'envoi du courriel. Merci de recommencer.");
    }
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_get_new_token', 'get_new_token');
add_action('wp_ajax_nopriv_get_new_token', 'get_new_token');
function get_new_token()
{
    $reponse = array();
    
    // vérification captcha
    if (!empty($_POST['identifiant']))
        return;
    
    if (strtolower($_POST['verif']) != strtolower(date_i18n("F")) && $_POST['verif'] != date("m") && $_POST['verif'] != date("n"))
    {
        $reponse['erreur'] = __("Vous n'avez pas répondu correctement à la question de vérification.");
        $reponse = json_encode($reponse);
    }
    else
        $reponse = reset_token_with_email($_POST['session_id'], $_POST['email']);
    
    echo $reponse;
    die();
}

/*
 * Traitement questionnaire de connaissances
 *
 */
function traitement_questionnaire_connaissances($sujet)
{
    if (!isset($_POST['quiz'][$sujet]) || !isset($_POST['quiz_'.$sujet])) return;
    
    $tab_quiz = $_POST['quiz'][$sujet];
    
    $quiz = new Quiz($tab_quiz['id']);
    $quiz->set_identite($sujet, $tab_quiz['parent_id']);
    
    $quiz->reponses = $_POST['quiz_'.$sujet];
    $quiz->update_reponses($_POST['user_id'], $tab_quiz['occurrence']);
}


add_action('wp_ajax_enregistrer_stagiaire_form', 'enregistrer_stagiaire_form');
function enregistrer_stagiaire_form()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);

    $stagiaire = get_stagiaire_by_id($_POST['session_id'], $_POST['user_id']);
    
    // champs simples, sans traitement particulier
    $fields = array
    (
        'attentes',
    );
    foreach ($fields as $f)
        if (isset($_POST[$f]))
            $stagiaire->update_meta($f, $_POST[$f]);

    if (isset($_POST['quiz_sujet']))
    {
        foreach($_POST['quiz_sujet'] as $sujet)
            traitement_questionnaire_connaissances($sujet);
    }
    $reponse['message'] = "<span class='succes'>".__("Modifications enregistrées")."</span>";
    echo json_encode($reponse);
    die();
}

/**
 * Filter the upload size limit for non-administrators.
 *
 * @param string $size Upload size limit (in bytes).
 * @return int (maybe) Filtered size limit.
 */
function filter_session_upload_size_limit($size)
{
    global $wpof;
    // Set the upload size limit to 60 MB for users lacking the 'manage_options' capability.
    if (is_numeric($wpof->max_upload_size) && ! current_user_can('manage_options'))
        $size = $wpof->max_upload_size * 1024 * 1024;
    return $size;
}
add_filter( 'upload_size_limit', 'filter_session_upload_size_limit', 20 );


add_action( 'wp_ajax_delete_entity', 'delete_entity' );
function delete_entity()
{
    $reponse = array();
    
    $object_class = $_POST['object_class'];
    
    switch ($object_class)
    {
        case "Client":
        case "SessionStagiaire":
            try
            {
                $object = new $object_class($_POST['session_id'], $_POST['id']);
            }
            catch(Exception $e)
            {
                $reponse['erreur'] = "Pas de $object_class ".$_POST['id']." pour la session ".$_POST['session_id'];
            }
            break;
        case "SessionFormation":
            try
            {
                $object = new $object_class($_POST['id']);
            }
            catch(Exception $e)
            {
                $reponse['erreur'] = "Pas de $object_class ".$_POST['id'];
            }
            break;
        case "Utilisateur":
            require_once ABSPATH . '/wp-admin/includes/user.php';
            break;
        default:
            $reponse['erreur'] = "Classe d'objet non reconnu ".$_POST['object_class'];
            break;
    }
    
    if (!isset($reponse['erreur']))
    {
        if ($object_class == "SessionStagiaire")
        {
            $client = get_client_by_id($object->session_formation_id, $object->client_id);
            if ($client)
                $reponse['url'] = $client->permalien."#stagiaires";
        }
        if ($object_class == "Utilisateur")
            $reponse['log'] = wp_delete_user($_POST['id']);
        else
            $reponse['log'] = $object->delete();
        $reponse['succes'] = "Succès";
    }
    
    echo json_encode($reponse);
    die();
}

add_action( 'wp_ajax_enregistrer_session_input', 'enregistrer_session_input' );
function enregistrer_session_input()
{
    $session_id = $_POST['session_id'];
    $fields = $_POST['fields'];
    
    foreach($fields as $key => $value)
    {
        $result = update_post_meta($session_id, $key, $value);
        if ($result)
            echo "<span class='succes'>[ ".$key." ] ".__("mise à jour")."</span><br />";
        else
            echo "<span class='alerte'>[ ".$key." ] ".__("inchangée")."</span><br />";
    }
    
    die();
}

add_action('wp_ajax_sort_clients_stagiaires', 'sort_clients_stagiaires');
function sort_clients_stagiaires()
{
    $session_id = $_POST['session_id'];
    $last_client = null;
    foreach($_POST['list_id'] as $duo)
    {
        if ($duo['type'] == "client") // le premier élément est forcément un client
        {
            if ($last_client != null)
                $last_client->update_meta("stagiaires"); // mise à jour du précédent client
            $last_client = get_client_by_id($session_id, $duo['id']);
            $last_client->stagiaires = array();
            $last_client_id = $duo['id'];
        }
        else
        {
            $stagiaire = get_stagiaire_by_id($session_id, $duo['id']);
            $stagiaire->update_meta("client_id", $last_client_id);
            $last_client->stagiaires[] = $duo['id'];
            echo "Stagiaire ".$duo['id']." → client $last_client_id";
        }
    }
    // mise à jour du dernier client
    $last_client->update_meta("stagiaires");
    
    die();
}


add_action('wp_ajax_show_missing_data', 'show_missing_data');
function show_missing_data()
{
    $reponse = array('erreur' => '');
    if (isset($_POST['contexte']) && isset($_POST['id']))
    {
        $entity = null;
        switch ($_POST['contexte'])
        {
            case 'formateur':
                $entity = new Formateur($_POST['id']);
                break;
            case 'formation':
                $entity = new Formation($_POST['id']);
                break;
            case 'session':
                $entity = new SessionFormation($_POST['id']);
                break;
            default:
                break;
        }
        
        if ($entity !== null)
        {
            $reponse['erreur'] .= '<h2>'.__('Vous devez encore renseigner les champs suivants').'</h2>';
            $reponse['erreur'] .= '<ul>';
            foreach($entity->get_completion_missing() as $term)
            {
                $reponse['erreur'] .= '<li>'.$term.'</li>';
            }
            $reponse['erreur'] .= '</ul>';
        }
    }
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_add_new_date_line', 'add_new_date_line');
function add_new_date_line()
{
    if (isset($_POST['session_id']))
        $session = new SessionFormation($_POST['session_id']);
    else
        $session = new SessionFormation();
    
    $newdate = null;
    
    if (!empty($_POST['fields']))
    {
        $datebase = $_POST['fields']['datebase'];
        $decaljour = $_POST['fields']['decaljour'];
        echo $datebase;
        $newdate_obj = DateTime::createFromFormat("d/m/Y", $datebase);
        $newdate_obj->add(new DateInterval('P'.$decaljour.'D'));
        $newdate = $newdate_obj->format('d/m/Y');
        $session->creneaux[$newdate] = $session->creneaux[$datebase];
        foreach($session->creneaux[$newdate] as $creno)
        {
            $creno->id = -1;
        }
    }
    echo $session->get_html_ligne_creneaux(true, $newdate);
    
    die();
}


add_action('wp_ajax_session_decale_date', 'session_decale_date');
function session_decale_date()
{
    $reponse = array();
    $session = get_session_by_id($_POST['session_id']);
    $new_date = DateTime::createFromFormat("d/m/Y", $_POST['new_date']);
    $old_date = DateTime::createFromFormat("d/m/Y", $session->first_date);
    $session->first_date = $new_date->format("d/m/Y");
    
    $decalage = $old_date->diff($new_date);
    
    $new_creneaux = array();
    foreach($session->creneaux as $date => $tab_creno)
    {
        $nouvelle_date = DateTime::createFromFormat("d/m/Y", $date)->add($decalage);
        $new_creneaux[$nouvelle_date->format("d/m/Y")] = $tab_creno;
        foreach(array_keys($tab_creno) as $c_id)
        {
            $creno = new Creneau($c_id);
            $creno->set_date($nouvelle_date->format("d/m/Y"));
            $creno->update();
        }
    }
    
    $session->init_slug(true);
    $reponse['result'] = $session->update_meta("creneaux", $new_creneaux);
    
    // au cas où, on précise bien que cette session n'a pas été réalisée
    $session->update_meta("realisee", 0);
    
    // attention, le permalien change lorsqu'on change de date
    // il faut donc obtenir le nouveau permalien
    // et supprimer l'ancien slug au début de uri_params
    global $wpof;
    $old_slug = array_shift($wpof->uri_params);
    $reponse['url'] = get_the_permalink($session->id).implode('/', $wpof->uri_params).'/';
    
    echo json_encode($reponse);
    die();
}

// TODO : à supprimer
add_action('wp_ajax_add_new_creneau', 'add_new_creneau');
function add_new_creneau()
{
    $creno = new Creneau();
    if (isset($_POST['date']))
    {
        $creno->date = $_POST['date'];
        $creno->session_id = $_POST['session_id'];
        $creno->lieu_id = $_POST['lieu_id'];
        if ($creno->lieu_id == -1)
            $creno->lieu_nom = $_POST['lieu_nom'];
        echo $creno->get_html("", true);
    }
    
    die();
}

add_action('wp_ajax_update_date', 'update_date');
function update_date()
{
    $reponse = array('log' => '');
    $session = get_session_by_id($_POST['session_id']);
    
    if (isset($_POST['old_date']) && array_key_exists($_POST['old_date'], $session->creneaux))
    {
        $session->creneaux[$_POST['date']] = $session->creneaux[$_POST['old_date']];
        unset($session->creneaux[$_POST['old_date']]);
        foreach(array_keys($session->creneaux[$_POST['date']]) as $c_id)
        {
            $creno = new Creneau($c_id);
            $creno->set_date($_POST['date']);
            $creno->update();
        }
    }
    else
    {
        $session->creneaux[$_POST['date']] = array();
        if (isset($_POST['creneaux']))
        {
            foreach($_POST['creneaux'] as $c_id)
            {
                $original = new Creneau($c_id);
                $copie = clone $original;
                $copie->id = -1;
                $copie->set_date($_POST['date']);
                $copie->update();
                $session->creneaux[$_POST['date']][] = $copie;
            }
        }
        $reponse['html'] = $session->get_html_ligne_creneaux(true, $_POST['date']);
    }
    
    $session->update_meta("creneaux");
    
    $reponse['log'] .= var_export($_POST, true);
    
    echo json_encode($reponse);
    die();
}

/*
add_action('wp_ajax_update_creneau', 'update_creneau');
function update_creneau()
{
    $creno = new Creneau();
    $session = get_session_by_id($_POST['data']['session_id']);
    if (isset($_POST['data']))
        $creno->init_from_form($_POST['data']);
    $creno->update();
    
    $session->creneaux[$_POST['data']['date']][] = $creno;
    $session->update_meta("creneaux");
    
    echo $creno->get_html("", true);
    
    die();
}
*/

add_action('wp_ajax_active_creneau', 'active_creneau');
function active_creneau()
{
    $reponse = array();
    if (isset($_POST['creno_id']) && isset($_POST['actif']) && isset($_POST['session_id']) && isset($_POST['objet_id']))
    {
        $objet = new $_POST['objet']($_POST['session_id'], $_POST['objet_id']);
        $objet->creneaux[$_POST['creno_id']] = (integer) $_POST['actif'];
        $objet->update_meta("creneaux", $objet->creneaux);
        $objet->update_meta("nb_heure_estime_decimal", "delete_meta");
        $objet->calcule_temps_session();
        $reponse['nb_heure_estime_decimal'] = $objet->nb_heure_estime_decimal;
    }
    else
    {
        $reponse['erreur'] = var_export($_POST, true);
    }
    
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_get_session_stagiaire_data', 'get_session_stagiaire_data');
function get_session_stagiaire_data()
{
    if (isset($_POST['session_id']) && isset($_POST['user_id']) && isset($_POST['data']))
    {
        $session_stagiaire = new SessionStagiaire($_POST['session_id'], $_POST['user_id']);
        $data = $_POST['data'];
        
        $reponse = array();
        foreach($data as $d)
            $reponse[$d] = $session_stagiaire->$d;
        
        echo json_encode($reponse);
    }
    
    die();
}

add_action('wp_ajax_update_duree_st_session', 'update_duree_st_session');
function update_duree_st_session()
{
    $session_formation = new SessionFormation($_POST['session_id']);
    $session_formation->update_meta('nb_heure_decimal', $_POST['nb_heures']);
    
    die();
}

add_action('wp_ajax_update_jpost_value', 'update_jpost_value');
function update_jpost_value()
{
    global $wpof;
    $reponse = array();
    $valeur = "delete_meta";
    if (isset($_POST['value']))
    {
        if (is_array($_POST['value']))
        {
            $valeur = array();
            foreach($_POST['value'] as $v)
                $valeur[] = stripslashes($v);
        }
        else
            $valeur = stripslashes($_POST['value']);
    }
    
    $object_class = (isset($_POST['object_class'])) ? $_POST['object_class'] : "SessionFormation" ;
    
    if (isset($_POST['stagiaire_id']) && (integer) $_POST['stagiaire_id'] > 0)
        $object_class = "SessionStagiaire";
    
    $reponse['log'] = $_POST;
    $reponse['log']['valeur'] = $valeur;
    
    switch ($object_class)
    {
        case "SessionStagiaire":
            $stagiaire = get_stagiaire_by_id($_POST['session_id'], $_POST['stagiaire_id']);
            $stagiaire->update_meta($_POST['meta_key'], $valeur);
            $valeur = $stagiaire->{$_POST['meta_key']};
            break;
        case "Client":
            $client = new Client($_POST['session_id'], $_POST['client_id']);
            $client->update_meta($_POST['meta_key'], $valeur);
            $valeur = $client->{$_POST['meta_key']};
            break;
        case "Formateur":
            $formateur = new Formateur($_POST['user_id']);
            $formateur->update_meta($_POST['meta_key'], $valeur);
            $valeur = $formateur->{$_POST['meta_key']};
            break;
        case "Formation":
            $formation = new Formation($_POST['formation_id']);
            $formation->update_meta($_POST['meta_key'], $valeur);
            $valeur = $formation->{$_POST['meta_key']};
            break;
        case "SessionFormation":
            $session = new SessionFormation($_POST['session_id']);
            $session->update_meta($_POST['meta_key'], $valeur);
            $valeur = $session->{$_POST['meta_key']};
            break;
        case "Document":
            $document = new Document($_POST['type_doc'], $_POST['session_id'], $_POST['contexte'], $_POST['contexte_id']);
            $document->update_meta($_POST['meta_key'], $valeur);
            $valeur = $document->{$_POST['meta_key']};
            break;
        case "Stat":
            $stat = new Stat($_POST['stat_id']);
            $stat->update_meta($_POST['meta_key'], $valeur);
            //$valeur = $stat->{$_POST['meta_key']};
            break;
        default:
            $reponse['erreur'] = sprintf(__("%s n'est pas une classe d'objet reconnue"), $object_class);
            break;
    }
    
    if (!isset($reponse['erreur']))
    {
        if (isset($_POST['postprocess']))
        {
            $reponse['postprocess_args'] = $_POST;
            $reponse['postprocess_args']['action'] = $_POST['postprocess'];
        }
        
        $listname = $_POST['meta_key'];
        if ($_POST['nodeName'] == "select")
        {
            if (!isset($wpof->$listname))
                init_term_list($listname);
            $reponse['valeur'] = $wpof->$listname->get_term($valeur);
            $reponse['key'] = $valeur;
        }
        else
            $reponse['valeur'] = $reponse['key'] = $valeur;
        
        $reponse['succes'] = 1;
    }
    
    echo json_encode($reponse);
    die();
}


add_action('wp_ajax_update_pour_infos', 'update_pour_infos');
function update_pour_infos()
{
    $reponse = array();
    $reponse['parent'] = ".pour-infos";
    $object_class = $_POST['object_class'];
    switch ($object_class)
    {
        case "Client":
            $objet = new $object_class($_POST['session_id'], $_POST['client_id']);
            break;
        case "SessionFormation":
            $objet = new $object_class($_POST['session_id']);
            break;
        case "SessionStagiaire":
        default:
            $reponse['erreur'] = __("Classe inconnue ").$object_class;
            break;
    }
    
    if (!isset($reponse['erreur']))
        $reponse['html'] = $objet->get_pour_infos_box();
    
    echo json_encode($reponse);
    die();
}


add_action('wp_ajax_update_data', 'update_data');
function update_data()
{
    $reponse = array();
    $session_id = (isset($_POST['session_id'])) ? $_POST['session_id'] : -1;
    switch($_POST['object_class'])
    {
        case "SessionFormation":
            $objet = new SessionFormation($session_id);
            if ($objet)
                $objet->update_meta($_POST['meta_key'], $_POST['meta_value']);
            else
                $reponse['log'] = sprintf(__("Erreur, session %d introuvable !"), $session_id);
            break;
        case "Client":
            $objet = new Client($session_id, $_POST['client_id']);
            if ($objet)
                $objet->update_meta($_POST['meta_key'], $_POST['meta_value']);
            else
                $reponse['log'] = sprintf(__("Erreur, client %d pour la session %d introuvable !"), $_POST['client_id'], $session_id);
            break;
        case "SessionStagiaire":
            $objet = new SessionStagiaire($session_id, $_POST['stagiaire_id']);
            if ($objet)
                $objet->update_meta($_POST['meta_key'], $_POST['meta_value']);
            else
                $reponse['log'] = sprintf(__("Erreur, stagiaire %d pour la session %d introuvable !"), $_POST['stagiaire_id'], $session_id);
            break;
        case "Formateur":
            $objet = new Formateur($_POST['user_id']);
            if ($objet)
                $objet->update_meta($_POST['meta_key'], $_POST['meta_value']);
            else
                $reponse['log'] = sprintf(__("Erreur, formateur⋅ice %d %d introuvable !"), $_POST['formateur_id']);
        default:
            break;
    }
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_update_lieu_details', 'update_lieu_details');
function update_lieu_details()
{
    $reponse = array();
    $session = get_session_by_id($_POST['session_id']);
    $reponse['html'] = $session->get_lieu_details();
    $reponse['log'] = var_export($_POST, true);
    
    echo json_encode($reponse);
    die();
}


add_action('wp_ajax_add_new_session', 'add_new_session');
function add_new_session()
{
    global $wpof;
    
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    $has_formation = isset($_POST['formation']);
    
    if ($_POST['session_unique_titre'] == "" && (($has_formation && $_POST['formation'] == -1) || (!$has_formation)))
    {
        $reponse['erreur'] = __("Vous devez fournir un titre");
        echo json_encode($reponse);
        die();
    }
    
    if ($has_formation)
    {
        $title = ($_POST['session_unique_titre'] != "") ? $_POST['session_unique_titre'] : get_the_title($_POST['formation']);
        $id_formation = $_POST['formation'];
    }
    else
    {
        $title = $_POST['session_unique_titre'];
        $id_formation = -1;
    }
    if (empty($title))
    {
        $reponse['erreur'] = __("Titre non reconnu")." : ".__("ID formation : ").$id_formation;
        echo json_encode($reponse);
        die();
    }
    $slug = sanitize_title($title)."-".date("Ymd-His");
    $session_id = wp_insert_post(array('post_title' => $title, 'post_type' => 'session', 'post_status' => 'publish', 'post_author' => get_current_user_id(), 'post_name' => $slug), true);
    if (is_wp_error($session_id))
    {
        $reponse['erreur'] = __("Erreur de création de session")." : ".__("WP Error : ").$session_id->get_error_message();
        echo json_encode($reponse);
        die();
    }
    
    // index de formation (-1 si pas de formation)
    update_post_meta($session_id, "formation", $id_formation);
    
    $session = get_session_by_id($session_id);

    $wpof->log->log_action(__FUNCTION__, $session);
    
    $formation = get_formation_by_id($id_formation);
    // ID des formateurs dans la session
    if (!empty($_POST['formateur']))
    {
        // liste des formateurs sous forme de tableau
        $formateur = explode(',', $_POST['formateur']);
    }
    else
        $formateur = $formation->formateur;
    
    $session->update_meta("formateur", $formateur);
    if (isset($formation->tarif_base_inter_total))
        $session->update_meta("tarif_total_chiffre", $formation->tarif_base_inter_total);
    
    // Accès sur invitation par défaut
    $session->update_meta("acces_session", "invite");
    
    // titre de session unique
    $session->update_meta("session_unique_titre", $_POST['session_unique_titre']);
    
    $reponse['url'] = get_permalink($session_id);
    
    if (!empty($_POST['type_client']) && $_POST['type_client'] == Client::FINANCEMENT_OPAC)
    {
        if (!empty($_POST['nom']))
        {
            $client = new Client($session_id);
            $client->update_meta("nom", $_POST["nom"]);
        }
        elseif (!empty($_POST['client_id'][Client::ENTITE_PERS_MORALE]))
        {
            $_POST['session_id'] = $session_id;
            $reponse['add_client'] = add_client_session();
            $client = $reponse['add_client']['client'];
            unset($reponse['add_client']['client']);
        }

        $client->update_meta('entite_client', 'morale');
        $client->update_meta("financement", "opac");
        
        if (!empty($_POST['dates']))
            $session->update_meta("creneaux", array_flip(array_values(convert_dates($_POST['dates']))));
        
        foreach(array("numero_facture", "num_of", "tarif_total_chiffre", "tarif_total_autres_chiffre", "nb_stagiaires", "nb_heure_estime_decimal", "nb_heures_stagiaires") as $key)
            if (!empty($_POST[$key]))
                $client->update_meta($key, $_POST[$key]);
    
        $session->update_meta("clients", array($client->id));
        $session->update_meta("nature_formation", "form");

        if (empty($_POST['reload_url']))
        {
            $reponse['succes'] = sprintf("Session %s et client %s créés", $session->get_a_link(), $client->get_displayname());
            unset($reponse['url']);
        }
        else
            $reponse['url'] = $session->permalien.'client/'.$client->id;
    }
    
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_add_new_client', 'add_new_client');
function add_new_client()
{
    global $wpof;
    
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    if ($_POST['nom'] == "")
    {
        $reponse['erreur'] = __("Vous devez fournir un nom ou une raison sociale");
        echo json_encode($reponse);
        die();
    }
    
    if (!isset($_POST['session_id']))
    {
        $reponse['erreur'] = __("Identifiant de session absent");
    }
    $session_id = $_POST['session_id'];
    $session = new SessionFormation($session_id);
    
    if ($session == null)
    {
        $reponse['erreur'] = sprintf(__("La session dont l'ID est %d n'existe pas"), $session_id);
    }
    
    $client = new Client($session_id);
    $client->id = $client->last_client_id() + 1;
    $client->update_meta('external_id', $client->id);
    $client->init_creneaux();
    
    $wpof->log->log_action(__FUNCTION__, $client);
    
    // Si le client est un particulier, alors on crée son entité stagiaire
    if ($_POST['type_client'] == Client::ENTITE_PERS_PHYSIQUE)
    {
        $stagiaire = add_stagiaire(array('session_id' => $session_id, 'client_id' => $client->id, 'nom' => $_POST['nom'], 'prenom' => $_POST['prenom']));
        $client->update_meta('entite_client', Client::ENTITE_PERS_PHYSIQUE);
        if (is_array($stagiaire))
            $reponse['erreur'] = array_merge(array('log' => __("Erreur sur la création de l'entité stagiaire")), $stagiaire);
    }
    else
    {
        $client->update_meta('entite_client', Client::ENTITE_PERS_MORALE);
        $client->update_meta("nom", $_POST["nom"]);
        if (isset($_POST['contact']))
            $client->update_meta('contact', $_POST['contact']);
    }
    
    if (!isset($reponse['erreur']))
    {
        if ($_POST['type_client'] == Client::FINANCEMENT_OPAC)
            $client->update_meta("financement", Client::FINANCEMENT_OPAC);
        
        $session->clients[] = $client->id;
        $session->update_meta("clients", array_unique($session->clients));
        
        $reponse['url'] = $session->permalien.'client/'.$client->id;
    }
    
    echo json_encode($reponse);
    die();
}



add_action('wp_ajax_add_client_session', 'add_client_session');
function add_client_session()
{
    global $wpof;
    
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    if (empty($_POST['type_client']))
    {
        $reponse['erreur'] = __("Vous devez sélectionner un type de client");
        echo json_encode($reponse);
        die();
    }
    $entite_client = ($_POST['type_client'] == Client::FINANCEMENT_OPAC) ? Client::ENTITE_PERS_MORALE : $_POST['type_client'];
    
    if ($_POST['client_id'][$entite_client] == "")
    {
        $reponse['erreur'] = __("Vous devez sélectionner ou créer un client");
        echo json_encode($reponse);
        die();
    }
    $client_id = $_POST['client_id'][$entite_client];
    
    if (!isset($_POST['session_id']))
    {
        $reponse['erreur'] = __("Identifiant de session absent");
    }
    $session_id = $_POST['session_id'];
    $session = new SessionFormation($session_id);
    
    if ($session == null)
    {
        $reponse['erreur'] = sprintf(__("La session dont l'ID est %d n'existe pas"), $session_id);
    }
    
    $client = new Client($session_id);
    // L'id du client au niveau de la session
    $client->id = $client->last_client_id() + 1;
    // L'id du client tel que transmis par le form et qui correspond à un id propre au client quelle que soit la session. Peut être l'id provenant d'un erp externe par exemple.
    $client->update_meta('external_id', $client_id);
    $client->init_creneaux();
    
    $wpof->log->log_action(__FUNCTION__, $client);

    $plugin = $wpof->plugin_manager->getPlugin();
    $client_infos = $plugin->getClientInfos($client_id);

    // TODO pour l'instant on prend le premier contact, gérer les contacts multiples.
    $contact = reset($client_infos['contact']);

    // Si le client est un particulier, alors on crée son entité stagiaire
    if ($entite_client == Client::ENTITE_PERS_PHYSIQUE)
    {
        $infos_stagiaire = ['session_id' => $session_id, 'client_id' => $client->id];
        foreach ($client_infos as $key => $value) {
            if ($value && in_array($key, ['nom', 'prenom', 'email'])) {
                $infos_stagiaire[$key] = $value;
            }
        }
        
        $reponse['log'] .= var_export($client_infos, true);
        $stagiaire = add_stagiaire($infos_stagiaire);
        if ("SessionStagiaire" != get_class($stagiaire))
            $reponse['erreur'] = array_merge(array('log' => __("Erreur sur la création de l'entité stagiaire")), $stagiaire);
        else {
            $client->update_meta('entite_client', Client::ENTITE_PERS_PHYSIQUE);
            $client->update_meta('stagiaires', array($stagiaire->id));
            foreach ($contact as $key => $value) {
                if ($value && in_array($key, ['adresse', 'code_postal', 'ville', 'pays', 'telephone'])) {
                    $client->update_meta($key, $value);
                }
            }
        }
    }
    else
    {
        $client->update_meta('entite_client', Client::ENTITE_PERS_MORALE);
        foreach ($client_infos as $key => $value) {
            if ($value && in_array($key, ['nom', 'siret', 'tva'])) {
                $client->update_meta($key, $value);
            }
        }
        foreach ($contact as $key => $value) {
            if ($value && in_array($key, ['adresse', 'code_postal', 'ville', 'pays', 'telephone'])) {
                $client->update_meta($key, $value);
            }
        }
    }
    
    
    if (!isset($reponse['erreur']))
    {
        if ($_POST['type_client'] == Client::FINANCEMENT_OPAC)
            $client->update_meta("financement", Client::FINANCEMENT_OPAC);
        
        $session->clients[] = $client->id;
        $session->update_meta("clients", array_unique($session->clients));
        
        $reponse['url'] = $session->permalien.'client/'.$client->id;
    }
    
    if (__FUNCTION__ == $_POST['action'])
    {
        echo json_encode($reponse);
        die();
    }
    else
    {
        $reponse['client'] = $client;
        return $reponse;
    }
}

add_action('wp_ajax_connect_client_externalid', 'connect_client_externalid');
function connect_client_externalid()
{
    $reponse = array("erreur" => __("Échec de la connexion"));
    ob_start();
    if (isset($_POST["client_id"]) && isset($_POST['session_id']) && isset($_POST['type_client']) && isset($_POST['opaga_client_id']))
    {
        $client = new Client($_POST['session_id'], $_POST["opaga_client_id"]);
        if (get_class($client) === "Client")
        {
            $client->update_meta('external_id', $_POST['client_id'][$_POST['type_client']]);
            unset($reponse['erreur']);
        }
    }

    $reponse['log'] = ob_get_clean();
    echo json_encode($reponse);
    die();
};

// Check qu'on peut bien facturer et si oui renvoie vers l'url de facturation.
add_action('wp_ajax_go_facturation', 'go_facturation');
function go_facturation()
{
    global $wpof;

    $reponse = array();
    $client  = get_client_by_id($_POST['session_id'], $_POST['client_id']);
    $session = get_session_by_id($_POST['session_id']);

    $is_acompte          = (!empty($_POST['is_acompte'])) ? $_POST['is_acompte'] : false;
    $is_solde          = (!empty($_POST['is_solde'])) ? $_POST['is_solde'] : false;

    global $Documents;
    $client->init_docs();
    
    $doc = $Documents[$client->get_contrat_uid(false)];
    if (! $token = $doc->get_meta('token')) {
        $token = new Token();
        $doc->update_meta('token', $token->token);
        $token = $token->token;
    }

    $http_params = [];
    $http_params['external_link'] = $doc->href.'&dt='.$token;
    if ($is_acompte) {
        $montant_facture = $client->acompte;
    } elseif ($is_solde) {
        $montant_facture = $client->tarif_total_chiffre;
    } else {
        $montant_facture = 0;
    }

    $plugin            = $wpof->plugin_manager->getPlugin();
    $reponse['url']    = $plugin->getCreateFactureUrl($client, $session, $montant_facture, $is_acompte, $is_solde, $http_params);
    $reponse['newtab'] = 1;
    $reponse['succes'] = __('Vous allez être redirigé vers la page de facturation');

    echo json_encode($reponse);
    die;
}

add_action('wp_ajax_sendmail_to_stagiaires', 'sendmail_to_stagiaires');
function sendmail_to_stagiaires()
{
    global $wpof, $Client, $SessionStagiaire;
    $message_retour = [];
    $session = new SessionFormation($_POST['session_id']);
    $session->init_clients();
    $questionnaire_url = home_url()."/".$wpof->url_questionnaire."/".$session->id."/".$_POST['questionnaire_id'];

    foreach($session->clients as $cid)
    {
        foreach($Client[$cid]->stagiaires as $sid)
        {
            if (!empty($SessionStagiaire[$sid]->email))
            {
                $message = new Message([ 'to' => $SessionStagiaire[$sid]->email, 'subject' => $_POST['subject'], 'content' => $_POST['content'] ]);
                $res = $message->sendmail();
                if ($res)
                {
                    $message_retour[] = '<p><span class="succes">'.__('Envoi réussi').'</span> '.__('pour').' '.$SessionStagiaire[$sid]->get_displayname(false).'</p>';
                    $wpof->log->log_action('questionnaire_send_by_mail', $SessionStagiaire[$sid], 'questionnaire_id', '', $_POST['questionnaire_id']);
                }
                else
                    $message_retour[] = '<p><span class="echec">'.__('Envoi échoué').'</span> '.__('pour').' '.$SessionStagiaire[$sid]->get_displayname(false).'</p>';
            }
        }
    }

    echo json_encode([ 'message' => implode('', $message_retour) ]);
    die;
}

add_action('wp_ajax_compose_missing_mail_stagiaires', 'compose_missing_mail_stagiaires');
function compose_missing_mail_stagiaires()
{
    $session = new SessionFormation($_POST['session_id']);
    $session->init_clients();
    ?>
    <p><?php _e("Les stagiaires suivant⋅es n'ont pas d'adresse email renseignée :"); ?></p>
    <ul>
    <?php foreach ($session->check_missing_email_stagiaires() as $id => $stagiaire_name) : ?>
        <li><?php echo $stagiaire_name; ?></li>
    <?php endforeach; ?>
    </ul>
    <p><?php _e('Souhaitez-vous tout de même envoyer le mail ?'); ?></p>
    <?php
    die;
}


// Modification d'un créneau au niveau de la session
add_action('wp_ajax_edit_creneau', 'edit_creneau');
function edit_creneau()
{
    $reponse = array();
    //$reponse['log'] = var_export($_POST, true);
    
    $session = get_session_by_id($_POST['session_id']);
    
    if (isset($_POST['creno_id']))
    {
        $data = $_POST;
        if (isset($_POST['lieu']))
            $data['lieu_id'] = $_POST['lieu'];
        
        $creno = new Creneau($_POST['creno_id']);
        $creno->init_from_form($data);
        $creno->update();
        
        $session->creneaux[$data['date']][] = $creno;
        $session->update_meta("creneaux");
        
        if ($_POST['creno_id'] < 0) // nouveau créneau
        {
            $reponse["replacewith"] = ".liste-creneau";
            $reponse["eventsfunc"] = "edit_date_add_events";
            $reponse['html'] = $session->get_html_ligne_creneaux(true, $data['date']);
        }
        else // créneau modifié
        {
            $reponse["replacewith"] = ".creneau";
            $reponse["eventsfunc"] = "edit_creneau_add_events";
            $reponse['html'] = $creno->get_html("", true);
        }
    }
    
    echo json_encode($reponse);
    
    die();
}

// Modification d'un créneau au niveau de la session
add_action('wp_ajax_edit_suivi_creneau', 'edit_suivi_creneau');
function edit_suivi_creneau()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    $stagiaire = get_stagiaire_by_id($_POST['session_id'], $_POST['stagiaire_id']);
    $stagiaire->creneaux_suivi[$_POST['creno_id']] = $_POST['suivi_creneau'];
    $reponse['htmlentities'] = htmlentities($_POST['suivi_creneau'], ENT_QUOTES);
    $reponse['stripslashes'] = stripslashes($_POST['suivi_creneau']);
    $stagiaire->update_meta("creneaux_suivi");

    echo json_encode($reponse);
    
    die();
}


add_action('wp_ajax_del_date_creneau', 'del_date_creneau');
function del_date_creneau()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    $session = get_session_by_id($_POST['session_id']);
    
    if (isset($_POST['creneau']))
    {
        if (isset($session->creneaux))
        {
            $creno = new Creneau($_POST['creneau']);
            $creno->delete();
        }
    }
    else
    {
        if (isset($session->creneaux))
        {
            foreach($session->creneaux[$_POST['date']] as $creno)
            {
                if (get_class($creno) == "Creneau")
                    $creno->delete();
                elseif (is_integer($creno))
                {
                    $creno_obj = new Creneau($creno);
                    $creno_obj->delete();
                }
            }
            
            unset($session->creneaux[$_POST['date']]);
            $session->update_meta("creneaux");
        }
        else
        {
            $session->supprime_sous_entite("dates_array", $_POST['date']);
            $session->update_meta("dates_array");
        }
    }
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_check_duree_max', 'check_duree_max');
function check_duree_max()
{
    global $wpof;
    $reponse = array();
    $session = new SessionFormation($_POST['session_id']);
    if ($session->depassement_duree > 0)
        $reponse['erreur'] = sprintf(__("Attention, la durée totale est limitée à %s h. Vous dépassez de %s h !"), $wpof->max_duree_bc, $session->depassement_duree);
    
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_update_session_titre', 'update_session_titre');
function update_session_titre()
{
    $reponse = array();
    $session = get_session_by_id($_POST['session_id']);
    if ($session->check_post_title())
        $reponse['titre'] = $session->titre_session;
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_session_get_dates_texte', 'session_get_dates_texte');
function session_get_dates_texte()
{
    $reponse = array();
    $session = get_session_by_id($_POST['session_id']);
    $reponse['dates'] = $session->dates_texte;
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_update_exe_comptable', 'update_exe_comptable');
function update_exe_comptable()
{
    global $wpof;
    $reponse = array('message' => '');
    
    if (isset($_POST['stagiaire_id']) && (integer) $_POST['stagiaire_id'] > 0)
    {
        $session = new SessionStagiaire($_POST['session_id'], $_POST['stagiaire_id']);
    }
    else
    {
        $session = new SessionFormation($_POST['session_id']);
    }
    
    $total_tarif = $session->tarif_total_chiffre;
    $c_annee = (integer) $_POST['annee'];
    $c_tarif = (float) $_POST['tarif'];
    if ($c_tarif > $total_tarif) $c_tarif = $total_tarif;
    
    $tarif_a_repartir = $total_tarif - $c_tarif;
    
    $session->exe_comptable[$c_annee] = $c_tarif;
    
    if (count($session->exe_comptable) == 2)
        foreach($session->exe_comptable as $a => $t)
            if ($a != $c_annee)
                $session->exe_comptable[$a] = $tarif_a_repartir;
    
    $difference = array_sum($session->exe_comptable) - $total_tarif;

    if ($difference != 0)
        $reponse['message'] = "<span class='erreur'>".__("Il y a une différence de ").$difference." ".$wpof->monnaie_symbole."</span>";
    
    $reponse['inputs'] = $session->get_input_exe_comptable();
    
    $session->update_meta("exe_comptable");
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_quiz_add_line', 'quiz_add_line');
function quiz_add_line()
{
    $reponse = array();
    
    $quiz = new Quiz($_POST['quiz_id']);
    $reponse['html'] = $quiz->get_edit_line("", $_POST['type']);
    $reponse['log'] = var_export($_POST, true);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_quiz_enregistrer', 'quiz_enregistrer');
function quiz_enregistrer()
{
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    if (!empty($_POST['quiz_id']))
        $quiz = new Quiz($_POST['quiz_id']);
    else
    {
        $quiz = new Quiz();
        $quiz->quiz_id = $quiz->last_quiz_id() + 1;
        $quiz->set_identite($_POST['sujet'], -1);
    }
    
    $quiz->new_parent_id = $_POST['parent_id'];
    $nb_questions = 0;
    if (isset($_POST['questions']))
    {
        $quiz->parse_list($_POST['questions']);
        $nb_questions = $quiz->update_questions();
    }
    if ($nb_questions == 0)
    {
        $quiz->quiz_id = 0;
        update_post_meta($quiz->new_parent_id, "{$quiz->namecode}_id", 0);
    }
    
    $reponse['quiz_id'] = $quiz->quiz_id; // le quiz_id peut changer suite à mise à jour, il faut le renvoyer
    $reponse['log'] = var_export($quiz->questions, true);
    
    echo json_encode($reponse);
    die();
}



add_action('wp_ajax_nopriv_set_default_tab', 'set_default_tab');
add_action('wp_ajax_set_default_tab', 'set_default_tab');
function set_default_tab()
{
    if (!session_id())
        @session_start();
    $_SESSION[$_POST['tab_name']] = $_POST['tab_id'] - 1;
    
    die();
}

add_action('wp_ajax_sql_session_formation', 'sql_session_formation');
function sql_session_formation()
{
    global $wpdb;
    
    $result = array();
    
    if (isset($_POST['stagiaire_id']) && (integer) $_POST['stagiaire_id'] > 1)
    {
        global $suffix_session_stagiaire;
        
        $query = $wpdb->prepare
        (
            "select meta_key, meta_value from ".$wpdb->prefix.$suffix_session_stagiaire." where session_id = '%d' and user_id = '%d';",
            $_POST['session_id'],
            $_POST['stagiaire_id']
        );
        
        $meta = $wpdb->get_results($query, ARRAY_A);
        foreach ($meta as $m)
            $result[$m['meta_key']] = $m['meta_value'];
        echo "<p>$query</p>";
        
        $objet = get_stagiaire_by_id($_POST['session_id'], $_POST['stagiaire_id']);
    }
    elseif (isset($_POST['client_id']) && (integer) $_POST['client_id'] > 1)
    {
        global $suffix_client;
        
        $query = $wpdb->prepare
        (
            "select meta_key, meta_value from ".$wpdb->prefix.$suffix_client." where session_id = '%d' and client_id = '%d';",
            $_POST['session_id'],
            $_POST['client_id']
        );
        
        $meta = $wpdb->get_results($query, ARRAY_A);
        foreach ($meta as $m)
            $result[$m['meta_key']] = $m['meta_value'];
        echo "<p>$query</p>";
        
        $objet = get_client_by_id($_POST['session_id'], $_POST['client_id']);
    }
    else
    {
        $query = $wpdb->prepare
        (
            "select * from ".$wpdb->prefix."posts where ID = '%d';",
            $_POST['session_id']
        );
        
        $result = $wpdb->get_results($query, ARRAY_A);
        $result = $result[0];
        echo "<p>$query</p>";
        
        $query = $wpdb->prepare
        (
            "select meta_key, meta_value from ".$wpdb->prefix."postmeta where post_id = '%d';",
            $_POST['session_id']
        );
        
        $meta = $wpdb->get_results($query, ARRAY_A);
        foreach ($meta as $m)
            $result[$m['meta_key']] = $m['meta_value'];
        echo "<p>$query</p>";
        
        $objet = get_session_by_id($_POST['session_id']);
    }
    
    echo "<h2>SQL</h2>";
    echo "<table class='sql'>";
    foreach ($result as $k => $v)
        echo "<tr><td>$k</td><td>$v</td></tr>";
    echo "</table>";
    
    echo "<h2>Objet</h2>";
    echo "<table class='sql'>";
    foreach ((array)$objet as $k => $v)
        echo "<tr><td>$k</td><td>".var_export($v, true)."</td></tr>";
    echo "</table>";
    
    die();
}

//add_filter('the_title', 'wpof_session_title', 10, 2);

function wpof_session_title($title, $id = null) {
    global $wpof;
    $post = get_post($id);
    if (isset($post->post_type) && $post->post_type == 'session' && $session = new SessionFormation($id)) {
        return $title . wp_date(' - d F Y', $session->first_date_timestamp );
    }
    return $title;
}
