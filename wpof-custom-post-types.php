<?php

require_once(wpof_path . "/cpt/cpt-modele.php");
require_once(wpof_path . "/cpt/cpt-formation.php");
require_once(wpof_path . "/cpt/cpt-lieu.php");
require_once(wpof_path . "/cpt/cpt-session-formation.php");

/*
 * Filtre sur the_content
 */
add_filter('the_content', 'wpof_content_modifier', 5);
function wpof_content_modifier($content)
{
    $post_id = get_the_ID();
    if (!empty($post_id))
    {
        global $wpof;
        $post_type = get_post_type($post_id);
   
        if (is_single())
        {
            switch ($post_type)
            {
                case "modele":
                    $content = "";
                    break;
                case "formation":
                    $formation = new Formation($post_id);
                    $content = "";
                    if ($formation->can_edit() && isset($_GET[$wpof->formation_edit_link_suffix]))
                        $content = $formation->get_edit_formation();
                    elseif ($formation->can_edit() || ($formation->acces_public && !$formation->brouillon))
                        $content = $formation->get_content();
                    else
                        $content = __("Cette formation n'est pas publiée au catalogue");
                
                    remove_filter( 'the_content', 'wpautop' );
                    break;
                case "session":
                    $session = new SessionFormation($post_id);
                    $content = $session->get_content();
                    remove_filter( 'the_content', 'wpautop' );
                    break;
            }
        }
        else
        {
            switch ($post_type)
            {
                case "modele":
                    $content = "";
                    break;
                case "formation":
                    $formation = new Formation($post_id);
                    $content = $formation->get_excerpt();
                    break;
                
                case "session":
                    $session = new SessionFormation($post_id);
                    $content = $session->get_excerpt();
                    break;
            }
        }
    }
    $content .= get_message_box();
    if (debug)
        $content .= get_global_log();
    return $content;
}

function session_prive_redirect()
{
    global $wpof, $post;
    if ($post && $post->post_type == "session")
    {
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        $session = get_session_by_id($post->ID);
        
        if ($session->acces_session != "public"
            &&  ($user_id == 0
                || ($role == "um_stagiaire" && !in_array($user_id, $session->inscrits))
                || ($role == "um_formateur-trice" && !in_array($user_id, $session->formateur))
                )
            )
        {
            wp_redirect(get_home_url(), "302", "OPAGA");
            exit();
        }
    }
}
add_action('template_redirect', 'session_prive_redirect');

add_filter('get_edit_post_link', 'opaga_get_edit_link', 20, 3);
function opaga_get_edit_link($link, $post_id, $context)
{
    global $wpof;
    $post = get_post($post_id);
    
    if (in_array($post->post_type, array("formation", "session")))
        $link = get_permalink($post_id)."?".$wpof->formation_edit_link_suffix;
        
    return $link;
}

/*
 * Filtre pour ajouter une classe spécifique dans le body
 * TODO : vérifier la pertinence de ce filtre !
 */
add_filter('body_class', 'wpof_class');
function wpof_class($classes)
{
    global $wpof;
    
    if (is_single())
    {
        $post = get_post();
        
        if (!empty($post))
        {
            if (isset($wpof->no_theme[$post->post_name]))
            {
                $classes[] = $post->post_name;
                $classes[] = 'wpof';
                $classes[] = 'opaga';
            }
            else
                switch ($post->post_type)
                {
                    case 'formation':
                        $formation = new Formation($post->ID);
                        if ($formation->can_edit() && isset($_GET[$wpof->formation_edit_link_suffix]))
                        {
                            $classes[] = 'edit-formation';
                            $classes[] = 'opaga';
                        }
                    case 'session':
                    case 'lieu':
                        $classes[] = 'wpof'; // pour compatibilité
                        $classes[] = 'opaga';
                        break;
                    default:
                        break;
                }
        }
    }
            
    return $classes;
}



?>
