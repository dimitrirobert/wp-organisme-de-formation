<?php
/*
 * wpof-utilisateur.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

$role_nom = array
(
    'um_formateur-trice' => __('Formateur⋅trice'),
    'um_responsable' => __('Responsable de formation'),
    'admin' => __('Administrateur⋅trice'),
);

function shortcode_show_liste_formateur( $atts )
{
    ob_start();

    $no = 12;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    if($paged==1){
      $offset=0;  
    }else {
       $offset= ($paged-1) * $no;
    }

    // Attributes
    $atts = shortcode_atts(
        array
            (
            'cat' => 'all',
            'tag' => 'all',
            'number' => $no,
            'paged' => $paged,
            'offset' => $offset,
            ),
	$atts
	);
	
    the_liste_formateur($atts);
    
    return ob_get_clean();
    //debug_info($formateurs, "formateurs");
}
add_shortcode( 'liste_formateurs', 'shortcode_show_liste_formateur' );

/**
 * Affiche une liste des formateurs que le user a le droit de voir.
 */
function the_liste_formateur($atts = array(), $echo = true)
{
    global $wpof;
    $formateurs = get_formateurs($atts);

    ob_start();

    // On renvoie une simple liste de liens vers les formateurices.
    if (isset($atts['type']) && $atts['type'] === 'list')
    {
        if (count($formateurs) === 1)
        {
            echo current($formateurs)->get_displayname(true);
        }
        else
        {
            echo '<ul>';
            foreach($formateurs as $f)
            {
                echo '<li>';
                echo $f->get_displayname(true);
                echo '</li>';
            }
            echo '</ul>';
        }

    }
    // Liste formatée avec toutes les infos.
    else
    {
        $list_formateurs_classes = apply_filters( 'opaga_utilisateur_list_formateurs_classes', ['list', 'list_formateur'] );
        $formateur_classes = apply_filters( 'opaga_utilisateur_formateur_classes', ['formateur'] );
        $tags_classes = apply_filters( 'opaga_utilisateur_formateur_tag_classes', ['meta-list'] );
    
        ?>
        <div class="<?php echo esc_attr( implode( ' ', $list_formateurs_classes ) ); ?>">
        <?php foreach($formateurs as $f) : ?>
            <article class="<?php echo esc_attr( implode( ' ', $formateur_classes ) ); ?>">
                <?php 
                // Actions avant d'afficher le contenu formateur
                do_action( 'opaga_before_formateur' ); 
                ?>
                <header>
                    <div class="formateur-photo">
                        <?php echo $f->get_permalink($f->get_picture('thumbnail')); ?>
                    </div>
                    <div class="formateur-nom-metas">
                        <h2 class="blog-entry-title entry-title">
                            <?php 
                            $disp_name = $f->get_displayname(false);
                            if ($f->marque != "") 
                            {
                                $disp_name .= " (" . $f->marque . ")";
                            }
                            echo $f->get_permalink($disp_name);
                            ?>
                        </h2>
                        <!-- TODO : tags à mettre en place 
                        <ul class="<?php echo esc_attr( implode( ' ', $tags_classes ) ); ?>">
                            <li class="meta tag">
                                <span class="tag-name fn"><a href="/" title="Titre" rel="tag">Ecologie</a></span>
                            </li>
                            <li class="meta tag">
                                <span class="tag-name fn"><a href="/" title="Titre" rel="tag">Société</a></span>
                            </li>
                            <li class="meta tag">
                                <span class="tag-name fn"><a href="/" title="Titre" rel="tag">Amour</a></span>
                            </li>
                        </ul>
                        -->
                    </div>
                </header>
    
                <div class="excerpt-wrap entry-summary">
                    <h4><?php _e('Ses formations');?></h4>
                    <ul>
                        <?php
                        // TODO mettre un filtre qui permet de trier par priorité : en premier les prochaines sessions, puis celles où il y a le plus de session, etc.
                        $formations = $wpof->get_formations($f->id);
                        if (is_array($formations))
                        {
                            // On ne prend que les 3 premières ou les x premières (si add_filter)
                            $formations = array_slice($formations, 0, apply_filters('opaga_utilisateur_formations_count', 3));
                            
                            foreach ($formations as $formation) 
                            {
                                // Item de formation dans la liste
                                $formation_html = apply_filters('opaga_utilisateur_formation_item', '<li><a href="'. $formation->permalien . '">' . $formation->titre . '</a></li>', $formation);
                                echo $formation_html;
                            }
                        }
                        ?>
                    </ul>
                </div>
    
                <footer>
                    <div class="read-more-wrapper">
                        <?php echo $f->get_permalink(__('Voir le profil')); ?>
                    </div>
                </footer>
                <?php 
                // Actions avant d'afficher le contenu formateur
                do_action( 'opaga_after_formateur' ); 
                ?>
            </article>
        <?php endforeach; ?>
        </div>
        <?php
        if (isset($atts['number']) && $atts['number'] > 0) {
            $total_users_query = new WP_User_Query(get_params_query_formateurs($atts));
            $total_users = $total_users_query->total_users;
            $nb_ut_par_page = $atts['number'];

            if ($total_users > $nb_ut_par_page)
            {
                $current_page = max(1, get_query_var('paged'));
                $links = paginate_links(array(
                    'base'      => get_pagenum_link(1) . '%_%',
                    'format'    => 'page/%#%/',
                    'current'   => $current_page,
                    'total'     => intval($total_users / $nb_ut_par_page) + 1,
                    'prev_next' => true,
                    'show_all'  => false,
                    'type'      => 'list',
                    ));
            
                apply_filters('opaga_utilisateur_pagination', $links);
                echo $links;
            }
        }
    }

    if ($echo)
        echo ob_get_clean();
    else
        return ob_get_clean();
}

function get_params_query_formateurs($atts) {
    $return = array();

    $roles = array('um_formateur-trice', 'um_responsable');

    if (isset($atts['role']))
        $roles = (is_array($atts['role'])) ? $atts['role'] : explode(',', str_replace(' ', '', $atts['role']));
    
    $return['role__in' ] = $roles;

    if (empty($atts['private']))
    {
        $return['meta_query'] = array(
            'relation' => 'AND',
            'acces_clause' => array(
                'key' => 'profil_public', 
                'value' => 1, 
                'compare' => '='
            ),
        );
    }
    return $return;
}

function get_formateurs($atts = array(), $single = false)
{
    global $wpof;
    
    if (in_array('only', array_keys($atts)))
    {
        foreach($atts['only'] as $id)
            $formateur_users[] = get_user_by("id", $id);
    }
    else
    {
        if (!$single)
        {
            $formateur_users = get_users ( $atts + get_params_query_formateurs($atts) );
        }
        else
            $formateur_users[0] = get_user( $atts['p'] );
    }
    
    $formateurs = array();
    
    foreach ($formateur_users as $fu)
        $formateurs[$fu->ID] = new Formateur($fu->ID);
    return $formateurs;
}

function get_opaga_users($role)
{
    $user = array();
    if (!is_array($role))
        $role = array($role);
    
    foreach($role as $r)
        if (in_array($r, array('um_responsable', 'um_formateur-trice')))
            foreach(get_users(array('role__in' => $r)) as $u)
                $user[$u->ID] = new Formateur($u->ID);
    
    return $user;
}

/*
 * La méthode get_role d'Ultimate member renvoie le rôle WP s'il existe et pas le rôle UM
 * Cette fonction vise à palier à ce manque en ne renvoyant QUE les rôles utilisés dans le plugin
 */
function wpof_get_role($user_id)
{
    if ($user_id == 0)
        return false;

    if (user_can($user_id, "um_formateur-trice"))
        return "um_formateur-trice";

    if (user_can($user_id, "um_responsable"))
        return "um_responsable";
    
    if (user_can($user_id, 'manage_options'))
        return "admin";
}

/*
 * Ajouter un utilisateur
 * Analyse des données du formulaire
 */
add_action( 'wp_ajax_add_compte', 'add_compte' );
function add_compte()
{
    global $ultimatemember, $wpof;
    
    $reponse = array('message' => '', 'html' => '');
    
    $user = array();
    $session = null;
    $session_id = 0;
    
    // Analyse des données
    if (isset($_POST['data']))
    {
        $data = json_decode(html_entity_decode(stripslashes($_POST['data'])));
        $user = $data->allusers;
    }
    else
    {
        $user[0] = new stdClass();
        foreach(array('genre', 'firstname', 'lastname', 'email', 'code', 'role') as $field)
            $user[0]->$field = (isset($_POST[$field])) ?? "";
    }
        
    foreach ($user as $u)
    {
        if ($u !== null && $u->firstname != "" && $u->lastname != "" && $u->email != "" )
        {
            $reponse['message'] .= "<p>{$u->firstname} {$u->lastname} ({$u->email}) ";
            $userdata = array
            (
                'first_name' => $u->firstname,
                'last_name' => $u->lastname,
                'user_email' => $u->email,
                'user_login' => sanitize_user( strtolower( str_replace( " ", ".", $u->firstname . " " . $u->lastname ) ) ),
                'user_pass' => NULL
            );
            $user_id = wp_insert_user($userdata);
            if (is_wp_error($user_id))
            {
                $reponse['message'] .= "<span class='erreur'>".$user_id->get_error_message()."</span>";
            }
            else
            {
                $formateur = new Formateur($user_id);

                $wpof->log->log_action(__FUNCTION__, $formateur);

                $formateur->set_role($u->role);
                if (champ_additionnel("genre_stagiaire"))
                    $formateur->update_meta('genre', $u->genre);
                if (champ_additionnel("formateur_code"))
                    $formateur->update_meta('code', $u->code);
                
                $wpof->log->log_action("update_meta", $formateur, "firstname", $u->firstname);
                $wpof->log->log_action("update_meta", $formateur, "lastname", $u->lastname);
                $wpof->log->log_action("update_meta", $formateur, "email", $u->email);
                $wpof->log->log_action("update_meta", $formateur, "role", $u->role);
                    
                //UM()->mail()->send($u->email, 'welcome_email'); // cette action est déjà effectuée par UM
                $reponse['message'] .= "<span class='succes'>".__("compte créé")."</span>";
            }
            $reponse['message'] .= "</p>";
        }
    }
    if ($reponse['message'] == "")
    {
        $reponse['message'] .= "<span class='erreur'>".__("Saisissez un prénom et un nom pour inscrire un⋅e stagiaire")."</span>";
        $reponse['erreur'] = $reponse['message'];
    }
    else
    {
        $reponse['succes'] = $reponse['message'];
        if ($session)
            $reponse['html'] = $session->get_tabs_clients();
    }

    echo json_encode($reponse);

    die();
}

/*
 * Ajouter un utilisateur
 * Affichage du formulaire
 * $session_id : si > 0 alors les stagiaires sont également inscrits à cette session
 */
function get_add_compte_form($session_id = 0, $client_id = 0)
{
    $data_genre = "";
    $data_selectrole = "";
    
    ob_start();
    ?>
    <div id="add_compte">
        <?php echo get_icone_aide("utilisateur_ajout_compte", __("Créer des comptes")); ?>
        <div id="add_compte_form" class="dynamic_form notif-modif">
        
        <table class="add-compte">
        <tbody>
        
        <tr id='0_new-compte' data-num='0' class='new-compte'>
        <?php if (champ_additionnel("genre_formateur")) : 
        $data_genre = "data-genre='1'"; ?>
        <td><select name="0_genre"><option value="Madame">Madame</option><option value="Monsieur">Monsieur</option></select></td>
        <?php endif; ?>
        <td><input type="text" name="0_firstname" placeholder="<?php _e("Prénom"); ?>" /></td>
        <td><input type="text" name="0_lastname" placeholder="<?php _e("Nom"); ?>" /></td>
        <td><input type="text" name="0_email" placeholder="<?php _e("Courriel"); ?>" /></td>
        <?php if (champ_additionnel("formateur_code")) : ?>
        <td><input type="text" name="0_code" placeholder="<?php _e("Code formateur⋅ice"); ?>" /></td>
        <?php endif; ?>
        <?php
        if ($session_id == 0) :
            global $role_nom;
            $role_list = $role_nom;
            unset($role_list['admin']);
            $select_role = select_by_list($role_list, "NUM_role", "um_formateur-trice");
        ?>
        <td><?php echo str_replace("NUM", "0", $select_role); ?></td>
        <?php
            // on doit passer la liste des rôles seulement dans le cas où $session_id vaut 0 (c'est-à-dire si cette boîte est affichée hors d'une session)
            $data_selectrole = "data-selectrole='".json_encode($role_list)."'";
            endif;
        ?>
        <td><span class="icone-bouton" <?php echo $data_selectrole; ?> id="plus"><?php the_wpof_fa_add(); ?></span></td>
        </tr>
        
        </tbody>
        </table>
        
        <input type='hidden' name='action' value='add_compte' />
        
        <p class="bouton submit" id="add-compte-bouton"><?php _e("Ajouter"); ?></p>
        <p class="message"></p>
        </div>
    </div>
    <?php
    return ob_get_clean();
}

// Initialise un tableau global classé par utilisateur avec la liste de ses formations
$anime_formation = array();
function init_anime_formation()
{
    global $anime_formation, $wpof;
    if (!empty($anime_formation))
        return;
    $formations = $wpof->get_actions_id("formation");
    if (!empty($formations))
    {
        foreach($formations as $fid)
        {
            $formateur = get_post_meta($fid, "formateur", true);
            if (is_array($formateur))
                foreach($formateur as $user_id)
                    $anime_formation[$user_id][$fid] = '<a title="'.get_the_title($fid).'" href="'.get_the_permalink($fid).'">'.$fid.'</a>';
        }
    }
}

// Renvoie un tableau classé par utilisateur avec la liste de ses sessions
$anime_session = array();
function init_anime_session()
{
    global $anime_session, $wpof;
    if (!empty($anime_session))
        return;
    $sessions = $wpof->get_actions_id("session");
    if (!empty($sessions))
    {
        foreach($sessions as $sid)
        {
            $formateur = get_post_meta($sid, "formateur", true);
            if (is_array($formateur))
                foreach($formateur as $user_id)
                    // strip_tags nettoie les balises <sup> lorsque la première date est le 1er du mois
                    $anime_session[$user_id][$sid] = '<a title="'.strip_tags(get_the_title($sid)).'" href="'.get_the_permalink($sid).'">'.$sid.'</a>';
        }
    }
}

add_action('admin_bar_menu', 'opaga_admin_menu', 999);
function opaga_admin_menu($wp_admin_bar) {
    global $wpof;

    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);

    if ($user_id == 0) {
        return;
    }

    $args = array(
		'id'    => 'opaga',
		'title' => '<span class="ab-icon dashicons dashicons-welcome-learn-more"></span>Gestion OPAGA',
		'href'  => home_url()."/".$wpof->url_user,
        'meta' => [
            'title' => __( "Gérer les options d'OPAGA" ),
        ]
	);
	$wp_admin_bar->add_node( $args );

	$args = array(
		'id'    => 'opaga-profile',
		'title' => 'Mon tableau de bord',
		'href'  => home_url()."/".$wpof->url_user,
		'parent' => 'opaga',
        'meta' => [
            'title' => __( "Catalogue, agenda, profil" ),
        ]
	);
	$wp_admin_bar->add_node( $args );

    if (in_array($role, array("um_responsable", "admin")))
    {
        $args = array(
            'id'    => 'opaga-gestion',
            'title' => "Gestion de l'organisme",
            'href'  => home_url().'/'.$wpof->url_gestion,
            'parent' => 'opaga',
            'meta' => [
                'title' => __( "Gestion de l'organisme de formation" ),
            ]
        );
        $wp_admin_bar->add_node( $args );
        $args = array(
            'id'    => 'opaga-admin',
            'title' => "Administration d'OPAGA",
            'href'  => admin_url('admin.php?page=wpof'),
            'parent' => 'opaga',
            'meta' => [
                'title' => __( "Options d'administration d'OPAGA (backend WordPress)" ),
            ]
        );
        $wp_admin_bar->add_node( $args );
    
        foreach($wpof->pages_gestion as $id => $title) {
            $args = array(
                'id'    => $id,
                'title' => $title,
                'href'  => home_url().'/'.$wpof->url_gestion.'/'.$id,
                'parent' => 'opaga-gestion',
                'meta' => [
                    'title' => $title,
                ]
            );
            $wp_admin_bar->add_node( $args );
        }
    }
    
	$args = array(
		'id'    => 'opaga-export',
		'title' => 'Exportez des sessions',
		'href'  => home_url().'/'.$wpof->url_export,
		'parent' => 'opaga',
        'meta' => [
            'title' => __( "Exportez des informations de sessions" ),
        ]
	);
	$wp_admin_bar->add_node( $args );

}

function get_switchuser_button(int $user_id, String $title = "", String $redirect_url = "")
{
    global $wpof;
    if (can_usurper())
    {
        $next_role = wpof_get_role($user_id);
        $title = __("Se connecter en tant que ").get_displayname($user_id);
        if (empty($redirect_url))
        {
            if ($next_role == 'um_formateur-trice')
                $url = home_url().'/'.$wpof->url_user;
            else
                $url = home_url().'/'.$wpof->url_gestion;
        }
        else
            $url = $redirect_url;
        
        ob_start();
        ?>
        <span data-userid="<?php echo $user_id; ?>" data-url="<?php echo $url; ?>" class="pointer switch-user" title="<?php echo $title; ?>">
        <?php the_wpof_fa_switch(); ?>
        </span>
        <?php
        return ob_get_clean();
    }
    else
        return "";
}

add_action( 'wp_ajax_switchuser', 'switchuser' );
function switchuser()
{
    if (can_usurper())
    {
        $user_id = $_POST['user_id'];
        $role_dest = wpof_get_role($user_id);
        if ($role_dest && $role_dest != "admin")
            UM()->user()->auto_login($user_id);
    }
    die();
}

add_action( 'wp_ajax_reinit_password', 'reinit_password' );
function reinit_password()
{
    global $wpof;
    if (in_array($wpof->role, $wpof->super_roles))
    {
        $formateur = new Formateur($_POST['user_id']);
        $result = retrieve_password($formateur->data->user_login);
        if ($result)
            $reponse['message'] = '<span class="succes">'.sprintf(__("Un courriel a été envoyé pour réinitilisation de mot de passe à %s (%s)"), $formateur->get_displayname(), $formateur->email).'</span>';
        else
            $reponse['erreur'] = '<span class="erreur">'.implode('<br />', $result->get_error_messages).'</span>';
    }
    else
        $reponse['erreur'] = '<span class="erreur">'.__("Vous n'avez pas les droits requis pour faire cela").'</span>';
    
    echo json_encode($reponse);
    die();
}

function can_usurper($user_id = null)
{
    global $wpof;
    if ($user_id !== null)
        $role = wpof_get_role($user_id);
    else
        $role = $wpof->role;
    
    return ($role == "admin" || (champ_additionnel("responsable_usurpe") && $role == "um_responsable"));
}

function redirect_function($username, $user)
{
    global $wpof;
    
    if (in_array(wpof_get_role($user->ID), $wpof->super_roles))
        wp_redirect(home_url().'/'.$wpof->url_gestion);
    else
        wp_redirect(home_url().'/'.$wpof->url_user);
    exit;
}
add_action('wp_login', 'redirect_function', 10, 2);

?>
