<?php
/*
 * wpof-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


function show_liste_formation( $atts )
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    
    ob_start();
    // Attributes
    $atts = shortcode_atts(
        array
            (
            'h2' => null,
            'h3' => null,
            'h4' => null,
            'only-public' => false,
            'orderby' => 'date',
            'order' => 'DESC',
            'numberposts' => 5,
            'format' => 'html',
            ),
	    $atts
	);
	
    $formations = get_formations($atts);
    
    ?>
    <div id="liste-formation"<?php if (in_array($role, array("admin", "um_responsable"))) echo "class='edit-data'"; ?>>
    <?php if (isset($atts['h2'])) echo "<h2>".$atts['h2']."</h2>"; ?>
    <?php if (isset($atts['h3'])) echo "<h3>".$atts['h3']."</h3>"; ?>
    <?php if (isset($atts['h4'])) echo "<h4>".$atts['h4']."</h4>"; ?>
    
    <?php if (!empty($formations)) : ?>
    
        <?php if (is_array($formations)) : ?>
        <ul>
        <?php foreach(array_rand($formations, $atts['numberposts']) as $fid) : ?>
            <?php $f = get_formation_by_id($fid); ?>
            <li><a href="<?php echo $f->permalien; ?>"><?php echo $f->titre; ?></a>
            </li>
        <?php endforeach; ?>
        </ul>
        <?php else : ?>
        <?php echo $formations; ?>
        <?php endif; ?>
    
    <?php else:
        _e("Aucune formation définie au catalogue");
    endif;
    ?>
    
    </div>
    <?php
    
    return ob_get_clean();
    //debug_info($formations, "formations");
}
add_shortcode( 'liste_formation', 'show_liste_formation' );


function get_formations($atts = array())
{
    global $wpof;
    
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    
    $html_output = isset($atts['format']) && $atts['format'] == 'html';
    $log = "";
    
    $formations_id = array();
    if (isset($atts['formation_id']))
        $formations_id[0] = $atts['formation_id'];
    else
    {
        $formations_id = $wpof->get_actions_id("formation");
        if (!(in_array($role, $wpof->super_roles) && $wpof->slug == "gestion"))
        {
            $formations_id = $wpof->filter_id_by_value($formations_id, "formation", "acces_public", 1, "=");
            $formations_id = $wpof->filter_id_by_value($formations_id, "formation", "brouillon", 1, "=", true);
        }
    }
    
    $heading = 'h' . (isset($atts['heading_level']) ? (int)($atts['heading_level']) : 2);
    
    $formations = array();
    
    foreach ($formations_id as $fid)
    {
        $fo = new Formation($fid);
        
        if ($html_output)
        {
            $article =                     
                '<article class="formation-detail">
                    <' . $heading . '><a href="' . $fo->permalien . '">' . $fo->titre . '</a></' . $heading . '>
                    <div class="excerpt-wrap entry-summary">
                        ' . wp_trim_words($fo->get_excerpt(), 30, ' &hellip;' ) . '
                        <div class="read-more-wrapper">
                            <a href="' . $fo->permalien . '" class="readmore" rel="bookmark">' . __('Voir la formation →') . '<span class="screen-reader-text">' . $fo->titre . '</span></a>
                        </div>
                    </div>
                </article>';

            $formations[$fo->id] = apply_filters('opaga_formation_item_html', $article, $fo);
        }

        else
            $formations[$fo->id] = $fo;
    }
    
    if (count($formations) == 0)
        return null;
    
    if (! isset($atts['orderby']))
        ksort($formations);

    if ($html_output)
        return apply_filters('opaga_formations_html', '<div class="posts-wrapper">'.implode($formations).'</div>', $formations);
    else
        return $formations;
}

function show_calendrier($atts)
{
    ob_start();
    // Attributes
    $atts = shortcode_atts(
        array
            (
            'ville' => null,
            'formateur' => null,
            'presentation' => null,
            'sort' => "DESC",
            'passe' => false,
            'futur' => true,
            'h2' => null,
            'h3' => null,
            'h4' => null,
            ),
	$atts
	);
    
    $param = array();
    if ($atts['passe'] && !$atts['futur'])
        $param['quand'] = "passe";
    if (!$atts['passe'] && $atts['futur'])
        $param['quand'] = "futur";

    $param['sort'] = $atts['sort'];
    
    $sessions = get_formation_sessions($param);
    global $wpof;
    
    //debug_info($sessions, "sessions");
    
    if ($sessions) :
    
    ?>
    
    <div id="calendrier">
    <?php if (isset($atts['h2'])) echo "<h2>".$atts['h2']."</h2>"; ?>
    <?php if (isset($atts['h3'])) echo "<h3>".$atts['h3']."</h3>"; ?>
    <?php if (isset($atts['h4'])) echo "<h4>".$atts['h4']."</h4>"; ?>
    
    <ul>
    <?php foreach($sessions as $k => $s): ?>
    
    <li class="calendrier">
    <?php if (!empty($s->first_date)) : ?>
    <a href="<?php echo $s->permalien; ?>">
    <span class="dates"><?php echo $s->dates_texte; ?></span>
    </a>
    <?php else : ?>
    <span class="dates"><?php _e("Pas de date"); ?></span>
    <?php endif; ?>
    <?php if ($s->acces_session == "invite" || $s->acces_session == "connecte") : ?>
        <span class="visibilite <?php echo $s->acces_session; ?>">
        <?php echo $wpof->acces_session->get_term($s->acces_session); ?>
        </span>
    <?php endif; ?>
    <a href="<?php echo $s->permalien; ?>">
    <span class="formation"><?php echo $s->titre_formation; ?></span>
    </a>
    <?php if ($atts['ville']) echo "<span class='ville'>".$s->lieu_ville."</span>"; ?>
    </li>
    
    <?php endforeach; ?>
    </ul>
    
    </div>
    
    <?php
    else:
    
    echo "<p>".__("Pas de session programmée pour l'instant")."</p>";
    
    endif;
    return ob_get_clean();
}
add_shortcode( 'calendrier', 'show_calendrier' );


require_once(wpof_path . "class/class-session-formation.php");
require_once(wpof_path . "wpof-utilisateur.php");

function get_formation_sessions($atts = array())
{
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    $meta_queries = array();
    $html_output = isset($atts['format']) && $atts['format'] == 'html';
    
    if (isset($atts['formation_id']))
        $meta_queries[] = array('key' => 'formation', 'value' => $atts['formation_id']);
    
    if ($user_id <= 0)
        $meta_queries[] = array('key' => 'acces_session', 'value' => 'public', 'compare' => 'IN');

    $posts_per_page = isset($atts['posts_per_page']) ? int($atts['posts_per_page']) : -1;
    $formation_session_posts = get_posts(array('post_type' => 'session', 'meta_query' => $meta_queries, 'posts_per_page' => $posts_per_page));
    
    $heading = 'h' . (isset($atts['heading_level']) ? (int)($atts['heading_level']) : 2);

    $formation_sessions = array();

    // gestion du cas où la session n'a pas encore de date
    $nodate = time() + 1000;
    foreach ($formation_session_posts as $fs)
    {
        $s = new SessionFormation($fs->ID);
        $today = time();
        
        $ts = (empty($s->first_date_timestamp)) ? $nodate++ : $s->first_date_timestamp;
        while (isset($formation_sessions[$ts]))
            $ts += 1;
        
        if ((!isset($atts['quand']) || ($atts['quand'] == "futur" && $ts >= $today) || ($atts['quand'] == "passe" && $ts < $today))
            && ((isset($atts['formateur']) && in_array($atts['formateur'], $s->formateur)) || !isset($atts['formateur'])))
        {
            if ($html_output)
            {
                $excerpt = $s->get_excerpt();
                $article = get_article('session-detail', $heading, $s->permalien, $s->titre_formation, $excerpt, ($ts >= $today) ? __("S'inscrire") : "");

                $formation_sessions[$ts] = apply_filters('opaga_session_item_html', $article, $s);
            }
            else
                $formation_sessions[$ts] = $s;
        }
        unset($s);
    }
    
    if (count($formation_sessions) == 0)
        return null;
    
    if (isset($atts['sort']) && $atts['sort'] == "ASC")
        ksort($formation_sessions);
    else
        krsort($formation_sessions);

    if ($html_output)
        return apply_filters('opaga_sessions_html', '<div class="posts-wrapper">'.join($formation_sessions).'</div>', $formation_sessions);
    else
        return $formation_sessions;
}


function get_article($classe, $titrage, $lien, $titre, $excerpt, $readmore_txt)
{
    $article =                     
        '<article class="' . $classe . '">
            <' . $titrage . '><a href="' . $lien . '">' . $titre . '</a></' . $titrage . '>
            <div class="excerpt-wrap entry-summary">
                ' . $excerpt . '
                <div class="read-more-wrapper">
                    <a href="' . $lien . '" class="readmore" rel="bookmark">' . $readmore_txt . '<span class="screen-reader-text">' . $titre . '</span></a>
                </div>
            </div>
        </article>';

    return $article;
}

?>
