<?php
/*
 * wpof-first-init.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 

// fixe le tableau global Client en fonction du tableau global SessionFormation
function select_clients_from_db()
{
    global $Client;
    if (!empty($_SESSION['Client']))
        $Client = $_SESSION['Client'];
    else
    {
        global $wpdb, $wpof, $suffix_client, $SessionFormation;
        
        $query = "SELECT DISTINCT client_id, session_id FROM ".$wpdb->prefix.$suffix_client." WHERE session_id IN ('".implode("','", array_keys($SessionFormation))."') ORDER BY session_id;";
        $client_tab_id = $wpdb->get_results($query);
        
        foreach($client_tab_id as $o)
            $Client[$o->client_id] = get_client_by_id($o->session_id, $o->client_id);
        $_SESSION['Client'] = $Client;
    }
}

// fixe le tableau global SessionStagiaire en fonction du tableau global SessionFormation
function select_stagiaires_from_db()
{
    global $SessionStagiaire;
    if (!empty($_SESSION['Stagiaire']))
        $SessionStagiaire = $_SESSION['Stagiaire'];
    else
    {
        global $wpdb, $wpof, $suffix_session_stagiaire, $SessionFormation;
        
        $query = "SELECT DISTINCT stagiaire_id, session_id FROM ".$wpdb->prefix.$suffix_session_stagiaire." WHERE session_id IN ('".implode("','", array_keys($SessionFormation))."') ORDER BY session_id;";
        $stagiaire_tab_id = $wpdb->get_results($query);
        
        foreach($stagiaire_tab_id as $o)
            $SessionStagiaire[$o->stagiaire_id] = get_stagiaire_by_id($o->session_id, $o->stagiaire_id);
        $_SESSION['Stagiaire'] = $SessionStagiaire;
    }
}


/*
 * Interrogation de la base de données pour extraire une meta_key depuis toutes les occurrences de l'entité
 * Renvoi un tableau indexé par les indices de l'entité et contenant les meta_value
 * Entités gérées :
 * * SessionFormation, session
 * * Client, client
 * * SessionStagiaire, stagiaire
 */ 
function get_raw_data($entite, $meta_key)
{
    global $wpdb;
    $result = array();
    $query = null;
    switch($entite)
    {
        case "SessionFormation":
        case "session":
            $query = $wpdb->prepare("SELECT post_id, meta_value FROM ".$wpdb->prefix."postmeta WHERE meta_key = '%s';", $meta_key);
            break;
        case "Client":
        case "client":
            global $suffix_client;
            $query = $wpdb->prepare("SELECT client_id, meta_value FROM ".$wpdb->prefix.$suffix_client." WHERE meta_key = '%s';", $meta_key);
            break;
        case "SessionStagiaire":
        case "stagiaire":
            break;
        default:
            break;
    }
    if ($query)
    {
        $select = $wpdb->get_results($query);
        if (is_array($select))
            foreach($select as $row)
            {
                $meta_value = unserialize($row->meta_value);
                if ($meta_value)
                    $result[$row->post_id] = $meta_value;
                else
                    $result[$row->post_id] = $row->meta_value;
            }
    }
    return $result;
}

add_action('wp_ajax_manage_random_stagiaire', 'manage_random_stagiaire');
function manage_random_stagiaire()
{
    $reponse = array('log' => array());
    $session = new SessionFormation($_POST['session_id']);
    $client = new Client($_POST['session_id'], $_POST['client_id']);
//    $client->init_stagiaires();
    
    $nb_stagiaire_actuel = count($client->stagiaires);
    $nb_stagiaire_futur = $_POST['meta_value'];
    
    if ($nb_stagiaire_futur > $nb_stagiaire_actuel)
    {
        require_once(wpof_path . "/wpof-extra-config.php");
        
        for($i = 0; $i < $nb_stagiaire_futur - $nb_stagiaire_actuel; $i++)
        {
            $identite = $faux_nom[rand(0, count($faux_nom) - 1)];
            $userdata = array
            (
                'first_name' => $identite['prenom'],
                'last_name' => $identite['nom'],
                'user_email' => sanitize_user(strtolower($identite['prenom'].$identite['nom']))."@".date_i18n("Ymjis")."fausseadres.se",
                'user_login' => sanitize_user(strtolower($identite['prenom'].$identite['nom'])),
                'user_pass' => NULL
            );
            
            $reponse['log'][] = $userdata;
            
            $user_id = wp_insert_user($userdata);
            $client->stagiaires[] = $user_id;
            $client->update_meta("stagiaires");
            $session->inscrits[] = $user_id;
            $session->update_meta("inscrits");
        }
    }
    
    $reponse['html'] = get_pilote_stagiaires($client);
    
    echo json_encode($reponse);
    die();
}

?>
