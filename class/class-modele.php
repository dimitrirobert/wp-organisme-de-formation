<?php
/*
 * class-modele.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// Inclure HtmlDomParser installé via composer
require_once wpof_path . '/vendor/autoload.php';
use voku\helper\HtmlDomParser;

class Modele
{
    public $desc = ""; // titre du post WP
    public $titre = ""; // titre du document tel qu'il apparait dans le PDF
    public $slug = "";
    public $contenu = "";
    
    public $contexte = 0;
    public $signature = 0;

    public $is_contrat = false;
    
    public $orientation = "portrait";
    
    public $ckw = array(); // custom keywords
    
    public $id;
    
    public function __construct($modele_id = -1)
    {
        $this->id = $modele_id;
        
        if ($modele_id > 0)
        {
            $data = get_post($modele_id);
            $meta = get_post_meta($modele_id);
            
            // infos issues du post
            $this->desc = $data->post_title;
            $this->slug = $data->post_name;
            $this->contenu = $data->post_content;
            
            // metadonnées
            foreach($meta as $key => $val)
            {
                if (substr($key, 0, 4) == "ckw_")
                    $this->ckw[substr($key, 4)] = json_decode(stripslashes($val[0]));
                elseif (substr($key, 0, 1) != "_")
                    $this->$key = $val[0];
            }
            
            if (empty($this->titre))
                $this->titre = $this->desc;
        }
    }
    
    public function get_keywords()
    {
        $content_html = HtmlDomParser::str_get_html($this->contenu);
        $keywords = array();
        
        foreach($content_html->findMulti('.keyword') as $var)
            $keywords[] = $var->innertext;
        
        return $keywords;
    }
    
    public function contains_keyword($keyword)
    {
        return in_array($keyword, $this->get_keywords());
    }
    
    // Insertion d'un nouveau post dans la base
    public function wp_insert()
    {
        $data = array
        (
            'post_title' => $this->desc,
            'post_type' => 'modele',
            'post_name' => $this->slug,
            'post_content' => $this->contenu,
            'post_status' => "publish",
        );
        
        remove_action('save_post','save_modele_metaboxes');
        $id = wp_insert_post($data, true);
        
        if (is_wp_error($id))
        {
            $reponse['log'] = __("Erreur création modèle")." ".$this->desc." ".$id->get_error_message();
            return $reponse;
        }
        
        unset($this->desc);
        unset($this->slug);
        unset($this->contenu);
        $props = get_object_vars($this);
        unset($props['id']);
        
        if (isset($props['ckw']))
            $props = array_merge($props, $this->import_ckw($props['ckw']));
        unset($props['ckw']);
        
        foreach($props as $key => $val)
            update_post_meta($id, $key, $val);

        $this->id = $id;
    }
    
    private function import_ckw($ckw_obj)
    {
        $ckw = array();
        if (is_object($ckw_obj))
        {
            foreach((array) $ckw_obj as $key => $val)
                $ckw["ckw_".$key] = json_encode($val, JSON_UNESCAPED_UNICODE);
        }
        return $ckw;
    }
}

function get_modele_by_id($id)
{
    global $Modele;
    
    if (!isset($Modele[$id]))
        $Modele[$id] = new Modele($id);
        
    return $Modele[$id];
}

function get_modele_by_slug($slug)
{
    global $wpdb;
    $query = $wpdb->prepare("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type = 'modele' AND post_status = 'publish' AND post_name = '%s';", $slug);
    $modele_id = $wpdb->get_var($query);
    return get_modele_by_id($modele_id);
}

function get_all_modeles()
{
    global $Modele;
    global $wpdb;
    
    $modele_id = $wpdb->get_col("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type = 'modele' AND post_status = 'publish';", 0);
    foreach($modele_id as $id)
        $Modele[$id] = new Modele($id);
}

add_action('wp_ajax_clean_modele', 'clean_modele');
function clean_modele($content = null)
{
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
        $content = stripslashes($_POST['content']);
    
    $html = preg_replace('/\{([a-z]+:[^}]+)\}/', '<span class="keyword">$1</span>', $content);
    
    $document_html = HtmlDomParser::str_get_html($html);
    
    foreach($document_html->findMulti('.keyword .keyword') as $var)
        $var->outertext = $var->innertext;
    
    foreach($document_html->findMulti('.keyword') as $var)
    {
        $var->innertext = trim($var->innertext);
        $class = $var->getAttribute("class");
        if (substr_count($class, "locked") == 0)
            $var->setAttribute("class", $class." locked");
    }
        
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        echo $document_html->save();
        die();
    }
    else
        return $document_html->save();
}

function check_custom_keywords($content)
{
    global $post, $wpof;
    
    $document_html = HtmlDomParser::str_get_html($content);
    
    foreach($document_html->findMulti('.custom_keyword') as $var)
    {
        $var->innertext = trim($var->innertext);
        $vars = array_map('trim', explode(':', $var->innertext));
        if (count($vars) == 1)
            $vars[1] = $vars[0];
        if ($vars[0] != "document")
            $vars[0] = "document";
        if ($vars[1] != "personnalisable")
        {
            $var->class = str_replace('custom_keyword', '', $var->class);
            $var->class = str_replace('erreur', '', $var->class);
            $vars[1] = str_replace('-', '_', sanitize_title($vars[1]));
            update_post_meta($post->ID, "ckw_".$vars[1], str_replace('KEYWORD', $vars[1], $wpof->custom_keyword->template));
            $var->innertext = join(":", $vars);
        }
        else
            $var->class .= " erreur";
    }
    return $document_html->save();
}

add_action('wp_ajax_ckw_supprime', 'ckw_supprime');
function ckw_supprime()
{
    $reponse = array();
    
    $meta_key = "ckw_".$_POST['ckw'];
    if (delete_post_meta($_POST['modele_id'], $meta_key) === false)
        $reponse["erreur"] = __("Impossible de supprimer ce mot-clé (delete_post_meta)");
    
    echo json_encode($reponse);
    die();
}
