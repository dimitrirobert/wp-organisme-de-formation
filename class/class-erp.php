<?php
/*
 * class-module.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

abstract class ERP
{
  protected int $limit = 0;
  public bool $enabled = true;

  public function set_limit(int $limit): void
  {
    $this->limit = $limit;
  }

  public function get_limit(): int
  {
    return $this->limit;
  }

  /**
   * Indique si cette classe peut renvoyer la liste des clients ou non
   */
  abstract public function canGetClients(): bool;

  /**
   * Renvoie la liste des clients que le.a formateurice a le droit d'inscrire pour ses formations
   * @param Formateur $formateurice comme son nom l'indique
   * @param string $entite_client : type de client, personne morale ou physique
   * @return array un tableau avec en index l'identifiant du client et en valeur le nom.
   */
  abstract public function getClients(Formateur $formateurice, string $entite_client = null): array;

  /**
   * Renvoie un lien ou bouton cliquable qui permet à un.e formateurice de créer un.e nouvelleau client.
   * @param SessionFormation $session : la session pour laquelle on crée le client
   * @param string $msg : le message à mettre dans le lien/bouton
   * @return string le code html avec tout ce qu'il faut pour accéder à la création d'un.e nouvelleau client.e.
   */
  abstract public function getCreateClientUrl(SessionFormation $session, string $msg = 'Créez le !'): string;

  /**
   * Est-ce qu'on peut faire un lien de facturation entre Opaga et l'ERP ? 
   * 
   * @param Client $client : le client pour lequel on affiche ou pas une facture
   * 
   * @return bool true si oui. Dans ce cas getCreateFactureUrl() doit renvoyer le lien en question.
   */
  abstract public function haveFactureLink(Client $client): bool;

  /**
   * Renvoie le bouton de connexion d'un client déjà saisi avec son external_id dans la base distante
   * 
   * @param int $session_id
   * @param int $client_id : id du client dans OPAGA
   * 
   * @return string tag HTML du bouton
   */
  abstract public function getConnectionButton(int $session_id, int $client_id): String;

  /**
   * Renvoie l'url qui permet à un.e formateurice de créer un.e nouvelleau facture pour un client.
   * Utilisé uniquement si haveFactureLink() est true.
   *
   * @param Client $client : le client pour lequel on crée une facture
   * @param SessionFormation $session : la session pour laquelle on crée la facture
   * @param int $total_ttc : Le total à facturer en TTC
   * @param bool $is_acompte : Est-ce que c'est l'acompte
   * @param bool $is_solde : Est-ce que c'est le solde
   * @param array $http_params : tableau des params http clé => valeur
   * @return string l'url. False si l'ERP ne permet pas de facturer.
   */
  abstract public function getCreateFactureUrl(Client $client, SessionFormation $session, float $total, bool $is_acompte, bool $is_solde = false, array $http_params = []): string|bool;

  /**
   * Tableau des factures d'acompte déjà facturées au client. De la forme :
   * [
   *   'montant_total_ht'  => 0,
   *   'montant_total_ttc' => 0,
   *   'monnaie'           => '€',
   *   'factures'          => [
   *     'num_facture' => '',
   *     'description' => '',
   *     'montant_ht'  => 0,
   *     'montant_ttc' => 0,
   *    ]
   * ]
   *
   * Utilisé uniquement si haveFactureLink() est true.
   *
   * @param Client $client
   * @param SessionFormation $session
   * @param bool $is_acompte : true si on récupère la liste des factures d'acompte (ou facture intemédiaire) false si on récupère la facture finale.
   * @return array|bool : Le tableau des factures, et des montants totaux ou false si haveFactureLink() est false.
   */
  abstract public function getFacturesClient(Client $client, SessionFormation $session, bool $is_acompte = true): array|bool;

  /**
   * URL pour accéder aux factures déjà effectuées.
   * Utilisé uniquement si haveFactureLink() est true.
   * 
   * @param Client $client : le client pour lequel on veut les précédentes factures
   * @param SessionFormation $session : la session concernée
   * @param bool $is_acompte : true si on récupère la liste des factures d'acompte (ou facture intemédiaire) false si on récupère la facture finale.
   * @return string|bool l'url pour accéder aux factures ou false s'il n'y en a pas.
   */
  abstract public function getFacturesClientUrl(Client $client, SessionFormation $session, bool $is_acompte = true): string|bool;

  /**
   * Renvoie le nom (ou la raison sociale) et/ou le prenom du client.
   * @param string $client_id l'identifiant du client tel que renvoyé par la fonction getClients()
   * @return array Un tableau avec les clés suivantes : ['nom', 'prenom', 'adresse', 'code_postal', 'ville', 'pays', 'telephone', 'email', 'siret', 'tva']
   * Les veleurs du tab leau sont à false s'il n'y a rien à renseigner.
   */
  abstract public function getClientInfos(string $client_id): array;

  abstract public function createContrat(Contrat $contrat);

}