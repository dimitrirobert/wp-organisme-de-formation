<?php
/*
 * class-document.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// Inclure dompdf, HtmlDomParser installés via composer
require_once wpof_path . '/vendor/autoload.php';
// Reference the Dompdf namespace 
use Dompdf\Dompdf; 
// Reference the Options namespace 
use Dompdf\Options; 
// Reference the Font Metrics namespace 
use Dompdf\FontMetrics;

use voku\helper\HtmlDomParser;

define( 'wpof_url_pdf', WP_CONTENT_URL . "/uploads/pdf");
define( 'wpof_path_pdf', WP_CONTENT_DIR . "/uploads/pdf");

class Document
{
    /* Valeurs pour l'état de validation (signature) de chaque document
     * NEED : le document devra être signé
     * REQUEST : un message est envoyé pour que la personne signe ce document
     * DONE : le document est signé
     * Attention : REQUEST doit disparaître lorsque DONE apparaît !
     */
    const VALID_STAGIAIRE_NEED = 1;
    const VALID_STAGIAIRE_REQUEST = 2;
    const VALID_STAGIAIRE_DONE = 4;
    const VALID_CLIENT_NEED = 8;
    const VALID_CLIENT_REQUEST = 16;
    const VALID_CLIENT_DONE = 32;
    const VALID_RESPONSABLE_NEED = 64;
    const VALID_RESPONSABLE_REQUEST = 128;
    const VALID_RESPONSABLE_DONE = 256;
    const VALID_FORMATEUR_NEED = 2048;
    const VALID_FORMATEUR_REQUEST = 4096;
    const VALID_FORMATEUR_DONE = 8192;
    const VALID_ALL_NEED = Document::VALID_CLIENT_NEED | Document::VALID_STAGIAIRE_NEED | Document::VALID_RESPONSABLE_NEED | Document::VALID_FORMATEUR_NEED;
    const VALID_ALL_REQUEST = Document::VALID_CLIENT_REQUEST | Document::VALID_STAGIAIRE_REQUEST | Document::VALID_RESPONSABLE_REQUEST | Document::VALID_FORMATEUR_REQUEST;
    const VALID_ALL_DONE = Document::VALID_CLIENT_DONE | Document::VALID_STAGIAIRE_DONE | Document::VALID_RESPONSABLE_DONE | Document::VALID_FORMATEUR_DONE;
    const DRAFT = 512;
    const SCAN = 1024;
    
    // qui doit signer et est-ce que le document est signé (utilise les constantes ci-dessus)
    public $valid = 0;
    
    
    /* Valeurs pour les colonnes du tableau gestion-docs-admin
    */
    const COL_ALL = 0xFFFF;
    const COL_DOCUMENT = 1;
    const COL_DRAFT = 2;
    const COL_FINAL = 4;
    const COL_REQUEST = 8;
    const COL_DIFFUSER = 16;
    const COL_SCAN = 32;
    const COL_SUPPRIMER = 64;
    const COL_NOM_ENTITE = 128;
    const COL_REJETER = 256;
    const COL_VALIDER = 512;

    // type de document : convention, ri, attestation de suivi, feuille d'émargement, etc.
    public $type = "";
    
    // chemin et nom de fichiers
    public $path = wpof_path_pdf;
    public $url = wpof_url_pdf;
    public $base_filename = "";
    public $html_filename = "";
    public $pdf_filename = "";
    public $text_name = "";
    public $link_name = "";
    
    // contexte du document
    private $contexte_nom = "";
    private $contexte_nom_lien = "";
    private $contexte_lien = "";
    
    // le document est-il diffusé au stagiaire
    public $visible = false;
    
    // date de dernière modification
    public $last_modif = 0;
    
    // est-ce que le document a une plage de validité ?
    // si false, on utilise la date du jour
    public $date_debut = "";
    public $date_fin = "";
    
    // entité concernée
    public $stagiaire_id = null;
    public $client_id = null;
    public $array_formateur_id = array();
    
    // la personne qui crée le document, peut-elle le signer ?
    public $signataire = false;
    
    // session de formation concernée
    public $session_formation_id = null;
    
    // modèle
    public $modele;
    // le modèle de ce document a combien de mots-clés personnalisés (Custom KeyWords) ?
    public $ckw = 0;
    public $ckw_ok = true; // les mots-clés sont-ils définis pour ce document ?
    
    public $titre = "";
    public $desc = "";
    
    public $infos_manquantes = array();
    
    // table suffix
    private $table_suffix = WPOF_TABLE_SUFFIX_DOCUMENTS;
    
    // objet DomPDF
    private $dompdf = null;
    private $pdf_opt = array();
    public bool $onthefly = false;
    
    // l'id d'un document est de la forme : type-<session_id> ou type-[csf]<contexte_id>
    // c → client, s → stagiaire, f → formateur
    public $id;
    
    public function __construct($type = null, $session_formation_id = -1, $contexte = 0, $contexte_id = -1)
    {
        global $wpdb, $wpof, $Client, $SessionStagiaire;
        
        if ($type)
        {
            $table_documents = $wpdb->prefix.$this->table_suffix;
            
            $this->session_formation_id = $session_formation_id;
            $this->type = $type;
            $this->contexte = $contexte;
            $this->contexte_id = ($contexte_id == -1) ? $session_formation_id : $contexte_id;
            $this->text_name = $wpof->documents->get_term($type);
            $this->path .= "/$session_formation_id"; // chaque session a son dossier (TODO : qui pourrait être créé dès la création de la session, voir dans cpt-session-formation)
            $this->url .= "/$session_formation_id";
            $this->base_filename = "$type";
            $this->modele = get_modele_by_slug($this->type);
            if ($this->modele->id > 0)
            {
                $this->titre = $this->modele->titre;
                $this->desc = $this->modele->desc;
                $this->ckw = count($this->modele->ckw);
            }
            elseif ($wpof->documents->is_term($this->type))
            {
                $this->desc = $this->titre = $wpof->documents->get_term($this->type);
            }
            else
            {
                $this->titre = __("pas de titre");
                $this->desc = __("pas de description");
            }
            
            if ($session_formation_id > 0)
            {
                $this->session = get_session_by_id($session_formation_id);
                $this->array_formateur_id = $this->session->formateur;
            }
            elseif ($contexte & $wpof->doc_context->formation)
            {
                $this->formation = get_formation_by_id($this->contexte_id);
                $this->array_formateur_id = $this->formation->formateur;
            }
            
            $this->contexte_nom = "";
            $this->contexte_nom_lien= "";
            switch ($contexte & $wpof->doc_context->entite)
            {
                case $wpof->doc_context->session:
                    $this->id = "{$this->contexte_id}-$type";
                    $this->contexte_nom = $this->session->titre_session;
                    $this->contexte_nom_lien = $this->session->get_a_link($this->contexte_nom);
                    $this->contexte_lien = $this->session->permalien."/session#gestion-docs-admin";
                    break;
                case $wpof->doc_context->client:
                    $this->client_id = $this->contexte_id;
                    $client = get_client_by_id($this->session_formation_id, $this->client_id);
                    if ($client === null)
                        echo "Session ".$this->session_formation_id." / Client ".$this->client_id;
                    $this->contexte_nom = $client->get_displayname(false);
                    $this->contexte_nom_lien = $client->get_displayname(true);
                    $this->contexte_lien = $client->permalien."#gestion-docs-admin";
                    $this->base_filename .= "-".sanitize_title($client->nom);
                    if ($client->entite_client == "physique")
                    {
                        $this->stagiaire_id = $client->stagiaires[0];
                        if ($this->modele->contexte & ($wpof->doc_context->stagiaire | $wpof->doc_context->physique)) // si le modèle est prévu pour un stagiaire on l'attribue au client
                        {
                            $this->contexte |= $wpof->doc_context->client;
                            $this->contexte &= ~$wpof->doc_context->stagiaire;
                            $this->contexte_id = $this->client_id;
                        }
                    }
                    $this->id = "{$this->session_formation_id}-c{$this->contexte_id}-$type";
                    break;
                case $wpof->doc_context->stagiaire:
                    $this->stagiaire_id = $this->contexte_id;
                    $stagiaire = get_stagiaire_by_id($this->session_formation_id, $this->stagiaire_id);
                    $this->contexte_nom = $stagiaire->get_displayname(false);
                    $this->contexte_nom_lien = $stagiaire->get_displayname(true);
                    $this->contexte_lien = $stagiaire->permalien."#gestion-docs-admin";
                    $this->client_id = $stagiaire->client_id;
                    $this->base_filename .= "-".sanitize_title($stagiaire->get_displayname());
                    $this->id = "{$this->session_formation_id}-c{$this->client_id}-s{$this->contexte_id}-$type";
                    break;
                case $wpof->doc_context->formateur:
                    $this->id = "$type-f{$this->contexte_id}";
                    $this->array_formateur_id[0] = $this->contexte_id;
                    break;
                case $wpof->doc_context->formation:
                    $this->id = "$type-f{$this->contexte_id}";
                    $this->formation_id = $this->contexte_id;
                    break;
            }
            
            $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_documents WHERE session_id = '%d' AND contexte & '%d' AND contexte_id = '%d' AND document = '%s';",
                $this->session_formation_id,
                $this->contexte,
                $this->contexte_id,
                $this->type
                );
            $res = $wpdb->get_results($query, OBJECT);
            
            foreach($res as $row)
            {
                if (isset($this->{$row->meta_key}) && is_array($this->{$row->meta_key}))
                {
                    $array_value = unserialize($row->meta_value);
                    if (is_array($array_value))
                        $this->{$row->meta_key} = array_stripslashes($array_value);
                }
                else
                    $this->{$row->meta_key} = stripslashes($row->meta_value);
            }
            
            if ($this->ckw > 0)
                $this->set_ckw_ok();
            
            if (isset($wpof->documents->term[$this->type]))
                $this->valid |= (integer) $wpof->documents->term[$this->type]->signature;
            
            $this->onthefly = isset($wpof->documents->term[$this->type]->onthefly);
            // si l'utilisateur n'est pas connecté ou si le modèle est déclaré comme nodraft ou si pas de session associée alors on ne fait pas de brouillon (retrait du bit DRAFT)
            if (!is_user_logged_in() || isset($wpof->documents->term[$this->type]->nodraft) || $this->session_formation_id == 0)
            {
                $this->valid &= ~ Document::DRAFT;
                $this->onthefly = true;
            }
            
            $this->path .= ($this->valid & Document::SCAN) ? "/scan" : "";
            $this->url .= ($this->valid & Document::SCAN) ? "/scan" : "";
            
            $this->init_link_name();
        }
    }
    
    public function init_link_name($text = true)
    {
        if ($this->pdf_filename != "")
        {
            $this->href = home_url()."?download={$this->type}&s={$this->session_formation_id}&ci={$this->contexte_id}&c={$this->contexte}";
            if (isset($_GET['t']))
                $this->href .= "&t=".$_GET['t'];
            $this->link_name = '<a href="'.$this->href.'">'.wpof_fa_pdf();
            if ($text)
                $this->link_name .= ' '.__("Télécharger le PDF");
            $this->link_name .= '</a>';
        }
    }
    
    public function get_displayname($link = false)
    {
        $name = $this->text_name;
        if ($link === true)
            $name = '<a href="'.$this->contexte_lien.'"/>'.$name.'</a>';
        
        return $name;
    }
    
    public function get_meta($meta_key)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("SELECT meta_value from $table
            WHERE contexte_id = '%d'
            AND contexte = '%d'
            AND session_id = '%d'
            AND document = '%s'
            AND meta_key = '%s';",
            $this->contexte_id, $this->contexte, $this->session_formation_id, $this->type, $meta_key);
        
        return $wpdb->get_var($query);
    }
    
    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpof;
        
        // mise à jour de l'instance courante
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key, $meta_value);
        
        if (is_array($meta_value))
            $meta_value = serialize($meta_value);

        // mise à jour de la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, contexte, contexte_id, document, meta_key, meta_value)
            VALUES ('%d', '%d', '%d', '%s', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->contexte, $this->contexte_id, $this->type, $meta_key, $meta_value, $meta_value);
        return $wpdb->query($query);
    }
    
    
    // Fixe la propriété ckw_ok a la bonne valeur
    public function set_ckw_ok()
    {
        $this->ckw_ok = true;
        foreach($this->modele->ckw as $key => $ckw)
            if (!empty($ckw->needed) && (!isset($this->$key) || $this->$key == ""))
            {
                $this->ckw_ok = false;
                break;
            }
    }
    
    public function get_contexte_nom($show_type_contexte = false, $link = true)
    {
        $contexte_nom = "";
        if ($this->stagiaire_id > 0)
        {
            $stagiaire = get_stagiaire_by_id($this->session_formation_id, $this->stagiaire_id);
            $contexte_nom = $stagiaire->get_displayname($link);
            if ($show_type_contexte)
                $contexte_nom .= " (".__("stagiaire").")";
        }
        elseif ($this->client_id > 0)
        {
            $client = get_client_by_id($this->session_formation_id, $this->client_id);
            $contexte_nom = $client->get_displayname($link);
            if ($show_type_contexte)
            {
                if ($client->financement == Client::FINANCEMENT_OPAC)
                    $contexte_nom .= " (".__("client donneur d'ordre").")";
                else
                    $contexte_nom .= " (".__("client").")";
            }
        }
        elseif (!empty($this->array_formateur_id))
        {
            $contexte_nom = get_displayname($this->array_formateur_id[0]);
            if ($show_type_contexte)
                $contexte_nom .= " (".__("formateur externe").")";
        }
        return $contexte_nom;
    }
    
    private function get_etat_signatures()
    {
        $signatures = array('ok' => array(), 'todo' => array());
        if ($this->valid & Document::VALID_RESPONSABLE_NEED)
        {
            $sig = "<span class='signature'>".__("Responsable")."</span>";
            if ($this->valid & Document::VALID_RESPONSABLE_DONE)
                $signatures['ok']['resp'] = $sig;
            else
                $signatures['todo']['resp'] = $sig;
            
        }
        if ($this->valid & Document::VALID_CLIENT_NEED)
        {
            $sig = "<span class='signature'>".__("Client")."</span>";
            if ($this->valid & Document::VALID_CLIENT_DONE)
                $signatures['ok']['client'] = $sig;
            else
                $signatures['todo']['client'] = $sig;
        }
        if ($this->valid & Document::VALID_STAGIAIRE_NEED)
        {
            $sig = "<span class='signature'>".__("Stagiaire(s)")."</span>";
            if ($this->valid & Document::VALID_STAGIAIRE_DONE)
                $signatures['ok']['stag'] = $sig;
            else
                $signatures['todo']['stag'] = $sig;
        }
        return $signatures;
    }
    
    public function mark_as_signed()
    {
        global $wpof;
        $reponse = array('message' => '');
        $session_formation = get_session_by_id($this->session_formation_id);
        if (is_signataire() !== false) // dans ce cas, on est dans le tableau de bord responsable et donc on fait disparaître le bouton Rejeter et on envoie un mail aux contacts de la session
        {
            // Suppression du bouton de rejet
            $reponse['remove'] = "tr#tr-{$this->id} .doc-rejeter";
            
            // Envoi du mail aux formateurs concernés
            $subject = "[".$wpof->of_nom." Formation] Document signé : ".$this->titre;
            if (!empty($contexte_nom = $this->get_contexte_nom(true, false)))
                $subject .= " pour ".$contexte_nom;
            $subject .= " pour la session du ".$session_formation->first_date." – ".$session_formation->titre_formation;
            $content_message = "Bonjour,\n\nLe document :\n".$this->titre." a été signé\n";
            $content_message .= "\nPour la session :\n".$session_formation->titre_formation."\n\ndébutant le ".$session_formation->first_date."\n";
            $content_message .= "\nTu peux le télécharger (connexion requise)\n → ".$this->href."\n";
            $content_message .= "\nou le diffuser à ton client depuis la page de session\n → ".$session_formation->permalien."\n";
            $content_message .= "\nCe message est envoyé automatiquement par l'équipe de ".$wpof->of_nom." ce qui n'empêche pas de souhaiter une bonne journée !";
            $message = new Message(array('to' => $session_formation->formateur, 'content' => $content_message, 'subject' => $subject));
            if ($message->sendmail() == false)
                $reponse['message'] .= '<span class="erreur">'.__("L'email n'a pas été envoyé à ").$session_formation->get_formateurs_noms().'</span><br />';
            else
                $reponse['message'] .= '<span class="succes">'.__("Email envoyé à ").$session_formation->get_formateurs_noms().'</span><br />'.$subject;
            
            foreach($session_formation->formateur as $fid)
            {
                $formateur = get_formateur_by_id($fid);
                $formateur->remove_pending_signature($this);
            }
        }
        
        $this->valid |= Document::VALID_RESPONSABLE_DONE;
        $this->valid |= Document::SCAN;
        $this->valid &= ~ Document::VALID_RESPONSABLE_REQUEST;
        $this->update_meta("valid");
        
        return $reponse;
    }
    
    public function get_html_ligne($cols = Document::COL_ALL & ~ (Document::COL_NOM_ENTITE | Document::COL_REJETER | Document::COL_VALIDER))
    {
        global $wpof;
        
        $role = wpof_get_role(get_current_user_id());
        $formation_id = get_post_meta("formation", $this->session_formation_id, true);
        
        // si l'utilisateur est signataire, on n'affiche pas la colonne de demande de signature
        if ($this->signataire)
            $cols &= ~ Document::COL_REQUEST;

        $rowclasses = "";
        if ($this->client_id > 0)
        {
            $client = get_client_by_id($this->session_formation_id, $this->client_id);
            if ($client->financement == Client::FINANCEMENT_OPAC)
                $rowclasses .= " soustraitance";
        }
        
        if (!isset($this->contexte))
            return '<tr><td colspan="100%"><pre>'.debug_print_backtrace()."\n".var_export($this, true).'</pre></td></tr>';

        $html = '<tr id="tr-'.$this->id.'" class="docrow '.$rowclasses.'" data-cols="'.$cols.'" data-sessionid="'.$this->session_formation_id.'" data-contexteid="'.$this->contexte_id.'" data-contexte="'.$this->contexte.'" data-typedoc="'.$this->type.'" data-docuid="'.$this->id.'">';

        // Nom du document
        if ($cols & Document::COL_DOCUMENT)
        {
            $icones = "";
            $brouillon = "";
            $periode_validite = "";
            $complet = "";

            if ($this->valid & Document::DRAFT)
                $brouillon = " <span class='alerte'>(".__("Brouillon").")</span> ";

            $entite_nom = "";
            if ($cols & Document::COL_NOM_ENTITE)
            {
                $entite_nom = $this->get_contexte_nom(true);
                if (!empty($entite_nom))
                    $entite_nom = " — ".$entite_nom;
            }

            $infos_doc = "";
            if (!empty($this->last_modif))
            {
                $infos_doc = '<p class="infos_doc"><span class="last-modif">'.__("Dernière modification").' : '.date_i18n("j/m/Y H:i:s", $this->last_modif).'</span>';
                $etat_signatures = $this->get_etat_signatures();
                if (count($etat_signatures['ok']) > 0)
                    $infos_doc .= "<br /><span class='fait'>".__("Document signé par : ").join(", ", $etat_signatures['ok']) . "</span>";
                if (count($etat_signatures['todo']) > 0)
                    $infos_doc .= "<br /><span class='alerte'>".__("Document à faire signer par : ").join(", ", $etat_signatures['todo']) . "</span>";
                $infos_doc .= "</p>";

                if (!($this->valid & Document::SCAN) && $this->ckw > 0)
                {
                    $etat = ($this->ckw_ok) ? "succes" : "alerte";
                    $infobulle = ($this->ckw_ok) ? __("Tous les champs personnalisés sont renseignés, mais vous pouvez les modifier") : __("Il reste des champs personnalisés à renseigner");
                    $icones .= "<span class='right float doc-check-ckw icone-doc big $etat' title='$infobulle'>".wpof_fa_edit()."</span>";
                }

                //$icones .= "<span class='right float doc-edit-html icone-doc big' title='Bla'>".wpof_fa_edit()."</span>";
                $complet = (empty($this->infos_manquantes))
                    ? " <div class='icone-info succes border'>".wpof_fa_ok().__("Document complet")."</div>"
                    : " <div class='icone-info alerte infos-manquantes border'>".wpof_fa('triangle-exclamation').sprintf(__("%d informations manquantes"), count($this->infos_manquantes))."</div>";
            }
            
            if (champ_additionnel('periode_validite') && $this->valid & Document::VALID_RESPONSABLE_NEED)
            {
                $validite_input_args = array("input" => "datepicker", "inline" => 1);
                if ($this->valid & Document::VALID_RESPONSABLE_DONE)
                    $validite_input_args['readonly'] = 1;
                $periode_validite = "<div class='flexrow edit-data'>";
                $validite_input_args['label'] = __("Période de validité : début");
                $periode_validite .= get_input_jpost($this, "date_debut", $validite_input_args);
                $validite_input_args['label'] = __("fin");
                $periode_validite .= get_input_jpost($this, "date_fin", $validite_input_args)."</div>";
            }
            
            $html .= "<td id='nom-{$this->id}'>";
            $html .= $icones; // icônes fonctionnelles alignées à droite 
            $html .= get_icone_aide("doc_".$this->type);
            $html .= '<p class="doc_nom"> '.$this->text_name.$entite_nom.$brouillon.'</p>';
            $html .= (!empty($this->link_name)) ? '<div class="icone-bouton">'.$this->link_name.'</div>' : '';
            $html .= $complet.$infos_doc;
            $html .= $periode_validite;
            if (debug && $role == "admin")
            {
                $html .= '<p class="admin debug">';
                $html .= decbin($this->valid)." / ".$this->valid."<br />";
                $html .= $this->path.'/'.$this->pdf_filename."<br />";
                $html .= decbin($this->contexte)." / ".$this->contexte."<br />";
                $html .= "</p>";
            }
            // MERGE TODO verif
            //$html .= "<td id='nom-{$this->id}'>".'<h3>'.$icones.get_icone_aide("doc_".$this->type).$this->text_name.$complet.'</h3>'.$entite_nom.$brouillon.$infos_doc."<div class='flexrow'>".(($role == "admin")?"<p>".decbin($this->valid)." / {$this->valid}</p>":'').$periode_validite.'<p class="doc-dwnld">'.$this->link_name.'</p>'."</div>";
            $html .= "</td>"; 
        }
        
        // Les deux premiers boutons ne sont affichés que si le doc n'est pas un scan
        // Bouton brouillon
        if ($cols & Document::COL_DRAFT)
        {
            $html .= '<td class="center">';
            if (!($this->valid & Document::SCAN))
            {
                $texte_bouton = (empty($this->last_modif)) ? __("Créer") : __("Mettre à jour");
                $html .= "<span class='doc-creer doc-bouton' data-ckw='{$this->ckw}'>$texte_bouton</span>";
            }
            $html .= "</td>";
        }
        
        // Bouton finaliser
        if ($cols & Document::COL_FINAL)
        {
            $html .= '<td class="center">';
            if (!empty($this->last_modif)
                && !($this->valid & Document::SCAN)
                && (!($this->valid & Document::VALID_RESPONSABLE_NEED) || $this->signataire)
                && !($this->valid & (Document::VALID_RESPONSABLE_DONE | Document::VALID_CLIENT_DONE))
                )
            {
                $texte_bouton = ($this->valid & Document::DRAFT) ? __("Créer") : __("Mettre à jour");

                $html .= '<span class="doc-creer doc-bouton" data-final="valider">'.$texte_bouton.'</span>';
            }
            $html .= "</td>";
        }
        
        // Bouton demander la signature
        //if (!$this->signataire && $cols & Document::COL_REQUEST)
        if ($cols & Document::COL_REQUEST)
        {
            $html .= '<td class="center">';
            if (!empty($this->last_modif) && empty($this->infos_manquantes) && $this->valid & Document::VALID_RESPONSABLE_NEED)
            {
                if (!($this->valid & Document::VALID_RESPONSABLE_DONE))
                {
                    $etat_class = ($this->valid & Document::VALID_RESPONSABLE_REQUEST) ? "en-cours" : "";
                    $html .= '<span class="doc-demande-valid '.$etat_class.' doc-bouton" title="'.__("Demandez la validation de ce document par votre responsable").'">'.__("Demander").'</span>';
                }
            }
            $html .= "</td>";
        }
        
        // Bouton diffuser
        if ($cols & Document::COL_DIFFUSER)
        {
            $html .= '<td class="center">';
            if (!empty($this->last_modif) && !($this->valid & Document::DRAFT))
            {
                $etat_class = ($this->visible) ? "en-cours" : "";
                $html .= '<span class="doc-diffuser '.$etat_class.' doc-bouton" title="'.__("Rendez ce document disponible au stagiaire et/ou au client").'">'.__("Diffuser").'</span>';
            }
            $html .= "</td>";
        }
        
        // bouton uploader
        if ($cols & Document::COL_SCAN)
        {
            $html .= '<td class="center">';
            $html .= $this->get_upload_link(__FUNCTION__, $cols);
            $html .= '</td>';
        }
        
        // Bouton supprimer
        if ($cols & Document::COL_SUPPRIMER)
        {
            $html .= '<td class="center">';
            if (!empty($this->last_modif))
            {
                $html .= '<span class="doc-supprimer attention doc-bouton" title="'.__("Supprimer le document (il faudra tout recommencer)").'">'.__("Supprimer").'</span>';
            }
            $html .= "</td>";
        }
        
        // Bouton rejeter
        if ($cols & Document::COL_VALIDER)
        {
            $html .= '<td class="center">';
            if (($this->valid & (Document::VALID_RESPONSABLE_REQUEST | Document::DRAFT)) == Document::VALID_RESPONSABLE_REQUEST)
                $html .= '<span class="doc-valider doc-bouton" title="'.__("Le document est prêt à être diffusé à son destinataire").'">'.__("Valider").'</span>';
            $html .= "</td>";
        }
        
        // Bouton rejeter
        if ($cols & Document::COL_REJETER)
        {
            $html .= '<td class="center">';
            if ($this->valid & Document::VALID_RESPONSABLE_REQUEST)
                $html .= "<span class='doc-rejeter attention doc-bouton' title=''>".__("Rejeter")."</span>";
            $html .= "</td>";
        }
        
        $html .= "</tr>";
        
        return $html;
    }

    public function get_upload_link($callback, $cols = 0)
    {
        global $wpof;
        
        $html = "";
        $id_span_filename = "file".rand();
        $id_span_message = "msg".rand();
        $upload_text = ($this->valid & Document::VALID_ALL_NEED) ? __("Déposer le document signé (scan ou autre)") : __("Déposer le document (scan ou autre)");
        ob_start();
        ?>
        <span class="doc-scan doc-bouton" title="<?php echo $upload_text; ?>"><?php the_wpof_fa('upload'); ?></span>
        <div class="dialog dialog-scan" style="display: none;">
        <form method="POST" name="upload-<?php echo $this->id; ?>" enctype="multipart/form-data">
        <input name="scan-<?php $this->id; ?>" type="file" accept="image/*,.pdf" />
        <p>
        <?php if ($this->valid & Document::VALID_RESPONSABLE_NEED) : ?>
            <label><input type="checkbox" name="signature_responsable" <?php checked(is_signataire() != false, true, false); ?> /> <?php _e("signé par le⋅la responsable"); ?></label><br />
        <?php endif; ?>
        <?php if ($this->valid & Document::VALID_CLIENT_NEED) : ?>
            <label><input type="checkbox" name="signature_client" /> <?php _e("signé par le⋅la client⋅e"); ?></label>
        <?php endif; ?>
        <?php if ($this->valid & Document::VALID_STAGIAIRE_NEED) : ?>
            <label><input type="checkbox" name="signature_stagiaire" /> <?php _e("signé par le⋅la⋅les stagiaire(s)"); ?></label>
        <?php endif; ?>
        </p>
        <?php
        echo hidden_input('id_span_message', $id_span_message);
        echo hidden_input('id_span_filename', $id_span_filename);
        echo hidden_input('action', 'archive_file');
        echo hidden_input('session_id', $this->session_formation_id);
        echo hidden_input('cols', $cols);
        echo hidden_input('callback', $callback);
        echo hidden_input('signataire', ($this->signataire) ? 1 : 0);
        echo hidden_input('ligne_id', "tr-{$this->id}");
        ?>
        </form>
        <p id="<?php echo $id_span_message; ?>" class="message"></p>
        </div>
        <span id="<?php echo $id_span_filename; ?>" class="filename"></span>
        
        <?php
        return ob_get_clean();
    }
    /*
     * Supprimer un document dans la base
     * Si $file vaut true, alors on supprime aussi les fichiers
     */
    public function supprimer($file = false)
    {
        if ($file)
        {
            unlink("{$this->path}/{$this->pdf_filename}");
            unlink("{$this->path}/{$this->html_filename}");
        }
        
        // dans tous les cas, on supprime le fichier .html.orig qui montre une modification manuelle du texte
        if (file_exists("{$this->path}/{$this->html_filename}.orig"))
            unlink("{$this->path}/{$this->html_filename}.orig");
        
        // mise à jour de la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        if ($this->ckw > 0) // on conserve les valeurs des mots-clés personnalisés
        {
            $not_in_clause = implode(', ', array_fill(0, $this->ckw, "'%s'"));
            $query = $wpdb->prepare
            ("DELETE FROM $table
                WHERE session_id = '%d'
                AND contexte_id = '%d'
                AND contexte = '%d'
                AND document = '%s'
                AND meta_key NOT IN (".$not_in_clause.");",
                array_merge(array($this->session_formation_id, $this->contexte_id, $this->contexte, $this->type), array_keys($this->modele->ckw))
            );
        }
        else
        {
            $query = $wpdb->prepare
            ("DELETE FROM $table
                WHERE session_id = '%d'
                AND contexte_id = '%d'
                AND contexte = '%d'
                AND document = '%s';",
                array($this->session_formation_id, $this->contexte_id, $this->contexte, $this->type)
            );
        }
        
        
        console_log($query);
        
        return $wpdb->query($query);
    }
    
    public function pdf_creer($html = "")
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $options = new Options();
        $options->set('chroot', WP_CONTENT_DIR);
        $options->set('tempDir', WP_CONTENT_DIR."/uploads");
        $options->set('isRemoteEnabled', TRUE);
        $options->set('isPhpEnabled', TRUE);
        $options->set('defaultPaperSize', 'A4');
        $options->set('defaultPaperOrientation', $this->modele->orientation);
        $options->set('defaultFont', $wpof->pdf_texte_font);
        $options->set('dpi', 200);
    
        $this->dompdf = new Dompdf($options);
        if ($wpof->dompdf_https_noverif)
            $this->dompdf->setHttpContext(
                stream_context_create([
                    'ssl' => [
                        'allow_self_signed'=> TRUE,
                        'verify_peer' => FALSE,
                        'verify_peer_name' => FALSE,
                    ]
                ])
            );

        $this->dompdf->add_info("Producer", "OPAGA");
        $this->dompdf->add_info("Creator", $wpof->of_nom);
        $this->dompdf->add_info('Subject', $this->get_displayname()." ".$this->contexte_nom);
        $this->dompdf->add_info("Keywords", implode(".", array($this->type, $this->contexte, $this->contexte_id, $this->session_formation_id, get_current_user_id())));
        
        if ($html == "")
        {
            // création du doc en HTML
            switch ($this->type)
            {
                case "questionnaire":
                    $html = $this->get_html_questionnaire();
                    break;
                case "proposition":
                    $html = $this->get_html_proposition();
                    break;
                default:
                    $html = $this->get_html_base();
                    break;
            }
        }
        elseif (!file_exists("{$this->path}/{$this->html_filename}.orig"))
            copy("{$this->path}/{$this->html_filename}", "{$this->path}/{$this->html_filename}.orig");
        
        if ($html != "")
        {
            
            $html = $this->html_from_model() . $html . '</body></html>';
            $this->html_save($html);
            
            //$html = $this->img_url_to_path($html); // l'utilisation d'image PNG locale semble poser problème (image vide intégrée dans le PDF)
            
            // output the HTML content
            $this->dompdf->loadHtml($html);
            $this->pdf_save();
            
            return true;
        }
        else
            return false;
    }
    
    /*
     * Création du document en HTML
     * Import du modèle
     * Substitution des variables
     */
    private function get_html_base()
    {
        if (isset($this->modele))
        {
            $html = "";
            if (preg_match('/<span [^>]*keyword[^>]*>([a-z]+:emargement_collectif.+)<\/span>/', $this->modele->contenu, $matches))
                $html .= $this->do_emargement_collectif($matches[1]);
            else
                $html .= $this->modele->contenu;
            return $this->substitution_auto(wpautop($html), "content");
        }
        else
            return "";
    }
    
    private function do_emargement_collectif($match)
    {
        $params = explode(':', $match);
        console_log($match);
        
        list($nb_lines, $nb_jour_max_par_page) = (isset($params[2])) ? explode('x', $params[2]) : array($wpof->nb_ligne_emargement, 1);
        
        $entite = $params[0];
        switch ($entite)
        {
            case "session":
                $session = $entite = get_session_by_id($this->session_formation_id);
                break;
            case "client":
                $entite = get_client_by_id($this->session_formation_id, $this->client_id);
                $session = get_session_by_id($this->session_formation_id);
                break;
            default:
                return wpautop($this->modele->contenu);
                break;
        }
        $fonction = 'get_doc_'.$params[1];
        
        // recalcul du nombre de jours maxi par page pour rééquilibrer sans dépasser la demande initiale
        $nb_jour_max = count($session->creneaux);
        $nb_page = (integer) ceil($nb_jour_max / $nb_jour_max_par_page);
        if ($nb_page == 0) return;
        $nb_jour_max_par_page = (integer) ceil($nb_jour_max / $nb_page);
        
        // calcul du nombre de stagiaires par page et du nombre de pages par plage de jours
        $nb_sous_page = 1; // nombre de sous-pages en fonction du nombre de stagiaires confirmés, si ça dépasse sur une seule
        if (substr_count($fonction, "vierge") == 0)
        {
            global $wpof;
            $nb_formateur = count($session->formateur);
            $nb_confirmes = $entite->nb_confirmes;
            $nb_sous_page = (integer) ceil(($nb_formateur + $nb_confirmes) / $nb_lines);
        }

        $html = array();
        for($page = 0; $page < $nb_page; $page++)
        {
            $premier_jour_de_la_page = $page * $nb_jour_max_par_page;
            
            for ($sp = 0; $sp < $nb_sous_page; $sp++)
            {
                $tableau = $entite->$fonction('valeur', $nb_lines."x".$nb_jour_max_par_page, $premier_jour_de_la_page, $sp);
                $dates_page = $entite->get_doc_emargement_dates_par_page('valeur', $nb_jour_max_par_page, $premier_jour_de_la_page, ($nb_sous_page > 1) ? $sp+1 ."/". $nb_sous_page : null);
                
                $index_page = "$page.$sp";
                
                $modele_html = HtmlDomParser::str_get_html($this->modele->contenu);
                $tag_emargement_dates_par_page = $entite->get_doc_emargement_dates_par_page('tag');
                foreach($modele_html->findMulti('.keyword') as $var)
                {
                    if ($var)
                    {
                        if ($var->innertext == $match)
                            $var->outertext = $tableau;
                        if ($var->innertext == $tag_emargement_dates_par_page)
                            $var->outertext = $this->brouillon_tag($dates_page);
                    }
                }
                $html[$index_page] = '<div class="emargement">'.$modele_html->save().'</div>';
            }
        }
        
        return join($this->get_saut_de_page(), $html);
    }

    private function get_html_proposition()
    {
        global $wpof;
        
        if ($this->contexte & $wpof->doc_context->formation)
        {
            $entite = get_formation_by_id($this->contexte_id);
            $titre = $entite->titre;
        }
        else
        {
            $entite = get_session_by_id($this->session_formation_id);
            $titre = $entite->titre_formation;
        }
        
        ob_start();
        ?>
        <h1><?php echo $titre; ?></h1>
        <?php if ($this->contexte & $wpof->doc_context->formation) : ?>
        <p><?php printf(__("Programme générique en date du %s. Il peut être adapté à vos besoins."), $entite->date_modif); ?></p>
        <?php else : ?>
        <p><?php echo $wpof->documents->get_term('proposition'); ?>. <?php printf(__("Document en date du %s."), $entite->date_modif); ?></p>
        <?php endif; ?>
        <div class="proposition">
        <?php
        $desc_proposition = $wpof->desc_formation->get_group("proposition");
        foreach($desc_proposition->term as $k => $term)
        {
            if ($k == "programme")
                continue; // on traite le programme plus loin
                
            if (!empty(trim($entite->$k)))
                echo '<div class="item-row"><div class="item-title">'.$term->text.'</div><div class="item-content">'.wpautop($entite->$k).'</div></div>';
            elseif ($this->valid & Document::DRAFT)
                printf('<div class="item-row"><div class="item-title">'.$term->text.'</div><div class="item-content">'.$this->brouillon_tag(__("Vous n'avez pas défini « %s ». Ce message n'apparait que dans le brouillon.")).'</div></div>', $term->text);
        }
        ?>
        
        <?php if ($this->contexte & $wpof->doc_context->formation) : ?>
        <div class="item-row"><div class="item-title"><?php _e("Tarif indicatif"); ?></div>
        <div class="item-content">
            <?php
                if ($entite->duree > 0)
                    echo '<p>'.get_tarif_formation($entite->tarif_base_inter_total).'</p>';
                if ($entite->show_tarif_heure)
                    echo '<p>'.get_tarif_formation($entite->tarif_base_inter_heure)." ".__("de l'heure").'</p>';
                if ($entite->duree > 0 || $entite->show_tarif_heure)
                {
                    echo $wpof->of_exotva ? '<br />'.$wpof->terms_exo_tva : "";
                    echo '<br />'.__("Tarif inter-entreprises par stagiaire. Contactez-nous pour un tarif de groupe");
                }
            ?>
        </div>
        </div>
        <div class="item-row"><div class="item-title"><?php _e("Durée"); ?></div>
        <div class="item-content">
        <p><?php printf(__("%d heures"), $entite->duree); ?>
        <?php if ($entite->nb_jour != "") echo $entite->nb_jour; ?>
        </p>
        </div>
        </div>
        <?php else : ?>
            <?php if (!is_user_logged_in()) : ?>
            <div class="item-row"><div class="item-title"><?php _e("Tarif par stagiaire"); ?>
            <div class="item-content">
            <p><strong><?php echo get_tarif_formation($entite->tarif_total_chiffre); ?></strong> <?php printf(__("soit %s de l'heure"), get_tarif_formation($entite->tarif_heure)); ?> <?php if ($wpof->of_exotva) echo "({$wpof->terms_exo_tva})"; ?></p>
            </div>
            </div>
            <?php endif; ?>
        <div class="item-row"><div class="item-title"><?php _e("Dates et durée"); ?></div>
        <div class="item-content">
        <p><?php echo $entite->dates_texte; ?></p>
        <p><?php echo ($entite->nb_heure != $entite->nb_heure_estime) ? __("Durée estimée") : __("Durée"); echo " ".$entite->nb_heure_estime; ?></p>
        </div>
        </div>
        <div class="item-row"><div class="item-title"><?php _e("Lieu"); ?></div>
        <div class="item-content">
        <p><?php echo $entite->lieu_nom; ?><br />
        <?php
            if (!empty($entite->lieu_adresse)) echo $entite->lieu_adresse.'<br />';
            if (!empty($entite->lieu_code_postal)) echo $entite->lieu_code_postal.' ';
            if (!empty($entite->lieu_ville)) echo $entite->lieu_ville;
        ?>
        </p>
        </div>
        </div>
        <div class="item-row">
        <div class="item-title"><?php _e('Planning de la session'); ?></div>
        <div class="item-content"><?php echo print_creneaux($entite->creneaux); ?></div>
        </div>
        <?php endif; ?>
        <div style="clear: both;"></div>
        
        <h3>
            <?php
            $pluriel = (count($entite->formateur) > 1) ? "s" : "";
            echo __("Intervenant(e)").$pluriel;
            ?>
        </h3>
        <?php
            foreach($entite->formateur as $f)
            {
                $formateur = new Formateur($f);
                echo $formateur->get_presentation_pdf();
            }
        ?>
        <div style="clear: both;"></div>

        <h3><?php echo $wpof->desc_formation->get_term('programme'); ?></h3>
        <?php echo wpautop($entite->programme); ?>
        
        </div> <!-- div class="proposition" -->
        <?php 
        return ob_get_clean();
    }
    
    private function get_liste_formateurs($entite, $presentation = false)
    {
        $liste_formateurs = array();
        $presentation_formateurs = array();
        foreach($entite->formateur as $f)
        {
            $formateur = new Formateur($f);
            $liste_formateurs[] = $formateur->get_displayname();
        
            if ($presentation)
            {
                $img_tag = "";
                if (!empty($formateur->photo))
                {
                    $src = $formateur->doc_url.$formateur->photo;
                    $img_tag = '<img class="photo-formateur" src="'.$src.'" />';
                }
                
                $cv_tag = ($this->valid & Document::DRAFT) ? '<p>'.$this->brouillon_tag(__("CV manquant, vous devez en fournir un !")).'</p>' : "";
                if (!empty($formateur->cv_url))
                    $cv_tag = '<p>Son CV<br /><a href="'.$formateur->cv_url.'">'.$formateur->cv_url.'</a></p>';
                
                $activite = get_user_meta($f, 'activite', true);
                if (empty($activite))
                    $activite = "";
                    
                if (!empty($formateur->realisations))
                    $realisations = '<p><strong>'.__("Ses réalisations").'</strong></p>'.wpautop($formateur->realisations);
                else
                    $realisations = "";
                
                $presentation_formateurs[] = '<div class="fiche-formateur"><div class="formateur-main"><h4>'.$formateur->get_displayname().'</h4><p>'.$formateur->activite.'</p><div class="formateur-side">'.$img_tag.$cv_tag.'</div>'.wpautop($formateur->presentation).$realisations.'</div></div>';
//                $presentation_formateurs[] = '<table class="fiche-formateur"><tr><td class="formateur-main"><h4>'.$formateur->get_displayname().'</h4><p>'.$formateur->activite.'</p>'.wpautop($formateur->presentation).$realisations.'</td><td class="formateur-side">'.$img_tag.$cv_tag.'</td></tr></table>';
            }
        }
        
        if ($presentation)
            return $presentation_formateurs;
        else
            return $liste_formateurs;
    }
    
    public function get_dialog_keywords()
    {
        global $tinymce_wpof_settings;
        ob_start();
        
        if ($this->ckw > 0) : ?>
            <div class="custom_keyword_dialog">
            <form>
            <input type="hidden" name="action" value="doc_save_custom_keywords" />
            <input type="hidden" name="contexte" value="<?php echo $this->contexte; ?>" />
            <input type="hidden" name="contexte_id" value="<?php echo $this->contexte_id; ?>" />
            <input type="hidden" name="session_id" value="<?php echo $this->session_formation_id; ?>" />
            <input type="hidden" name="type_doc" value="<?php echo $this->type; ?>" />
            
            <p><?php _e("Les champs à remplir obligatoirement sont marqués par"); ?> <?php the_wpof_fa_edit('important '); ?>
            
            <?php foreach($this->modele->ckw as $key => $val) :
                if (!isset($this->$key)) $this->$key = "";
            ?>
            <div class="ckw_bloc">
                <p class="ckw_desc openButton <?php echo ($val->needed) ? "open" : "close"; ?>" data-id="div_<?php echo $key; ?>">
                    <?php echo $val->desc; ?> 
                    <?php if ($val->needed) : ?><?php the_wpof_fa_edit('important '); ?>
                    <?php endif; ?>
                </p>
                    <?php if (!empty($val->defaut)) : ?>
                    <span class="float right icone-bouton add_keyword_defaut" data-id="ckw_<?php echo $key; ?>" data-type="<?php echo $val->type; ?>" data-content="<?php echo $val->defaut; ?>"><?php _e("Valeur par défaut"); ?></span>
                    <?php endif; ?>
                <div id="div_<?php echo $key; ?>" class="<?php echo ($val->needed) ? "" : "blocHidden" ; ?>">
                <?php
                switch ($val->type)
                {
                    case "bool":
                        ?>
                        <input type="radio" name="<?php echo $key; ?>" id="ckw_<?php echo $key."oui"; ?>" value="1" <?php checked(1, $this->$key, true); ?> /><label for="ckw_<?php echo $key."oui"; ?>" class="inline">Oui</label> | 
                        <input type="radio" name="<?php echo $key; ?>" id="ckw_<?php echo $key."non"; ?>" value="0" <?php checked(0, $this->$key, true); ?> /><label for="ckw_<?php echo $key."non"; ?>" class="inline">Non</label> 
                        <?php break;
                    case "text":
                        wp_editor($this->$key, $key, array_merge($tinymce_wpof_settings, array('textarea_rows' => 5, 'textarea_name' => $key)));
                        break;
                    case "number":
                        ?>
                        <input type="number" step="0.001" class="long-text" id="ckw_<?php echo $key; ?>" value="<?php echo $this->$key; ?>" name="<?php echo $key; ?>" />
                        <?php break;
                    default:
                        if (isset($this->val))
                            printf("Mauvais type de donnée : %s", $this->val);
                        else
                            printf("Pas de type de donnée pour %s", $key);
                        break;
                }
                ?>
                </div>
            </div>
            <?php endforeach; ?>
            </form>
            </div>
        <?php endif;
        
        return ob_get_clean();
    }
    
    private function get_html_questionnaire(): String
    {
        global $wpof;
        $session = get_session_by_id($this->session_formation_id);
        if ($session->id <= 0)
            return __("Identifiant de session incorrect ").$this->session_formation_id;
        $questionnaire = new Questionnaire($this->contexte_id);
        if (empty($questionnaire->question_id))
            return __("Questionnaire vide ou inconnu ").$this->contexte_id;
        
        ob_start(); ?>
        <div>
        <h2><?php echo $questionnaire->title; ?></h2>
        <p><?php printf(__("Pour la session <strong>%s</strong> ayant eu lieu le%s %s"), $session->get_displayname(false), (count($session->dates_array) > 1) ? "s" : "", $session->dates_texte); ?></p>
        <p><?php printf(__("Animée par <strong>%s</strong>"), $session->get_formateurs_noms()); ?></p>
        <?php if (!empty($lieu_infos = $session->get_lieu_infos(true))) : ?>
        <p><?php echo __("À")." ".$lieu_infos; ?></p>
        <?php endif; ?>
        </div>

        <?php echo $questionnaire->get_display([ 'session_id' => $session->id, 'questionnaire_id' => $questionnaire->id, 'output' => 'pdf' ]); ?>
        <?php
        return ob_get_clean();
    }

    /*
    * Création de la feuille d'émargement par jour
    * $content vaut
    * * "vide" pour créer des feuilles vierges de noms
    * * "tous" pour mentionner tous les présents, pas seulement ceux marqués comme émargeant
    */
    /*
    private function get_html_emargement_journee($content = "")
    {
        global $wpof;
        global $SessionStagiaire;
        
        $session_formation = get_session_by_id($this->session_formation_id);

        $emargement = get_post($wpof->emargement_id);
        $modele_page = "<div class='emargement'>".wpautop($emargement->post_content)."</div>";
        
        $contenu = "";
        foreach($session_formation->dates_array as $d)
        {
            $nb_stagiaires = 0;
            $colonnes = array();
            $liste_stagiaires = array();
            
            $contenu .= $modele_page;
            $contenu = str_replace("{titre}", $d, $contenu);
            
            foreach($session_formation->creneaux[$d] as $c)
            {
                if (!in_array($c->type, array("foad_sync", "foad_async")))
                    $colonnes[] = "<th class='plage'>".$c->titre."<br />".$wpof->type_creneau[$c->type]."</th>";
            }
            
            $tableau_complet = "<table class='tableau-stagiaires'><tbody><tr><th class='client'>".__("Client")."</th><th class='stagiaire'>".__("Stagiaire")."</th>".join($colonnes)."</tr>";
            
            foreach($session_formation->clients as $client_id)
            {
                $client = get_client_by_id($this->session_formation_id, $client_id);
                foreach($client->stagiaires as $stagiaire_id)
                {
                    $session_stagiaire = get_stagiaire_by_id($this->session_formation_id, $stagiaire_id);
                    if (in_array($d, $session_stagiaire->dates_array) && ($session_stagiaire->confirme || $content == "tous"))
                    {
                        $tableau_complet .= "<tr><td>";
                        if ($content != "vide")
                        {
                            if ($client->financement != "part" && isset($client->nom))
                                $tableau_complet .= $client->nom;
                        }
                        $tableau_complet .= "</td><td>";
                        if ($content != "vide")
                            $tableau_complet .= $session_stagiaire->get_displayname();
                        $tableau_complet .= "</td>";
                            
                        foreach($session_formation->creneaux[$d] as $c)
                            if (!in_array($c->type, array("foad_sync", "foad_async")))
                            {
                                if ($session_stagiaire->creneaux[$c->id] == 1 || $content == "vide" || $content == "tous")
                                    $tableau_complet .= "<td></td>";
                                else
                                    $tableau_complet .= "<td class='stagiaire-absent'></td>";
                            }
                        
                        if (isset($colonnes['ri']))
                            $tableau_complet .= "<td></td>";
                        $tableau_complet .= "</tr>";
                        $nb_stagiaires ++;
                    }
                }
            }
            
            $i = $nb_stagiaires;
            while ($i <= $session_formation->stagiaires_max)
            {
                $tableau_complet .= "<tr>".str_repeat("<td></td>", count($colonnes) + 2)."</tr>"; // 2 : colonne client + colonne stagiaire
                $i++;
            }
            
            $tableau_complet .= "</tbody></table>";
            
            $contenu = str_replace("{tableau-stagiaires}", $tableau_complet, $contenu);
            $contenu .= "<div class='saut-de-page'></div>";
        }
        
        return $this->substitute_values(wpautop($contenu));
    }
    */
    
    /*
    * Création de la feuille d'émargement par stagiaire
    */
    /*
    private function get_html_emargement_stagiaire()
    {
        global $wpof;
        global $SessionStagiaire;
        
        $session_formation = get_session_by_id($this->session_formation_id);

        $emargement = get_post($wpof->emargement_id);
        $modele_page = "<div class='emargement'>".wpautop($emargement->post_content)."</div>";
        
        $contenu = "";
        foreach($session_formation->inscrits as $stagiaire_id)
        {
            $session_stagiaire = $SessionStagiaire[$stagiaire_id];
            
            if ($session_stagiaire->confirme)
            {
                $contenu .= $modele_page;
                $stagiaire = $session_stagiaire->get_displayname();
                if ($session_formation->type_index == "inter" && $session_stagiaire->entreprise != "")
                    $stagiaire .= " (".$session_stagiaire->entreprise.")";
                $contenu = str_replace("{titre}", $stagiaire, $contenu);
                
                $tableau_complet = "<table class='tableau-dates'><tbody>";
                $tableau_complet .= "<tr><th>".__("Date")."</th><th>".__("Créneau")." / ".__("Type")."</th><th>".__("Signature")."</th></tr>";
                
                $last_date = "";
                foreach($session_formation->creneaux as $date => $creneaux)
                {
                    foreach($creneaux as $c)
                    {
                        if ($session_stagiaire->creneaux[$c->id] == 1 && !in_array($c->type, array("foad_sync", "foad_async")))
                        {
                            $cell = "td";
                            if ($date != $last_date)
                            {
                                $last_date = $date;
                                $cell = "th";
                            }
                            $tableau_complet .= "<tr><$cell>".pretty_print_dates($c->date)."</$cell><td>".$c->titre." / ".$wpof->type_creneau[$c->type]."</td><td> </td></tr>";
                        }
                    }
                }
                
                $tableau_complet .= $ligne_valid_ri."</tbody></table>";
                $contenu = str_replace("{tableau-stagiaires}", $tableau_complet, $contenu);
                $contenu .= "<div class='saut-de-page'></div>";
            }
        }
        
        return $this->substitute_values(wpautop($contenu));
    }
    */

    /*
     * Enregistrement d'un fichier HTML avec le contenu du document sans html_model
     * Servira à générer des documents PDF regroupant plusieurs documents
     */
    private function html_save($html)
    {
        if (!is_dir($this->path)) mkdir($this->path, 0777, true);
        
        $this->html_filename = $this->base_filename.".html";
        
        $html_file = fopen($this->path."/".$this->html_filename, "w");
        fwrite($html_file, $html);
    }

    /*
     * Création du document PDF avec son nom et son chemin
     * retourne le nom du fichier PDF
     */
    private function pdf_save()
    {
        global $wpof;
        if (!is_dir($this->path)) mkdir($this->path, 0777, true);
        
        $this->pdf_filename = $this->base_filename.".pdf";
        $this->dompdf->render();

        if (!$this->onthefly)
        {
            $canvas = $this->dompdf->getCanvas();
            $canvas->page_script(function ($pageNumber, $pageCount, $canvas, $fontMetrics)
            {
                global $wpof;
                
                if ($canvas->get_page_number() == $pageNumber)
                {
                    $w = $canvas->get_width();
                    $h = $canvas->get_height();
                    $size = 6;
                    $text = "Page $pageNumber / $pageCount";
                    $font_normal = $fontMetrics->getFont("helvetica", "normal");
                    $text_height = $fontMetrics->getFontHeight($font_normal, $size);
                    $text_width = $fontMetrics->getTextWidth($text, $font_normal, $size);
                    $y = $h - $text_height - $wpof->pdf_marge_bas / 0.352778;
                    $x = ($w - $text_width) / 2;
                    $canvas->page_text($x, $y, "Page {PAGE_NUM} / {PAGE_COUNT}", $font_normal, $size, [0.5, 0.5, 0.5]);
                }
            });
            $pdf_content = $this->dompdf->output();
            
            $pdf_name = $this->path."/".$this->pdf_filename;
            $pdf_file = fopen($pdf_name, "w");
            fwrite($pdf_file, $pdf_content);
            fclose($pdf_file);
        }
        else
        {
            // si le document est créé à la volée on est sur de la création de proposition de formation ou d'un document lié à une session
            if ($this->contexte & $wpof->doc_context->formation)
            {
                $formation = get_formation_by_id($this->contexte_id);
                $filename = "formation-".$formation->slug;
            }
            else
            {
                $session = get_session_by_id($this->session_formation_id);
                $filename = $this->get_displayname()." - ".$session->get_displayname();
            }
            
            $this->dompdf->stream($filename);
        }
    }
    
    /*
    * Modèle de contenu HTML header et footer (modèle de base)
    * Comme on ne peut invoquer plusieurs fois loadHtml, on prépare une chaîne html envoyée en une seule fois
    */
    private function html_from_model()
    {
        global $wpof;

/*        $html .= "h1.saut-page {page-break-before: always; break-before: always;}"; // force le saut de page avant les h1 de classe saut-page
        $html .= "h1.saut-page:nth-of-type(1) { page-break-before: avoid; break-before: avoid; }"; // sauf le premier*/

        ob_start();
        ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<style type='text/css'>
@page
{
    margin: <?php echo $wpof->pdf_marge_haut; ?>mm <?php echo $wpof->pdf_marge_droite; ?>mm <?php echo $wpof->pdf_marge_bas; ?>mm <?php echo $wpof->pdf_marge_gauche; ?>mm;
}
#pdfheader
{
    position: fixed;
    height: <?php echo $wpof->pdf_hauteur_header; ?>mm;
    top: 0;
    left: 0;
    padding: 0;
    overflow: hidden;
}
#pdffooter
{
    position: fixed;
    height: <?php echo $wpof->pdf_hauteur_footer; ?>mm;
    padding: 0;
    bottom: 0;
    left: 0;
    overflow: hidden;
}
body
{
    margin: <?php echo $wpof->pdf_hauteur_header+1; ?>mm 0 <?php echo $wpof->pdf_hauteur_footer+1; ?>mm 0;
}
div.footnote
{
    position: absolute;
    bottom: <?php echo $wpof->pdf_hauteur_footer+2; ?>mm;
}
<?php echo file_get_contents(wpof_path . "/css/pdf.css"); ?>

h1 { color: <?php echo $wpof->pdf_couleur_titre_doc; ?>; }
h2, h3, h4, h5, h6 { color: <?php echo $wpof->pdf_couleur_titre_autres; ?>; }
h1, h2, h3, h4, h5, h6 { font-family: <?php echo $wpof->pdf_titre_font; ?>; }

<?php echo $wpof->pdf_css; ?>
</style>
</head>
<body>
<?php if ($this->valid & Document::DRAFT) : ?>
<div id="watermark"><?php _e("BROUILLON"); ?></div>
<?php endif; ?>
<div id='pdfheader'><?php echo wpautop($this->substitution_auto($wpof->pdf_header, "header", false)); ?></div>
<div id='pdffooter'><?php echo wpautop($this->substitution_auto($wpof->pdf_footer, "footer", false)); ?>
</div>
    <?php
        return ob_get_clean();
    }

    /*
    * Modèle de contenu HTML avec le footer mis dans le header
    * Comme on ne peut invoquer plusieurs fois loadHtml, on prépare une chaîne html envoyée en une seule fois
    */
    private function html_from_model_header_only()
    {
        global $wpof;

        // calcul totalement empirique : le header de ce modèle a une hauteur égale à la somme de celle du header et celle du footer définies dans les options
        $hauteur_header = $wpof->pdf_hauteur_header + $wpof->pdf_hauteur_footer;

        $html = "";
        
        $html .= "<style type='text/css'>";
        $html .= "@page { margin: ".$wpof->pdf_marge_haut."mm ".$wpof->pdf_marge_droite."mm ".$wpof->pdf_marge_bas."mm ".$wpof->pdf_marge_gauche."mm; }";
        $html .= "h1.saut-page {page-break-before: always; break-before: always;}"; // force le saut de page avant les h1 de classe saut-page
        $html .= "h1.saut-page:nth-of-type(1) { page-break-before: avoid; break-before: avoid; }"; // sauf le premier
        $html .= "#pdfheader { height: ".$hauteur_header."mm; width: 50%; }";
        $html .= "body { margin: ".$hauteur_header."mm 0 0 0; }";
        $html .= $wpof->pdf_css;
        $html .= "#pdffooter { height: ".$hauteur_header."mm; top: 0mm; width: 100%; border: none; }";
        $html .= "#pdffooter p { text-align: right; font-size: 0.9em; line-height: 1.1em; padding-left: 120mm; color: cmyk(0, 0, 0, 1); }";
        $html .= "</style>";
        $html .= "<div id='pdfheader'>".wpautop($wpof->pdf_header)."</div>";
        $html .= "<div id='pdffooter'>".wpautop($wpof->pdf_footer)."</div>";
        
        return $html;
    }

    /*
     * Convertit les URL des images en chemin absolu
     * Évite la copie des images dans un dossier temporaire avant de les inclure dans le PDF
     */
    private function img_url_to_path($html)
    {
        /*
        $dom = new DOMDocument("1.0","UTF-8");
        $dom->loadHTML(utf8_decode($html));

        foreach ($dom->getElementsByTagName('img') as $img)
        {
            $src = $img->getAttribute("src");
            $src = str_replace(WP_CONTENT_URL, WP_CONTENT_DIR, $src);
            $img->setAttribute("src", $src);
        }
        return $dom->saveHTML();
        */
        return preg_replace('#(<img)(.*)(src=["\'])'.WP_CONTENT_URL.'([^"\']*)(["\'][^>]*>)#', '$1$2$3'.WP_CONTENT_DIR.'$4$5', $html);
    }
    
    private function get_saut_de_page()
    {
        return "<div class='saut-de-page'></div>";
    }
    
    private function brouillon_tag($content, $force = false)
    {
        if ($force || ($this->valid & Document::DRAFT))
        {
            $tag = 'span';
            $content_html = HtmlDomParser::str_get_html($content);
            
            if ($content_html->findMulti("p, div, ul, li, ol, table, tr, td, blockquote, dl, dt, dd, h1, h2, h3, h4, h5, h6, figure, canvas")->count() > 1)
                $tag = 'div';
            
            return '<'.$tag.' class="brouillon">'.$content.'</'.$tag.'>';
        }
        else
            return $content;
    }
    
    /*
    * Substitution automatique des variables disséminées dans les modèles de document
    * selon le schéma : <span class="keyword">entité:variable</span>
    * entité peut être :
    * * of (l'organisme de formation, options préfixées de wpof_of_)
    * * formation (la fiche de formation)
    * * session (la session de formation)
    * * client
    * * stagiaire
    * * formateur (l'équipe pédagogique de la formation ou de la session)
    *
    * À part of toutes ces entités sont dépendantes du contexte du document
    */
    private function substitution_auto($text, $part, $highlight = false)
    {
        global $wpof;
        global $document_actuel;
        $document_actuel = $this;
        
        if ($part == "content")
            $document_actuel->infos_manquantes = array();
        
        if ($highlight === false)
            $highlight = $this->valid & Document::DRAFT;
        
        $document_html = HtmlDomParser::str_get_html($text);
        
        // Analyse des mots-clés
        foreach($document_html->findMulti('.keyword') as $var)
        {
            if ($var)
            {
                $non_reconnu = false;
                
                $vars = explode(':', $var->innertext);
                $entite_type = $vars[0];
                $param_nom = $vars[1];
                $entite = array();
                $get_doc_func = "get_doc_$param_nom";
                $empty_ok = false;
                
                $param = null;
                if (isset($vars[2]))
                    $param = $vars[2];
                    
                $valeur = array();
                
                switch($entite_type)
                {
                    case 'of':
                        $entite[0] = $wpof;
                        break;
                    /*case 'formation': // TODO : à tester si pertinent !
                        $func = "get_".$entite_type."_by_id";
                        $entite[0] = $func($this->formation_id);
                        break;*/
                    case 'session':
                        $func = "get_".$entite_type."_by_id";
                        $entite[0] = $func($this->session_formation_id);
                        break;
                    case 'client':
                        $entite[0] = get_client_by_id($this->session_formation_id, $this->client_id);
                        break;
                    case 'stagiaire':
                        $entite[0] = get_stagiaire_by_id($this->session_formation_id, $this->stagiaire_id);
                        break;
                    case 'formateur':
                        $entite = array_values(get_tab_formateur_by_tab_id($this->array_formateur_id));
                        break;
                    case 'document':
                        $entite[0] = $this;
                        break;
                    default:
                        $non_reconnu = true;
                        break;
                }
                if (!empty($entite) && method_exists($entite[0], $get_doc_func))
                {
                    foreach($entite as $e)
                    {
                        if ($param)
                            $valeur[] = trim($e->$get_doc_func('valeur', $param));
                        else
                            $valeur[] = trim($e->$get_doc_func('valeur'));
                    }
                    $desc = $entite[0]->$get_doc_func('desc');
                    $empty_ok = $entite[0]->$get_doc_func('empty_ok');
                }
                elseif ($entite_type == "document")
                {
                    $desc = $entite[0]->modele->ckw[$param_nom]->desc;
                    $empty_ok = ! $entite[0]->modele->ckw[$param_nom]->needed;
                    if (isset($entite[0]->$param_nom))
                        $valeur[] = trim($entite[0]->$param_nom);
                    else if (!$empty_ok && $highlight)
                        $valeur = '<span class="doc_info_absente" data-func="'.$param_nom.'" data-entite="'.$entite_type.'" data-entitenom="'.$this->desc.'" data-desc="'.$desc.'">'.$desc.'</span>';
                }
                else
                    $non_reconnu = true;
                    
                if ($non_reconnu)
                    $var->outertext = $this->brouillon_tag($var->innertext." non reconnu", true);
                else
                {
                    if (is_array($valeur))
                    {
                        // suppression des valeurs vides 
                        $valeur = array_filter($valeur, "strlen");

                        if (!empty($valeur))
                        {
                            $html_valeur = HtmlDomParser::str_get_html(reset($valeur));
                            if ($html_valeur->find("p, div, ul, li, ol, table, tr, td, blockquote, dl, dt, dd, h1, h2, h3, h4, h5, h6, figure, canvas"))
                                $valeur = implode(' ', $valeur);
                            else
                                $valeur = implode(', ', $valeur);
                        }
                        else
                            $valeur = "";
                    }
                    if ($valeur == '' && !$empty_ok && $highlight)
                    {
                        $entite_nom = (method_exists($entite[0], 'get_displayname')) ? $entite[0]->get_displayname() : "";
                        $valeur = '<span class="doc_info_absente" data-func="'.$get_doc_func.'" data-entite="'.$entite_type.'" data-entitenom="'.$entite_nom.'" data-desc="'.$desc.'">'.$desc.'</span>';
                    }
                    $var->outertext = $this->brouillon_tag($valeur, $highlight);
                }
            }
        }

        // Analyse des tests
        foreach($document_html->findMulti('.test-expression > .test[data-kw="fi"]') as $var)
        {
            $var = $var->parent();
            //error_log($var->text());
            if ($var && !in_array("erreur", explode(" ", $var->getAttribute('class'))))
            {
                $non_reconnu = false;
                $operande = ["", ""];
                
                // Analyse de l'expression conditionnelle
                $current_node = $var->first_child();
                while($current_node->getAttribute('data-kw') != "if")
                    $current_node = $current_node->next_sibling();

                $current_node = $current_node->next_sibling();
                while($current_node->getAttribute('data-kw') != "sign")
                {
                    if ($current_node->findOneOrFalse('.doc_info_absente') === false)
                        $operande[0] .= $current_node->outertext;
                    $current_node = $current_node->next_sibling();
                }
                $test_sign = $current_node->getAttribute('data-sign');
                $current_node = $current_node->next_sibling();
                while($current_node->getAttribute('data-kw') != "then")
                {
                    if (!in_array($test_sign, array("vrai", "faux")) && ($current_node->findOneOrFalse('.doc_info_absente') === false))
                    {
                        $operande[1] .= $current_node->outertext;
                    }
                    $current_node = $current_node->next_sibling();
                }

                $operande = array_map(fn($c): String => strip_tags($c), $operande);

                // Analyse condition
                if ($operande[1] == "")
                {
                    $condition = (!empty($operande[0]) && in_array($test_sign, array("vrai", "faux")));
                }
                elseif (false !== $filtered_operande = filter_var_array(array_map(fn($c): String => str_replace(',', '.', $c), $operande), FILTER_VALIDATE_FLOAT))
                {
                    switch($test_sign)
                    {
                        case "egal":
                            $condition = ($filtered_operande[0] == $filtered_operande[1]);
                            break;
                        case "infegal":
                            $condition = ($filtered_operande[0] <= $filtered_operande[1]);
                            break;
                        case "supegal":
                            $condition = ($filtered_operande[0] >= $filtered_operande[1]);
                            break;
                        case "inf":
                            $condition = ($filtered_operande[0] < $filtered_operande[1]);
                            break;
                        case "sup":
                            $condition = ($filtered_operande[0] > $filtered_operande[1]);
                            break;
                        case "diff":
                            $condition = ($filtered_operande[0] != $filtered_operande[1]);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    $operande[0] = sanitize_title($operande[0]);
                    $operande[1] = sanitize_title($operande[1]);
                    switch($test_sign)
                    {
                        case "egal":
                        case "infegal":
                        case "supegal":
                            $condition = (strcmp($operande[0], $operande[1]) == 0);
                            break;
                        case "inf":
                        case "sup":
                        case "diff":
                            $condition = (strcmp($operande[0], $operande[1]) != 0);
                            break;
                        default:
                            break;

                    }
                }

                $replacement_text = "";

                if (isset($condition))
                {
                    $current_node = $current_node->next_sibling();
                    while($current_node->getAttribute('data-kw') != "else")
                    {
                        if ($condition)
                            $replacement_text .= $current_node->outertext;
                        $current_node = $current_node->next_sibling();
                    }
                    if ($condition == false)
                    {
                        $current_node = $current_node->next_sibling();
                        while($current_node->getAttribute('data-kw') != "fi")
                        {
                            $replacement_text .= $current_node->outertext;
                            $current_node = $current_node->next_sibling();
                        }
                    }
                    $var->outertext = $replacement_text;
                    if (empty(trim($replacement_text)))
                        $var->outertext = "vide";

                }
                else
                {
                    $non_reconnu = true;
                    $var->outertext = "<span class=\"brouillon\">Test non reconnu : ".$var->innertext."</span>";
                    error_log("Mot-clé non reconnu ".$var->innertext);
                }
            }
            else
                $var->outertext = "";
        }
        
        // Comptage des infos manquantes
        if ($part == "content")
        {
            foreach($document_html->findMulti('.doc_info_absente') as $var)
                $this->infos_manquantes[$var->getAttribute("data-entite").':'.$var->getAttribute("data-func")] = array
                (
                    'doc_func' => $var->getAttribute("data-func"),
                    'entite_type' => $var->getAttribute("data-entite"),
                    'entite_nom' => $var->getAttribute("data-entitenom"),
                    'desc' => $var->getAttribute("data-desc"),
                );
            $this->update_meta("infos_manquantes");
        }
        
        $save = $document_html->save();
        $text = $document_html->html();

        return typographic_correction($text);
    }

    // tableau listing des documents
    // en-tête de tableau
    public static function get_document_control_head()
    {
        global $wpof;
        $page_contexte = $wpof->slug;
        ob_start(); ?>
        <tr>
        <th class="center thin selectable not_orderable"><input type="checkbox" name="all" /></th>
        <th><?php _e("Entité concernée"); ?></th>
        <th><?php _e("Nom entité"); ?></th>
        <th><?php _e("Document"); ?></th>
        <th><?php _e("État"); ?></th>
        <th><?php _e("Signé par"); ?></th>
        <th><?php _e("Attente signature"); ?></th>
        <th><?php _e("Diffusé"); ?></th>
        <th <?php if ($page_contexte == 'session') : ?>id="order" data-order="desc"<?php endif; ?>><?php _e("Modifié le"); ?></th>
        <?php if ($page_contexte != 'session') : ?>
        <th><?php _e("Formateur⋅ice"); ?></th>
        <th id="order" data-order="desc"><?php _e("Date prestation"); ?></th>
        <?php endif; ?>
        <th class="center not_orderable"><?php _e("Commandes"); ?></th>
        
        <?php if ($wpof->role == "admin") : // en attente d'un menu hamburger avec toutes les fonctions nécessaires ?>
        <th class="center thin"><?php _e("Supprimer"); ?></th>
        <?php endif; // if (role == admin) ?>
        
        </tr>
        <?php
        return ob_get_clean();
    }
    
    public function get_document_control()
    {
        global $wpof;
        $page_contexte = $wpof->slug;
        $contexte_entite = $wpof->doc_context_terms[$this->contexte & $wpof->doc_context->entite];
        $contexte_type = sanitize_title($contexte_entite);
        $signatures = $this->get_etat_signatures();
        $this->init_link_name(false);
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        $info_icone = "";
        switch ($contexte_type)
        {
            case "client":
                $info_icone = '<div class="float right icone-info pointer info-parent" title="'.sprintf(__("Dans session %s – %s"), $this->session->titre_session, $this->session->first_date).'" data-parent="session'.$this->session_formation_id.'">'.wpof_fa("fa-circle-info").'</div>';
                if ($client->entite_client == Client::ENTITE_PERS_PHYSIQUE)
                    $contexte_entite .= "<br />".__("particulier");
                break;
            case "stagiaire":
                if ($client)
                    $info_icone = '<div class="float right icone-info pointer info-parent" title="'.sprintf(__("Dans client %s"), $client->get_displayname()).'" data-parent="client'.$this->client_id.'">'.wpof_fa("fa-circle-info").'</div>';
                break;
            default:
                break;
        }
        
        ob_start(); ?>
        
        <?php if ($page_contexte == 'session' || $this->last_modif) : ?>
        
        <tr id="tr-<?php echo $this->id; ?>" class="docrow <?php echo $contexte_type; ?> <?php echo $contexte_type.$this->contexte_id; ?> <?php echo $this->type; ?> <?php echo ($this->last_modif) ? "" : "empty" ?>" data-contexte="<?php echo $this->contexte; ?>" data-contexteid="<?php echo $this->contexte_id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-typedoc="<?php echo $this->type; ?>" role="row">
        
        <td class="center thin selectable not_orderable"><input type="checkbox" name="<?php echo $this->id; ?>" /></td>
        <td><?php echo $contexte_entite.$info_icone; ?></td>
        <td><?php echo $this->contexte_nom_lien; ?></td>
        <td><?php echo $this->desc; ?></td>
        <td>
        <?php
            if ($this->last_modif)
            {
                echo ($this->valid & self::DRAFT) ? __("Brouillon") : __("Final");
                if ($this->valid & self::VALID_RESPONSABLE_REQUEST)
                    echo '<br /><span class="alerte">'.__('validation demandée').'</span>';
            }
        ?></td>
        <td><?php echo implode(', ', $signatures['ok']); ?></td>
        <td><?php echo implode(', ', $signatures['todo']); ?></td>
        <td class="center"><?php echo ($this->visible) ? wpof_fa("share-nodes") : ''; ?></td>
        <td><span class="hidden"><?php echo $this->last_modif; ?></span><?php if ($this->last_modif) echo date_i18n("j/m/Y H:i:s", $this->last_modif); ?></td>
        <?php if ($page_contexte != 'session') : ?>
        <td><?php echo $this->session->get_formateurs_noms(); ?></td>
        <td><span class="hidden"><?php echo $this->session->first_date_timestamp; ?></span><?php echo $this->session->first_date; ?></td>
        <?php endif; ?>
        <td class="center flexrow">
        <?php if (!($this->valid & Document::SCAN)) : ?>
            <?php $texte_bouton = (empty($this->last_modif)) ? __("Créer brouillon") : __("Mettre à jour brouillon"); ?>
            <span class="doc-creer doc-bouton brouillon" data-ckw="<?php echo $this->ckw; ?>"><?php the_wpof_fa("square-plus", "", $texte_bouton) ?></span>
        <?php endif; ?>
        <?php if (!empty($this->last_modif)
                && !($this->valid & Document::SCAN)
                && (!($this->valid & Document::VALID_RESPONSABLE_NEED) || $this->signataire)
                && !($this->valid & (Document::VALID_RESPONSABLE_DONE | Document::VALID_CLIENT_DONE))
                ) : ?>
            <?php $texte_bouton = (empty($this->last_modif)) ? __("Créer final") : __("Mettre à jour final"); ?>
            <span class="doc-creer doc-bouton final" data-final="valider"><?php the_wpof_fa("square-plus", "solid", $texte_bouton); ?></span>
        <?php endif; ?>
        <?php if (!empty($this->last_modif) && $this->valid & Document::VALID_RESPONSABLE_NEED && !($this->valid & Document::VALID_RESPONSABLE_DONE)) : ?>
            <span class="doc-demande-valid doc-bouton"><?php the_wpof_fa("file-signature", "", __("Demandez la validation de ce document par votre responsable")); ?></span>
        <?php endif; ?>
        <?php if (!empty($this->last_modif) && !($this->valid & Document::DRAFT)) : ?>
            <span class="doc-diffuser doc-bouton"><?php the_wpof_fa("share-nodes", "", __("Rendez ce document disponible au stagiaire et/ou au client")); ?></span>
        <?php endif; ?>
        <?php echo $this->get_upload_link(__FUNCTION__); ?>
        <?php if ($this->last_modif) : ?>
            <div><?php echo $this->link_name; ?></div>
            <?php if (empty($this->infos_manquantes)) : ?>
                <div class="icone-info succes" title="<?php _e("Document complet"); ?>"><?php the_wpof_fa_ok(); ?></div>
            <?php else : ?>
                <div class="icone-info alerte infos-manquantes" title="<?php printf(__("%d informations manquantes"), count($this->infos_manquantes)); ?>"><?php the_wpof_fa('triangle-exclamation'); ?></div>
            <?php endif; ?>
            <?php
            if (!($this->valid & Document::SCAN) && $this->ckw > 0) :
                $etat = ($this->ckw_ok) ? "succes" : "alerte";
                $infobulle = ($this->ckw_ok) ? __("Tous les champs personnalisés sont renseignés, mais vous pouvez les modifier") : __("Il reste des champs personnalisés à renseigner"); ?>
                <span class="doc-check-ckw icone-doc <?php echo $etat; ?>" title="<?php echo $infobulle; ?>"><?php the_wpof_fa_edit(); ?></span>
            <?php endif; ?>
        <?php endif; ?>
        </td>
        
        <?php if ($wpof->role == "admin") : // en attente d'un menu hamburger avec toutes les fonctions nécessaires ?>
        <td class="center thin">
        <?php if ($this->last_modif) : ?>
            <span class="doc-supprimer attention doc-bouton icone-bouton" title="<?php _e("Supprimer le document (il faudra tout recommencer)"); ?>"><?php the_wpof_fa_del(); ?></span>
        <?php endif; ?>
        </td>
        <?php endif; // if (role == admin) ?>
        
        </tr>
        
        <?php endif; // ($page_contexte == 'session' || $this->last_modif) ?>
        
        <?php
        return ob_get_clean();
    }

    public function get_elements_preuve()
    {
        global $wpof;
        $page_contexte = $wpof->slug;
        $contexte_entite = $wpof->doc_context_terms[$this->contexte & $wpof->doc_context->entite];
        $contexte_type = sanitize_title($contexte_entite);
        $signatures = $this->get_etat_signatures();
        $this->init_link_name(false);
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        ob_start(); ?>
        
        <?php if ($page_contexte == 'session' || $this->last_modif) : ?>
        
        <tr id="tr-<?php echo $this->id; ?>" class="docrow <?php echo $contexte_type; ?> <?php echo $contexte_type.$this->contexte_id; ?> <?php echo $this->type; ?> <?php echo ($this->last_modif) ? "" : "empty" ?>" data-contexte="<?php echo $this->contexte; ?>" data-contexteid="<?php echo $this->contexte_id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-typedoc="<?php echo $this->type; ?>" role="row">

        <td class="sorting_1"><?php echo $this->session->get_formateurs_noms(); ?><span class="hidden">[<?php echo get_displayname($this->session->formateur[0]);?>/<?php if ($this->desc == "Contrat de sous-traitance prestation de formation professionnelle"): echo "Sous traitance/"; endif;?>Formation du <?php echo date_i18n("j F", $this->session->first_date_timestamp); ?><?php if ($this->session->first_date != $this->session->last_date): echo ' au ' . date_i18n("j F", $this->session->last_date_timestamp); endif;?>]</span></td>
        <td><span class="hidden"><?php echo $this->session->first_date_timestamp; ?></span><?php echo $this->session->first_date; ?></td>
        <td><?php echo $this->session->titre_session; ?></td>
        <td><?php echo $this->desc; ?></td>
        <td><?php echo $contexte_entite; ?> (<?php echo $this->contexte_nom_lien; ?>)</td>
        <td>
        <?php
            if ($this->last_modif)
            {
                echo ($this->valid & self::DRAFT) ? __("Brouillon") : __("Final");
                if ($this->valid & self::VALID_RESPONSABLE_REQUEST)
                    echo '<br /><span class="alerte">'.__('validation demandée').'</span>';
            }
        ?></td>
        <td><span class="hidden"><?php echo $this->last_modif; ?></span><?php if ($this->last_modif) echo date_i18n("j/m/Y H:i:s", $this->last_modif); ?></td>
        <td class="center">
            <?php if ($this->last_modif) : ?>
                <?php echo $this->link_name; ?>
                <span class="hidden"><?php echo $this->href; ?></span>
                <?php if (empty($this->infos_manquantes)) : ?>
                    <span class="icone-info succes" title="<?php _e("Document complet"); ?>"><?php the_wpof_fa_ok(); ?></span>
                <?php else : ?>
                    <span class="icone-info alerte infos-manquantes" title="<?php printf(__("%d informations manquantes"), count($this->infos_manquantes)); ?>"><?php the_wpof_fa('clock'); ?></span>
                <?php endif; ?>
            <?php endif; ?>
        </td>
        
        </tr>
        
        <?php endif; // ($page_contexte == 'session' || $this->last_modif) ?>
        
        <?php
        return ob_get_clean();
    }

    public function get_doc_titre($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "document:$function_name", 'desc' => __("Intitulé du document"));
        return $result[$arg];
    }
    public function get_doc_date_debut($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "document:$function_name", 'desc' => __("Date de début de validité"));
        return $result[$arg];
    }
    public function get_doc_date_fin($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "document:$function_name", 'desc' => __("Date de fin de validité"));
        return $result[$arg];
    }
    public function get_doc_liste_formateurs($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            $session = get_session_by_id($this->session_formation_id);
            $valeur = implode("\n", $this->get_liste_formateurs($session, true));
        }
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "document:$function_name", 'desc' => __("Présentation des formateur⋅trices"));
        return $result[$arg];
    }
    
    // fonction appelée par les 5 fonctions suivantes pour renvoyer un booléen qui dit si on est dans le contexte demandé (dans le nom de la fonction) ou pas
    public function get_is_contexte($fonction, $arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', $fonction);
        $contexte = str_replace('get_doc_contexte_', '', $fonction);
        $valeur = ($arg == 'valeur') ? $wpof->doc_context->$contexte & $this->contexte : "";
        $result = array('empty_ok' => true, 'valeur' => $valeur, 'tag' => "document:$function_name", 'desc' => sprintf(__("1 en contexte %s, 0 sinon"), $contexte));
        return $result[$arg];
        
    }
    public function get_doc_contexte_session($arg = 'valeur')
    {
        return $this->get_is_contexte(__FUNCTION__, $arg);
    }
    public function get_doc_contexte_formation($arg = 'valeur')
    {
        return $this->get_is_contexte(__FUNCTION__, $arg);
    }
    public function get_doc_contexte_client($arg = 'valeur')
    {
        return $this->get_is_contexte(__FUNCTION__, $arg);
    }
    public function get_doc_contexte_stagiaire($arg = 'valeur')
    {
        return $this->get_is_contexte(__FUNCTION__, $arg);
    }
    public function get_doc_contexte_formateur($arg = 'valeur')
    {
        return $this->get_is_contexte(__FUNCTION__, $arg);
    }
}

/*
* Crée un tableau pour gérer les documents administratifs
*/
function get_gestion_docs($objet)
{
    global $Documents;
    global $wpof;
    
    // l'utilisateur courant peut-il signer les documents
    $signataire = is_signataire();
    
    // quelle est la source de documents ?
    $doc_necessaire = $objet->doc_necessaire;
    $doc_uid_suffix = "-{$objet->doc_suffix}";
    
    // pas de session prefix si objet est une session, on a déjà l'info dans suffix
    $session_id_prefix = (get_class($objet) == "SessionFormation") ? "" : $objet->session_formation_id.'-';
    
    // si aucun document n'est nécessaire (j'en doute), on ne fait rien
    if (count($doc_necessaire) == 0) return "";
            
    $html = "";
    
    ob_start();
    ?>
    <table class="gestion-docs-admin" id="gestion-docs-admin">
        <thead>
            <tr class="tr-titre">
                <th><?php _e("Document"); echo " ".get_icone_aide("document"); ?></th>
                <th><?php _e("Brouillon"); echo " ".get_icone_aide("brouillon"); ?></th>
                <th><?php _e("Final"); echo " ".get_icone_aide("final"); ?></th>
                <?php if (!$signataire): ?>
                    <th><?php _e("Signature responsable"); echo " ".get_icone_aide("signature_responsable"); ?></th>
                <?php endif; ?>
                <th><?php _e("Diffuser"); echo " ".get_icone_aide("diffuser"); ?></th>
                <th><?php _e("Déposer document"); echo " ".get_icone_aide("scan"); ?></th>
                <th><?php _e("Supprimer"); echo " ".get_icone_aide("supprimer"); ?></th>
            </tr>
        </thead>
        <tbody>
    <?php
    
    $html .= ob_get_clean();
    
    foreach($doc_necessaire as $doc_slug)
    {
        $doc = $Documents[$session_id_prefix.$objet->doc_suffix.'-'.$doc_slug];
        $doc->signataire = $signataire;
        
        $html .= $doc->get_html_ligne();
    }
        
    $html .= "</tbody></table>";
    
    return $html;
}

add_action('wp_ajax_get_html_doc_content', 'get_html_doc_content');
function get_html_doc_content()
{
    $reponse = $_POST;
    
    $document = new Document($_POST['typedoc'], $_POST['session_id'], $_POST['contexte'], $_POST['contexte_id']);
    ob_start();
    ?>
    <div class='dialog'><?php wp_editor(file_get_contents("{$document->path}/{$document->html_filename}"), "content".time()); ?></div>;
    <?php
    $reponse['html'] = ob_get_clean();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_update_html_doc_content', 'update_html_doc_content');
function update_html_doc_content()
{
    $reponse = $_POST;
    
    $document = new Document($_POST['typedoc'], $_POST['session_id'], $_POST['contexte'], $_POST['contexte_id']);
    $document->pdf_creer($_POST['content']);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_doc_check_custom_keywords', 'doc_check_custom_keywords');
function doc_check_custom_keywords()
{
    $reponse = array();
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
    
    $empty_kw = array();
    
    foreach($doc->modele->ckw as $key => $val)
    {
        if (empty($doc->$key))
            $empty_kw[$key] = $val;
    }
    
    $reponse['ckw'] = count($empty_kw);
    $reponse['dialog'] = $doc->get_dialog_keywords();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_doc_save_custom_keywords', 'doc_save_custom_keywords');
function doc_save_custom_keywords()
{
    global $wpof;
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
    $manque = 0;
    
    foreach(array_keys($doc->modele->ckw) as $key)
    {
        if (isset($_POST[$key]))
            $doc->update_meta($key, $_POST[$key]);
        else
            $manque++;
    }
    $doc->set_ckw_ok();
    
    $reponse['manque'] = $manque;
    if ($wpof->slug == $wpof->url_gestion && $wpof->uri_params[1] == "signer" || ($wpof->slug == "session" && $wpof->uri_params[1] != "documents"))
        $reponse['lignedoc'] = $doc->get_html_ligne();
    else
        $reponse['lignedoc'] = $doc->get_document_control();
    echo json_encode($reponse);
    die();
}

/*
 * Vérification de la présence et de la complétion de mots-clés personnalisés
 */


/*
 * Fonction de traitement d'un document
 * Utilisée via une requête POST Ajax
 */
function traitement_doc()
{
    global $SessionFormation;
    global $wpof;
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $action_doc = $_POST['action_doc'];
    
    ob_start(); // permet de récupérer les éventuels message d'erreur ou alerte dans $reponse['log'] et éviter de foirer l'analyse JSON de $reponse
    
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
    
    if ($contexte == 0)
    {
        $reponse['log'] = debug_print_backtrace();
        echo json_encode($reponse);
        die();
    }

    $wpof->log->log_action($action_doc, $doc);
        
    $reponse = array('message' => '', 'lignedoc' => '');
    
    switch($action_doc)
    {
        case "final":
        case "brouillon":
            if ($action_doc == "brouillon")
                $doc->valid |= Document::DRAFT;
            else
                $doc->valid &= ~Document::DRAFT;
            $doc->valid &= ~(Document::VALID_ALL_DONE);
            
            $doc->update_meta('valid', $doc->valid);
            
            $doc->signataire = is_signataire();
            
            if ($doc->pdf_creer())
            {
                $doc->update_meta('pdf_filename', $doc->pdf_filename);
                $doc->update_meta('html_filename', $doc->html_filename);
                $doc->update_meta('last_modif', current_time('timestamp'));
                
                $doc->init_link_name();
            }
            else
                $reponse['message'] .= "<span class='erreur'>".__("Création échouée, le document est vide !")."</span>";
            // TODO : faire un objet JSON avec champs html, erreur, demande_valid, diffuser
            break;
            
        case "demande_valid":
            $doc->update_meta('valid', $doc->valid ^ Document::VALID_RESPONSABLE_REQUEST);
            $session = get_session_by_id($session_id);
            
            if ($doc->valid & Document::VALID_RESPONSABLE_REQUEST)
            {
                foreach(array_reverse($session->formateur) as $fid)
                {
                    $pending_signatures = array();
                    $pending_signatures = get_user_meta($fid, "pending_signatures", true);
                    if (!is_array($pending_signatures))
                        $pending_signatures = array();
                    $pending_signatures[$doc->session_formation_id."_".$doc->contexte."_".$doc->contexte_id.$doc->type] = array('session_id' => $doc->session_formation_id, 'contexte' => $doc->contexte, 'contexte_id' => $doc->contexte_id, 'document' => $doc->type);
                    update_user_meta($fid, "pending_signatures", $pending_signatures);
                    echo $fid." ".count($pending_signatures)."|";
                }
            }
            else
            {
                foreach($session->formateur as $fid)
                {
                    $pending_signatures = get_user_meta($fid, "pending_signatures", true);
                    if (isset($pending_signatures[$doc->session_formation_id."_".$doc->contexte."_".$doc->contexte_id.$doc->type]))
                        unset($pending_signatures[$doc->session_formation_id."_".$doc->contexte."_".$doc->contexte_id.$doc->type]);
                    update_user_meta($fid, "pending_signatures", $pending_signatures);
                }
            }
            break;
            
        case "diffuser":
            $doc->update_meta('visible', !$doc->visible);
            // TODO : envoi un message au stagiaires
            break;
        case "supprimer":
            $doc->supprimer();
            $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
            // TODO : éventuellement supprimer le fichier
            break;
        case "valider":
            $reponse += $doc->mark_as_signed();
            if ($doc->valid & Document::SCAN)
            {
                if (!is_dir($doc->path.'/scan'))
                    mkdir($doc->path.'/scan');
                rename($doc->path.'/'.$doc->pdf_filename, $doc->path.'/scan/'.$doc->pdf_filename);
            }
            break;
        case "rejeter":
            $doc->update_meta('valid', $doc->valid & ~ Document::VALID_RESPONSABLE_REQUEST);
            $session = get_session_by_id($session_id);
            foreach($session->formateur as $fid)
            {
                $formateur = get_formateur_by_id($fid);
                $formateur->remove_pending_signature($doc);
            }
            $resp = get_formateur_by_id(get_current_user_id());
            $subject = "[".$wpof->of_nom." Formation] Refus de validation pour ".$doc->titre;
            $content_message = "Bonjour,\n\nVotre document :\n".$doc->titre." a été refusé par ".$resp->get_displayname()."\n";
            $content_message .= "\nPour la session :\n".$session->titre_formation."\n\ndébutant le ".$session->first_date."\n";
            
            if (!empty($_POST['message_rejet']))
                $content_message .= "\n".$_POST['message_rejet']."\n";
            if (!empty($doc->infos_manquantes))
            {
                $content_message .= "\nDe manière automatique OPAGA a détecté que ces informations sont manquantes :\n";
                foreach($doc->infos_manquantes as $key => $val)
                    $content_message.= "  — ".$val['desc']."\n";
            }
            $content_message .= "\nConnectez-vous sur la page de la session pour corriger →\n".$session->permalien;
            $message = new Message(array('to' => $session->formateur, 'content' => $content_message, 'subject' => $subject));
            $message->sendmail();
            $reponse['message'] = "Notification faite à ".implode(', ', $message->to_email);
            break;
    }
    
    if ($wpof->slug == $wpof->url_gestion && $wpof->uri_params[1] == "signer" || ($wpof->slug == "session" && $wpof->uri_params[1] != "documents"))
        $reponse['lignedoc'] = $doc->get_html_ligne((isset($_POST['cols'])) ? $_POST['cols'] : Document::COL_ALL);
    else
        $reponse['lignedoc'] = $doc->get_document_control();

        echo "plop";
    $reponse['log'] = ob_get_clean();

    echo json_encode($reponse);

    die();
}
add_action('wp_ajax_traitement_doc', 'traitement_doc');

function doc_get_infos_manquantes()
{
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $reponse = array('erreur' => '');
    
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
    
    if (!empty($doc->infos_manquantes))
    {
        $reponse['erreur'] = "<h3>".__("Informations manquantes")."</h3>";
        foreach($doc->infos_manquantes as $info)
            if (isset($info['doc_func']))
                $reponse['erreur'] .= "<p><em>".str_replace("get_doc_", "", $info['doc_func'])."</em> : <strong>".$info['desc']."</strong> pour ".$info['entite_type']." <strong>".$info['entite_nom']."</strong></p>";
            else
                $reponse['erreur'] .= $info;
    }
    
    $reponse['erreur'] = '<div class="left">'.$reponse['erreur'].'</div>';
    echo json_encode($reponse);
    die();
}
add_action('wp_ajax_doc_get_infos_manquantes', 'doc_get_infos_manquantes');

/*
 * Fonction de traitement d'un documents
 * Utilisée via une requête POST Ajax
 * TODO : fonction inutilisée actuellement. Elle permettait de créer une compilation de docuemnts en un seul en récupérant le HTML
 * Peut-être qu'elle pourrait servir à autre chose
 */
function export_pdf()
{
    global $SessionFormation;
    global $SessionStagiaire;
    global $Documents;
    
    $user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : -1;
    $doc_id = (isset($_POST['doc_id'])) ? $_POST['doc_id'] : "";
    $session_id = $_POST['session_id'];
    
    $suffixe = ($doc_id != "") ? "-$doc_id" : "";
    
    $session_formation = $SessionFormation[$session_id] = new SessionFormation($session_id);
    $session_formation->init_stagiaires();
    
    if ($user_id > 0)
    {
        $SessionStagiaire[$user_id]->init_docs();
        $suffixe .= "-".$SessionStagiaire[$user_id]->user->user_nicename;
    }
    else
    {
        $session_formation->init_docs();
        foreach($session_formation->inscrits as $i)
            $SessionStagiaire[$i]->init_docs();
    }
    
    $first_doc = reset($Documents);
    $html = $first_doc->html_from_model();
    
    foreach($Documents as $doc)
    {
        if ($doc_id == "" || $doc_id == $doc->type)
        {
            $file_content = file_get_contents($doc->path."/".$doc->html_filename);
            
            if ($file_content != "")
            {
                $html .= $file_content;
                $html .= "<div class='saut-de-page'></div>";
            }
        }
    }
    
    $options = new Options();
    $options->set('chroot', WP_CONTENT_DIR);
    $options->set('tempDir', WP_CONTENT_DIR."/uploads");
    $options->set('isRemoteEnabled', TRUE);
    $options->set('isPhpEnabled', TRUE);
    $options->set('defaultPaperSize', 'A4');
    $options->set('dpi', 200);
    
    $dompdf = new Dompdf($options);
    $dompdf->loadHtml($html);
    $dompdf->render();
    $pdf_content = $dompdf->output();
    
    $pdf_name = sanitize_title($session_formation->titre_session.$suffixe).".pdf";
    $pdf_file = fopen($first_doc->path."/".$pdf_name, "w");
    fwrite($pdf_file, $pdf_content);
    fclose($pdf_file);
    
    echo "<a href='{$first_doc->url}/$pdf_name'>$pdf_name</a>";
    
    die();
}
add_action( 'wp_ajax_export_pdf', 'export_pdf' );


add_action('wp_ajax_archive_file', 'archive_file');
function archive_file()
{
    global $wpof;
    require_once(wpof_path . "/class/class-upload.php");
    
    $reponse = array('message' => '', 'filename' => '', 'lignedoc' => '');
    
    if (isset($_POST['session_id']) && $_POST['session_id'] > 0)
    {
        $session_id = $_POST['session_id'];
        $session_formation = get_session_by_id($session_id);
        
        if (!empty($_POST['contexte_id']) && $_POST['contexte_id'] > 0)
        {
            $contexte_id = $_POST['contexte_id'];
            $contexte = $_POST['contexte'];
        }
        else
        {
            $contexte_id = -1;
            $contexte = $wpof->doc_context->session;
        }
        
        for ($i = 0; $i < $_POST['nb_files']; $i++)
        {
            if ($_FILES["files"]["error"][$i] > 0)
            {
                $reponse['message'] .= "<span class='erreur'>".__("Erreur de téléversement code ")." ".$_FILES["files"]["error"][$i]." ".__("pour le fichier")." ".$_FILES["files"]["name"][$i]."</span>";
            }
            else
            {
                $upload = new Upload($session_id);
                
                if (!is_dir($upload->path))
                    mkdir($upload->path, 0777, true);
                
                if (isset($_POST['type_doc']))
                {
                    $type_doc = $_POST['type_doc'];
                
                    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
                    if (empty($doc->pdf_filename))
                        $doc->update_meta("pdf_filename", $doc->base_filename . ".pdf");
                    
                    $extension = explode(".", $_FILES['files']['name'][0]);
                    $extension = end($extension);

                    $upload->filename = $doc->pdf_filename = preg_replace('/pdf$/', $extension, $doc->pdf_filename);
                    
                    $doc->update_meta('html_filename', "");
                    $doc->update_meta('last_modif', current_time('timestamp'));
                    $doc->update_meta('infos_manquantes', array());
                    $doc->valid |= Document::SCAN;
                    $doc->valid &= ~Document::DRAFT;
                    
                    if ($_POST["signature_responsable"] == "true")
                    {
                        $reponse += $doc->mark_as_signed();
                    }
                    else
                        $doc->valid &= ~ Document::VALID_RESPONSABLE_DONE;
                    
                    if ($_POST["signature_client"] == "true")
                        $doc->valid |= Document::VALID_CLIENT_DONE;
                    else
                        $doc->valid &= ~ Document::VALID_CLIENT_DONE;
                        
                    if ($_POST["signature_stagiaire"] == "true")
                        $doc->valid |= Document::VALID_STAGIAIRE_DONE;
                    else
                        $doc->valid &= ~ Document::VALID_STAGIAIRE_DONE;
                    
                    $doc->update_meta('valid', $doc->valid);
                    
                    $doc->init_link_name();
                    
                    if ($wpof->slug == $wpof->url_gestion && $wpof->uri_params[1] == "signer" || ($wpof->slug == "session" && $wpof->uri_params[1] != "documents"))
                        $reponse['lignedoc'] = $doc->get_html_ligne((isset($_POST['cols'])) ? $_POST['cols'] : Document::COL_ALL);
                    else
                        $reponse['lignedoc'] = $doc->get_document_control();
                }
                else
                    $upload->filename = $_FILES['files']['name'][$i];
                
                if (file_exists($upload->path . $upload->filename))
                    rename($upload->path . $upload->filename, $upload->path . preg_replace("((.*)\.)", "$1-".date("Ymd_His").".", $upload->filename));
                move_uploaded_file( $_FILES['files']['tmp_name'][$i], $upload->path . $upload->filename);
                
                if (!isset($_POST['type_doc']))
                {
                    $upload->set_md5sum();
                    
                    $upload->timestamp = time();
                    $upload->date_text = date_i18n("j F Y H:i:s", $upload->timestamp);
                    
                    $session_formation->uploads[$upload->md5sum] = $upload;
                }
                
                $reponse['message'] .= "<span class='succes'>".__("Fichier correctement copié")." ".$upload->filename."</span>";
                $wpof->log->log_action(__FUNCTION__, $upload);
            }
        }
        update_post_meta($session_id, "uploads", serialize($session_formation->uploads));
        foreach ($session_formation->uploads as $u)
        {
            $reponse['filename'] .= $u->get_html('tr');
        }
    }
    elseif (!empty($_POST['user_id']))
    {
        $user_id = $_POST['user_id'];
        $formateur = new Formateur($user_id);
        if ($formateur !== null)
        {
            for ($i = 0; $i < $_POST['nb_files']; $i++)
            {
                if ($_FILES["files"]["error"][$i] > 0)
                {
                    $reponse['message'] .= "<span class='erreur'>".__("Erreur de téléversement code ")." ".$_FILES["files"]["error"][$i]." ".__("pour le fichier")." ".$_FILES["files"]["name"][$i]."</span>";
                }
                else
                {
                    $user_path = wpof_path_user_doc.'/'.$formateur->username;
                    $user_url = wpof_url_user_doc.'/'.$formateur->username;
                    if (!is_dir($user_path))
                        mkdir($user_path, 0777, true);
                    
                    if (isset($_POST['meta_key']))
                    {
                        $meta_key = $_POST['meta_key'];
                        $filename = $_FILES['files']['name'][$i];
                        $mimetype = $_FILES['files']['type'][$i];
                        move_uploaded_file($_FILES['files']['tmp_name'][$i], $formateur->doc_path.'/'.$filename);
                        
                        $formateur->update_meta($meta_key, $filename);
                        if (substr($mimetype, 0, 5) == "image")
                            $reponse['img'] = '<img src="'.$formateur->doc_url.'/'.$filename.'" id="'.$meta_key.'_img" />';
                        else
                            $reponse['link'] = '<a href="'.$formateur->doc_url.'/'.$filename.'" id="'.$meta_key.'_link">'.$filename.'</a>';
                    }
                }
            }
        }
    }
    else
    {
        $reponse['message'] .= "<span class='erreur'>".__("Aucune session ou formateur⋅ice à laquelle rattacher ce fichier")."</span>";
    }
    
    $log = $_POST;
    unset($log['files']);
    $reponse['log'] = $log;
    
    echo json_encode($reponse);

    die();
}

add_action('wp_ajax_delete_scan_file', 'delete_scan_file');
function delete_scan_file()
{
    require_once(wpof_path . "/class/class-upload.php");

    $reponse = array('message' => '', 'succes' => '');
    
    if (isset($_POST['session_id']) && $_POST['session_id'] > 0)
    {
        global $wpof;
        
        $session_id = $_POST['session_id'];
        $md5sum = $_POST['md5sum'];
        
        global $SessionFormation;
        $SessionFormation[$session_id] = new SessionFormation($session_id);
        $session_formation =& $SessionFormation[$session_id];
        
        $upload = $session_formation->uploads[$md5sum];
        
        if (file_exists($upload->path . $upload->filename))
        {
            unlink($upload->path . $upload->filename);
            $reponse['message'] = "<span class='succes'>".sprintf(__("Supression effective de %s"), $upload->path . $upload->filename)."</span>";
        }
        else
            $reponse['message'] = "<span class='erreur'>".sprintf(__("Le fichier %s n'existe pas"), $upload->path . $upload->filename)."</span>";
            
        $reponse['succes'] = true;
        $wpof->log->log_action(__FUNCTION__, $upload);
        unset($session_formation->uploads[$md5sum]);
        update_post_meta($session_id, "uploads", serialize($session_formation->uploads));
    }
        
    echo json_encode($reponse);

    die();
}
