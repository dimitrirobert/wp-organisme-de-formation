<?php
/*
 * class-session-stagiaire.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-document.php");
require_once(wpof_path . "/class/class-creneau.php");

class SessionStagiaire
{
    public $permalien = "";
    public $statut_stagiaire = "";
    public $attentes = "";
    public $date_modif_attentes = "aucune";
    public $date_entretien = "";
    public $etat_session = "initial";
    public $confirme = 1;
    public $temps; // Objet DateTime
    public $nb_heure; // au format horaire : 3:30
    public $nb_heure_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    public $nb_heure_estime_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    
    // Créneaux de session suivis par le stagiaire
    public $creneaux = array();
    public $creneaux_suivi = array();
    public $dates_array = array();
    public $dates_texte = "";
    
    public $eval_prerequis = array();
    public $eval_preform = array();
    public $eval_postform = array();
    
    public $doc_necessaire = array();
    public $doc_uid = array();
    public $doc_suffix = "";
    public $session_formation_id;
    public $prenom = "";
    public $nom = "";
    public $email = "";
    public $username = "";
    public $genre = "";
    public $client_id = -1;
    public $id = -1;
    
    public $fonction = "";
    
    // token d'accès aux infos
    public $token = "";
    public $token_time = 0;
    public $acces = 0;
    
    // table suffix
    private $table_suffix;
    
    public function __construct($session_id = -1, $id = -1)
    {
        global $SessionFormation;
        global $wpof;
        global $suffix_session_stagiaire;
        
        $this->table_suffix = $suffix_session_stagiaire;
        
        if ($session_id > 0)
        {
            $this->session_formation_id = $session_id;
            
            $session = get_session_by_id($session_id);
            
            //log_add(__METHOD__."({$this->get_displayname()})", "stagiaire");
            
            if ($id > 0)
            {
                $this->id = $id;
                global $SessionStagiaire;
                $SessionStagiaire[$this->id] = $this;
                
                $meta = $this->get_meta();
                
                foreach($meta as $m)
                {
                    if (!isset($this->{$m['meta_key']}))
                    {
                        log_add($m['meta_key']);
                        continue;
                    }
                    if (is_array($this->{$m['meta_key']}))
                    {
                        $array_value = unserialize($m['meta_value']);
                        if (is_array($array_value))
                            $this->{$m['meta_key']} = array_stripslashes($array_value);
                    }
                    else
                        $this->{$m['meta_key']} = stripslashes($m['meta_value']);
                }
                
                $client = get_client_by_id($this->session_formation_id, $this->client_id);
                
                // recherche du numéro de client si absent
                /*
                if ($this->client_id == -1)
                {
                    $session = get_session_by_id($this->session_formation_id);
                    $client_id = reset($session->clients);
                    while ($client_id !== false && $this->client_id == -1)
                    {
                        $client = get_client_by_id($this->session_formation_id, $client_id);
                        if (in_array($this->id, $client->stagiaires))
                            $this->client_id = $client_id;
                        else
                            $client_id = next($session->clients);
                    }
                }
                */
                
                $this->init_permalink();
                
                // nom d'utilisateur (non utilisé par OPAGA mais exportable)
                if (empty($this->username))
                    $this->update_meta("username", sanitize_user(strtolower(str_replace(" ", "-", $this->get_displayname()))));
            
                // documents nécessaires
                foreach($wpof->documents->term as $doc_index => $doc)
                {
                    if ($doc->contexte & $wpof->doc_context->stagiaire)
                        $this->doc_necessaire[] = $doc_index;
                }
                $this->doc_suffix = "c".$this->client_id."-s".$this->id;
                
                // les créneaux actifs sont stockés dans un tableau plat : l'id du créneau en clé et la valeur 0 (inactif) ou 1 (actif)
                if (!is_array($this->creneaux) && is_array($session->creneaux))
                    $this->init_creneaux();
                
                $this->calcule_temps_session();
            }
        }
    }
    
    public function init_permalink()
    {
        $session = get_session_by_id($this->session_formation_id);
        $client = get_client_by_id($session->id, $this->client_id);
        if ($client != null)
        {
            if ($client->entite_client == Client::ENTITE_PERS_PHYSIQUE)
                $this->permalien = $session->permalien.'client/'.$this->client_id;
            else
                $this->permalien = $session->permalien.'client/'.$this->client_id.'/'.$this->id;
        }
    }
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session stagiaire
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        if ($client->entite_client == Client::ENTITE_PERS_PHYSIQUE)
            return;

        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->session_formation_id, $wpof->doc_context->stagiaire, $this->id);
            $Documents[$doc->id] = $doc;
            $this->doc_uid[] = $doc->id;
        }
    }
    
    public function init_creneaux()
    {
        $creneaux = array();
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        foreach($client->creneaux as $key => $val)
            if ($val == 1)
                $creneaux[$key] = $val;
        $this->update_meta("creneaux", $creneaux);
    }
    
    /*
     * Retourne une métadonnée (ou toutes) de session stagiaire
     * $meta_key : clé de la valeur recherchée
     *
     * Retourne la valeur recherchée ou un tableau de toutes les valeurs
     */
    public function get_meta($meta_key = null)
    {
        return get_stagiaire_meta($this->id, $meta_key);
    }
    
    public function get_displayname($link = false, $anchor = "")
    {
        if ($link)
        {
            if (!empty($anchor) && substr($anchor, 0, 1) != '#')
                $anchor = '#'.$anchor;
            return '<a href="'.$this->permalien.$anchor.'">'.$this->prenom.' '.$this->nom.'</a>';
        }
        else
            return $this->prenom." ".$this->nom;
    }
    
    /*
     * Retourne une balise a avec le permalien et un texte au choix.
     * En option, une ancre
     */ 
    public function get_a_link($text, $anchor = "")
    {
        if (empty($text))
            $text = __("Définissez un texte pertinent pour ce lien");
        if (!empty($anchor) && substr($anchor, 0, 1) != '#')
            $anchor = '#'.$anchor;
        return '<a href="'.$this->permalien.$anchor.'">'.$text.'</a>';
    }
    

    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpdb, $wpof;
        
        if ($meta_value == "delete_meta")
            return $this->delete_meta($meta_key);
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key, $meta_value);
        
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
        
        if ($meta_key == "creneaux")
            $meta_value = $this->update_creneaux();
        
        if (is_array($meta_value))
            $meta_value = serialize($meta_value);

        if (isset($_SESSION['Stagiaire'][$this->id]))
            $_SESSION['Stagiaire'][$this->id]->$meta_key = $meta_value;
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, stagiaire_id, meta_key, meta_value)
            VALUES ('%d', '%d', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->id, $meta_key, $meta_value, $meta_value);
        
        $res = $wpdb->query($query);
        
        return $res;
    }
    
    private function update_creneaux()
    {
        global $wpdb, $wpof;
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        if ($client === null)
            return array();
        $creneaux = array_intersect_key($this->creneaux, $client->creneaux_actifs);
        $diff = array_diff_key($client->creneaux_actifs, $creneaux);
        if (!empty($diff))
            $creneaux = $creneaux + $diff;
        
        // aplatir le tableau s'il ne l'est pas
        return array_flatten($creneaux);
    }
    
    
    public function delete_meta($meta_key)
    {
        global $wpdb, $wpof;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare ("DELETE FROM $table WHERE meta_key = '%s' AND session_id = '%d' AND stagiaire_id = '%d';", $meta_key, $this->session_formation_id, $this->id);
        $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key);
        $this->$meta_key = null;
        
        return $wpdb->query($query);
    }
    
    
    // Renseigne le tableau des exercices comptables
    // En général ce tableau n'aura qu'une valeur (si toute la session se déroule dans la même année civile)
    public function set_exercice_comptable()
    {
        global $SessionFormation;
        if ($SessionFormation[$this->session_formation_id]->type_index == 'inter')
        {
            $this->exe_comptable = unserialize($this->get_meta("exe_comptable"));
            
            if (!is_array($this->exe_comptable) || count($this->exe_comptable) == 0)
            {
                $this->exe_comptable = array();
                foreach($this->dates_array as $d)
                {
                    $date = explode('/', $d);
                    $this->exe_comptable[$date[2]] = 0;
                }
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] = $this->tarif_total_chiffre;
                
                $this->update_meta("exe_comptable");
            }
            $diff = $this->tarif_total_chiffre - array_sum($this->exe_comptable);
            if ($diff != 0)
            {
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] += $diff;
                
                $this->update_meta("exe_comptable");
            }
        }
    }
    
    
    // Calcule le temps (en heures) de suis de la session par ce stagiaire (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        $this->temps = DateTime::createFromFormat("U", "0");
        $this->dates_array = array();
        
        foreach($session_formation->creneaux as $date => $tab_date)
            foreach($tab_date as $creno)
            {
                if (!isset($this->creneaux[$creno->id]) || !isset($client->creneaux[$creno->id]) || $this->creneaux[$creno->id] * $client->creneaux[$creno->id] != 0)
                {
                    if ($creno->type != "technique")
                        $this->temps->add($creno->duree);
                    $this->dates_array[] = $date;
                }
            }
        
        if (empty($this->dates_array))
            $this->dates_array = array_keys($session_formation->creneaux);
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        $h = $this->temps->format("U") / 3600;
        $m = ($this->temps->format("U") % 3600) / 60;
        $this->nb_heure_decimal = $this->temps->format("U") / 3600;
        $this->nb_heure = sprintf("%02d:%02d", $h, $m);
        
        if ($this->nb_heure_decimal == 0 && $client->nb_heure_estime_decimal > 0 && $client->nb_heure_estime_decimal != $this->nb_heure_estime_decimal)
            $this->update_meta("nb_heure_estime_decimal", $client->nb_heure_estime_decimal);

        if (empty($this->nb_heure_estime_decimal))
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
    }
    
    public function calcule_tarif()
    {
        if ($this->tarif_base_total == 1)
        {
            if ($this->nb_heure_decimal > 0)
                $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
        }
        else
            $this->tarif_total_chiffre = sprintf("%.2f", round($this->tarif_heure * $this->nb_heure_decimal, 1));
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
    }
    
    /*
     * Suppression du stagiaire pour cette session
     */
    public function delete()
    {
        global $wpof;
        
        if ($this->client_id > 0)
        {
            $client = get_client_by_id($this->session_formation_id, $this->client_id);
            $client->supprime_sous_entite("stagiaires", $this->id);
            
            // Vérification si le stagiaire est également le responsable ou le contact chez le client
            if ($client->responsable_stagiaire == $this->id)
                $client->update_meta("responsable_stagiaire", 0);
            if ($client->contact_stagiaire == $this->id)
                $client->update_meta("contact_stagiaire", 0);
        }
        
        // suppression du client en mémoire
        if (isset($_SESSION['Stagiaire'][$this->id]))
            unset($_SESSION['Stagiaire'][$this->id]);
        
        // suppression du stagiaire dans la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE stagiaire_id = '%d' AND session_id = '%d';",
            $this->id, $this->session_formation_id
        );
        $wpof->log->log_action(__FUNCTION__, $this);
        
        return $wpdb->query($query);
    }
    
    public function get_delete_bouton($texte_bouton = "", $reload = true)
    {
        ob_start();
        ?>
        <div class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-reload="<?php echo $reload; ?>">
            <?php the_wpof_fa_del(); ?>
            <?php echo $texte_bouton; ?>
        </div>
        <?php
        return ob_get_clean();
    }

    public function get_delete_icone($reload = true)
    {
        ob_start();
        ?>
        <span class="delete-entity icone" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-reload="<?php echo $reload; ?>">
            <?php the_wpof_fa_del(); ?>
        </span>
        <?php
        return ob_get_clean();
    }

    
    public function get_last_id()
    {
        global $wpdb, $suffix_session_stagiaire;
        $table = $wpdb->prefix.$suffix_session_stagiaire;
        
        $last_id = $wpdb->get_var("SELECT MAX(`stagiaire_id`) FROM $table;");
        if (!$last_id)
            $last_id = 0;
        
        return $last_id;
    }
    
    /*
     * Interface privée avec le stagiaire
     */
    public function get_the_interface($token)
    {
        global $wpof;
        $session = get_session_by_id($this->session_formation_id);
        $tabs = array
        (
            "presentation" => __("Votre formation"),
            "initial" => __("Avant la formation"),
            "documents" => __("Documents"),
            "apres" => __("Après la formation")
        );
        
        $current_slug = (empty($wpof->uri_params[0])) ? array_key_first($tabs) : $wpof->uri_params[0];
        if (!in_array($current_slug, array_keys($tabs)))
            return "";
        ob_start();
        
        ?>
        <p><?php echo $this->get_displayname(); ?></p>
        <h2><?php echo $session->titre_formation; ?></h2>
        <p><strong><?php _e("Dates"); ?></strong> <?php echo $session->dates_texte; ?></p>
        <div class="opaga_tabs">
            <ul>
            <?php foreach($tabs as $slug => $name) : ?>
                <li class="<?php echo ($slug === $current_slug) ? "ui-state-active" : ""; ?>"><a href="<?php echo $token->get_url($slug); ?>"><?php echo $name; ?></a></li>
            <?php endforeach; ?>
            </ul>
        </div>
        
        <div id="tab-<?php echo $current_slug; ?>" class="client white-board" data-id="<?php echo $this->id; ?>">
        <?php
        $get_private_func = "get_private_".$current_slug;
        echo $this->$get_private_func();
        ?>
        </div> <!-- <?php echo $current_slug; ?> -->
        <?php
        return ob_get_clean();
    }
    
    public function get_private_presentation()
    {
        $session = get_session_by_id($this->session_formation_id);
        ob_start();
        echo $session->get_html_presentation(array('entite' => $this));
        return ob_get_clean();
    }
    
    public function get_private_initial()
    {
        $session = get_session_by_id($this->session_formation_id);
        ob_start();
        
        echo $this->get_attentes_edit();
        ?>
        <form class="notif-modif">
        <?php
            if (!empty($session->quizpr->sujet))
            {
                echo "<h2>{$session->quizpr->sujet_titre}</h2>";
                echo $session->quizpr->get_html($this->id, $session->quizpr_id, 0);
            }
            if (!empty($session->quizobj->sujet))
            {
                echo "<h2>{$session->quizobj->sujet_titre} avant la formation</h2>";
                echo $session->quizobj->get_html($this->id, $session->quizobj_id, 0);
            }
        ?>
        <div class='icone-bouton submit stagiaire-submit'><?php the_wpof_fa_add(); ?><?php _e("Enregistrez vos modifications"); ?></div>
        </form>
        <p class="message"></p>
        <?php
        return ob_get_clean();
    }
    
    public function get_private_documents()
    {
        ob_start();
        global $Documents;
        $this->init_docs();
        $liste_doc = "";
        foreach ($this->doc_uid as $docuid)
        {
            $doc = $Documents[$docuid];
            if ($doc->visible)
                $liste_doc .= '<li class="doc '.$doc->type.'">'.$doc->link_name.'</li>';
        }
        if ($liste_doc != "") : ?>
        <ul class="liste-doc-stagiaire">
        <?php echo $liste_doc; ?>
        </ul>
        <?php else : ?>
        <p><?php _e("Pas de document pour l'instant. Vous serez averti lorsqu'ils seront prêts."); ?></p>
        <?php endif; ?>
        
        <?php
        return ob_get_clean();
    }
    
    public function get_private_apres()
    {
        $session = get_session_by_id($this->session_formation_id);
        ob_start();
        ?>
        <form>
        <?php
            if (!empty($session->quizobj->sujet))
            {
                echo "<h2>{$session->quizobj->sujet_titre} après la formation</h2>";
                echo $session->quizobj->get_html($this->id, $session->quizobj_id, 1);
            }
        ?>
        <div class='icone-bouton stagiaire-submit'><?php the_wpof_fa_add(); ?><?php _e("Enregistrez vos modifications"); ?></div>
        </form>
        <p class="message"></p>
        
        <?php
        return ob_get_clean();
    }
    
    /*
     * Tableau de bord stagiaire, pour le formateur
     */
    public function the_board()
    {
        echo $this->get_the_board();
    }
    public function get_the_board()
    {
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        $session = get_session_by_id($this->session_formation_id);
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        global $doc_nom;
        global $wpof;
        global $Documents;
        $this->init_docs();
        
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        ?>
        <div class='board board-stagiaire client-<?php echo $this->client_id; ?> stagiaire-<?php echo $this->id; ?>' id='tab-s<?php echo $this->id; ?>'>
            <h2><?php echo esc_html( $this->get_displayname() ); ?></h2>
            <div class="icones">            
                <p class="token-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-parent=".stagiaire-<?php echo $this->id; ?>">
                <?php the_wpof_fa_private(); ?>
                <?php _e("Envoyer accès privé"); ?>
                </p><?php echo get_icone_aide("envoi_token", "Accès privé"); ?>
                
                <?php echo $this->get_delete_bouton(__('Supprimer ce stagiaire'), false); ?>

                <?php if ($role == "admin"): ?>
                <p>ID : <?php echo $this->id; ?></p>
                <p>Client ID : <?php echo $this->client_id; ?></p>
                <?php endif; ?>

                <p><span class='last-modif'><?php //echo __("Dernière connexion") TODO : intégrer le log de connexion via l'espace privé du stagiaire'?></span></p>
            </div>

            <div class="infos-client">
                <div class="white-board edit-data">
                    <div class="info-generale-stagiaire">
                    <?php echo $wpof->toc->set_title("Informations générales", 2); ?>
                        <?php
                            echo get_input_jpost($this, "prenom", array('input' => 'text', 'label' => __("Prénom"), 'grid_col' => '2', 'needed' => true));
                            echo get_input_jpost($this, "nom", array('input' => 'text', 'label' => __("Nom"), 'grid_col' => '2', 'needed' => true));
                            echo get_input_jpost($this, "fonction", array('input' => 'text', 'label' => __("Fonction"), 'grid_col' => '2'));
                            if (champ_additionnel('genre_stagiaire'))
                                echo get_input_jpost($this, "genre", array('select' => '', 'label' => __("Genre"), 'grid_col' => '2', 'needed' => true));
                            echo get_input_jpost($this, "email", array('input' => 'text', 'label' => __("Adresse email"), 'grid_col' => '2', 'needed' => true));
                            echo get_input_jpost($this, "statut_stagiaire", array('select' => '', 'label' => __("Statut du stagiaire"), 'grid_col' => '2', 'needed' => true));
                            echo get_input_jpost($this, "nb_heure_estime_decimal", array('input' => 'number', 'label' => __("Durée estimée en heures (en décimal)"), 'grid_col' => '2', 'needed' => true)); 
                            echo get_input_jpost($this, "confirme", array('input' => 'checkbox', 'label' => __("Inscription confirmée"), 'grid_col' => '2'));
                        ?>
                    </div> <!-- .info-generale-stagiaire -->
                </div>
            </div>
            
            <?php
            $gest_docs = get_gestion_docs($this); 
            if ($gest_docs)
            {
                ?>
                <div class="white-board">
                    <?php echo $wpof->toc->set_title("Documents administratifs pour le/la stagiaire", 2); ?>
                    <?php echo $gest_docs; ?>
                </div>
                <?php
            }
            ?>
            <div class="white-board">
                <?php echo $wpof->toc->set_title("Créneaux de présence", 2); ?>
                <?php
                    if (count($session_formation->creneaux) == 0)
                        echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
                    else
                        echo $session_formation->get_html_creneaux(false, $this);
                ?>
            </div>
            <div class="white-board edit-data">
                <?php echo $wpof->toc->set_title("Évaluations et besoins", 2); ?>
                <?php echo $this->get_bilan_eval(); ?>
            </div>
                    
        </div> <!-- board-stagiaire -->
        <?php
        return ob_get_clean();
    }
    
    public static function get_stagiaire_control_head()
    {
        ob_start(); ?>
        <tr>
        <th><?php _e("ID"); ?></th>
        <th><?php _e("Prénom nom"); ?></th>
        <?php if (champ_additionnel('genre_stagiaire')) : ?>
        <th><?php echo __('Genre'); ?></th>
        <?php endif; ?>
        <th><?php _e("Fonction"); ?></th>
        <th><?php _e("Client"); ?></th>
        <th><?php _e("Session"); ?></th>
        <th><?php _e("Formateur⋅trice"); ?></th>
        <th><?php _e("Nature"); echo get_icone_aide("formation_nature_formation"); ?></th>
        <th id="order" data-order="desc"><?php _e("Première date"); ?></th>
        <th><?php _e("Dernière date"); ?></th>
        <th class="thin"><?php _e("Durée (h)"); ?></th>
        <th><?php _e("Statut"); ?></th>
        <th class="thin not_orderable"><?php _e("Supprimer"); ?></th>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    public function get_stagiaire_control()
    {
        global $wpof;
        $session = ($this->session_formation_id > 0) ? get_session_by_id($this->session_formation_id) : null;
        $client = ($this->client_id > 0) ? get_client_by_id($this->session_formation_id, $this->client_id) : null;
        $alerte = false;
        
        ob_start(); ?>
        <tr id="s<?php echo $this->id; ?>" class="editable client<?php echo $this->client_id; ?>" data-clientid="<?php echo $this->client_id; ?>">
        <td><?php echo $this->id; ?></td>
        <td><span class="hidden"><?php echo $this->nom; ?></span><?php echo $this->get_displayname(true); ?></td>
        <?php if (champ_additionnel('genre_stagiaire')) : ?>
        <td><?php echo get_input_jpost($this, "genre", array('select' => '', 'aide' => false)); ?></td>
        <?php endif; ?>
        <td><?php echo get_input_jpost($this, "fonction", array('input' => 'text', 'aide' => false)); ?></td>
        <td <?php if (!$client) : ?> class="bg-alerte" <?php endif; ?>>
        <?php
        if ($client)
            echo $client->get_displayname(true)." [<strong>".$client->id."</strong>]";
        else
        {
            echo '<span class="hidden">_</span>'.__("Pas de client !");
            $alerte = true;
        }
        ?>
        </td>
        <td <?php if (!$session) : ?> class="bg-alerte" <?php endif; ?>>
        <?php
        if ($session)
            echo $session->titre_formation.' ['.$session->get_id_link().']';
        else
        {
            echo '<span class="hidden">_</span>'.__("Pas de session !");
            $alerte = true;
        }
        ?>
        </td>
        <td><?php if ($session) echo $session->get_formateurs_noms(); ?></td>
        <td><?php echo $wpof->nature_formation->get_term($session->nature_formation); ?></td>
        <td><span class="hidden"><?php if ($session) echo $session->first_date_timestamp; ?></span><?php if ($session) echo $session->first_date; ?></td>
        <td><?php echo end($this->dates_array); ?></td>
        <td class="center thin" <?php if ($this->nb_heure_estime_decimal == 0) : $alerte = true; ?> class="bg-alerte" <?php endif; ?>>
        <?php echo get_input_jpost($this, "nb_heure_estime_decimal", array('input' => 'number', 'aide' => false)); ?></td>
        <td <?php if (empty($this->statut_stagiaire)) : $alerte = true; ?> class="bg-alerte" <?php endif; ?>>
        <?php echo get_input_jpost($this, "statut_stagiaire", array('select' => '', 'aide' => false)); ?>
        <span class="icone-bouton statut_stagiaire_client table-bouton"><?php _e('Appliquer sur tout le client'); ?></span>
        </td>
        <td class="center thin">
        <span class="delete-entity icone-bouton" data-objectclass="SessionStagiaire" data-id="<?php echo $this->id; ?>" data-parent="#s<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>"><?php the_wpof_fa_del(); ?></span>
        </td>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    public function get_attentes_edit()
    {
        global $wpof;
        ob_start();
        ?>
        <div class="attentes">
        <?php
        echo get_input_jpost($this, "attentes", array('textarea' => 1, 'rows' => '10', 'cols' => '60', 'label' => __("Attentes, besoins, motivations"), 'postprocess' => 'update_data', 'ppargs' => array('ppkey' => 'date_modif_attentes', 'ppvalue' => date("d/m/Y", time()), 'ppdest' => "#date_modif_attentes".$this->id, 'grid_col' => '6')));
        if ($wpof->role != null)
            echo get_input_jpost($this, "date_entretien", array("input" => "datepicker", "label" => "Date de l'entretien", 'grid_col' => '3'));
        ?>
        <div class="input_jpost derniere-modif" style="grid-column-end: span 3;"><label><?php _e("Date de dernière modification des attentes"); ?></label>
        <p id="date_modif_attentes<?php echo $this->id; ?>"><?php echo $this->date_modif_attentes; ?></p></div>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function get_bilan_eval()
    {
        global $wpof;
        $session = get_session_by_id($this->session_formation_id);
        $role = wpof_get_role(get_current_user_id());
        
        $html = "";
        $html .= $this->get_attentes_edit();
        
        $html .= "<div class='radio-disable'>";
        if (!empty($session->quizpr->sujet))
        {
            $html .= $wpof->toc->set_title($session->quizpr->sujet_titre, 3);
            $html .= $session->quizpr->get_html($this->id, $session->quizpr_id, 0);
        }
        if (!empty($session->quizobj->sujet))
        {
            $html .= $wpof->toc->set_title($session->quizobj->sujet_titre." avant la formation", 3);
            $html .= $session->quizobj->get_html($this->id, $session->quizobj_id, 0);
            $html .= $wpof->toc->set_title($session->quizobj->sujet_titre." après la formation", 3);
            $html .= $session->quizobj->get_html($this->id, $session->quizobj_id, 1);
        }
        $html .= "</div>";
        
        return $html;
    }

    public function get_doc_prenom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Prénom"));
        return $result[$arg];
    }
    public function get_doc_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Nom"));
        return $result[$arg];
    }
    public function get_doc_fonction($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => true, 'valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Fonction du/de la stagiaire"));
        return $result[$arg];
    }
    public function get_doc_email($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Adresse email"));
        return $result[$arg];
    }
    public function get_doc_statut_stagiaire($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            if ($wpof->$function_name->is_term($this->$function_name))
                $valeur = $wpof->$function_name->get_term($this->$function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "stagiaire:$function_name", 'desc' => __("Statut du stagiaire (cf. BPF)"));
        return $result[$arg];
    }
    public function get_doc_attentes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Attentes, besoins, motivations"));
        return $result[$arg];
    }
    public function get_doc_confirme($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Inscription confirmée"));
        return $result[$arg];
    }
    public function get_doc_nb_heure($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Nombre d’heures suivies"));
        return $result[$arg];
    }

}

function get_stagiaire_meta($stagiaire_id, $meta_key)
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    if ($meta_key)
    {
        $query = $wpdb->prepare
        ("SELECT meta_value
            from $table_session_stagiaire
            WHERE stagiaire_id = '%d'
            AND meta_key = '%s';",
            $stagiaire_id, $meta_key);
        return $wpdb->get_var($query);
    }
    else
    {
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_session_stagiaire WHERE stagiaire_id = '%d';", $stagiaire_id);
        return $wpdb->get_results($query, ARRAY_A);
    }
}

function get_stagiaire_by_id($session_id, $id)
{
    global $SessionStagiaire;
    
    if (!isset($SessionStagiaire[$id]))
        $SessionStagiaire[$id] = new SessionStagiaire($session_id, $id);
    
    return $SessionStagiaire[$id];
}

function get_all_stagiaires()
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    $stagiaires = array();
    $result = $wpdb->get_results("SELECT DISTINCT session_id, stagiaire_id FROM $table_session_stagiaire;");
    if (is_array($result))
        foreach($result as $row)
        {
            $s = new SessionStagiaire($row->session_id, $row->stagiaire_id);
            if (!empty($s->prenom))
                $stagiaires[] = $s;
        }
    
    return $stagiaires;
}

function get_stagiaire_displayname($id)
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    $query = $wpdb->prepare("SELECT meta_key, meta_value FROM $table_session_stagiaire WHERE stagiaire_id = '%d' AND (meta_key = 'prenom' OR meta_key = 'nom');", $id);
    $result = $wpdb->get_results($query);
    $prenom = $nom = "";
    foreach($result as $m)
        ${$m->meta_key} = $m->meta_value;
    
    return "$prenom $nom";
}

/*
 * Ajouter un stagiaire
 * Analyse des données du formulaire
 */
add_action( 'wp_ajax_add_stagiaire', 'add_stagiaire' );
function add_stagiaire($args = array())
{
    $reponse = array('message' => '');
    
    $session = null;
    $session_id = 0;
    $stagiaire = null;
    $ajax = false;
    $erreur = false;
    
    if (empty($args))
    {
        if (isset($_POST))
        {
            $args = $_POST;
            $ajax = true;
        }
        else
            return false;
    }
    
    if (debug)
        $reponse['log2'] = var_export($args, true);
    
    if (isset($args['session_id'])) $session_id = $args['session_id'];
    if (isset($args['client_id'])) $client_id = $args['client_id'];
        
    if ($session_id > 0)
    {
        $session = get_session_by_id($session_id);
        
        if ($client_id > 0)
        {   
            $client = get_client_by_id($session_id, $client_id);
            $stagiaire = new SessionStagiaire($session_id);
            
            if (isset($args['source']))
            {
                $source = $args['source'];
                if ($client->{$source . "_stagiaire"} > 0)
                {
                    $reponse["message"] = '<span class="erreur">'.__("Ce stagiaire existe déjà !").'</span>';
                    $erreur = true;
                }
                else
                {
                    $identite = explode(' ', $client->$source, 2);
                    $args['prenom'] = $identite[0];
                    $args['nom'] = (isset($identite[1])) ? $identite[1] : "Nom";
                    $args['email'] = $client->{$source . "_email"};
                }
            }
            
            if (!$erreur)
            {
                global $wpof;
                foreach(array('genre', 'prenom', 'nom', 'email') as $field)
                    $stagiaire->$field = (isset($args[$field])) ? $args[$field] : "";
                
                if ($stagiaire->prenom != "" && $stagiaire->nom != "")
                {
                    $stagiaire->client_id = $client_id;
                    $stagiaire_id = $stagiaire->id = $stagiaire->get_last_id() + 1;
                    $stagiaire->init_permalink();
                    $wpof->log->log_action(__FUNCTION__, $stagiaire);
                    
                    $stagiaire->update_meta("prenom");
                    $stagiaire->update_meta("nom");
                    $stagiaire->update_meta("email");
                    $stagiaire->update_meta("genre");
                    $stagiaire->update_meta("username", sanitize_user(strtolower(str_replace(" ", "-", $stagiaire->get_displayname()))));
                    $stagiaire->update_meta("confirme", 1);
                    $stagiaire->update_meta("client_id", $client_id);
                    $stagiaire->init_creneaux();
                    $reponse['message'] .= "<span class='succes'>".$stagiaire->get_displayname()." ".__("ajouté⋅e")."</span>";
                }
                
                $client->stagiaires[] = $stagiaire->id;
                $client->update_meta("stagiaires", $client->stagiaires);
                if (debug)
                    $reponse['log2'] .= var_export($stagiaire, true);
                
                if (isset($source))
                    $client->update_meta($source."_stagiaire", $stagiaire->id);
            }
        }
    }
    
    if ($reponse['message'] == "")
    {
        $reponse['message'] .= "<span class='erreur'>".__("Saisissez un prénom et un nom pour inscrire un⋅e stagiaire")."</span>";
        $reponse['erreur'] = $reponse['message'];
    }
    else
    {
        $reponse['succes'] = $reponse['message'];
    }
    
    if ($ajax)
    {
        
        echo json_encode($reponse);
        die();
    }
    elseif ($stagiaire)
        return $stagiaire;
    else
        return $reponse;
}
