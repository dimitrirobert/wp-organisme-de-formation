<?php
/*
 * class-stat.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


require_once(wpof_path . "/class/class-question.php");
require_once(wpof_path . "/class/class-qreponse.php");

class Stat
{
    public int $id = -1;
    public String $title = "";
    public String $description = "";
    public Array $theme = [];
    public Array $question_id = [];
    
    public function __construct($id = -1)
    {
        $this->id = $id;

        if ($id > 0)
        {
            foreach($this->get_meta() as $m)
            {
                if (is_array($this->{$m->meta_key}))
                    $this->{$m->meta_key} = json_decode(stripslashes($m->meta_value), true) ?? [];
                else
                    $this->{$m->meta_key} = stripslashes($m->meta_value);
            }
        }
    }

    public function get_displayname(bool $link = false): String
    {
        return $this->title;
    }

    /**
     * Retourne une ou toutes les meta_value de la stat
     */
    public function get_meta(String $meta_key = null): mixed
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_statmeta;
        
        if ($meta_key !== null)
        {
            $query = $wpdb->prepare
            ("SELECT meta_value
                FROM ".$table.
                "WHERE stat_id = '%d'
                AND meta_key = '%s';",
                $this->id, $meta_key);
            return $wpdb->get_var($query);
        }
        else
        {
            $query = $wpdb->prepare("SELECT meta_key, meta_value FROM ".$table." WHERE stat_id = '%d';", $this->id);
            $meta = $wpdb->get_results($query);
            return $meta;
        }
    }

    public static function get_all_stats_id(): Array
    {
        global $wpdb, $wpof;
        
        $table = $wpdb->prefix.$wpof->suffix_statmeta;
        
        $query = "SELECT DISTINCT stat_id FROM ".$table.";";

        return $wpdb->get_col($query);
    }

    /**
     * Update d'une meta key
     */
    public function update_meta(String $meta_key, String|Array $meta_value = null)
    {
        global $wpdb, $wpof;
        
        $table = $wpdb->prefix.$wpof->suffix_statmeta;

        if ($meta_value === null)
            $meta_value = $this->meta[$meta_key];
        else
            $this->meta[$meta_key] = $meta_value;

        if (is_array($meta_value))
            $meta_value = json_encode($meta_value);

        $query = $wpdb->prepare("DELETE FROM ".$table." WHERE stat_id = '%d' AND meta_key = '%s';", $this->id, $meta_key);
        $res['delete '.$meta_key] = $wpdb->query($query);

        $query = $wpdb->prepare
        ("INSERT INTO $table (stat_id, meta_key, meta_value) VALUES ('%d', '%s', '%s');",
            $this->id, $meta_key, $meta_value);
        
        $res[$meta_value] = $wpdb->query($query);
        return $res;
    }


    /**
     * Retourne le dernier id de la stat enregistré dans la bas de données
     */
    public function get_last_id(): int
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_statmeta;
        
        $last_id = $wpdb->get_var("SELECT MAX(`stat_id`) FROM $table;");
        if (!$last_id)
            $last_id = 0;
        
        return $last_id;
    }

    /**
     * Interface de création et configuration d'une statistique
     */
    public function get_edit(): String
    {
        global $wpof, $qmanager;

        if (!isset($qmanager))
            $qmanager = new QManager();

        ob_start(); ?>
        <form class="add_stat grid-4-col grid-forms">
            <input type="hidden" name="stat_id" value="<?php echo $this->id; ?>" />
            <div><label for="title" class="top"><?php _e("Titre"); ?></label><input name="title" id="title" type="text" value="<?php echo $this->title; ?>" /></div>
            <div><label for="description" class="top"><?php _e("Description"); ?></label><input name="description" id="description" type="text" value="<?php echo $this->description; ?>" /></div>
            <div class="grid-2"><label for="question_id" class="top"><?php _e("Questions dont les réponses seront utilisées"); ?></label>
            <select id="question_id" name="question_id" multiple="multiple">
                <?php foreach($qmanager->get_needed_questions('likert') as $id => $q) : ?>
                    <option value="<?php echo $id; ?>"><?php echo $q->title; ?></option>
                <?php endforeach; ?>
            </select>
            </div>
            <span class="bouton submit" data-function="save_stat" data-reload="1"><?php _e("Enregistrer"); ?></span>
        </form>

        <?php
        return ob_get_clean();
    }

    public function the_value(String $contexte = null, String $value = null)
    {
        $stat_value = $this->get_value($contexte, $value);
        if ($stat_value)
            echo number_format($stat_value, 2, ',')." %";
    }

    /**
     * Retourne la valeur après calcul de la moyenne des réponses
     * 
     * $contexte peut contenir les clés :
     * * tag : filtre par tag associé aux formations et aux sessions
     * * nature : filtre par nature de formation
     * * formateur : filtre par formateur (sur toutes ses sessions)
     * * formation : filtre par formation (toutes les sessions de cette formation)
     * 
     * Si une formation n'a pas de stats, on renvoie les stats du formateur⋅ice
     * Si un autre contexte n'a pas de stats, on renvoie false
     * Si $contexte est vide, on compile toutes les réponses
     */
    public function get_value(String $contexte = null, String $value = null): float|bool
    {
        global $wpof, $wpdb;

        // On ne peut avoir de contexte sans valeur
        if (($value === null || $value == "") && $contexte !== null)
            $contexte = null;

        //error_log(var_export($contexte, true)." : ".var_export($value, true));
        $global_session_id = $wpof->get_actions_id('session');

        if ($contexte == 'tag')
            $global_session_id = $wpof->get_sessions_id_by_term($value);

        if ($contexte == 'nature' && $wpof->nature_formation->is_term($value))
        {
            $query = $wpdb->prepare(
                "SELECT DISTINCT post_id FROM ".$wpdb->prefix."postmeta".
                " WHERE post_id IN ('".implode("','", $global_session_id)."')".
                " AND meta_key = 'formation' AND meta_value IN ".
                "(SELECT DISTINCT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key = 'nature_formation' AND meta_value = %s);",
                $value);
            $global_session_id = $wpdb->get_col($query);
        }

        if (empty($global_session_id))
        {
            //error_log("empty list ".$contexte." ".$value);
            return false;
        }

        $formation_session_id = [];
        if ($contexte == 'formation')
        {
            $query = $wpdb->prepare(
                "SELECT DISTINCT post_id FROM ".$wpdb->prefix."postmeta".
                " WHERE meta_key = 'formation' AND meta_value = %s AND post_id IN ('".implode("','", $global_session_id)."');",
                $value);
            $formation_session_id = $wpdb->get_col($query);
            //error_log("formation ".$query);

            if (!empty($formation_session_id))
                $global_session_id = $formation_session_id;
        }

        $formateur_session_id = [];
        if ($contexte == 'formateur' || ($contexte == "formation" && empty($formation_session_id)))
        {
            //error_log(implode(',', $wpof->get_actions_id("session", $value)));
            $formateur_session_id = array_intersect($wpof->get_actions_id("session", $value), $global_session_id);

            if (!empty($formateur_session_id))
                $global_session_id = $formateur_session_id;
            else
                return false;
        }

        return $this->get_moyenne_value($global_session_id);
    }

    private function get_moyenne_value(Array $session_id): float|bool
    {
        global $wpof, $wpdb;

        $query = $wpdb->prepare("SELECT AVG(reponse) FROM ".$wpdb->prefix.$wpof->suffix_qreponse.
        " WHERE session_id IN ('".implode("','", $session_id)."') AND question_id IN ('".implode("','", $this->question_id)."');");
        $result = $wpdb->get_var($query);

        if ($result !== null)
            return $result;
        else
            return false;
    }
}

add_action('wp_ajax_save_stat', 'save_stat');
function save_stat()
{
    $reponse = array('log' => $_POST);
    
    $stat = new Stat($_POST['stat_id']);
    if ($stat->id == -1)
        $stat->id = $stat->get_last_id() + 1;

    $_POST['question_id'] = (array)(json_decode(stripslashes($_POST['question_id'])));
    foreach($_POST as $key => $value)
    {
        if (property_exists($stat, $key))
        {
            $reponse[] = $stat->update_meta($key, $value);
        }
    }
    
    echo json_encode($reponse);
    die();
}


add_shortcode('opagastat_titre', 'opagastat_title_shortcode');
function opagastat_title_shortcode($atts)
{
    // Attributes
    $atts = shortcode_atts(array (0 => 'stat_id'), $atts);
    if ($atts[0] != 'stat_id')
    {
        $stat = new Stat($atts[0]);
        if (!empty($stat->title))
            return apply_filters('opaga_display_stat_title', $stat->title);
    }
    return "";
}

add_shortcode('opagastat', 'opagastat_shortcode');
function opagastat_shortcode($atts)
{
    global $wpof;
    $text = "";
    
    // Attributes
    $atts = shortcode_atts(
        array
            (
            0 => 'stat_id',
            'contexte' => null,
            'valeur' => null,
            'decimale' => 1,
            'notitre' => null
            ),
        $atts
    );

    if ($atts[0] != 'stat_id')
    {
        $stat = new Stat($atts[0]);
        if (empty($stat->title))
            return $text;

        $text = apply_filters('opaga_display_stat', $text, $stat);
        if (empty($text))
        {
            ob_start();
            if ($atts['contexte'] == 'tag' && $atts['valeur'] == 'all')
            {
                foreach(get_terms(array('taxonomy' => 'post_tag', 'hide_empty' => false)) as $tag)
                {
                    $stat_value = $stat->get_value($atts['contexte'], $tag->slug);
                    if ($stat_value !== false) : ?>
                    <div class="stat-bloc" title="<?php echo $stat->description; ?>">
                        <?php if ($atts['notitre'] === null) : ?>
                        <p class="stat-title"><?php echo $stat->title; ?></p>
                        <?php endif; ?>
                        <p class="stat-tag"><?php echo $tag->name; ?></p>
                        <p class="stat-value"><?php echo number_format($stat_value, $atts['decimale'], ','); ?> %</p>
                    </div>
                    <?php endif;
                }
            }
            $stat_value = $stat->get_value($atts['contexte'], $atts['valeur']);
            if ($stat_value !== false) : ?>
            <div class="stat-bloc" title="<?php echo $stat->description; ?>">
                <?php if ($atts['notitre'] === null) : ?>
                <p class="stat-title"><?php echo $stat->title; ?></p>
                <?php endif; ?>
                <p class="stat-value"><?php echo number_format($stat_value, $atts['decimale'], ','); ?> %</p>
            </div>
            <?php endif;

            $text = ob_get_clean();
        }
    }
    return $text;
}

/**
 * Wrapper de contruction d'une Stat
 * 
 * Stockage dans la globale $Stat
 * 
 * Si elle existe déjà, la stat est simplement renvoyée
 */
function get_stat_by_id($id): Stat
{
    global $Questonnaire;
    
    if (!isset($Stat[$id]))
    {
        $Stat[$id] = new Stat($id);
    }
    return $Stat[$id];
}