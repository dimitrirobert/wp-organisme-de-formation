<?php
/*
 * class-info.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-opaga-content.php");

class Info extends OpagaContent
{
    public  int $id = -1;
    public  String $text = "";
    public  String $category = "";
    public  String $date = "";


    public function __construct(int $id = -1)
    {
        global $wpof;
        $this->type = 'info';
        parent::__construct($id);
        if ($id > 0)
        {
            $this->edit_permalink = home_url().'/'.$wpof->url_gestion.'/info/edit/?info_id='.$id;
            $this->display_permalink = home_url().'/'.$wpof->url_info.'/'.$id;
        }
    }

    /**
     * Récupère la liste des catégories d'info
     */
    public function get_info_categories() : Array
    {
        return parent::get_categories($this->type);
    }

    public function get_pretty_date() : String
    {
        $date = date_create_from_format("Y-m-d", $this->date);
        return pretty_print_dates($date->format("d/m/Y"));
    }

    /**
     * Affichage de l'info
     * @return string
     */
    public function get_display($viewed = false) : String
    {
        $cat = OpagaContent::get_category($this->category);
        if ($viewed)
            $class = "bg-neutral-100";
        else
            $class = "bg-white";
        
        ob_start();
        ?>
        <div id="info-<?php echo $this->id ?>" role="info" class="mt-3 relative flex flex-col w-full p-3 rounded-xl shadow-lg <?php echo $class; ?> text-gray-800">
            <div class="flex">
                <div class="space-y-2 me-5">
                    <div class="whitespace-nowrap rounded-md bg-orange-600 px-2 py-1 text-white ring-1 ring-inset ring-green-700/10 h-8"><?php echo $this->get_pretty_date(); ?></div>
                    <?php if ($cat !== null) : ?>
                    <div class="whitespace-nowrap text-orange-600 font-bold">#<?php echo $cat->name; ?></div>
                    <?php endif; ?>
                </div>
                <div role="description" class="line-clamp-2 hover:line-clamp-none w-11/12">
                    <?php echo stripslashes($this->text); ?>
                </div>
            </div>
            <div class="unfold flex items-center justify-center bg-neutral-50 rounded-b-md mt-2 cursor-pointer">
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 448 512" class="h-5 w-5 text-slate-600"><path d="M201.4 374.6c12.5 12.5 32.8 12.5 45.3 0l160-160c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L224 306.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l160 160z"/></svg>
            </div>
            
            <?php if(! $viewed): ?>
            <div class="close-info flex items-center justify-center transition-all w-8 h-8 rounded-md text-white hover:bg-gray-200 active:bg-slate-200 cursor-pointer absolute top-1.5 right-1.5" type="button" data-id="<?php echo $this->id; ?>">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="h-5 w-5 text-slate-600" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12"></path></svg>
            </div>
            <?php endif; ?>
        </div>
        <?php
        return ob_get_clean();
    }

    /**
     * Retourne un tableau d'Info selon les critères choisis
     * @param array $args : paramètres de filtres sous la forme key => value
     * @return array
     */
    public static function get_infos($viewed = false) : array
    {
        global $wpdb, $wpof;
        $table_content = $wpdb->prefix.$wpof->suffix_content;
        $table_content_rel = $wpdb->prefix.$wpof->suffix_content_rel;

        if ($viewed)
            $select = $wpdb->prepare("SELECT id_content FROM ".$table_content_rel." WHERE viewed = 1 AND id_user = %d limit 20;", get_current_user_id());
        else
            $select = $wpdb->prepare("SELECT id FROM ".$table_content." WHERE id NOT IN (SELECT id_content FROM ".$table_content_rel." WHERE id_user = %d AND viewed = 1) limit 20;", get_current_user_id());
        
        $contents = [];
        foreach($wpdb->get_col($select) as $id)
            $contents[$id] = new Info($id);

        return $contents;
    }

    /**
     * Met viewed à 1 pour l'utilisateur courant
     * @return array
     */
    public function switch_state() : Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_content_rel;
        $select = $wpdb->prepare("SELECT viewed FROM ".$table." WHERE id_user = %d AND id_content = %d", get_current_user_id(), $this->id);
        $viewed = $wpdb->get_var($select);
        if ($viewed == null)
            $query = $wpdb->prepare("INSERT INTO ".$table." (id_user, id_content, viewed) VALUES (%d, %d, %d);", get_current_user_id(), $this->id, 1);
        else
            $query = $wpdb->prepare("UPDATE ".$table." SET viewed = 1 WHERE id_user = %d AND id_content = %d;", get_current_user_id(), $this->id);
        
        return [ 'res' => $wpdb->query($query), "id" => $this->id ];
    }
}

add_action('wp_ajax_update_info_state', 'update_info_state');
function update_info_state()
{
    $post = (object) $_POST;
    $reponse = [];

    if (isset($post->content_id))
    {
        $info = new Info($post->content_id);
        if (get_class($info) == "Info")
        {
            $reponse = $info->switch_state();
        }
    }
    echo json_encode($reponse);
    die();
}