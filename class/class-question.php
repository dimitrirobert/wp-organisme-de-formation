<?php
/*
 * class-question.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Question
{
    public int $id;
    public int $author_id = 0;
    public String $qtype = "";
    public String $title = "";
    public String $level = "";
    public Object $options;
    public bool $needed = false; // n'a de sens que dans un questionnaire
    
    public function __construct($id = -1)
    {
        $this->id = $id;
        $this->options = (object) [];
        if ($id > -1)
        {
            $this->init_from_db();
        }
    }
    
    public function __toString()
    {
        return "[".$this->qtype."] ".$this->title;
    }
    
    public function init_from_db()
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_question;
        
        $query = $wpdb->prepare("SELECT * FROM $table WHERE id = '%d';", $this->id);
        
        $result = $wpdb->get_row($query, ARRAY_A);

        if (!empty($result))
        {
            foreach($result as $key => $val)
            {
                if (is_array($this->$key))
                {
                    $array_value = json_decode($val);
                    $this->$key = (is_array($array_value)) ? array_map(function($val) { return stripslashes($val); }, $array_value) : [];
                }
                elseif (is_object($this->$key))
                {
                    $array_value = json_decode($val);
                    $this->$key = (is_object($array_value)) ? $array_value : (object)[];
                }
                else
                    $this->$key = stripslashes($val);
            }
        }
    }
    
    /**
     * Retourne le dernier id de la question enregistré dans la bas de données
     */
    public function get_last_id(): int
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_question;
        
        $last_id = $wpdb->get_var("SELECT MAX(`id`) FROM $table;");
        if (!$last_id)
            $last_id = 0;
        
        return $last_id;
    }

    /**
     * Retourne un tableau contenant les id des questionnaires où est utilisée cette question
     */
    public function get_linked_questionnaire($exclude = -1): Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_question_relationships;

        $query = $wpdb->prepare("SELECT questionnaire_id FROM ".$table." WHERE question_id = '%d' AND questionnaire_id != '%d';", $this->id, $exclude);
        return $wpdb->get_col($query);
    }

    /**
     * Retourne les réponses faites à cette question
     */
    public function get_reponses_repondeur_id(): Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_qreponse;

        $query = $wpdb->prepare("SELECT DISTINCT repondeur_id FROM ".$table." WHERE question_id = '%d';", $this->id);
        return $wpdb->get_col($query);
    }

    /** 
     * Vérifie si la question a déjà des réponses
     */
    public function has_reponses(): bool
    {
        return !empty($this->get_reponses_repondeur_id());
    }

    /**
     * Retourne la liste des stat_id où ette question est utilisée
     */
    public function get_stat_id(): Array
    {
        global $wpof, $wpdb;

        $query = $wpdb->prepare("SELECT s.stat_id as stat_id FROM ".$wpdb->prefix.$wpof->suffix_statmeta." as s, ".$wpdb->prefix.$wpof->suffix_question." as q ".
            "WHERE s.meta_key = 'question_id' ".
            "AND s.stat_id = q.id ".
            "AND JSON_SEARCH(REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(s.meta_value, 'i:\\\d*;s:\\\d*:', ''), ';', ','), '.*{(.*)}', '[\\\\1]'), 'one', %d) is not null;",
            $this->id);

        return $wpdb->get_col($query);
    }
    
    public function update(): Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_question;
        
        $query = $wpdb->prepare
        ("INSERT INTO ".$table." (id, title, level, author_id, qtype, options)
            VALUES ('%d', '%s', '%s', '%d', '%s', '%s')
            ON DUPLICATE KEY UPDATE title = '%s', level = '%s', options = '%s';",
            $this->id, $this->title, $this->level, get_current_user_id(), $this->qtype, json_encode($this->options, JSON_FORCE_OBJECT),
            $this->title, $this->level, json_encode($this->options, JSON_FORCE_OBJECT)
        );

        $res = $wpdb->query($query);
        return [ "question_update", $res, $query ];
    }

    /**
     * Supprimer une question dans la base de données
     */
    public function delete(bool $force = false): bool
    {
        global $wpdb, $wpof;

        $result = true;

        // recherche de réponses à cette question
        $table = $wpdb->prefix.$wpof->suffix_qreponse;
        $query = $wpdb->prepare("SELECT repondeur_id FROM ".$table." WHERE question_id = %d;", $this->id);
        $reponses = $wpdb->get_col($query);

        if (count($reponses) > 0)
        {
            if ($force)
            {
                $query = $wpdb->prepare("DELETE FROM ".$table." WHERE question_id = %d;", $this->id);
                $result = boolval($wpdb->query($query));
            }
            else
                $result = false;
        }

        if (!$result)
            return $result;

        // recherche de questionnaires où cette question est incluse
        $table = $wpdb->prefix.$wpof->suffix_question_relationships;
        $query = $wpdb->prepare("SELECT questionnaire_id FROM ".$table." WHERE question_id = %d;", $this->id);
        $questionnaires = $wpdb->get_col($query);

        if (count($questionnaires) > 0)
        {
            if ($force)
            {
                $query = $wpdb->prepare("DELETE FROM ".$table." WHERE question_id = %d;", $this->id);
                $result = boolval($wpdb->query($query));
            }
            else
                $result = false;
        }

        if ($result)
        {
            $table = $wpdb->prefix.$wpof->suffix_question;
            $query = $wpdb->prepare("DELETE FROM ".$table." WHERE id = %d;", $this->id);
            $result = boolval($wpdb->query($query));
        }

        return $result;
    }
    
    /**
     * Retourne l'interface de modification d'une question
     */
    public function get_edit(int $questionnaire_id): String
    {
        global $wpof, $Questionnaire;
        $uid = time();
        $new_line = ($this->title == "") ? "data-new='".$uid."'" : "";
        $type_term = $wpof->questionnaire_type_question->term[$this->qtype];

        $warning = $this->get_warnings($questionnaire_id);
        $title_warning = $warning['title'] ?? "";

        ob_start();
        ?>
        <li class="liq <?php echo $this->qtype; if(!empty($type_term->noquestion)) echo " noquestion"; ?>" <?php echo $new_line; ?> data-qtype="<?php echo $this->qtype; ?>" data-id="<?php echo $this->id; ?>">
        <div class="flexrow margin">
            <span class="big"><?php the_wpof_fa("up-down"); ?></span>
            <input type="text" name='title' value="<?php echo stripslashes($this->title); ?>" />
            <div class="question-type"><?php echo $type_term->text; ?> </div>

            <table class="icons">
            <tr>
            <td>
            <?php if (!empty($type_term->options)) : ?>
            <span class="big question action" data-function="toggle_options" title="<?php _e('Afficher les options'); ?>"><?php the_wpof_fa("gear"); ?></span>
            <?php endif; ?>
            </td>
            <td>
            <?php if (!empty($title_warning)) : ?>
                <span class="big question action alerte" data-function="toggle_options" title="<?php echo $title_warning; ?>"><?php the_wpof_fa("triangle-exclamation"); ?></span>
            <?php endif; ?>
            </td>
            <td>
                <span class="big question action <?php echo ($this->needed) ? "important" : "disabled"; ?>" data-function="toggle_needed" title="<?php _e("Rendre la question utilisable en statistique via CE questionnaire"); ?>"><?php the_wpof_fa('asterisk'); ?></span>
                <input type="hidden" name="needed" value="<?php echo ($this->needed); ?>" />
            </td>
            <td><span class="big question action" data-function="question-dupliquer" title="<?php _e('Dupliquer cette question'); ?>"><?php the_wpof_fa("copy", 'regular'); ?></span></td>
            <td>
            <span class="big question action delete" data-function="question-retirer" title="<?php _e('Retirer cette question de CE questionnaire'); ?>"><?php the_wpof_fa("right-from-bracket"); ?></span>
            </td>
            </tr>
            </table>

        </div>
        <div class="blocHidden options">
            <?php if (!empty($type_term->options)) : ?>
            <div class="flexrow margin">
            <?php
            $choice = (in_array('choice', array_keys($type_term->options)));
            ?>
            <?php foreach($type_term->options as $key => $text) : ?>
                <?php if ($key != 'choice') : ?>
                <input type="text" placeholder="<?php echo $text; ?>" name="<?php echo $key; ?>" value="<?php if (!empty($this->options->$key)) echo stripslashes($this->options->$key); ?>"/>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if ($choice) : ?>
                <div class="choices">
                    <?php
                        if (!empty($this->options->choice))
                        {
                            foreach($this->options->choice as $c)
                                echo $this->get_choice_edit(stripslashes($c));
                        }
                        else
                        {
                            for($i = 0; $i < 3; $i++) // on propose trois choix vides par défaut
                                echo $this->get_choice_edit();
                        }
                    ?>
                    <div class="center question action big" data-function="add_choice"> <?php the_wpof_fa_add(); ?> </div>
                </div>
            <?php endif; ?>
            </div>
            <?php endif; ?>

            <?php if (!empty($warning)) : ?>
            <div class="question-alerte">
                <?php
                    unset($warning['title']);
                    echo implode('<br />', $warning);
                ?>
            </div>
            <?php endif; ?>
        </div>
        </li>
        <?php
        
        return ob_get_clean();
    }

    /**
     * Retourne la liste des warnings
     * 
     * * la question est utilisée dans au moins un autre questionnaire (liste des questionnaires)
     * * la question a déjà des réponses enregistrées
     * * la question est utilisée dans au moins une statistique
     * 
     * $warning['title'] contient le title affiché sur l'icône de warning
     * 
     */
    private function get_warnings(int $questionnaire_id): Array
    {
        $warning = [];
        $title = [];

        if ($this->has_reponses())
        {
            $warning[] = __('<strong>Attention</strong> cette question a déjà reçu des réponses');
            $title[] = __("Cette question a déjà des réponses");
        }

        if (!empty($linked_s = $this->get_stat_id()))
        {
            $listeS = [];
            foreach($linked_s as $stid)
            {
                $s = get_stat_by_id($stid);
                $listeS[] = $s->get_displayname(true);
            }
            $title[] = __("Question utilisée dans au moins une statistique");
            $warning[] = __("<strong>Attention</strong>, cette question est utilisée dans les statistiques suivantes :");
            $warning = array_merge($warning, $listeS);
        }

        if (!empty($linked_q = $this->get_linked_questionnaire($questionnaire_id)))
        {
            $listeQ = [];
            foreach($linked_q as $qid)
            {
                $q = get_questionnaire_by_id($qid);
                $listeQ[] = $q->get_displayname(true, true);
            }
            $title[] = __("Question utilisée dans d'autres questionnaires");
            $warning[] = __("<strong>Attention</strong>, cette question est également utilisée dans les questionnaires suivants :");
            $warning = array_merge($warning, $listeQ);
        }

        if (!empty($title))
        {
            array_unshift($warning, '<span class="alerte">'.__("Ne modifiez pas le sens de cette question ni de ses options !").'</span>');
            $warning['title'] = implode("\n", $title);
        }

        return $warning;
    }

    public function get_choice_edit(String $choice = ""): String
    {
        ob_start();
        ?>
        <div class="choice"><input type="text" name="choice[]" value="<?php echo $choice; ?>" /> <span class="question action delete" data-function="del_choice"><?php the_wpof_fa_del(); ?></span></div>
        <?php
        return ob_get_clean();
    }

    /**
     * Renvoie le contenu HTML d'une question pour affichage et demande de réponse
     */
    public function get_display(QReponse $reponse = null): String
    {
        global $wpof;
        $type_term = $wpof->questionnaire_type_question->term[$this->qtype];

        ob_start(); ?>
        <div class="question <?php echo $this->qtype; ?>">
        <p class="title"><?php echo stripslashes($this->title); ?> <?php if ($this->needed && is_user_logged_in()) the_wpof_fa('asterisk', 'important', __('utilisé pour les statistiques')); ?></p>
        <?php
        if (!isset($type_term->noquestion))
        {
            if ($reponse === null)
                $reponse = new QReponse();
            $function = 'the_'.$this->qtype;
            if (method_exists($this, $function))
                $this->$function($reponse);
        }
        ?>
        </div>
        <?php
        return ob_get_clean();
    }

    private function the_likert(QReponse $reponse)
    {
        $step = 100 / ((int) ($this->options->size) - 1);
        if (isset($this->output) && $this->output == 'pdf') : ?>
        <table class="likert">
        <tbody>
            <tr class="legend">
                <td class="left" colspan="<?php echo $this->options->size / 2; ?>"><?php echo stripslashes($this->options->legend_min); ?></td>
                <?php if ($this->options->size % 2 == 1) : ?><td></td><?php endif; ?>
                <td class="right" colspan="<?php echo $this->options->size / 2; ?>"><?php echo stripslashes($this->options->legend_max); ?></td>
            </tr>
            <tr class="likert-choice">
                <?php for($val = 0; $val <= 100; $val += $step) : ?>
                    <td><?php echo $val; ?></td>
                <?php endfor; ?>
            </tr>
        </tbody>
        </table>
        <?php else : ?>
        <input name="q<?php echo $this->id; ?>" type="hidden" value="<?php echo $reponse->reponse; ?>" />
        <div class="legend">
            <div class="float left"><?php echo stripslashes($this->options->legend_min); ?></div>
            <div class="float right"><?php echo stripslashes($this->options->legend_max); ?></div>
        </div>
        <div class="likert-choice">
        <?php for($val = 0; $val <= 100; $val += $step) : ?>
            <?php $checked = ($val == $reponse->reponse) ? 'checked="checked"' : "";?>
            <input class="checkable" type="radio" id="<?php echo "q".$this->id."c".$val; ?>" name="q<?php echo $this->id; ?>" value="<?php echo $val; ?>" <?php echo $checked; ?> /> <label for="<?php echo "q".$this->id."c".$val; ?>"><?php echo $val; ?></label>
        <?php endfor; ?>
        </div>
        <script>
        jQuery(document).ready(function($)
        {
            $('.question.likert input[type="radio"]').checkboxradio({ icon: false });
        });
        </script>
        <?php
        endif;
    }

    private function the_checkbox(QReponse $reponse)
    {
        ?>
        <p class="legend"><?php _e('Choix multiple'); ?></p>
        <?php foreach($this->options->choice as $idx => $c) : ?>
        <?php $checked = (is_array($reponse->reponse) && in_array($idx, $reponse->reponse)) ? 'checked="checked"' : "";?>
        <input class="checkable" type="checkbox" id="<?php echo "q".$this->id."c".$idx; ?>" name="q<?php echo $this->id; ?>" value="<?php echo $idx; ?>" <?php echo $checked; ?> /> <label for="<?php echo "q".$this->id."c".$idx; ?>"><?php echo stripslashes($c); ?></label>
        <?php endforeach; ?>
        <script>
        jQuery(document).ready(function($)
        {
            $(".question.checkbox input").checkboxradio();
        });
        </script>
        <?php
    }

    private function the_radio(QReponse $reponse)
    {
        ?>
        <p class="legend"><?php _e('Choix unique'); ?></p>
        <?php foreach($this->options->choice as $idx => $c) : ?>
        <?php $checked = ($idx == $reponse->reponse) ? 'checked="checked"' : "";?>
        <input class="checkable" type="radio" id="<?php echo "q".$this->id."c".$idx; ?>" name="q<?php echo $this->id; ?>" value="<?php echo $idx; ?>" <?php echo $checked; ?> /> <label for="<?php echo "q".$this->id."c".$idx; ?>"><?php echo stripslashes($c); ?></label>
        <?php endforeach; ?>
        <script>
        jQuery(document).ready(function($)
        {
            $(".question.radio input").checkboxradio();
        });
        </script>
        <?php
    }

    private function the_number(QReponse $reponse)
    {
        ?>
        <input type="number" min="<?php echo str_replace(',', '.', $this->options->min); ?>" max="<?php echo str_replace(',', '.', $this->options->max); ?>" step="<?php echo str_replace(',', '.', $this->options->step); ?>" name="q<?php echo $this->id; ?>" value="<?php echo $reponse->reponse; ?>" /> 
        <?php
    }

    private function the_text(QReponse $reponse)
    {
        ?>
        <input type="text" name="q<?php echo $this->id; ?>" value="<?php echo stripslashes($reponse->reponse); ?>" /> 
        <?php
    }

    private function the_textarea(QReponse $reponse)
    {
        ?>
        <textarea name="q<?php echo $this->id; ?>" rows="10"><?php echo stripslashes($reponse->reponse); ?></textarea> 
        <?php
    }

}

add_action('wp_ajax_add_choice', 'add_choice');
function add_choice()
{
    $reponse = array();
    
    $question = new Question();
    $reponse['html'] = $question->get_choice_edit();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_delete_question', 'delete_question');
function delete_question()
{
    $reponse = array('log' => $_POST);
    
    $question = new Question($_POST['question_id']);
    $reponse['result'] = $question->delete($_POST['force'] ?? false);
    
    echo json_encode($reponse);
    die();
}