<?php
/*
 * class-plugins-manager.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

define('opaga_plugins_path', wpof_path . 'plugins');
define('opaga_template_plugins_path', opaga_custom_path . '/plugins');

class PluginsManager
{
    private $opaga_plugins;
    private $opaga_plugins_options;

    function __construct()
    {
        $this->opaga_plugins         = false;
        $this->opaga_plugins_options = [];
    }

    /**
     * Include les plugins opaga et retourne la liste des plugins valide (dont la classe implemente "OpagaPlugin")
     *
     * @return string[] Tableau des noms des classes
     */
    public function getOpagaPlugins()
    {
        if ($this->opaga_plugins !== false) {
            return $this->opaga_plugins;
        }

        $this->opaga_plugins = [];

        foreach(array(opaga_plugins_path, opaga_template_plugins_path) as $plugin_dir) {
            if (is_dir($plugin_dir)) {
                $dh = opendir($plugin_dir);
                if ($dh) {
                    while (($plugin = readdir($dh)) !== false) {
                        $plugin_path = $plugin_dir . '/' . $plugin;

                        if (is_dir($plugin_dir.'/'.$plugin) && '.' !== substr($plugin, 0, 1)) {
                            $sdh = opendir($plugin_dir.'/'.$plugin);
                            if ($sdh) {
                                while(($sd_plugin = readdir($sdh)) !== false) {
                                    if ('.php' === substr($sd_plugin, -4)) {
                                        $plugin_path .= '/'.$sd_plugin;
                                        $plugin = $sd_plugin;
                                    }
                                }
                                closedir($sdh);
                            }
                        }

                        if ('.php' === substr($plugin, -4)) {
                            $classname   = self::getPluginClassname($plugin);
                            include_once($plugin_path);
                            // Si la classe implemente bien OpagaPlugin, on peut l'ajouter à la liste des plugins.
                            if (isset(class_implements($classname)['OpagaPlugin'])) {
                                $this->opaga_plugins[$classname] = $classname::getPluginName();
                                $this->opaga_plugins_options     = $classname::getOptions();
                            }
                        }
                    }
                    closedir($dh);
                }
            }
        }
        asort($this->opaga_plugins);

        return $this->opaga_plugins;
    }

    public function getPluginsOptions()
    {
        return $this->opaga_plugins_options;
    }

    public static function getPluginClassname($plugin)
    {
        // transforme test-plugin_opaga.php en TestPluginOpaga
        $classname = str_replace('.php', '', str_replace('_', '', ucwords(str_replace('-', '_', $plugin), '_')));
        return $classname;
    }

    private static $plugin = null;

    public static function getPlugin(): OpagaPlugin
    {
        if (is_null(self::$plugin)) {
            $pluginname = get_option('wpof_plugin');
            if (!isset(class_implements($pluginname)['OpagaPlugin'])) {
                throw new Exception("La classe $pluginname n'implémente pas OpagaPlugin !");
            }
            self::$plugin = new $pluginname();
        }

        return self::$plugin;
    }
}