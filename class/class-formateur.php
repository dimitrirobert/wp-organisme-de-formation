<?php
/*
 * class-formateur.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-info.php");

define( 'wpof_url_user_doc', WP_CONTENT_URL . "/uploads/user_doc");
define( 'wpof_path_user_doc', WP_CONTENT_DIR . "/uploads/user_doc");

class Formateur extends WP_User
{
    public $nom = "";
    public $prenom = "";
    public $email = "";
    public $url;
    public $id = "";
    public $permalink;
    
    public $role = "";
        
    public $presentation = "";
    public $photo = "";
    public $marque = "";
    public $logo_marque = "";
    public $activite = "";
    public $statut = "";
    public $code = "";
    public $realisations = "";
    public $cv_url = "";
    public $cv = 0;
    public $profil_public = 0;
    public $archive = 0;
    public $sous_traitant = 0;
    public $genre = null;
    
    public $doc_path = "";
    public $doc_url = "";
    
    public $tags = array();
    
    private $signataire = false;
    public $image_signature = "";
    
    public $tmp_file = "";
    
    public $veille_bloc_notes = "";
    public $date_modif_veille = 0;
    
    public $last_online = 0;
    
    public function __construct($user_id = -1)
    {
        if ($user_id > 0)
        {
            parent::__construct($user_id);
            if (isset($this->data))
            {
                global $wpof;
                $this->id = $user_id;
                
                $this->role = wpof_get_role($user_id);
                
                $meta = get_user_meta($user_id);
                
                // Recherche du permalien de l'utilisateur
                $this->permalink = home_url()."/".$wpof->url_user."/".$this->data->user_login;
                
                ?>
                <?php
                // patch TODO
                if (isset($meta['formateur_marque']))
                {
                    $this->update_meta("marque", $meta['formateur_marque']);
                    delete_user_meta($this->id, "formateur_marque");
                }
                
                foreach(array_keys($wpof->desc_formateur->term) as $k)
                    if (isset($meta[$k][0]))
                    {
                        if (is_array($this->$k))
                        {
                            $array_value = unserialize($meta[$k][0]);
                            if (is_array($array_value))
                                $this->$k = array_stripslashes($array_value);
                        }
                        else
                            $this->$k = stripslashes($meta[$k][0]);
                    }
                
                $this->nom = (isset($meta['last_name'][0])) ? $meta['last_name'][0] : "";
                $this->prenom = (isset($meta['first_name'][0])) ? $meta['first_name'][0] : "";
                $this->email = $this->data->user_email;
                $this->url = $this->data->user_url;
                $this->username = $this->data->user_nicename;
                
                $this->last_online = (isset($meta['last_online'][0])) ? $meta['last_online'][0] : 0;
                
                $this->signataire = is_signataire($this->id);
                if (champ_additionnel("image_signature") && isset($meta['image_signature'][0]))
                    $this->image_signature = $meta['image_signature'][0];
                $completion_rate = explode('/', $this->completion);
                $this->profil_public = (count($completion_rate) == 2 && $completion_rate[0] == $completion_rate[1]) ? 1 - $this->archive : 0;
                
                $this->tmp_file = $meta['tmp_file'][0] ?? "";
                    
                $this->doc_path = wpof_path_user_doc.'/'.$this->username.'/';
                $this->doc_url = wpof_url_user_doc.'/'.$this->username.'/';
                
                // Choix entre un CV déposé sur OPAGA ou un CV distant
                $this->set_cv_url();
            }
            else
                return null;
        }
    }
    
    /*
    * Retourne le nom (display_name) d'un utilisateur par son ID
    * Si $link vaut true, alors le nom est entouré d'une balise a qui pointe vers sa page de profil
    */
    public function get_displayname($link = false)
    {
        $display_name = "{$this->prenom} {$this->nom}";
        
        if ($this->profil_public != 1 && !is_user_logged_in())
            $link = false;
        
        if ($link)
        {
            return $this->get_permalink($display_name);
        }
        else
            return $display_name;
    }

    /**
     * Retourne le html du permalink du formateur avec $text dedans
     * Possibilité d'ajouter des attributs html avec le filter opaga_formateur_link_attrs.
     * 
     * @return string le <a> du permalink
     */
    public function get_permalink($text, $sub_page = "", $class = null)
    {
        $link_attrs = apply_filters('opaga_formateur_link_attrs', '');
        $class_attr = "";
        if ($class !== null)
            $class_attr = 'class="'.$class.'"';
        // Recherche du permalien de l'utilisateur
        return '<a href="' . $this->permalink . '/'.esc_html($sub_page).'" ' . esc_attr($link_attrs) . ' '.$class_attr.'>' . $text . '</a>';
    }
    
    // pour compatibilité avec les classes SessionFormation et Formation
    public function get_a_link($text)
    {
        return $this->get_permalink($text);
    }

    /**
     * Retourne l'<img> de la photo de profil du formateur s'il y en a une. 
     * S'il n'y en a pas retourne une image par défaut.
     * 
     * @param string $size : la taille de l'image. Si rien prend la size 'formateur_photo' définie dans wp-organisme-de-formation.php ou ce qui est dans le filtre opaga_formateur_picture_size
     * 
     * @return string le html permettant l'affichage de la photo de profil, ou une photo par défaut.
     * 
     * @see wp_get_attachment_image()
     */
    public function get_picture($size = 'formateur_photo')
    {
        // La taille à appliquer à l'image
        $pic_size = apply_filters('opaga_formateur_picture_size', $size);

        // Tableau des tailles d'images définies et leurs width/height.
        $wp_sizes = wp_get_registered_image_subsizes();

        // Si la pic_size n'existe pas on prend size.
        if (! in_array($pic_size, array_keys($wp_sizes))) {
            // Si la size n'existe pas on prend thumbnail.
            $pic_size = in_array($size, array_keys($wp_sizes)) ? $size : 'medium';
        }

        // L'url de l'image par défaut du plugin
        $fallback_path = plugin_dir_url( __DIR__ ) . 'images/circle-user-solid.svg';

        $hwstring   = (isset($wp_sizes[$pic_size])) ? image_hwstring( $wp_sizes[$pic_size]['width'], $wp_sizes[$pic_size]['height'] ) : "";

        // Les noms à utiliser pour les classes (reprend le comportement par défaut de wordpress)
        $size_class = $pic_size;
        if ( is_array( $size_class ) ) {
            $size_class = implode( 'x', $size_class );
        }

        // Les attributs de l'image
        $fallback_attrs = array(
            'src' => $fallback_path,
            'class' => "attachment-$size_class size-$size_class",
            'alt' => __('Portrait par défaut'),
            'title' => $this->get_displayname(),
        );

        // Lazy loading si c'est activé
        if ( wp_lazy_loading_enabled( 'img', 'wp_get_attachment_image' ) ) {
            $fallback_attrs['loading'] = wp_get_loading_attr_default( 'wp_get_attachment_image' );
        }

        $fallback_attrs = array_map( 'esc_attr', $fallback_attrs );

        // Et écriture du html de l'image avec sa taille et ses attributs.
        $fallback_html = rtrim( "<img $hwstring" );
 
        foreach ( $fallback_attrs as $name => $value ) {
            $fallback_html .= " $name=" . '"' . $value . '"';
        }
 
        $fallback_html .= ' />';

        // Le html de l'image par défaut s'il n'y en a pas de définie pour le formateur
        $pic_fallback = apply_filters('opaga_formateur_picture_default', $fallback_html);

        //$pic_html = empty($this->photo) ? $pic_fallback : wp_get_attachment_image($this->photo, $pic_size, false, ['alt' => sprintf(__("Portrait de %s"), $this->get_displayname()), 'title' => $this->get_displayname()]);
        $pic_html = empty($this->photo) ? $pic_fallback :
        '<img class="attachment-'.$pic_size.' size-'.$pic_size.'" src="'.$this->doc_url.$this->photo.'" width="'.$wp_sizes[$pic_size]['width'].'" height="auto" alt="'.sprintf(__("Portrait de %s"), $this->get_displayname()).'" title="'.$this->get_displayname().'" />';
        
        return $pic_html;
    }
    
    /*
     * Fixe l'URL du CV, qu'il soit local ou distant
     */ 
    public function set_cv_url()
    {
        if (empty($this->cv_url) && !empty($this->cv))
            $this->cv_url = $this->doc_url.$this->cv;
    }
    
    /*
     * Calcule le taux de complétion d'un profil de formateur
     * Renvoie ce taux de complétion en float
     */
    public function set_profil_completion()
    {
        global $wpof;
        
        $completion_profil = $wpof->desc_formateur->get_fields("needed", true);
        
        // Le total comprend les champs requis plus au moins une formation complète au catalogue
        $total = count($completion_profil) + 1;
        
        // Calcul du nombre de formations complètes (accès public) non cachées
        $formations_id = $wpof->get_actions_id("formation", $this->id);
        $formations_id = $wpof->filter_id_by_value($formations_id, "formation", "acces_public", 1, "=");
        $formations_id = $wpof->filter_id_by_value($formations_id, "formation", "brouillon", 1, "=", true);
        $rempli = (count($formations_id) > 0) ? 1 : 0;
        
        foreach(array_keys($completion_profil) as $k)
            if (!empty($this->$k) && trim($this->$k) != "")
                $rempli++;
        
        $this->completion = $rempli."/".$total;
        update_user_meta($this->id, "completion", $this->completion);
        return $rempli / $total;
    }
    
    /*
     * Retourne la liste des champs vides à compléter
     */
    public function get_completion_missing()
    {
        global $wpof;
        
        $completion_profil = $wpof->desc_formateur->get_fields("needed", true);
        $vide = array();
        foreach(array_keys($completion_profil) as $k)
            if (empty(trim($this->$k)))
            {
                if ($wpof->desc_formateur->is_term($k))
                    $vide[] = $wpof->desc_formateur->get_term($k);
                else
                    $vide[] = $k;
            }
        
        $formations_id = $wpof->get_actions_id("formation", $this->id);
        $formations_id = $wpof->filter_id_by_value($formations_id, "formation", "acces_public", 1, "=");
        $formations_id = $wpof->filter_id_by_value($formations_id, "formation", "brouillon", 1, "=", true);
        if (count($formations_id) == 0)
            $vide[] = __("Au moins une formation complète et publique dans votre catalogue");
        
        return $vide;
    }
    
    /*
     * Retourne le taux de complétion de la formation sous la forme :
     * nb_champs_remplis / nb_champs_a_repmlir
     */
    public function get_profil_completion($html = true)
    {
        if ($html)
        {
            $completion_rate = explode('/', $this->completion);
            $class = (count($completion_rate) == 2 && $completion_rate[0] == $completion_rate[1]) ? "fait" : "attention pointer";
            return '<span class="'.$class.' completion">'.$this->completion.'</span>';
        }
        else
            return $this->completion;
    }
    
    
    /**
     * Affiche le tableau des sessions du formateur
     *
     * @param string $quand 'futur' ou 'passe'
     * @return void
     */
    public function the_sessions($quand)
    {
        //$sessions = get_formation_sessions(array('formateur' => $this->id, 'quand' => $quand));
        $sessions = $this->get_my_sessions($quand);
        if ($sessions)
        {
            if ($quand == 'futur') : ?>
            <p><em><?php _e("Contient également les sessions sans date"); ?></em></p>
            <?php endif; ?>
            <table class="opaga opaga2 datatable">
            <thead>
            <?php echo SessionFormation::get_session_control_head($quand); ?>
            </thead>
            <?php
                foreach($sessions as $s)
                    echo $s->get_session_control($quand);
            ?>
            </table>
            <?php
        }
        else
            echo "<p>".__("Aucune session programmée")."</p>";
    }
    
    /**
     * Renvoie un tableau des clients du formateur selon $entite_client
     * @param $entite_client peut valoir Client::ENTITE_PERS_MORALE, Client::ENTITE_PERS_PHYSIQUE, index ou null
     * Si $entite_client == index le tableau renvoyé est indexé par type d'entité, puis par id
     */ 
    public function get_my_clients(string $entite_client = null) : array
    {
        global $wpof;
        $sessions = $wpof->get_sessions($this->id);
        $clients = array();
        
        foreach($sessions as $s)
            foreach($s->clients as $cid)
            {
                $client = get_client_by_id($s->id, $cid);
                $clients[$client->entite_client][$cid] = $client;
            }
        
        if ($entite_client === null)
            return array_flatten($clients);
        elseif ($entite_client == "index")
            return $clients;
        else
            return $clients[$entite_client];
    }
    
    public function get_my_sessions($quand = null)
    {
        global $wpof;
        $sessions = array();
        $session_id = $wpof->get_actions_id("session", $this->id, null, $quand);
        
        if (is_array($session_id))
            foreach($session_id as $id)
            {
                $s = get_session_by_id($id);
                $sessions[$s->first_date_timestamp.'-'.$s->id] = $s;
            }
        
        return $sessions;
    }
    
    public function get_my_formations()
    {
        global $wpof;
        $formations = array();
        $formation_id = $wpof->get_actions_id("formation", $this->id);
        
        if (is_array($formation_id))
            foreach($formation_id as $id)
                $formations[$id] = get_formation_by_id($id);
        
        return $formations;
    }
    
    public function update_my_tags()
    {
        $this->tags = array();
        foreach($this->get_my_formations() as $f)
        {
            $this->tags = array_merge($this->tags, $f->post_tag);
        }
        $this->tags = array_unique($this->tags);
        $this->update_meta("tags");
    }
    
    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpof;
        if ($meta_value == "delete_meta")
        {
            $this->delete_meta($meta_key);
            return;
        }
        
        $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key, $meta_value);
        
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        $meta_value = $this->$meta_key;
        
        $result = false;
        switch($meta_key)
        {
            case 'prenom':
                $result = update_user_meta($this->id, "first_name", $meta_value);
                break;
            case 'nom':
                $result = update_user_meta($this->id, "last_name", $meta_value);
                break;
            case 'url':
            case 'email':
                $result = wp_update_user(array('ID' => $this->id, 'user_'.$meta_key => $this->$meta_key));
                break;
            case 'archive':
                $result = update_user_meta($this->id, $meta_key, $meta_value);
                $this->disable_my_formations_and_sessions();
                break;
            default:
                $result = update_user_meta($this->id, $meta_key, $meta_value);
                break;
        }
        
        $profil_public = ($this->archive == 0) ? (integer) $this->set_profil_completion() : 0;
        update_user_meta($this->id, "profil_public", $profil_public);
        
        return $result;
    }
    
    /**
     * Désactive (met en brouillon) mes formations si archive == 1
     * La formation est mise en brouillon uniquement si toustes les formateur⋅ices l'animant son mises en archive
     */
    public function disable_my_formations_and_sessions()
    {
        if ($this->archive == 1)
        {
            foreach($this->get_my_formations() as $formation)
                $res = $formation->set_brouillon_if_no_formateur();
            
            foreach($this->get_my_sessions() as $session)
                $res = $session->hide_if_no_formateur();
        }
    }
    
    public function delete_meta($meta_key)
    {
        global $wpof;
        if (in_array($meta_key, array_keys($wpof->desc_formateur->term)))
        {
            $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key);
            return delete_user_meta($this->id, $meta_key);
        }
        else
            return false;
    }
    
    /**
     * Affiche le tableau des formations du formateur
     * 
     * @return void
     */
    public function get_formations_tableau()
    {
        $formations = $this->get_my_formations();
        if ($formations) : ?>
            <table class="opaga opaga2 datatable">
            <thead>
            <?php echo Formation::get_formation_control_head(); ?>
            </thead>
            <?php
                foreach($formations as $f)
                    echo $f->get_formation_control();
            ?>
            </table>
            <table class="opaga">
            <tr id="formation<?php echo $this->id; ?>" data-context="formation">
            <td colspan="100%" class="right">
            <?php _e("Nouvelle session sans lien avec le catalogue"); ?>
            <div class="icone-bouton dynamic-dialog" data-function="new_session">
            <?php the_wpof_fa('file-circle-plus'); ?>
            </div>
            </td>
            </tr>
            </table>
        <?php endif;
    }
    
    /**
     * Formate les informations de présentation d'un⋅e formateur⋅ice pour un document PDF
     */
    public function get_presentation_pdf()
    {
        $html = "";
        apply_filters("opaga_presentation_pdf_formateur", $html, $this);
        if (empty($html))
        {
            $img_tag = "";
            if (!empty($this->photo))
            {
                $src = $this->doc_url.$this->photo;
                $img_tag = '<img class="photo-formateur" src="'.$src.'" />';
            }
            
            $cv_tag = "";
            if (!empty($this->cv_url))
                $cv_tag = '<p style="text-align: right; font-size: 6pt;">Son CV<br /><a href="'.$this->cv_url.'">'.$this->cv_url.'</a></p>';
            
            $url_tag = "";
            if (!empty($this->url))
                $url_tag = '<p style="text-align: right; font-size: 6pt;">Site Web<br /><a href="'.$this->url.'">'.$this->url.'</a></p>';
            
            if (!empty($this->realisations))
                $realisations = '<p><strong>'.__("Ses réalisations").'</strong></p>'.wpautop($this->realisations);
            else
                $realisations = "";
            
            $html = '<div class="fiche-formateur">';
            $html .= '<div class="formateur-side">'.$img_tag.$cv_tag.$url_tag.'</div>';
            $html .= '<div class="formateur-main"><h4>'.$this->get_displayname().'</h4>';
            $html .= '<p>'.$this->activite.'</p>';
            $html .= wpautop($this->presentation).$realisations;
            $html .= '</div></div>';
        }
        
        return $html;
    }
    
    /**
     * Affiche les informations publiques sur le formateur.
     * 
     * @return void
     */
    public function vue_publique()
    {
        global $wpof;
        
        // Met une classe spécifique dans le body pour identifier qu'on affiche un formateur
        add_filter( 'body_class', array($this, 'body_class') );

        $role = wpof_get_role(get_current_user_id());
        $is_responsable = in_array($role, array("um_responsable", "admin"));
        
        $formations = $wpof->get_formations($this->id);
        $sessions_futures = $wpof->get_sessions($this->id, "futur");
        $sessions_passees = array_merge($wpof->get_sessions($this->id, "present"), $wpof->get_sessions($this->id, "passe"));
        ?>
        <div class="presentation-formateur">
            <div class="meta-formateur">
                <div class="meta-infos">
                <?php if ($this->photo): ?>
                    <?php echo $this->get_picture(); ?>
                <?php endif;?>
                <div class="icone-bouton dynamic-dialog premier-contact" data-function="premier_contact" data-formateur_id="<?php echo $this->id; ?>" data-submit-txt="<?php _e("Envoyer mon message"); ?>"><?php the_wpof_fa_sendmail(); ?> <?php printf(__("Contacter %s"), $this->get_displayname()); ?></div>
                <?php if (!empty($this->cv_url)): ?>
                    <div><a target="_blank"  class="bouton cv-bouton" href="<?php echo $this->cv_url; ?>"><?php the_wpof_fa_pdf(); ?><?php _e("Télécharger son CV"); ?></a></div>
                <?php endif;?>
                <?php if (!empty($this->url)): ?>
                    <div><a target="_blank"  class="bouton cv-bouton" target="_blank" href="<?php echo $this->url; ?>"><?php the_wpof_fa("earth"); ?><?php _e("Visiter son site Web"); ?></a></div>
                <?php endif;?>
                </div>
                <div class="meta-links">
                <!-- Inactif pour le moment
                <h4>Ses liens</h4>
                <ul>
                    <li>Lien un</li>
                    <li>Lien deux</li>
                </ul>
                -->
                <?php if (!empty($this->tags)) : ?>
                <h4><?php _e("Thèmes"); ?></h4>
                <ul>
                <?php foreach($this->tags as $slug) : ?>
                <li><?php $term = get_term_by("slug", $slug, "post_tag"); ?><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name ?></a></li>
                <?php endforeach; ?>
                </ul>
                <?php endif; ?>
                
                <?php if ($this->realisations): ?>
                    <h4><?php _e("Ses réalisations"); ?></h4>
                    <?php echo $this->realisations; ?>
                <?php endif;?>
                </div>
            </div> <!-- .meta-formateur -->

            <div class="content-formateur">
                <?php if (champ_additionnel('formateur_marque') && $this->logo_marque) : ?>
                    <img src="<?php echo $this->doc_url.$this->logo_marque; ?>" class="logo_marque" />
                <?php endif; ?>
                <h1 class="entry-title formateur-nom"><?php echo $this->get_displayname(); ?></h1>
                <?php if (champ_additionnel('formateur_marque') && $this->marque) : ?>
                <h2><?php echo $this->marque; ?></h2>
                <?php endif; ?>
                <?php if ($this->activite) : ?>
                <p><?php echo $this->activite; ?></p>
                <?php endif; ?>
                <?php echo $this->presentation; ?>

                <?php if (!empty($formations)) : ?>
                    <h2 id="catalogue"><?php _e("Son catalogue de formations"); ?></h2>
                    <?php
                        $wrapper = array();
                        foreach($formations as $f)
                            $wrapper[$f->id] = $f->get_item_html('h3');
                        echo apply_filters('opaga_formations_html', '<div class="posts-wrapper">'.implode($wrapper).'</div>', $wrapper);
                    ?>
                    <?php if ($is_responsable) echo $this->get_formations_tableau(); ?>
                <?php endif;?>
            
                <?php if (!empty($sessions_futures)) : ?>
                    <h2 id="prochaines-sessions"><?php _e("Ses prochaines sessions"); ?></h2>
                    <?php
                        $wrapper = array();
                        foreach($sessions_futures as $s)
                            $wrapper[$s->first_date_timestamp.'_'.$s->id] = $s->get_item_html('h3');
                        ksort($wrapper);
                        echo apply_filters('opaga_sessions_html', '<div class="posts-wrapper">'.implode($wrapper).'</div>', $wrapper);
                    ?>
                    <?php if ($is_responsable) echo $this->the_sessions("futur"); ?>
                <?php endif;?>
            
                <?php if ($sessions_passees): ?>
                    <h2><?php _e("Ses sessions passées"); ?></h2>
                    <?php
                        $wrapper = array();
                        foreach($sessions_passees as $s)
                            $wrapper[$s->first_date_timestamp.'_'.$s->id] = $s->get_item_html('h3');
                        krsort($wrapper);
                        echo apply_filters('opaga_sessions_html', '<div class="posts-wrapper">'.implode($wrapper).'</div>', $wrapper);
                    ?>
                    <?php
                    if ($is_responsable)
                    {
                        echo $this->the_sessions("present");
                        echo $this->the_sessions("passe");
                    }
                    ?>
                <?php endif;?>
            </div><!-- .content-formateur -->
        </div><!-- .presentation-formateur -->
    <?php
    }
    
    public function get_notifications()
    {
        $has_notifications = false;
        $notifications_text = "";
        $notifications_icon = '<span class="icone big important notif-icon" data-id="notifications_text">'.wpof_fa("bell").'</span>';
        
        ob_start();
        ?>
        <div id="notifications">
        <?php
        $pending_signatures = get_user_meta($this->id, "pending_signatures", true);
        if (!empty($pending_signatures))
        {
            $notifications_text .= '<p>'.sprintf(__("Vous avez coché %d document(s) pour validation."), count($pending_signatures)).'</p>';
            $notifications_text .= '<span class="icone-bouton notif-action" data-action="send_demande_valid">'.__("Envoyez votre demande").'</span>';
            $has_notifications = true;
        } ?>
        
        <?php if ($has_notifications) :
            echo $notifications_icon; ?>
        <div id="notifications_text" class="blocHidden">
        <?php echo wpof_fa('circle-xmark', 'close').$notifications_text; ?>
        </div>
        <?php endif; ?>
        </div>
        <?php
        
        return ob_get_clean();
    }
    
    public function remove_pending_signature($doc)
    {
        if (is_array($this->pending_signatures))
        {
            $ps = $this->pending_signatures;
            unset($ps[$doc->session_formation_id."_".$doc->contexte."_".$doc->contexte_id.$doc->type]);
            $this->update_meta("pending_signatures", $ps);
        }
    }
    
    private function get_tab_agenda()
    {
        global $wpof;
        ob_start();
        ?>
        <div class="boutons">
            <div class="icone-bouton dynamic-dialog" data-function="new_session" data-type="opac"><?php the_wpof_fa_add(); ?> <?php _e("Nouvelle session en sous-traitance"); ?></div>
            <?php echo get_icone_aide("session_definition", __("Qu'est-ce qu'une session ?")); ?>
        </div>
        <?php $session_en_cours = $wpof->get_actions_id("session", $this->id, null, "present");
        if (!empty($session_en_cours)) : ?>
        <div class="white-board">
            <h2><?php _e("Sessions en cours"); ?></h2>
            <?php $this->the_sessions("present"); ?>
        </div> <!-- .white-board -->
        <?php endif; ?>

        <div class="white-board">
            <h2><?php _e("Sessions futures"); ?></h2>
            <?php $this->the_sessions("futur"); ?>
        </div> <!-- .white-board -->

        <?php $session_passe = $wpof->get_actions_id("session", $this->id, null, "passe");
        if (!empty($session_passe)) : ?>
        <div class="white-board old-sessions">
            <h2><?php _e("Sessions passées"); ?> <span class="openButton" data-id="sessions-passees">Voir</span></h2>
            <div id="sessions-passees" class="blocHidden">
            <?php $this->the_sessions("passe"); ?>
            </div>
        </div> <!-- .white-board -->
        <?php endif; ?>
        <?php
        return ob_get_clean();
    }
    
    private function get_tab_catalogue()
    {
        ob_start();
        ?>
        <div class="boutons">
            <div class="icone-bouton dynamic-dialog" data-function="new_formation"><?php the_wpof_fa_add(); ?> <?php _e("Nouvelle formation"); ?></div>
            <?php echo get_icone_aide("session_definition", __("Qu'est-ce qu'une session ?")); ?>
            <?php echo get_icone_aide("formation_definition", __("Qu'est-ce qu'une formation ?")); ?>
        </div>
        <div class="white-board">
            <h2><?php _e("Mon catalogue de formations"); ?></h2>
            <?php
            echo $this->get_formations_tableau();
            ?>
        </div> <!-- .white-board -->
        <?php
        return ob_get_clean();
    }
    
    private function get_tab_clients()
    {
        global $wpof;
        $clients = $this->get_my_clients();
        ob_start();
        ?>
        <div class="white-board">
            <h2><?php _e("Mes clients"); ?></h2>
            <table class="opaga opaga2 datatable">
            <thead>
            <tr>
            <th><?php _e("Nom"); ?></th>
            <th><?php _e("Type"); ?></th>
            <th><?php _e("Coordonnées"); ?></th>
            <th><?php _e("Siret"); ?></th>
            <th><?php _e("RCS"); ?></th>
            <th><?php _e("Session"); ?></th>
            <th><?php _e("Supprimer"); ?></th>
            </tr>
            </thead>
            <?php foreach($clients as $c) : ?>
            <tr id="<?php echo $c->id; ?>">
            <td><?php echo $c->get_displayname(true); ?></td>
            <td class="thin"><?php echo ($c->financement == Client::FINANCEMENT_OPAC) ? __("Sous-traitance") : $wpof->entite_client->get_term($c->entite_client); ?></td>
            <td><?php echo $c->adresse.'<br />'.$c->code_postal.' '.$c->ville.' '.$c->pays; ?></td>
            <td class="thin"><?php echo $c->siret; ?></td>
            <td class="thin"><?php echo $c->rcs; ?></td>
            <td>
            <?php 
            $session = get_session_by_id($c->session_formation_id);
            echo $session->get_a_link().'<br />'.$session->dates_texte;
            ?>
            </td>
            <td class="thin center"><?php echo $c->get_delete_bouton("", false, "tr#".$c->id); ?></td>
            </tr>
            <?php endforeach; ?>
            </table>
        </div> <!-- .white-board -->
        <?php
        return ob_get_clean();
    }
    
    private function get_tab_contrats()
    {
        $clients = $this->get_my_clients();
        ob_start();
        ?>
        <div class="white-board">
            <h2><?php _e("Mes contrats"); ?></h2>
            <table class="opaga opaga2 datatable">
            <thead>
            <?php echo Client::get_client_control_head(); ?>
            </thead>
            <?php
                foreach($clients as $c)
                {
                    echo $c->get_client_control();
                }
            ?>
            </table>
        </div> <!-- .white-board -->
        <?php
        return ob_get_clean();
    }
    
    private function get_tab_profil()
    {
        global $wpof, $tinymce_wpof_settings;
        ob_start();
        ?>
        <div class="boutons">
            <?php echo get_icone_aide("utilisateur_profil", __("Renseignez votre profil")); ?>
            <?php
                $completion_rate = explode('/', $this->completion);
                if (count($completion_rate) == 2 && $completion_rate[0] < $completion_rate[1]) : ?>
                <div data-context="formateur" id="formateur<?php echo $this->id; ?>" class="icone-bouton alerte">
                <span class="attention completion"><?php _e("Profil incomplet !"); ?></span>
                </div>
                <?php endif; ?>
        </div>
        <div id="formateur-profil" class="white-board edit-data">
            <?php if (champ_additionnel('image_signature') && $this->role == "um_responsable") : ?>
            <h2><?php _e("Signature"); ?></h2>
            <?php echo get_input_jpost($this, "image_signature", array('input' => 'media', 'class' => 'photo-profil', 'button_text' => __("Téléversez une image"), 'aide' => 'responsable_signature', 'needed' => true)); ?>
            <?php endif; ?>
        
            <h2><?php _e("Photo"); ?></h2>
            <?php echo get_input_jpost($this, "photo", array('input' => 'media', 'class' => 'photo-profil', 'button_text' => __("Téléversez une image"), 'aide' => 'utilisateur_photo', 'needed' => $wpof->desc_formateur->term['photo']->needed)); ?>
            
            <h2><?php _e("CV"); ?></h2>
            <?php echo get_input_jpost($this, "cv", array('input' => 'media', 'button_text' => __("Téléversez un document PDF"), 'aide' => 'utilisateur_cv', 'needed' => $wpof->desc_formateur->term['cv']->needed)); ?>
            <?php echo get_input_jpost($this, "cv_url", array('input' => 'text', 'label' => __("Lien externe vers votre CV"), 'aide' => false, 'needed' => $wpof->desc_formateur->term['cv_url']->needed)); ?>
            
            <?php if (champ_additionnel('formateur_statut')) : ?>
            <h2><?php _e("Statut"); ?></h2>
            <?php echo get_input_jpost($this, "statut", array('input' => 'text', 'label' => __("Votre statut dans l'organisme de formation"), 'aide' => "utilisateur_statut", 'needed' => $wpof->desc_formateur->term['statut']->needed)); ?>
            <?php endif; ?>
        
            <h2><?php _e("Activité"); ?></h2>
            <?php echo get_input_jpost($this, "activite", array('input' => 'text', 'label' => __("Votre activité en lien avec le contenu de vos formations"), 'aide' => "utilisateur_activite", 'needed' => $wpof->desc_formateur->term['activite']->needed)); ?>
        
            <?php if (champ_additionnel('formateur_marque')) : ?>
            <?php echo get_input_jpost($this, "marque", array('input' => 'text', 'label' => __("Votre marque ou nom commercial"), 'aide' => "utilisateur_marque", 'needed' => $wpof->desc_formateur->term['marque']->needed)); ?>
            <?php echo get_input_jpost($this, "logo_marque", array('input' => 'media', 'class' => 'photo-profil', 'label' => __("Logo de votre marque"), 'button_text' => __("Téléversez une image"), 'aide' => 'utilisateur_logo_marque', 'needed' => $wpof->desc_formateur->term['logo_marque']->needed)); ?>
            <?php endif; ?>
            
            <?php if (champ_additionnel('formateur_code')) : ?>
            <?php echo get_input_jpost($this, "code", array('input' => 'text', 'label' => __("Code, indentifiant"), 'aide' => "utilisateur_code", 'needed' => $wpof->desc_formateur->term['code']->needed, 'readonly' => !in_array($wpof->role, $wpof->super_roles))); ?>
            <?php endif; ?>
            
            <?php echo get_input_jpost($this, "url", array('input' => 'text', 'label' => __("Site Web"), 'aide' => "utilisateur_url", 'needed' => $wpof->desc_formateur->term['url']->needed)); ?>
        
            <?php echo get_input_jpost($this, "presentation", array('editor' => '', 'label' => __("Présentation"), 'header' => 'h2', 'aide' => 'utilisateur_presentation', 'needed' => $wpof->desc_formateur->term['presentation']->needed)); ?>
            <?php echo get_input_jpost($this, "realisations", array('editor' => '', 'label' => __("Réalisations"), 'header' => 'h2', 'needed' => $wpof->desc_formateur->term['realisations']->needed)); ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    private function get_tab_veille()
    {
        global $tinymce_wpof_settings;
        ob_start();
        ?>
        <div class="boutons">
            <?php echo get_icone_aide("utilisateur_veille", __("Veille sur la formation")); ?>
        </div>
        <div class="white-board edit-data">
            <p><?php _e('Notez ici toutes vos actions de veille : lecture de sites, livres, réseaux, lettres d’info, participations à des conférences, événements, etc. Pensez à dater !'); ?><br />
            <?php _e("Dernière mise à jour :"); ?> <span id="date_modif_veille<?php echo $this->id; ?>"><?php echo $this->date_modif_veille; ?>
            </p>
            <?php echo get_input_jpost($this, 'veille_bloc_notes', array('editor' => '', 'label' => __('Bloc-notes'), 'postprocess' => 'update_data', 'ppargs' => array('ppkey' => 'date_modif_veille', 'ppvalue' => date("d/m/Y", time()), 'ppdest' => "#date_modif_attentes{$this->id}"))); ?>
        </div>
        <?php
        return ob_get_clean();
    }

    private function get_tab_infos() : string
    {
        ob_start();
        ?>
        <div class="info-canal">
            <?php $infos = Info::get_infos(false); ?>
            <?php if (!empty($infos)) : ?>
            <h3><?php _e("Dernières informations"); ?></h3>
            <div class="flex w-full flex-col gap-2">
            <?php
            foreach($infos as $info)
                echo $info->get_display();
            ?>
            <?php endif; ?>
            </div>

            <?php $infos = Info::get_infos(true); ?>
            <?php if (!empty($infos)) : ?>
            <h3><?php _e("Historique des informations"); ?></h3>
            <div class="flex w-full flex-col gap-2">
            <?php
            foreach($infos as $info)
                echo $info->get_display(true);
            ?>
            <?php endif; ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
    
    private function get_tab_default()
    {
        global $wpof;
        $formations_id = $wpof->get_actions_id("formation", $this->id);
        $sessions_futur_id = $wpof->get_actions_id("session", $this->id, null, "futur");
        $sessions_present_id = $wpof->get_actions_id("session", $this->id, null, "present");
        $sessions_passe_id = $wpof->get_actions_id("session", $this->id, null, "passe");
        $all_sessions_id = array_merge($sessions_passe_id, $sessions_present_id, $sessions_futur_id);
        
        $delai_modif_veille = delta_time($this->date_modif_veille, "d/m/Y");
        $infos = Info::get_infos(false);

        ob_start();
        ?>
        <?php if ($infos): ?>
        <div class="info-canal">
            <div class="float right"><a href="<?php echo $this->permalink.'/infos'; ?>"><?php _e("Voir toutes les informations"); ?></a></div>
            <h3><?php _e("Dernières infos"); ?> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="h-6 w-6 mx-3 align-bottom">  <path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM216 336l24 0 0-64-24 0c-13.3 0-24-10.7-24-24s10.7-24 24-24l48 0c13.3 0 24 10.7 24 24l0 88 8 0c13.3 0 24 10.7 24 24s-10.7 24-24 24l-80 0c-13.3 0-24-10.7-24-24s10.7-24 24-24zm40-208a32 32 0 1 1 0 64 32 32 0 1 1 0-64z"/></svg></h3>
            <div class="flex w-full flex-col gap-2">
            <?php
            foreach($infos as $info)
                echo $info->get_display();
            ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="white-board action-grid">
            <?php
            $completion_rate = explode('/', $this->completion);
            if (count($completion_rate) == 2 && $completion_rate[0] < $completion_rate[1]) : ?>
            <div data-context="formateur" id="formateur<?php echo $this->id; ?>" class="action-board important">
            <h3><?php _e("Profil incomplet"); ?> <?php the_wpof_fa("triangle-exclamation", "fa-beat"); ?></h3>
            <span class="bouton attention completion"><?php _e("Quels sont les informations manquantes ?"); ?></span>
            </div>
            <?php endif;
            ?>
            
            <div class="action-board">
            <h3><?php _e("Nouvelle formation"); ?></h3>
            <?php if (empty($formations_id)) : ?><span class="important"><?php the_wpof_fa("triangle-exclamation", "fa-beat"); ?> <?php _e("Vous n'avez aucune formation"); ?></span><br /><?php endif; ?>
            <span class="bouton dynamic-dialog" data-function="new_formation"><?php _e("Ajoutez une formation à votre catalogue"); ?></span>
            </div>
            
            <div class="action-board">
            <h3><?php _e("Nouvelle session"); ?></h3>
            <?php if (empty($formations_id)) : ?>
            <span class="bouton dynamic-dialog" data-function="new_formation"><?php _e("Créez d'abord votre premier programme de formation"); ?></span>
            <?php else: ?>
            <a class="bouton" href="<?php echo $this->permalink.'/catalogue'; ?>"><?php the_wpof_fa('file-circle-plus', 'fa-beat'); ?> <?php _e("Programmez une session depuis votre catalogue"); ?></a>
            <?php endif; ?>
            </div>
            
            <?php if (!empty($all_sessions_id)) : ?>
            <div class="action-board">
            <h3><?php _e("Gérer les sessions"); ?></h3>
            <a class="bouton" href="<?php echo $this->permalink.'/agenda'; ?>"><?php _e("Toutes vos sessions passées, présentes et futures"); ?></a>
            <?php if (count($sessions_present_id) > 0) : ?><p class="legend"><?php printf(__("%d session%s en cours"), count($sessions_present_id), count($sessions_present_id) > 1 ? "s" : "") ?></p><?php endif; ?>
            <?php if (count($sessions_futur_id) > 0) : ?><p class="legend"><?php printf(__("%d session%s à venir"), count($sessions_futur_id), count($sessions_futur_id) > 1 ? "s" : "") ?></p><?php endif; ?>
            </div>
            <?php endif; ?>
            
            <div class="action-board">
            <h3><?php _e("Nouvelle session en sous-traitance"); ?></h3>
            <span class="bouton dynamic-dialog" data-function="new_session" data-type="opac"><?php _e("Déclarez une session entièrement en sous-traitance"); ?>
            </div>
            
            <?php if ($delai_modif_veille > (integer) $wpof->delai_alerte_veille * 24 * 60 * 60) : ?>
            <div class="action-board important">
            <h3><?php _e("Veille"); ?> <?php the_wpof_fa('triangle-exclamation', 'fa-beat'); ?></h3>
            <a class="bouton" href="<?php echo $this->permalink.'/veille'; ?>"><?php _e("Ça fait longtemps que vous n'avez pas noté votre veille ! N'avez-vous rien à raconter ?"); ?></a>
            </div>
            <?php endif; ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function board_profil()
    {
        global $SessionFormation;
        global $tinymce_wpof_settings;
        global $wpof;
        
        remove_filter('the_content', 'wpautop');
        
        $onglet = "";
        $onglet_param = null;
        
        if ($this->signataire == false || $this->id != get_current_user_id())
            unset($wpof->onglet_user['docs']);
        
        if (isset($wpof->uri_params[1]) && in_array($wpof->uri_params[1], array_keys($wpof->onglet_user)))
        {
            $onglet = $wpof->uri_params[1];
            if (isset($wpof->uri_params[2]))
                $onglet_param = array_slice($wpof->uri_params, 2);
        }
        ?>
        <?php if (in_array($wpof->role, $wpof->super_roles)) : ?>
        <h2><?php echo $this->get_displayname().((!empty($this->marque)) ? " – ".$this->marque : ""); ?></h2>
        <?php endif; ?>
        <div class="opaga_tabs">
            <ul>
            <?php $notifications = $this->get_notifications();
            if (!empty(trim(strip_tags($notifications)))) : ?>
                <li><?php echo $notifications; ?></li>
            <?php endif ;?>
                <li><a href="<?php echo $this->permalink; ?>"><?php the_wpof_fa('house'); ?></a></li>
            <?php foreach($wpof->onglet_user as $slug => $text) :
                if ($text) :?>
                <li <?php if ($onglet == $slug) : ?> class="ui-state-active" <?php endif; ?>><a href="<?php echo $this->permalink.'/'.$slug; ?>"><?php echo $text; ?></a></li>
                <?php endif;
            endforeach; ?>
            </ul>
            </div>
        
            <div class="board-formateur tableau have-white-boards">
            <?php
            if (!empty($onglet))
            {
                $onglet_func = "get_tab_".$onglet;
                if ($onglet_param)
                    echo $this->$onglet_func($onglet_param);
                else
                    echo $this->$onglet_func();
            }
            else
                echo $this->get_tab_default();
            ?>
            </div>
            
        </div> <!-- board-formateur -->
        <?php
    }
    
    public static function get_formateur_control_head()
    {
        global $wpof;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr>
        <?php if (champ_additionnel('formateur_code')) : ?>
        <th><?php _e("Code, identifiant"); echo get_icone_aide("utilisateur_code") ?></th>
        <?php endif; ?>
        <th id="order" data-order="asc"><?php _e("Nom"); ?></th>
        <th><?php _e("Prénom"); ?></th>
        <th><?php _e("Email"); ?></th>
        <th><?php _e("Dernière connexion"); ?></th>
        <th><?php _e("Rôle"); echo get_icone_aide("user_role"); ?></th>
        <?php if (champ_additionnel("formateur_statut")) : ?>
        <th><?php _e("Statut"); echo get_icone_aide("utilisateur_statut"); ?></th>
        <?php endif; ?>
        <th class="thin"><?php _e("Completion profil"); echo get_icone_aide("user_completion_profil"); ?></th>
        <th class="thin"><?php _e("Sous-traitant"); echo get_icone_aide("user_sous_traitant"); ?></th>
        <th class="thin not_orderable"><?php _e("Catalogue"); echo get_icone_aide("user_catalogue"); ?></th>
        <th class="thin not_orderable"><?php _e("Agenda"); echo get_icone_aide("user_agenda"); ?></th>
        <th class="thin not_orderable"><?php _e("Voir profil"); echo get_icone_aide("user_voir_profil"); ?></th>
        <th class="thin not_orderable"><?php _e("Modifier profil WP"); echo get_icone_aide("user_modifier_profil_wp"); ?></th>
        <?php if (champ_additionnel("comptes_locaux")) : ?>
        <th class="thin not_orderable"><?php _e("Réinit mot de passe"); echo get_icone_aide("user_reset_password_wp"); ?></th>
        <?php endif; ?>
        <th class="thin not_orderable"><?php _e("Archivé"); echo get_icone_aide("user_archiver"); ?></th>
        <th class="thin not_orderable"><?php _e("Supprimer"); echo get_icone_aide("user_supprimer"); ?></th>
        <?php if (can_usurper()) : ?>
            <th class="thin"><?php _e("Usurper"); echo get_icone_aide("user_usurper"); ?></th>
        <?php endif; ?>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    /*
     * Renvoie une ligne de tableau pour contrôler la formation
     * 
     */
    public function get_formateur_control()
    {
        global $wpof, $anime_formation, $anime_session, $role_nom;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr id="formateur<?php echo $this->id; ?>" data-context="formateur" class="editable">
        <?php if (champ_additionnel('formateur_code')) : ?>
        <td><?php echo get_input_jpost($this, "code", array('input' => 'text', 'aide' => false)); ?></td>
        <?php endif; ?>
        <td><?php echo $this->last_name; ?></td>
        <td><?php echo $this->first_name; ?></td>
        <td><?php echo $this->email; ?></td>
        <td><span class="hidden"><?php echo $this->last_online; ?></span><?php echo date("j/m/Y à H:i", $this->last_online); ?></td>
        <td><?php echo $role_nom[$this->role]; ?></td>
        <?php if (champ_additionnel("formateur_statut")) : ?>
        <td><?php echo get_input_jpost($this, "statut", array('input' => 'text', 'aide' => false)); ?></td>
        <?php endif; ?>
        <td class="center"><?php echo $this->get_profil_completion(); ?></td>
        <td class="center edit-data"><?php echo get_input_jpost($this, "sous_traitant", array('input' => 'checkbox', 'display' => 'inline', 'aide' => false)); ?></td>
        <td class="center">
        <a href="<?php echo $this->permalink.'/catalogue'; ?>"><?php _e("Catalogue"); ?></a>
        <?php
        /*
            if (isset($anime_formation[$this->id]))
                echo "<p>".__("Formations")." : ".join(', ', $anime_formation[$this->id])."</p>";
            if (isset($anime_session[$this->id]))
                echo "<p>".__("Sessions")." : ".join(', ', $anime_session[$this->id])."</p>";
        */
        ?></td>
        <td class="center"><a href="<?php echo $this->permalink.'/agenda'; ?>"><?php _e("Agenda") ?></a></td>
        <td class="center"><a href="<?php echo $this->permalink; ?>"><?php the_wpof_fa_show(); ?></a></td>
        <td class="center"><a href="<?php echo admin_url("user-edit.php?user_id=".$this->id); ?>"><?php the_wpof_fa_edit(); ?></a></td>
        <?php if (champ_additionnel("comptes_locaux")) : ?>
        <td class="center">
        <span data-userid="<?php echo $this->id; ?>" data-func="reinit_password" class="pointer user_func" title="<?php printf(__("Envoyez un mail à %s"), $this->email); ?>"> <?php the_wpof_fa_switch(); ?> </span>
        </td>
        <?php endif; ?>
        <td class="center edit-data"><?php echo get_input_jpost($this, "archive", array('input' => 'checkbox', 'display' => 'inline', 'aide' => false)); ?></td>
        <td class="center"><span class="pointer delete-entity" data-objectclass="Utilisateur" data-id="<?php echo $this->id; ?>" data-parent="#user<?php echo $this->id; ?>"><?php the_wpof_fa_del(); ?></a></td>
        <?php if (can_usurper()) : ?>
            <td class="center"><span class="hidden"><?php printf("%05d", $this->id); ?></span><?php echo get_switchuser_button($this->id)." [ID ".$this->id."]"; ?></td>
        <?php endif; ?>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    public function send_demande_valid()
    {
        global $wpof;
        $reponse = array();
        $pending_signatures = get_user_meta($this->id, "pending_signatures", true);
        if (!empty($pending_signatures))
        {
            $subject = "[".$wpof->of_nom." Formation] Document à signer pour ".$this->get_displayname();
            $content_message = "Bonjour,\n\nNouveau(x) document(s) à valider pour ".$this->get_displayname()."\n";
            
            $session_id = -1;
            $session = null;
            
            ksort($pending_signatures);
            
            foreach($pending_signatures as $ps_id => $ps)
            {
                if ($ps['session_id'] != $session_id)
                {
                    $session_id = $ps['session_id'];
                    $session = get_session_by_id($session_id);
                    $content_message .= "\nPour la session : ".$session->titre_formation."\ndébutant le ".$session->first_date."\n\n";
                }
                $doc = new Document($ps['document'], $session_id, $ps['contexte'], $ps['contexte_id']);
                if ($doc !== null)
                {
                    $content_message .= "— ".$doc->desc." pour ".$doc->get_contexte_nom(true, false)."\n";
                    
                    // suppression de la demande auprès de tous les formateur⋅ices de la session
                    foreach($session->formateur as $fid)
                    {
                        $temp_pending_signatures = get_user_meta($fid, "pending_signatures", true);
                        unset($temp_pending_signatures[$ps_id]);
                        update_user_meta($fid, "pending_signatures", $temp_pending_signatures);
                    }
                }
            }
            
            $content_message .= "\nConnectez-vous pour le lire → ".home_url()."/login";
            $message = new Message(array('to' => get_signataires_id(), 'content' => $content_message, 'subject' => $subject));
            $message->sendmail();
            $reponse['message'] = "Notification faite à ".implode(', ', $message->to_email);
        }
        return $reponse;
    }
    
    
    public function get_doc_prenom_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $identite = "{$this->prenom} {$this->nom}";
        $result = array('empty_ok' => false, 'valeur' => $identite, 'tag' => "formateur:$function_name", 'desc' => __("Identité complète de la personne : prénom nom. Adapté lorsque ce marqueur doit faire apparaître les infos de plusieurs personnes."));
        return $result[$arg];
    }
    public function get_doc_identite($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $identite = "{$this->prenom} {$this->nom}";
        if ($this->marque != "")
            $identite .= " ({$this->marque})";
        $result = array('empty_ok' => false, 'valeur' => $identite, 'tag' => "formateur:$function_name", 'desc' => __("Identité complète de la personne : prénom nom (marque si existe). Adapté lorsque ce marqueur doit faire apparaître les infos de plusieurs personnes."));
        return $result[$arg];
    }
    public function get_doc_prenom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Prénom"));
        return $result[$arg];
    }
    public function get_doc_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Nom"));
        return $result[$arg];
    }
    public function get_doc_presentation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Présentation"));
        return $result[$arg];
    }
    public function get_doc_photo($arg = 'valeur', $dimensions = "")
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $photo = "";
        if ($arg == 'valeur')
        {
            $style = get_style_dimensions($dimensions);
            $photo = '<img src="'.$this->doc_url.$this->photo.'" '.$style.' />';
        }
        $result = array('empty_ok' => false, 'valeur' => $photo, 'tag' => "formateur:$function_name", 'desc' => __("Photo (dimensions à ajouter sous la forme {formateur:photo:largxhaut} unité(s) en mm obligatoirement ; ex. {formateur:photo:x50} pour une hauteur de 50mm et largeur automatique)"));
        return $result[$arg];
    }
    public function get_doc_marque($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $marque = $this->$function_name;
        if (empty($marque))
            $marque = $this->get_doc_prenom_nom();
        $result = array('empty_ok' => false, 'valeur' => $marque, 'tag' => "formateur:$function_name", 'desc' => __("Marque (si l’option est activée)"));
        return $result[$arg];
    }
    public function get_doc_logo_marque($arg = 'valeur', $dimensions = "")
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $image = "";
        if ($arg == 'valeur')
        {
            $style = get_style_dimensions($dimensions);
            if (!empty($this->logo_marque))
                $image = '<img src="'.$this->doc_url.$this->logo_marque.'" '.$style.' />';
        }
        $result = array('empty_ok' => true, 'valeur' => $image, 'tag' => "formateur:$function_name", 'desc' => __("Logo de la marque (dimensions à ajouter sous la forme {formateur:logo_marque:largxhaut} unité(s) en mm obligatoirement ; ex. {formateur:logo_marque:x50} pour une hauteur de 50mm et largeur automatique)"));
        return $result[$arg];
    }
    public function get_doc_activite($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Activité, métier"));
        return $result[$arg];
    }
    public function get_doc_statut($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => true, 'valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Statut formateur (si l’option est activée)"));
        return $result[$arg];
    }
    public function get_doc_realisations($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Réalisations"));
        return $result[$arg];
    }
    public function get_doc_cv_url($arg = 'valeur')
    {
        $cv_url = "";
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("URL du CV (interne ou externe)"));
        return $result[$arg];
    }
    public function get_doc_veille_bloc_notes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Bloc-notes de veille"));
        return $result[$arg];
    }
    
    public function body_class( $classes ) {
        $classes[] = 'page-membre';
        return $classes;
    }
}

function get_formateur_by_id($id)
{
    if ($id < 0) return null;
    global $Formateur;
    
    if (!isset($Formateur[$id]))
        $Formateur[$id] = new Formateur($id);
        
    return $Formateur[$id];
}


function get_tab_formateur_by_tab_id($ids)
{
    $formateur_tab = array();
    
    foreach($ids as $id)
        $formateur_tab[$id] = new Formateur($id);
    
    return $formateur_tab;
}

add_action('wp_ajax_check_my_formation', 'check_my_formation');
function check_my_formation()
{
    $reponse = array("my_formation" => 1);
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    
    $entite = ($_POST['object_class'] == "Formation") ? "formation" : "session";
    
    if ($role == 'um_formateur-trice' && (empty($_POST['value']) || !in_array($user_id, $_POST['value'])))
    {
        $reponse['my_formation'] = 0;
        $reponse['message'] = sprintf(__("Vous n'êtes pas ou plus dans l'animation de cette %s."), $entite).'<br />';
            $reponse['message'] .= '<span class="important">'.sprintf(__("Une fois cette page fermée ou rechargée vous ne pourrez plus modifier cette %s !"), $entite).'</span><br />'.__("Si ce n'est pas votre souhait, remettez-vous dans l'équipe pédagogique (Ctrl + clic pour sélectionner plusieurs noms)");
    }
    $reponse['log'] = "$role / user_id $user_id";
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_notification_action', 'notification_action');
function notification_action()
{
    $available_commands = array('send_demande_valid');
    $command = $_POST['command'];
    $reponse = array();
    $formateur = new Formateur(get_current_user_id());
    if ($formateur !== null && in_array($command, $available_commands))
        $reponse = $formateur->$command();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_update_notification_icone', 'update_notification_icone');
function update_notification_icone()
{
    $reponse = array();
    $formateur = new Formateur(get_current_user_id());
    if ($formateur !== null)
        $reponse['notif'] = $formateur->get_notifications();
    
    echo json_encode($reponse);
    die();
}
