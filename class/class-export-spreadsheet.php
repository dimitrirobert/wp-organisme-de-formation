<?php
/*
 * class-export-spreadsheet.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// Inclure dompdf, HtmlDomParser installés via composer
require_once wpof_path . '/vendor/autoload.php';
require_once(wpof_path . "/class/class-export.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ExportSpreadsheet extends Export
{
    public function __construct()
    {
        $this->spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $this->spreadsheet->getProperties()->setCreator('OPAGA')
            ->setLastModifiedBy('OPAGA');
    }
    
    public function export()
    {
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="01simple.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($this->spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
    
    public function get_spreadsheet()
    {
        return $this->spreadsheet;
    }
    
    public function get_sheet($id = null)
    {
        if ($id === null)
            return $this->spreadsheet->getActiveSheet();
        else
            return $this->spreadsheet->getSheet($id);
    }
    
    public function get_filemane()
    {
        return $this->filename;
    }
    
    public function get_format()
    {
        return $this->format;
    }
    
    public function set_spreadsheet($arg)
    {
        $this->spreadsheet = $arg;
    }
    
    public function set_spreadsheet_from_array($arg)
    {
        $active_sheet = $this->spreadsheet->getActiveSheet();
        $row_num = 1;
        foreach($arg as $row)
        {
            $col_num = 'A';
            foreach($row as $cell)
            {
                $active_sheet->setCellValue($col_num.$row_num, $cell);
                if ($col_num != 'Z')
                    $col_num++;
                else
                    $col_num = 'AA';
            }
            $row_num++;
        }
    }
    
    public function set_spreadsheet_from_html($arg)
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $this->spreadsheet = $reader->loadFromString($arg);
    }
    
    public function set_filemane($arg)
    {
        $this->filename = $arg;
    }
    
    public function set_format($arg)
    {
        $this->format = $arg;
    }
}
