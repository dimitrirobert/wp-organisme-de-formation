<?php
/*
 * class-board-toc.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/* Crée une table des matières pour tableau de bord */
class BoardToggleButton
{
    private $pattern = '<div class="toggle-button" data-args>INNER_HTML</div>';
    private $text = array('on' => 'Non défini', 'off' => '');
    private $data = array();
    
    public function __construct($args = array())
    {
        $this->button = $this->pattern;
        if (is_array($args))
        {
            if (isset($args['text_on']))
                $this->text['on'] = $this->text['off'] = esc_html($args['text_on']);
            if (isset($args['text_off']))
                $this->text['off'] = esc_html($args['text_off']);
            foreach(array('hide', 'keep', 'parent') as $a)
            {
                if (isset($args[$a]))
                    $this->data[] = 'data-'.$a.'="'.esc_html($args[$a]).'"';
            }
        }
    }
    
    public function get_html_button($toggle = 'on')
    {
        $button = $this->pattern;
        $button = str_replace('INNER_HTML', '<span class="on">'.$this->text['on'].'</span><span class="off">'.$this->text['off'].'</span>', $button);
        $button = str_replace('toggle-button', 'toggle-button '.$toggle, $button);
        $button = str_replace('data-args', implode(' ', $this->data), $button);
        
        return $button;
    }
}
