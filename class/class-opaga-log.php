<?php
/*
 * class-opaga-log.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class OpagaLog
{
    public function __construct()
    {
    }
    
    public function log_action($action, $entite, $meta_key = null, $last_value = null, $new_value = null)
    {
        if (is_array($last_value))
            $last_value = serialize($last_value);
        if (is_array($new_value))
            $new_value = serialize($new_value);
        
        $last_action = $this->get_my_last_log_action();
        if (!empty($last_action)
            && $last_action->action == $action
            && $last_action->entite_type == get_class($entite)
            && $last_action->entite_id == ($entite->id ?? 0)
            && $last_action->meta_key == $meta_key
        )
            $this->update_log_action($last_action, $new_value);
        else
            $this->insert_log_action($action, $entite, $meta_key, $last_value, $new_value);
    }
    
    public function insert_log_action($action, $entite, $meta_key = null, $last_value = null, $new_value = null)
    {
        global $wpdb, $wpof;
        
        $formateur = get_formateur_by_id(get_current_user_id());
        
        $query = $wpdb->prepare("INSERT INTO ".$wpdb->prefix.$wpof->suffix_log.
            " (user_name, action, entite_type, entite_id, entite_name, meta_key, last_value, new_value)".
            " VALUES (%s, %s, %s, %d, %s, %s, %s, %s);",
            $formateur->get_displayname(true),
            $action,
            get_class($entite),
            $entite->id ?? 0,
            $entite->get_displayname(true),
            $meta_key,
            $last_value,
            $new_value
        );
        
        $wpdb->query($query);
    }
    
    public function update_log_action($last_action, $new_value)
    {
        global $wpdb, $wpof;
        
        $query = $wpdb->prepare("UPDATE ".$wpdb->prefix.$wpof->suffix_log.
            " SET date = NOW(), new_value = %s".
            " WHERE id = %d;",
            $new_value,
            $last_action->id
        );
        
        $wpdb->query($query);
    }
    
    public function get_my_last_log_action()
    {
        global $wpdb, $wpof;
        
        $formateur = get_formateur_by_id(get_current_user_id());
        
        $query = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix.$wpof->suffix_log.
            " WHERE user_name = %s AND TIMEDIFF(NOW(), date) < '00:05:00.00'".
            " ORDER BY id DESC".
            " LIMIT 1;",
            $formateur->get_displayname(true)
        );
        
        return $wpdb->get_row($query);
    }
    
    public function get_table_cols()
    {
        global $wpdb, $wpof;
        
        $query = "SHOW FULL COLUMNS FROM ".$wpdb->prefix.$wpof->suffix_log.";";
        
        $cols_th = array();
        foreach($wpdb->get_results($query) as $row)
            $cols_th[$row->Field] = $row->Comment;
            
        return $cols_th;
    }
    
    public function get_log($max = 10000)
    {
        global $wpdb, $wpof;
        
        $query = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix.$wpof->suffix_log." ORDER BY id DESC LIMIT %d;", $max);
        return $wpdb->get_results($query);
    }
    
    public function get_formateur_log($formateur_name, $max = 10000)
    {
        global $wpdb, $wpof;
        
        $query = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix.$wpof->suffix_log." WHERE user_name LIKE %s ORDER BY id DESC LIMIT %d;", "%".str_replace('%20', ' ', $formateur_name)."%", $max);
        return $wpdb->get_results($query);
    }
    
    public function get_entite_log($entite_type, $entite_id, $max = 10000)
    {
        global $wpdb, $wpof;
        
        $query = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix.$wpof->suffix_log." WHERE entite_type = %s AND entite_id = %s ORDER BY id DESC LIMIT %d;", $entite_type, $entite_id, $max);
        return $wpdb->get_results($query);
    }
    
    public function get_connexion_log()
    {
        global $wpdb, $wpof;
        
        $query = "SELECT SQL_CALC_FOUND_ROWS MAX(date), user_name
                FROM ".$wpdb->prefix.$wpof->suffix_log."
                GROUP BY user_name;";
        return $wpdb->get_results($query);
    }
}

/*
add_action('wp_ajax_delete_aide', 'delete_aide');
function delete_aide()
{
    global $wpdb;
    
    if (isset($_POST['slug']))
    {
        $aide = new Aide($_POST['slug']);
        echo $aide->delete_hard();
    }
    
    die();
}
*/
?>
