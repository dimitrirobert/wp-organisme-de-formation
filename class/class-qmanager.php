<?php
/*
 * class-qmanager.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-questionnaire.php");

class QManager
{
    public Array $questionnaire = [];

    public function __construct()
    {
    }

    public function get_button(String $fonction, String $text, String $fa_icon = ""): String
    {
        ob_start();
        ?>
        <div class="icone-bouton qmanager" data-function="<?php echo $fonction; ?>"><?php echo $fa_icon; ?> <?php echo $text; ?></div>
        <?php
        return ob_get_clean();
    }

    public function get_questionnaire_control_head(): String
    {
        ob_start();
        ?>
        <tr>
            <th class="thin"><?php _e("ID"); ?></th>
            <th><?php _e("Titre"); ?></th>
            <th><?php _e("Sujet"); ?></th>
            <th><?php _e("Nature(s) de formation"); ?></th>
            <th><?php _e("Modalité de session"); ?></th>
            <th class="thin"><?php _e("Nb questions"); ?></th>
            <th><?php _e("Créé par"); ?></th>
            <th><?php _e("Niveau"); ?></th>
            <th class="thin not_orderable"><?php _e("Modifier"); ?></th>
            <th class="thin not_orderable"><?php _e("Dupliquer"); ?></th>
            <th class="thin not_orderable"><?php _e("Supprimer"); ?></th>
        </tr>
        <?php
        return ob_get_clean();
    }

    /**
     * Obtention des objets Questionnaire selon divers critères
     * 
     * Selon le contexte (responsable, admin ou formateur⋅ice) la sélection peut être restreinte selon le niveau du questionnaire (level)
     * @param Array $meta : tableau de meta_key => meta_value que le questionnaire doit vérifier
     * @param bool $by_type : le tableau de retour est divisé en sous-tableaux par types de questionnaire (subject)
     */
    public function get_questionnaires(Array $meta = [], bool $by_type = false): Array
    {
        global $wpdb, $wpof;

        $questionnaires = [];
        $table_base = $wpdb->prefix.$wpof->suffix_questionnaire;
        $table_meta = $wpdb->prefix.$wpof->suffix_questionnaire."meta";

        if (in_array($wpof->role, $wpof->super_roles))
            $query = "SELECT id FROM ".$table_base.";";
        else
            $query = $wpdb->prepare("SELECT id FROM ".$table_base." WHERE level = 'of' OR author_id = '%d';", get_current_user_id());

        $questionnaires_id = $wpdb->get_col($query);

        foreach($meta as $key => $value)
        {
            if ($value != -1 && isset($wpof->$key) && $wpof->$key->is_term($value))
            {
                $query = $wpdb->prepare("SELECT qid FROM ".$table_meta." WHERE meta_key = '%s' AND (meta_value = '%s' OR meta_value = -1) AND qid IN ('".implode("','", $questionnaires_id)."');",
                    $key, $value);
                $questionnaires_id = $wpdb->get_col($query);
            }
        }

        if (is_array($questionnaires_id))
        {
            foreach($questionnaires_id as $id)
            {
                if ($by_type)
                {
                    $q = new Questionnaire($id);
                    if (!isset($questionnaires[$q->subject]))
                        $questionnaires[$q->subject] = [];
                    $questionnaires[$q->subject][] = $q;
                }
                else
                    $questionnaires[$id] = new Questionnaire($id);
            }
        }

        $this->questionnaire = $questionnaires;

        return $questionnaires;
    }

    public function get_questions_id() : Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_question;

        $query = $wpdb->prepare("SELECT DISTINCT id FROM ".$table.";");
        return $wpdb->get_col($query);
    }

    /**
     * Retourne les id des questions marquée comme obligatoire dans au moins un questionnaire
     */
    public function get_needed_questions_id(String $qtype = null) : Array
    {
        global $wpdb, $wpof;
        $table_rel = $wpdb->prefix.$wpof->suffix_question_relationships;
        $table_q = $wpdb->prefix.$wpof->suffix_question;

        if ($qtype != null)
            $query = $wpdb->prepare("SELECT DISTINCT rel.question_id FROM ".$table_rel." AS rel, ".$table_q." AS q WHERE rel.needed = '1' AND q.qtype = '%s';", $qtype);
        else
            $query = $wpdb->prepare("SELECT DISTINCT question_id FROM ".$table_rel." WHERE needed = '1';");
        return $wpdb->get_col($query);
    }

    /**
     * Retourne les objets Question correspondant aux id obtenus par get_needed_questions_id()
     */
    public function get_needed_questions(String $qtype = null) : Array
    {
        $questions = [];
        foreach($this->get_needed_questions_id($qtype) as $id)
            $questions[$id] = new Question($id);

        return $questions;
    }
}
