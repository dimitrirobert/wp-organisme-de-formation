<?php
/*
 * class-board-by-date.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/* Crée une table des matières pour tableau de bord */
class BoardSelectByDate
{
    public const REF_DATE_FIRST = 'first';
    public const REF_DATE_LAST = 'last';
    public const REF_DATE_AND = 'and';
    public const REF_DATE_OR = 'or';
    public const META_KEY_YEAR = 'annee_comptable';
    public const META_KEY_FIRST = 'begin_date_filter';
    public const META_KEY_LAST = 'end_date_filter';
    public const META_KEY_REF_DATE = 'ref_date_filter';
    public const META_KEY_NO_DATE = 'no_date_filter';
    
    // limites de la plage de sélection
    private $begin_date = null;
    private $end_date = null;
    
    // date de référence à utiliser (par exemple, date de session)
    // first ou last
    private $ref_date = self::REF_DATE_FIRST;
    
    // inclure les entités sans date
    private $no_date_filter = false;
    
    // classe à filtrer
    private $filtered_class = "SessionFormation";
    
    // identifiant unique de formulaire
    private $form_id = null;
    
    // id de l'utilisateur affichant ce formulaire
    private $user_id;
    
    // une fonction à appeler après le choix de dates
    private $callback = "";
    
    public function __construct($filtered_class = "SessionFormation")
    {
        if (in_array($filtered_class, array("SessionFormation", "Client", "SessionStagiaire", "Formateur")))
        {
            $this->filtered_class = $filtered_class;
            $this->form_id = rand(0, 65535);
            $this->user_id = get_current_user_id();
            
            $this->begin_date = get_user_meta($this->user_id, self::META_KEY_FIRST, true);
            $this->end_date = get_user_meta($this->user_id, self::META_KEY_LAST, true);
            $annee = get_user_meta($this->user_id, self::META_KEY_YEAR, true);
            if (empty($annee))
                $annee = date('Y') - 1;
            if (empty($this->begin_date))
                $this->begin_date = '01/01/'.$annee;
            if (empty($this->end_date))
                $this->end_date = '31/12/'.$annee;
        
            $this->ref_date = get_user_meta($this->user_id, self::META_KEY_REF_DATE, true);
            if (empty($this->ref_date))
                $this->ref_date = self::REF_DATE_FIRST;
            
            $this->no_date_filter = get_user_meta($this->user_id, self::META_KEY_NO_DATE, true);
        }
    }
    
    public function get_form()
    {
        ob_start();
        
        if ($this->form_id !== null) : ?>
        <div class="select_by_date flexrow margin" id="<?php echo $this->form_id; ?>" data-class="<?php echo $this->filtered_class; ?>">
            <div><?php echo $this->get_range_selector(); ?></div>
            <div><?php echo $this->get_ref_date_selector(); ?></div>
            <div>
                <?php echo $this->get_nodate_checkbox(); ?>
                <div class="icone-bouton appliquer-plage"><?php _e("Appliquer et vider le cache"); ?></div>
            </div>
        </div>
        <?php endif;
        
        return ob_get_clean();
    }
    
    private function get_year_selector()
    {
        global $wpof;
        
        $annee_actuelle = date('Y');
            
        $liste_annee = array(-1 => __("Année"), 0 => __("Toutes"));
        for ($a = $wpof->annee1; $a <= $annee_actuelle+1; $a++)
            $liste_annee[$a] = $a;
        
        $html = '<input type="hidden" name="annee1" value="'.$wpof->annee1.'" />';
        $html .= select_by_list($liste_annee, self::META_KEY_YEAR);
        
        return $html;
    }
    
    private function get_nodate_checkbox()
    {
        ob_start();
        ?>
        <div>
        <label for="no_date_filter"><?php _e("Inclure les données non datées"); echo get_icone_aide("no_date_tri") ?></label>
        <input id="no_date_filter" name="no_date_filter" type="checkbox" value="1" <?php checked($this->no_date_filter, true); ?> />
        </div>
        <?php
        return ob_get_clean();
    }
    
    private function get_ref_date_selector()
    {
        ob_start();
        ?>
        <div><?php _e("Date de référence"); echo get_icone_aide("date_reference_tri") ?></div>
        <p>
        <input id="date_first" type="radio" name="<?php echo self::META_KEY_REF_DATE; ?>" value="<?php echo self::REF_DATE_FIRST; ?>" <?php checked($this->ref_date, self::REF_DATE_FIRST); ?> />
        <label for="date_first"><?php _e("Première date"); ?></label>
        </p>
        <p>
        <input id="date_last" type="radio" name="<?php echo self::META_KEY_REF_DATE; ?>" value="<?php echo self::REF_DATE_LAST; ?>" <?php checked($this->ref_date, self::REF_DATE_LAST); ?> />
        <label for="date_last"><?php _e("Dernière date"); ?></label>
        </p>
        <p>
        <input id="date_and" type="radio" name="<?php echo self::META_KEY_REF_DATE; ?>" value="<?php echo self::REF_DATE_AND; ?>" <?php checked($this->ref_date, self::REF_DATE_AND); ?> />
        <label for="date_and"><?php _e("Les deux"); ?></label>
        </p>
        <p>
        <input id="date_or" type="radio" name="<?php echo self::META_KEY_REF_DATE; ?>" value="<?php echo self::REF_DATE_OR; ?>" <?php checked($this->ref_date, self::REF_DATE_OR); ?> />
        <label for="date_or"><?php _e("L'une ou l'autre"); ?></label>
        </p>
        <?php
        return ob_get_clean();
    }
    
    private function get_range_selector()
    {
        ob_start();
        ?>
        <div class="choix_plage_date margin">
            <div class="year_selector"><?php _e("Plage de dates"); ?> <?php echo $this->get_year_selector(); ?></div>
            <div>
            <label for="debut<?php echo $this->form_id; ?>"><?php _e("Début"); ?></label>
            <input name="date_debut" type="text" class="datepicker debut" id="debut<?php echo $this->form_id; ?>" value="<?php echo $this->begin_date; ?>" />
            </div>
            <div>
            <label for="fin<?php echo $this->form_id; ?>"><?php _e("Fin"); ?></label>
            <input name="date_fin" type="text" class="datepicker fin" id="fin<?php echo $this->form_id; ?>" value="<?php echo $this->end_date; ?>" />
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function select_by_plage()
    {
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        
        global $wp_query;
        global $SessionFormation, $Formation;
        if (!empty($_SESSION['Session']))
        {
            $SessionFormation = $_SESSION['Session'];
        }
        else
        {
            //echo '<p>Sessions from DB</p>';
            // on crée les SessionFormation dans tous les cas, c'est là qu'il y a les dates
            $session_posts = get_posts( array('post_type' => 'session', 'posts_per_page' => -1));
            
            $date_min = DateTime::createFromFormat('d/m/Y', $this->begin_date);
            $date_max = DateTime::createFromFormat('d/m/Y', $this->end_date);
            
            foreach($session_posts as $post)
            {
                $formateur = get_post_meta($post->ID, "formateur", true);
                if (!is_array($formateur))
                    $formateur = array();
                if ($role == "um_formateur-trice" && !in_array($user_id, $formateur))
                    continue;
                $creneaux = get_post_meta($post->ID, "creneaux", true);
                if (!empty($creneaux))
                {
                    $dates = array_keys($creneaux);
                    if (!empty($dates))
                    {
                        $debut = DateTime::createFromFormat('d/m/Y', $dates[0]);
                        $fin = DateTime::createFromFormat('d/m/Y', $dates[count($dates)-1]);
                        $selected = false;
                        switch($this->ref_date)
                        {
                            case self::REF_DATE_FIRST:
                                $selected = ($debut >= $date_min && $debut <= $date_max);
                                break;
                            case self::REF_DATE_LAST:
                                $selected = ($fin >= $date_min && $fin <= $date_max);
                                break;
                            case self::REF_DATE_AND:
                                $selected = ($debut >= $date_min && $fin <= $date_max);
                                break;
                            case self::REF_DATE_OR:
                                $selected = (($debut >= $date_min && $debut <= $date_max) || ($fin >= $date_min && $fin <= $date_max));
                                break;
                        }
                        if ($selected)
                            $SessionFormation[$post->ID] = new SessionFormation($post->ID);
                    }
                }
                elseif ($this->no_date_filter == true)
                {
                    $SessionFormation[$post->ID] = new SessionFormation($post->ID);
                }
            }
            $_SESSION['Session'] = $SessionFormation;
        }
        
        // puis on crée le tableau de filtered_class si Client ou SessionStagiaire
        switch ($this->filtered_class)
        {
            case 'Client':
                select_clients_from_db();
                break;
            case 'SessionStagiaire':
                select_stagiaires_from_db();
                break;
            default:
                break;
        }
    }
}


add_action('wp_ajax_set_date_range_filter', 'set_date_range_filter');
function set_date_range_filter()
{
    $user_id = get_current_user_id();
    
    if (isset($_POST['plage']))
    {
        update_user_meta($user_id, BoardSelectByDate::META_KEY_FIRST, $_POST['plage']['date_debut']);
        update_user_meta($user_id, BoardSelectByDate::META_KEY_LAST, $_POST['plage']['date_fin']);
        update_user_meta($user_id, BoardSelectByDate::META_KEY_REF_DATE, $_POST['ref_date']);
        update_user_meta($user_id, BoardSelectByDate::META_KEY_NO_DATE, $_POST['no_date_filter']);
        if (isset($_SESSION['Session']))
            unset($_SESSION['Session']);
        if (isset($_SESSION['Client']))
            unset($_SESSION['Client']);
        if (isset($_SESSION['Stagiaire']))
            unset($_SESSION['Stagiaire']);
    }

    die();
}
