<?php
/*
 * class-wpof.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-opaga-log.php");

#[\AllowDynamicProperties]
class WPOF
{
    // Valeurs par défaut
    
    // Aide en ligne
    public $aide_file = "aide";
    public $aide_url = "https://opaga.fr/ressources/";
    
    // valeurs par défaut
    public $of_adresse = "";
    public $of_code_postal = "";
    public $of_datadock = "";
    public $of_qualiopi = "";
    public $of_description = "";
    public $of_exotva = "";
    public $of_hastva = "";
    public $of_logo = "";
    public $of_nom = "";
    public $of_noof = "";
    public $of_siret = "";
    public $of_rcs = "RCS";
    public $of_tva_intracom = "";
    public $of_ape = "";
    public $of_tauxtva = "";
    public $of_telephone = "";
    public $of_email = "";
    public $of_url = "";
    public $of_ville = "";
    public $pdf_header = "";
    public $pdf_footer = "";
    public $pdf_titre_font = "sans-serif";
    public $pdf_texte_font = "sans-serif";
    public $pdf_couleur_titre_doc = "#000000";
    public $pdf_couleur_titre_autres = "#000000";
    
    public $delai_alerte_veille = 15; // Délai avant alerte sur mise à jour de la veille personnelle (en jours)
    public $token_validity = 24; // validité d'un token en heures
    public $token_seconds_by_unit = 3600; // nombre de secondes par unité de token_validity
    public $token_length = 40;
    public $acompte_pourcent = 30;
    public $max_duree_bc;
    public $bc_page = -1;
    public $acompte_pourcent_max_particulier;
    
    // tarif plancher horaire qui déclenche une alerte si le tarif est inférieur
    public $tarif_heure_alerte = 15;
    
    // paramètres de l'URL en cas de réécriture
    public $uri_params = array();
    
    // taxonomies WordPress
    public $use_category = 1;
    public $use_tag = 1;
    
    public $content_revisions_max = 30;

    public $max_upload_size = 16; // taille maxi d'upload pour formateur⋅trices en Mo

    public $dompdf_https_noverif = 0;
    
    public $role;
    
    public OpagaLog $log;
    public BoardToc $toc;

    public function __construct()
    {
        $all_options = wp_load_alloptions();
        
        $this->aide = $this->init_aide();
        
        foreach ($all_options as $key => $value)
        {
            if (substr($key, 0, 10) == "wpof_aide_")
            {
                $prop = str_replace("-", "_", substr($key, 10));
                $this->aide->$prop = json_decode($value);
            }
            elseif (substr($key, 0, 5) == "wpof_")
            {
                $value = apply_filters( "option_{$key}", maybe_unserialize($value), $key);
                $prop = str_replace("-", "_", substr($key, 5));
                $this->$prop = $value;
            }
        }
        
        $this->log = new OpagaLog();
        $this->toc = new BoardToc();
    }

    /**
     * Initialisation des paramètres qui ne peuvent être initialisés dans le constructeur
     */
    public function init()
    {
        $this->role = wpof_get_role(get_current_user_id());

        global $tinymce_wpof_settings;
        if (in_array($this->role, $this->super_roles))
            $tinymce_wpof_settings['quicktags'] = true;
    }
    
    /**
     * Teste si le rôle courant ou lke rôle passé en argument est un super_role
     * @param mixed $role
     * @return bool
     */
    public function has_super_role($role = null) : bool
    {
        if ($role === null)
            $role = $this->role;
        return in_array($role, $this->super_roles);
    }
    // Renvoie les id des formateurs par rôle (formateur, responsable)
    public function get_formateurs_id_by_role($role = 'all')
    {
        global $wpdb, $wpof;
        
        $entities = array();
        if (substr_count($role, "form") > 0)
            $role = "um_formateur-trice";
        elseif (substr_count($role, "resp") > 0)
            $role = "um_responsable";
        
        switch($role)
        {
            case "um_formateur-trice":
            case "um_responsable":
                $query = $wpdb->prepare(
                    "SELECT user_id as id
                    FROM ".$wpdb->prefix."usermeta
                    WHERE meta_key = %s
                    AND meta_value LIKE %s;",
                    $wpdb->prefix."capabilities",
                    "%".$role."%"
                );
                break;
            case "all":
                $query = $wpdb->prepare(
                    "SELECT user_id as id
                    FROM ".$wpdb->prefix."usermeta
                    WHERE meta_key = %s
                    AND (meta_value LIKE %s OR meta_value LIKE %s);",
                    $wpdb->prefix."capabilities",
                    "%um_formateur-trice%", "%um_responsable%"
                );
                break;
            default:
                $query = null;
                break;
        }
        
        if ($query !== null)
            $entities = $wpdb->get_col($query);
        return $entities;
    }
    
    // Renvoie les id des formateurs par rôle (formateur, responsable)
    public function get_formateurs_by_role($role = 'all')
    {
        $formateurs = array();
        $tab_id = $this->get_formateurs_id_by_role($role);
        if (is_array($tab_id))
            foreach($tab_id as $id)
                $formateurs[$id] = get_formateur_by_id($id);
        return $formateurs;
    }
    
    // Renvoie tous les objets Formateur ayant le Wp_Term $term
    public function get_formateurs_by_term($term)
    {
        $formateurs = array();
        $formateurs_id = $this->get_entities_id_by_term("formateur", $term);
        if (is_array($formateurs_id))
        {
            foreach($formateurs_id as $user_id)
                $formateurs[$user_id] = new Formateur($user_id);
        }
        return $formateurs;
    }
    
    // Renvoie tous les id de Formateur ayant le Wp_Term $term
    public function get_formateurs_id_by_term($term)
    {
        return $this->get_entities_id_by_term("formateur", $term);
    }
    
    // Renvoie tous les objets Formation ayant le Wp_Term $term
    public function get_formations_by_term($term)
    {
        $formations = array();
        $formations_id = $this->get_entities_id_by_term("formation", $term);
        if (is_array($formations_id))
        {
            foreach($formations_id as $id)
                $formations[$id] = new Formation($id);
        }
        return $formations;
    }
    
    // Renvoie tous les id de Formation ayant le Wp_Term $term
    public function get_formations_id_by_term($term)
    {
        return $this->get_entities_id_by_term("formation", $term);
    }
    
    // Renvoie tous les objets SessionFormation ayant le Wp_Term $term
    public function get_sessions_by_term($term)
    {
        $sessions = array();
        $sessions_id = $this->get_entities_id_by_term("session", $term);
        if (is_array($sessions_id))
        {
            foreach($sessions_id as $id)
                $sessions[$id] = new SessionFormation($id);
        }
        return $sessions;
    }
    
    // Renvoie tous les id de SessionFormation ayant le Wp_Term $term
    public function get_sessions_id_by_term($term)
    {
        return $this->get_entities_id_by_term("session", $term);
    }
    
    private function get_entities_id_by_term($entity, $term)
    {
        global $wpdb, $wpof;
        
        $entities = array();
        
        switch($entity)
        {
            case "formateur":
                $query = $wpdb->prepare(
                    "SELECT user_id as id
                    FROM ".$wpdb->prefix."usermeta
                    WHERE meta_key = 'tags'
                    AND JSON_SEARCH(REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(meta_value, 'i:\\\d*;s:\\\d*:', ''), ';', ','), '.*{(.*)}', '[\\\\1]'), 'one', %s) is not null;",
                    $term
                );
                break;
            case "formation":
                $query = $wpdb->prepare(
                    "SELECT tr.object_id as id
                    FROM ".$wpdb->prefix."terms as t, ".$wpdb->prefix."term_relationships as tr
                    WHERE t.term_id = tr.term_taxonomy_id
                    AND t.slug = '%s';",
                    $term
                );
                break;
            case "session":
                $query = $wpdb->prepare(
                    "SELECT pm.post_id as id
                    FROM ".$wpdb->prefix."terms as t, ".$wpdb->prefix."term_relationships as tr, ".$wpdb->prefix."postmeta as pm
                    WHERE t.term_id = tr.term_taxonomy_id
                    AND t.slug = '%s'
                    AND pm.meta_key = 'formation'
                    AND pm.meta_value = tr.object_id",
                    $term
                );
        }
        
        return $wpdb->get_col($query);
    }
    
    public function get_formations($formateur_id = null)
    {
        $formations = array();
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        $where_request = "";
        
        if ($user_id != $formateur_id && !in_array($role, $this->super_roles))
            $where_request = "meta_key = 'acces_public' AND meta_value = 1";
        
        foreach($this->get_actions_id('formation', $formateur_id, $where_request) as $sid)
            $formations[$sid] = new Formation($sid);
        
        ksort($formations);
        return $formations;
    }
    
    public function get_sessions($formateur_id = null, $quand = null)
    {
        $sessions = array();
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        $where_request = "";
        
        if ($user_id != $formateur_id && !in_array($role, $this->super_roles))
            $where_request = "meta_key = 'acces_session' AND meta_value = 'public'";
        
        foreach($this->get_actions_id('session', $formateur_id, $where_request, $quand) as $sid)
            $sessions[$sid] = new SessionFormation($sid);
        
        return $sessions;
    }
    
    public function get_actions_id($action, $formateur_id = null, $where_request = null, $quand = null)
    {
        global $wpdb, $wpof;
        
        $action_id = array();
        if (in_array($action, array("session", "formation")))
        {
            if ($formateur_id === null)
            {
                $query = $wpdb->prepare(
                    "SELECT p.ID as action_id
                    FROM ".$wpdb->prefix."posts as p
                    WHERE p.post_type = '%s'
                    AND p.post_status = 'publish';",
                    $action
                );
                $action_id = $wpdb->get_col($query);
            }
            else
            {
                $query = $wpdb->prepare(
                    "SELECT pm.post_id as action_id ".
                    "FROM ".$wpdb->prefix."postmeta as pm, ".$wpdb->prefix."posts as p ".
                    "WHERE pm.meta_key = 'formateur' ".
                    "AND pm.post_id = p.ID ".
                    "AND p.post_type = '%s' ".
                    "AND p.post_status = 'publish' ".
                    "AND JSON_SEARCH(REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(pm.meta_value, 'i:\\\d*;s:\\\d*:', ''), ';', ','), '.*{(.*)}', '[\\\\1]'), 'one', %d) is not null;",
                    $action,
                    $formateur_id
                );
                $action_id = $wpdb->get_col($query);
            }
            
            if (!empty($where_request))
            {
                $all_id = "'".join("','", $action_id)."'";
                $query = "SELECT post_id as action_id FROM ".$wpdb->prefix."postmeta WHERE ".$where_request." AND post_id IN (".$all_id.");";
                $action_id = $wpdb->get_col($query);
            }
            
            if ($quand !== null)
            {
                $all_id = "'".join("','", $action_id)."'";
                //echo '<pre>'.$all_id.'</pre>';
                switch ($quand)
                {
                    case "present":
                        $query = 
                        "SELECT post_id as action_id
                        FROM ".$wpdb->prefix."postmeta
                        WHERE meta_key = 'last_date_timestamp'
                        AND meta_value >= UNIX_TIMESTAMP()
                        AND post_id IN
                        (SELECT post_id as action_id
                        FROM ".$wpdb->prefix."postmeta
                        WHERE meta_key = 'first_date_timestamp'
                        AND meta_value <= UNIX_TIMESTAMP()
                        AND post_id IN (".$all_id."));" ;
                        $action_id = $wpdb->get_col($query);
                        break;
                    case "futur":
                        // la liste des actions futures doit aussi contenir celles qui n'ont pas de dates
                        // on sélectionne d'abord toutes celles qui ne sont pas dans le futur puis on fait une différence avec toutes les actions
                        $query_no_futur = 
                        "SELECT post_id as action_id
                        FROM ".$wpdb->prefix."postmeta
                        WHERE meta_key = 'first_date_timestamp'
                        AND meta_value <= UNIX_TIMESTAMP()
                        AND post_id IN (".$all_id.");";
                        $no_furure_id = $wpdb->get_col($query_no_futur);
                        $action_id = array_diff($action_id, $no_furure_id);
                        break;
                    case "passe":
                        $query =
                        "SELECT post_id as action_id
                        FROM ".$wpdb->prefix."postmeta
                        WHERE meta_key = 'last_date_timestamp'
                        AND meta_value < UNIX_TIMESTAMP()
                        AND post_id IN (".$all_id.");" ;
                        $action_id = $wpdb->get_col($query);
                        break;
                    default:
                        break;
                }
            }
            //echo '<pre>'.$query.'</pre>';
        }
        return $action_id;
    }
    
    public function filter_id_by_value($array_id, $entite, $meta_key, $meta_value, $op = '=', $not = false)
    {
        global $wpdb;
        $filtered_id = array();
        
        if (is_array($array_id))
        {
            $all_id = "'".join("','", $array_id)."'";
            switch($entite)
            {
                case "session":
                case "formation":
                    $query = $wpdb->prepare
                    (
                        "SELECT post_id as action_id
                        FROM ".$wpdb->prefix."postmeta
                        WHERE meta_key = '%s'
                        AND meta_value ".$op." '%s'
                        AND post_id IN (".$all_id.");",
                        $meta_key,
                        $meta_value
                    );
                    $filtered_id = $wpdb->get_col($query);
                    break;
                case "formateur":
                    break;
                default:
                    break;
            }
        }
        if ($not)
            return  array_diff($array_id, $filtered_id);
        else
            return $filtered_id;
    }
    
    function get_entite_by_id($class, $id)
    {
        global $wpdb, $wpof;
        if (class_exists($class))
        {
            switch ($class)
            {
                case "SessionFormation":
                case "Formation":
                case "Formateur":
                case "Creneau":
                    return new $class($id);
                    break;
                case "Client":
                    $query = $wpdb->prepare("SELECT DISTINCT session_id FROM ".$wpdb->prefix.$wpof->suffix_client." WHERE client_id = %d;", $id);
                    $session_id = $wpdb->get_var($query);
                    return new Client($session_id, $id);
                    break;
                case "SessionStagiaire":
                    $query = $wpdb->prepare("SELECT DISTINCT session_id FROM ".$wpdb->prefix.$wpof->suffix_stagiaire." WHERE stagiaire_id = %d;", $id);
                    $session_id = $wpdb->get_var($query);
                    return new SessionStagiaire($session_id, $id);
                    break;
            }
        }
        return null;
    }
    
    public function init_aide()
    {
        $aide_data = file_get_contents(wpof_path . $this->aide_file . ".json");
            
        return json_decode($aide_data);
    }
    
    public function export_aide($format = "json", $display = false)
    {
        $data = "";
        switch ($format)
        {
            case "json":
                $data = json_encode($this->aide, JSON_UNESCAPED_UNICODE);
                break;
            case "html":
                foreach($this->aide as $a)
                {
                    $data .= "<h2>{$a->titre}</h2>";
                    $data .= $a->texte;
                }
                break;
            default:
                break;
        }
        
        if ($display)
            echo $data;
        else
        {
            $file_name = wpof_path . $this->aide_file . "." . $format;
            $file_handle = fopen($file_name, "w");
            fwrite($file_handle, $data);
            fclose($file_handle);
            return $file_name;
        }
    }
    
    public function get_doc_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_nom, 'tag' => "of:$function_name", 'desc' => __("Nom de l’organisme de formation"));
        return $result[$arg];
    }
    public function get_doc_adresse($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_adresse, 'tag' => "of:$function_name", 'desc' => __("Adresse de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_code_postal($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_code_postal, 'tag' => "of:$function_name", 'desc' => __("Code postal de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_datadock($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_datadock, 'tag' => "of:$function_name", 'desc' => __("Numéro Datadock de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_qualiopi($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_qualiopi, 'tag' => "of:$function_name", 'desc' => __("Numéro Qualiopi de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_description($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_description, 'tag' => "of:$function_name", 'desc' => __("Description de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_logo($arg = 'valeur', $dimensions = "")
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $style = get_style_dimensions($dimensions);
        
        $logo = '<img src="'.wp_get_attachment_url($this->of_logo).'" '.$style.' />';
        $result = array('empty_ok' => false, 'valeur' => $logo, 'tag' => "of:$function_name", 'desc' => __("Logo de l’organisme de formation (dimensions à ajouter sous la forme {of:logo:largxhaut} unité(s) en mm obligatoirement ; ex. {of:logo:x10} pour une hauteur de 10mm et largeur automatique)"));
        return $result[$arg];
    }

    public function get_doc_no_of($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_noof, 'tag' => "of:$function_name", 'desc' => __("Numéro de déclaration d'activité de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_siret($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_siret, 'tag' => "of:$function_name", 'desc' => __("Siret de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_ape($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_ape, 'tag' => "of:$function_name", 'desc' => __("Code APE de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_rcs($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_rcs, 'tag' => "of:$function_name", 'desc' => __("RCS de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_tva_intracom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_tva_intracom, 'tag' => "of:$function_name", 'desc' => __("Numéro de TVA intra-communautaire"));
        return $result[$arg];
    }

    public function get_doc_url($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_url, 'tag' => "of:$function_name", 'desc' => __("Lien Web vers lequel renvoyer les clients"));
        return $result[$arg];
    }

    public function get_doc_telephone($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_telephone, 'tag' => "of:$function_name", 'desc' => __("Numéro de téléphone de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_ville($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_ville, 'tag' => "of:$function_name", 'desc' => __("Ville de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_email($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_email, 'tag' => "of:$function_name", 'desc' => __("Adresse électronique de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_signataire($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $resp_nom = "";
        if ($arg == 'valeur')
        {
            $signature_pouvoir = is_signataire();
            $fonction = get_fonction_responsable();
            if (!empty($fonction))
                $fonction = ', '.$fonction;
            
            if ($signature_pouvoir !== false)
            {
                $resp = new Formateur(get_current_user_id());
                if ($resp)
                {
                    $resp_nom = $resp->get_displayname();
                    if ($signature_pouvoir == "signature_po")
                        $resp_nom .= " (P/O ".signataire_pour_nom().$fonction.")";
                    else
                        $resp_nom .= $fonction;
                }
                else
                    $resp_nom = '<span class="erreur">'.__("Vous n'êtes pas autorisé.e à signer, ce message ne devrait jamais s'afficher...").'</span>';
            }
            else
                $resp_nom = signataire_pour_nom().$fonction;
        }
        
        $result = array('empty_ok' => false, 'valeur' => $resp_nom, 'tag' => "of:$function_name", 'desc' => __("Signataire (officiel ou sur ordre) des documents et fonction du/de la signataire officielle"));
        return $result[$arg];
    }

    public function get_doc_responsable_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $resp_nom = "";
        if ($arg == 'valeur')
            $resp_nom = signataire_pour_nom();
        
        $result = array('empty_ok' => false, 'valeur' => $resp_nom, 'tag' => "of:$function_name", 'desc' => __("Responsable de formation signataire des documents"));
        return $result[$arg];
    }

    public function get_doc_responsable_fonction($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $resp_fonction = "";
        if ($arg == 'valeur')
            $resp_fonction = get_fonction_responsable();
        $result = array('empty_ok' => false, 'valeur' => $resp_fonction, 'tag' => "of:$function_name", 'desc' => __("Fonction du/de la responsable de formation"));
        return $result[$arg];
    }
    public function get_doc_image_signature($arg = 'valeur', $dimensions = "")
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $signature = "";
        if ($arg == 'valeur')
        {
            $responsable = new Formateur(get_current_user_id());
            if (champ_additionnel("image_signature") && !empty($responsable->image_signature))
            {
                $style = get_style_dimensions($dimensions);
                $signature = '<img src="'.$responsable->doc_url.$responsable->image_signature.'" '.$style.' />';
            }
        }
        $result = array('empty_ok' => true, 'valeur' => $signature, 'tag' => "of:$function_name", 'desc' => __("Image de la signature responsable (dimensions à ajouter sous la forme {of:image_signature:largxhaut} unité(s) en mm obligatoirement ; ex. {of:image_signature:x50} pour une hauteur de 50mm et largeur automatique)"));
        return $result[$arg];
    }
    
    public function get_doc_tauxtva($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->of_tauxtva, 'tag' => "of:$function_name", 'desc' => __("Taux de la TVA"));
        return $result[$arg];
    }
    
    public function get_doc_today($arg = 'valeur')
    {
        $date = '';
        if ($arg == 'valeur')
        {
            global $document_actuel;
            if (champ_additionnel('periode_validite') && $document_actuel->date_debut != "")
                $date = pretty_print_dates($document_actuel->date_debut);
            else
                $date = pretty_print_dates(date("d/m/Y", time()));
        }
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $date, 'tag' => "of:$function_name", 'desc' => __("Date du jour"));
        return $result[$arg];
    }
}
