<?php
/*
 * class-client.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

class Client
{
    public const ENTITE_PERS_MORALE = 'morale';
    public const ENTITE_PERS_PHYSIQUE = 'physique';
    public const FINANCEMENT_OPAC = 'opac';

    public $adresse = "";
    public $code_postal = "";
    public $ville = "";
    public $pays = "France";
    public $telephone = "";
    public $nom = "";
    
    public $permalien = "";
    
    public $entite_client = self::ENTITE_PERS_MORALE; // personne morale ou physique
    
    public $contact_needed = false;
    public $contact = "";
    public $contact_email = "";
    public $contact_tel = "";
    public $contact_stagiaire = 0;
    
    public $responsable = "";
    public $responsable_email = "";
    public $responsable_tel = "";
    public $responsable_fonction = "";
    public $responsable_stagiaire = 0;
    
    public $rcs = "";
    public $siret = "";
    public $num_of = "";
    public $region = -1;
    public $nature_formation = "";
    public $financement = "";
    public $echeancier = "";
    public $opco = "";
    public $no_dossier_opco = "";
    public $etat_session = "initial";
    
    public $numero_contrat = "";
    public $numero_facture = "";
    public $verifie = 0;

    public $tva;
    
    public $tarif_heure = 0;
    public $tarif_total_chiffre = 0;
    public $tarif_animation = 0;
    public $tarif_preparation = 0;
    public $tarif_total_autres_chiffre = 0;
    public $acompte = 0;
    public $autres_frais = "";
    public $exe_comptable = array();
    
    public $dates_array = array();
    public $dates_texte = "";
    public $nb_heure;
    public $nb_heure_decimal = 0;
    public $nb_heure_estime;
    public $nb_heure_estime_decimal = 0;
    public $creneaux = array();
    public $creneaux_actifs = array();
    public $creneaux_object = array();
    
    public $doc_necessaire = array();
    public $doc_uid = array();
    private $contrat_uid = false;
    
    public $attentes = "";
    public $date_modif_attentes = "aucune";
    public $date_entretien = "";
    public $analyse_besoin = "";
    
    public $stagiaires = array();
    public $nb_stagiaires = 0;
    public $nb_confirmes = 0;
    public $nb_heures_stagiaires = 0;
    public $session_formation_id = -1;
    public $id = -1;
    
    // L'id du client dans un système externe (si le client provient d'un systeme externe)
    // @see classes ERP, OpagaPlugin
    public $external_id = -1;

    // token d'accès aux infos
    public $token = "";
    public $token_time = 0;
    public $acces = 0;
    
    // table suffix
    private $table_suffix;
    
    public function __construct($session_id = -1, $client_id = -1)
    {
        global $suffix_client;
        global $wpof;
        $this->table_suffix = $suffix_client;
        
        if ($session_id > 0)
        {
            $this->session_formation_id = $session_id;
            $session_formation = get_session_by_id($session_id);
            
            //log_add(__METHOD__."({$this->nom})", "client");
            if ($client_id > 0)
            {
                $this->id = $client_id;
                global $Client;
                $Client[$client_id] = $this;
                
                $this->permalien = $session_formation->permalien.'client/'.$this->id;
                
                $meta = $this->get_meta();
                
                foreach($meta as $m)
                {
                    if (isset($this->{$m['meta_key']}))
                    {
                        if (is_array($this->{$m['meta_key']}))
                        {
                            $array_value = unserialize($m['meta_value']);
                            if (is_array($array_value))
                                $this->{$m['meta_key']} = array_stripslashes($array_value);
                        }
                        else
                            $this->{$m['meta_key']} = stripslashes($m['meta_value']);
                    }
                }
                
                
                // les créneaux actifs sont stockés dans un tableau plat : l'id du créneau en clé et la valeur 0 (inactif) ou 1 (actif)
                if ($session_formation->nb_creneaux > 0 && (!isset($this->creneaux) || empty($this->creneaux)))
                    $this->init_creneaux();
                
                $this->calcule_temps_session();
                
                $this->calcule_tarif();
                
                $this->init_stagiaires();
                
                $doc_contexte = 0;

                if (!is_numeric($this->nb_stagiaires))
                    $this->nb_stagiaires = 0;
                
                // cas de la sous-traitance : le client est un autre OPAC
                if ($this->financement == self::FINANCEMENT_OPAC)
                {
                    $doc_contexte = $wpof->doc_context->sous_traitance | $wpof->doc_context->client;
                    if ($this->nb_heures_stagiaires == 0)
                        $this->nb_heures_stagiaires = (int)$this->nb_stagiaires * (int)$this->nb_heure_estime_decimal;
                }
                elseif ($this->entite_client == self::ENTITE_PERS_MORALE)
                {
                    $doc_contexte = $wpof->doc_context->morale | $wpof->doc_context->client;
                }
                else
                {
                    $doc_contexte = $wpof->doc_context->physique | $wpof->doc_context->client | $wpof->doc_context->stagiaire;
                }

                // documents nécessaires
                foreach($wpof->documents->term as $doc_type => $doc)
                {
                    if (($doc->contexte & $doc_contexte & $wpof->doc_context->contrat) != 0 && ($doc->contexte & $doc_contexte & $wpof->doc_context->entite) != 0)
                        $this->doc_necessaire[] = $doc_type;
                }
                $this->doc_suffix = "c".$this->id;
                
                $this->nature_formation = $session_formation->nature_formation;
            }
        }
    }
    
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session formation
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;
        
        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->session_formation_id, $wpof->doc_context->client, $this->id);
            $Documents[$doc->id] = $doc;
            $this->doc_uid[] = $doc->id;
            if ($doc->modele->is_contrat)
                $this->contrat_uid = $doc->id;
        }
    }

    /**
     * Renvoie le docuid du contrat de ce client
     * @param $check : si true on vérifie que le contrat est bien signé par les deux parties
     * @return : docuid ou false si $check vaut true et que le contrat n'est pas entièrement signé
     */
    public function get_contrat_uid(bool $check = true): string|bool
    {
        if ($this->contrat_uid === false)
            $this->init_docs();

        $contrat_necessaire = $this->contrat_uid;
        if ($contrat_necessaire != false)
        {
            global $Documents;

            if ($check && !($Documents[$this->contrat_uid]->valid & Document::VALID_CLIENT_DONE && $Documents[$this->contrat_uid]->valid & Document::VALID_RESPONSABLE_DONE)) {
                $contrat_necessaire = false;
            }
        }

        return $contrat_necessaire;
    }

    /**
     * Vérification des obligations légales
     * 
     */
    public function get_missing_legal(): array
    {
        $session = get_session_by_id($this->session_formation_id);
        $manque = [];

        if (empty($this->tarif_total_chiffre)) {
            $manque[] = __("Le montant total de la session pour ce client n'est pas renseigné");
        }
        if (!$this->get_contrat_uid(true)) {
            $manque[] = __("Le contrat de formation n'est pas valide (absent ou pas de signature client et responsable)");
        }
        if ($this->nb_heure_decimal == 0 && $this->financement != Client::FINANCEMENT_OPAC)
            $manque[] = sprintf(__("Hors sous-traitance vous devez définir des créneaux horaires. Merci de le renseigner dans l'onglet <a href=\"%s\">Dates</a>"), $session->permalien."dates");

        if ($session->is_session_ended())
        {
            if (empty($session->get_lieu_infos()))
                $manque[] = sprintf(__("Aucun lieu n'est précisé. Merci de le renseigner dans l'onglet <a href=\"%s\">Lieu</a>"), $session->permalien."lieu");
            if (empty($this->nb_confirmes)) {
                $manque[] = __("Le nombre de stagiaires n'est pas renseigné");
            }
            if (empty($this->nb_heure_estime_decimal)) {
                $manque[] = __("Le nombre d'heures par stagiaire n'est pas renseigné");
            }
            if($this->financement == Client::FINANCEMENT_OPAC && empty($this->nb_heures_stagiaires)) {
                $manque[] = __("Le cumul d'heures effectuées par tous les stagiaires n'est pas renseigné");
            }
        }

        return $manque;
    }
    
    public function init_creneaux()
    {
        $session = get_session_by_id($this->session_formation_id);
        $creneaux = array();
        foreach($session->creneaux as $d)
            foreach($d as $c)
                $creneaux[$c->id] = 1;
        $this->update_meta("creneaux", $creneaux);
        $this->update_meta("creneaux_actifs", $creneaux);
    }
    
    public function init_stagiaires()
    {
        global $SessionStagiaire;
        $update = false;
        $this->nb_confirmes = 0;
        
        foreach($this->stagiaires as $key => $stagiaire_id)
        {
            if (is_numeric($stagiaire_id) && $stagiaire_id > 0)
            {
                $SessionStagiaire[$stagiaire_id] = new SessionStagiaire($this->session_formation_id, $stagiaire_id);
                if ($SessionStagiaire[$stagiaire_id]->client_id == -1)
                    $SessionStagiaire[$stagiaire_id]->update_meta("client_id", $this->id);
                if ($SessionStagiaire[$stagiaire_id]->confirme != 0)
                    $this->nb_confirmes++;
            }
            else
            {
                unset($this->stagiaires[$key]);
                $update = true;
            }
        }
        
        if ($update)
            $this->update_meta("stagiaires", array_values($this->stagiaires));
        
        if ($this->financement != "opac")
        {
            if ($this->nb_stagiaires != count($this->stagiaires))
                $this->update_meta("nb_stagiaires", count($this->stagiaires));
        }
        else
            $this->nb_confirmes = $this->nb_stagiaires;
        
        if ($this->entite_client == self::ENTITE_PERS_PHYSIQUE)
        {
            $this->nb_stagiaires = $this->nb_confirmes = 1;
            if (empty($this->stagiaires))
            {
                $this->init_stagiaire_from_db();
                if (empty($this->stagiaires))
                {
                    log_add("Erreur : client ".$this->nom." n'a pas de stagiaire");
                    $this->nb_confirmes = 0;
                }
            }
            else
            {
                $this->stagiaire = $SessionStagiaire[reset($this->stagiaires)];
                $this->nom = $this->stagiaire->get_displayname();
            }
        }
    }
    
    /*
     * Procédure de secours pour réinitialiser les stagiaires en cherchant dans la table des stagiaires le client_id courant
     */
    private function init_stagiaire_from_db()
    {
        global $wpdb, $suffix_session_stagiaire;
        
        $this->stagiaires = array();
        $table_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
        $query = $wpdb->prepare("SELECT stagiaire_id FROM $table_stagiaire WHERE meta_key = 'client_id' AND meta_value = '%d';", $this->id);
        $stagiaire = $wpdb->get_results($query);
        foreach($stagiaire as $row)
            $this->stagiaires[] = $row->stagiaire_id;
        $this->update_meta("stagiaires");
    }
    
    /*
     * renvoie la liste des stagiaires (prénom nom) sous forme de liste
     * , : séparés par des virgules
     * liste : liste à puce li sans les balises ul ou ol, à rajouter autour de cet appel
     * tableau : tableau tr td sans les balises table, à rajouter autour de cet appel
     * En fait, seule la première lettre est analysée, donc souplesse
     * 
     * Si $confirme est vrai, on ne sort que les stagiaires confirmés
     */ 
    public function get_liste_stagiaires($type_liste = ',', $confirme = false)
    {
        $liste_stagiaires = "";
        $stagiaires = array();
        $fonctions = array();
        $format = substr($type_liste, 0, 1);
        
        foreach($this->stagiaires as $sid)
        {
            $s = get_stagiaire_by_id($this->session_formation_id, $sid);
            if (!$confirme || $s->confirme == 1)
            {
                $text = $s->get_displayname();
                if ($format != 't' && !empty($s->fonction))
                    $text .= '('.$s->fonction.')';
                else
                    $fonctions[] = $s->fonction;
                
                $stagiaires[] = $text;
            }
        }
        
        if (!empty($stagiaires))
            switch($format)
            {
                case 'l':
                    $liste_stagiaires = "<li>".join("</li><li>", $stagiaires)."</li>";
                    break;
                case 't':
                    $liste_stagiaires = "";
                    for ($i = 0; $i < count($stagiaires); $i++)
                        $liste_stagiaires .= '<tr><td>'.$stagiaires[$i].'</td><td>'.$fonctions[$i].'</td></tr>';
                    break;
                default:
                    $liste_stagiaires = join(', ', $stagiaires);
                    break;
            }
        
        return $liste_stagiaires;
    }
    
    public function get_nom()
    {
        global $wpof;
        $nom = "";
        if (champ_additionnel('numero_contrat'))
            $nom = "<strong>".$this->numero_contrat."</strong> ";
        $nom .= (empty($this->nom)) ? __("Sans nom")." ID = ".$this->id : $this->nom;
        
        return $nom;
    }
    
    public function get_nb_heure_stagiaire()
    {
        if ($this->financement != self::FINANCEMENT_OPAC)
        {
            $total = 0;
            
            foreach($this->stagiaires as $user_id)
            {
                $stagiaire = new SessionStagiaire($this->session_formation_id, $user_id);
                $total += $stagiaire->nb_heure_estime_decimal;
            }
            return $total;
        }
        else 
            return $this->nb_heures_stagiaires;
    }
    
    public function get_meta($meta_key = null)
    {
        return get_client_meta($this->id, $meta_key);
    }

    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpdb, $wpof;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        if ($meta_value == "delete_meta")
            return $this->delete_meta($meta_key);
        
        $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key, $meta_value);
        
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if ($meta_key == "creneaux")
            $meta_value = $this->update_creneaux();
        
        if (is_array($meta_value))
            $meta_value = serialize($meta_value);
        
        // si le client n'est pas encore entré dans la base on lui crée un nouvel id
        if ($this->id < 1)
            $this->id = $this->last_client_id() + 1;
        
        if (isset($_SESSION['Client'][$this->id]))
            $_SESSION['Client'][$this->id]->$meta_key = $meta_value;
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, client_id, meta_key, meta_value)
            VALUES ('%d', '%d', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->id, $meta_key, $meta_value, $meta_value);
        
        return $wpdb->query($query);
    }
    
    private function update_creneaux()
    {
        global $wpdb, $wpof;
        $creneaux = array();
        $actifs = array();
        $session = get_session_by_id($this->session_formation_id);
        
        foreach($session->creneaux as $d)
            foreach($d as $c)
                if (!isset($this->creneaux[$c->id]))
                    $this->creneaux[$c->id] = 1;
        
        foreach($this->creneaux as $cid => $value)
        {
            $query = $wpdb->prepare("SELECT id FROM ".$wpdb->prefix.$wpof->suffix_creneaux. " WHERE id = %d AND session_id = %d;", $cid, $this->session_formation_id );
            $id_from_db = $wpdb->get_var($query);
            if ($id_from_db == $cid)
            {
                $creneaux[$cid] = $value;
                if ($value == 1)
                    $actifs[$cid] = 1;
            }
        }
        $this->update_meta("creneaux_actifs", $actifs);
        
        foreach($this->stagiaires as $sid)
        {
            $stagiaire = get_stagiaire_by_id($this->session_formation_id, $sid);
            $stagiaire->update_meta("creneaux");
        }
        
        return $creneaux;
    }
    
    public function delete_meta($meta_key)
    {
        global $wpdb, $wpof;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare ("DELETE FROM $table WHERE meta_key = '%s' AND session_id = '%d' AND client_id = '%d';", $meta_key, $this->session_formation_id, $this->id);
        $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key);
        $this->$meta_key = null;
        
        return $wpdb->query($query);
    }
    
    // Supprime un élément d'un tableau de sous-entité (stagiaires)
    public function supprime_sous_entite($tab_name, $id)
    {
        if (isset($this->$tab_name) && is_array($this->$tab_name))
        {
            $key = array_search($id, $this->$tab_name);
            unset($this->$tab_name[$key]);
            $this->update_meta($tab_name, $this->$tab_name);
            if (isset($_SESSION['Client'][$this->id]))
                $_SESSION['Client'][$this->id]->$tab_name = $this->$tab_name;
        }
    }

    // Calcule le tarif total
    public function calcule_tarif()
    {
        if (!is_numeric($this->tarif_total_autres_chiffre))
            $this->update_meta('tarif_total_autres_chiffre', 0);
        if (!is_numeric($this->tarif_total_chiffre))
            $this->update_meta('tarif_total_chiffre', 0);
        if ($this->nb_heure_estime_decimal > 0)
            $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_estime_decimal, 2));
            
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
        $this->tarif_total_autres_lettre = num_to_letter($this->tarif_total_autres_chiffre);
    }
    

    // Calcule le temps (en heures) de la session contractualisé avec ce client (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $this->temps = DateTime::createFromFormat("U", "0");
        $this->dates_array = array();
        
        if ($this->financement != Self::FINANCEMENT_OPAC)
        {
            foreach($session_formation->creneaux as $date => $tab_date)
            {
                foreach($tab_date as $creno)
                {
                    if (!empty($this->creneaux[$creno->id]))
                    {
                        if ($creno->type != "technique")
                            $this->temps->add($creno->duree);
                        $this->dates_array[] = $date;
                        $this->creneaux_object[$date][] = $creno;
                    }
                }
            }
            
            if (empty($this->dates_array))
                $this->dates_array = array_keys($session_formation->creneaux);
            $this->dates_array = array_unique($this->dates_array);
        }
        else
            $this->dates_array = array_keys($session_formation->creneaux);
            
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        $h = $this->temps->format("U") / 3600;
        $m = ($this->temps->format("U") % 3600) / 60;
        $this->nb_heure_decimal = $this->temps->format("U") / 3600;
        $this->nb_heure = sprintf("%02d:%02d", $h, $m);
        
        if (empty($this->nb_heure_estime_decimal))
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
            /*
            $this->nb_heure_decimal = $this->nb_heure_estime_decimal;
            $this->nb_heure = $this->nb_heure_estime;
            */
        }
    }
    
    
    public function delete()
    {
        global $wpof;
        
        // supression du client du tableau clients de la session
        $session = get_session_by_id($this->session_formation_id);
        
        foreach($this->stagiaires as $user_id)
        {
            $stagiaire = get_stagiaire_by_id($this->session_formation_id, $user_id);
            $stagiaire->delete();
        }
        
        $session->supprime_sous_entite("clients", $this->id);
        
        // suppression du client en mémoire
        if (isset($_SESSION['Client'][$this->id]))
            unset($_SESSION['Client'][$this->id]);
        
        // suppression du client dans la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE client_id = '%d';",
            $this->id
        );
        $wpof->log->log_action(__FUNCTION__, $this);
        
        return $wpdb->query($query);
    }
    
    public function get_delete_bouton($texte_bouton = "", $reload = true, $parent_to_kill = null)
    {
        ob_start();
        ?>
        <div class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-reload="<?php echo $reload; ?>" <?php if ($parent_to_kill !== null) : ?> data-parent="<?php echo $parent_to_kill; ?>"<?php endif;?>>
            <?php the_wpof_fa_del(); ?>
            <?php echo $texte_bouton; ?>
        </div>
        <?php
        return ob_get_clean();
    }

    public function get_delete_icone($reload = true)
    {
        ob_start();
        ?>
        <span class="delete-entity icone" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-reload="<?php echo $reload; ?>">
            <?php the_wpof_fa_del(); ?>
        </span>
        <?php
        return ob_get_clean();
    }

    
    public function last_client_id()
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = "SELECT MAX(`client_id`) FROM $table;";
        return $wpdb->get_var($query);
    }
    
    public function get_the_board()
    {
        global $SessionFormation;
        global $SessionStagiaire;
        global $wpof;
        
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        $session = get_session_by_id($this->session_formation_id);
        $this->init_docs();
        
        ob_start();
        if ($this->entite_client == "morale")
            $nom = (empty($this->nom)) ? __("Sans nom") : $this->nom;
        else
            $nom = (empty($this->stagiaire->nom)) ? __("Sans nom") : $this->stagiaire->prenom." ".$this->stagiaire->nom;
        
        ?>
        <div class="board board-client" data-id="<?php echo $this->id; ?>">
            <h2><?php echo $nom; ?></h2>
            <?php if (in_array($role, $wpof->super_roles)) : ?>
            <div class="edit-data float right bord-alerte">
            <?php echo get_input_jpost($this, "verifie", array('input' => 'checkbox', 'label' => __('Client vérifié'))); ?>
            </div>
            <?php endif; ?>
            <?php if ($this->financement == self::FINANCEMENT_OPAC) : ?>
                <p class="bg-alerte center"><?php _e("Vous êtes sous-traitant⋅e de ce client"); ?></p>
            <?php elseif ($this->entite_client == self::ENTITE_PERS_PHYSIQUE) : ?>
                <p><?php _e("Ce client est une personne physique (un particulier)"); ?></p>
            <?php else : ?>
                <p><?php _e("Ce client est une personne morale"); ?></p>
            <?php endif; ?>
            
            <?php $this->the_client_boutons(); ?>
            <div class="pour-infos"> <?php echo $this->get_pour_infos_box(); ?> </div>

            <div class="infos-client infos-client-<?php echo esc_attr($this->entite_client); ?>">
            <?php
            $infos_client_func = "the_infos_client_".$this->entite_client;
            $this->$infos_client_func(); ?>
            
            <?php if (debug && $role == 'admin' && $this->financement != self::FINANCEMENT_OPAC) : ?>
            <div class="action-resp editable">
                <?php echo get_input_jpost($this, "entite_client", array('select' => '', 'display' => 'inline', 'label' => __("Changez uniquement pour réparer une erreur ! De quel type de personne est ce client ?"), 'postprocess' => 'page-reload')); ?>
            </div>
            <?php endif; ?>
            </div>

            <?php
            $gest_docs = get_gestion_docs($this); 
            if ($gest_docs)
            {
                ?>
                <div class="white-board">
                    <?php echo $wpof->toc->set_title("Documents administratifs pour le client", 2); ?>
                    <?php echo $gest_docs; ?>
                </div>
                <?php
            }
            
            if ($this->financement != self::FINANCEMENT_OPAC) :
                $object_for_creno = $this;
                ?>
                <div class="white-board">
                <?php if ($this->entite_client == self::ENTITE_PERS_MORALE) : ?>
                    <?php echo $wpof->toc->set_title("Créneaux contractualisés", 2); ?>
                    <?php echo get_icone_aide("client_creneaux", __("Créneaux contractualisés")); ?>
                <?php elseif ($this->entite_client == self::ENTITE_PERS_PHYSIQUE) : ?>
                    <?php echo $wpof->toc->set_title("Créneaux de présence", 2); ?>
                    <?php
                        echo get_icone_aide("stagiaire_creneaux", __("Créneaux de présence"));
                        $object_for_creno = $this->stagiaire;
                    ?>
                <?php endif; ?>
                    <?php
                        if (count($session->creneaux) == 0)
                            echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
                        else
                            echo $session->get_html_creneaux(false, $object_for_creno);
                    ?>
                </div>
            <?php endif; ?>
            <?php if ($this->entite_client == self::ENTITE_PERS_PHYSIQUE) : ?>
            <div class="besoins white-board edit-data">
                <?php echo $wpof->toc->set_title("Évaluations et besoins", 2); ?>
                <?php echo $this->stagiaire->get_bilan_eval(); ?>
            </div>
            
            <?php endif; ?>
        </div>
        <?php
        echo $wpof->toc->get_html_toc();
        return ob_get_clean();
    }
    
    public function the_board()
    {
        echo $this->get_the_board();
    }

    /**
     * Affiche les boutons de la fiche client
     */
    public function the_client_boutons()
    {
        global $wpof;
        ?>
        <div class="boutons">
            <div class="token-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-parent=".client-<?php echo $this->id; ?>">
                <?php the_wpof_fa_private(); ?>
                <?php _e("Envoyer accès privé"); ?>
            </div>
            <?php echo get_icone_aide("envoi_token", "Accès privé"); ?>
            
            <div class="dynamic-dialog icone-bouton" data-function="copy_move_client" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>">
            <?php the_wpof_fa('clone'); ?>
            <?php _e("Copier/déplacer ce client"); ?>
            </div>
            
            <?php $plugin = $wpof->plugin_manager->getPlugin();?>
            <?php if ($plugin->haveFactureLink($this)): ?>
                <div class="dynamic-dialog icone-bouton" style="--colorBoutonBackground: var(--nv-secondary-accent);color: var(--colorBoutonText);" data-function="facturer_client" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>">
                    <?php the_wpof_fa('file-invoice-dollar'); ?>
                    <?php _e("Facturer ce client"); ?>
                </div>
            <?php else: ?>
                <?php echo $plugin->getConnectionButton($this->session_formation_id, $this->id); ?>
            <?php endif; ?>

            <?php echo $this->get_delete_bouton(__("Supprimer ce client")); ?>
            
            <?php if ($wpof->role == "admin") : ?>
            <p>ID : <?php echo $this->id; ?></p>
            <p class="icone-bouton dynamic-dialog" data-function="sql_session_formation" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>"><?php the_wpof_fa_add(); ?> <?php _e("SQL client"); ?></p>
            <?php endif; ?>
        </div>
        <?php
    }
    
    // Infos clients personne morale
    public function the_infos_client_morale()
    {
        global $wpof;
        
        $role = wpof_get_role(get_current_user_id());
        $session = get_session_by_id($this->session_formation_id);
        
        ?>
        <div class="white-board edit-data infos-generales">
            <div class="info-generale-block">
            <?php echo $wpof->toc->set_title("Informations générales", 2); ?>

            <?php
                echo get_input_jpost($this, "nom", array('input' => 'text', 'label' => __("Nom"), 'postprocess' => 'update_client_nom', 'grid_col' => '2', 'needed' => true));
                if (champ_additionnel('numero_contrat'))
                    echo get_input_jpost($this, "numero_contrat", array('input' => 'text', 'label' => __("Numéro de contrat"), 'grid_col' => '2'));

                $plugin = $wpof->plugin_manager->getPlugin();
                if (!$plugin->haveFactureLink($this)) 
                    echo get_input_jpost($this, "numero_facture", array('input' => 'text', 'label' => __("Numéro de facture"), 'grid_col' => '2'));
                
                echo get_input_jpost($this, "adresse", array('textarea' => '', 'label' => __("Adresse"), 'grid_col' => '6', 'needed' => true));
                echo get_input_jpost($this, "code_postal", array('input' => 'text', 'label' => __("Code postal"), 'grid_col' => '2', 'needed' => true));
                echo get_input_jpost($this, "ville", array('input' => 'text', 'label' => __("Ville"), 'grid_col' => '2', 'needed' => true));
                echo get_input_jpost($this, "pays", array('input' => 'text', 'label' => __("Pays"), 'grid_col' => '2', 'needed' => true));
                echo get_input_jpost($this, "telephone", array('input' => 'text', 'label' => __("Téléphone"), 'grid_col' => '2'));
                echo get_input_jpost($this, "rcs", array('input' => 'text', 'label' => __("RCS"), 'grid_col' => '2'));
                echo get_input_jpost($this, "siret", array('input' => 'text', 'label' => __("N° Siret"), 'grid_col' => '2'));

                if ($this->financement == self::FINANCEMENT_OPAC)
                {
                    echo get_input_jpost($this, "num_of", array('input' => 'text', 'label' => __("Numéro de déclaration d'activité de formation"), 'grid_col' => '1/3', 'needed' => true));
                    echo get_input_jpost($this, "region", array('select' => '', 'label' => __("Préfecture de région d'attribution du numéro"), 'first' => __("Sélectionnez une région"), 'grid_col' => '2'));
                }
            ?>
            </div>
                
            <div class="resp-block">
            <?php echo $wpof->toc->set_title("Responsable", 2); ?>
            <?php
                echo get_input_jpost($this, "responsable", array('input' => 'text', 'label' => __("Responsable signataire"), 'grid_col' => '3', 'needed' => true));
                echo get_input_jpost($this, "responsable_fonction", array('input' => 'text', 'label' => __("Fonction responsable"), 'grid_col' => '3', 'needed' => true));
                echo get_input_jpost($this, "responsable_email", array('input' => 'text', 'label' => __("E-mail responsable"), 'grid_col' => '3', 'needed' => true));
                echo get_input_jpost($this, "responsable_tel", array('input' => 'text', 'label' => __("Téléphone responsable"), 'grid_col' => '3'));
            ?>
            
            <?php if ($this->financement != self::FINANCEMENT_OPAC && !in_array($this->responsable_stagiaire, $this->stagiaires)) : ?>
                <div class="icone-bouton add_as_stagiaire" data-source="responsable">
                <?php the_wpof_fa_add(); _e("Ajouter comme stagiaire"); ?>
                </div>
            <?php endif; ?>
            </div>
                
            
            <div class="contact-block">
            <?php
            echo get_input_jpost($this, "contact_needed", array('input' => 'checkbox', 'label' => __('Besoin de préciser un autre contact'), 'postprocess' => 'toggle_class', 'ppargs' => array('ppselector' => '.contact_field', 'pptoggle' => 'hidden'), 'grid_col' => '3'));
            $contact_class = ($this->contact_needed) ? "" : "hidden"; ?>
                <div class="contact_field <?php echo $contact_class; ?>">
                    <h2><?php _e("Contact"); ?></h2>
                    <?php
                    echo get_input_jpost($this, "contact", array('input' => 'text', 'label' => __("Contact"), 'class' => 'contact_field '.$contact_class, 'grid_col' => '2'));
                    echo get_input_jpost($this, "contact_email", array('input' => 'text', 'label' => __("E-mail contact"), 'class' => 'contact_field '.$contact_class, 'grid_col' => '2'));
                    echo get_input_jpost($this, "contact_tel", array('input' => 'text', 'label' => __("Téléphone contact"), 'class' => 'contact_field '.$contact_class, 'grid_col' => '2'));
                    
                    if ($this->financement != self::FINANCEMENT_OPAC && !in_array($this->contact_stagiaire, $this->stagiaires)) : ?>
                        <div class="icone-bouton add_as_stagiaire contact_field <?php echo $contact_class; ?>" data-source="contact">
                        <?php the_wpof_fa_add(); _e("Ajouter comme stagiaire"); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            
            <div class="financement-block">
            <?php echo $wpof->toc->set_title("Financement", 2); ?>
            <?php
                $financement_args = array('select' => '', 'label' => __("Principale source de financement"), 'postprocess' => 'toggle_opco', 'class' => esc_attr($this->financement), 'grid_col' => '3', 'needed' => true);
                if ($this->financement == self::FINANCEMENT_OPAC)
                    $financement_args["readonly"] = 1;
                echo get_input_jpost($this, "financement", $financement_args);                
                
                $opco_jpost_args = array('select' => '', 'label' => __("Opérateur de compétences"), 'first' => 'Autre', 'class' => 'toggle-opco', 'grid_col' => '1 / 4');
                $no_dossier_opco_jpost_args = array('input' => 'text', 'label' => __("Numéro de dossier chez l'OPCO"), 'class' => 'toggle-opco', 'grid_col' => '3');
                if (!in_array($this->financement, array("mutu8")))
                {
                    $opco_jpost_args['display'] = 'none';
                    $no_dossier_opco_jpost_args['display'] = 'none';
                }
                echo get_input_jpost($this, "opco", $opco_jpost_args);
                echo get_input_jpost($this, "no_dossier_opco", $no_dossier_opco_jpost_args);
                
                global $Documents;
                $tarif_editable = (in_array($wpof->role, $wpof->super_roles)
                                    || ($this->contrat_uid !== false 
                                        && ($Documents[$this->contrat_uid]->valid & Document::DRAFT || $Documents[$this->contrat_uid]->last_modif == false)));
                if (!$tarif_editable) : ?>
                <p class="bg-alerte center" style="grid-column: 1/7"><?php _e('Attention, un contrat existe déjà ! Ne modifiez ces valeurs que pour corriger une erreur ou incohérence'); ?></p>
                <?php
                endif;
                $check_tarif = check_client_tarif($this);
                $taxe = ($wpof->of_hastva) ? __("HT") : __("TTC");
                echo get_input_jpost($this, "tarif_total_chiffre", array('input' => 'number', 'step' => '0.01', 'label' => __("Tarif total en ").$wpof->monnaie." ".$taxe, 'postprocess' => 'update_pour_infos_client+update_client_tarif', 'grid_col' => '1/3', 'needed' => true/*, 'readonly' => !$tarif_editable*/));

                echo get_input_jpost($this, "acompte", array(
                    'input'       => 'number',
                    'step'        => '0.01',
                    'label'       => __("Acompte en ") . $wpof->monnaie . " " . $taxe,
                    'grid_col'    => '2',
                    'postprocess' => 'update_pour_infos_client',
//                    'readonly'    => !$tarif_editable,
                    'after'       => ($tarif_editable) ? '<span title="' . sprintf(__("Appliquer un acompte de %d %%"), $wpof->acompte_pourcent) . '" class="icone-bouton set-acompte" data-acompte="' . $wpof->acompte_pourcent . '">' . $wpof->acompte_pourcent . ' %</span>' : ''));

                echo get_input_jpost($this, "tarif_total_autres_chiffre", array(
                    'input'       => 'number',
                    'step'        => '0.01',
                    'label'       => __("Dont autres frais non pédagogiques"),
                    'postprocess' => 'update_pour_infos_client+update_client_tarif',
//                    'readonly'    => !$tarif_editable,
                    'class'       => in_array('tarif_animation', $check_tarif['alerte']) ? "bg-alerte" : "",
                    'grid_col'    => '1/3'));
                ?>
                <?php if (champ_additionnel('detail_tarif_peda')) : ?>
                <?php $detail_tarif_peda = ($this->tarif_animation + $this->tarif_preparation) != 0; ?>
                <div class="grid-4 switch-detail-tarif-peda" style="<?php if ($detail_tarif_peda) : ?>display: none;<?php endif; ?>"><span class="bouton"><?php _e("Détailler les frais pédagogiques (préparation, animation)"); ?></span></div>
                <?php
                echo get_input_jpost($this, "tarif_animation", array(
                    'input'       => 'number',
                    'step'        => '0.01',
                    'label'       => __("Dont animation pédagogique"),
                    'postprocess' => 'update_pour_infos_client+update_client_tarif',
                    'class'       => "detail-peda".(in_array('tarif_animation', $check_tarif['alerte']) ? " bg-alerte" : ""),
                    'display'     => $detail_tarif_peda ? "block" : "none",
                    'grid_col'    => '2'));
                
                echo get_input_jpost($this, "tarif_preparation", array(
                    'input'       => 'number',
                    'step'        => '0.01',
                    'label'       => __("Dont préparation pédagogique"),
                    'postprocess' => 'update_pour_infos_client+update_client_tarif',
                    'class'       => "detail-peda".(in_array('tarif_preparation', $check_tarif['alerte']) ? " bg-alerte" : ""),
                    'display'     => $detail_tarif_peda ? "block" : "none",
                    'grid_col'    => '2'));
                ?>
                <div class="grid-full detail-peda info-message" style="<?php if (!$detail_tarif_peda) : ?>display: none;<?php endif; ?>">
                    <span class="icone-info <?php if (!empty($check_tarif['alerte'])) : ?>bg-alerte<?php endif; ?>"><?php the_wpof_fa('info'); ?></span>
                    <?php _e("La somme des tarifs d'animation, de préparation et des frais non pédagogiques doit être égale au tarif total. Si tarif d'animation et de préparation sont nuls, alors ces informations sont ignorées.") ?></div>
                <?php endif; // Fin de condition du détail de tarif pédagogique : (champ_additionnel('detail_tarif_peda'))

                echo get_input_jpost($this, "autres_frais", array('input' => 'text', 'label' => __("Nature des autres frais non pédagogiques"), 'grid_col' => '4'));
                
                if ($this->financement == self::FINANCEMENT_OPAC)
                    echo $wpof->toc->set_title("Bilan pédagogique", 2);
                
                echo get_input_jpost($this, "nb_heure_estime_decimal", array('input' => 'number', 'step' => 0.25, 'min' => 0, 'label' => __("Nombre d'heures estimé"), 'postprocess' => 'update_pour_infos_client', 'grid_col' => '2', 'needed' => true));
                
                if ($this->financement == self::FINANCEMENT_OPAC)
                {
                    echo get_input_jpost($this, "nb_stagiaires", array('input' => 'number', 'step' => 1, 'min' => 0, 'label' => __('Nombre de stagiaires'), 'grid_col' => '2', 'needed' => true));
                    echo get_input_jpost($this, "nb_heures_stagiaires", array('input' => 'number', 'step' => 1, 'min' => 0, 'label' => __("Cumul d'heures par stagiaire"), 'grid_col' => '2', 'after' => '<span class="icone-bouton calcul-cumul" id="calcul-cumul">h × s</span>', 'needed' => true));
                }
                
                ?>
            </div>
        </div><!-- .white-board -->
        <?php if ($this->financement != self::FINANCEMENT_OPAC) : ?>
            <div class="besoins white-board edit-data">
            <?php echo $wpof->toc->set_title("Analyse des besoins", 2); ?>
                <?php echo $this->get_attentes(); ?>
            </div>
            <?php endif; ?>
        <?php
        $this->the_client_stagiaires();
    }
    
    public function the_client_stagiaires() {
        global $SessionFormation, $wpof;
        $session = get_session_by_id($this->session_formation_id);
        if ($this->financement === self::FINANCEMENT_OPAC) return;
        ?>
        <div class="white-board">
            <?php echo $wpof->toc->set_title("Stagiaires", 2); ?>
            <div class="icones">
            <div class="icone-bouton dynamic-dialog" data-function="new_stagiaire" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>"><?php the_wpof_fa_add(); ?> <?php _e("Ajouter stagiaire"); ?></div>
            <?php echo get_icone_aide("inscrire_stagiaires", __("Stagiaires")); ?>
            </div>

            <table>
                <thead>
                    <tr>
                        <th><?php echo __('Stagiaire'); ?></th>
                        <?php if (champ_additionnel('genre_stagiaire')) : ?>
                        <th><?php echo __('Genre'); ?></th>
                        <?php endif; ?>
                        <th><?php echo __('Adresse mail'); ?></th>
                        <th><?php echo __('Statut'); ?></th>
                        <th><?php echo __('Fonction'); ?></th>
                        <th><?php echo __('Confirmé'); ?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($this->stagiaires as $sid) {
                        $s = get_stagiaire_by_id($this->session_formation_id, (int)$sid);
                        $name = $s->get_displayname();
                        ?>
                        <tr class="editable">
                            <td><a href="<?php echo $s->permalien; ?>"><?php the_wpof_fa_edit(); ?> <?php echo esc_html($name); ?></a></td>
                            <?php if (champ_additionnel('genre_stagiaire')) : ?>
                            <td><?php echo get_input_jpost($s, "genre", array('select' => '', 'aide' => false)); ?></td>
                            <?php endif; ?>
                            <td><?php echo get_input_jpost($s, "email", array('input' => 'text', 'aide' => false)); ?></td>
                            <td><?php echo get_input_jpost($s, "statut_stagiaire", array('select' => '', 'aide' => false)); ?></td>
                            <td><?php echo get_input_jpost($s, "fonction", array('input' => 'text', 'aide' => false)); ?></td>
                            <td class="edit-data"><?php echo get_input_jpost($s, "confirme", array('input' => 'checkbox', 'aide' => false)); ?></td>
                            <td><?php echo $s->get_delete_icone(); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    }

    // infos client persone physique
    public function the_infos_client_physique()
    {
        global $wpof;
        
        $session = get_session_by_id($this->session_formation_id);
        $role = wpof_get_role(get_current_user_id());
        ?>

        <div class="white-board edit-data infos-generales">
            <div class="info-generale-block">
                <?php echo $wpof->toc->set_title("Informations générales", 2); ?>
                <?php
                    echo get_input_jpost($this->stagiaire, "prenom", array('input' => 'text', 'label' => __("Prénom"), 'postprocess' => 'update_client_nom', 'grid_col' => '3', 'needed' => true));
                    echo get_input_jpost($this->stagiaire, "nom", array('input' => 'text', 'label' => __("Nom"), 'postprocess' => 'update_client_nom', 'grid_col' => '3', 'needed' => true));

                    echo get_input_jpost($this, "adresse", array('textarea' => '', 'label' => __("Adresse"), 'grid_col' => '6', 'needed' => true));
                    echo get_input_jpost($this, "code_postal", array('input' => 'text', 'label' => __("Code postal"), 'grid_col' => '2', 'needed' => true));
                    echo get_input_jpost($this, "ville", array('input' => 'text', 'label' => __("Ville"), 'grid_col' => '2', 'needed' => true));
                    echo get_input_jpost($this, "pays", array('input' => 'text', 'label' => __("Pays"), 'grid_col' => '2', 'needed' => true));

                    echo get_input_jpost($this->stagiaire, "email", array('input' => 'text', 'label' => __("Adresse email"), 'grid_col' => '3', 'needed' => true));
                    echo get_input_jpost($this, "telephone", array('input' => 'text', 'label' => __("Téléphone"), 'grid_col' => '3'));
                    if (champ_additionnel('numero_contrat'))
                        echo get_input_jpost($this, "numero_contrat", array('input' => 'text', 'label' => __("Numéro de contrat"), 'grid_col' => '3'));
                    $plugin = $wpof->plugin_manager->getPlugin();
                    if (!$plugin->haveFactureLink($this)) 
                        echo get_input_jpost($this, "numero_facture", array('input' => 'text', 'label' => __("Numéro de facture"), 'grid_col' => '3'));
                ?>
            </div>
            <div class="financement-block">
                <?php echo $wpof->toc->set_title("Financement", 2); ?>

                <?php
                $financement_args = array('select' => '', 'label' => __("Principale source de financement"), 'postprocess' => 'toggle_opco', 'grid_col' => '3', 'needed' => true);
                echo get_input_jpost($this, "financement", $financement_args);
                
                global $Documents;
                $tarif_editable = (in_array($wpof->role, $wpof->super_roles)
                                    || ($this->contrat_uid !== false 
                                        && ($Documents[$this->contrat_uid]->valid & Document::DRAFT || $Documents[$this->contrat_uid]->last_modif == false)));
                if (!$tarif_editable) : ?>
                <p class="bg-alerte center" style="grid-column: 1/7"><?php _e('Attention, un contrat existe déjà ! Ne modifiez ces valeurs que pour corriger une erreur ou incohérence'); ?></p>
                <?php endif;
                $check_tarif = check_client_tarif($this);
                $taxe = ($wpof->of_hastva) ? __("HT") : __("TTC");
                echo get_input_jpost($this, "tarif_total_chiffre", array(
                    'input'       => 'number',
                    'step'        => '0.01',
                    'label'       => __("Tarif total en ").$wpof->monnaie." ".$taxe,
                    'postprocess' => 'update_pour_infos_client+update_client_tarif',
//                    'readonly'    => !$tarif_editable,
                    'grid_col'    => '1/3',
                    'needed' => true));
                echo get_input_jpost($this, "acompte", array(
                    'input'       => 'number', 'step' => '0.01',
                    'postprocess' => 'update_pour_infos_client',
                    'max'         => $this->tarif_total_chiffre * $wpof->acompte_pourcent_max_particulier,
                    'label'       => __("Acompte (maxi 30 %)").' '.$wpof->monnaie." ".$taxe, 'grid_col' => '2',
//                    'readonly'    => !$tarif_editable,
                    'after'       => ($tarif_editable) ? '<span title="'.sprintf(__("Appliquer un acompte de %d %%"), $wpof->acompte_pourcent).'" class="icone-bouton set-acompte" data-acompte="'.$wpof->acompte_pourcent.'">'.$wpof->acompte_pourcent.' %</span>' : ''));
                
                ?>
                <?php                
                echo get_input_jpost($this, "tarif_total_autres_chiffre", array(
                    'input'       => 'number',
                    'step'        => '0.01',
                    'label'       => __("Dont autres frais non pédagogiques"),
                    'postprocess' => 'update_pour_infos_client+update_client_tarif',
//                    'readonly'    => !$tarif_editable,
                    'class'       => in_array('tarif_animation', $check_tarif['alerte']) ? "bg-alerte" : "",
                    'grid_col'    => '1/3'));

                ?>
                <?php if (champ_additionnel('detail_tarif_peda')) : ?>
                <?php $detail_tarif_peda = ($this->tarif_animation + $this->tarif_preparation) != 0; ?>
                <div class="grid-4 switch-detail-tarif-peda" style="<?php if ($detail_tarif_peda) : ?>display: none;<?php endif; ?>"><span class="bouton"><?php _e("Détailler les frais pédagogiques (préparation, animation)"); ?></span></div>
                <?php
                echo get_input_jpost($this, "tarif_animation", array(
                    'input'       => 'number',
                    'step'        => '0.01',
                    'label'       => __("Dont animation pédagogique"),
                    'postprocess' => 'update_pour_infos_client+update_client_tarif',
                    'class'       => "detail-peda".(in_array('tarif_animation', $check_tarif['alerte']) ? " bg-alerte" : ""),
                    'display'     => $detail_tarif_peda ? "block" : "none",
                    'grid_col'    => '2'));
                
                echo get_input_jpost($this, "tarif_preparation", array(
                    'input'       => 'number',
                    'step'        => '0.01',
                    'label'       => __("Dont préparation pédagogique"),
                    'postprocess' => 'update_pour_infos_client+update_client_tarif',
                    'class'       => "detail-peda".(in_array('tarif_preparation', $check_tarif['alerte']) ? " bg-alerte" : ""),
                    'display'     => $detail_tarif_peda ? "block" : "none",
                    'grid_col'    => '2'));
                ?>
                <div class="grid-full detail-peda info-message" style="<?php if (!$detail_tarif_peda) : ?>display: none;<?php endif; ?>">
                    <span class="icone-info <?php if (!empty($check_tarif['alerte'])) : ?>bg-alerte<?php endif; ?>"><?php the_wpof_fa('info'); ?></span>
                    <?php _e("La somme des tarifs d'animation, de préparation et des frais non pédagogiques doit être égale au tarif total. Si tarif d'animation et de préparation sont nuls, alors ces informations sont ignorées.") ?></div>
                <?php endif; // Fin de condition du détail de tarif pédagogique : (champ_additionnel('detail_tarif_peda'))

                echo get_input_jpost($this, "autres_frais", array('input' => 'text', 'label' => __("Nature des autres frais non pédagogiques"), 'grid_col' => '3'));
                echo get_input_jpost($this, "echeancier", array('label' => __("Échéancier de paiement après l'acompte"), 'textarea' => '', 'grid_col' => '3', 'needed' => true));
                
                echo $wpof->toc->set_title("Bilan pédagogique", 2);
                $nb_heure_args =  array('input' => 'number', 'step' => 0.25, 'min' => 0, 'label' => __("Nombre d'heures estimé"), 'grid_col' => '2', 'needed' => true);
                if ($session->nature_formation == "bilan")
                    $nb_heure_args['max'] = $wpof->max_duree_bc;
                echo get_input_jpost($this->stagiaire, "nb_heure_estime_decimal", $nb_heure_args);
                echo get_input_jpost($this->stagiaire, "statut_stagiaire", array('select' => '', 'label' => __("Statut du stagiaire"), 'grid_col' => '2', 'needed' => true));
                echo get_input_jpost($this->stagiaire, "confirme", array('input' => 'checkbox', 'label' => __("Inscription confirmée"), 'grid_col' => '2'));
                ?>
            </div>
        </div>

        <div class="besoins white-board edit-data">
        <?php echo $wpof->toc->set_title("Analyse des besoins", 3); ?>
            <?php echo $this->get_attentes(); ?>
        </div>
        
        <?php
        
    }
    
    public function get_pour_infos_box()
    {
        global $wpof;
        $session = get_session_by_id($this->session_formation_id);
        ob_start();
        ?>
            <table class="infos-content opaga">
            <tr>
            <td>
            <?php echo $wpof->toc->set_title("Pour informations", 3); ?>
            <p><?php _e("Tarif horaire"); ?> : <span class="tarif_heure"><?php echo get_tarif_formation($this->tarif_heure); ?></span> </p>
            <p><?php _e("Nombre d'heures"); ?> : <span class="nb_heure"><?php echo $this->nb_heure_estime; ?></span> </p>
            <?php if ($this->acompte > 0) : ?>
            <p><?php _e("Montant de l'acompte"); ?> : <span class="acompte"><?php echo get_tarif_formation($this->acompte); ?></span> </p>
            <?php endif; ?>
            <?php $plugin = $wpof->plugin_manager->getPlugin();?>
            <?php if ($plugin->haveFactureLink($this)) : ?>
                <?php $url_factures = $plugin->getFacturesClientUrl($this, $session);
                $factures = $plugin->getFacturesClient($this, $session);
                if ($factures['montant_total_ht'] > 0): ?>
                    <p>
                        <?php _e("Montant déjà facturé (acompte et/ou facture(s) intermédiaire(s)"); ?> : 
                        <?php echo numf($factures['montant_total_ht'], 2) . " " . $factures['monnaie'] . " HT (" . numf($factures['montant_total_ttc'], 2) . " " . $factures['monnaie'] . " TTC)"; ?>
                        <?php if ($url_factures): ?><a href="<?php echo $url_factures; ?>" target="_blank" style="margin-left:0.5em;">Voir en détail <?php the_wpof_fa('up-right-from-square'); ?></a><?php endif; ?>
                    </p>
                <?php endif; ?>
            <?php endif; ?>
            <p><?php _e("Tarif total"); ?> : <span class="tarif_total"><?php echo get_tarif_formation($this->tarif_total_chiffre); ?></span> </p>
            <p><?php _e("Tarif total (lettres)"); ?> : <span class="tarif_total_lettre"><?php echo $this->tarif_total_lettre; ?></span> </p>
            <?php if ($this->tarif_total_autres_chiffre > 0) : ?>
            <p><?php _e("Dont autres frais"); ?> : <span class="tarif_total_autres_lettre"><?php echo $this->tarif_total_autres_lettre; ?></span> </p>
            <?php endif; ?>
            </td>
            <td>
            <?php $missing_legal = $this->get_missing_legal();
            if (!empty($missing_legal)) : ?>
            <h3><?php the_wpof_fa('triangle-exclamation', 'fa-beat alerte'); ?> <?php _e("Alertes"); ?></h3>
            <p> <?php echo implode('<br />', $missing_legal); ?></p>
            <?php endif; ?>
            </td>
            </tr>
            </table>
        <?php
        return ob_get_clean();
    }
    
    /**
     * Retourne le bloc de saisie des attentes et analyse des besoins pour le client
     */
    public function get_attentes()
    {
        global $wpof;
        
        ob_start(); ?>
        
        <div class="attentes">
        <?php echo get_input_jpost($this, "attentes", array('textarea' => 1, 'rows' => '10', 'cols' => '60', 'label' => __("Attentes, besoins, motivations"), 'postprocess' => 'update_data', 'ppargs' => array('ppkey' => 'date_modif_attentes', 'ppvalue' => date("d/m/Y", time()), 'ppdest' => "#date_modif_attentes{$this->id}", 'grid_col' => '6'))); ?>
        <?php
        if ($wpof->role != null)
            echo get_input_jpost($this, "date_entretien", array("input" => "datepicker", "label" => "Date de l'entretien", "inline" => 1, 'grid_col' => '3'));
        ?>
        <div class="input_jpost derniere-modif" style="grid-column-end: span 3;"><label><?php _e("Date de dernière modification des attentes"); ?></label>
        <p id="date_modif_attentes<?php echo $this->id; ?>"><?php echo $this->date_modif_attentes; ?></p>
        </div>
        </div> <!-- attentes -->
        <?php
        if ($wpof->role != null)
            echo get_input_jpost($this, "analyse_besoin", array('editor' => 1, 'rows' => '10', 'cols' => '60', 'label' => __("Synthèse de l'analyse (valeur contractuelle)"), 'needed' => true, 'grid_col' => '6'));
        ?>
        <?php
        $attentes_stagiaires = array();
        foreach($this->stagiaires as $sid)
        {
            $stagiaire = get_stagiaire_by_id($this->session_formation_id, $sid);
            if ($stagiaire !== null && !empty($stagiaire->attentes))
                $attentes_stagiaires[$stagiaire->get_displayname()] = $stagiaire->attentes;
        }
        if (!empty($attentes_stagiaires)) : ?>
            <p><?php _e("Attentes spécifiques de stagiaires"); ?> <span class="openButton" data-id="attentes-stagiaires">Voir</span></p>
            <div id="attentes-stagiaires" class="blocHidden">
            <?php foreach ($attentes_stagiaires as $name => $attentes) : ?>
            <h3><?php echo $name; ?></h3>
            <?php echo wpautop($attentes); ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
        
        <?php 
        return ob_get_clean();
    }
    
    /*
     * Interface privée avec le client
     */
    public function get_the_interface($token)
    {
        global $wpof;
        $session = get_session_by_id($this->session_formation_id);
        $tabs = array
        (
            "presentation" => __("La formation"),
            "planning" => __("Planning"),
            "documents" => __("Documents"),
            "stagiaires" => __("Stagiaires")
        );
        
        $current_slug = (empty($wpof->uri_params[0])) ? array_key_first($tabs) : $wpof->uri_params[0];
        if (!in_array($current_slug, array_keys($tabs)))
            return "";
        ob_start();
        
        ?>
        <p><strong><?php echo $this->nom; ?></strong> <?php if (!empty($this->contact)) echo __("représenté par")." ".$this->contact; ?></p>
        <h2><?php echo $session->titre_formation; ?></h2>
        <p><strong><?php _e("Dates"); ?></strong> <?php echo $session->dates_texte; ?></p>
        
        <div class="opaga_tabs">
            <ul>
            <?php foreach($tabs as $slug => $name) : ?>
                <li class="<?php echo ($slug === $current_slug) ? "ui-state-active" : ""; ?>"><a href="<?php echo $token->get_url($slug); ?>"><?php echo $name; ?></a></li>
            <?php endforeach; ?>
            </ul>
        </div>
        
        <div id="tab-<?php echo $current_slug; ?>" class="stagiaire white-board" data-id="<?php echo $this->id; ?>">
        <?php
        $get_private_func = "get_private_".$current_slug;
        echo $this->$get_private_func();
        ?>
        </div> <!-- <?php echo $current_slug; ?> -->
        <?php
        return ob_get_clean();
    }
    
    private function get_private_presentation()
    {
        $session = get_session_by_id($this->session_formation_id);
        ob_start();
        echo $session->get_html_presentation(array('entite' => $this));
        return ob_get_clean();
    }
            
    private function get_private_planning()
    {
        $session = get_session_by_id($this->session_formation_id);
        ob_start();
        echo print_creneaux($this->creneaux_object);
        return ob_get_clean();
    }
            
    private function get_private_documents()
    {
        global $Documents;
        $this->init_docs();
        $liste_doc = "";
        foreach ($this->doc_uid as $docuid)
        {
            $doc = $Documents[$docuid];
            if ($doc->visible)
                $liste_doc .= '<li class="doc '.$doc->type.'">'.$doc->link_name.'</li>';
        }
        if ($liste_doc != "") : ?>
        <ul class="liste-doc-stagiaire">
        <?php echo $liste_doc; ?>
        </ul>
        <?php else : ?>
        <p><?php _e("Pas de document pour l'instant. Vous serez averti lorsqu'ils seront prêts."); ?></p>
        <?php endif; ?>
        <?php
    }
    
    private function get_private_stagiaires()
    {
        ob_start();
        return ob_get_clean();
    }
    
    /*
     * retourne le nom du client
     * pour compatibilité avec les classes Stagiaire et Formateur
     */
    public function get_displayname($link = false, $anchor = "")
    {
        if ($link)
        {
            if (!empty($anchor) && substr($anchor, 0, 1) != '#')
                $anchor = '#'.$anchor;
            return '<a href="'.$this->permalien.$anchor.'">'.$this->nom.'</a>';
        }
        else
            return $this->nom;
    }
    
    /*
     * Retourne une balise a avec le permalien et un texte au choix.
     * En option, une ancre
     */ 
    public function get_a_link($text, $anchor = "")
    {
        if ($text != 0 && $text == "")
            $text = __("Définissez un texte pertinent pour ce lien");
        if (!empty($anchor) && substr($anchor, 0, 1) != '#')
            $anchor = '#'.$anchor;
        return '<a href="'.$this->permalien.$anchor.'">'.$text.'</a>';
    }
    
    public function get_contacts($param = '', $delim = null)
    {
        $result = "";
        
        switch($param)
        {
            case 'email':
                $result = array($this->contact_email, $this->responsable_email);
                break;
            default:
                $result = array($this->contact, $this->responsable);
                break;
        }
        if ($delim)
            $result = implode(esc_html($delim), array_filter($result, function($val) { return $val !== ''; }));
        return $result;
    }
    
    public static function get_client_control_head()
    {
        global $wpof;
        ob_start(); ?>
        <tr>
        <?php if (in_array($wpof->role, $wpof->super_roles)) : ?>
        <th><?php _e("ID"); ?></th>
        <?php endif; ?>
        <?php if (champ_additionnel("numero_contrat")) : ?>
        <th><?php _e("N° contrat"); ?></th>
        <?php endif; ?>
        <th><?php _e("N° facture"); ?></th>
        <th><?php _e("Nom ou raison sociale"); ?></th>
        <th><?php _e("Type"); ?></th>
        <th><?php _e("Session"); ?></th>
        <?php if (in_array($wpof->role, $wpof->super_roles)) : ?>
        <th><?php _e("Formateur⋅trice"); ?></th>
        <?php endif; ?>
        <th><?php _e("Nature"); echo get_icone_aide("formation_nature_formation"); ?></th>
        <th id="order" data-order="desc"><?php _e("Première date"); ?></th>
        <th><?php _e("Dernière date"); ?></th>
        <th><?php _e("Durée en h"); ?></th>
        <th><?php _e("Stagiaires"); ?></th>
        <th><?php _e("Financement"); ?></th>
        <th><?php _e("Tarif € HT"); ?></th>
        <?php if (in_array($wpof->role, $wpof->super_roles)) : ?>
        <th><?php _e("Vérifié"); ?></th>
        <th><?php _e("Alerte"); ?></th>
        <?php endif; ?>
        <th class="thin not_orderable"><?php _e("Supprimer"); ?></th>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    public function get_client_control()
    {
        global $wpof, $client_control;
        
        if ($this->session_formation_id > 0)
            $session = get_session_by_id($this->session_formation_id);
        else
            $session = null;
        
        $alerte = false;
        if (!is_array($client_control))
            $client_control = array();
        $id_control = sanitize_title($this->numero_facture.reset($this->dates_array).$session->titre_formation);
        if (isset($client_control[$id_control]) && !$this->verifie)
        {
            $doublon = true;
            $alerte = true;
        }
        else
        {
            $doublon = false;
            $client_control[$id_control] = "c".$this->id;
        }
        $type_client = ($this->financement == self::FINANCEMENT_OPAC) ? __("Sous-traitance") : $wpof->entite_client->get_term($this->entite_client);
        $entite_object = ($this->entite_client == self::ENTITE_PERS_MORALE) ? $this : $this->stagiaire;
        
        ob_start(); ?>
        <tr id="c<?php echo $this->id; ?>" class="editable <?php echo sanitize_title($type_client); ?>">
        
        <?php if (in_array($wpof->role, $wpof->super_roles)) : ?>
        <td data-value="<?php echo $this->id; ?>"><?php echo $this->id; ?></td>
        <?php endif; ?>
        
        <?php // $empty_contrat = (champ_additionnel("numero_contrat") && empty($this->numero_contrat)); ?>
        <?php if (champ_additionnel("numero_contrat")) : ?>
        <td data-value="<?php echo $this->numero_contrat; ?>"><?php echo get_input_jpost($this, "numero_contrat", array('input' => 'text', 'aide' => false)); //if ($empty_contrat) echo '<br />'.__("Pas de numéro !"); ?></td>
        <?php endif; ?>
        
        <td data-value="<?php echo $this->numero_facture; ?>"><?php echo get_input_jpost($this, "numero_facture", array('input' => 'text', 'aide' => false)); ?></td>
        
        <td data-value="<?php echo $this->get_displayname(false); ?>"><?php echo $this->get_displayname(true); ?></td>
        
        <td data-value="<?php echo $type_client; ?>"><?php echo $type_client; ?></td>
        
        <?php
        $session_erreur = "";
        if (!$session)
            $session_erreur = '<strong>'.__("Pas de session").'</strong>';
        elseif (!in_array($this->id, $session->clients))
            $session_erreur = '<strong>'.__("Client détaché").'</strong> <span class="icone-bouton dynamic-dialog" data-function="relink" data-clientid="'.$this->id.'" data-sessionid="'.$this->session_formation_id.'">Rattacher</span>';
        ?>
        <td data-value="<?php if ($session) echo $session->titre_formation; ?>" <?php if ($session_erreur != "") : $alerte = true; ?> class="bg-alerte" <?php endif; ?>
        title="<?php echo __("Créée le ").$session->wp_post->post_date."\n".__("Par ").get_displayname($session->wp_post->post_author); ?>">
        <?php if (!$session) : ?>
        <span class="hidden">_</span><span class="erreur"><?php echo $session_erreur; ?></span>
        <?php else : ?>
        <?php echo '['.$session->get_id_link().'] '.$session->titre_formation.' '.$session_erreur; ?>
        <?php endif; ?>
        </td>
        
        <?php if (in_array($wpof->role, $wpof->super_roles)) : ?>
        <td data-value="<?php if ($session) echo $session->get_formateurs_noms(); ?>"><?php if ($session && !empty($session->formateur)) echo $session->get_formateurs_noms(); else $alerte = true; ?></td>
        <?php endif; ?>
        
        <td><?php echo $wpof->nature_formation->get_term($session->nature_formation); ?></td>
        <td data-value="<?php if ($session) echo date("Y-m-d", $session->first_date_timestamp); ?>" class="date"><span class="hidden"><?php if ($session) echo $session->first_date_timestamp; ?></span><?php if (!empty($this->dates_array)) echo reset($this->dates_array); ?></td>
        
        <td data-value="<?php if ($session) echo date("Y-m-d", $session->last_date_timestamp); ?>" class="date"><?php if (!empty($this->dates_array)) echo end($this->dates_array); ?></td>
        
        <td data-value="<?php echo $entite_object->nb_heure_estime_decimal; ?>" <?php if (empty($entite_object->dates_array) || $entite_object->nb_heure_estime_decimal == 0) : $alerte = true; ?> class="bg-alerte" title="<?php _e("Durée nulle"); ?>"<?php endif; ?>>
        <?php
            if (!empty($entite_object->dates_array))
            {
                echo get_input_jpost($entite_object, "nb_heure_estime_decimal", array('input' => 'number', 'aide' => false));
                echo '<p class="infos_doc">';
                printf(__("%dj %.1fh/j"), count($entite_object->dates_array), $entite_object->nb_heure_estime_decimal / count($entite_object->dates_array));
                echo '</p>';
            }
            else
                _e("Aucune date");
        ?>
        </td>
        
        <td data-value="<?php echo $this->nb_confirmes; ?>" <?php if ($this->nb_confirmes == 0) : $alerte = true; ?> class="bg-alerte" title="<?php _e("Aucun stagiaire"); ?>"<?php endif; ?>>
        <?php
        if ($this->financement == self::FINANCEMENT_OPAC)
        {
            echo get_input_jpost($this, 'nb_stagiaires', array('input' => 'number', 'step' => 1, 'aide' => false));
            echo get_input_jpost($this, 'nb_heures_stagiaires', array('input' => 'number', 'step' => 1, 'aide' => false, 'label' => __('Cumul h×s')));
            ?>
            <span class="icone-bouton calcul-cumul table-bouton" data-parent="tr#c<?php echo $this->id; ?>" title="<?php _e('Calcul automatique du cumul'); ?>">h × s</span>
            <?php
        }
        elseif ($this->entite_client == self::ENTITE_PERS_MORALE)
            echo $this->get_a_link($this->nb_confirmes, 'stagiaires');
        else
            echo $this->get_a_link($this->nb_confirmes);
        ?>
        </td>
        
        <td data-value="<?php echo $wpof->financement->get_term($this->financement); ?>" <?php if (empty($this->financement)) : $alerte = true; ?> class="bg-alerte"<?php endif; ?>>
        <?php echo get_input_jpost($this, "financement", array('select' => '', 'aide' => false)); ?>
        </td>
        
        <td data-value="<?php echo $this->tarif_total_chiffre; ?>" <?php if ($this->tarif_heure <= $wpof->tarif_heure_alerte) : $alerte = true; ?> class="bg-alerte" title="<?php printf(__("Tarif inférieur ou égal à %d €/h"), $wpof->tarif_heure_alerte); ?>"<?php endif; ?>>
        <?php echo get_input_jpost($this, "tarif_total_chiffre", array('input' => 'number', 'step' => '0.01', 'aide' => false)); ?>
        </td>
        
        <?php if (in_array($wpof->role, $wpof->super_roles)) : ?>
        <td data-value="<?php echo ($this->verifie) ? "vérifié" : ""; ?>" class="center edit-data"><?php echo get_input_jpost($this, "verifie", array('input' => 'checkbox', 'aide' => false)); ?></td>
        <td data-value="<?php if ($alerte) echo "alerte"; ?>" class="center">
        <?php if ($alerte) echo "alerte"; if ($doublon) echo '<br /><div data-id="#'.$client_control[$id_control].'" class="icone-bouton bg-alerte highlight-element">'.__("Doublon potentiel").'</div>'; ?>
        </td>
        <?php endif; ?>
        
        <td class="center"> <?php echo $this->get_delete_bouton("", false, "#c".$this->id); ?> </td>
        
        </tr>
        <?php
        return ob_get_clean();
    }
    
    public function get_doc_est_personne_morale($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $personne = 1;
        if ($arg == 'valeur')
            $personne = ($this->entite_client == self::ENTITE_PERS_MORALE) ? 1 : 0;
        $result = array('empty_ok' => false, 'valeur' => $personne, 'tag' => "client:$function_name", 'desc' => __("Le client est-il une personne morale ?"));
        return $result[$arg];
    }
    public function get_doc_est_personne_physique($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $personne = 1;
        if ($arg == 'valeur')
            $personne = ($this->entite_client == self::ENTITE_PERS_PHYSIQUE) ? 1 : 0;
        $result = array('empty_ok' => false, 'valeur' => $personne, 'tag' => "client:$function_name", 'desc' => __("Le client est-il une personne physique ?"));
        return $result[$arg];
    }
    public function get_doc_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Raison sociale du client (ou prénom nom si particulier)"));
        return $result[$arg];
    }
    public function get_doc_adresse($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Adresse postale"));
        return $result[$arg];
    }
    public function get_doc_code_postal($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Code postal"));
        return $result[$arg];
    }
    public function get_doc_ville($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Ville"));
        return $result[$arg];
    }
    public function get_doc_pays($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $pays = (empty($this->pays)) ? "France" : $this->pays; // on n'affiche le pays que s'il est différent de France
        $result = array('empty_ok' => false, 'valeur' => $pays, 'tag' => "client:$function_name", 'desc' => __("Pays (affiché seulement si différent de France)"));
        return $result[$arg];
    }
    public function get_doc_telephone($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de téléphone"));
        return $result[$arg];
    }
    public function get_doc_contact($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nom du contact chez le client"));
        return $result[$arg];
    }
    public function get_doc_contact_email($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Email du contact"));
        return $result[$arg];
    }
    public function get_doc_contact_tel($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de téléphone du contact"));
        return $result[$arg];
    }
    public function get_doc_responsable($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Prénom et nom du/de la responsable qui signe les documents"));
        return $result[$arg];
    }
    public function get_doc_responsable_email($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Email du/de la responsable qui signe les documents"));
        return $result[$arg];
    }
    public function get_doc_responsable_tel($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de téléphone du/de la responsable qui signe les documents"));
        return $result[$arg];
    }
    public function get_doc_responsable_fonction($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Fonction du/de la responsable qui signe les documents"));
        return $result[$arg];
    }
    public function get_doc_rcs($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de RCS"));
        return $result[$arg];
    }
    public function get_doc_siret($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de Siret"));
        return $result[$arg];
    }
    public function get_doc_num_of($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de déclaration d’activité (le client est un organisme de formation)"));
        return $result[$arg];
    }
    public function get_doc_num_of_region($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            global $wpof;
            if ($wpof->region->is_term($this->region))
                $valeur = $wpof->region->get_term($this->region);
        }
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "client:$function_name", 'desc' => __("Région d'attribution du numéro de déclaration d’activité"));
        return $result[$arg];
    }
    public function get_doc_nature_formation($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            if ($wpof->$function_name->is_term($this->$function_name))
                $valeur = $wpof->$function_name->get_term($this->$function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "client:$function_name", 'desc' => __("Objectif de la prestation (cf. BPF)"));
        return $result[$arg];
    }
    public function get_doc_financement($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            if ($wpof->$function_name->is_term($this->$function_name))
                $valeur = $wpof->$function_name->get_term($this->$function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "client:$function_name", 'desc' => __("Mode de financement (cf. BPF)"));
        return $result[$arg];
    }
    public function get_doc_echeancier($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "client:$function_name", 'desc' => __("Échéancier de paiement après l'acompte (pour un particulier)"));
        return $result[$arg];
    }
    public function get_doc_opco($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("OPCO du client"));
        return $result[$arg];
    }
    public function get_doc_no_dossier_opco($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de dossier chez l’OPCO du client"));
        return $result[$arg];
    }
    public function get_doc_numero_contrat($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de contrat interne pour ce client"));
        return $result[$arg];
    }
    public function get_doc_tarif_heure($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->$function_name, $taxe);
        $result = array('empty_ok' => false, 'valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Tarif horaire (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_tarif_stagiaire($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
        {
            if ($this->nb_confirmes > 0)
                $tarif = get_tarif_taxe($this->tarif_total_chiffre / $this->nb_confirmes, $taxe);
            else
                $tarif = __("Non applicable");
        }
        $result = array('empty_ok' => false, 'valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Tarif par stagiaire (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_tarif_total_chiffre($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->$function_name, $taxe);
        $result = array('empty_ok' => false, 'valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Tarif total de la prestation (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_tarif_preparation($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = ($this->$function_name > 0) ? get_tarif_taxe($this->$function_name, $taxe) : "";
        $result = array('empty_ok' => true, 'valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Tarif de préparation de la prestation (HT ou TTC affiché par le mot-clé ou vide si montant nul)"));
        return $result[$arg];
    }
    public function get_doc_tarif_animation($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = ($this->$function_name > 0) ? get_tarif_taxe($this->$function_name, $taxe) : "";
        $result = array('empty_ok' => true, 'valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Tarif d'animation de la prestation (HT ou TTC affiché par le mot-clé ou vide si montant nul)"));
        return $result[$arg];
    }
    public function get_doc_acompte($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->$function_name, $taxe);
        $result = array('empty_ok' => false, 'valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Acompte (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_solde($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe(floatval($this->tarif_total_chiffre) - floatval($this->acompte), $taxe);
        $result = array('empty_ok' => false, 'valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Solde (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_tarif_total_autres_chiffre($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->$function_name, $taxe);
        $result = array('empty_ok' => false, 'valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Montant des frais non pédagogiques (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_autres_frais($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nature des frais non pédagogiques"));
        return $result[$arg];
    }
    public function get_doc_nb_heure($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nombre d’heures"));
        return $result[$arg];
    }
    public function get_doc_nb_heure_estime($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nombre d’heures estimé"));
        return $result[$arg];
    }
    public function get_doc_attentes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => true, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Attentes, besoins"));
        return $result[$arg];
    }
    public function get_doc_analyse_besoin($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Analyse des besoins à valeur contractuelle"));
        return $result[$arg];
    }
    public function get_doc_dates($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->dates_texte, 'tag' => "client:$function_name", 'desc' => __("Dates contractuelles concernées"));
        return $result[$arg];
    }
    public function get_doc_creneaux($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = '';
        if ($arg == 'valeur')
            $valeur = print_creneaux($this->creneaux_object);
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "client:$function_name", 'desc' => __("Créneaux contractualisés avec le client"));
        return $result[$arg];
    }
    public function get_doc_stagiaires($arg = 'valeur', $type_liste = ',')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $liste_stagiaires = '';
        if ($arg == 'valeur')
        {
            $liste_stagiaires = $this->get_liste_stagiaires($type_liste);
            switch(substr($type_liste, 0, 1))
            {
                case 't':
                    $liste_stagiaires = '<table class="list_tab">'.$liste_stagiaires.'</table>';
                    break;
                case 'l':
                    $liste_stagiaires = "<ul>".$liste_stagiaires."</ul>";
                    break;
                case ',':
                default:
                    break;
            }
        }
        
        $result = array('empty_ok' => false, 'valeur' => $liste_stagiaires, 'tag' => "client:$function_name", 'desc' => __("Liste des stagiaires. En paramètre indiquez « liste » pour une liste à puce, « tableau » pour un tableau ou rien pour avoir les noms séparés par des virgules"));
        return $result[$arg];
    }
    public function get_doc_stagiaires_confirmes($arg = 'valeur', $type_liste = ',')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $liste_stagiaires = '';
        if ($arg == 'valeur')
        {
            $liste_stagiaires = $this->get_liste_stagiaires($type_liste, true);
            switch(substr($type_liste, 0, 1))
            {
                case 't':
                    $liste_stagiaires = '<table class="list_tab">'.$liste_stagiaires.'</table>';
                    break;
                case 'l':
                    $liste_stagiaires = "<ul>".$liste_stagiaires."</ul>";
                    break;
                case ',':
                default:
                    break;
            }
        }
        
        $result = array('empty_ok' => false, 'valeur' => $liste_stagiaires, 'tag' => "client:$function_name", 'desc' => __("Liste des stagiaires confirmés. En paramètre indiquez « liste » pour une liste à puce, « tableau » pour un tableau ou rien pour avoir les noms séparés par des virgules"));
        return $result[$arg];
    }
    public function get_doc_nb_confirmes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nombre de stagiaires confirmés"));
        return $result[$arg];
    }
    public function get_doc_emargement_collectif_vierge($arg = 'valeur', $nb_line_nb_jour = "x1", $premier_jour = 0)
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tableau = "";
        if ($arg == 'valeur')
            $tableau = $this->get_doc_emargement_collectif_stagiaires('vierge', $nb_line_nb_jour, $premier_jour);
        $result = array('empty_ok' => false, 'valeur' => $tableau, 'tag' => "client:$function_name", 'desc' => __("Tableau vierge pour des feuilles d'émargement vierges (en paramètre nombre de lignes x nombre de jours maximum par page)"));
        return $result[$arg];
    }
    public function get_doc_emargement_collectif_stagiaires($arg = 'valeur', $nb_line_nb_jour = "x1", $premier_jour = 0, $sp = 0)
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        
        $tableau = "";
        if (in_array($arg, array('valeur', 'vierge')))
        {
            list($nb_lines, $nb_jour_max_par_page) = explode('x', $nb_line_nb_jour);
            $nb_lines = (is_numeric($nb_lines)) ? (integer) $nb_lines : $wpof->nb_ligne_emargement;
            $nb_jour_max_par_page = (is_numeric($nb_jour_max_par_page)) ? (integer) $nb_jour_max_par_page : 1;
            echo "lignes ".$nb_lines." / jours ".$nb_jour_max_par_page;

            $session = get_session_by_id($this->session_formation_id);
            $nb_stagiaires = 0;
            if ($arg != 'vierge')
            {
                global $SessionStagiaire;
                $this->init_stagiaires();
                $nb_stagiaires = count($SessionStagiaire);
            }
            
            $dates = array_keys($session->creneaux);
            $nb_jour_max_par_page = min($nb_jour_max_par_page, count($dates) - $premier_jour);
            $cases_stagiaires = "";
            $cases_formateurs = "";
            
            //return var_export($dates, true);
            ob_start();
            ?>
            <table class="tableau-stagiaires">
            <tr><th rowspan="2" class="stagiaire"><?php _e("Prénom, nom des stagiaires"); ?></th>
            <?php 
            for($i = $premier_jour; $i < $premier_jour + $nb_jour_max_par_page; $i++)
            {
                $colspan = count($session->creneaux[$dates[$i]]); // nombre de créneau dans la journée
                ?>
                <th class="plage" colspan="<?php echo $colspan; ?>"><?php echo $dates[$i]; ?></th>
                <?php
            }
            ?>
            <th rowspan="2" class="col-nb-heures"><?php _e("Nombre d'heures total"); ?></th>
            </tr>
            <tr>
            <?php 
            for($i = $premier_jour; $i < $premier_jour + $nb_jour_max_par_page; $i++)
            {
                foreach($session->creneaux[$dates[$i]] as $creno)
                {
                    echo "<th class=\"plage {$creno->type}\">".$creno->titre."</th>";
                    $cases_stagiaires .= '<td class="'.$creno->type.' contrat'.$this->creneaux[$creno->id].' case-signature"></td>';
                    $cases_formateurs .= '<td class="'.$creno->type.' case-signature"></td>';
                }
            }
            $cases_stagiaires .= "<td></td>"; // ajout de la case pour le nombre d'heures
            $cases_formateurs .= "<td></td>"; // ajout de la case pour le nombre d'heures
            $ligne_vierge = "<tr><td></td>$cases_stagiaires</tr>";
            ?>
            </tr>
            <?php
            if ($arg != 'vierge' && !empty($SessionStagiaire)) :
                global $wpof;
                $max_confirmes = $nb_lines - count($session->formateur);
                $stagiaires = array_chunk($SessionStagiaire, $max_confirmes);
                if (!empty($stagiaires)) :
                    foreach($stagiaires[$sp] as $s) :
                        if ($s->confirme == 1) : ?>
                        <tr>
                        <td><?php echo $s->get_displayname(); if (!empty($s->fonction)) echo " – ".$s->fonction; ?></td>
                        <?php echo $cases_stagiaires; ?>
                        </tr>
                        <?php endif;
                    endforeach;
                endif;
            endif;
            
            $nb_lignes_vierges = $nb_lines- $nb_stagiaires - count($session->formateur);
            
            for ($i = 0; $i < $nb_lignes_vierges; $i++)
                echo $ligne_vierge;
            
            foreach($session->formateur as $fid) : ?>
            <tr class="formateur">
            <td><?php $formateur = get_formateur_by_id($fid); echo $formateur->get_displayname(); ?></td>
            <?php echo $cases_formateurs; ?>
            </tr>
            <?php endforeach; ?>
            
            </table>
            
            <?php
            $tableau .= ob_get_clean();
            $arg = 'valeur';
        }
        else
            $tableau = "";
        
        $result = array('empty_ok' => false, 'valeur' => $tableau, 'tag' => "client:$function_name", 'desc' => __("Tableau des stagiaires du client (en paramètre nombre de lignes x nombre de jours maximum par page)"));
        return $result[$arg];
    }
    public function get_doc_emargement_dates_par_page($arg = 'valeur', $nb_jour_max_par_page = 1, $premier_jour = 0, $num_sp = null)
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        
        $select_dates = array();
        if ($arg == 'valeur')
        {
            $session = get_session_by_id($this->session_formation_id);
            $dates = array_keys($session->creneaux);
            $nb_jour_max_par_page = min($nb_jour_max_par_page, count($dates) - $premier_jour);
            
            for($i = $premier_jour; $i < $premier_jour + $nb_jour_max_par_page; $i++)
                $select_dates[] = $dates[$i];
            
            $select_dates = pretty_print_dates($select_dates);
            if ($num_sp)
                $select_dates .= " (p$num_sp)";
            //$select_dates = join(", ", $select_dates);
        }
            
        $result = array('empty_ok' => false, 'valeur' => $select_dates, 'tag' => "client:$function_name", 'desc' => __("Dates concernées par cette page de feuille d’émargement (correspond aux dates mentionnées dans le tableau)"));
        return $result[$arg];
    }
}

function get_client_meta($client_id, $meta_key)
{
    global $wpdb;
    global $suffix_client;
    $table_client = $wpdb->prefix.$suffix_client;
    
    if ($meta_key)
    {
        $query = $wpdb->prepare
        ("SELECT meta_value
            from $table_client
            WHERE client_id = '%d'
            AND meta_key = '%s';",
            $client_id, $meta_key);
        return $wpdb->get_var($query);
    }
    else
    {
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_client WHERE client_id = '%d';", $client_id);
        return $wpdb->get_results($query, ARRAY_A);
    }
}

add_action('wp_ajax_check_client_tarif', 'check_client_tarif');
function check_client_tarif($client = null)
{
    global $wpdb;
    $reponse = [ 'alerte' => [] ];

    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
        $post = (object) $_POST;
    elseif (get_class($client) == 'Client')
    {
        $post = new stdClass();
        $post->tarif_preparation = $client->tarif_preparation;
        $post->tarif_animation = $client->tarif_animation;
        $post->tarif_total_autres_chiffre = $client->tarif_total_autres_chiffre;
        $post->tarif_total_chiffre = $client->tarif_total_chiffre;
    }
    else
        $post = null;

    if ($post !== null)
    {
        if ($post->tarif_preparation + $post->tarif_animation != 0 && $post->tarif_preparation + $post->tarif_animation + $post->tarif_total_autres_chiffre != $post->tarif_total_chiffre)
        {
            $reponse['alerte'][] = 'tarif_preparation';
            $reponse['alerte'][] = 'tarif_animation';
            if ($post->tarif_total_autres_chiffre > 0)
                $reponse['alerte'][] = 'tarif_total_autres_chiffre';
        }
        if ($post->tarif_total_autres_chiffre > $post->tarif_total_chiffre)
            $reponse['alerte'][] = 'tarif_total_autres_chiffre';
    }
    else
        $reponse['erreur'] = __('Aucune information utilisable');

    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        echo json_encode($reponse);
        die();
    }
    else
        return $reponse;
}

/*
 * Copie ou déplacement d'un client vers une autre session
 * $session_dest_id : id de la session de destination
 * $client_source_id : id du client source
 * $choix_cp_mv : copy ou move
 */
add_action('wp_ajax_copie_ou_deplace_client', 'copie_ou_deplace_client');
function copie_ou_deplace_client($session_dest_id = 0, $client_source_id = 0, $choix_cp_mv = 'copy')
{
    global $wpdb;
    global $suffix_client, $suffix_session_stagiaire;
    $table_client = $wpdb->prefix.$suffix_client;
    $table_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    $reponse = array('log' => '', 'query' => array());
    
    $meta_key_copy = array
    (
        'adresse',
        'code_postal',
        'ville',
        'pays',
        'telephone',
        'nom',
        'entite_client',
        'contact',
        'contact_email',
        'contact_tel',
        'contact_needed',
        'responsable',
        'responsable_email',
        'responsable_tel',
        'responsable_fonction',
        'rcs',
        'siret',
        'num_of',
        'external_id',
    );
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        $client_source_id = $_POST['client_id'];
        $session_source_id = $_POST['session_source_id'];
        $session_dest_id = $_POST['session_dest_id'];
        $choix_cp_mv = (isset($_POST['choix_cp_mv'])) ? $_POST['choix_cp_mv'] : "";
    }
    else {
        $query = $wpdb->prepare("SELECT DISTINCT session_id from $table_client WHERE client_id = '%d';", $client_source_id);
        $session_source_id = $wpdb->get_var($query);
    }
    
    if ($session_source_id * $session_dest_id * $client_source_id == 0)
        $reponse["erreur"] = sprintf(__("Au moins une information n'a pas été définie. Session source : %d / Session destination : %d / Client source %d"), $session_source_id, $session_dest_id, $client_source_id);
    
    $client_dest_id = $client_source_id;
    
    if (empty($reponse['erreur']))
    {
        if ($choix_cp_mv == "copy")
        {
            $client = get_client_by_id($session_source_id, $client_source_id);
            if ($client->financement == Client::FINANCEMENT_OPAC) // si on est sous-traitant du client à copier, la copie conserve cette info
                $meta_key_copy[] = 'financement';
            $client_dest_id = $client->last_client_id() + 1;
            
            $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_client WHERE client_id = '%d' AND meta_key IN ('".join("','", $meta_key_copy)."');", $client_source_id);
            $client_data = $wpdb->get_results($query);
            $reponse['query'][] = $query;
            
            foreach($client_data as $data)
            {
                $query = $wpdb->prepare("INSERT INTO $table_client SET client_id = %d, session_id = %d, meta_key = '%s', meta_value = '%s';",
                    $client_dest_id,
                    $session_dest_id,
                    $data->meta_key,
                    $data->meta_value
                );
                $wpdb->query($query);
                $reponse['query'][] = $query;
            }
            if ($client->entite_client == Client::ENTITE_PERS_PHYSIQUE)
            {
                $meta_key_stagiaire = array('prenom', 'nom', 'email', 'username');
                $stagiaire_source_id = $client->stagiaires[0];
                $stagiaire = get_stagiaire_by_id($session_source_id, $stagiaire_source_id);
                $stagiaire_dest_id = $stagiaire->get_last_id() + 1;
                $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_stagiaire WHERE stagiaire_id = '%d' AND meta_key IN ('".join("','", $meta_key_stagiaire)."');", $stagiaire_source_id);
                $stagiaire_data = $wpdb->get_results($query);
                $reponse['query'][] = $query;
                
                foreach($stagiaire_data as $data)
                {
                    $query = $wpdb->prepare("INSERT INTO $table_stagiaire SET session_id = %d, stagiaire_id = %d, meta_key = '%s', meta_value = '%s';",
                        $session_dest_id, $stagiaire_dest_id, $data->meta_key, $data->meta_value);
                    $wpdb->query($query);
                    $reponse['query'][] = $query;
                }
                
                // client_id
                $query = $wpdb->prepare("INSERT INTO $table_stagiaire SET session_id = %d, stagiaire_id = %d, meta_key = 'client_id', meta_value = '%s';",
                    $session_dest_id, $stagiaire_dest_id, $client_dest_id);
                $wpdb->query($query);
                $reponse['query'][] = $query;
                
                $client = get_client_by_id($session_dest_id, $client_dest_id);
                $client->update_meta("stagiaires", array($stagiaire_dest_id));
                //$stagiaire->update_meta("client_id", $client_dest_id);
            }
            $reponse['log'] .= "ajout client id $client_dest_id";
        }
        elseif ($choix_cp_mv == "move")
        {
            if ($session_source_id != $session_dest_id)
            {
                $client = get_client_by_id($session_source_id, $client_source_id);
                $query = $wpdb->prepare("UPDATE $table_client SET session_id = %d WHERE client_id = %d;", $session_dest_id, $client_source_id);
                $wpdb->query($query);
                $reponse['query'][] = $query;
                foreach($client->stagiaires as $stagiaire_id)
                {
                    $query = $wpdb->prepare("UPDATE $table_stagiaire SET session_id = %d WHERE stagiaire_id = %d;", $session_dest_id, $stagiaire_id);
                    $wpdb->query($query);
                    $reponse['query'][] = $query;
                }
                $session_source = new SessionFormation($session_source_id);
                unset($session_source->clients[array_search($client_source_id, $session_source->clients)]);
                $session_source->update_meta("clients");
            }
            else
                $reponse['erreur'] = __("La session de destination est la même que la source.");
        }
        else
            $reponse['erreur'] = __("Vous devez choisir de copier ou déplacer ce client.");
    }
    
    if (!isset($reponse['erreur']))
    {
        $session_dest = new SessionFormation($session_dest_id);
        $session_dest->clients[] = $client_dest_id;
        $session_dest->update_meta("clients", array_unique($session_dest->clients));
        $reponse['succes'] = sprintf(__("Allez à la session <a href=\"%s\">%s – %s</a><br />ou cliquez sur <strong>Fermer</strong> pour rester sur celle-ci"), $session_dest->permalien, $session_dest->titre_session, $session_dest->dates_texte);
        $reponse['succes'] .= '<input type="hidden" name="deja_valide" value="1" />';
    }
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        echo json_encode($reponse);
        die();
    }
    return $reponse;
    
}

function get_client_by_id($session_id, $client_id)
{
    global $Client, $global_log;
    
    $client_id = (integer) $client_id;
    
    if ($client_id <= 0)
       return null;
    if (!isset($Client[$client_id]))
        $Client[$client_id] = new Client($session_id, $client_id);
    
    return $Client[$client_id];
}
