<?php
/*
 * class-questionnaire.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-question.php");
require_once(wpof_path . "/class/class-qreponse.php");

class Questionnaire
{
    public int $id;
    public String $subject = "";
    public String $title = "";
    public String $level = "";
    public Array $question_id = [];
    public Array $question = [];
    public int $author_id = 0;
    public Array $meta = [];
    public Array $reponses = []; // réponses à ce questionnaire
    public int $nb_repondant = 0; // nombre de personnes ayant répondu à ce questionnaire
    private String $permalink;
    
    public function __construct($id = -1)
    {
        global $wpof;

        foreach($wpof->questionnaire_meta->term as $key => $term)
            if ($term->multi)
                $this->meta[$key] = [];
            else
                $this->meta[$key] = "";
        
        if ($id > -1)
        {
            $this->id = $id;
            $this->init_from_db();
            $this->permalink = home_url().'/'.$wpof->url_gestion.'/questionnaires/manager/?questionnaire_id='.$this->id;
        }
    }

    public function __toString()
    {
        global $wpof;

        $metaA = [];
        foreach($this->meta as $m)
        {
            if (is_array($m))
                $metaA[] = implode('+', $m);
            else
                $metaA[] = $m;
        }
        return '['.$this->id.']'.' '.$this->title.'|'.$wpof->questionnaire_type->get_term($this->subject).'|'.$this->level.'|'
            .implode(', ', $this->get_display_meta('nature_formation')).'|'
            .implode(', ', $this->get_display_meta('modalite_session'));
    }

    public function get_displayname(bool $link, bool $with_details = true): String
    {
        global $wpof;
        $title = $this->title;
        if ($with_details)
            $title = sprintf($this);
        if ($link)
            return '<a href="'.$this->permalink.'">'.$title.'</a>';
        else
            return $title;
    }

    /**
     * Retourne une metadonnée pour affichage
     */
    public function get_display_meta(String $meta): Array
    {
        global $wpof;

        $meta_text = [];
        if ($meta !== null)
        {
            if (isset($this->meta[$meta]) && isset($wpof->questionnaire_meta->term[$meta]))
            {
                foreach($this->meta[$meta] as $term)
                    $meta_text[] = $wpof->$meta->get_term($term);
            }
        }
        return $meta_text;
    }
    
    public function init_from_db()
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_questionnaire;
        
        $query = $wpdb->prepare("SELECT * FROM $table WHERE id = '%d';", $this->id);

        $result = $wpdb->get_row($query, ARRAY_A);

        if (!empty($result))
        {
            foreach($result as $key => $val)
            {
                if (is_array($this->$key))
                {
                    $array_value = json_decode($val);
                    $this->$key = (is_array($array_value)) ? $array_value : [];
                }
                else
                    $this->$key = $val;
            }
        }

        foreach($this->get_meta() as $m)
        {
            if (is_array($this->meta[$m->meta_key]))
                $this->meta[$m->meta_key][] = $m->meta_value;
            else
                $this->meta[$m->meta_key] = $m->meta_value;
        }

        $this->question = $this->get_questions();
    }

    public function get_meta(String $meta_key = null): mixed
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_questionnaire."meta";
        
        if ($meta_key !== null)
        {
            $query = $wpdb->prepare
            ("SELECT meta_value
                FROM ".$table.
                "WHERE qid = '%d'
                AND meta_key = '%s';",
                $this->id, $meta_key);
            return $wpdb->get_var($query);
        }
        else
        {
            $query = $wpdb->prepare("SELECT meta_key, meta_value FROM ".$table." WHERE qid = '%d';", $this->id);
            $meta = $wpdb->get_results($query);
            return $meta;
        }
    }

    /**
     * Requête SQL pour récupérer les question_id liés à ce questionnaire
     * 
     * @return Array Tableau d'id
     */
    public function get_questions_id(): Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_question_relationships;

        $query = $wpdb->prepare("SELECT question_id FROM ".$table." WHERE questionnaire_id = '%d' ORDER BY ordre;", $this->id);
        return $wpdb->get_col($query);
    }

    /**
     * requête SQL pour obtenir les id des questions obligatoires
     * 
     * @return Array Tableau d'id
     */
    public function get_needed_questions_id(): Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_question_relationships;

        $query = $wpdb->prepare("SELECT question_id FROM ".$table." WHERE questionnaire_id = '%d' AND needed = '1' ORDER BY ordre;", $this->id);
        return $wpdb->get_col($query);
    }

    /**
     * Récupérer les objets Question liés à ce questionnaire
     * 
     * @return Array Tableau d'objets Question
     */
    public function get_questions(): Array
    {
        if (empty($this->question_id))
            $this->question_id = $this->get_questions_id();

        $question = [];
        $question_needed_id = $this->get_needed_questions_id();

        foreach($this->question_id as $qid)
        {
            $question[$qid] = new Question($qid);
            $question[$qid]->needed = (in_array($qid, $question_needed_id));
        }

        return $question;
    }

    /**
     * Retourne le dernier id de questionnaire enregistré dans la bas de données
     */
    public function get_last_id(): int
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_questionnaire;
        
        $last_id = $wpdb->get_var("SELECT MAX(`id`) FROM $table;");
        if (!$last_id)
            $last_id = 0;
        
        return $last_id;
    }
    
    
    /**
     * Affichage de la barre d'ajout de questions par type
     */
    public function get_add_question_buttons(): String
    {
        global $wpof;

        ob_start();
        ?>
        <div class="add_question_manager" data-dest="questions<?php echo $this->id; ?>">
        <?php foreach($wpof->questionnaire_type_question->term as $fonction => $term) : ?>
            <span class="bouton question action" data-function="add_question" data-type="<?php echo $fonction; ?>"><?php echo $term->text; ?></span>
        <?php endforeach; ?>
        <div class="bouton submit questionnaire-enregistrer" data-function="save_questionnaire"><?php _e("Enregistrer"); ?></div>
        </div>
        <?php
        return ob_get_clean();
    }

    /**
     * Affichage du formulaire de paramètrage du questionnaire
     */
    public function get_params_form(): String
    {
        global $wpof;

        ob_start();
        ?>
        <div class="flexrow margin main-title"><label><?php _e('Titre'); ?></label> <input name="title" type="text" value="<?php echo $this->title; ?>" /></div>
        <div id="params">
        <div><label class="top"><?php _e("Type de questionnaire"); ?></label><?php echo $wpof->questionnaire_type->get_select_list($this->subject); ?></div>
        <div><label class="top"><?php _e("Nature de formation"); ?></label><?php echo $wpof->nature_formation->get_select_list($this->meta['nature_formation'], $wpof->questionnaire_meta->term['nature_formation']->multi ? "multiple" : ""); ?></div>
        <div><label class="top"><?php _e("Modalité de session"); ?></label><?php echo $wpof->modalite_session->get_select_list($this->meta['modalite_session'], $wpof->questionnaire_meta->term['modalite_session']->multi ? "multiple" : ""); ?></div>
        </div>
        <?php
        return ob_get_clean();
    }
    
    /**
     * Initialisation des réponses pour une session donnée, éventuellement un repondeur_id
     * 
     * Le tableau $this->reponses est classé par question_id, puis par repondeur_id
     */
    public function init_reponses(int $session_id, String $repondeur_id = null)
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_qreponse;
        
        if ($repondeur_id !== null)
        {
            $query = $wpdb->prepare("SELECT * FROM $table WHERE repondeur_id = %s AND session_id = %d AND questionnaire_id = %d;", $repondeur_id, $session_id, $this->id);
            $this->nb_repondant = 1;
        }
        else
        {
            $query = $wpdb->prepare("SELECT COUNT(DISTINCT repondeur_id) FROM $table WHERE session_id = %d AND questionnaire_id = %d;", $session_id, $this->id);
            $this->nb_repondant = $wpdb->get_var($query);
            $query = $wpdb->prepare("SELECT * FROM $table WHERE session_id = %d AND questionnaire_id = %d;", $session_id, $this->id);
        }

        $reponsesA = $wpdb->get_results($query);
        foreach($reponsesA as $repO)
        {
            $reponse = json_decode(stripslashes($repO->reponse));
            if ($reponse !== null)
                $repO->reponse = $reponse;
            
            if (!isset($this->reponses[$repO->question_id]))
                $this->reponses[$repO->question_id] = [];
            $this->reponses[$repO->question_id][$repO->repondeur_id] = new QReponse((array) $repO);
        }
    }

    /**
     * Obtention des identifiants des répondants
     */
    public function get_repondeur_id(int $session_id): Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_qreponse;
        
        $query = $wpdb->prepare("SELECT DISTINCT repondeur_id FROM $table WHERE session_id = %d AND questionnaire_id = %d;", $session_id, $this->id);
        return $wpdb->get_col($query);
    }


    /**
     * Compilation des réponses
     * **Attention**, les réponses doivent être initialisées au préalable via **$this->init_reponses**
     * 
     * * Moyenne pour les échelles de Likert
     * * Total par choix pour les radio et les checkbox
     * * Concaténation pour les textes
     * 
     * @return Array Tableau classé par question_id
     */
    public function get_compilation_reponses(int $session_id): Array
    {
        global $wpof;

        $result = array_flip($this->question_id);
        if (empty($this->reponses))
            return $result;

        foreach($this->question as $qid => $q)
        {
            if (isset($wpof->questionnaire_type_question->term[$q->qtype]->noquestion))
            {
                unset($result[$qid]);
                continue;
            }

            $reponses = [];
            if (isset($this->reponses[$qid]))
            {
                foreach($this->reponses[$qid] as $rep)
                {
                    if (!(is_array($rep->reponse) && empty($rep->reponse)))
                        $reponses[] = $rep->reponse;
                }
            }
            
            switch($q->qtype)
            {
                case 'likert':
                    $result[$qid] = [ 'title' => $q->title, 'reponse' => round(array_sum($reponses) / count($reponses), 2) ];
                    break;
                case 'checkbox':
                case 'radio':
                    $result[$qid] = [ 'title' => $q->title, 'reponse' => [] ];
                    $choice = [];
                    foreach($reponses as $r)
                    {
                        if (!is_array($r))
                            $r = [ $r => $r ];
                        foreach($r as $subr)
                        {
                            if (!isset($choice[$subr]))
                                $choice[$subr] = 0;

                            $choice[$subr]++;
                        }
                        foreach($r as $ch_id => $subr)
                        {
                            $result[$qid]['reponse'][$ch_id] = [ 'title' => stripslashes($q->options->choice->$ch_id), 'reponse' => round(100 * $choice[$subr] / $this->nb_repondant, 2) ];
                        }
                    }
                    unset($choice);
                    break;
                case 'text':
                case 'textarea':
                case 'number':
                    $result[$qid] = [ 'title' => $q->title, 'reponse' => [] ];
                    foreach($reponses as $r)
                    {
                        $result[$qid]['reponse'][] = $r;
                    }
                    break;
                default:
                    break;
            }
        }

        return $result;
    }
    
    /** 
     * Enregistrement du questionnaire
     * Mise à jour de son contenu
     */
    public function update()
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_questionnaire;

        $query = $wpdb->prepare("INSERT INTO ".$table." (id, level, author_id, subject, title)
            VALUES ('%d', '%s', '%d', '%s', '%s')
            ON DUPLICATE KEY UPDATE title = '%s', level = '%s', subject = '%s';",
            $this->id, $this->level, get_current_user_id(), $this->subject, $this->title,
            $this->title, $this->level, $this->subject
        );

        $res['insert_questionnaire'] = $wpdb->query($query);

        foreach($this->meta as $key => $value)
            $res = array_merge($res, $this->update_meta($key));
        
        $table = $wpdb->prefix.$wpof->suffix_question_relationships;
        $query = $wpdb->prepare("DELETE FROM ".$table." WHERE questionnaire_id = '%d';", $this->id);
        $res['delete_relation'] = $wpdb->query($query); // suppression des questions liées
        if (empty($this->question))
            $this->question = $this->get_questions();

        $order = 0;
        foreach($this->question as $q)
        {
            $query = $wpdb->prepare("INSERT INTO ".$table." (questionnaire_id, question_id, ordre, needed) VALUES ('%d', '%d', '%d', '%d'); ", $this->id, $q->id, $order++, $q->needed);
            $res['insert_relation_'.$q->id] = $wpdb->query($query);
        }

        return [ $res, $query ];
    }

    /**
     * Update d'une meta key
     */
    public function update_meta(String $meta_key, String $meta_value = null)
    {
        global $wpdb, $wpof;
        
        $table = $wpdb->prefix.$wpof->suffix_questionnaire."meta";

        if ($meta_value === null)
            $meta_value = $this->meta[$meta_key];
        else
            $this->meta[$meta_key] = $meta_value;

        if (!is_array($meta_value))
            $meta_value = [ $meta_value ];

        $query = $wpdb->prepare("DELETE FROM ".$table." WHERE qid = '%d' AND meta_key = '%s';", $this->id, $meta_key);
        $res['delete '.$meta_key] = $wpdb->query($query);

        foreach($meta_value as $mv)
        {
            $query = $wpdb->prepare
            ("INSERT INTO $table (qid, meta_key, meta_value) VALUES ('%d', '%s', '%s');",
                $this->id, $meta_key, $mv);
            
            $res[$mv] = $wpdb->query($query);
        }
        return $res;
    }

    public function delete()
    {
        global $wpdb, $wpof;

        $res = [];

        $table = $wpdb->prefix.$wpof->suffix_questionnaire."meta";
        $query = $wpdb->prepare("DELETE FROM ".$table." WHERE qid = '%d';", $this->id);
        $res['meta'] = $wpdb->query($query);

        $table = $wpdb->prefix.$wpof->suffix_question_relationships;
        $query = $wpdb->prepare("DELETE FROM ".$table." WHERE questionnaire_id = '%d';", $this->id);
        $res['relation'] = $wpdb->query($query);

        $table = $wpdb->prefix.$wpof->suffix_questionnaire;
        $query = $wpdb->prepare("DELETE FROM ".$table." WHERE id = '%d';", $this->id);
        $res['main'] = $wpdb->query($query);

        return $res;
    }

    /**
     * Réinitialise le tableau questions à partir d'une liste
     * Chaque élément de liste est un tableau contenant une clé 'type' (titre, competence) et une clé 'text' (le texte de l'entrée)
     */
    public function parse_list(Array $list_questions = []): Array
    {
        if (empty($list_questions)) return [];
        
        unset($this->question);
        $this->question = array();
        
        $log = [];
        $new_id = [];

        foreach($list_questions as $row)
        {
            if (!empty($row['title']))
            {
                $q = new Question($row['id']);
                if ($row['id'] == -1)
                {
                    $q->id = $q->get_last_id() + 1;
                    $new_id[$row['tmp_id']] = $q->id;
                }
                $q->needed = $row['needed'] == true;
                $q->title = $row['title'];
                $q->qtype = $row['qtype'];
                $q->level = $this->level;
                if (!empty($row['options']))
                {
                    $q->options = (object) $row['options'];
                    $log[] = var_export($q->options, true);
                }
                $this->question[$q->id] = $q;
                $log[] = $q->update();
            }
        }
        $this->question_id = array_keys($this->question);

        return [ 'logq' => $log, 'new_id' => $new_id ];
    }
    
    
    public function get_edit_questions(): String
    {
        global $wpof;
        if (count($this->question) == 0 && $this->id > 0)
            $this->question = $this->get_questions();
            
        ob_start();
        ?>
        <h2><?php echo $this->subject; ?></h2>
        <div class='questionnaire <?php echo $this->subject; ?>' data-id='<?php echo $this->id; ?>' data-subject='<?php echo $this->subject; ?>'>
        
        <div>
        <span class='bouton submit questionnaire-enregistrer'><?php _e("Enregistrer l'évaluation"); ?></span> 
        </div>
        <div>
        <span><?php _e("Ajouter"); ?> </span>
        <?php foreach($wpof->questionnaire_type_question->term as $key => $type) : ?>
        <span class='bouton questionnaire-ajouter' data-type='<?php echo $key; ?>'><?php echo $type->text; ?></span> 
        <?php endforeach; ?>
        </div>
        
        <ul class='questionnaire-sortable'>
        
        <?php
        foreach($this->question as $q)
        {
            $html .= $q->get_edit($this->id);
        }
        ?>
        
        </ul>
        </div>
        
        <?php
        return ob_get_clean();
    }
    
    public function get_questionnaire_control()
    {
        global $wpof;

        ob_start();
        ?>
        <tr class="questionnaire_row" data-id="<?php echo $this->id; ?>">
            <td class="center"><?php echo $this->id; ?></td>
            <td><a href="?questionnaire_id=<?php echo $this->id; ?>"><?php echo $this->title; ?></a></td>
            <td><?php echo $wpof->questionnaire_type->get_term($this->subject); ?></td>
            <td>
                <ul>
                <?php foreach($this->meta['nature_formation'] as $nf) : ?>
                <li><?php echo $wpof->nature_formation->get_term($nf); ?></li>
                <?php endforeach; ?>    
                </ul>
            </td>
            <td><?php echo $wpof->modalite_session->get_term($this->meta['modalite_session']); ?></td>
            <td class="center"><?php echo count($this->question_id); ?></td>
            <td><?php echo get_displayname($this->author_id, false); ?></td>
            <td><?php echo $wpof->questionnaire_level->get_term($this->level); ?></td>
            <td class="center"><a href="?questionnaire_id=<?php echo $this->id; ?>"><?php the_wpof_fa('pen', 'regular'); ?></a></td>
            <td class="center"><span class='pointer action qmanager' data-function='copy_questionnaire'><?php the_wpof_fa('copy', 'regular'); ?></span></td>
            <td class="center"><span class='pointer action qmanager delete' data-function='del_questionnaire'><?php the_wpof_fa_del(); ?></span></td>
        </tr>
        <?php
        return ob_get_clean();
    }

    /**
     * Renvoie le contenu HTML d'un questionnaire pour affichage et demande de réponse
     */
    public function get_display($args = []): String
    {
        global $wpof;

        if (isset($args['repondeur_id']))
            $this->init_reponses($args['session_id'], $args['repondeur_id']);

        $output = (isset($args['output'])) ?? "screen";
        
        ob_start(); ?>
        
        <form class="form formq">
        <?php foreach($args as $key => $value) : ?>
            <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>" />
        <?php endforeach; ?>
        <?php
        foreach($this->question as $q)
        {
            $q->output = $output;
            if (empty($this->reponses))
                echo $q->get_display();
            else
                echo $q->get_display($this->reponses[$q->id][$args['repondeur_id']] ?? null);
        }
        ?>
        <?php if ($output != 'pdf') : ?>
        <input type="button" class="icone-bouton reponse-submit" value="<?php _e('Enregistrez vos réponses'); ?>" />
        <?php endif; ?>
        </form>
        <?php
        return ob_get_clean();
    }
}

add_action('wp_ajax_add_questionnaire', 'add_questionnaire');
function add_questionnaire()
{
    $reponse = array();
    
    $questionnaire = new Questionnaire();
    $questionnaire->id = $questionnaire->get_last_id() + 1;
    $reponse['update'] = $questionnaire->update();
    $reponse['id'] = $questionnaire->id;
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_copy_questionnaire', 'copy_questionnaire');
function copy_questionnaire()
{
    $reponse = array();
    
    $questionnaire = new Questionnaire($_POST['id']);
    $questionnaire->id = $questionnaire->get_last_id() + 1;
    $reponse['update'] = $questionnaire->update();
    $reponse['html'] = $questionnaire->get_questionnaire_control();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_del_questionnaire', 'del_questionnaire');
function del_questionnaire()
{
    $reponse = array();
    
    $questionnaire = new Questionnaire($_POST['id']);
    $res = $questionnaire->delete();
    if (in_array(false, $res, true))
        $reponse['erreur'] = __('Problème dans la suppression de ce questionnaire. ').var_export($res, true);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_questionnaire_add_question', 'questionnaire_add_question');
function questionnaire_add_question()
{
    $reponse = array();
    
    $question = new Question();
    $questionnaire = new Questionnaire($_POST['questionnaire_id']);
    $question->qtype = $_POST['type'];
    $reponse['html'] = $question->get_edit($questionnaire->id);
    $reponse['log'] = var_export($_POST, true);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_questionnaire_enregistrer', 'questionnaire_enregistrer');
function questionnaire_enregistrer()
{
    global $wpof;
    $reponse = array();
    $reponse['logi'] = var_export(empty($_POST['nature_formation']) ? [] : $_POST['nature_formation'], true);

    if (empty($_POST['sujet']))
    {
        $reponse['erreur'] = __('Sujet du questionnaire non défini !');
        echo json_encode($reponse);
        die();
    }
    
    $level = (empty($_POST['level'])) ? 'user' : $_POST['level'];
    if (!in_array($wpof->role, $wpof->super_roles) && $level == 'of')
        $level = 'user';
    
    if (empty($_POST['questionnaire_id']))
    {
        $questionnaire = new Questionnaire();
        $questionnaire->id = $questionnaire->get_last_id() + 1;
    }
    else
    {
        $questionnaire = new Questionnaire($_POST['questionnaire_id']);
    }
    
    $questionnaire->subject = $_POST['sujet'];
    $questionnaire->level = $_POST['level'];
    $questionnaire->title = $_POST['title'];
    $questionnaire->meta['nature_formation'] = (empty($_POST['nature_formation'])) ? [] : $_POST['nature_formation'];// array of wpof->nature_formation
    $questionnaire->meta['modalite_session'] = (empty($_POST['modalite_session'])) ? [] : $_POST['modalite_session'];// one of wpof->modalite_session
    
    if (isset($_POST['questions']))
    {
        $reponse = array_merge($reponse, $questionnaire->parse_list($_POST['questions']));
    }
    $reponse['questions'] = $questionnaire->question;
    $reponse['logu'] = $questionnaire->update();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_nopriv_questionnaire_enregistrer_reponse', 'questionnaire_enregistrer_reponse');
add_action('wp_ajax_questionnaire_enregistrer_reponse', 'questionnaire_enregistrer_reponse');
function questionnaire_enregistrer_reponse()
{
    global $wpof;
    $json_reponse = [];

    $json_reponse = $_POST;

    $session_id = $_POST['session_id'] ?? null;
    $questionnaire_id = $_POST['questionnaire_id'] ?? null;
    $repondeur_id = $_POST['repondeur_id'] ?? null;
    $repondeur_type = $_POST['repondeur_type'] ?? null;

    if ($session_id && $questionnaire_id && $repondeur_type && $repondeur_id)
    {
        $questionnaire = new Questionnaire($questionnaire_id);
        $q_keys = array_keys($_POST);
        $q_keys = preg_grep('/q\d+/', $q_keys);
        foreach($q_keys as $qid)
        {
            $qid = (int) str_replace('q', '', $qid);
            $reponse = new QReponse();
            $reponse->questionnaire_id = $questionnaire_id;
            $reponse->question_id = $qid;
            $reponse->session_id = $session_id;
            $reponse->repondeur_id = $repondeur_id;
            $reponse->repondeur_type = $repondeur_type;
            $reponse->reponse = $_POST['q'.$qid];
            $json_reponse['log'] = $reponse->update();
        }
        $json_reponse['message_fin'] = __("Merci d'avoir pris le temps de répondre à ce questionnaire !");
        $wpof->log->log_action(__FUNCTION__, $questionnaire, 'repondeur_id', '', $repondeur_id);
    }
    else
        $json_reponse['erreur'] = __('Informations manquantes, enregistrement impossible');

    echo json_encode($json_reponse);
    die();
}

/**
 * Wrapper de contruction d'un Questionnaire
 * 
 * Stockage dans la globale $Questionnaire
 * 
 * S'il existe déjà, le questionnaire est simplement renvoyé
 */
function get_questionnaire_by_id($id): Questionnaire
{
    global $Questonnaire;
    
    if (!isset($Questionnaire[$id]))
    {
        $Questionnaire[$id] = new Questionnaire($id);
    }
    return $Questionnaire[$id];
}

