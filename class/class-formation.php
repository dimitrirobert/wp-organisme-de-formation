<?php
/*
 * class-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#[AllowDynamicProperties]
class Formation
{
    public $titre = "";
    public $permalien = "";
    public $editlink = "";
    public $slug = "";
    public $bandeau_id = "-1"; // image mise en avant
    
    // tableau d'ID des formateurs
    public $formateur = array();
    
    public $presentation = "";
    public $public_cible = "";
    public $objectifs = "";
    public $prerequis = "";
    public $programme = "";
    public $materiel_pedagogique = "";
    public $ressources = "";
    public $nature_formation = "form";
    public $nature_url = "";
    
    public $duree = 0;
    public $nb_jour = 0;
    public $show_tarif_heure = true;
    public $quizpr = "";
    public $quizobj = "";
    public $specialite = "";
    public $id;
    public $date_modif;
    public $timestamp_modif;
    
    public $post_tag = array();
    
    public $completion = "";
    public $acces_public = 0;
    public $brouillon = 0;
    
    public function __construct($formation_id = -1)
    {
        global $wpof;
        
        foreach(array_keys($wpof->desc_formation->term) as $k)
            $this->$k = "";
        
        /*
        echo '<div style="border: 1px green solid"><p>'.$formation_id.'</p><pre>';
        debug_print_backtrace(0, 3);
        echo '</pre></div>';
        */
        
        
        if ($formation_id > 0)
        {
            $this->id = $formation_id;
            $data = get_post($formation_id);
            if ($data === null)
                return null;
            
            $meta = get_post_meta($formation_id);
            
            // date de dernière modif
            $datetime_modif = DateTime::createFromFormat("Y-m-d H:i:s", $data->post_modified);
            $this->timestamp_modif = $datetime_modif->getTimestamp();
            $this->date_modif = date_i18n("j F Y", $this->timestamp_modif);
            
            // infos issues du post
            $this->titre = $data->post_title;
            $this->permalien = get_the_permalink($formation_id);
            $this->editlink = $this->permalien.'?'.$wpof->formation_edit_link_suffix;
            $this->slug = $data->post_name;
            
            // infos issues des meta données du post
            $this->formateur = get_post_meta($formation_id, "formateur", true); // permet de récupérer l'info sous forme de tableau
            if (!is_array($this->formateur))
                $this->formateur = array();
            
            // caractéristiques de la formation
            foreach($wpof->desc_formation->term as $k => $t)
            {
                if ($wpof->{"formation_".$k."_mode"} != 'force')
                    $this->$k = (isset($meta[$k][0])) ? $meta[$k][0] : "";
                else
                    $this->$k = $wpof->{"formation_".$k."_text"};
            }
            
            $this->duree = (isset($meta['duree'][0])) ? $meta['duree'][0] : "";
            $this->nb_jour = (isset($meta['nb_jour'][0])) ? $meta['nb_jour'][0] : "";
            $this->acces_public = (isset($meta['acces_public'][0])) ? $meta['acces_public'][0] : "";
            $this->completion = (isset($meta['completion'][0])) ? $meta['completion'][0] : "";
            $this->brouillon = (isset($meta['brouillon'][0])) ? $meta['brouillon'][0] : 0;
            if (isset($meta['nature_formation'][0]))
                $this->nature_formation = $meta['nature_formation'][0];
            switch ($this->nature_formation)
            {
                case 'bilan':
                    $this->nature_url = get_permalink($wpof->bc_page);
                    break;
                default:
                    $this->nature_url = "";
                    break;
            }
            
            // Tarif
            $this->init_tarif($meta);
            
            $this->quizpr = (isset($meta['quizpr'][0])) ? $meta['quizpr'][0] : "";
            $this->quizpr_id = (isset($meta['quizpr_id'][0])) ? $meta['quizpr_id'][0] : "";
            $this->quizobj = (isset($meta['quizobj'][0])) ? $meta['quizobj'][0] : "";
            $this->quizobj_id = (isset($meta['quizobj_id'][0])) ? $meta['quizobj_id'][0] : "";
            $this->ressources = (isset($meta['ressources'][0])) ? $meta['ressources'][0] : "";
            
            $this->specialite = (isset($meta['specialite'][0])) ? $meta['specialite'][0] : "";
            
            if (has_post_thumbnail($formation_id))
                $this->bandeau_id = get_post_thumbnail_id($formation_id);
            
            $tags = get_the_tags($this->id);
            if (!empty($tags))
                foreach($tags as $term)
                    $this->post_tag[] = $term->slug;
        }
        else
        {
            foreach($wpof->desc_formation->term as $k => $t)
            {
                if ($wpof->{"formation_".$k."_mode"} != 'force')
                    $this->$k = "";
                else
                    $this->$k = $wpof->{"formation_".$k."_text"};
            }
        }
    }
    
    // ensemble de contraintes à respecter sur les valeurs
    private function check_constraint($meta_key)
    {
        global $wpof;
        $valeur = $this->$meta_key;
        switch ($meta_key)
        {
            case 'duree':
                if ($this->nature_formation == "bilan")
                    $valeur = min($this->$meta_key, $wpof->max_duree_bc);
                break;
            default:
                break;
        }
        return $valeur;
    }
    
    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpof;
        if ($meta_value == "delete_meta")
            return $this->delete_meta($meta_key);
        
        $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key, $meta_value);
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
        
        $meta_value = $this->$meta_key = $this->check_constraint($meta_key);
        
        if (in_array($meta_key, array_keys($wpof->desc_formation->term)))
        {
            wp_update_post(array('ID' => $this->id));
            update_post_meta($this->id, "timestamp_modif", time());
        }
        
        $result = false;
        switch($meta_key)
        {
            case 'titre':
                $slug = sanitize_title($meta_value);
                update_post_meta($this->id, "timestamp_modif", time());
                $result = wp_update_post(array('ID' => $this->id, 'post_title' => $meta_value, 'post_name' => $slug));
                break;
            case 'category':
            case 'post_tag':
                $result = wp_set_object_terms($this->id, $this->$meta_key, $meta_key);
                foreach($this->formateur as $fid)
                {
                    $formateur = new Formateur($fid);
                    $formateur->update_my_tags();
                }
                break;
            default:
                $result = update_post_meta($this->id, $meta_key, $meta_value);
                break;
        }
        
        $acces_public = (integer) $this->set_formation_completion();
        update_post_meta($this->id, "completion", $this->completion);
        update_post_meta($this->id, "acces_public", $acces_public);
        
        foreach($this->formateur as $fid)
        {
            $f = get_formateur_by_id($fid);
            $f->set_profil_completion();
            $f->update_meta("completion");
        }
            
        return $result;
    }
    
    public function delete_meta($meta_key)
    {
        global $wpof;
        $this->$meta_key = 0;
        
        if (in_array($meta_key, array_keys($wpof->desc_formation->term)))
        {
            $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key);
            return delete_post_meta($this->id, $meta_key);
        }
        
        if (in_array($meta_key, array('category', 'post_tag')))
        {
            $res = wp_set_object_terms($this->id, array(), $meta_key);
            foreach($this->formateur as $fid)
            {
                $formateur = new Formateur($fid);
                $formateur->update_my_tags();
            }
            $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key);
            return $res;
        }
        
        return false;
    }
    
    
    /*
     * Qui peut modifier cette formation ?
     * Renvoie true si
     * $user_id a le rôle responsable ou admin
     * $user_id fait partie des formateurs animant cette session ($this->formateur)
     * $this->formateur est vide (session non définie)
     */
    public function can_edit($user_id = -1)
    {
        global $wpof;
        
        if ($user_id == -1)
            $user_id = get_current_user_id();
        
        $role = wpof_get_role($user_id);
        $super_roles = array("admin", "um_responsable");
        
        if ($role == null)
            return false;
        
        if (!isset($this->formateur)
            || !is_array($this->formateur)
            || count($this->formateur) == 0
            || in_array($user_id, $this->formateur)
            || in_array($role, $super_roles))
            return true;
        
        return false;
    }
    
    /*
     * Initialisation des tarifs
     */
    private function init_tarif($meta)
    {
        global $wpof;
        
        foreach(array_keys($wpof->type_tarif_formation->term) as $type)
            foreach(array_keys($wpof->declinaison_tarif) as $variante)
            {
                $tarif_var = "tarif_{$type}_{$variante}";
                $this->$tarif_var = (isset($meta[$tarif_var][0])) ? (float) ($meta[$tarif_var][0]) : 0;
            }
            
        if (isset($meta['tarif'][0]) && $this->tarif_base_inter_heure == 0)
            $this->tarif_base_inter_heure = (float) ($meta['tarif'][0]);
        
        if ($this->tarif_base_inter_heure == 0)
        {
            $this->tarif_base_inter_heure = (float) $wpof->tarif_inter;
            if ($this->duree > 0)
                $this->tarif_base_inter_total = $this->tarif_base_inter_heure * $this->duree;
        }
        
        // Tarif public
        if ($this->nature_formation == "bilan")
            $this->show_tarif_heure = false;
    }
    
    /*
     * Calcul de tarif
     * $modif = dernier paramètre modifié dans le tryptique duree, heure, total (heure et total sont préfixés soit par inter_, soit par intra_)
     * $type = type de tarif : le tarif de base est toujours présent, d'autres tarifs peuvent être définis dans les options
     * 
     * La règle de calcul :
     * si modif == duree ou heure, on recalcule total
     * si modif == total, on recalcule heure
     * En aucun cas la durée ne peut être modifiée par un changement de tarif
     */
    public function calcul_tarif($modif, $type = 'base')
    {
        global $wpof;
        
        if ($this->duree == 0)
            return;
        
        $update_form = array();
        
        if ($modif == "duree")
        {
            foreach (array_keys($wpof->type_tarif_formation->term) as $type)
                foreach(array("inter", "intra") as $var)
                {
                    $tarif_total = "tarif_{$type}_{$var}_total";
                    $tarif_heure = "tarif_{$type}_{$var}_heure";
                    $this->update_meta($tarif_total, $this->$tarif_heure * $this->duree);
                    $update_form[$tarif_total] = $this->$tarif_total;
                }
            
        }
        else
        {
            $param = explode('_', $modif);
            $tarif_total = "tarif_{$type}_".$param[0]."_total";
            $tarif_heure = "tarif_{$type}_".$param[0]."_heure";
            
            if ($param[1] == 'total')
            {
                $this->update_meta($tarif_heure, $this->$tarif_total / $this->duree);
                $update_form[$tarif_heure] = $this->$tarif_heure;
            }
            else
            {
                $this->update_meta($tarif_total, $this->$tarif_heure * $this->duree);
                $update_form[$tarif_total] = $this->$tarif_total;
            }
        }
        
        return json_encode($update_form);
    }
    
    /*
     * Renvoie le formulaire de création/modification de la formation
     */
    public function get_edit_formation()
    {
        global $wpof, $post;

        $role = wpof_get_role(get_current_user_id());
        ob_start(); ?>
        <div id="formation" class="id formation edit-data" data-id="<?php echo $this->id; ?>">
        <?php echo get_icone_aide("fiche_formation", __("Important : comment rédiger votre programme de formation")); ?> 
        <a href="<?php the_permalink(); ?>" target="_blank"><span class="icone-bouton"><?php the_wpof_fa_show(); ?> <?php _e("Aperçu"); ?></span></a>
        <div class="icone-bouton programme-session" data-formateur="<?php echo get_current_user_id(); ?>" data-id="<?php echo $this->id; ?>">
        <?php echo wpof_fa('file-circle-plus')." ".__('Programmer une session'); ?>
        </div>
        
        <div class="white-board">
        <?php echo get_input_jpost($this, "titre", array('input' => 'text', 'label' => __('Titre de la formation'))); ?>
        </div>
        <div class="formation-edit flexrow flextop nowrap margin">
            <div class="white-board side-formation">
                <?php
                    echo get_input_jpost($this, "brouillon", array('input' => 'checkbox', 'label' => __('Programme privé')));
                    if (!isset($wpof->formateur))
                        init_term_list("formateur");
                    echo get_input_jpost($this, "formateur", array('select' => 'multiple', 'rows' => 10, 'label' => __("Équipe pédagogique"),  'postprocess' => 'check_my_formation', 'needed' => true));
                    echo get_input_jpost($this, "specialite", array('select' => '', 'label' => __("Spécialité"), 'first' => __("Choisissez une spécialité, la plus précise possible"), 'aide' => "sessionformation_specialite", 'needed' => true));
                    echo get_input_jpost($this, "nature_formation", array('select' => '', 'label' => __("Nature de la formation"), 'postprocess' => 'toggle_bloc', 'ppargs' => array('ppclass' => 'nature_formation_text'), 'needed' => true));
                    echo get_input_jpost($this, "duree", array('input' => 'number', 'step' => 0.25, 'label' => __('Durée prévue de la formation en heures'), 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'duree'), 'needed' => true));
                    
                ?>
                    <p class="show-when nature_formation_text bilan alerte" style="<?php echo ($this->nature_formation == "bilan") ? "display: block": "" ; ?>"><?php _e("Il est interdit de publier un tarif horaire dans le cadre du bilan de compétence. Il n'apparaît ci-dessous que pour vous."); ?></p>
                <?php
                    foreach($wpof->type_tarif_formation->term as $type_key => $type) : ?>
                        <table class="opaga tarif" data-type="<?php echo $type_key; ?>">
                        <tr>
                            <td class="center"><?php echo $wpof->toc->set_title($type->text, 3); ?> <?php echo get_icone_aide("formation_tarif_$type_key"); ?></td>
                            <th class="thin"><?php echo $wpof->declinaison_tarif['heure']; ?></th>
                            <th class="thin"><?php echo $wpof->declinaison_tarif['total']; ?></th>
                        </tr>
                        <tr>
                            <th><?php echo $wpof->declinaison_tarif['inter']; ?></th>
                            <td class="right"><?php echo get_input_jpost($this, "tarif_{$type_key}_inter_heure", array('input' => 'number', 'step' => 0.001, 'aide' => false, 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'inter_heure', 'pptype' => $type_key))); ?></td>
                            <td class="right"><?php echo get_input_jpost($this, "tarif_{$type_key}_inter_total", array('input' => 'number', 'step' => 0.01, 'aide' => false, 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'inter_total', 'pptype' => $type_key))); ?></td>
                        </tr>
                        <tr>
                            <th><?php echo $wpof->declinaison_tarif['intra']; ?></th>
                            <td class="right"><?php echo get_input_jpost($this, "tarif_{$type_key}_intra_heure", array('input' => 'number', 'step' => 0.001, 'aide' => false, 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'intra_heure', 'pptype' => $type_key))); ?></td>
                            <td class="right"><?php echo get_input_jpost($this, "tarif_{$type_key}_intra_total", array('input' => 'number', 'step' => 0.01, 'aide' => false, 'postprocess' => 'update_tarif', 'ppargs' => array('ppmodif' => 'intra_total', 'pptype' => $type_key))); ?></td>
                        </tr>
                        </table>
                    <?php endforeach; ?>
                    <?php
                    if (in_array($role, $wpof->super_roles))
                    {
                        if (!isset($wpof->post_tag))
                            init_term_list("post_tag");
                        echo get_input_jpost($this, "post_tag", array('select' => 'multiple', 'label' => __("Classification")));
                    }
                    ?>
            </div>
            <div  class="white-board desc-formation metadata">
                <?php
                    foreach($wpof->desc_formation->term as $k => $t)
                    {
                        if ($wpof->{"formation_".$k."_mode"} != 'force')
                            echo get_input_jpost($this, $k, array('editor' => '', 'label' => $t->text, 'toc' => 1, 'needed' => $t->formation_needed, 'aide' => 'sessionformation_'.$k));
                        else
                        {
                            echo $wpof->toc->set_title($t->text, 3);
                            ?>
                            <p><em><?php _e("Ce paramètre est fixé par votre responsable de formation et ne peut être modifié."); ?></em></p>
                            <p><?php echo $wpof->{"formation_".$k."_text"}; ?></p>
                            <?php
                        }
                    }
                ?>
            </div>
        </div>
        
        </div>
        <?php
        echo $wpof->toc->get_html_toc("formation");
        
        return ob_get_clean();
    }

    
    /**
     * Retourne le contenu résumé de la session sous forme de vignette (article au sens WP) pour les listes publiques
     */
    public function get_item_html($heading = 3)
    {
        $heading_class = ($this->acces_public) ? 'public' : 'prive';
        $article =
            '<article class="formation-detail '.$heading_class.'">
                <' . $heading . '><a href="' . $this->permalien . '">' . $this->titre . '</a></' . $heading . '>
                <div class="excerpt-wrap entry-summary">
                    ' . wp_trim_words($this->get_excerpt(), 30, ' &hellip;' ) . '
                    <div class="read-more-wrapper">
                        <a href="' . $this->permalien . '" class="readmore" rel="bookmark">' . __('Voir la formation →') . '<span class="screen-reader-text">' . $this->titre . '</span></a>
                    </div>
                </div>
            </article>';

        return apply_filters('opaga_formation_item_html', $article, $this);
    }
    
    /**
     * Construit le content au sens Wordpress à afficher dans un post de type formation
     * 
     * @return string le HTML à afficher dans le "content" wordpress
     * 
     * @see /wpof-custom-post-types.php
     */
    public function get_content()
    {
        global $wpof;
        
        $sessions = get_formation_sessions(array('formation_id' => $this->id, 'quand' => 'futur'));
        ob_start();
        ?>
        
        <div class="formation-global">
            <div class="formation-side-content">
                <?php if ($this->can_edit()) : ?>
                <a href="<?php echo $this->editlink; ?>" target="_blank"><span class="icone-bouton"><?php the_wpof_fa_edit(); ?> <?php _e("Modifier"); ?></span></a>
                <div class="icone-bouton programme-session" data-formateur="<?php echo get_current_user_id(); ?>" data-id="<?php echo $this->id; ?>">
                <?php echo wpof_fa('file-circle-plus')." ".__('Programmer une session'); ?>
                </div>
                <?php endif; ?>
                <p><?php printf(__("Programme édité le %s"), $this->date_modif); ?></p>
                <?php if ($this->duree > 0) : ?>
                    <h2><?php _e("Durée"); ?></h2>
                    <p><?php printf(__("%d heures"), $this->duree); ?></p>
                    <?php if ($this->nb_jour != "") echo "<p>".$this->nb_jour."</p>"; ?>

                    <?php
                    foreach($wpof->type_tarif_formation->term as $type_key => $type) :
                        $tarif_inter_t = "tarif_{$type_key}_inter_total";
                        $tarif_intra_t = "tarif_{$type_key}_intra_total";
                        $tarif_inter_h = "tarif_{$type_key}_inter_heure";
                        $tarif_intra_h = "tarif_{$type_key}_intra_heure";
                        
                        if ($this->$tarif_inter_t + $this->$tarif_intra_t > 0) : ?>
                            <h2><?php echo $type->text; ?></h2>
                            <?php if ($this->$tarif_inter_t > 0) : ?>
                                <p><?php echo get_tarif_formation($this->$tarif_inter_t); ?></p>
                                <?php if ($this->show_tarif_heure) : ?>
                                <p><?php printf(__('Soit %s de l’heure'), get_tarif_formation($this->$tarif_inter_h)); ?></p>
                                <?php endif; ?>
                                <p><?php printf(__('Tarif %s'), $wpof->declinaison_tarif['inter']); ?></p>
                            <?php endif; ?>
                            
                            <?php if ($this->$tarif_intra_t > 0) : ?>
                                <p><?php echo get_tarif_formation($this->$tarif_intra_t); ?></p>
                                <?php if ($this->show_tarif_heure) : ?>
                                <p><?php printf(__('Soit %s de l’heure'), get_tarif_formation($this->$tarif_intra_h)); ?></p>
                                <?php endif; ?>
                                <p><?php printf(__('Tarif %s'), $wpof->declinaison_tarif['intra']); ?></p>
                            <?php endif; ?>
                        <?php endif;
                    endforeach;
                    
                    echo $wpof->of_exotva ? "<p>".$wpof->terms_exo_tva."</p>" : "";
                ?>
                <?php endif; ?>

                <?php if (count($this->formateur) > 0) :?>
                    <h2><?php _e("Équipe pédagogique"); ?></h2>
                    <?php the_liste_formateur(array('only' => $this->formateur, 'type' => 'list')); ?>
                <?php endif; ?>
            
                <h2><?php _e("Prochaines sessions"); ?></h2>
                <?php
                    if (null == $sessions)
                        echo "<p>".__("Pas de session programmée pour l'instant")."</p>";
                    else
                    {
                        ?>
                        <ul class="list list_date">
                        <?php
                        foreach($sessions as $s)
                        {
                            echo "<li>".wpof_fa('calendar-days')." <a href='{$s->permalien}'>{$s->dates_texte}</a></br>{$s->ville}</li>";
                        }
                        ?>
                        </ul>
                        <?php
                    }
                ?>
            </div>

            <div class="formation-content">
            <?php if (!$this->acces_public || $this->brouillon) : ?>
                <div class="important"><?php _e("Ce programme n'est pas visible publiquement"); ?></div>
            <?php endif; ?>
            <?php
            
                $desc_proposition = $wpof->desc_formation->get_group("proposition")->term;
                
                if (isset($desc_proposition['presentation']))
                {
                    $pres_gal = $this->presentation;
                    unset($desc_proposition['presentation']);
                    ?>
                    
                    <div class="chapo">
                        <?php echo wpautop($pres_gal); ?>
                    </div>
                    <div class="boutons">
                        <a class="icone-bouton" href="/?download=proposition&ci=<?php echo $this->id; ?>&c=<?php echo $wpof->doc_context->formation;?>" target="_blank"><?php the_wpof_fa_pdf(); ?><?php _e("Télécharger le programme"); ?></a>
                        <div class="icone-bouton dynamic-dialog premier-contact" data-function="premier_contact" data-formationid="<?php echo $this->id; ?>" data-submit-txt="<?php _e("Envoyer mon message"); ?>">
                            <?php the_wpof_fa_sendmail(); ?>
                            <?php _e("Demande de renseignements"); ?>
                        </div>
                        <?php if (!empty($this->nature_url)) : ?>
                        <a href="<?php echo $this->nature_url; ?>"><div class="icone-bouton"><?php echo wpof_fa("fa-circle-info").$wpof->nature_formation->get_term($this->nature_formation)." : ".__("en savoir plus"); ?></div></a>
                        <?php endif; ?>
                    </div>
                    <?php
                }
                foreach($desc_proposition as $k => $t)
                {
                    if (!empty($this->$k))
                    {
                    ?>
                        <h2><?php echo $t->text; ?></h2>
                        <?php echo wpautop($this->$k); ?>
                    <?php
                    }
                }
            ?>
            </div> <!-- .formation-content -->
        </div> <!-- .formation-global -->
        <?php    
        return ob_get_clean();
    }
    
    /**
     * Retourne la présentation de la formation pour une affichage dans le excerpt du type de contenu formation
     * 
     * @see /wpof-custom-post-types.php
     */
    public function get_excerpt()
    {
        return $this->presentation;
    }
    
    public function get_displayname($link = false)
    {
        if ($link)
            return $this->get_a_link();
        else
            return $this->titre;
    }
    
    public function get_a_link($text = null)
    {
        if ($text === null)
            $text = $this->titre;
        return '<a href="'.$this->permalien.'" title="'.strip_tags($this->titre).'">'.$text.'</a>';
    }
    
    /**
     * Fixe la valeur brouillon à 1 si pas de formateur⋅ice public
     * @return true si changement d'état
     */
    public function set_brouillon_if_no_formateur()
    {
        if ($this->brouillon != 1 && empty($this->get_public_formateurs_id()))
        {
            $this->update_meta("brouillon", 1);
            return true;
        }
        return false;
    }
    
    /**
     * Renvoie la liste des formateur⋅ices dont le profil n'est pas archivé
     */
    public function get_public_formateurs_id()
    {
        $public_formateurs_id = array();
        foreach($this->formateur as $fid)
            if (get_user_meta($fid, "archive", true) != 1)
                $public_formateurs_id[] = $fid;
        
        return $public_formateurs_id;
    }

    public function get_formateurs_noms($format = 'string')
    {
        $formateurs_nom = array();
        foreach($this->formateur as $f_id)
            $formateurs_nom[] = get_displayname($f_id);
        if ($format == 'string')
            return implode(', ', $formateurs_nom);
        else
            return $formateurs_nom;
    }
    
    /*
     * Calcule le taux de complétion d'une formation
     * Renvoie ce taux de complétion en float
     */
    public function set_formation_completion()
    {
        global $wpof;
        
        $total = count($wpof->completion_formation);
        $rempli = 0;
        foreach($wpof->completion_formation as $k)
            if (!empty($this->$k) && trim($this->$k) != "")
                $rempli++;
        
        $this->completion = $rempli."/".$total;
        
        return $rempli / $total;
    }
    
    /*
     * Retourne la liste des champs vides à compléter
     */
    public function get_completion_missing()
    {
        global $wpof;
        
        $vide = array();
        foreach($wpof->completion_formation as $k)
            if (empty(trim($this->$k)))
            {
                if ($wpof->desc_formation->is_term($k))
                    $vide[] = $wpof->desc_formation->get_term($k);
                else
                    $vide[] = $k;
            }
        
        return $vide;
    }
    
    /*
     * Retourne le taux de complétion de la formation sous la forme :
     * nb_champs_remplis / nb_champs_a_repmlir
     */
    public function get_formation_completion($html = true)
    {
        if ($html)
        {
            $class = ($this->acces_public) ? "fait" : "attention pointer";
            return '<span class="'.$class.' completion">'.$this->completion.'</span>';
        }
        else
            return $this->completion;
    }
    
    // Renvoie un tableau d'ID de sessions liées à cette formation
    public function get_session_links()
    {
        global $wpdb;
        
        $query = $wpdb->prepare(
            "SELECT pm.post_id as session_id
            FROM ".$wpdb->prefix."postmeta as pm, ".$wpdb->prefix."posts as p
            WHERE pm.meta_key = 'formation'
            AND pm.meta_value = '%d'
            AND p.ID = '%d'
            AND p.post_type = 'formation';",
            $this->id, $this->id);
        
        $session_id = $wpdb->get_col($query);
        if (empty($session_id))
            $session_id = array();
        else
        {
            $query =
                "SELECT meta_value as timestamp, post_id as session_id
                FROM ".$wpdb->prefix."postmeta
                WHERE meta_key = 'first_date_timestamp'
                AND post_id IN ('".implode("','", $session_id)."');";
            $results = $wpdb->get_results($query);
            
            $date_session_id = array();
            foreach($results as $row)
                $date_session_id[$row->timestamp] = $row->session_id;
            
            // les entrées avec clé datées doivent se trouver en début de tableau pour être préservée par array_unique
            $session_id = $date_session_id + $session_id;
            $session_id = array_unique($session_id);
            krsort($session_id);
        }
        return $session_id;
    }
    public function get_delete_bouton($texte_bouton = "Supprimer cette formation")
    {
        ob_start();
        ?>
        <div class="dynamic-dialog delete icone-bouton" data-function="remove_formation_form" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>">
            <?php the_wpof_fa_del(); ?>
            <?php echo $texte_bouton; ?>
        </div>
        <?php
        return ob_get_clean();
    }

    
    public static function get_formation_control_head()
    {
        global $wpof;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr>
        <th><?php _e("Titre formation"); ?></th>
        <?php if (in_array($role, $wpof->super_roles)) : ?>
        <th class='thin'><?php _e("Date modif"); ?></th>
        <th><?php _e("Formateur⋅trice(s)"); ?></th>
        <?php if (champ_additionnel('formateur_code')) : ?>
        <th class='thin'><?php _e("Code, identifiant"); ?></th>
        <?php endif; ?>
        <th><?php _e("Classification"); ?> <a href="<?php echo admin_url('edit-tags.php?taxonomy=post_tag&post_type=formation'); ?>" title="<?php _e("Gérer les mots-clés"); ?>"><?php the_wpof_fa_edit(); ?></a></th>
        <th><?php _e("Spécialité"); echo get_icone_aide("sessionformation_specialite"); ?></th>
        <?php endif; ?>
        <th><?php _e("Sessions liées"); ?></th>
        <th class='thin'><?php _e("Complétion description"); echo get_icone_aide("sessionformation_completion_description"); ?></th>
        <th class='thin not_orderable'><?php _e("Programmer une session"); echo get_icone_aide("formation_programmer_session"); ?></th>
        <th class="thin"><?php _e("Privé"); echo get_icone_aide("formation_brouillon"); ?></th>
        <?php if (in_array($role, $wpof->super_roles) || champ_additionnel("all_can_remove_formation")) : ?>
        <th class="thin not_orderable"><?php _e("Supprimer"); ?></th>
        <?php endif; ?>
        </tr>
        <?php
        return ob_get_clean();
    }
    /*
     * Renvoie une ligne de tableau pour contrôler la formation
     * 
     */
    public function get_formation_control()
    {
        global $wpof;
        $role = wpof_get_role(get_current_user_id());
        $admin_acces = in_array($role, $wpof->super_roles);
        ob_start();
        ?>
        <tr id="formation<?php echo $this->id; ?>" data-context="formation" class="editable">
        <td>
        <a href="<?php echo $this->editlink; ?>"><?php the_wpof_fa_edit(); echo $this->titre; ?></a>
        </td>
        <?php if ($admin_acces) : ?>
        <td><span class="hidden"><?php echo $this->timestamp_modif; ?></span><?php echo $this->date_modif; ?></td>
        <td>
        <?php
            /*
            if (!isset($wpof->formateur))
                init_term_list("formateur");
            echo get_input_jpost($this, "formateur", array('select' => 'multiple', 'aide' => false));
            */
            echo $this->get_formateurs_noms();
        ?></td>
        <?php if (champ_additionnel('formateur_code')) : ?>
        <td>
            <?php if (!empty($this->formateur))
            {
                $formateur = get_formateur_by_id($this->formateur[0]);
                echo $formateur->code;
            }
            ?>
        </td>
        <?php endif; ?>
        <td>
        <?php
            if (!isset($wpof->post_tag))
                init_term_list("post_tag");
            echo get_input_jpost($this, "post_tag", array('select' => 'multiple', 'aide' => false));
        ?>
        </td>
        <td><?php echo get_input_jpost($this, "specialite", array('select' => '', 'aide' => false)); ?></td>
        <?php endif; ?>
        <td>
        <?php
        foreach($this->get_session_links() as $ts => $sid)
        {
            $date = ($ts > 100000) ? date('d/m/Y', $ts) : "pas de date";
            echo '<a title="'.$date.'" href="'.get_permalink($sid).'">'.$sid.'</a> ';
        }
        ?>
        </td>
        <td class="center"><?php echo $this->get_formation_completion(true); ?></td>
        <td class="center">
        <div class="icone-bouton programme-session" data-id="<?php echo $this->id; ?>">
        <?php the_wpof_fa('file-circle-plus'); //the_wpof_fa('calendar-days') ?>
        </div></td>
        <td class="center edit-data"><?php echo get_input_jpost($this, "brouillon", array('input' => 'checkbox', 'display' => 'inline', 'aide' => false)); ?></td>
        <?php if ($admin_acces || champ_additionnel("all_can_remove_formation")) : ?>
        <td class="center"><?php echo $this->get_delete_bouton(""); ?></td>
        <?php endif; ?>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    public function get_doc_titre_formation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "formation:$function_name", 'desc' => __("Intitulé de la formation"));
        return $result[$arg];
    }
    public function get_doc_formateur($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->get_formateurs_noms(), 'tag' => "formation:$function_name", 'desc' => __("Équipe pédagogique"));
        return $result[$arg];
    }
    public function get_doc_nb_formateur($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => count($this->formateur), 'tag' => "formation:$function_name", 'desc' => __("Nombre de formateur⋅trices"));
        return $result[$arg];
    }
    public function get_doc_duree($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->nb_heure_decimal." h", 'tag' => "formation:$function_name", 'desc' => __("Durée en heures"));
        return $result[$arg];
    }
    /*
     * Deux paramètres possibles pour param :
     * * type de tarif (base par défaut)
     * * 'h' pour horaire, 't' pour total (par défaut)
     * Les deux paramètres séparés par une virgule
     */
    private function parse_tarif_param($param, $session_type = 'inter')
    {
        $params = explode(',', $param);
        $type = 'base';
        $temps  ='total';
        
        foreach($params as $p)
        {
            switch($p)
            {
                case 't':
                    $temps = 'total';
                    break;
                case 'h':
                    $temps = 'heure';
                    break;
                default:
                    $type = $p;
                    break;
            }
        }
        return "tarif_".$type."_".$session_type."_".$temps;
    }
    public function get_doc_tarif_inter($arg = 'valeur', $param = 'base,t')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = '';
        if ($arg == 'valeur')
        {
            $tarif = $this->parse_tarif_param($param, 'inter');
            if (isset($this->$tarif))
                $valeur = $this->$tarif;
        }
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "formation:$function_name", 'desc' => __("Tarif inter-entreprise (par personne)"));
        return $result[$arg];
    }
    public function get_doc_tarif_intra($arg = 'valeur', $param = 'base,t')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = '';
        if ($arg == 'valeur')
        {
            $tarif = $this->parse_tarif_param($param, 'intra');
            if (isset($this->$tarif))
                $valeur = $this->$tarif;
        }
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "formation:$function_name", 'desc' => __("Tarif intra-entreprise (groupe)"));
        return $result[$arg];
    }
    public function get_doc_presentation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_objectifs_pro($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_objectifs($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_prerequis($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_public_cible($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_modalites_pedagogiques($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_ressources($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_organisation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_materiel_pedagogique($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_accessibilite($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_modalites_evaluation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_inscription_delai($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_programme($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_propriete_intellectuelle($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "formation:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
}

function get_formation_by_id($id)
{
    global $Formation;
    
    if (!isset($Formation[$id]))
        $Formation[$id] = new Formation($id);
        
    return $Formation[$id];
}

add_action( 'wp_ajax_formation_update_tarif', 'formation_update_tarif' );
function formation_update_tarif($args = array())
{
    $reponse = array();
    
    $formation = new Formation($_POST['formation_id']);
    $type = (isset($_POST['pptype'])) ? $_POST['pptype'] : "";
    $reponse['update_form'] = $formation->calcul_tarif($_POST['ppmodif'], $type);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_add_new_formation', 'add_new_formation');
function add_new_formation()
{
    global $wpof;
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    if ($_POST['titre'] == "")
    {
        $reponse['erreur'] = __("Vous devez fournir un titre");
        echo json_encode($reponse);
        die();
    }
    $title = $_POST['titre'];
    
    $slug = sanitize_title($title);
    $formation_id = wp_insert_post(array('post_title' => $title, 'post_type' => 'formation', 'post_status' => 'publish', 'post_author' => get_current_user_id(), 'post_name' => $slug), true);
    if (is_wp_error($formation_id))
    {
        $reponse['erreur'] = __("Erreur de création de formation")." : ".__("WP Error : ").$formation_id->get_error_message();
        echo json_encode($reponse);
        die();
    }

    // Brouillon par défaut
    update_post_meta($formation_id, "brouillon", 1);

    // nature_formation = form par défaut
    update_post_meta($formation_id, "nature_formation", "form");
    
    if (!empty($_POST['formateur']))
    {
        // liste des formateurs sous forme de tableau
        $formateur = explode(',', $_POST['formateur']);
    }
    else
        $formateur = array(get_current_user_id());
    
    // ID des formateurs dans la formation
    update_post_meta($formation_id, "formateur", $formateur);

    $reponse['url'] = get_permalink($formation_id) . '?' . $wpof->formation_edit_link_suffix;
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_remove_formation', 'remove_formation');
function remove_formation()
{
    global $wpof;
    $reponse = array();
    $reponse['log'] = var_export($_POST, true);
    
    $formation = new Formation($_POST['formation_id']);
    if (isset($_POST['session']))
    {
        $session_id = $_POST['session'];
        
        $session = array();
        foreach($session_id as $sid)
            $session[$sid] = get_session_by_id($sid);
        
        switch($_POST['session_action'])
        {
            case "remove_all":
                foreach($session as $s)
                    $s->delete();
                break;
            case "detach":
                foreach($session as $s)
                {
                    $s->update_meta("formation", -1);
                    if (empty($s->session_unique_titre))
                    {
                        $s->titre_formation = $formation->titre;
                        $s->init_slug(false);
                        wp_update_post(array('ID' => $s->id, 'post_title' => $s->titre_formation, 'post_name' => $s->slug));
                    }
                    $s->update_meta("nature_formation", $formation->nature_formation);
                    $s->update_meta("specialite", $formation->specialite);
                    foreach(array_keys($wpof->desc_formation->term) as $term)
                        $s->update_meta($term, $formation->$term);
                }
                break;
            case "attach":
                // déterminer l'ID de la formation à laquelle rattacher les sessions
                $new_formation_id = null;
                if (!empty($_POST['new_formation_id_all']))
                    $new_formation_id = $_POST['new_formation_id_all'];
                if (!empty($_POST['new_formation_id']))
                    $new_formation_id = $_POST['new_formation_id'];
                
                if ($new_formation_id === null)
                {
                    $reponse['erreur'] = __("Rien à faire !");
                    echo json_encode($reponse);
                    die();
                }
                foreach($session as $s)
                {
                    $s->update_meta("formation", $new_formation_id);
                    if (empty($s->session_unique_titre))
                    {
                        $s->titre_formation = get_the_title($new_formation_id);
                        $s->init_slug(false);
                        wp_update_post(array('ID' => $s->id, 'post_title' => $s->titre_formation, 'post_name' => $s->slug));
                    }
                }
                break;
        }
    }
    $wpof->log->log_action(__FUNCTION__, $formation);
    wp_delete_post($_POST['formation_id']);
    
    echo json_encode($reponse);
    
    die();
}
