<?php
/*
 * class-opaga-content.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class OpagaContent
{
    public  int $id = -1;
    public  String $text = "";
    public  String $category = "";
    public  String $date = "";
    public  String $type = "";
    public  String $edit_permalink = "";
    public  String $display_permalink = "";

    public function __construct(int $id = -1)
    {
        if ($id > 0)
        {
            $this->id = $id;
            $data = $this->init_from_db();
            if ($data !== null)
            {
                $this->text = $data->text;
                $this->category = $data->category;
                $this->date = $data->date;
            }
        }
        else
            $this->date = date("Y-m-d");
    }

    public function __toString()
    {
        return $this->text.'<br />'.$this->category.'/'.$this->date;
    }
    
    /**
     * Initialise un content depuis la base de données
     */
    protected function init_from_db() : stdClass|null
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_content;

        $query = $wpdb->prepare("SELECT text, category, date FROM ".$table." WHERE id = %d AND type = %s;", $this->id, $this->type);
        return $wpdb->get_row($query);
    }

    /**
     * Crée le formulaire de saisie d'un content
     */
    public function get_edit() : String
    {
        global $tinymce_wpof_settings;
        ?>
        <form class="grid-6col">
            <div class="grid-1">
                <div>
                <label class="top" for="date"><?php _e("Date de publication"); ?></label>
                <input type="text" name="date" id="date" class="datepicker_sql" value="<?php echo $this->date; ?>" />
                </div>
                <div>
                <label class="top" for="category"><?php _e("Catégorie"); ?></label>
                <?php echo $this->get_select_categories($this->category); ?>
                </div>
                <div class="submit icone-bouton edit-opaga-content"><?php _e("Publier"); ?></div>
            </div>
            <div class="editor grid-5">
                <?php echo wp_editor($this->text, "text", $tinymce_wpof_settings); ?>
            </div>
            <input type="hidden" name="id" value="<?php echo $this->id; ?>" />
            <input type="hidden" name="type" value="<?php echo $this->type; ?>" />
        </form>

        <?php
        return ob_get_clean();
    }

    /**
     * Renvoie une catégorie sous la forme d'un objet avec tous ses paramètres
     * Le slug est unique dans la table, il ne peut y avoir le même slug pour deux types différents
     * @param string $slug
     * @return stdClass|null
     */
    public static function get_category(String $slug) : stdClass|null
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_content_cat;

        $query = $wpdb->prepare("SELECT * FROM ".$table." WHERE slug = %s;", $slug);
        return $wpdb->get_row($query);
    }

    /**
     * Insère une nouvelle catégorie de contenu dans la table
     * @param object $cat
     * @return int|bool
     */
    public static function insert_category(object $cat) : int|bool
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_content_cat;

        if (empty($cat->slug) || empty($cat->name))
            return false;

        $query = $wpdb->prepare("INSERT INTO ".$table." (slug,name,style,icon,type) VALUES (%s,%s,%s,%s,%s);", $cat->slug, $cat->name, $cat->style ?? "", $cat->icon ?? "", $cat->type);
        echo $query;
        return $wpdb->query($query);
    }

    /**
     * Mise à jour de catégorie $cat
     * @param object $cat
     * @return int|bool
     */
    public static function update_category(object $cat) : int|bool
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_content_cat;

        $query = $wpdb->prepare("UPDATE ".$table." SET name = %s, style = %s, icon = %s WHERE slug = %s;", $cat->name, $cat->style ?? "", $cat->icon ?? "", $cat->slug);
        return $wpdb->query($query);
    }
    /**
     * Récupère la liste des catégories de content selon le type demandé type
     */
    public static function get_categories(String $type = null) : Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_content_cat;

        if ($type === null)
            $query = "SELECT slug, name FROM ".$table.";";
        else
            $query = $wpdb->prepare("SELECT slug, name FROM ".$table." WHERE type = %s;", $type);
        $categories = [];
        foreach($wpdb->get_results($query) as $catobj)
            $categories[$catobj->slug] = $catobj->name;
        return $categories;
    }

    /**
     * Renvoie un input select renseigné avec toutes les catégories du type de contenu correspondant
     * @param mixed $selected
     * @return string
     */
    public function get_select_categories($selected = "") : string
    {
        $categories = self::get_categories($this->type);
        
        ob_start();
        if (!empty($categories))
            echo select_by_list($categories, 'category', $selected, '', __('Choisissez une catégorie'));
        
        return ob_get_clean();
    }

    /**
     * Retroune un tableau avec tous les contents selon le type demandé
     * @param string $type
     * @return array
     */
    public static function get_contents(String $type = null) : Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_content;

        if ($type === null)
            $query = $wpdb->prepare("SELECT id FROM ".$table." ORDER BY date DESC;");
        else
            $query = $wpdb->prepare("SELECT id FROM ".$table." WHERE type = %s ORDER BY date DESC;", $type);
        
        $contents = [];
        foreach($wpdb->get_col($query) as $id)
            $contents[$id] = new Info($id);

        return $contents;
    }

    /**
     * Retourne l'en-tête du tableau de contrôle de content
     * @return String
     */
    public static function get_control_head() : String
    {
        global $wpof;
        ob_start();
        ?>
        <thead>
        <tr>
            <th><?php _e("Texte"); ?></th>
            <th class="thin"><?php _e("Catégorie"); ?></th>
            <th class="thin" id="order" data-order="desc"><?php _e("Date"); ?></th>
            <th class="thin"><?php _e("Modifier"); ?></th>
        </tr>
        </thead>
        <?php
        return ob_get_clean();
    }

    /**
     * Retourne une ligne de contrôle pour un content
     * @return string
     */
    public function get_control() : String
    {
        ob_start();
        ?>
        <tr>
            <td><?php echo explode("\n", $this->text, 1)[0]; ?></td>
            <td class="center"><?php echo $this->category; ?></td>
            <td class="center"><?php echo $this->date; ?></td>
            <td class="center"><a href="<?php echo $this->edit_permalink; ?>" class="icone-bouton"><?php the_wpof_fa_edit(); ?></a></td>
        </tr>
        <?php
        return ob_get_clean();
    }
}

add_action('wp_ajax_edit_opaga_content', 'edit_opaga_content');
function edit_opaga_content()
{
    global $wpof;
    $reponse = [];
    if ($wpof->has_super_role())
    {
        global $wpof, $wpdb;
        $table = $wpdb->prefix.$wpof->suffix_content;

        if (!empty($_POST['text']) && isset($_POST['type']) && in_array($_POST['type'], array_keys($wpof->content_types)))
        {
            $category = "";
            if (isset($_POST['category']) && in_array($_POST['category'], array_keys(OpagaContent::get_categories($_POST['type']))))
                $category = $_POST['category'];
            if (is_numeric($_POST['id']))
            {
                if ($_POST['id'] <= 0)
                    $query = $wpdb->prepare(
                        "INSERT INTO ".$table." (text,category,type,date) VALUES (%s,%s,%s,%s);",
                        $_POST['text'], $category, 'info', $_POST['date']);
                else
                    $query = $wpdb->prepare(
                        "UPDATE ".$table." SET text = %s, category = %s, type = %s, date = %s WHERE id = %d;",
                        $_POST['text'], $category, 'info', $_POST['date'], $_POST['id']);
                $reponse['message'] = $wpdb->query($query);
                if ($reponse['message'] != false)
                    $reponse['message'] = __('Information ajoutée ou modifiée avec succès');
                $reponse['query'] = $query;
            }

        }
        else
            $reponse['message'] = __('Texte vide ou type incorrect');
    }
    else
        $reponse['message'] = __('Droits insuffisants');
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_edit_content_category', 'edit_content_category');
function edit_content_category()
{
    global $wpof;
    $reponse = [];
    if ($wpof->has_super_role())
    {
        global $wpof, $wpdb;
        $table = $wpdb->prefix.$wpof->suffix_content_cat;
        $reponse['post'] = $_POST;
        $post = (object) $_POST;
        if (isset($post->new_category))
        {
            $post->slug = sanitize_title($post->slug);
            $res = OpagaContent::insert_category($post);
        }
        else
            $res = OpagaContent::update_category($post);

        if ($res === false)
            $reponse['erreur'] = sprintf(__("Impossible d'ajouter ou mettre à jour la catégorie %s"), var_export($post, true));
    }
    else
        $reponse['erreur'] = __('Droits insuffisants');
    echo json_encode($reponse);
    die();
}