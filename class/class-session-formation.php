<?php
/*
 * class-session-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
require_once(wpof_path . "/class/class-formation.php");
require_once(wpof_path . "/class/class-client.php");
require_once(wpof_path . "/class/class-lieu.php");
require_once(wpof_path . "/class/class-document.php");
require_once(wpof_path . "/class/class-upload.php");
require_once(wpof_path . "/class/class-session-stagiaire.php");
require_once(wpof_path . "/class/class-quiz.php");
require_once(wpof_path . "/class/class-creneau.php");
require_once(wpof_path . "/class/class-token.php");
require_once(wpof_path . "/class/class-board-toc.php");
require_once(wpof_path . "/class/class-qmanager.php");


$type_emargement_text = array
(
    'jour' => __("par jour"),
    'tous' => __("tous les stagiaires inscrits à la session"),
    'vide' => __("feuilles vierges"),
    'stagiaire' => __("par stagiaire"),
);

$global_log = array();

#[AllowDynamicProperties]
class SessionFormation
{
    public $numero = ""; // nomenclature interne
    public $titre_session = "";
    public $titre_formation = "";
    public $session_unique_titre = "";
    public $permalien = "";
    public $slug = "";
    public $bandeau_id = "-1"; // image mise en avant
    public $acces_session;
    public $completion = "";
    
    public $nature_formation = "form";
    public $nature_url;
    public $specialite;
    
    // tableau d'ID des formateurs
    public $formateur = array();
    
    // Infos venant de la formation catalogue
    public $duree = 0;
    public $tarif_inter = 0;
    public $tarif_intra = 0;
    
    // infos effectives pour cette session
    public $tarif_heure = 0;
    public $nb_heure; // au format horaire : 3:30
    public $nb_heure_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    public $depassement_duree = 0; // si = 0, c'est bon, sinon, indique le dépassement horaire
    public $nb_jour = "";
    public $tarif_base_total = false; // détermine si le tarif de base défini pour le total (true) ou à l'heure (false)
    public $tarif_total_chiffre = 0;
    public $tarif_total_lettre = 0;
    public $budget_global = 0; // montant de la facture ou total des factures en inter
    public $exe_comptable = array(); // les clés sont la ou les années, les valeurs sont le ou les budgets de l'année concernée
    
    public $quizpr = "";
    public $quizobj = "";
    public $quizpr_id = "";
    public $quizobj_id = "";
    
    public $dates_array = array();
    public $dates_texte = "";
    public $first_date = "";
    public $first_date_timestamp = 0;
    public $last_date_timestamp = 0;
    public $modalite = "";
    
    // vaut 1 si la session a bien été réalisée
    public $realisee = 0;
    
    public $type_emargement = array();
    
    // type de session
    public $type_index = "";
    public $type_texte = "";
    
    // Objet lieu
    public $lieu = -1;
    public $lieu_nom = "";
    public $lieu_adresse = "";
    public $lieu_code_postal = "";
    public $lieu_ville = "";
    public $lieu_localisation = "";
    public $lieu_secu_erp = "";
    
    // Clients
    public $clients = array();
    
    // stagiaires
    public $stagiaires_min = 0;
    public $stagiaires_max = 0;
    public $inscrits = array();
    public $stagiaires = array(); // tableau de SessionStagiaire
    public $nb_stagiaires = 0;
    public $nb_confirmes = 0;
    
    // documents
    public $doc_suffix;
    public $doc_necessaire = array(); // identifiant des docs globaux nécessaires pour cette session
    public $uploads = array(); // tableau des documents scannés ou extérieurs, attachés à cette session
    
    public $id;
    
    public $date_modif = "";
    
    // ID de la formation en doublon pour assurer la compatibilité
    public $formation_id = -1;
    public $formation = -1;
    
    public $creneaux = array();
    public $nb_creneaux = 0;
    public $wp_post = null;
    
    public function __construct($session_id = -1)
    {
        global $Formation;
        global $wpof;
        
        $this->tarif_inter = $wpof->tarif_inter;
        
//        log_add(__METHOD__."($session_id)", "session");
        
        /*
        echo '<div style="border: 1px red solid"><p>'.$session_id.'</p><pre>';
        debug_print_backtrace(0, 3);
        echo '</pre></div>';
        */
        
        if ($session_id > 0)
        {
            global $SessionFormation;
            $SessionFormation[$session_id] = $this;
            
            $this->id = $session_id;
            $wp_post = get_post($session_id);
            if ($wp_post === null)
                return null;
            else
                $this->wp_post = $wp_post;
            $meta = get_post_meta($session_id);
            
            // date de dernière modif
            if (isset($meta['timestamp_modif'][0]))
            {
                $this->timestamp_modif = $meta['timestamp_modif'][0];
                $this->date_modif = date_i18n("j F Y", $this->timestamp_modif);
            }
            
            // caractéristiques de la formation
            $this->specialite = (isset($meta['specialite'][0])) ? $meta['specialite'][0] : "";
            $this->nature_formation = (isset($meta['nature_formation'][0])) ? $meta['nature_formation'][0] : "";
            
            // la session a été réalisée
            $this->realisee = (isset($meta['realisee'][0])) ? $meta['realisee'][0] : "";
            
            foreach($wpof->desc_formation->term as $k => $t)
                $this->$k = (isset($meta[$k][0])) ? $meta[$k][0] : "";
            
            // infos issues des meta données du post
            if (isset($meta['formation'][0]) && $meta['formation'][0] > 0)
            {
                $this->formation_id = $this->formation = $meta['formation'][0];
                
                $formation = get_formation_by_id($this->formation);
                //echo $formation->titre;
                
                foreach($wpof->desc_formation->term as $k => $t)
                {
                    if ($wpof->{"formation_".$k."_mode"} == 'force')
                        $this->$k = $wpof->{"formation_".$k."_text"};
                    elseif ($this->$k == "")
                        $this->$k = $formation->$k;
                }
                        
                if ($this->titre_formation == "")
                    $this->titre_formation = $formation->titre;

                //$this->tarif_inter = $formation->tarif;
                $this->titre_formation = $formation->titre;
                
                if (empty($this->date_modif))
                    $this->date_modif = $formation->date_modif;
                
                // nature session de formation (objectif de la prestation)
                $this->nature_formation = $formation->nature_formation;
                $this->nature_url = $formation->nature_url;
                $this->specialite = $formation->specialite;
            }
            if (isset($meta['session_unique_titre'][0]) && $meta['session_unique_titre'][0] != "")
                $this->session_unique_titre = $this->titre_formation = $meta['session_unique_titre'][0];
                
            // récupération des bons identifiants de quiz et du parent
            if (isset($meta['quizpr_id'][0]))
            {
                $this->quizpr_id = $meta['quizpr_id'][0];
                $this->quizpr_parent_id = (isset($meta['quizpr_parent_id'][0])) ? $meta['quizpr_parent_id'][0] : $this->id;
                $this->quizpr = new Quiz($this->quizpr_id);
            }
            
            if (isset($meta['quizobj_id'][0]))
            {
                $this->quizobj_id = $meta['quizobj_id'][0];
                $this->quizobj_parent_id = (isset($meta['quizobj_parent_id'][0])) ? $meta['quizobj_parent_id'][0] : $this->id;
                $this->quizobj = new Quiz($this->quizobj_id);
            }

            $this->formateur = get_post_meta($session_id, "formateur", true); // permet de récupérer l'info sous forme de tableau
            if (!is_array($this->formateur))
                $this->formateur = array();
            
            // Visibilité de la session
            $this->visibilite_session = (isset($meta['visibilite_session'][0])) ? $meta['visibilite_session'][0] : 'public';
            $this->acces_session = (isset($meta['acces_session'][0])) ? ($meta['acces_session'][0]) : $this->visibilite_session;
            $this->completion = (isset($meta['completion'][0])) ? $meta['completion'][0] : "";

            // lieu
            $lieu = null;
            if (isset($meta['lieu'][0]))
            {
                $this->lieu = $meta['lieu'][0];
                $lieu = get_lieu_by_id($this->lieu);
            }
                
            foreach(array("nom", "adresse", "code_postal", "ville", "localisation", "secu_erp") as $data)
            {
                $lieu_data = 'lieu_'.$data;
                if ($lieu && isset($lieu->$data))
                    $this->$lieu_data = $lieu->$data;
                elseif (isset($meta[$lieu_data][0]))
                    $this->$lieu_data = $meta[$lieu_data][0];
            }
            $this->ville = $this->lieu_ville;
            
            // stagiaires
            $this->clients = get_post_meta($session_id, 'clients', true); // tableau
            if (!is_array($this->clients))
                $this->clients = array();
            else
            {
                // on inscrit le numéro du premier client en tant que numéro de session pour classer par ce critère
                if (champ_additionnel('numero_contrat'))
                    $this->numero = get_client_meta(reset($this->clients), "numero_contrat");
            }
            
            $this->stagiaires_min = (isset($meta['stagiaires_min'][0])) ? $meta['stagiaires_min'][0] : 0;
            $this->stagiaires_max = (isset($meta['stagiaires_max'][0])) ? $meta['stagiaires_max'][0] : 0;
            $this->inscrits = get_post_meta($session_id, 'inscrits', true); // tableau
            if (!is_array($this->inscrits))
                $this->inscrits = array();
            
            // indication de durée
            if (champ_additionnel('duree_jour') && isset($meta['nb_jour'][0]))
                $this->nb_jour = $meta['nb_jour'][0];
            
            // types de feuilles d'émargement
            $this->type_emargement = get_post_meta($session_id, 'type_emargement', true);
            if ($this->type_emargement == "")
            {
                global $type_emargement_text;
                $this->type_emargement = array_fill_keys(array_keys($type_emargement_text), 0);
                $this->type_emargement['jour'] = 1;
            }
            
            // documents nécessaires
            foreach($wpof->documents->term as $doc_index => $doc)
            {
                if ($doc->contexte & $wpof->doc_context->session)
                    $this->doc_necessaire[] = $doc_index;
            }
            $this->doc_suffix = $this->id;
            
            /*
            if ($this->type_index != "")
            {
                global $doc_nom;
                foreach (array_keys($doc_nom) as $doc_index)
                {
                    if ($doc_index == "pv_secu" && ($lieu) && $lieu->secu_erp == "")
                        continue;

                    if ($wpof->{$doc_index."_global_".$this->type_index} == 1)
                        $this->doc_necessaire[] = $doc_index;
                }
            }
            */
            
            // scans et cie
            $this->uploads = unserialize(get_post_meta($this->id, "uploads", true));
            if (!is_array($this->uploads))
                $this->uploads = array();
            
            $this->init_dates_creneaux();
            $this->init_modalite();
            
            if (!empty($meta['nb_heure_estime_decimal'][0]))
                $this->nb_heure_estime_decimal = $meta['nb_heure_estime_decimal'][0];
            
            $this->calcule_temps_session();
            
            // renseignement du lieu si pas de lieu défini et que seuls des créneaux à distance sont définis
            $this->init_lieu_by_creneaux();

            
            if (isset($meta['tarif_total_chiffre'][0]))
            {
                $this->tarif_total_chiffre = (float) $meta['tarif_total_chiffre'][0];
                $this->calcule_tarif();
            }
            
            // infos issues du post
            $this->titre_session = (!empty($wp_post->post_title)) ? $wp_post->post_title : __('Pas de titre !');
            
            // Vérification de la conformité du post_title
            //$this->check_post_title($wp_post);
            
            $this->permalien = get_the_permalink($session_id);
            $this->slug = (!empty($wp_post->post_name)) ? $wp_post->post_name : "";
        }
        else
        {
            foreach(array_keys($wpof->desc_formation->term) as $k)
                $this->$k = "";
        }
    }
    
    /*
     * Qui peut modifier cette session ?
     * Renvoie true si
     * $user_id a le rôle responsable ou admin
     * $user_id fait partie des formateurs animant cette session ($this->formateur)
     * $this->formateur est vide (session non définie)
     */
    public function can_edit($user_id = -1)
    {
        global $wpof;
        
        if ($user_id == -1)
            $user_id = get_current_user_id();
        
        $role = wpof_get_role($user_id);
        $super_roles = array("admin", "um_responsable");
        
        if ($role == "um_stagiaire" || $role == null)
            return false;
        
        if (!isset($this->formateur)
            || !is_array($this->formateur)
            || count($this->formateur) == 0
            || in_array($user_id, $this->formateur)
            || in_array($role, $super_roles))
            return true;
        
        return false;
    }
    
    /*
     * Création des sessions stagiaires
     * En effet, on n'a pas toujours besoin des sessions stagiaires lorsqu'on instancie une session formation
     */
    public function init_stagiaires()
    {
        global $Client;
        $this->nb_stagiaires = $this->nb_confirmes = 0;
        
        if (!isset($Client[reset($this->clients)]))
            $this->init_clients();
        foreach($this->clients as $c)
        {
            if (isset($Client[$c]))
            {
                $this->nb_stagiaires += (integer) $Client[$c]->nb_stagiaires;
                $this->nb_confirmes += (integer) $Client[$c]->nb_confirmes;
            }
            else
                log_add($this->titre_session." : client inconnu ".$c);
        }
    }
    
    public function init_clients($client_id = -1)
    {
        global $Client;
        
        if ($client_id > 0)
            $client_list = array($client_id);
        else
            $client_list = $this->clients;
        
        foreach($client_list as $cid)
            if (!isset($Client[(integer)$cid]))
                $Client[(integer)$cid] = new Client($this->id, $cid);
            
        $this->calcule_budget_global();
    }
    
    /*
     * Initialisation des dates et des créneaux
     */
    public function init_dates_creneaux()
    {
        $tmp_creneaux = get_post_meta($this->id, "creneaux", true);
        
        if (!is_array($tmp_creneaux) || empty($tmp_creneaux))
            $this->dates_array = $this->creneaux = array();
        else
        {
            $update = false;
            foreach($tmp_creneaux as $date => $jour_creneaux)
            {
                if (!isset($this->creneaux[$date]))
                    $this->creneaux[$date] = array();
                if (is_array($jour_creneaux) && count($jour_creneaux) > 0)
                    foreach ($jour_creneaux as $c)
                    {
                        $creno = new Creneau($c);
                        if (!empty($creno->date))
                        {
                            if ($creno->lieu_id == -1)
                                $creno->lieu_nom = $this->lieu_nom;
                            $this->creneaux[$date][$c] = $creno;
                            $this->nb_creneaux++;
                        }
                        else
                        {
                            $update = true;
                        }
                    }
            }
            if ($update)
            {
                $this->update_creneaux();
            }
            $this->dates_array = array_keys($this->creneaux);
            $this->dates_texte = pretty_print_dates($this->dates_array);
            $this->first_date = $this->dates_array[0];
            $this->first_date_timestamp = date_create_from_format("d/m/Y", $this->first_date)->getTimestamp();
            $this->last_date = end($this->dates_array);
            $this->last_date_timestamp = date_create_from_format("d/m/Y", $this->last_date)->getTimestamp();
        }
    }
    
    private function init_lieu_by_creneaux()
    {
        if ($this->lieu_ville == "")
        {
            $type_creneau = array();
            foreach($this->creneaux as $date)
                foreach($date as $c)
                    $type_creneau[$c->type] = 1;
            
            if (!empty($type_creneau) && !isset($type_creneau['presentiel']) && !isset($type_creneau['afest']))
                $this->lieu_nom = __("distance");
        }
    }
    
    /**
     * Calcule la modalité de session
     */
    private function init_modalite()
    {
        global $wpof;
        if ($this->is_a_distance(true))
            $this->modalite = 'dist';
        elseif ($this->is_a_distance(false))
            $this->modalite = 'mixte';
        else
            $this->modalite = 'pres';
    }
    public function get_displayname($link = false)
    {
        if ($link)
            return $this->get_a_link();
        else
            return $this->titre_session;
    }
    
    public function get_a_link($text = null)
    {
        if ($text === null)
            $text = $this->titre_session;
        return '<a href="'.$this->permalien.'" title="'.strip_tags($this->titre_session).'">'.$text.'</a>';
    }
    
    public function get_id_link()
    {
        return $this->get_a_link($this->id);
    }
    
    /**
     * Fixe la visibilité à prive si pas de formateur⋅ice public
     * @return true si changement d'état
     */
    public function hide_if_no_formateur()
    {
        if (($this->visibilite_session == 'public' || $this->acces_session == 'public') && empty($this->get_public_formateurs_id()))
        {
            $this->update_meta("visibilite_session", 'invite');
            $this->update_meta("acces_session", 'invite');
            return true;
        }
        return false;
    }
    
    /**
     * Renvoie la liste des formateur⋅ices dont le profil n'est pas archivé
     */
    public function get_public_formateurs_id()
    {
        $public_formateurs_id = array();
        foreach($this->formateur as $fid)
            if (get_user_meta($fid, "archive", true) != 1)
                $public_formateurs_id[] = $fid;
        
        return $public_formateurs_id;
    }
    
    public function get_client_by_stagiaire($stagiaire_id)
    {
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            if (in_array($stagiaire_id, $client->stagiaires))
                return $client;
        }
        return null;
    }
    
    public function init_slug($update = true)
    {
        $wp_post = get_post($this->id);
        
        $actuel_titre_session = strip_tags($this->titre_formation);
        if (empty($this->creneaux))
            $slug = sanitize_title($actuel_titre_session).'-'.date("Ymd-His");
        else
            $slug = sanitize_title($actuel_titre_session.'-'.array_key_first($this->creneaux));
        
        if ($update === true)
        {
            $new_data = array ('ID' => $this->id, 'post_name' => $slug);
            remove_action( 'post_updated', 'wp_save_post_revision' );
            $res = wp_update_post(wp_slash($new_data), true);
            add_action( 'post_updated', 'wp_save_post_revision' );
        }
        $this->slug = $slug;
    }
    
    public function check_post_title($wp_post = null)
    {
        if (!$wp_post)
            $wp_post = get_post($this->id);
        
        $actuel_titre_session = strip_tags($this->titre_formation);
        
        $this->init_slug(false);

        if ($actuel_titre_session != $this->titre_session)
        {
            $new_data = array
            (
                'ID' => $this->id,
                'post_title' => $actuel_titre_session,
                'post_name' => $this->slug,
            );
            remove_action( 'post_updated', 'wp_save_post_revision' );
            $res = wp_update_post(wp_slash($new_data), true);
            add_action( 'post_updated', 'wp_save_post_revision' );
            if ($res != $this->id)
                var_dump($res);
            $this->titre_session = $new_data['post_title'];
            
            return true;
        }
        return false;
    }

    /**
     * Vérifie si les stagiaires ont bien un email renseigné.
     * 
     * @return Array tableau d'id des stagiaires n'ayant pas d'email
     */
    public function check_missing_email_stagiaires(int $client_id = null) : Array
    {
        global $SessionStagiaire, $Client;

        $missing = [];

        $clients = ($client_id === null) ? $this->clients : [ $client_id ];

        foreach($clients as $cid)
        {
            foreach($Client[$cid]->stagiaires as $sid)
            {
                if (empty($SessionStagiaire[$sid]->email))
                    $missing[$sid] = $SessionStagiaire[$sid]->get_displayname();
            }
        }

        return $missing;
    }
    
    /*
     * Surcharge de update_post_meta
     * Peut être appelée dans un contexte où l'on veut mettre à jour une donnée sans se soucier de savoir
     * si l'on est sur une SessionFormation ou une SessionStagiaire
     */
    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpof;
        
        if ($meta_value == "delete_meta")
            return $this->delete_meta($meta_key);
        
        $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key, $meta_value);
        
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if ($meta_key == "creneaux")
            $meta_value = $this->update_creneaux();
            
        if (in_array($meta_key, array_keys($wpof->desc_formation->term)))
        {
            wp_update_post(array('ID' => $this->id));
            update_post_meta($this->id, "timestamp_modif", time());
        }
        $result = update_post_meta($this->id, $meta_key, $meta_value);
        
        if (isset($_SESSION['Session'][$this->id]))
            $_SESSION['Session'][$this->id]->$meta_key = $meta_value;
        
        $this->set_session_completion();
        update_post_meta($this->id, "completion", $this->completion);
        
        return $result;
    }
    
    public function delete_meta($meta_key)
    {
        global $wpof;
        if (in_array($meta_key, array_keys($wpof->desc_formation->term)))
        {
            $wpof->log->log_action(__FUNCTION__, $this, $meta_key, $this->$meta_key);
            return delete_post_meta($this->id, $meta_key);
        }
        else
            return false;
    }
    
    // Cas particulier des créneaux où le stocke que les index
    private function update_creneaux()
    {
        global $wpof;

        // Tri des créneaux dans l'ordre chronologique par heure de fin
        $this->creneaux = sort_creneaux($this->creneaux, 'fin');

        // initialisation de tmp_creneaux avec les dates triées en clés
        $tmp_creneaux = array_fill_keys(sort_dates(array_keys($this->creneaux)), array());
        
        foreach($this->creneaux as $date => $jour_creneaux)
        {
            if (is_array($jour_creneaux) && count($jour_creneaux) > 0)
            {
                foreach($jour_creneaux as $c)
                {
                    $tmp_creneaux[$date][] = $c->id;
                }
            }
            else
                $tmp_creneaux[$date] = array();
        }
        
        $this->dates_array = array_keys($tmp_creneaux);
        $this->first_date = $this->dates_array[0];
        $this->first_date_timestamp = date_create_from_format("d/m/Y", $this->first_date)->getTimestamp();
        $this->last_date = end($this->dates_array);
        $this->last_date_timestamp = date_create_from_format("d/m/Y", $this->last_date)->getTimestamp();
        update_post_meta($this->id, "last_date_timestamp", $this->last_date_timestamp);
        update_post_meta($this->id, "first_date_timestamp", $this->first_date_timestamp);
        $wpof->log->log_action("update_meta", $this, "creneaux", get_post_meta($this->id, "creneaux", true), serialize($tmp_creneaux));
        
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            $client->update_meta('creneaux');
        }
        
        return $tmp_creneaux;
    }
    
    public function delete()
    {
        global $wpof;
        
        $log = array();
        // suppression des clients et stagiaires
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            $client->delete();
            
            $log[] = "Suppression de ".$client->nom;
        }
        
        global $wpdb;
        
        // suppression des créneaux
        global $suffix_creneaux;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_creneaux." WHERE session_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des quiz
        global $suffix_quiz;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_quiz." WHERE parent_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des documents
        global $suffix_documents;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_documents." WHERE session_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des metas
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix."postmeta WHERE post_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression du post (sans passer par la case poubelle)
        wp_delete_post($this->id, true);
        
        $wpof->log->log_action(__FUNCTION__, $this);
        
        // suppression de la session en mémoire
        if (isset($_SESSION['Session'][$this->id]))
            unset($_SESSION['Session'][$this->id]);
        
        return $log;
    }

    public function get_delete_bouton($texte_bouton = "Supprimer cette session", $reload = true, $parent_to_kill = null)
    {
        ob_start();
        ?>
        <div class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-reload="<?php echo $reload; ?>" <?php if ($parent_to_kill !== null) : ?> data-parent="<?php echo $parent_to_kill; ?>"<?php endif;?>>
            <?php the_wpof_fa_del(); ?>
            <?php echo $texte_bouton; ?>
        </div>
        <?php
        return ob_get_clean();
    }

    /*
     * renvoie la liste des stagiaires (prénom nom) sous forme de liste
     * , : séparés par des virgules
     * liste : liste à puce li sans les balises ul ou ol, à rajouter autour de cet appel
     * tableau : tableau tr td sans les balises table, à rajouter autour de cet appel
     * En fait, seule la première lettre est analysée, donc souplesse
     * 
     * Si $confirme est vrai, on ne sort que les stagiaires confirmés
     */ 
    public function get_liste_stagiaires($type_liste = ',', $confirme = false)
    {
        $liste = "";
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            $liste .= $client->get_liste_stagiaires($type_liste, $confirme);
        }
        if ($liste != "")
        {
            switch(substr($type_liste, 0, 1))
            {
                case 't':
                    $liste = '<table class="list_tab">'.$liste.'</table>';
                    break;
                case 'l':
                    $liste = "<ul>".$liste."</ul>";
                    break;
                case ',':
                default:
                    break;
            }
        }
        return $liste;
    }
    
    public function get_formateurs_noms(String $format = 'string', bool $link = false) : String|Array
    {
        $formateurs_nom = array();
        foreach($this->formateur as $f_id)
            $formateurs_nom[$f_id] = get_displayname($f_id, $link);
        if ($format == 'string')
            return implode(', ', $formateurs_nom);
        else
            return $formateurs_nom;
    }
    
    
    // Crée les inputs pour définir la répartition par exercire comptable
    public function get_input_exe_comptable($exe_comptable = null)
    {
        if (!$exe_comptable) $exe_comptable = $this->exe_comptable;
        
        $html = "<div class='exe_comptable'>";
        $rand = rand();
        
        foreach($exe_comptable as $annee => $tarif)
        {
            $html .= "<label for='".$annee.$rand."'>$annee</label>";
            $html .= "<input id='".$annee.$rand."' name='$annee' type='number' step='0.01' value='$tarif' />";
        }
        $html .= "</div>";
        
        return $html;
    }
    
    
    // Renseigne le tableau des exercices comptables
    // En général ce tableau n'aura qu'une valeur (si toute la session se déroule dans la même année civile)
    public function set_exercice_comptable()
    {
        if ($this->type_index != 'inter' && !empty($this->dates_array))
        {
            $this->exe_comptable = get_post_meta($this->id, "exe_comptable", true);
            
            if (!is_array($this->exe_comptable) || count($this->exe_comptable) == 0 || in_array("", array_keys($this->exe_comptable)))
            {
                $this->exe_comptable = array();
                foreach($this->dates_array as $d)
                {
                    $date = explode('/', $d);
                    $this->exe_comptable[$date[2]] = 0;
                }
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] = $this->tarif_total_chiffre;
                $this->update_meta("exe_comptable");
            }
            $diff = $this->tarif_total_chiffre - array_sum($this->exe_comptable);
            if ($diff != 0)
            {
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] += $diff;
                
                $this->update_meta("exe_comptable");
            }
        }
    }
    
    // Calcule le temps (en heures) de la session
    public function calcule_temps_session()
    {
        global $wpof;
        $this->temps = DateTime::createFromFormat("U", "0");
            
        foreach($this->creneaux as $tab_date)
        {
            if (!empty($tab_date))
            {
                foreach($tab_date as $creno)
                {
                    if ($creno->type != "technique")
                        $this->temps->add($creno->duree);
                }
                $h = floor($this->temps->format("U") / 3600);
                $m = ($this->temps->format("U") % 3600) / 60;
                $this->nb_heure_decimal = $this->temps->format("U") / 3600;
                $this->nb_heure = sprintf("%02d:%02d", $h, $m);
            }
        }
        
        $temps_client = 0;
        foreach($this->clients as $cid)
            $temps_client = max($temps_client, get_client_meta($cid, "nb_heure_estime_decimal"));
            
        if ($this->nb_heure_decimal < $temps_client)
            $this->nb_heure_decimal = $temps_client;
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        if (!isset($this->nb_heure_estime_decimal))
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
        
        $wpof->max_duree_bc = (integer) $wpof->max_duree_bc;
        if ($this->nature_formation == "bilan" && $this->nb_heure_estime_decimal > $wpof->max_duree_bc)
            $this->depassement_duree = $this->nb_heure_estime_decimal - $wpof->max_duree_bc;
    }    

    // Calcule le tarif total affiché
    public function calcule_tarif()
    {
        if ($this->nb_heure_decimal > 0)
            $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
    }
            
    public function calcule_budget_global()
    {
        global $Client;
        $this->budget_global = 0;
        foreach ($this->clients as $cid)
            $this->budget_global += get_client_meta($cid, "tarif_total_chiffre");
    }
    
    /**
     * Renvoie le timestamp de l'heure de fin du dernier créneau.
     * Si pas de créneau on renvoie le timestamp actuel + 30 jours
     * @return int timestamp
     */
    public function get_end_timestamp() : int
    {
        $timestamp = time() + 30 * 24 * 60 * 60;
        if (!empty($this->creneaux))
        {
            $last_date = end($this->creneaux);
            if (!empty($last_date))
            {
                $last_creno = end($last_date);
                $timestamp = $last_creno->fin->getTimestamp();
            }
            else
            {
                $date = DateTime::createFromFormat("d/m/Y H:i", array_key_last($this->creneaux)." 16:00");
                $timestamp = $date->getTimestamp();
            }
        }
        return $timestamp;
    }

    /**
     * Vérifie si la session est terminée. Compare l'heure actuelle avec l'heure de fin du dernier créneau
     * @return true si la session est finie (à la seconde près), false sinon
     */
    public function is_session_ended(): bool
    {
        return time() - $this->get_end_timestamp() > 0;
    }

    // Supprime un élément d'un tableau de sous-entité (clients, inscrits)
    public function supprime_sous_entite($tab_name, $id)
    {
        if (isset($this->$tab_name) && is_array($this->$tab_name))
        {
            $key = array_search($id, $this->$tab_name);
            unset($this->$tab_name[$key]);
            $this->update_meta($tab_name, $this->$tab_name);
            if (isset($_SESSION['Session'][$this->id]))
                $_SESSION['Session'][$this->id]->$tab_name = $this->$tab_name;
        }
    }
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session formation
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;
        
        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->id, $wpof->doc_context->session);
            $Documents[$doc->id] = $doc;
        }
    }
    
    /*
     * Création de tous les documents : session, client, stagiaire
     */
    public function init_all_docs()
    {
        global $Documents;
        $this->init_docs();
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            $client->init_docs();
            if ($client->entite_client == Client::ENTITE_PERS_MORALE)
                foreach($client->stagiaires as $sid)
                {
                    $stagiaire = get_stagiaire_by_id($this->id, $sid);
                    $stagiaire->init_docs();
                }
        }
    }
    
    public function get_clients($meta = "nom", $format = ',')
    {
        $html = array();
        
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            if ($client->financement == Client::FINANCEMENT_OPAC)
                $class = "sous-traitance";
            else
                $class = "autre";
            $html[] = "<span class='$class'>".$client->$meta."</span>";
        }
        
        switch (substr($format, 0, 1))
        {
            case 'l':
                $html = "<ul><li>".join("</li><li>", $html)."</li></ul>";
                break;
            case 't':
                $html = "<table><tr><td>".join("</td><td>", $html)."</td></tr></table>";
                break;
            case 'b':
                $html = join('<br />', $html);
                break;
            default:
                $html = join(', ', $html);
                break;
        }
        return $html;
    }

    /*
     * Calcule le taux de complétion d'une session
     * Renvoie ce taux de complétion en float
     */
    public function set_session_completion()
    {
        global $wpof;
        
        $total = count($wpof->completion_session[$this->acces_session]);
        $rempli = 0;
        foreach($wpof->completion_session[$this->acces_session] as $k)
            if (!empty($this->$k) && trim($this->$k) != "")
                $rempli++;
        
        $this->completion = $rempli."/".$total;
        return $rempli / $total;
    }
    
    /*
     * Retourne la liste des champs vides à compléter
     */
    public function get_completion_missing()
    {
        global $wpof;
        
        $vide = array();
        foreach($wpof->completion_session[$this->acces_session] as $k)
            if (empty(trim($this->$k)))
            {
                if ($wpof->desc_formation->is_term($k))
                    $vide[] = $wpof->desc_formation->get_term($k);
                else
                    $vide[] = $k;
            }
        
        return $vide;
    }
    
    /*
     * Retourne le taux de complétion de la session sous la forme :
     * nb_champs_remplis / nb_champs_a_repmlir
     */
    public function get_session_completion($html = true)
    {
        if ($html)
        {
            $completion_rate = explode('/', $this->completion);
            $class = ($completion_rate[0] == $completion_rate[1]) ? "fait" : "attention pointer";
            return '<span class="'.$class.' completion">'.$this->completion.'</span>';
        }
        else
            return $this->completion;
    }
    
    public static function get_session_control_head($tempo = "futur")
    {
        global $wpof;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr>
        <th><?php _e("Titre session"); ?></th>
        <?php if (in_array($role, $wpof->super_roles)) : ?>
        <th class="thin"><?php _e("ID"); ?></th>
        <th><?php _e("Formateur⋅trice"); ?></th>
        <th><?php _e("Date création"); ?></th>
        <th><?php _e("Créée par"); ?></th>
        <th><?php _e("Dernière modification"); ?></th>
        <?php endif; ?>
        <th><?php _e("Nature"); echo get_icone_aide("formation_nature_formation"); ?></th>
        <th id="order" data-order="desc"><?php _e("Premier jour"); ?></th>
        <th><?php _e("Dernier jour"); ?></th>
        <th class='thin'><?php _e("Complétion description"); echo get_icone_aide("sessionformation_completion_description"); ?></th>
        <th><?php _e("Clients"); ?><br /><?php _e('M : pers. morale / P : pers. physique / S : sous-traitance'); ?></th>
        <th><?php _e("Visibilité agenda"); echo get_icone_aide("sessionformation_acces_session"); ?></th>
        <?php if ($tempo == 'passe') : ?>
        <th class='thin'><?php _e("Réalisée"); echo get_icone_aide("sessionformation_realisee"); ?></th>
        <?php endif; ?>
        <th class='thin not_orderable'><?php _e("Supprimer"); echo get_icone_aide("sessionformation_supprimer"); ?></th>
        </tr>
        <?php
        return ob_get_clean();
    }
    /*
     * Renvoie une ligne de tableau pour contrôler la session
     * 
     */
    public function get_session_control($tempo = "futur")
    {
        global $wpof, $Formation;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr id="session<?php echo $this->id; ?>" data-context="session" class="editable">
        <td <?php if ($this->formation_id > 0) : ?> class="catalogue" title="<?php echo __("Basée sur ").get_the_title($this->formation_id); ?>" <?php endif; ?>>
        <a href='<?php echo $this->permalien; ?>'><?php echo $this->titre_session; ?></a>
        </td>
        <?php if (in_array($role, $wpof->super_roles)) : ?>
        <td><?php echo $this->id; ?></td>
        <td><?php echo $this->get_formateurs_noms(); // init_term_list("formateur"); echo get_input_jpost($this, 'formateur', array('select' => 'multiple', 'aide' => false)); ?></td>
        <td><?php echo $this->wp_post->post_date; ?></td>
        <td><?php echo get_displayname($this->wp_post->post_author); ?></td>
        <td><?php echo $this->wp_post->post_modified; ?></td>
        <?php endif; ?>
        <td> <?php echo get_input_jpost($this, "nature_formation", array('select' => 1, 'aide' => false, 'readonly' => $this->formation_id > 0)); ?></td>
        <td><span class="hidden"><?php echo $this->first_date_timestamp; ?></span><?php echo $this->first_date; ?></td>
        <td><span class="hidden"><?php echo $this->last_date_timestamp; ?></span><?php echo end($this->dates_array); ?></td>
        <td class="center"><?php echo $this->get_session_completion(true); ?></td>
        <td class="liste-client">
            <ul>
            <?php
            $nb_stagiaires = 0;
            foreach ($this->clients as $cid)
            {
                $client = get_client_by_id($this->id, $cid);
                $class = 'morale';
                if ($client->entite_client == Client::ENTITE_PERS_PHYSIQUE)
                    $class = 'physique';
                elseif ($client->financement == Client::FINANCEMENT_OPAC)
                    $class = 'sous-traitance';
                
                $client->init_stagiaires();
                echo "<li class='$class'>";
                echo $client->get_displayname(true);
                if (!empty($client->numero_contrat))
                    echo ' <span title="'.__("Numéro de contrat").'">('.$client->numero_contrat.')';
                if ($client->financement != Client::FINANCEMENT_OPAC && $client->entite_client == Client::ENTITE_PERS_MORALE)
                {
                    $ss = ($client->nb_confirmes > 1) ? "s" : "";
                    echo " – ".sprintf(__("%d stagiaire%s confirmé%s / %d"), $client->nb_confirmes, $ss, $ss, $client->nb_stagiaires);
                }
                elseif ($client->financement == "opac")
                {
                    $ss = ($client->nb_stagiaires > 1) ? "s" : "";
                    echo " – ".sprintf(__("%d stagiaire%s"), $client->nb_stagiaires, $ss);
                }
                    
                echo "</li>";
                $nb_stagiaires += $client->nb_confirmes;
            }
            /*
            echo $this->get_clients("nom", "l");
            */
            $nb_clients = count($this->clients);
            $sc = ($nb_clients > 1) ? "s" : "";
            $ss = ($nb_stagiaires > 1) ? "s" : "";
            echo "<p class='right'>$nb_clients ".__("client").$sc." / $nb_stagiaires ".__('stagiaire').$ss."</p>";
            ?>
            </ul>
        </td>
        <td><?php echo get_input_jpost($this, "acces_session", array('select' => '', 'aide' => false));  ?></td>
        <?php if ($tempo == 'passe') : ?>
            <td class="center">
            <?php 
                if ($this->realisee == 1 && !in_array($role, array("um_responsable", "admin")))
                    the_wpof_fa_ok('succes');
                else
                    echo get_input_jpost($this, "realisee", array('input' => 'checkbox', 'display' => 'inline', 'aide' => false));
            ?>
            </td>
        <?php endif; ?>
        <td class="center">
        <div class="delete-entity icone-bouton" data-objectclass="SessionFormation" data-id="<?php echo $this->id; ?>" data-parent="tr#session<?php echo $this->id; ?>" title="<?php _e("Supprimer"); ?>">
        <?php the_wpof_fa_del(); ?>
        </div>
        </td>
        </tr>
        <?php
        return ob_get_clean();
    }

    /**
     * Retourne les dates et le nombre de place disponibles pour une session. Pour être affiché en tant qu'excerpt au sens Wordpress pour type de contenu "Session".
     *
     * @return void
     * @see /wpof-custom-post-types.php
     */
    public function get_excerpt()
    {
        return '<p class="session-dates">' . $this->dates_texte . '</p>' . "<!-- more -->" . '<p class="session-places">' . $this->get_places_dispos() . '</p>';
    }
    
    /**
     * Retourne le contenu résumé de la session sous forme de vignette (article au sens WP) pour les listes publiques
     */
    public function get_item_html($heading = 'h3')
    {
        $inscription = (empty($this->first_date_timestamp) || $this->first_date_timestamp < time()) ? "" : __("S'inscrire");
        $heading_class = ($this->acces_session == 'public') ? 'public' : 'prive';
        
        $article = get_article('session-detail '.$heading_class, $heading, $this->permalien, $this->titre_formation, $this->first_date.' '.$this->get_excerpt(), $inscription);

        return apply_filters('opaga_session_item_html', $article, $this);
    }

    /**
     * Affiche le contenu d'une formation pour être affiché dans le "content" au sens Worpdress pour type de contenu "Session".
     * 
     * Si le visiteur n'est pas connecté c'est une vue publique de la session
     * Sinon c'est le tableau de bord pour éditer la session
     *
     * @return void
     * @see /wpof-custom-post-types.php
     */
    public function get_content()
    {
        global $wpof;
        
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        
        // Si le visiteur n'est pas connecté ou n'a pas le droit de modification, on affiche le template public
        if ($user_id == 0
            || ($role == "um_formateur-trice" && !in_array($user_id, $this->formateur))) : 
            echo $this->get_html_presentation();
        else : ?>
            <div id="session" class="<?php echo $role;?> session_box id session" data-id="<?php echo $this->id; ?>">
            
            <?php
                $this->init_clients();
                $this->init_stagiaires();
                $board = $this->get_the_board();
                
                echo $board['head'];
                echo $board['tabs'];
                //echo $board['toc'];
                echo $board['content'];
            ?>
            
            </div> <!-- session_box session -->
        <?php endif;
    }


    /*
     * Renvoie la présentation de la session
     * $args['complet'] : affiche tous les détails
     * $args['entite] : affiche les informations actualisées pour le client ou le stagiaire dans sa page privée
     */
    public function get_html_presentation($args = array('complet' => true, 'entite' => null))
    {
        global $wpof, $post;
        
        if (!isset($args['complet']))
            $args['complet'] = true;
        if (isset($args['entite']))
        {
            $object = $args['entite'];
            $object_class = get_class($object);
            if ($object_class == "Client")
                $client = $object;
            else
            {
                $stagiaire = $object;
                $client = get_client_by_id($stagiaire->session_formation_id, $stagiaire->client_id);
            }
        }
        else
            $object = null;
        
        ob_start();
        ?>
        <div class="session-global">
        <?php if ($args['complet']) : ?>
        <div class="session-side-content">
            <?php if ($object == null) : ?>
                <div class="boutons">
                    <?php if (!empty($this->nature_url)) : ?>
                    <div class="icone-bouton"><a href="<?php echo $this->nature_url; ?>"><?php echo $wpof->nature_formation->get_term($this->nature_formation)." : ".__("en savoir plus"); ?></a></div>
                    <?php endif; ?>
                    <div class="icone-bouton get-proposition-pdf">
                        <a href="/?download=proposition&s=<?php echo $this->id; ?>&ci=<?php echo $this->id; ?>&c=<?php echo $wpof->doc_context->session;?>">
                        <?php the_wpof_fa_pdf(); ?>
                        <?php _e("Détails de la formation"); ?>
                        </a>
                    </div>
                </div>
            <?php endif; ?>

            <p><small><?php printf(__("Programme édité le %s"), $this->date_modif); ?></small></p>

            <h2><?php _e("Durée"); ?></h2>
            <p><?php echo $this->nb_heure; ?></p>

            <?php if (isset($object) && $object_class != "SessionStagiaire") : ?>
                <h2><?php _e("Tarif"); ?></h2>
                <?php if (isset($object) && $object_class == "Client") : ?>
                    <p><?php echo get_tarif_formation($client->tarif_total_chiffre); ?></p>
                <?php endif; ?>
            <?php else : ?> 
                <h2><?php _e("Tarif"); ?></h2>
                <p><?php echo get_tarif_formation($this->tarif_total_chiffre)." ".__("(par stagiaire)"); ?></p>
            <?php endif; ?>
                
            <h2><?php _e("Équipe pédagogique"); ?></h2>
            <p><?php the_liste_formateur(array('only' => $this->formateur, 'type' => 'list'), true); ?></p>

            <h2><?php _e("Dates"); ?></h2>
            <p><?php echo $this->dates_texte; ?></p>
            <?php if ($object == null) : ?>
            <p><a href="#planning-session"><?php _e("Voir planning détaillé") ?></a></p>
            <?php endif; ?>

            <?php if ($this->stagiaires_min * $this->stagiaires_max != 0) : ?>
            <h2><?php _e("Nombre de stagiaires"); $this->init_stagiaires();?></h2>
                <p><?php if ($this->stagiaires_min > 0) echo __("minimum")." : ".$this->stagiaires_min; ?></p>
                <p><?php if ($this->stagiaires_max > 0) echo __("maximum")." : ".$this->stagiaires_max; ?></p>
                <?php if ($this->stagiaires_max > 0): ?><p><?php echo $this->get_places_dispos();?></p><?php endif; ?>
            <?php endif; ?>
            
            <?php if ($object != null): ?>
            <h2><?php _e("Nombre de stagiaires"); $this->init_stagiaires(); echo " ".$this->nb_stagiaires."/".$this->nb_confirmes; ?></h2>
                <p><strong><?php echo $client->nom; ?></strong> : <?php echo $client->nb_confirmes; ?></p>
                <?php if ($client->nb_confirmes < $this->nb_confirmes) : ?>
                <p><strong><?php _e("Autres") ?></strong> : <?php echo $this->nb_confirmes - $client->nb_confirmes; ?></p>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($this->lieu_ville != "") : ?>
                <h2><?php _e("Lieu"); ?></h2>
                <p><?php echo $this->lieu_nom; ?></p>
                <p><?php echo $this->lieu_adresse; ?></p>
                <p><?php echo $this->lieu_ville; ?></p>
            <?php endif; ?>
            <?php if ($object == null) : ?>
            <h2><?php _e("Vous êtes client et/ou stagiaire sur cette session ? "); ?></h2>
            <p>
            <a class="dynamic-dialog" data-function="new_token_form" data-sessionid="<?php echo $this->id; ?>">
                <?php the_wpof_fa('square-up-right'); ?>
                <?php _e("Obtenez un lien d'accès à vos informations"); ?>
            </a>
            </p>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        
        <div class='session-content'>
        <?php if ($args['complet']) : ?>
        <?php
            $desc_proposition = $wpof->desc_formation->get_group("proposition")->term;

            if (isset($desc_proposition['presentation']))
            {
                $pres_gal = $this->presentation;
                unset($desc_proposition['presentation']);
                ?>
                
                <div class="chapo">
                    <?php echo wpautop($pres_gal); ?>
                </div>
                <?php if ($object == null) : ?>
                <div class="boutons">
                    <div class="icone-bouton dynamic-dialog" data-function="premier_contact" data-sessionid="<?php echo $this->id; ?>">
                        <?php the_wpof_fa_sendmail(); ?>
                        <?php _e("Inscription et/ou renseignements"); ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php
            }
            if ($object == null) : ?>
                <div id="planning-session">
                    <h3><?php _e("Planning de la session"); ?></h3>
                    <?php echo print_creneaux($this->creneaux, true); ?>
                </div>
            <?php endif; ?>
            <?php
            foreach($desc_proposition as $k => $t)
            {
                if (!empty(trim($this->$k))) :
                ?>
                <h3><?php echo $t->text; ?></h3>
                <p><?php echo wpautop($this->$k); ?></p>
                <?php
                endif;
            }
        ?>
        </div>
        <?php
            else :
                echo wpautop($this->presentation);
            endif;
        ?>
        </div> <!-- session-content -->
        
        </div> <!-- session-global -->
        <?php 
        return ob_get_clean();
    }
    
    public function get_places_dispos() {
        if ($this->stagiaires_max > 0)
        {
            if ($this->nb_confirmes < $this->stagiaires_max)
            {
                $places_dispos = $this->stagiaires_max - $this->nb_confirmes;
                return sprintf( _n( 'Il reste une place', 'Il reste %s places', $places_dispos ), number_format_i18n( $places_dispos ) );
            }
            else
                return "<span class='alerte'>".__("Les inscriptions sont closes")."</span>";
        }
    }

    /*
    * Tableau de bord du formateur pour une session donnée
    * $session_id : ID de la session
    * $user_id : ID du stagiaire
    * $formation_id : ID de la formation
    * $inscrits : tableau contenant les ID des stagiaires inscrits
    */
    public function get_the_board()
    {
        global $Formation;
        global $SessionStagiaire;
        global $Documents;
        global $wpof, $global_log;
        
        $html = array('head' => '', 'tabs' => '', 'toc' => '', 'content' => '');
        
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        $formateur0 = get_formateur_by_id($this->formateur[0]);
        
        $this->init_docs();
        
        ob_start();
        ?>
        
        <?php if (in_array($role, $wpof->super_roles)) : ?>
        <p>
            <?php
            $anime_par = $this->get_formateurs_noms('array', true);
            if (can_usurper())
            {
                global $wp;
                foreach($anime_par as $fid => $fname)
                    $anime_par[$fid] .= " ".get_switchuser_button($fid, __("Se connecter en tant que ").$fname, home_url($wp->request));
            }
            printf("Animée par <strong>%s</strong>", implode(', ', $anime_par));
            if ($this->is_a_distance(false) === true)
                echo " / ".__("en partie à distance");
            elseif ($this->is_a_distance(true) === true)
                echo " / ".__("totalement à distance");
            ?>
        </p>
        <?php endif; ?>
        <?php if ($this->is_session_ended()) : ?>
        <p><?php _e("La session est terminée"); ?></p>
        <?php endif; ?>
        <div class="opaga_head">
        <div class="dates"><?php echo $this->dates_texte; ?>
        <?php if ($this->depassement_duree > 0) : ?>
        <p class="erreur"> <?php printf(__("Attention, dépassement de durée de %s heures. Corrigez cela dans l'onglet Dates."), $this->depassement_duree); ?></p>
        <?php endif; ?>
        </div>
        <?php echo get_icone_aide("aide_session_globale", __("Pour gérer cette session")); ?>
        <?php echo $this->get_delete_bouton("Supprimer", $formateur0->permalink); ?>
        </div>
        
        <?php
        $html['head'] = ob_get_clean();
        
        $onglet = "";
        $onglet_param = null;
        if (empty($wpof->uri_params[1]))
        {
            if (empty($this->creneaux))
                $wpof->uri_params[1] = "dates";
            elseif (empty($this->clients))
                $wpof->uri_params[1] = "session";
            else
                $wpof->uri_params[1] = "client";
        }
        if (isset($wpof->uri_params[1]) && in_array($wpof->uri_params[1], array_keys($wpof->onglet_session)))
        {
            $onglet = $wpof->uri_params[1];
            if (isset($wpof->uri_params[2]))
                $onglet_param = array_slice($wpof->uri_params, 2);
        }
        $this->url_main_tab = $this->permalien.'/'.$onglet;
        ob_start();
        ?>

        <div class="opaga_tabs">
        <ul>
            <li><?php $formateur = get_formateur_by_id((in_array($role, $wpof->super_roles)) ? $this->formateur[0] : get_current_user_id()); echo $formateur->get_notifications(); ?></li>
        <?php foreach($wpof->onglet_session as $slug => $text) : ?>
            <li <?php if ($onglet == $slug) : ?> class="ui-state-active" <?php endif; ?>><a href="<?php echo $this->permalien.$slug; ?>"><?php echo $text; ?></a></li>
        <?php endforeach; ?>
        </ul>
        </div>
        <?php
        $html['tabs'] = ob_get_clean();
        
        if (!empty($onglet))
        {
            $onglet_func = "get_tab_".$onglet;
            if ($onglet_param)
                $html['content'] = $this->$onglet_func($onglet_param);
            else
                $html['content'] = $this->$onglet_func();
        }
        
        // Activer la table des matières quand elle sera prête (styles)
        $html['toc'] = $wpof->toc->get_html_toc("session");
        
        return $html;
    }
    
    public function get_tab_session()
    {
        global $wpof;
        ob_start();
        ?>
        <div id="tab-session" class="have-white-boards">
            <div class="infos-session edit-data" data-id="<?php echo $this->id; ?>">
                <div class="white-board session-infos">
                    <?php
                        if ($this->formation_id > 0)
                        {
                            $formation = get_formation_by_id($this->formation_id);
                            printf(__("Basée sur la formation catalogue : <strong><a href='%s'>%s</a></strong>"), $formation->permalien, $formation->titre); ?>
                            <a href="<?php echo $formation->permalien.'?'.$wpof->formation_edit_link_suffix; ?>" target="_blank"><?php the_wpof_fa_edit(); ?></a>
                            <?php
                        }
                        ?>
                        <div class="grid-forms">
                        <?php
                        echo get_input_jpost($this, "acces_session", array('select' => '', 'label' => __("Accès à la session (visibilité dans l'agenda)"), 'readonly' => empty($this->creneaux), 'grid_col' => '3'));
                        if (!isset($wpof->formation))
                            init_term_list("formation");
                        echo get_input_jpost($this, "session_unique_titre", array('input' => 'text', 'label' => __("Intitulé de session unique"), 'size' => '80', 'postprocess' => 'tabs_reload+update_session_titre', 'grid_col' => '3'));
                        if ($this->formation_id <= 0) :
                            echo get_input_jpost($this, "specialite", array('select' => '', 'label' => __("Spécialité"), 'first' => __("Choisissez une spécialité, la plus précise possible"), 'needed' => true, 'grid_col' => '3'));
                            echo get_input_jpost($this, "nature_formation", array('select' => '', 'label' => __("Nature de la formation"), 'needed' => true, 'grid_col' => '3', 'aide' => 'formation_nature_formation'));
                        else : ?>
                            <div class="input_jpost grid-3"><label class="top"><?php _e("Spécialité"); ?></label><?php echo $wpof->specialite->get_term($this->specialite); ?></div>
                            <div class="input_jpost grid-3"><label class="top"><?php _e("Nature de la formation"); ?></label><?php echo $wpof->nature_formation->get_term($this->nature_formation); ?></div>
                        <?php endif; ?>
                        </div>
                    
                    <h2><?php _e("Équipe pédagogique"); ?></h2>
                    <?php
                        if (!isset($wpof->formateur))
                            init_term_list("formateur");
                        echo get_input_jpost($this, "formateur", array('select' => 'multiple', 'rows' => 10, 'postprocess' => 'check_my_formation'));
                    ?>
                    
                    <h2><?php _e("Stagiaires"); ?></h2>
                        <div class="grid-forms">
                        <?php echo get_input_jpost($this, "stagiaires_min", array('input' => 'number', 'step' => 1, 'min' => 1, 'label' => __('Nombre minimum souhaité'), 'grid_col' => '2')); ?>
                        <?php echo get_input_jpost($this, "stagiaires_max", array('input' => 'number', 'step' => 1, 'min' => 1, 'label' => __('Nombre maximum souhaité'), 'grid_col' => '2')); ?>
                        <?php echo get_input_jpost($this, "tarif_total_chiffre", array('input' => 'number', 'step' => 0.01, 'min' => 0, 'label' => __('Tarif/stagiaire affiché'),  'postprocess' => 'update_pour_infos_session', 'grid_col' => '2')); ?>
                        </div>

                </div> <!-- .white-board -->
                <div class="pour-infos">
                    <h3><?php _e("Pour informations"); ?></h3>
                    <?php echo $this->get_pour_infos_box(); ?>
                </div>
            </div> <!-- .infos-session -->
            <?php
            $gest_docs = get_gestion_docs($this); 
            if ($gest_docs)
            {
                ?>
                <div class="white-board">
                <fieldset class="edit-data">
                    <legend><?php _e("Documents administratifs pour la session"); ?></legend>
                    <?php echo $gest_docs; ?>
                </fieldset>
                </div>
                <?php
            }
            ?>
            <div class="white-board">
                <?php echo $this->get_televersement_fieldset(); ?>
            </div>
        </div> <!-- tab-session -->
        <?php
        return ob_get_clean();
    }
    
    public function get_tab_dates()
    {
        global $wpof;
        ob_start();
        ?>
        <div id="tab-dates" class="white-board">
            <?php echo get_icone_aide("tab_dates", __("Dates et créneaux horaires")); ?>
            <h3><?php _e("Dates et créneaux horaires"); ?></h3>
            <p class="bg-alerte center"><?php _e('Attention, les créneaux ne doivent couvrir que le temps pédagogique ! Ne comptez pas la pause repas !'); ?></p>
            <p class="center"><?php _e('Par exemple, pour une journée en présentiel classique, créez deux créneaux, un pour le matin, un pour l’après-midi.'); ?></p>
            <p class="center"><?php _e('Si vous n’avez pas de client en direct (seulement sous-traitance), inutile de préciser les créneaux, les dates suffisent.'); ?></p>
            <p>
            <?php
                _e("Types de créneaux : ");
                foreach($wpof->type_creneau as $type => $text)
                    echo "<span class='faux-bouton creneau $type'>$text</span>";
            ?>
            </p>
            <?php echo $this->get_html_creneaux(true); ?>
            
            <p class="decale_date">
            <?php if (count($this->creneaux) > 1) : // on affiche la fonction de décalage uniquement si deux dates ou plus sont définies ?>
            <?php echo get_icone_aide("decale_date"); ?>
            <?php _e("Décaler la première date au "); ?>
            <input class="datepicker" name="decale_date" type="text" value="" />
            <span class="icone-bouton valider_decale_date"><?php _e("Valider"); ?></span>
            <?php endif; ?>
            </p>
            
            <?php if (champ_additionnel('duree_jour')) : ?>
            <div class="edit-data">
            <?php echo get_input_jpost($this, "nb_jour", array('input' => 'text', 'label' => __("Indication de durée en jours ou demies-journées"))); ?>
            </div>
            <?php endif; ?>
            
        </div> <!-- tab-dates -->
        <?php
        return ob_get_clean();
    }
        
    public function get_tab_lieu()
    {
        global $wpof;
        ob_start();
        ?>
        <div id="tab-lieu" class="white-board">
            <?php echo get_icone_aide("tab_lieu", __("Informations sur le lieu")); ?>
            <h3><?php _e("Lieu"); ?></h3>
        <div class="lieu-session">
            <div class="select edit-data">
            <?php
            if (!isset($wpof->lieu))
                init_term_list("lieu");
            echo get_input_jpost($this, "lieu", array('select' => '', 'label' => __("Lieu prédéfini"), 'first' => __("Lieu occasionnel"), 'postprocess' => 'toggle_lieu_details'));
            ?>
            </div>
            <?php echo $this->get_lieu_details(); ?>
        </div>
        </div> <!-- tag-lieu -->
        <?php
        return ob_get_clean();
    }
        
    public function get_tab_formation()
    {
        global $wpof;
        ob_start();
        ?>
        <div id="tab-formation" class="white-board desc-session edit-data metadata">
            <?php echo get_icone_aide("tab_formation", __("Décrire la session")); ?>
            <h3><?php _e("Description de la formation pour cette session"); ?></h3>
            <?php
                foreach($wpof->desc_formation->term as $k => $t)
                {
                    if ($wpof->{"formation_".$k."_mode"} != 'force')
                        echo get_input_jpost($this, $k, array('editor' => '', 'label' => $t->text, 'header' => 'h4', 'needed' => $t->session_needed));
                    else
                    {
                        ?>
                        <h4><?php echo $t->text; ?></h4>
                        <p><em><?php _e("Ce paramètre est fixé par votre responsable de formation et ne peut être modifié."); ?></em></p>
                        <p><?php echo $wpof->{"formation_".$k."_text"}; ?></p>
                        <?php
                    }
                }
            ?>
        </div> <!-- tab-formation -->
        <?php
        return ob_get_clean();
    }
        
    public function get_tab_client($param = null)
    {
        global $wpof, $Client, $SessionStagiaire;
        $client = null;
        $stagiaire = null;
        
        if (is_array($param))
        {
            if (isset($Client[$param[0]]) && $Client[$param[0]]->session_formation_id == $this->id)
                $client = $Client[$param[0]];
            if (isset($param[1]) && isset($SessionStagiaire[$param[1]]) && $SessionStagiaire[$param[1]]->client_id == $client->id)
                $stagiaire = $SessionStagiaire[$param[1]];
        }
        
        ob_start();
        ?>
        <div id="tab-client" class="have-white-boards">
            <div class="boutons">
                <?php if ($stagiaire) : ?>
                    <?php echo $client->get_a_link('<div class="icone-bouton">'.wpof_fa("arrow-left").__("Retour client").'</div>'); ?>
                    <div class="icone-bouton dynamic-dialog" data-function="new_stagiaire" data-clientid="<?php echo $client->id; ?>" data-sessionid="<?php echo $client->session_formation_id; ?>"><?php the_wpof_fa_add(); ?> <?php _e("Ajouter stagiaire"); ?></div>
                    <?php echo get_icone_aide("inscrire_stagiaires", __("Stagiaires")); ?>
                <?php elseif ($client) : ?>
                    <a class="icone-aide border" href="<?php echo $this->permalien.'client'; ?>"><?php echo wpof_fa("arrow-left").__("Retour à la liste des clients"); ?></a>
                <?php else : ?>
                    <p class="icone-bouton dynamic-dialog" data-function="select_client" data-sessionid="<?php echo $this->id; ?>"> <?php echo wpof_fa_add(); ?> <?php _e("Ajouter client"); ?> </p>
                    <?php echo get_icone_aide("tab_clients", __("Gestion des clients")); ?>
                <?php endif; ?>
            </div>
            
            <div id="tabs-clients">
            <?php
            if ($stagiaire)
                $stagiaire->the_board();
            elseif ($client)
                $client->the_board();
            else
                echo $this->get_tabs_clients();
            ?>
            </div>
        </div> <!-- tab-client -->
        <?php
        return ob_get_clean();
    }
        
    public function get_tab_questionnaires(Array $param = [])
    {
        global $wpof, $SessionStagiaire;

        $bouton_retour = '<a class="icone-bouton" href="'.$this->url_main_tab.'">'.wpof_fa('arrow-left').' '.__("Retour").'</a>';
        ob_start();
        ?>
        <div id="tab-questionnaires" class="white-board">
            <?php echo get_icone_aide("tab_questionnaires", __("Questionnaires")); ?>
        <?php
        if ($this->has_soustraitance(true) === false)
        {
            if (isset($param[0]))
            {
                switch($param[0])
                {
                    case 'reponses':
                        echo $bouton_retour;
                        if (isset($param[1]))
                            echo $this->get_questionnaires_reponses_tab($param[1], $param[2] ?? null);
                        break;
                    default:
                        echo $this->get_questionnaires_tab();
                        break;
                }
            }
            else
                echo $this->get_questionnaires_tab();
        }
        else
            _e("Pas de client en direct, pas besoin de questionnaire ici");
        ?>
        </div> <!-- tab-questionnaires -->
        <?php
        return ob_get_clean();
    }

    public function get_tab_documents()
    {
        global $wpof, $Documents;
        $this->init_all_docs();
        ob_start();
        ?>
        <div id="tab-docs" class="white-board">
        <?php if (!empty($Documents)) : ?>
        <?php echo get_icone_aide('session_documents', __('Gestion des doucments de la session')); ?>
        <div class="control_board flexrow margin">
        <?php
        $toggle_empty_docs = new BoardToggleButton(
            array(
                'text_off' => __('Afficher les documents vides'),
                'text_on' => __('Masquer les documents vides'),
                'hide' => '.empty',
                'parent' => 'table.gestion-docs-admin',
            ));
        echo $toggle_empty_docs->get_html_button('on');
        ?>
        <div id="doc_select">
        <?php echo $wpof->documents->get_select_list(-1, 'data-dest="table.gestion-docs-admin"', __("Sélectionnez un type de documents")); ?>
        </div>
        <div id="doc_global_command" data-dest="table.gestion-docs-admin">
        <span class="icone-bouton" data-function="doc_view_checked" title="<?php _e('Afficher seulement la sélection'); ?>"><?php the_wpof_fa("eye"); ?></span>
        <span class="icone-bouton" data-target=".doc-creer.brouillon" title="<?php _e('Créer ou mettre à jour les brouillons'); ?>"><?php the_wpof_fa("square-plus"); ?></span>
        <span class="icone-bouton" data-target=".doc-creer.final" title="<?php _e('Créer ou mettre à jour les versions finales'); ?>"><?php the_wpof_fa("square-plus"); ?></span>
        <span class="icone-bouton" data-target=".doc-demande-valid" title="<?php _e('Demander ou annuler la validation'); ?>"><?php the_wpof_fa("file-signature"); ?></span>
        <span class="icone-bouton" data-target=".doc-diffuser" title="<?php _e("Rendez ce document disponible au stagiaire et/ou au client"); ?>"><?php the_wpof_fa("share-nodes"); ?></span>
        <?php echo get_icone_aide("session_documents_global_commands"); ?>
        </div>
        </div>
        <table class="opaga opaga2 datatable gestion-docs-admin">
        <thead>
        <?php echo Document::get_document_control_head(); ?>
        </thead>
        <?php
        foreach($Documents as $doc)
            echo $doc->get_document_control();
        ?>
        </table>
        <?php endif; ?>
        </div> <!-- tab-documents -->
        <?php
        return ob_get_clean();
    }
        
    public function get_tab_token()
    {
        global $wpof;
        ob_start();
        ?>
        <div id="tab-token" class="white-board">
            <?php echo get_icone_aide("tab_token", __("Gestion des accès privés")); ?>
            <?php echo $this->get_token_tab(); ?>
        </div> <!-- tab-eval -->
        <?php
        return ob_get_clean();
    }
    
    public function get_pour_infos_box()
    {
        global $wpof;
        $this->calcule_temps_session();
        $this->calcule_tarif();
        $this->calcule_budget_global();
       
        ob_start();
        ?>
            <div class="infos-content">
            <p><?php _e("Dates concernées"); ?> : <span class="dates_concernees"><?php echo $this->dates_texte; ?></span></p> 
            <p><?php _e("Tarif horaire par stagiaire (affiché)"); ?> : <span class="tarif_heure"><?php echo get_tarif_formation($this->tarif_heure); ?></span> </p>
            <p><?php _e("Nombre d'heures"); ?> : <span class="nb_heure"><?php echo $this->nb_heure; ?></span>
            </p>
            <p><?php _e("Tarif total par stagiaire (affiché)"); ?> : <span class="tarif_total"><?php echo get_tarif_formation($this->tarif_total_chiffre); ?></span> </p>
            <p><?php _e("Budget global"); ?> : <span class="budget_global"><?php echo get_tarif_formation($this->budget_global); ?> </span> </p>
            </div>
        <?php
        return ob_get_clean();
    }

    /**
     * Renvoie les informations de lieu
     */
    public function get_lieu_infos(bool $adresse = false)
    {
        $text = $this->lieu_nom;
        if ($adresse && !empty($this->lieu_adresse))
            $text .= " – ".$this->lieu_adresse;
        if (!empty($this->lieu_ville))
            $text .= " – ".(($adresse && !empty($this->lieu_code_postal)) ? $this->lieu_code_postal : "")." ".$this->lieu_ville;
        return strip_tags($text);
    }
    
    public function get_lieu_details()
    {
        global $wpof;
        ob_start();
        ?>
            <div class="lieu details <?php if ($this->lieu == -1) echo "edit-data"; ?>">
            <?php
            foreach($wpof->desc_lieu->term as $key => $term)
            {
                switch ($term->type)
                {
                    case "text":
                    case "number":
                        echo get_input_jpost($this, "lieu_$key", array('input' => $term->type, 'label' => $term->text));
                        break;
                    case "textarea":
                    case "editor":
                    case "select":
                        echo get_input_jpost($this, "lieu_$key", array($term->type => '', 'label' => $term->text));
                        break;
                    default:
                        echo "<p>$key pas encore géré</p>";
                        break;
                }
                
            }
            ?>
            </div>
        <?php
        
        return ob_get_clean();
    }
    
    public function get_questionnaires_tab(): String
    {
        require_once wpof_path . 'class/class-qrcode-creator.php';

        global $wpof;

        $qmanager = new QManager();
        $questionnaires = $qmanager->get_questionnaires(array('nature_formation' => $this->nature_formation, 'modalite_session' => $this->modalite), true);

        ob_start();
        foreach($wpof->questionnaire_type->term as $key => $term) : ?>
            <?php if (!empty($questionnaires[$key])) : ?>
            <div class="questionnaire_list">
            <h2><?php echo $term->text; echo get_icone_aide('session_questionnaire_'.$key); ?></h2>
                <?php foreach($questionnaires[$key] as $q) : ?>
                <?php $questionnaire_url = home_url()."/".$wpof->url_questionnaire."/".$this->id."/".$q->id; ?>
                <div class="questionnaire_block" data-id="<?php echo $q->id; ?>">
                    <p><?php echo $q->title; ?></p>
                    <p>
                        <a class="icone-bouton" href="<?php echo $questionnaire_url; ?>" target="_blank"><?php the_wpof_fa('eye'); ?> <?php _e("Voir et répondre"); ?></a>
                        <?php if ($key == 'satisfaction') : ?>
                            <span class="icone-bouton"><?php echo get_copy_span(__("Copier le lien"), false, $questionnaire_url); ?></span>
                            <span class="icone-bouton qrcode action" data-function="show" data-text="<?php echo $questionnaire_url; ?>">
                            <?php the_wpof_fa('qrcode'); ?> <?php _e("Afficher le QR code"); ?></span>
                            <?php $subject = sprintf(__('Questionnaire %s pour la session %s – %s'), $term->text, $this->get_displayname(), $this->dates_texte);
                            $content = sprintf(__("Bonjour,\n\nVoici le lien générique pour répondre au questionnaire\n\n%s\n\nVos réponses sont anonymes. Un lien personnalisé est présent sur la page du questionnaire, il vous permet de le retrouver et le compléter à postériori.\nNous ne conservons aucun lien entre votre adresse email et ce lien personnalisé."), $questionnaire_url);
                            ?>
                            <span class="icone-bouton questionnaire action <?php if (!empty($this->check_missing_email_stagiaires())) echo 'bg-alerte'; ?>" data-function="sendmail" data-subject="<?php echo $subject; ?>" data-content="<?php echo $content; ?>"><?php the_wpof_fa('envelope'); ?> <?php _e('Envoyer par email'); ?></span>
                        <?php endif; ?>
                        <a class="icone-bouton" href="<?php echo home_url().'?download=questionnaire&ci='.$q->id.'&s='.$this->id.'&c='.$wpof->doc_context->questionnaire; ?>"><?php the_wpof_fa('file-pdf'); ?> <?php _e('Version imprimable'); ?></a>
                        <?php $q->init_reponses($this->id); 
                        if ($q->nb_repondant > 0) : ?>
                        <a class="icone-bouton" href="<?php echo $this->permalien.'questionnaires/reponses/'.$q->id; ?>"><?php the_wpof_fa('eye'); ?> <?php printf(__("Réponses (%d)"), $q->nb_repondant); ?></a>
                        <?php endif; ?>
                    </p>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        <?php
        endforeach;
        return ob_get_clean();
    }
    
    public function get_questionnaires_reponses_tab(int $questionnaire_id, string $repondeur_id = null): String
    {
        global $wpof, $wp;

        $questionnaire = new Questionnaire($questionnaire_id);
        $questionnaire->init_reponses($this->id, $repondeur_id);

        $questionnaire_url = home_url()."/".$wpof->url_questionnaire."/".$this->id."/".$questionnaire->id;

        ob_start(); ?>
        <?php if ($repondeur_id === null) : ?>
            <h2><?php _e("Compilation des réponses"); ?></h2>
        <?php else : ?>
            <h2><?php printf(__("Réponses de %s"), $repondeur_id); ?><?php if (in_array($wpof->role, $wpof->super_roles)) :
                ?> <a title="<?php _e("Ne modifiez les valeurs que pour corriger une erreur !"); ?>" href="<?php echo home_url().'/questionnaire/'.$this->id.'/'.$questionnaire_id.'/'.$repondeur_id; ?>"><?php the_wpof_fa_edit('alerte'); ?></a><?php
                endif; ?></h2>
        <?php endif; ?>
        <div class="boutons">
            <span><?php _e("Réponses individuelles"); ?></span>
            <?php foreach($questionnaire->get_repondeur_id($this->id) as $id) : ?>
            <a class="icone-bouton" href="<?php echo $this->permalien.'/questionnaires/reponses/'.$questionnaire->id.'/'.$id; ?>"><?php echo $id; ?></a>
            <?php endforeach; ?>
            <a href="<?php echo $this->url_main_tab.'/reponses/'.$questionnaire_id; ?>" class="icone-bouton float right"><?php _e("Compilation des réponses"); ?></a>
        </div>

        <?php if (count($questionnaire->reponses) > 0) : ?>
        <?php $compil_reponses = $questionnaire->get_compilation_reponses($this->id); ?>
        <table class="opaga compil-reponses">
        <tr><th><?php _e("Questions et réponses"); ?></th><th><?php _e("Type question"); ?></th><th><?php _e("Pourcentage"); ?></th></tr>
        <?php foreach($compil_reponses as $qid => $repA) : ?>
            <tr id="q<?php echo $qid; ?>"><td class="question title"><?php echo $repA['title']; ?></td>
            <td><?php echo $wpof->questionnaire_type_question->get_term($questionnaire->question[$qid]->qtype) ; ?></td>
            <?php if (is_array($repA['reponse'])) : ?>
                <td></td>
                <?php foreach($repA['reponse'] as $subrepA) : ?>
                    <tr>
                    <?php if (isset($subrepA['title'])) : ?>
                    <td class="reponse title"><?php echo $subrepA['title']; ?></td>
                    <td></td><td><?php echo $subrepA['reponse']; ?> %</td>
                    <?php else: ?>
                    <td class="reponse title"><?php echo $subrepA; ?></td><td></td><td></td>
                    <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <td><?php echo $repA['reponse']; ?> %</td>
            <?php endif; ?>
        <?php endforeach; ?>
        </table>
        <?php else: ?>
            <p><?php _e("Aucune réponse pour l'instant"); ?></p>
        <?php endif;
        return ob_get_clean();
    }
    
    public function get_token_tab()
    {
        $token_list = array();
        
        foreach($this->clients as $c)
        {
            $client = get_client_by_id($this->id, $c);
            
            if ($client->token != "")
            {
                $token_info = new stdClass();
                $token_info->id = $c;
                $token_info->nom = $client->nom;
                $token_info->class = "Client";
                $token_info->email = implode(', ', array($client->contact_email, $client->responsable_email));
                $token_info->token = $client->token;
                $token_info->token_time = $client->token_time;
                $token_info->valid_token = valid_token($client->token_time);
                $token_info->acces = $client->acces;
                $token_list[] = $token_info;
            }
            
            if (!empty($client->stagiaires))
                foreach($client->stagiaires as $stagiaire_id)
                {
                    $stagiaire = get_stagiaire_by_id($this->id, $stagiaire_id);
                    
                    if ($stagiaire->token != "")
                    {
                        $token_info = new stdClass();
                        $token_info->id = $stagiaire_id;
                        $token_info->nom = $stagiaire->get_displayname();
                        $token_info->class = "Stagiaire";
                        $token_info->email = $stagiaire->email;
                        $token_info->token = $stagiaire->token;
                        $token_info->token_time = $stagiaire->token_time;
                        $token_info->valid_token = valid_token($stagiaire->token_time);
                        $token_info->acces = $stagiaire->acces;
                        $token_list[] = $token_info;
                    }
                }
        }
        
        if (!empty($token_list))
        {
            global $wpof;
            
            ob_start();
            ?>
            <table class="token_list opaga">
            <thead>
            <tr>
            <th class="thin"><?php _e('Entité'); ?></th>
            <th><?php _e('Nom/raison sociale'); ?></th>
            <th><?php _e('Email'); echo get_icone_aide("token_email"); ?></th>
            <th><?php _e('Lien privé'); echo get_icone_aide("token_lien"); ?></th>
            <th><?php _e('Validité'); echo get_icone_aide("token_validite"); ?></th>
            <th><?php _e('Dernier accès'); echo get_icone_aide("token_dernier_acces"); ?></th>
            <th class="thin"><?php _e('Supprimer'); ?></th>
            <th class="thin"><?php _e('Renouveler'); echo get_icone_aide("token_renouveler"); ?></th>
            </tr>
            </thead>
            
            <tbody>
            <?php foreach($token_list as $token_info) : ?>
            <tr class="<?php echo $token_info->class; ?> token_info" data-id="<?php echo $token_info->id; ?>">
            <td><?php echo $token_info->class; ?></td>
            <td><?php echo $token_info->nom; if (debug) echo " (".$token_info->id.")"; ?></td>
            <td><?php echo $token_info->email; ?></td>
            <td><a href="<?php echo get_site_url()."/".$wpof->url_acces."/?t=".$token_info->token; ?>" class="short-info"><?php echo $token_info->token; ?></td>
            <td>
                <?php
                    if ($token_info->valid_token === true)
                        echo ($token_info->acces > 0) ? __("Illimitée") : __("Non utilisé");
                    else
                        echo ($token_info->valid_token === false) ? __("Expiré") : get_pretty_duree($token_info->valid_token); ?>
            </td>
            <td><?php if ($token_info->acces > 0) echo date_i18n("d/m/Y à H:i", $token_info->acces); ?></td>
            <td class="center">
                <p class="icone-bouton token-delete" data-id="<?php echo $token_info->id; ?>" data-remove="tr.token_info" data-class="<?php echo $token_info->class; ?>"><?php the_wpof_fa_del(); ?></p>
            </td>
            <td class="center">
                <p class="token-reset icone-bouton" data-email="<?php echo $token_info->email; ?>"><?php the_wpof_fa('rotate'); ?></p>
            </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
            </table>
            <?php
            
            return ob_get_clean();
        }
        
        return "";
    }
    
    /*
     * Retourne l'onglet des clients
     */
    public function get_tabs_clients()
    {
        global $wpof;
        $list_tabs = "";
        $content_tabs = "";
        
        $return = '<table><thead><tr><th scope="col">Clients</th><th scope="col">Stagiaires</th><th scope="col">N° de contrat</th><th scope="col">Contact</th><th scope="col">Tarif total HT</th><th scope="col"></th></tr></thead><tbody>';
        foreach($this->clients as $c)
        {
            $return .= '<tr>';
            $client = new Client($this->id, $c);
            $client->init_stagiaires();
            
            if ($client->entite_client == Client::ENTITE_PERS_MORALE)
                $nom = (empty($client->nom)) ? __("Sans nom") : $client->nom;
            else
                $nom = (empty($client->stagiaire->nom)) ? __("Sans nom") : $client->stagiaire->prenom." ".$client->stagiaire->nom;
            
            $entite_after = ($client->entite_client === Client::ENTITE_PERS_PHYSIQUE) ? __('Particulier') : ( ($client->financement === Client::FINANCEMENT_OPAC) ? __('Sous-traitant') : __('Structure') );
            
            $return .= $this->get_tabs_clients_col($nom, '<a href="'.$client->permalien.'" class="client_nom">'.wpof_fa_edit(), '</a><div class="icone-info border">'.wpof_fa('building-columns').esc_html($entite_after).'</div>', false, false);
            
            if ($client->entite_client == Client::ENTITE_PERS_MORALE)
            {
                // Sous traitant = il n'y a pas de liste de stagiaires.
                if ($client->financement === Client::FINANCEMENT_OPAC)
                {
                    $stag_link = $client->get_a_link(__('Modifier'), 'bilan-pedagogique');
                }
                else
                {
                    $stag_link = $client->get_a_link(__('Voir'), 'stagiaires');
                }

                if ($nb_stagiaires = $client->nb_stagiaires)
                    $stag_txt = sprintf( _n( 'Un stagiaire', '%s stagiaires', $nb_stagiaires ), number_format_i18n( $nb_stagiaires ) );
                else
                    $stag_txt = __('Pas de stagiaire');

                $return .= $this->get_tabs_clients_col($stag_txt, 'fa-users', ' ' . $stag_link, false, false);
            }
            else // Personne physique
            {
                $return .= $this->get_tabs_clients_col(__('N/A'), 'fa-users');
            }
            
            $return .= $this->get_tabs_clients_col($client->numero_contrat, 'fa-hashtag', '', false, true, 'short-info');
            $return .= $this->get_tabs_clients_col($client->get_contacts('', ', '), 'fa-id-badge');
            $tarif_client = number_format_i18n($client->tarif_total_chiffre, 2);
            $return .= $this->get_tabs_clients_col($tarif_client, 'fa-euro-sign', !empty($tarif_client) ? '€' : '');
            $return .= $this->get_tabs_clients_col(' ', $client->get_delete_icone(), '', false);
    
            $return .= '</tr>';
        }
        $return .= '</tbody></table>';
        return $return;
    }
    
    /**
     * Crée un td à mettre dans le tableau des clients de tab-clients
     *
     * @param string $prop la valeur à mettre dans le TD
     * @param string $before la chaine à mettre avant prop (ou un fa-) pour mettre une font image dans le TD
     * @param string $after la chaine à mettre après prop dans le TD
     * @param boolean $esc_before si différent de false fait un esc_html de before
     * @param boolean $esc_after si différent de false fait un esc_html de after
     * @param string $tdclass la classe à mettre dans le TD
     * @return string le TD à mettre dans le tableau
     */
    private function get_tabs_clients_col($prop, $before='', $after='', $esc_before=true, $esc_after=true, $tdclass='') 
    {
        // Reformate before si besoin
        if (!empty($before))
        {
            if (strpos($before, 'fa-') === 0)
            {
                $before = wpof_fa($before);
            }
            elseif ($esc_before !== false) 
            {
                $before = esc_html($before);
            }
        }

        // Reformate after si besoin
        if (!empty($after) && $esc_after !== false)
        {
            $after = esc_html($after);
        }

        // Escape la prop ou met N/A si vide
        if (!empty($prop))
        {
            $prop = esc_html($prop);
        }
        else
        {
            $prop = __('N/A');
        }

        $tdclass = esc_attr($tdclass);
        if (!empty($tdclass))
        {
            $tdclass_begin = '<div class="' . $tdclass . '">';
            $tdclass_end = '</div>';
        }
        else
        {
            $tdclass_begin = $tdclass_end = '';
        }
        return "<td>$tdclass_begin $before $prop $after $tdclass_end</td>";    
    }

    /*
     * Retourne le découpage temporel de la session
     */
    public function get_decoupage_temporel()
    {
        return print_creneaux($this->creneaux);
    }
    
    protected function get_televersement_fieldset()
    {
        ?>
            <fieldset><legend><?php _e("Téléversement"); ?></legend>
            <p><?php _e("Déposez ici les documents signés et scannés ou tout autre document qui vous parait utile (devis, facture par exemple)"); ?></p>
            <p><?php echo __("Poids maximal par fichier : ").ini_get("upload_max_filesize"); ?></p>
            
            <form method="POST" name="upload" enctype="multipart/form-data">
                <input name="scan" type="file" multiple />
                <?php
                    echo hidden_input('id_span_message', 'archive-message');
                    echo hidden_input('action', 'archive_file');
                    echo hidden_input('session_id', $this->id);
                ?>
                <input class="ajax-save-file bouton" type="button" value="<?php _e("Déposer scan") ?>" />
            </form>
            <p id="archive-message" class="message"></p>
            <table class='gestion-docs-admin' id="liste-scan">
            <tr>
            <th><?php _e("Fichier"); ?></th>
            <th><?php _e("Date de dépôt"); ?></th>
            <!-- <th><?php //_e("Diffuser à"); ?></th> -->
            <th><?php _e("Supprimer"); ?></th>
            </tr>
            <?php
            foreach ($this->uploads as $u)
                echo $u->get_html('tr');
            ?>
            </table>
            
            </fieldset>
            
        <?php
    }

    /*
     * Renvoie la liste de tous les créneaux classés par dates sous forme html
     * $edit : si true on peut modifier les créneaux, si false, ils sont simplement affichés
     * $actif : tableau $session_stagiaire->creneaux contenant la liste des ID des créneaux auxquels un stagiaire est inscrit
     */
    public function get_html_creneaux($edit = false, $objet = null)
    {
        $data_objet = "";
        if ($objet)
        {
            $objet_class = get_class($objet);
            if (in_array($objet_class, array('Client', 'SessionStagiaire')))
                $data_objet = "data-objetid='{$objet->id}' data-objet='$objet_class'";
        }
        ob_start();
        
        if (!$edit && count($this->creneaux) == 0) :
            echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
        else : ?>
            <div class='tableau-creneau' <?php echo $data_objet; ?> data-sessionid='<?php echo $this->id; ?>'>
            <?php
            foreach($this->creneaux as $date => $creneau)
                if ($objet === null || !empty(array_intersect_key($this->creneaux[$date], $objet->creneaux)))
                    echo $this->get_html_ligne_creneaux($edit, $date, $objet);
            
            if ($edit) : ?>
                <div class="icone-bouton add-date empty-date">
                    <?php echo wpof_fa_add(); ?>
                    <?php _e("Date"); ?>
                </div>
                <?php echo get_icone_aide("creneau_ajouter_date"); ?>
            <?php endif; ?>
            </div>
        <?php endif;
        
        return ob_get_clean();
    }
    
    /*
     * Renvoie une ligne de créneaux correspondant à une date au format DateTime
     */
    public function get_html_ligne_creneaux($edit = false, $date = null, $objet = null)
    {
        ob_start();
        echo "<div class='liste-creneau' data-date='$date'>";
        
        if ($edit)
        {
            the_wpof_fa_del('icone del-date'); ?>
            <div class="date">
            <p class="week_day"><?php echo get_week_day($date, true); ?></p>
            <input class="datepicker" name="dates[]" type="text" value="<?php echo $date; ?>" />
            </div>
            <?php
        }
        
        if ($date != null)
        {
            if (!$edit)
                echo "<span class='creneau-date'>$date</span>";
            
            echo '<div class="creneau-times">';
            foreach ($this->creneaux[$date] as $creno)
                if ($objet === null || in_array($creno->id, array_keys($objet->creneaux)))
                    echo $creno->get_html($objet, $edit);
            echo '</div>';
        }
        
        if ($edit)
        {
            ?>
            <div class="boutons-date">
                <div class="icone-bouton dynamic-dialog" data-function="add_or_edit_creneau" data-crenoid="-1" data-sessionid="<?php echo $this->id; ?>">
                    <?php echo wpof_fa_add(); ?>
                    <?php _e("Créneau"); ?>
                </div>
                <div class="icone-bouton add-date" data-decaljour="1">
                    <?php //echo wpof_fa_add(); ?>
                    <?php _e("Copie +1 j"); ?>
                </div>
                <div class="icone-bouton add-date" data-decaljour="7">
                    <?php //echo wpof_fa_add(); ?>
                    <?php _e("Copie +7 j"); ?>
                </div>
            </div>
            <?php //echo get_icone_aide("creneau_boutons_edit"); ?>
            <div class="creneau-day">
            </div>
            <?php
        }
        else
            echo get_icone_aide(strtolower(get_class($objet))."_creneau_active");
        echo "</div>";
        
        return ob_get_clean();
    }
    
    /*
     * Détermine si une session se déroule tout ou partie à distance
     * À distance le type de créneau est soit foad_sync, soit foad_async
     * $full == false, retourne true si en partie à distance
     * $full == true, retourne true si totalement à distance
     */ 
    public function is_a_distance($full = false)
    {
        $no_creno = true;
        foreach($this->creneaux as $date)
            foreach($date as $c)
            {
                if (!$full && substr($c->type, 0, 4) == "foad")
                    return true;
                elseif ($full && substr($c->type, 0, 4) != "foad")
                    return false;
                $no_creno = false;
            }
        
        // si on arrive ici, c'est qu'on a parcouru tous les créneaux sans que la condition == ou != de foad n'aie été trouvée
        if ($no_creno)
            return null;
        else
            return $full;
    }
    
    /*
     * Détermine si cette session est effectuée en partie ($full = false) ou totalement ($full = true) en sous-traitance
     * $full == false, retourne true si au moins un client qui nous sous-traite
     * $full == true, retourne true si aucun client en direct
     */
    public function has_soustraitance($full = true)
    {
        foreach($this->clients as $client_id)
        {
            $client = get_client_by_id($this->id, $client_id);
            if ($full && $client->financement != Client::FINANCEMENT_OPAC)
                return false;
            elseif (!$full && $client->financement == Client::FINANCEMENT_OPAC)
                return true;
        }
        return $full;
    }
    
    // retourne un select avec la liste des clients de la session
    public function get_select_client($selected_client_id)
    {
        ob_start();
        ?>
        <select name="change_client">
        <?php foreach($this->clients as $client_id) : ?>
            <?php $client = get_client_by_id($this->id, $client_id); ?>
            <option value="<?php echo $session_id; ?>" <?php selected($selected_client_id, $client_id); ?>><?php echo $client->get_nom(); ?></option>
        <?php endforeach; ?>
        </select>
        <?php
        return ob_get_clean();
    }
    
    public function get_sql_select_button($stagiaire_id = -1)
    {
        return "<span class='sql_select bouton' data-sessionid='".$this->id."' data-stagiaireid='$stagiaire_id'>SQL</span>";
    }
    
    public function get_doc_titre_formation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Intitulé de la formation"));
        return $result[$arg];
    }
    public function get_doc_formateur($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->get_formateurs_noms(), 'tag' => "session:$function_name", 'desc' => __("Équipe pédagogique"));
        return $result[$arg];
    }
    public function get_doc_nb_formateur($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => count($this->formateur), 'tag' => "session:$function_name", 'desc' => __("Nombre de formateur⋅trices"));
        return $result[$arg];
    }
    public function get_doc_modalite($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        $result = array('empty_ok' => false, 'valeur' => $function_name, 'tag' => "session:$function_name", 'desc' => __("Modalité de la session"));
        return $result[$arg];
    }

    public function get_doc_duree($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->nb_heure_decimal." h", 'tag' => "session:$function_name", 'desc' => __("Durée en heures"));
        return $result[$arg];
    }
    public function get_doc_nb_jour($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name." h", 'tag' => "session:$function_name", 'desc' => __("Durée indicative personnalisée (par ex. en journées ou demies-journées)"));
        return $result[$arg];
    }
    public function get_doc_tarif_heure($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name." ".$wpof->monnaie_symbole, 'tag' => "session:$function_name", 'desc' => __("Tarif public horaire (inter)"));
        return $result[$arg];
    }
    public function get_doc_tarif_total_chiffre($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name." ".$wpof->monnaie_symbole, 'tag' => "session:$function_name", 'desc' => __("Tarif total public en chiffres"));
        return $result[$arg];
    }
    public function get_doc_tarif_total_lettre($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name." ".$wpof->monnaie."s", 'tag' => "session:$function_name", 'desc' => __("Tarif total public en lettres"));
        return $result[$arg];
    }
    public function get_doc_dates($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->dates_texte, 'tag' => "session:$function_name", 'desc' => __("Toutes les dates de la session"));
        return $result[$arg];
    }
    public function get_doc_premiere_date($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $date = "";
        if ($arg == 'valeur' && $this->first_date != "")
            $date = pretty_print_dates($this->first_date);
        
        $result = array('empty_ok' => false, 'valeur' => $date, 'tag' => "session:$function_name", 'desc' => __("Première date de la session"));
        return $result[$arg];
    }
    public function get_doc_derniere_date($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $date = "";
        if ($arg == 'valeur' && !empty($this->creneaux))
            $date = pretty_print_dates(end(array_keys($this->creneaux)));
        
        $result = array('empty_ok' => false, 'valeur' => $date, 'tag' => "session:$function_name", 'desc' => __("Dernière date de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nom du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_adresse($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Adresse du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_code_postal($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Code postal du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_ville($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Ville du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_localisation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => true, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Informations de localisation du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_stagiaires_min($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nombre minimum de stagiaires pour réaliser la session"));
        return $result[$arg];
    }
    public function get_doc_stagiaires_max($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nombre maximum de stagiaires acceptés dans la session"));
        return $result[$arg];
    }
    public function get_doc_nb_stagiaires($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nombre de stagiaires intéressés"));
        return $result[$arg];
    }
    public function get_doc_nb_confirmes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nombre de stagiaires confirmés"));
        return $result[$arg];
    }
    public function get_doc_date_modif($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('empty_ok' => false, 'valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Date de dernière modification du programme"));
        return $result[$arg];
    }
    public function get_doc_creneaux($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = '';
        if ($arg == 'valeur')
            $valeur = print_creneaux($this->creneaux);
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "session:$function_name", 'desc' => __("Planning de la session"));
        return $result[$arg];
    }
    public function get_doc_presentation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_nature_formation($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            if ($wpof->$function_name->is_term($this->$function_name))
                $valeur = $wpof->$function_name->get_term($this->$function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => $valeur, 'tag' => "session:$function_name", 'desc' => __("Objectif de la prestation (cf. BPF)"));
        return $result[$arg];
    }
    public function get_doc_objectifs_pro($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_objectifs($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_prerequis($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_public_cible($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_modalites_pedagogiques($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_ressources($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_organisation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_materiel_pedagogique($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_accessibilite($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_modalites_evaluation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_inscription_delai($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_programme($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_propriete_intellectuelle($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('empty_ok' => false, 'valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
}

function get_session_by_id($id)
{
    global $SessionFormation;
//    log_add(__FUNCTION__."($id)", "session");
    
    if (!isset($SessionFormation[$id]))
    {
        $SessionFormation[$id] = new SessionFormation($id);
    }
    return $SessionFormation[$id];
}

