<?php
/*
 * class-qreponse.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class QReponse
{
    public int $id;
    public int $questionnaire_id;
    public int $question_id;
    public int $session_id;
    public String $repondeur_type;
    public String $repondeur_id;
    public String|Array $reponse = "";
    
    public function __construct($args = [])
    {
        if (is_array($args))
            foreach($args as $key => $val)
                if (property_exists($this, $key))
                    $this->$key = is_string($val) ? stripslashes($val) : $val;
    }
    
    public function update() : Array
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_qreponse;
        $res = [];

        $query = $wpdb->prepare(
            "INSERT INTO ".$table." (questionnaire_id, question_id, session_id, repondeur_id, repondeur_type, reponse, date) ".
            "VALUES (%d, %d, %d, %s, %s, %s, NOW()) ".
            "ON DUPLICATE KEY UPDATE reponse = %s, date = NOW();",
            $this->questionnaire_id, $this->question_id, $this->session_id, $this->repondeur_id, $this->repondeur_type, $this->reponse,
            $this->reponse
        );

        $res['insert'] = $wpdb->query($query);
        $res['query'] = $query;

        return $res;
    }

    /**
     * Suppression des réponses d'un repondeur_id d'une session donnée dans la table
     */
    public function delete(): bool
    {
        global $wpdb, $wpof;
        $table = $wpdb->prefix.$wpof->suffix_qreponse;

        $query = $wpdb->prepare("DELETE FROM ".$table.
            " WHERE repondeur_id = %s AND repondeur_type = %s AND session_id = %d AND questionnaire_id = %d;",
            $this->repondeur_id, $this->repondeur_type, $this->session_id, $this->questionnaire_id);
        
        return $wpdb->query($query);
    }
}

add_action('wp_ajax_delete_reponse', 'delete_reponse');
function delete_reponse()
{
    $reponse = array('log' => $_POST);
    
    $qreponse = new QReponse(
        [
            'repondeur_id' => $_POST['repondeur_id'],
            'repondeur_type' => $_POST['repondeur_type'],
            'session_id' => $_POST['session_id'],
            'questionnaire_id' => $_POST['questionnaire_id']
        ]);
    $reponse['result'] = $qreponse->delete();
    
    echo json_encode($reponse);
    die();
}