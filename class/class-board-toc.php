<?php
/*
 * class-board-toc.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/* Crée une table des matières pour tableau de bord */
class BoardToc
{
    private $toc = array();
    
    public function __construct()
    {
    }
    
    public function set_title($title, $level = 2)
    {
        $level = ($level < 2) ? 2 : $level;
        $level = ($level > 5) ? 5 : $level;
        $anchor = sanitize_title($title);
        $this->toc[] = (object) array('level' => $level, 'title' => $title, 'anchor' => $anchor);
        return '<h'.$level.' id="'.sanitize_title($title).'">'.esc_html($title).'</h'.$level.'>';
    }
    
    public function get_html_toc($class = "")
    {
        ob_start();
        ?>
        <div class="toc <?php echo $class; ?>">
            <div class="title"><span><?php _e("Sommaire"); ?></span></div>
            <div class="content">
                <?php foreach($this->toc as $item) : ?>
                    <p class="toc-level-<?php echo $item->level; ?>"><a href="#<?php echo $item->anchor; ?>"><?php echo $item->title; ?></a></p>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
}
