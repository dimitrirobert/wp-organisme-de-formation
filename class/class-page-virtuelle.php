<?php
/*
 * class-page-virtuelle.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/wpof-custom-post-types.php");

class PageVirtuelle
{
    private $args = NULL;
    private $url = "";
 
    public function __construct($args)
    {
        $this->args = $args; // save for use in virtual_page() method
        add_filter('the_posts', array($this, 'get_virtual_page'));
    }

    public function pre_get_shortlink_filter($return, $id = 0, $context = 0, $allow_slugs = 0)
    {
        return $this->url;
    }
    
    // filter callback to create virtual page content
    public function get_virtual_page($posts)
    {
        global $wp, $wpof;
        $slug_var = "url_".$this->args['pagename'];
        $slug = $wpof->$slug_var;
        
        if (in_array($slug, $wpof->no_theme))
        {
            // create a fake post instance
            $post = new stdClass();
 
            // fill properties of $post with everything a page in the database would have
            $post->ID = -15;                          // use an illegal value for page ID
            $post->post_author = 1;   // post author id
            $post->post_date = current_time('mysql'); // date of post
            $post->post_date_gmt = current_time('mysql', 1);
            
            $content_var = "get_".$this->args['pagename']."_content";
            $params = array();
            switch($this->args['pagename'])
            {
                case 'user':
                    if (isset($this->args['params'][0]))
                    {
                        $params['username'] = $this->args['params'][0];
                        if (get_current_user_id() > 0)
                        {
                            if (isset($this->args['params'][1]) && isset($wpof->onglet_user[$this->args['params'][1]]))
                                $onglet_user_id = $this->args['params'][1];
                            else
                                $onglet_user_id = array_key_first($wpof->onglet_user);
                            $this->args['title'] = $wpof->onglet_user[$onglet_user_id]." – ".$wpof->{"title_".$this->args['pagename']};
                        }
                    }
                    break;
                case 'gestion':
                    if (isset($this->args['params'][0]))
                    {
                        $page_title = array();
                        $params['subpage'] = $sub_page = $this->args['params'][0];
                        if (isset($wpof->pages_gestion[$sub_page]))
                        {
                            require_once(wpof_path . "/pages/gestion/".$sub_page.".php");
                            if (isset($wpof->{$sub_page."_sub_pages"}))
                            {
                                $sub_sub_pages = $wpof->{$sub_page."_sub_pages"};
                                if (isset($this->args['params'][1]) && isset($sub_sub_pages[$this->args['params'][1]]))
                                    $sub_sub_page = $this->args['params'][1];
                                else
                                    $sub_sub_page = array_key_first($sub_sub_pages);
                                $page_title[] = $sub_sub_pages[$sub_sub_page];
                            }
                            $page_title[] = $wpof->pages_gestion[$params['subpage']];
                            $page_title[] = $wpof->{"title_".$this->args['pagename']};
                            $this->args['title'] = implode(' – ', $page_title);
                        }
                    }
                    break;
                case 'info':
                    if (isset($this->args['params'][0]))
                        $params['info_id'] = $this->args['params'][0];
                    break;
                default:
                    break;
            }
            
            $post->post_content = $content_var($params);
            $post->post_content .= get_message_box();
            if (debug)
                $post->post_content .= get_global_log();
            
            if (!empty($this->args['title']))
                $post->post_title = $this->args['title'];
            else
            {
                $title_var = "title_".$this->args['pagename'];
                $post->post_title = $wpof->$title_var;
            }
            $post->post_excerpt = '';
            $post->post_status = 'publish';
            $post->comment_status = 'closed';        // mark as closed for comments, since page doesn't exist
            $post->ping_status = 'closed';           // mark as closed for pings, since page doesn't exist
            $post->post_password = '';               // no password
            $post->post_name = $slug;
            $post->to_ping = '';
            $post->pinged = '';
            $post->post_modified = $post->post_date;
            $post->post_modified_gmt = $post->post_date_gmt;
            $post->post_content_filtered = '';
            $post->post_parent = 0;
            $post->guid = get_home_url( '/' . $slug );
            $post->menu_order = 0;
            $post->post_type = 'virtual';
            $post->post_mime_type = '';
            $post->comment_count = 0;
 
            // allows for any last minute updates to the $post content
            $post = apply_filters('opaga_virtual_page_content', $post);
            
            $this->url = $post->guid;
            add_filter('pre_get_shortlink', array($this, 'pre_get_shortlink_filter'));
 
            // set filter results
            $posts[] = $post;
            
            // reset wp_query properties to simulate a found page
            global $wp_query;
            $wp_query->is_page = FALSE;
            $wp_query->is_singular = TRUE;
            $wp_query->is_single = TRUE;
            $wp_query->is_home = FALSE;
            $wp_query->is_front_page = FALSE;
            $wp_query->is_archive = FALSE;
            $wp_query->is_category = FALSE;
            unset($wp_query->query['error']);
            $wp_query->query_vars['error'] = '';
            $wp_query->is_404 = FALSE;
        }
        return $posts;
    }
}
