<?php
/*
 * class-export.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


abstract class Export
{
    private $spreadsheet;
    private $filename;
    private $format;
    
    abstract protected function export();
    
    public function get_spreadsheet()
    {
        return $this->spreadsheet;
    }
    
    public function get_filemane()
    {
        return $this->filename;
    }
    
    public function get_format()
    {
        return $this->format;
    }
    
    public function set_spreadsheet($arg)
    {
        $this->spreadsheet = $arg;
    }
    
    public function set_filemane($arg)
    {
        $this->filename = $arg;
    }
    
    public function set_format($arg)
    {
        $this->format = $arg;
    }
}
